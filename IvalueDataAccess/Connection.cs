﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvalueDataAccess
{
    public static class Connection
    {
        #region Member Variable


        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <value>The connection string.</value>
        public static string SqlConnectionString
        {
            get
            {
                // return string.Empty;
                return ConfigurationManager.ConnectionStrings["Ivaluecon"].ConnectionString;

            }
        }



        #endregion

        #region Constructor



        #endregion

        #region Member Function


        #endregion
    }
}
