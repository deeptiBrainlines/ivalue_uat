﻿/// <reference path="decintellvalidation.js" />

/////   Alphanumeric and Special char validation

function IsANSp(evt, i, j, k) {
    var strWarning = "#Valid Input# 0-9,a-z,A-Z,-,.,space,&,@";
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 32 || charCode == 38 ||
        charCode == 45 || charCode == 46 || charCode == 64 ||
        (charCode > 47 && charCode < 58) || (charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123)) {
        $(i).css("display", "none");
        $(i).html(k);
        $(j).css("display", "none");
        return true
    } else {
        $(j).css("display", "block");
        $(j).html(strWarning);
        $(i).css("display", "none");
        return false
    }
}

// Alphnumeric and lower case and underscore

function IsANSLowerp(evt, i, j, k) {
    var strWarning = "#Valid Input# 0-9,a-z,_";
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 95  || (charCode > 47 && charCode < 58) || (charCode > 96 && charCode < 123)) {
        $(i).css("display", "none");
        $(i).html(k);
        $(j).css("display", "none");
        return true
    } else {
        $(j).css("display", "block");
        $(j).html(strWarning);
        $(i).css("display", "none");
        return false
    }
}

function IsEMAILID(evt, i, j, k) {
    var strWarning = "#Valid Input# 0-9,a-z,_,.,@";
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 95 || charCode == 64 ||
        charCode == 46 || charCode == 64 ||
        (charCode > 47 && charCode < 58) || (charCode > 96 && charCode < 123)) {
        $(i).css("display", "none");
        $(i).html(k);
        $(j).css("display", "none");
        return true
    } else {
        $(j).css("display", "block");
        $(j).html(strWarning);
        $(i).css("display", "none");
        return false
    }
}





/////   Integer validation

function IsNUM(evt, i, j, k) {
    var strWarning = "#Valid Input# 0-9";
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 47 && charCode < 58) {
        $(i).css("display", "none");
        $(i).html(k);
        $(j).css("display", "none");
        return true
    } else {
        $(j).css("display", "block");
        $(j).html(strWarning);
        $(i).css("display", "none");
        return false
    }
}

/////   Decimal validation


function IsDEC(evt, i, j, k) {
    var strWarning = "#Valid Input# 0-9,.";
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (
        (charCode > 47 && charCode < 58) || charCode == 46) {
        $(i).css("display", "none");
        $(i).html(k);
        $(j).css("display", "none");
        return true
    } else {
        $(j).css("display", "block");
        $(j).html(strWarning);
        $(i).css("display", "none");
        return false
    }
}



//////Range validateion

function IsALNUM(evt, i, j, k) {
    var strWarning = "#Valid Input# +,/,-,.,0-9 Example# +/- 5.5";
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 47 && charCode < 58 || charCode == 43 || charCode == 45 || charCode == 46 || charCode == 47) {
        $(i).css("display", "none");
        $(i).html(k);
        $(j).css("display", "none");
        return true
    } else {
        $(j).css("display", "block");
        $(j).html(strWarning);
        $(i).css("display", "none");
        return false
    }
}

// Password validation
function PasswordValidation() {
    var ChangePassword = $("#ChangePassword").val();
    var count = ChangePassword.length;
    if (count >= 8) {
        var regex = new Array();
        regex.push("[A-Z]"); //Uppercase Alphabet.
        regex.push("[a-z]"); //Lowercase Alphabet.
        regex.push("[0-9]"); //Digit.
        regex.push("[!@@#$%^&*]"); //Special Character.

        var passed = 0;
        for (var i = 0; i < regex.length; i++) {
            if (new RegExp(regex[i]).test(ChangePassword)) {
                passed++;
            }
        }
        if (passed > 3) {

        }
        else {
            alert("Password must contain at least 1 capital letter,\n\n1 small letter, 1 number and 1 special character");
            $("#ChangePassword").val('');
            return false;
        }
    }
    else {
        alert('Password must be greater than 8 character');
    }
}

//Check current password
function CheckPassword() {
        var CurrentPassword = $("#CurrentPassword").val();
        $.ajax({
            url: "/Profile/checkPasswordExist",
           // url: '@Url.Action("checkPasswordExist", "Profile")',
            data: { CurrentPassword: CurrentPassword },
            type: 'GET',
            dataType: "json",
            success: function (data) {
                if (data.message != "") {
                    $("#CurrentPassword").val('');
                    alert(data.message);
                } else {
                }
            },
            error: function () {
            }
        });
    }


