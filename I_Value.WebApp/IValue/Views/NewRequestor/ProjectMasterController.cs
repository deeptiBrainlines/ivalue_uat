﻿using I_Value.WebApp.Models;
using IvalueDataAccess;
using IValuePublishProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IValuePublishProject.Controllers
{
    public class ProjectMasterController : Controller
    {

        SQLDataAccess sqlDataAccess;
        SQLDataAccess sqlDataAccess1;
        Hashtable hInputPara;
        Hashtable hInputPara1;
        IvalueDataModel Datamodel = new IvalueDataModel();
        ViewModel viewModel;
        // GET: ProjectMaster
        public ActionResult Index()
        {
            try
            {
                ViewModel vmreq = new ViewModel();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                DataTable dtprojects = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectIndexdetails");

                List<VMProjectIndex> Listprojects = new List<VMProjectIndex>();


                if (dtprojects.Rows.Count > 0)

                {
                    for (int i = 0; i < dtprojects.Rows.Count; i++)
                    {

                        Listprojects.Add(new VMProjectIndex
                        {
                            Project_ID = Convert.ToInt32(dtprojects.Rows[i]["Project_ID"]),
                            BuilderGroupName = Convert.ToString(dtprojects.Rows[i]["BuilderGroupName"]),
                            Builder_Company_Name = Convert.ToString(dtprojects.Rows[i]["Builder_Company_Name"]),
                            Project_Name = Convert.ToString(dtprojects.Rows[i]["Project_Name"]),
                            City = Convert.ToString(dtprojects.Rows[i]["City"]),
                            State = Convert.ToString(dtprojects.Rows[i]["State"]),

                        });
                    }
                }
                vmreq.VMProjectIndexs = Listprojects;

                return View(vmreq);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }

        }



        public ActionResult ProjectSearch()
        {
            return View();
        }

        public ActionResult AddProject()
        {
            ViewModel vm = new ViewModel();
            vm.ival_BuilderGroupMasters = GetBuilderMaster();
            vm.ival_States = GetStates();
            vm.ival_LookupCategoryValuestypeofownership = GetTypeofOwnership();
            vm.ival_LookupCategoryValuesNatureofproject = GetNatureofproject();
            vm.ival_LookupCategoryValuesProjecttype = GetProjectTypeMaster();
            //vm.ival_LookupCategoryValuesunittype = GetChkboxunits();
            vm.ival_LookupCategoryLiftTypes = GetLiftType();
            vm.ival_LookupCategoryextfinish = Getextfinish();
            vm.ival_LookupCategoryValuesTypeOfStrycture = GetTypeOfStructure();
            vm.ival_LookupCategoryValuesProjecttype = GetProjectTypeMaster();
            vm.ival_LookupCategoryValuesQualityOfConstruction = GetQualityOfConstruction();
            vm.ival_LookupCategoryValuesMaintenanceOfproperty = GetMaintenanceOfProperty();
            vm.ival_LookupCategoryValuesdemarcation = GetDemarcation();
            vm.ival_LookupCategoryValuesRoadFinish = GetRoadFinish();
            vm.ival_ProjectApprovalDetailsnew = GetApprovalTypesnew();
            vm.ival_LookupCategoryBoundaries = GetBoundaries();
            vm.ival_LookupCategoryValuesSocialDev = GetSocialDevelopment();
            vm.ival_LookupCategoryValuesbasicInfra = GetBasicInfra();
            vm.ival_LookupCategoryValuesconnectivity = GetConnectivity();
            vm.ival_LookupCategoryValuesClassOfLocality = GetClassoflocality();
            vm.ival_LookupCategoryValuesTypeOfLocality = GetTypeOfLocality();
            vm.ival_LookupCategoryValuesProjectUnitDescription = GetTypeOfUnitdesc();
            vm.ival_DocumentListNew = GetdocumentsofProject();
            vm.ival_DocumentListNewBuilder = Getdocumentsofbildercomp();

            DataTable DocID = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1DOCIDListNew");
            ViewBag.Srno = DocID.Rows[0]["Document_ID"].ToString();
            DataTable SrNo = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1DoclListNewSrNoIdproject");
            ViewBag.SrnoNew = SrNo.Rows[0]["SrNo"].ToString();
            return View(vm);
        }

        public List<ival_BuilderGroupMaster> GetBuilderMaster()
        {
            List<ival_BuilderGroupMaster> ListBuilders = new List<ival_BuilderGroupMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuliderGroup");
            for (int i = 0; i < dtBuildermaster.Rows.Count; i++)
            {

                ListBuilders.Add(new ival_BuilderGroupMaster
                {
                    Builder_Group_Master_ID = Convert.ToInt32(dtBuildermaster.Rows[i]["Builder_Group_Master_ID"]),
                    BuilderGroupName = Convert.ToString(dtBuildermaster.Rows[i]["BuilderGroupName"])

                });


            }
            return ListBuilders;
        }

        public JsonResult GetCompByGroupId(int Group_Id)
        {
            List<ival_BuilderCompanyMasterNew> ival_Compmaster = new List<ival_BuilderCompanyMasterNew>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Builder_Group_Master_ID", Group_Id);
            DataTable dtcompmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuliderCompanybygrpid", hInputPara);
            for (int i = 0; i < dtcompmaster.Rows.Count; i++)
            {
                ival_Compmaster.Add(new ival_BuilderCompanyMasterNew
                {
                    Builder_Company_Master_ID = Convert.ToInt32(dtcompmaster.Rows[i]["Builder_Company_Master_ID"]),
                    Builder_Company_Name = Convert.ToString(dtcompmaster.Rows[i]["Builder_Company_Name"]),

                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonival_Compmaster = jsonSerialiser.Serialize(ival_Compmaster);
            return Json(jsonival_Compmaster);
        }


        [HttpPost]
        public List<ival_States> GetStates()
        {
            List<ival_States> ListStates = new List<ival_States>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtCompBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetStateByID");
            for (int i = 0; i < dtCompBuildermaster.Rows.Count; i++)
            {

                ListStates.Add(new ival_States
                {
                    State_ID = Convert.ToInt32(dtCompBuildermaster.Rows[i]["State_ID"]),
                    State_Name = Convert.ToString(dtCompBuildermaster.Rows[i]["State_Name"])

                });


            }
            return ListStates;
        }
        public JsonResult GetDistByStateId(int State_Id)
        {
            List<ival_Districts> ival_Districts = new List<ival_Districts>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@State_ID", State_Id);
            DataTable dtdist = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDisticts", hInputPara);
            for (int i = 0; i < dtdist.Rows.Count; i++)
            {
                ival_Districts.Add(new ival_Districts
                {
                    District_ID = Convert.ToInt32(dtdist.Rows[i]["District_ID"]),
                    District_Name = Convert.ToString(dtdist.Rows[i]["District_Name"]),

                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectdistMaster = jsonSerialiser.Serialize(ival_Districts);
            return Json(jsonProjectdistMaster);
        }

        public JsonResult GetcitybydistId(int District_ID)
        {
            List<ival_Cities> ival_city = new List<ival_Cities>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@District_ID", District_ID);
            DataTable dtcity = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCitys", hInputPara);
            for (int i = 0; i < dtcity.Rows.Count; i++)
            {
                ival_city.Add(new ival_Cities
                {
                    Loc_Id = Convert.ToInt32(dtcity.Rows[i]["City_ID"]),
                    City_Name = Convert.ToString(dtcity.Rows[i]["City_Name"]),

                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectcityMaster = jsonSerialiser.Serialize(ival_city);
            return Json(jsonProjectcityMaster);
        }

        public JsonResult GetpinbyCityId(int City_ID)
        {
            List<ival_Cities> ival_pin = new List<ival_Cities>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@City_ID", City_ID);
            DataTable dtpin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPinbycity", hInputPara);
            for (int i = 0; i < dtpin.Rows.Count; i++)
            {
                ival_pin.Add(new ival_Cities
                {
                    Loc_Id = Convert.ToInt32(dtpin.Rows[i]["City_ID"]),
                    City_Name = Convert.ToString(dtpin.Rows[i]["City_Name"]),
                    Pin_Code = Convert.ToString(dtpin.Rows[i]["Pin_Code"]),

                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectpinMaster = jsonSerialiser.Serialize(ival_pin);
            return Json(jsonProjectpinMaster);
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetTypeofOwnership()
        {
            List<ival_LookupCategoryValues> Listtypeofowner = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dttypeofowner = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTypeOfOwnership");
            for (int i = 0; i < dttypeofowner.Rows.Count; i++)
            {

                Listtypeofowner.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dttypeofowner.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dttypeofowner.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dttypeofowner.Rows[i]["Lookup_Value"])

                });

            }
            return Listtypeofowner;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetNatureofproject()
        {
            List<ival_LookupCategoryValues> Listprjtrans = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtnatureprjtrans = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNatureOfProject");
            for (int i = 0; i < dtnatureprjtrans.Rows.Count; i++)
            {

                Listprjtrans.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtnatureprjtrans.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtnatureprjtrans.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtnatureprjtrans.Rows[i]["Lookup_Value"])

                });

            }
            return Listprjtrans;
        }

        [HttpPost]
        public ActionResult SaveProjectDetails(string str)
        {
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

            for (int i = 0; i < list1.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                hInputPara.Add("@Builder_Group_ID", Convert.ToInt32(list1.Rows[i]["ddlgroupmaster"]));
                hInputPara.Add("@Builder_Company_ID", Convert.ToInt32(list1.Rows[i]["ddlcompanymaster"]));
                hInputPara.Add("@Project_Name", Convert.ToString(list1.Rows[i]["txtprjname"]));
                hInputPara.Add("@Plot_Number", Convert.ToString(list1.Rows[i]["txtplotnum"]));
                hInputPara.Add("@Survey_Number", Convert.ToString(list1.Rows[i]["txtsurveynum"]));
                hInputPara.Add("@Village_Name", Convert.ToString(list1.Rows[i]["txtvillagename"]));
                hInputPara.Add("@Street", Convert.ToString(list1.Rows[i]["txtstreet"]));
                hInputPara.Add("@Locality", Convert.ToString(list1.Rows[i]["txtlocality"]));
                hInputPara.Add("@Sub_Locality", Convert.ToString(list1.Rows[i]["txtsublocality"]));
                hInputPara.Add("@State", Convert.ToString(list1.Rows[i]["ddlstatemaster"]));
                hInputPara.Add("@District", Convert.ToString(list1.Rows[i]["txtDistrict"]));
                hInputPara.Add("@City", Convert.ToString(list1.Rows[i]["txtCity"]));
                hInputPara.Add("@PinCode", Convert.ToString(list1.Rows[i]["ddlpincodemaster"]));
                hInputPara.Add("@Type_Of_Ownership", Convert.ToString(list1.Rows[i]["ddlOwner"]));
                hInputPara.Add("@Name_Of_PropertyOwner1", Convert.ToString(list1.Rows[i]["txtpropowner1"]));
                hInputPara.Add("@Name_Of_PropertyOwner2", Convert.ToString(list1.Rows[i]["txtpropowner2"]));
                hInputPara.Add("@Nature_Of_ProjectTransaction", Convert.ToString(list1.Rows[i]["ddlnatureofprj"]));
                hInputPara.Add("@Created_By", Session["UserName"].ToString());
                sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectMasterDetails", hInputPara);
                hInputPara.Clear();

            }

            return Json(new { success = true, Message = "Project details Inserted Successfully" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateProjectDetails(int ProjectID ,string str)
        {
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

            for (int i = 0; i < list1.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@Projet_ID", Convert.ToInt32(ProjectID));
                hInputPara.Add("@Builder_Group_ID", Convert.ToInt32(list1.Rows[i]["ddlgroupmaster"]));
                hInputPara.Add("@Builder_Company_ID", Convert.ToInt32(list1.Rows[i]["ddlcompanymaster"]));
                hInputPara.Add("@Project_Name", Convert.ToString(list1.Rows[i]["txtprjname"]));
                hInputPara.Add("@Plot_Number", Convert.ToString(list1.Rows[i]["txtplotnum"]));
                hInputPara.Add("@Survey_Number", Convert.ToString(list1.Rows[i]["txtsurveynum"]));
                hInputPara.Add("@Village_Name", Convert.ToString(list1.Rows[i]["txtvillagename"]));
                hInputPara.Add("@Street", Convert.ToString(list1.Rows[i]["txtstreet"]));
                hInputPara.Add("@Locality", Convert.ToString(list1.Rows[i]["txtlocality"]));
                hInputPara.Add("@Sub_Locality", Convert.ToString(list1.Rows[i]["txtsublocality"]));
                hInputPara.Add("@State", Convert.ToString(list1.Rows[i]["ddlstatemaster"]));
                hInputPara.Add("@District", Convert.ToString(list1.Rows[i]["txtDistrict"]));
                hInputPara.Add("@City", Convert.ToString(list1.Rows[i]["txtCity"]));
                hInputPara.Add("@PinCode", Convert.ToString(list1.Rows[i]["ddlpincodemaster"]));
                hInputPara.Add("@Type_Of_Ownership", Convert.ToString(list1.Rows[i]["ddlOwner"]));
                hInputPara.Add("@Name_Of_PropertyOwner1", Convert.ToString(list1.Rows[i]["txtpropowner1"]));
                hInputPara.Add("@Name_Of_PropertyOwner2", Convert.ToString(list1.Rows[i]["txtpropowner2"]));
                hInputPara.Add("@Nature_Of_ProjectTransaction", Convert.ToString(list1.Rows[i]["ddlnatureofprj"]));
                hInputPara.Add("@Created_By", Session["UserName"].ToString());
                sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectMasterDetails", hInputPara);
                hInputPara.Clear();

            }

            return Json(new { success = true, Message = "Project details Updated Successfully" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetProjectTypeMaster()
        {
            List<ival_LookupCategoryValues> Listprojtype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtprojecttypermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectType");
            for (int i = 0; i < dtprojecttypermaster.Rows.Count; i++)
            {

                Listprojtype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtprojecttypermaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtprojecttypermaster.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtprojecttypermaster.Rows[i]["Lookup_Value"])

                });
            }
            return Listprojtype;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetChkboxunits()
        {
            List<ival_LookupCategoryValues> Listunittype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtunittypermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitTypeNew");
            for (int i = 0; i < dtunittypermaster.Rows.Count; i++)
            {

                Listunittype.Add(new ival_LookupCategoryValues
                {
                    Lookup_Value = dtunittypermaster.Rows[i]["UnitType"].ToString(),

                });

            }
            return Listunittype;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetLiftType()
        {
            List<ival_LookupCategoryValues> ListLiftType = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtLiftType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLiftType");
            for (int i = 0; i < dtLiftType.Rows.Count; i++)
            {

                ListLiftType.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtLiftType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtLiftType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtLiftType.Rows[i]["Lookup_Value"])

                });

            }
            return ListLiftType;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> Getextfinish()
        {
            List<ival_LookupCategoryValues> Listextfinish = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtextfinish = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetextFinish");
            for (int i = 0; i < dtextfinish.Rows.Count; i++)
            {

                Listextfinish.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtextfinish.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtextfinish.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtextfinish.Rows[i]["Lookup_Value"])

                });

            }
            return Listextfinish;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetTypeOfStructure()
        {
            List<ival_LookupCategoryValues> ListTypeOfStructure = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtTypeOfStructure = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTypeOfstructure");
            for (int i = 0; i < dtTypeOfStructure.Rows.Count; i++)
            {

                ListTypeOfStructure.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtTypeOfStructure.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtTypeOfStructure.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtTypeOfStructure.Rows[i]["Lookup_Value"])

                });

            }
            return ListTypeOfStructure;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetQualityOfConstruction()
        {
            List<ival_LookupCategoryValues> ListQualityOfConstruction = new List<ival_LookupCategoryValues>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            DataTable dtQualityOfConstruction = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetQualityOfConstruction");
            for (int i = 0; i < dtQualityOfConstruction.Rows.Count; i++)
            {
                ListQualityOfConstruction.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtQualityOfConstruction.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtQualityOfConstruction.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtQualityOfConstruction.Rows[i]["Lookup_Value"])

                });
            }
            return ListQualityOfConstruction;
        }
        public List<ival_LookupCategoryValues> GetMaintenanceOfProperty()
        {
            List<ival_LookupCategoryValues> ListOfMaintenanceOfProperty = new List<ival_LookupCategoryValues>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtMaintenanceOfprop = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetMaintenanceOfProperty");
            for (int i = 0; i < dtMaintenanceOfprop.Rows.Count; i++)
            {
                ListOfMaintenanceOfProperty.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtMaintenanceOfprop.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtMaintenanceOfprop.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtMaintenanceOfprop.Rows[i]["Lookup_Value"])

                });
            }
            return ListOfMaintenanceOfProperty;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetDemarcation()
        {
            List<ival_LookupCategoryValues> ListDemarcation = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtdemarcation = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTypeOfDemarcation");
            for (int i = 0; i < dtdemarcation.Rows.Count; i++)
            {

                ListDemarcation.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtdemarcation.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtdemarcation.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtdemarcation.Rows[i]["Lookup_Value"])

                });

            }
            return ListDemarcation;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetRoadFinish()
        {
            List<ival_LookupCategoryValues> ListRoad = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtroad = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRoadFinish");
            for (int i = 0; i < dtroad.Rows.Count; i++)
            {

                ListRoad.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtroad.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtroad.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtroad.Rows[i]["Lookup_Value"])

                });

            }
            return ListRoad;
        }

        [HttpPost]
        public List<ival_ProjectApprovalDetails> GetApprovalTypesnew()
        {
            List<ival_ProjectApprovalDetails> ListApprovalType = new List<ival_ProjectApprovalDetails>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            CultureInfo provider = CultureInfo.InvariantCulture;
            DataTable dtProject_ID = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1ProjectdetailsID");
            int Project_ID = Convert.ToInt32(dtProject_ID.Rows[0]["Project_ID"]);
            // Session["Project_ID"] = Project_ID;
            hInputPara.Add("@Project_ID", Project_ID);

            DataTable dtApprovalType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDetailsbyPRJIDNew", hInputPara);

            for (int i = 0; i < dtApprovalType.Rows.Count; i++)
            {
                if ((dtApprovalType.Rows[i]["Approval_Number"].ToString()) != String.Empty && (dtApprovalType.Rows[i]["Approval_Number"].ToString()) != null)
                {
                    ListApprovalType.Add(new ival_ProjectApprovalDetails
                    {
                        Approval_Type = Convert.ToString(dtApprovalType.Rows[i]["Document_Name"]),
                        Approving_Authority = Convert.ToString(dtApprovalType.Rows[i]["Approval_Authority"]),
                        Date_of_Approval = Convert.ToDateTime(dtApprovalType.Rows[i]["Date_Of_Approval"].ToString()),
                        Approval_Num = Convert.ToString(dtApprovalType.Rows[i]["Approval_Number"]),
                    });
                }
            }
            return ListApprovalType;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetBoundaries()
        {
            List<ival_LookupCategoryValues> ListBoundaries = new List<ival_LookupCategoryValues>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBoundaries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBoundryType");
            for (int i = 0; i < dtBoundaries.Rows.Count; i++)
            {

                ListBoundaries.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtBoundaries.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Value"])

                });

            }
            return ListBoundaries;
        }
        public List<ival_LookupCategoryValues> GetSocialDevelopment()
        {
            List<ival_LookupCategoryValues> ListSocialDev = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtFloors = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSocialDevelopment");
            for (int i = 0; i < dtFloors.Rows.Count; i++)
            {
                ListSocialDev.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtFloors.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtFloors.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtFloors.Rows[i]["Lookup_Value"])
                });
            }
            return ListSocialDev;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetBasicInfra()
        {
            List<ival_LookupCategoryValues> ListBasicInfra = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicInfraType");
            for (int i = 0; i < dtBasicInfra.Rows.Count; i++)
            {

                ListBasicInfra.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtBasicInfra.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtBasicInfra.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtBasicInfra.Rows[i]["Lookup_Value"])

                });

            }
            return ListBasicInfra;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetConnectivity()
        {
            List<ival_LookupCategoryValues> ListConnectivity = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtConnectivity = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalTransportType");
            for (int i = 0; i < dtConnectivity.Rows.Count; i++)
            {

                ListConnectivity.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtConnectivity.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtConnectivity.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtConnectivity.Rows[i]["Lookup_Value"])

                });

            }
            return ListConnectivity;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetClassoflocality()
        {
            List<ival_LookupCategoryValues> ListClassoflocality = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtListClassoflocality = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalityType");
            for (int i = 0; i < dtListClassoflocality.Rows.Count; i++)
            {

                ListClassoflocality.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtListClassoflocality.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtListClassoflocality.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtListClassoflocality.Rows[i]["Lookup_Value"])

                });

            }
            return ListClassoflocality;
        }
        public List<ival_LookupCategoryValues> GetTypeOfLocality()
        {
            List<ival_LookupCategoryValues> ListFloors = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtFloors = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTypeOfLocality");
            for (int i = 0; i < dtFloors.Rows.Count; i++)
            {
                ListFloors.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtFloors.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtFloors.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtFloors.Rows[i]["Lookup_Value"])
                });
            }
            return ListFloors;
        }
        public JsonResult GetUnitsBYPtype(string PType)
        {
            List<ival_UnitTypeParamterLink> ival_units = new List<ival_UnitTypeParamterLink>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@PType", PType);
            DataTable dtunits = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitTypeNew", hInputPara);
            for (int i = 0; i < dtunits.Rows.Count; i++)
            {
                ival_units.Add(new ival_UnitTypeParamterLink
                {
                    UnitType = Convert.ToString(dtunits.Rows[i]["UnitType"]),


                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectunits = jsonSerialiser.Serialize(ival_units);
            return Json(jsonProjectunits);
        }

        [HttpPost]
        public List<ival_ProjectUnitDescription> GetTypeOfUnitdesc()
        {
            List<ival_ProjectUnitDescription> Listunitdesc = new List<ival_ProjectUnitDescription>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtdesc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPrjUnitdesc");
            for (int i = 0; i < dtdesc.Rows.Count; i++)
            {

                Listunitdesc.Add(new ival_ProjectUnitDescription
                {
                    Desc_Unit_ID = Convert.ToInt32(dtdesc.Rows[i]["Desc_Unit_ID"]),
                    Description_of_Units = Convert.ToString(dtdesc.Rows[i]["Description_of_Units"]),
                    Type = Convert.ToString(dtdesc.Rows[i]["Type"])

                });

            }
            return Listunitdesc;
        }

        [HttpPost]
        public List<ival_DocumentListNew> GetdocumentsofProject()
        {
            List<ival_DocumentListNew> Listdoc = new List<ival_DocumentListNew>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPrjDocList");
            for (int i = 0; i < dtdoc.Rows.Count; i++)
            {

                Listdoc.Add(new ival_DocumentListNew
                {
                    Document_ID = Convert.ToInt32(dtdoc.Rows[i]["Document_ID"]),
                    Document_Name = Convert.ToString(dtdoc.Rows[i]["Document_Name"]),
                    Project_Type = Convert.ToString(dtdoc.Rows[i]["Project_Type"]),
                    SrNo = Convert.ToString(dtdoc.Rows[i]["SrNo"]),
                });

            }
            return Listdoc;
        }

        [HttpPost]
        public List<ival_DocumentListNew> Getdocumentsofbildercomp()
        {
            List<ival_DocumentListNew> Listdocbuilder = new List<ival_DocumentListNew>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtdocbuilder = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPrjDocListofbuildercomp");
            for (int i = 0; i < dtdocbuilder.Rows.Count; i++)
            {

                Listdocbuilder.Add(new ival_DocumentListNew
                {
                    Document_ID = Convert.ToInt32(dtdocbuilder.Rows[i]["Document_ID"]),
                    Document_Name = Convert.ToString(dtdocbuilder.Rows[i]["Document_Name"]),
                    Project_Type = Convert.ToString(dtdocbuilder.Rows[i]["Project_Type"]),
                    SrNo = Convert.ToString(dtdocbuilder.Rows[i]["SrNo"]),
                });

            }
            return Listdocbuilder;
        }

        public JsonResult Download()
        {
            var FilePath = Session["File"].ToString();
            var folderPath = Server.MapPath("~/UploadFiles");
            var fileCount = Directory.GetFiles(Server.MapPath("~/UploadFiles"));
            if (fileCount.Length > 1) // exactly only 1 file
            {
                string embed = "<object data=\"{0}\" type=\"application/pdf\"width=\"2000px\" height=\"1000px\">";
                embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
                embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                embed += "</object>";
                TempData["Embed"] = string.Format(embed, VirtualPathUtility.ToAbsolute("~/UploadFiles/" + FilePath));
                return Json("Exist", JsonRequestBehavior.AllowGet);
            }

            else
            {
                Process.Start(folderPath);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult DownloadPDF1()
        {
            return View();
        }

        [HttpPost]
        public JsonResult UploadPDFFiles(string DocId)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            string a = DocId.PadLeft(2, '0');
            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i];

                int fileSize = file.ContentLength;
                string fileName = file.FileName;
                string mimeType = file.ContentType;
                System.IO.Stream fileContent = file.InputStream;

                int Project_ID = Convert.ToInt32(Session["Project_ID"]);
                //int Project_ID = 33;
                var pdffile = Project_ID + "_" + a + "_" + fileName;

                file.SaveAs(Server.MapPath("../UploadFiles/") + pdffile); //File will be saved in application root
                Session["File"] = pdffile;
            }

            return Json("Uploaded " + Request.Files.Count + " files");
        }

        [HttpPost]
        public ActionResult UploadFiles(string str, string str1)
        {
            try
            {
                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                DataTable list2 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));
                Session["ApprovalDetails"] = list2;

                hInputPara = new Hashtable();
                hInputPara1 = new Hashtable();
                sqlDataAccess1 = new SQLDataAccess();
                sqlDataAccess = new SQLDataAccess();
                DataTable dtProject_ID = sqlDataAccess1.GetDatatableExecuteStoreProcedure("usp_Gettop1ProjectdetailsID");
                int Project_ID = Convert.ToInt32(dtProject_ID.Rows[0]["Project_ID"]);
                Session["Project_ID"] = Project_ID;

                if (list1.Rows.Count > 0)
                {
                    for (int i = 0; i < list1.Rows.Count; i++)
                    {
                        string fileName = "";

                        string file = list1.Rows[i]["filename"].ToString();

                        if (file != "")
                        {
                            string[] newValue = file.Split('\\');
                            if (newValue != null)
                            {
                                string docId = list1.Rows[i]["docid"].ToString();
                                string DocID = docId.PadLeft(2, '0');
                                string Name = newValue[newValue.Length - 1];
                                fileName = Project_ID + "_" + DocID + "_" + Name;
                            }
                        }



                        if ((list1.Rows[i]["apprnum"].ToString()) != String.Empty && (list1.Rows[i]["apprnum"].ToString()) != null)
                        {
                            int docId = Convert.ToInt32(list1.Rows[i]["docid"]);
                            //hInputPara.Add("@Document_ID_New", Convert.ToInt32(list1.Rows[i]["docid"]));
                            //hInputPara.Add("@ProjectID_New", Convert.ToInt32(Project_ID));
                            //string result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertNewUploadDocumentProject", hInputPara);
                            //if (result == "document already exist")
                            //{
                            //    if (list1.Rows[i]["filename"].ToString() != string.Empty && (list1.Rows[i]["filename"].ToString()) != null)
                            //    {
                            //        hInputPara1.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]));
                            //        hInputPara1.Add("@ProjectID", Convert.ToInt32(Project_ID));

                            //        //hInputPara1.Add("@Document_Name", Convert.ToString(list1.Rows[i]["filename"]));

                            //        hInputPara1.Add("@Document_Name", fileName);

                            //        hInputPara1.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));
                            //        var dateString = list1.Rows[i]["dateofapp"].ToString();
                            //        var format = "dd/MM/yyyy";
                            //        if (dateString != "")
                            //        {
                            //            var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                            //            hInputPara1.Add("@Date_Of_Approval", dateTime);
                            //        }
                            //        hInputPara1.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));
                            //        sqlDataAccess.GetDatatableExecuteStoreProcedure("[usp_UpdateNewUploadDocumentProject]", hInputPara1);
                            //    }
                            //    else
                            //    {
                            //        hInputPara1.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]));
                            //        hInputPara1.Add("@ProjectID", Convert.ToInt32(Project_ID));

                            //        hInputPara1.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));

                            //        var dateString = list1.Rows[i]["dateofapp"].ToString();
                            //        var format = "dd/MM/yyyy";
                            //        if (dateString != "")
                            //        {
                            //            var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                            //            hInputPara1.Add("@Date_Of_Approval", dateTime);
                            //        }

                            //        hInputPara1.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));
                            //        sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_UpdateBlankDocumentProject", hInputPara1);
                            //    }
                            //}
                            //else
                            //{

                            hInputPara1.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]));
                            hInputPara1.Add("@Document_Name", fileName);
                            hInputPara1.Add("@Project_ID", Convert.ToInt32(Project_ID));
                            hInputPara1.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));
                            try
                            {
                                string dateString = list1.Rows[i]["dateofapp"].ToString();
                                if (dateString != "")
                                {

                                    string datestring1 = Convert.ToDateTime(dateString).ToString("dd/MM/yyyy");
                                    var format = "dd/MM/yyyy";
                                    var dateTime = DateTime.ParseExact(datestring1, format, CultureInfo.InvariantCulture);
                                    hInputPara1.Add("@Date_Of_Approval", dateTime);
                                }
                            }
                            catch (Exception e)
                            {

                                var dateString = list1.Rows[i]["dateofapp"].ToString();
                                var format = "dd/MM/yyyy";
                                if (dateString != "")
                                {
                                    var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                    hInputPara1.Add("@Date_Of_Approval", dateTime);
                                }

                            }

                            hInputPara1.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));
                            sqlDataAccess.ExecuteStoreProcedure("usp_InsertUploadDocMaster", hInputPara1);

                            //   }

                        }

                        hInputPara.Clear();
                        hInputPara1.Clear();
                        if ((list1.Rows[i]["docname"].ToString()) != null && (list1.Rows[i]["docname"].ToString()) != string.Empty)
                        {
                            hInputPara1.Add("@Document_Name", Convert.ToString(list1.Rows[i]["docname"]));

                            sqlDataAccess.ExecuteStoreProcedure("usp_InsertDocMasterNew", hInputPara1);
                        }
                        hInputPara1.Clear();
                    }

                    return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new { success = true, Message = "NO records found" }, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public ActionResult SaveProjectDetailsOthers(string str, string str1, string Allunits, string strconnect, string stringBasicInfra, string stringSocialDev, string Allboundaries, string AllUnitdesc)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));
            DataTable list2 = (DataTable)JsonConvert.DeserializeObject(Allunits, (typeof(DataTable)));
            DataTable list3 = (DataTable)JsonConvert.DeserializeObject(strconnect, (typeof(DataTable)));
            DataTable list4 = (DataTable)JsonConvert.DeserializeObject(stringBasicInfra, (typeof(DataTable)));
            DataTable list5 = (DataTable)JsonConvert.DeserializeObject(stringSocialDev, (typeof(DataTable)));
            DataTable list6 = (DataTable)JsonConvert.DeserializeObject(Allboundaries, (typeof(DataTable)));
            DataTable list7 = (DataTable)JsonConvert.DeserializeObject(AllUnitdesc, (typeof(DataTable)));
            DataTable list8 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            DataTable dtProject_ID = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1ProjectdetailsID");
            //  int Project_ID =Convert.ToInt32(Session["Project_ID"]);
            int Project_ID = Convert.ToInt32(dtProject_ID.Rows[0]["Project_ID"]);
            // int Project_ID = 37;
            Session["Project_ID"] = Project_ID;
            try
            {
                for (int i = 0; i < list1.Rows.Count; i++)
                {

                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@NearBy_Landmark ", Convert.ToString(list1.Rows[i]["txtlandmark"]));
                    hInputPara.Add("@Nearest_RailwayStation", Convert.ToString(list1.Rows[i]["txtrailwaystation"]));
                    hInputPara.Add("@Nearest_BusStop", Convert.ToString(list1.Rows[i]["txtbusstop"]));
                    hInputPara.Add("@Nearest_Hopital", Convert.ToString(list1.Rows[i]["txthospital"]));
                    hInputPara.Add("@Nearest_Market", Convert.ToString(list1.Rows[i]["txtnearmarket"]));
                    hInputPara.Add("@Approved_No_Of_Buildings", Convert.ToString(list1.Rows[i]["txtapprednoofbuildings"]));
                    hInputPara.Add("@Approved_No_Wings", Convert.ToString(list1.Rows[i]["txtapprnoofwings"]));
                    hInputPara.Add("@Approved_No_Of_Floors", Convert.ToString(list1.Rows[i]["txtappooffloors"]));
                    hInputPara.Add("@Type_Of_Structure", Convert.ToString(list1.Rows[i]["ddltypeofstructure"]));
                    hInputPara.Add("@Approved_Usage_Of_Property", Convert.ToString(list1.Rows[i]["ddlApprovedusageprojecttype"]));
                    hInputPara.Add("@Actual_Usage_Of_Property", Convert.ToString(list1.Rows[i]["ddlActualprojecttype"]));
                    hInputPara.Add("@Current_Age_Of_Property", Convert.ToString(list1.Rows[i]["txtcurentage"]));
                    hInputPara.Add("@Residual_Age_Of_Property", Convert.ToString(list1.Rows[i]["txtresidualage"]));
                    hInputPara.Add("@ProposedAmenities", Convert.ToString(list1.Rows[i]["txtprposedamenities"]));
                    hInputPara.Add("@Common_Areas", Convert.ToString(list1.Rows[i]["txtcomonareas"]));
                    hInputPara.Add("@Facilities", Convert.ToString(list1.Rows[i]["txtFacilities"]));
                    hInputPara.Add("@Anyother_observation", Convert.ToString(list1.Rows[i]["txtanyotherobs"]));
                    hInputPara.Add("@Roofing_Terracing", Convert.ToString(list1.Rows[i]["txtroofterrace"]));
                    hInputPara.Add("@Quality_Of_Fixture", Convert.ToString(list1.Rows[i]["txtqualityfixture1"]));
                    hInputPara.Add("@Quality_Construction", Convert.ToString(list1.Rows[i]["ddlQualityOfConstruction"]));
                    hInputPara.Add("@Maintenance_Of_Property", Convert.ToString(list1.Rows[i]["ddlMaintenanceOfProp"]));
                    hInputPara.Add("@Marketability_Of_Property", Convert.ToString(list1.Rows[i]["txtmarketability"]));
                    hInputPara.Add("@BriefDescriptionoftheproperty", Convert.ToString(list1.Rows[i]["txtbrief"]));
                    hInputPara.Add("@Cord_Latitude", Convert.ToString(list1.Rows[i]["txtlatitude"]));
                    hInputPara.Add("@Cord_Longitude", Convert.ToString(list1.Rows[i]["txtlongitude"]));
                    hInputPara.Add("@AdjoiningRoad_Width1", Convert.ToString(list1.Rows[i]["txtroad1"]));
                    hInputPara.Add("@AdjoiningRoad_Width2", Convert.ToString(list1.Rows[i]["txtroad2"]));
                    hInputPara.Add("@AdjoiningRoad_Width3", Convert.ToString(list1.Rows[i]["txtroad3"]));
                    hInputPara.Add("@AdjoiningRoad_Width4", Convert.ToString(list1.Rows[i]["txtroad4"]));
                    hInputPara.Add("@Type_Of_Locality", Convert.ToString(list1.Rows[i]["ddlTypeOfLocality"]));
                    hInputPara.Add("@Class_Of_Locality", Convert.ToString(list1.Rows[i]["ddlclassoflocality"]));
                    hInputPara.Add("@DistFrom_City_Center", Convert.ToString(list1.Rows[i]["txtdistcity"]));
                    hInputPara.Add("@Remarks", Convert.ToString(list1.Rows[i]["txtremark"]));
                    hInputPara.Add("@Total_No_Residential_Units", Convert.ToString(list1.Rows[i]["txttotresidentialunits"]));
                    hInputPara.Add("@Total_No_Shops", Convert.ToString(list1.Rows[i]["txttotshops"]));
                    hInputPara.Add("@Lifts", Convert.ToString(list1.Rows[i]["ddlLifttype"]));
                    hInputPara.Add("@NoOfLifts", Convert.ToString(list1.Rows[i]["txtnooflifts"]));
                    hInputPara.Add("@Total_No_Commercial_Units", Convert.ToString(list1.Rows[i]["txttotnoofcommerialunits"]));
                    hInputPara.Add("@Total_No_Of_Units_Propsed", Convert.ToString(list1.Rows[i]["txtnoofunitsproposed"]));
                    hInputPara.Add("@Plot_Demarcated_At_Site", Convert.ToString(list1.Rows[i]["plotdemosite"]));
                    hInputPara.Add("@Type_Of_Demarcation", Convert.ToString(list1.Rows[i]["ddldemarcation"]));
                    hInputPara.Add("@Are_Boundaries_Matching", Convert.ToString(list1.Rows[i]["boundmatch"]));
                    hInputPara.Add("@FourBoundaries_Remarks", Convert.ToString(list1.Rows[i]["txtremarksboundaries"]));
                    hInputPara.Add("@Road_Finish", Convert.ToString(list1.Rows[i]["ddlroadfinish"]));
                    hInputPara.Add("@SeismicZone", Convert.ToString(list1.Rows[i]["txtseismic"]));
                    hInputPara.Add("@FloodZone", Convert.ToString(list1.Rows[i]["txtflood"]));
                    hInputPara.Add("@CycloneZone", Convert.ToString(list1.Rows[i]["txtcyclone"]));
                    hInputPara.Add("@CRZ", Convert.ToString(list1.Rows[i]["txtCRZ"]));
                    hInputPara.Add("@Final_Remarks", Convert.ToString(list1.Rows[i]["txtfinalremark"]));
                    hInputPara.Add("@Project_Registered_Under_RERA", Convert.ToString(list1.Rows[i]["isprjregistered"]));
                    hInputPara.Add("@Registration_Num", Convert.ToString(list1.Rows[i]["txtregno"]));
                    hInputPara.Add("@Num_Of_Buildings", Convert.ToString(list1.Rows[i]["txtnoofbuildings"]));
                    hInputPara.Add("@Num_Of_Wings", Convert.ToString(list1.Rows[i]["txtnoofwings"]));
                    hInputPara.Add("@Num_Of_Floors", Convert.ToString(list1.Rows[i]["txtnooffloors"]));
                    hInputPara.Add("@Num_Of_Units", Convert.ToString(list1.Rows[i]["txtnoofunits"]));
                    var dateString = list1.Rows[i]["datepicker1"].ToString();
                    hInputPara.Add("@Expected_Completion_Date", dateString);
                    hInputPara.Add("@Type_Of_Flooring", Convert.ToString(list1.Rows[i]["txttypeflooring"]));
                    hInputPara.Add("@Wall_Finish", Convert.ToString(list1.Rows[i]["txtwallfinish"]));
                    hInputPara.Add("@Plumbing_Fitting", Convert.ToString(list1.Rows[i]["txtplumfitting"]));
                    hInputPara.Add("@Electrical_Fitting", Convert.ToString(list1.Rows[i]["txtelectricalfit"]));
                    hInputPara.Add("@Quality_Of_Fixtures", Convert.ToString(list1.Rows[i]["txtqualityfixture1"]));
                    hInputPara.Add("@External_Finishing_Of_Property", Convert.ToString(list1.Rows[i]["ddlextfinish"]));

                    sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectMasterDetailsOthers", hInputPara);
                }
                for (int i4 = 0; i4 < list5.Rows.Count; i4++)
                {

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@Social_Development_Type", Convert.ToString(list5.Rows[i4]["value"]));
                    hInputPara.Add("@Created_By", "Branch Head");
                    var res3 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertSocialDev", hInputPara);
                }

                for (int i4 = 0; i4 < list3.Rows.Count; i4++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@LocalTransport_ID", Convert.ToString(list3.Rows[i4]["value"]));
                    hInputPara.Add("@Created_By", "Branch Head");
                    var res4 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectLocaltransport", hInputPara);
                }


                for (int i4 = 0; i4 < list4.Rows.Count; i4++)
                {
                    //DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1ProjectIdFromProjectMasterNew");

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@BasicInfra_type", Convert.ToString(list4.Rows[i4]["value1"]));
                    hInputPara.Add("@Created_By", "Branch Head");
                    var res5 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectBasicInfraTypes", hInputPara);
                }

                for (int i3 = 0; i3 < list6.Rows.Count; i3++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@Building_ID", 1);
                    hInputPara.Add("@Wing_ID", 1);
                    hInputPara.Add("@Unit_ID", 1);
                    hInputPara.Add("@Boundry_Name", list6.Rows[i3]["BoundariesType"].ToString());
                    hInputPara.Add("@AsPer_Document", list6.Rows[i3]["asperdoc"].ToString());
                    hInputPara.Add("@AsPer_Site", list6.Rows[i3]["aspersite"].ToString());
                    //hInputPara.Add("@Dimension", list4.Rows[i3]["Dimension"].ToString());

                    var res4 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectBoundaries", hInputPara);
                    hInputPara.Clear();
                }
                for (int i4 = 0; i4 < list2.Rows.Count; i4++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@UType_ID", Convert.ToString(list2.Rows[i4]["value"]));
                    sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectUnitTypes", hInputPara);
                }

                for (int i = 0; i < list7.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();

                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@Description_Of_Unit", Convert.ToString(list7.Rows[i]["Description_of_Units"]));
                    hInputPara.Add("@Carpet_Area", Convert.ToString(list7.Rows[i]["carpetarea"]));
                    hInputPara.Add("@Saleable_Area", Convert.ToString(list7.Rows[i]["Salablearea"]));
                    hInputPara.Add("@Total_No_Of_Units", Convert.ToString(list7.Rows[i]["Totalunits"]));
                    hInputPara.Add("@Gross_Saleable_Area_Of_Units", Convert.ToString(list7.Rows[i]["grosssalablearea"]));
                    var res1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectUnitdesc", hInputPara);
                }

                for (int i1 = 0; i1 < list8.Rows.Count; i1++)
                {

                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@Building_ID", 1);
                    hInputPara.Add("@Wing_ID", 1);
                    hInputPara.Add("@Approval_Type", Convert.ToString(list8.Rows[i1]["ApprovalType"]));
                    hInputPara.Add("@Approving_Authority", Convert.ToString(list8.Rows[i1]["Approving_Authority"]));
                    hInputPara.Add("@Date_of_Approval", Convert.ToString(list8.Rows[i1]["Approving_Date"]));
                    hInputPara.Add("@Approval_Num", Convert.ToString(list8.Rows[i1]["Approving_Num"]));
                    var res2 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectApprovalDetails", hInputPara);
                    hInputPara.Clear();
                }
            }
            catch (Exception ex)
            {
                ex.InnerException.ToString();
            }
            return Json(new { success = true, Message = "Project details Inserted Successfully" }, JsonRequestBehavior.AllowGet);

        }


        public ActionResult ProjectdetailsView(int ID)
        {
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Project_ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectdetailsByPrjID", hInputPara);
                ViewBag.BuilderGroupName = Convert.ToString(dtRequests.Rows[0]["BuilderGroupName"]);
                ViewBag.Builder_Company_Name = Convert.ToString(dtRequests.Rows[0]["Builder_Company_Name"]);
                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Name_Of_PropertyOwner1 = Convert.ToString(dtRequests.Rows[0]["Name_Of_PropertyOwner1"]);
                ViewBag.Name_Of_PropertyOwner2 = Convert.ToString(dtRequests.Rows[0]["Name_Of_PropertyOwner2"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Nature_Of_Company = Convert.ToString(dtRequests.Rows[0]["Nature_Of_Company"]);
                ViewBag.Plot_Number = Convert.ToString(dtRequests.Rows[0]["Plot_Number"]);
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Nature_Of_ProjectTransaction = Convert.ToString(dtRequests.Rows[0]["Nature_Of_ProjectTransaction"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);
                ViewBag.Nearest_Hopital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hopital"]);
                ViewBag.TypeOfProject = Convert.ToString(dtRequests.Rows[0]["TypeOfProject"]);
                ViewBag.Approved_No_Of_Buildings = Convert.ToString(dtRequests.Rows[0]["Approved_No_Of_Buildings"]);
                ViewBag.Approved_No_Wings = Convert.ToString(dtRequests.Rows[0]["Approved_No_Wings"]);
                ViewBag.Approved_No_Of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_Of_Floors"]);
                ViewBag.Total_No_Commercial_Units = Convert.ToString(dtRequests.Rows[0]["Total_No_Commercial_Units"]);
                ViewBag.Total_No_Residential_Units = Convert.ToString(dtRequests.Rows[0]["Total_No_Residential_Units"]);
                ViewBag.Total_No_Shops = Convert.ToString(dtRequests.Rows[0]["Total_No_Shops"]);
                ViewBag.Total_No_Of_Units_Propsed = Convert.ToString(dtRequests.Rows[0]["Total_No_Of_Units_Propsed"]);
                ViewBag.Lifts = Convert.ToString(dtRequests.Rows[0]["Lifts"]);
                ViewBag.NoOfLifts = Convert.ToString(dtRequests.Rows[0]["NoOfLifts"]);
                ViewBag.Type_Of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_Of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fitting = Convert.ToString(dtRequests.Rows[0]["Electrical_Fitting"]);
                ViewBag.External_Finishing_Of_Property = Convert.ToString(dtRequests.Rows[0]["External_Finishing_Of_Property"]);
                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);
                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Proposed_Amenities = Convert.ToString(dtRequests.Rows[0]["Proposed_Amenities"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                ViewBag.Anyother_observation = Convert.ToString(dtRequests.Rows[0]["Anyother_observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.Quality_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Construction"]);
                ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.BriefDescriptionoftheproperty = Convert.ToString(dtRequests.Rows[0]["BriefDescriptionoftheproperty"]);
                ViewBag.Type_Of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_Of_Locality"]);
                ViewBag.Class_Of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_Of_Locality"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks"]);
                ViewBag.Cord_Latitude = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude"]);
                ViewBag.Cord_Longitude = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude"]);
                ViewBag.AdjoiningRoad_Width1 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width1"]);
                ViewBag.AdjoiningRoad_Width2 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width2"]);
                ViewBag.AdjoiningRoad_Width3 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width3"]);
                ViewBag.AdjoiningRoad_Width4 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width4"]);
                ViewBag.Plot_Demarcated_At_Site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_At_Site"]);
                ViewBag.Type_Of_Demarcation = Convert.ToString(dtRequests.Rows[0]["Type_Of_Demarcation"]);
                ViewBag.Are_Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Are_Boundaries_Matching"]);
                ViewBag.FourBoundaries_Remarks = Convert.ToString(dtRequests.Rows[0]["FourBoundaries_Remarks"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                ViewBag.SeismicZone = Convert.ToString(dtRequests.Rows[0]["SeismicZone"]);
                ViewBag.FloodZone = Convert.ToString(dtRequests.Rows[0]["FloodZone"]);
                ViewBag.CycloneZone = Convert.ToString(dtRequests.Rows[0]["CycloneZone"]);
                ViewBag.CRZ = Convert.ToString(dtRequests.Rows[0]["CRZ"]);
                ViewBag.Final_Remarks = Convert.ToString(dtRequests.Rows[0]["Final_Remarks"]);
                ViewBag.Project_Registered_Under_RERA = Convert.ToString(dtRequests.Rows[0]["Project_Registered_Under_RERA"]);
                ViewBag.Registration_Num = Convert.ToString(dtRequests.Rows[0]["Registration_Num"]);
                ViewBag.Num_Of_Buildings = Convert.ToString(dtRequests.Rows[0]["Num_Of_Buildings"]);
                ViewBag.Num_Of_Wings = Convert.ToString(dtRequests.Rows[0]["Num_Of_Wings"]);
                ViewBag.Num_Of_Floors = Convert.ToString(dtRequests.Rows[0]["Num_Of_Floors"]);
                ViewBag.Num_Of_Units = Convert.ToString(dtRequests.Rows[0]["Num_Of_Units"]);
                ViewBag.Expected_Completion_Date = Convert.ToString(dtRequests.Rows[0]["Expected_Completion_Date"]);


                ViewBag.Builder_Address1 = Convert.ToString(dtRequests.Rows[0]["Builder_Address1"]);
                ViewBag.Builder_Address2 = Convert.ToString(dtRequests.Rows[0]["Builder_Address2"]);
                ViewBag.Director_Name1 = Convert.ToString(dtRequests.Rows[0]["Director_Name1"]);
                ViewBag.Director_Name2 = Convert.ToString(dtRequests.Rows[0]["Director_Name2"]);
                ViewBag.Director_Name3 = Convert.ToString(dtRequests.Rows[0]["Director_Name3"]);
                ViewBag.Director_Phone1 = Convert.ToString(dtRequests.Rows[0]["Director_Phone1"]);
                ViewBag.Director_Phone2 = Convert.ToString(dtRequests.Rows[0]["Director_Phone2"]);
                ViewBag.Director_Phone3 = Convert.ToString(dtRequests.Rows[0]["Director_Phone3"]);
                ViewBag.Director_EmailId1 = Convert.ToString(dtRequests.Rows[0]["Director_EmailId1"]);
                ViewBag.Director_EmailId2 = Convert.ToString(dtRequests.Rows[0]["Director_EmailId2"]);
                ViewBag.Director_EmailId3 = Convert.ToString(dtRequests.Rows[0]["Director_EmailId3"]);
                ViewBag.Builder_Grade = Convert.ToString(dtRequests.Rows[0]["Builder_Grade"]);
                ViewBag.Builder_Address1 = Convert.ToString(dtRequests.Rows[0]["Builder_Address1"]);
                ViewBag.Builder_Address2 = Convert.ToString(dtRequests.Rows[0]["Builder_Address2"]);
                ViewBag.Director_Mobile1 = Convert.ToString(dtRequests.Rows[0]["Director_Mobile1"]);
                ViewBag.Director_Mobile2 = Convert.ToString(dtRequests.Rows[0]["Director_Mobile2"]);
                ViewBag.Director_Mobile3 = Convert.ToString(dtRequests.Rows[0]["Director_Mobile3"]);

            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            ViewModel vm = new ViewModel();

            vm.ival_LocalTransportForProject = Gettransport(ID);
            vm.ival_SocialDevelopmentForProject = GetSocialInfra(ID);
            vm.ival_BasicInfraAvailability = GetSocialBasicInfra(ID);
            vm.ival_ProjectUnitDetail = GetUnitTypes(ID);
            vm.ival_ProjectBoundaries = GetBoundariesByReqID(ID);
            vm.ival_Document_Uploaded_ListS = GetFilesList(ID);
            vm.ival_ProjectDetails = GetProjectUnitDescription(ID);
            vm.ival_ProjectApprovalDetailsnew = GetApprovalTypesnewView(ID);
            return View(vm);

        }

        [HttpPost]
        public List<ival_ProjectApprovalDetails> GetApprovalTypesnewView(int ID)
        {
            List<ival_ProjectApprovalDetails> ListApprovalType = new List<ival_ProjectApprovalDetails>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            CultureInfo provider = CultureInfo.InvariantCulture;

            hInputPara.Add("@Project_ID", ID);

            DataTable dtApprovalType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDetailsbyPRJIDNew", hInputPara);

            for (int i = 0; i < dtApprovalType.Rows.Count; i++)
            {
                if ((dtApprovalType.Rows[i]["Approval_Number"].ToString()) != String.Empty && (dtApprovalType.Rows[i]["Approval_Number"].ToString()) != null)
                {
                    ListApprovalType.Add(new ival_ProjectApprovalDetails
                    {
                        Approval_Type = Convert.ToString(dtApprovalType.Rows[i]["Document_Name"]),
                        Approving_Authority = Convert.ToString(dtApprovalType.Rows[i]["Approval_Authority"]),
                        Date_of_Approval = Convert.ToDateTime(dtApprovalType.Rows[i]["Date_Of_Approval"].ToString()),
                        Approval_Num = Convert.ToString(dtApprovalType.Rows[i]["Approval_Number"]),
                    });
                }
            }
            return ListApprovalType;
        }

        [HttpPost]
        public List<ival_LocalTransportForProject> Gettransport(int ID)
        {
            List<ival_LocalTransportForProject> Listlocalinfra = new List<ival_LocalTransportForProject>();
            hInputPara = new Hashtable();

            hInputPara.Add("@Project_ID", ID);
            sqlDataAccess = new SQLDataAccess();
            DataTable dtlocal = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYPRJID", hInputPara);
            for (int i = 0; i < dtlocal.Rows.Count; i++)
            {

                Listlocalinfra.Add(new ival_LocalTransportForProject
                {
                    LocalTransport_ID = Convert.ToString(dtlocal.Rows[i]["LocalTransport_ID"]),
                    ProjectLocalTransport_ID = Convert.ToInt32(dtlocal.Rows[i]["ProjectLocalTransport_ID"]),
                });
            }
            return Listlocalinfra;
        }
        [HttpPost]
        public List<ival_SocialDevelopmentForProject> GetSocialInfra(int ID)
        {
            List<ival_SocialDevelopmentForProject> Listsocialinfra = new List<ival_SocialDevelopmentForProject>();
            hInputPara = new Hashtable();
            hInputPara.Add("@Project_ID", ID);
            sqlDataAccess = new SQLDataAccess();
            DataTable dtsocialinfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSocialDevBYprjID", hInputPara);
            for (int i = 0; i < dtsocialinfra.Rows.Count; i++)
            {

                Listsocialinfra.Add(new ival_SocialDevelopmentForProject
                {
                    Social_Dev_ID = Convert.ToInt32(dtsocialinfra.Rows[i]["Social_Dev_ID"]),
                    Social_Development_Type = Convert.ToString(dtsocialinfra.Rows[i]["Social_Development_Type"]),


                });
            }
            return Listsocialinfra;
        }

        [HttpPost]
        public List<ival_BasicInfraAvailability> GetSocialBasicInfra(int ID)
        {
            List<ival_BasicInfraAvailability> Listbasicinfra = new List<ival_BasicInfraAvailability>();
            hInputPara = new Hashtable();
            hInputPara.Add("@Project_ID", ID);
            sqlDataAccess = new SQLDataAccess();
            DataTable dtbasicinfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicInfraBYPRJID", hInputPara);
            for (int i = 0; i < dtbasicinfra.Rows.Count; i++)
            {

                Listbasicinfra.Add(new ival_BasicInfraAvailability
                {
                    ProjectInfra_ID = Convert.ToInt32(dtbasicinfra.Rows[i]["ProjectInfra_ID"]),
                    BasicInfra_type = Convert.ToString(dtbasicinfra.Rows[i]["BasicInfra_type"]),
                });
            }
            return Listbasicinfra;
        }
        [HttpPost]
        public List<ival_ProjectUnitDetails> GetUnitTypes(int ID)
        {
            List<ival_ProjectUnitDetails> Listbasicinfra = new List<ival_ProjectUnitDetails>();
            hInputPara = new Hashtable();
            hInputPara.Add("@Project_ID", ID);
            sqlDataAccess = new SQLDataAccess();
            DataTable dtbasicinfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUNITTYPESBYPRJID", hInputPara);
            for (int i = 0; i < dtbasicinfra.Rows.Count; i++)
            {

                Listbasicinfra.Add(new ival_ProjectUnitDetails
                {
                    ID = Convert.ToInt32(dtbasicinfra.Rows[i]["ID"]),
                    UType_ID = Convert.ToString(dtbasicinfra.Rows[i]["UType_ID"]),
                });
            }
            return Listbasicinfra;
        }

        [HttpPost]
        public List<ival_ProjectBoundaries> GetBoundariesByReqID(int ID)
        {
            List<ival_ProjectBoundaries> ListBoundaries = new List<ival_ProjectBoundaries>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            hInputPara.Add("@Project_ID", ID);
            DataTable dtBoundaries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBoudriesByPrjID", hInputPara);

            for (int i = 0; i < dtBoundaries.Rows.Count; i++)
            {

                ListBoundaries.Add(new ival_ProjectBoundaries
                {
                    Boundry_ID = Convert.ToInt32(dtBoundaries.Rows[i]["Boundry_ID"]),
                    Boundry_Name = Convert.ToString(dtBoundaries.Rows[i]["Boundry_Name"]),
                    AsPer_Document = Convert.ToString(dtBoundaries.Rows[i]["AsPer_Document"]),
                    AsPer_Site = Convert.ToString(dtBoundaries.Rows[i]["AsPer_Site"]),
                    Plot_Dimentions = Convert.ToString(dtBoundaries.Rows[i]["Plot_Dimentions"]),



                });

            }
            return ListBoundaries;
        }

        [HttpPost]
        public List<ival_Document_Uploaded_List> GetFilesList(int ID)
        {

            List<ival_Document_Uploaded_List> listfiles = new List<ival_Document_Uploaded_List>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Request_Id", ID);
            DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadFilesLists", hInputPara);
            for (int i = 0; i < dtdoc.Rows.Count; i++)
            {
                ival_Document_Uploaded_List obj = new ival_Document_Uploaded_List();

                //listfiles.Add(new ival_Document_Uploaded_List
                //{
                if ((dtdoc.Rows[i]["Docupload_ID"]) != DBNull.Value)
                {
                    obj.Docupload_ID = Convert.ToInt32(dtdoc.Rows[i]["Docupload_ID"]);
                }
                obj.Document_ID = Convert.ToInt32(dtdoc.Rows[i]["Document_ID"]);
                obj.Document_Name = Convert.ToString(dtdoc.Rows[i]["Document_Name"]);
                if ((dtdoc.Rows[i]["Uploaded_Document"]) != DBNull.Value)
                {
                    obj.Uploaded_Document = Convert.ToString(dtdoc.Rows[i]["Uploaded_Document"]);
                }
                if ((dtdoc.Rows[i]["Approval_Authority"]) != DBNull.Value)
                {
                    obj.Approval_Authority = Convert.ToString(dtdoc.Rows[i]["Approval_Authority"]);
                }
                if ((dtdoc.Rows[i]["Date_Of_Approval"]) != DBNull.Value)
                {

                    obj.Date_Of_Approval = Convert.ToDateTime(dtdoc.Rows[i]["Date_Of_Approval"]).Date;

                }
                if ((dtdoc.Rows[i]["Approval_Number"]) != DBNull.Value)
                {
                    obj.Approval_Number = Convert.ToString(dtdoc.Rows[i]["Approval_Number"]);
                }
                listfiles.Add(obj);
                //});

            }
            return listfiles;
        }

        [HttpPost]
        public List<ival_ProjectDetails> GetProjectUnitDescription(int ID)
        {
            List<ival_ProjectDetails> Listprjdetails = new List<ival_ProjectDetails>();
            hInputPara = new Hashtable();

            hInputPara.Add("@Project_ID", ID);
            sqlDataAccess = new SQLDataAccess();
            DataTable dtprjdetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectUnitdescByPrjID", hInputPara);
            for (int i = 0; i < dtprjdetails.Rows.Count; i++)
            {

                Listprjdetails.Add(new ival_ProjectDetails
                {
                    ProjectDetail_ID = Convert.ToInt32(dtprjdetails.Rows[i]["ProjectDetail_ID"]),
                    Project_ID = Convert.ToInt32(dtprjdetails.Rows[i]["Project_ID"]),
                    Description_Of_Unit = Convert.ToString(dtprjdetails.Rows[i]["Description_Of_Unit"]),
                    Carpet_Area = Convert.ToString(dtprjdetails.Rows[i]["Carpet_Area"]),
                    Saleable_Area = Convert.ToString(dtprjdetails.Rows[i]["Saleable_Area"]),
                    Total_No_Of_Units = Convert.ToString(dtprjdetails.Rows[i]["Total_No_Of_Units"]),
                    Gross_Saleable_Area_Of_Units = Convert.ToString(dtprjdetails.Rows[i]["Gross_Saleable_Area_Of_Units"]),

                });
            }
            return Listprjdetails;
        }



        public ActionResult EditProjectdetails(int ID)
        {
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            hInputPara.Add("@Project_ID", ID);
            DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectdetailsByPrjID", hInputPara);

            try
            {
                ViewBag.ProjectID = Convert.ToString(ID);
                ViewBag.SelectedModelGroupmaster = Convert.ToInt32(dtRequests.Rows[0]["Builder_Group_ID"]);
                ViewBag.SelectedModelCompanyMaster = Convert.ToInt32(dtRequests.Rows[0]["Builder_Company_ID"]);
                ViewBag.BuilderCompanyID = Convert.ToInt32(dtRequests.Rows[0]["Builder_Company_ID"]);
                ViewBag.BuilderGroupName = Convert.ToString(dtRequests.Rows[0]["BuilderGroupName"]);
                ViewBag.Builder_Company_Name = Convert.ToString(dtRequests.Rows[0]["Builder_Company_Name"]);
                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.SelectedModelstates = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Name_Of_PropertyOwner1 = Convert.ToString(dtRequests.Rows[0]["Name_Of_PropertyOwner1"]);
                ViewBag.Name_Of_PropertyOwner2 = Convert.ToString(dtRequests.Rows[0]["Name_Of_PropertyOwner2"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Nature_Of_Company = Convert.ToString(dtRequests.Rows[0]["Nature_Of_Company"]);
                ViewBag.Plot_Number = Convert.ToString(dtRequests.Rows[0]["Plot_Number"]);
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Nature_Of_ProjectTransaction = Convert.ToString(dtRequests.Rows[0]["Nature_Of_ProjectTransaction"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);
                ViewBag.Nearest_Hopital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hopital"]);
                ViewBag.TypeOfProject = Convert.ToString(dtRequests.Rows[0]["TypeOfProject"]);
                ViewBag.Approved_No_Of_Buildings = Convert.ToString(dtRequests.Rows[0]["Approved_No_Of_Buildings"]);
                ViewBag.Approved_No_Wings = Convert.ToString(dtRequests.Rows[0]["Approved_No_Wings"]);
                ViewBag.Approved_No_Of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_Of_Floors"]);
                ViewBag.Total_No_Commercial_Units = Convert.ToString(dtRequests.Rows[0]["Total_No_Commercial_Units"]);
                ViewBag.Total_No_Residential_Units = Convert.ToString(dtRequests.Rows[0]["Total_No_Residential_Units"]);
                ViewBag.Total_No_Shops = Convert.ToString(dtRequests.Rows[0]["Total_No_Shops"]);
                ViewBag.Total_No_Of_Units_Propsed = Convert.ToString(dtRequests.Rows[0]["Total_No_Of_Units_Propsed"]);
                ViewBag.Lifts = Convert.ToString(dtRequests.Rows[0]["Lifts"]);
                ViewBag.NoOfLifts = Convert.ToString(dtRequests.Rows[0]["NoOfLifts"]);
                ViewBag.Type_Of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_Of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fitting = Convert.ToString(dtRequests.Rows[0]["Electrical_Fitting"]);
                ViewBag.External_Finishing_Of_Property = Convert.ToString(dtRequests.Rows[0]["External_Finishing_Of_Property"]);
                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);
                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Proposed_Amenities = Convert.ToString(dtRequests.Rows[0]["Proposed_Amenities"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                ViewBag.Anyother_observation = Convert.ToString(dtRequests.Rows[0]["Anyother_observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.Quality_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Construction"]);
                ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.BriefDescriptionoftheproperty = Convert.ToString(dtRequests.Rows[0]["BriefDescriptionoftheproperty"]);
                ViewBag.Type_Of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_Of_Locality"]);
                ViewBag.Class_Of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_Of_Locality"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks"]);
                ViewBag.Cord_Latitude = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude"]);
                ViewBag.Cord_Longitude = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude"]);
                ViewBag.AdjoiningRoad_Width1 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width1"]);
                ViewBag.AdjoiningRoad_Width2 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width2"]);
                ViewBag.AdjoiningRoad_Width3 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width3"]);
                ViewBag.AdjoiningRoad_Width4 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width4"]);
                ViewBag.Plot_Demarcated_At_Site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_At_Site"]);
                ViewBag.Type_Of_Demarcation = Convert.ToString(dtRequests.Rows[0]["Type_Of_Demarcation"]);
                ViewBag.Are_Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Are_Boundaries_Matching"]);
                ViewBag.FourBoundaries_Remarks = Convert.ToString(dtRequests.Rows[0]["FourBoundaries_Remarks"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                ViewBag.SeismicZone = Convert.ToString(dtRequests.Rows[0]["SeismicZone"]);
                ViewBag.FloodZone = Convert.ToString(dtRequests.Rows[0]["FloodZone"]);
                ViewBag.CycloneZone = Convert.ToString(dtRequests.Rows[0]["CycloneZone"]);
                ViewBag.CRZ = Convert.ToString(dtRequests.Rows[0]["CRZ"]);
                ViewBag.Final_Remarks = Convert.ToString(dtRequests.Rows[0]["Final_Remarks"]);
                ViewBag.Project_Registered_Under_RERA = Convert.ToString(dtRequests.Rows[0]["Project_Registered_Under_RERA"]);
                ViewBag.Registration_Num = Convert.ToString(dtRequests.Rows[0]["Registration_Num"]);
                ViewBag.Num_Of_Buildings = Convert.ToString(dtRequests.Rows[0]["Num_Of_Buildings"]);
                ViewBag.Num_Of_Wings = Convert.ToString(dtRequests.Rows[0]["Num_Of_Wings"]);
                ViewBag.Num_Of_Floors = Convert.ToString(dtRequests.Rows[0]["Num_Of_Floors"]);
                ViewBag.Num_Of_Units = Convert.ToString(dtRequests.Rows[0]["Num_Of_Units"]);
                ViewBag.Expected_Completion_Date = Convert.ToString(dtRequests.Rows[0]["Expected_Completion_Date"]);


                ViewBag.Builder_Address1 = Convert.ToString(dtRequests.Rows[0]["Builder_Address1"]);
                ViewBag.Builder_Address2 = Convert.ToString(dtRequests.Rows[0]["Builder_Address2"]);
                ViewBag.Director_Name1 = Convert.ToString(dtRequests.Rows[0]["Director_Name1"]);
                ViewBag.Director_Name2 = Convert.ToString(dtRequests.Rows[0]["Director_Name2"]);
                ViewBag.Director_Name3 = Convert.ToString(dtRequests.Rows[0]["Director_Name3"]);
                ViewBag.Director_Phone1 = Convert.ToString(dtRequests.Rows[0]["Director_Phone1"]);
                ViewBag.Director_Phone2 = Convert.ToString(dtRequests.Rows[0]["Director_Phone2"]);
                ViewBag.Director_Phone3 = Convert.ToString(dtRequests.Rows[0]["Director_Phone3"]);
                ViewBag.Director_EmailId1 = Convert.ToString(dtRequests.Rows[0]["Director_EmailId1"]);
                ViewBag.Director_EmailId2 = Convert.ToString(dtRequests.Rows[0]["Director_EmailId2"]);
                ViewBag.Director_EmailId3 = Convert.ToString(dtRequests.Rows[0]["Director_EmailId3"]);
                ViewBag.Builder_Grade = Convert.ToString(dtRequests.Rows[0]["Builder_Grade"]);
                ViewBag.Builder_Address1 = Convert.ToString(dtRequests.Rows[0]["Builder_Address1"]);
                ViewBag.Builder_Address2 = Convert.ToString(dtRequests.Rows[0]["Builder_Address2"]);
                ViewBag.Director_Mobile1 = Convert.ToString(dtRequests.Rows[0]["Director_Mobile1"]);
                ViewBag.Director_Mobile2 = Convert.ToString(dtRequests.Rows[0]["Director_Mobile2"]);
                ViewBag.Director_Mobile3 = Convert.ToString(dtRequests.Rows[0]["Director_Mobile3"]);

            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            ViewModel vm = new ViewModel();

            vm.ival_LocalTransportForProject = Gettransport(ID);
            vm.ival_SocialDevelopmentForProject = GetSocialInfra(ID);
            vm.ival_BasicInfraAvailability = GetSocialBasicInfra(ID);
            vm.ival_ProjectUnitDetail = GetUnitTypes(ID);
            vm.ival_ProjectBoundaries = GetBoundariesByReqID(ID);
            vm.ival_Document_Uploaded_ListS = GetFilesList(ID);
            vm.ival_ProjectDetails = GetProjectUnitDescription(ID);
            vm.ival_ProjectApprovalDetailsnew = GetApprovalTypesnewView(ID);
            vm.ival_States = GetStates();

            vm.ival_DistrictsList = GetDistrict();
            vm.ival_CitiesList = GetCity();
            vm.ival_pincodelist = GetCity();

            vm.ival_BuilderGroupMasters = GetBuilderMaster();
            IEnumerable<ival_BuilderGroupMaster> ival_BuilderGroupMasters = GetBuilderMaster();
            if (ival_BuilderGroupMasters.ToList().Count > 0 && ival_BuilderGroupMasters != null)
            {
                var BuilderGroupID = ival_BuilderGroupMasters.Where(s => s.Builder_Group_Master_ID == Convert.ToInt32(dtRequests.Rows[0]["Builder_Group_ID"])).Select(s => s.Builder_Group_Master_ID);

                vm.selectedBuilderGroupID = Convert.ToInt32(BuilderGroupID.FirstOrDefault());

            }

            vm.ival_BuilderCompanyMasters = GetCompanyBuilderMaster();
            IEnumerable<ival_BuilderCompanyMaster> ival_BuilderCompanyMasters = GetCompanyBuilderMaster();
            if (ival_BuilderCompanyMasters.ToList().Count > 0 && ival_BuilderCompanyMasters != null)
            {
                var BuilderCompanyID = ival_BuilderCompanyMasters.Where(s => s.Builder_Company_Master_ID == Convert.ToInt32(dtRequests.Rows[0]["Builder_Company_ID"])).Select(s => s.Builder_Company_Master_ID);

                vm.selectedBuilderCompanyID = Convert.ToInt32(BuilderCompanyID.FirstOrDefault());

            }


            IEnumerable<ival_States> ival_States = GetStates();
            if (ival_States.ToList().Count > 0 && ival_States != null)
            {
                var BuilderState = ival_States.Where(s => s.State_Name == dtRequests.Rows[0]["State"].ToString()).Select(s => s.State_ID);

                vm.selectedStateID = Convert.ToInt32(BuilderState.FirstOrDefault());

            }


            IEnumerable<ival_Cities> ival_pincodelist = GetCity();
            if (ival_pincodelist.ToList().Count > 0 && ival_pincodelist != null)
            {
                var builderpostalpincode = ival_pincodelist.Where(s => s.Pin_Code == dtRequests.Rows[0]["PinCode"].ToString()).Select(s => s.Loc_Id);

                vm.selectedpinID = Convert.ToInt32(builderpostalpincode.FirstOrDefault());

            }
            IEnumerable<ival_Districts> ival_DistrictsList = GetDistrict();
            if (ival_DistrictsList.ToList().Count > 0 && ival_DistrictsList != null)
            {
                var BuilderDistrict = ival_DistrictsList.Where(s => s.District_Name == dtRequests.Rows[0]["District"].ToString()).Select(s => s.District_ID);

                vm.selectedDistID = Convert.ToInt32(BuilderDistrict.FirstOrDefault());

            }
            IEnumerable<ival_Cities> ival_CitiesList = GetCity();
            if (ival_CitiesList.ToList().Count > 0 && ival_CitiesList != null)
            {
                var BuilderCity = ival_CitiesList.Where(s => s.City_Name == dtRequests.Rows[0]["City"].ToString()).Select(s => s.Loc_Id);

                vm.selectedCityID = Convert.ToInt32(BuilderCity.FirstOrDefault());

            }

            return View(vm);
        }

        [HttpPost]
        public List<ival_BuilderCompanyMaster> GetCompanyBuilderMaster()
        {
            List<ival_BuilderCompanyMaster> ListCompanyBuilders = new List<ival_BuilderCompanyMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtCompBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuliderCompanyName");
            for (int i = 0; i < dtCompBuildermaster.Rows.Count; i++)
            {

                ListCompanyBuilders.Add(new ival_BuilderCompanyMaster
                {
                    Builder_Company_Master_ID = Convert.ToInt32(dtCompBuildermaster.Rows[i]["Builder_Company_Master_ID"]),
                    Builder_Company_Name = Convert.ToString(dtCompBuildermaster.Rows[i]["Builder_Company_Name"])

                });


            }
            return ListCompanyBuilders;
        }

        public IEnumerable<ival_Districts> GetDistrict()
        {
            List<ival_Districts> listDistrict = new List<ival_Districts>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDistrictByID");
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                listDistrict.Add(new ival_Districts
                {
                    District_ID = Convert.ToInt32(dtState.Rows[i]["District_ID"]),
                    District_Name = Convert.ToString(dtState.Rows[i]["District_Name"])

                });


            }
            return listDistrict.AsEnumerable();
        }
        public IEnumerable<ival_Cities> GetCity()
        {
            List<ival_Cities> listCity = new List<ival_Cities>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCityByID");
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                listCity.Add(new ival_Cities
                {
                    Loc_Id = Convert.ToInt32(dtState.Rows[i]["City_ID"]),
                    City_Name = Convert.ToString(dtState.Rows[i]["City_Name"]),
                    Pin_Code = Convert.ToString(dtState.Rows[i]["Pin_Code"])

                });


            }
            return listCity.AsEnumerable();
        }


        public IEnumerable<ival_LookupCategoryValues> GetPincode()
        {
            List<ival_LookupCategoryValues> listPincode = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtPincode = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPincode");
            for (int i = 0; i < dtPincode.Rows.Count; i++)
            {

                listPincode.Add(new ival_LookupCategoryValues
                {
                    Lookup_Value = Convert.ToString(dtPincode.Rows[i]["Lookup_Value"]),
                    Lookup_ID = Convert.ToInt32(dtPincode.Rows[i]["Lookup_ID"])

                });


            }
            return listPincode.AsEnumerable();
        }
    }
}