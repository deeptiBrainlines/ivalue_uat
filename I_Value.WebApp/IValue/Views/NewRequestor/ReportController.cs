﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using I_Value.WebApp.Models;
using IvalueDataAccess;
using IValuePublishProject.Models;
using Newtonsoft.Json;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Diagnostics;
using Rotativa;
using Rotativa.Options;

namespace IValuePublishProject.Controllers
{
    public class ReportController : Controller
    {
        SQLDataAccess sqlDataAccess;

       
        Hashtable hInputPara;
        Hashtable hInputPara1;
       string UserName = "";

        IvalueDataModel Datamodel = new IvalueDataModel();

        // GET: Report
        public ActionResult Index()
        {
            return View();
        }
      


        public ActionResult BankWiseView(int ID)
        {
           
             ViewModelRequest vm = new ViewModelRequest();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            string todaydate = DateTime.Now.ToString("d/MM/yyyy");
            try
            {
                if (Session["Role"].ToString() == null)
                {
                    return RedirectToAction("Index1", "Login");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index1", "Login");
            }
                Session["RequestID"] = ID;
            hInputPara.Add("@ID", ID);
            DataTable dtinfo = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
            string UnitType = dtinfo.Rows[0]["TypeOfUnit"].ToString();
            string PType = dtinfo.Rows[0]["Property_Type"].ToString();
            string Bank = dtinfo.Rows[0]["Client_Name"].ToString();
            string ProjectId = dtinfo.Rows[0]["Project_ID"].ToString();
             UserName = Session["UserName"].ToString();
           
            hInputPara1.Add("@UnitType", UnitType);
            hInputPara1.Add("@PType", PType);
            hInputPara1.Add("@BankName", Bank);
           
            DataTable dtselectreport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetReportName", hInputPara1);
            if(dtselectreport.Rows.Count > 0)
            { 
                string RptName= dtselectreport.Rows[0]["Report_Name"].ToString();
                string [] test = RptName.Split('.');
                string ReportName = test[0].ToString();
                
              
                string header = ConfigurationSettings.AppSettings["Header"];
            //string footer = ConfigurationSettings.AppSettings["Footer"];
            
               ///string footer = "http://localhost:44304/Report/Footer1?UserName="+UserName+"";
               string footer = "http://brainlines.co.in/IvalueUAT/Report/Footer1?UserName=" + UserName + "";
                // string a = "--header - right \"Page [page] of [topage]\"";
                string customSwitches = string.Format(
                                           
                                              "--print-media-type --header-html  \"{0}\" " +
                                       "--header-spacing \"0\" " +
                                       "--footer-html \"{1}\"  " +
                                       "--footer-spacing \"10\" " +
                                       "--footer-font-size \"10\" " +
                                       "--header-font-size \"10\" "
                                      





                                       , header, footer);
                //string customSwitches1 = "--page-offset\"0\" --footer-right[page] --footer-font-size\"8\"";
                //ViewBag.pagenumber = customSwitches1;
                //"--page-offset \"0\" --footer-right[page] --footer-font-size \"8\""
                var actionResult = new ActionAsPdf(ReportName, new { Id = ID })
                {




                    CustomSwitches = customSwitches,

                    // FileName = ID +"_"+ Bank + "_"+ UnitType +"_" + todaydate + ".pdf",
                    // FileName = ID +"_"+ Bank + "_"+ UnitType +"_" + todaydate + ".pdf",
                    PageSize = Size.A4,
                    PageOrientation = Orientation.Portrait,

                   // CustomSwitches = customSwitches1,

                };
                //byte[] pdfData = actionResult.BuildPdf(ControllerContext);
                //string fullPath = Server.MapPath("SavePDF/" + actionResult.FileName);
                //using (var fileStream = new FileStream(fullPath, FileMode.Create, FileAccess.Write))
                //{
                //    fileStream.Write(pdfData, 0, pdfData.Length);
                //}

                //hInputPara.Add("@Project_ID", ProjectId);
                //hInputPara.Add("@Report_Name", actionResult.FileName);
                //hInputPara.Add("@UserName", Session["UserName"].ToString());
                //DataTable rpt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_InsertDataReportHistorytbl", hInputPara);
                return actionResult;
                //var byteArray = actionResult.BuildPdf(ControllerContext);
                //string fullPath = Server.MapPath("~/SavePDF/" + actionResult.FileName);
                //var fileStream = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
                //fileStream.Write(byteArray, 0, byteArray.Length);
                //fileStream.Close();


            }
            

            return View();
            
        }
        public ActionResult BankWiseView1(int ID)
        {
            try
            {
                if (Session["Role"].ToString() == null)
                {
                    return RedirectToAction("Index1", "Login");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index1", "Login");
            }
            ViewModelRequest vm = new ViewModelRequest();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            Hashtable hInputPara10 = new Hashtable();
            string todaydate = DateTime.Now.ToString("d/MM/yyyy");


            Session["RequestID"] = ID;
            hInputPara10.Add("@ID", ID);
            
            sqlDataAccess.ExecuteStoreProcedure("usp_Requestupdate", hInputPara10);

            hInputPara.Add("@ID", ID);
            DataTable dtinfo = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
            string UnitType = dtinfo.Rows[0]["TypeOfUnit"].ToString();
            string PType = dtinfo.Rows[0]["Property_Type"].ToString();
            string Bank = dtinfo.Rows[0]["Client_Name"].ToString();
            string ProjectId = dtinfo.Rows[0]["Project_ID"].ToString();
            UserName = Session["UserName"].ToString();

            hInputPara1.Add("@UnitType", UnitType);
            hInputPara1.Add("@PType", PType);
            hInputPara1.Add("@BankName", Bank);

            DataTable dtselectreport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetReportName", hInputPara1);
            if (dtselectreport.Rows.Count > 0)
            {
                string RptName = dtselectreport.Rows[0]["Report_Name"].ToString();
                string[] test = RptName.Split('.');
                string ReportName = test[0].ToString();


                string header = ConfigurationSettings.AppSettings["Header"];
                //string footer = ConfigurationSettings.AppSettings["Footer"];

                ///string footer = "http://localhost:44304/Report/Footer1?UserName="+UserName+"";
                string footer = "http://brainlines.co.in/IvalueUAT/Report/Footer1?UserName=" + UserName + "";
                // string a = "--header - right \"Page [page] of [topage]\"";
                string customSwitches = string.Format(

                                              "--print-media-type --header-html  \"{0}\" " +
                                       "--header-spacing \"0\" " +
                                       "--footer-html \"{1}\"  " +
                                       "--footer-spacing \"10\" " +
                                       "--footer-font-size \"10\" " +
                                       "--header-font-size \"10\" "






                                       , header, footer);
                //string customSwitches1 = "--page-offset\"0\" --footer-right[page] --footer-font-size\"8\"";
                //ViewBag.pagenumber = customSwitches1;
                //"--page-offset \"0\" --footer-right[page] --footer-font-size \"8\""
                var actionResult = new ActionAsPdf(ReportName, new { Id = ID })
                {




                    CustomSwitches = customSwitches,

                    // FileName = ID +"_"+ Bank + "_"+ UnitType +"_" + todaydate + ".pdf",
                    // FileName = ID +"_"+ Bank + "_"+ UnitType +"_" + todaydate + ".pdf",
                    PageSize = Size.A4,
                    PageOrientation = Orientation.Portrait,

                    // CustomSwitches = customSwitches1,

                };
                //byte[] pdfData = actionResult.BuildPdf(ControllerContext);
                //string fullPath = Server.MapPath("SavePDF/" + actionResult.FileName);
                //using (var fileStream = new FileStream(fullPath, FileMode.Create, FileAccess.Write))
                //{
                //    fileStream.Write(pdfData, 0, pdfData.Length);
                //}

                //hInputPara.Add("@Project_ID", ProjectId);
                //hInputPara.Add("@Report_Name", actionResult.FileName);
                //hInputPara.Add("@UserName", Session["UserName"].ToString());
                //DataTable rpt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_InsertDataReportHistorytbl", hInputPara);
                return actionResult;
                //var byteArray = actionResult.BuildPdf(ControllerContext);
                //string fullPath = Server.MapPath("~/SavePDF/" + actionResult.FileName);
                //var fileStream = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
                //fileStream.Write(byteArray, 0, byteArray.Length);
                //fileStream.Close();


            }


            return View();

        }
        //public ActionResult Footer1()
        //{
        //    string UserName = Session["UserName"].ToString();
        //    hInputPara1.Add("@userName", UserName);
        //    DataTable dtrequestCity = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetEmployeeCurrentCity", hInputPara1);
        //    hInputPara1.Clear();
        //    string cityName = dtrequestCity.Rows[0]["Current_City"].ToString();
        //    ViewBag.city = cityName;
        //    Session["currentCity"] = cityName;
        //    return View();
        //}

        //HDFC Bank Report for Flat
        public ActionResult HDFCBankMultiUnit(int ID)
        {
            //int ID = 111;
            Session["RequestID"]=ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);
                //int imageTypeId = 0;
                //for (int i=0;i< dtrequestIdGoogleMapImage.Rows.Count;i++)
                //{
                //    imageTypeId = Convert.ToInt32(dtrequestIdGoogleMapImage.Rows[i]["Image_Type_Id"]);
                //}
                
                
                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.TotalFlatwords = Convert.ToString(dtRequests.Rows[0]["TotalFlatwords"]);
                ViewBag.PltNo = Convert.ToString(dtRequests.Rows[0]["Plot_No"]);
                ViewBag.WardNo = Convert.ToString(dtRequests.Rows[0]["Ward_No"]);
                ViewBag.MunicipalNo = Convert.ToString(dtRequests.Rows[0]["Municipal_No"]);
                ViewBag.external = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.ttlfloor     = Convert.ToString(dtRequests.Rows[0]["Unit_on_floor"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    try
                    {
                        DateTime dti = DateTime.ParseExact(ViewBag.Inspectiondate.ToString(), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);

                        string Inspectiondate = dti.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);

                        ViewBag.Date_of_Inspection = Inspectiondate;
                    }
                    catch (Exception e)
                    {
                        string datestring1 = Convert.ToDateTime(ViewBag.Inspectiondate).ToString("dd/MM/yyyy");
                        ViewBag.Date_of_Inspection = datestring1;

                    }

                //    var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                //var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                //ViewBag.Date_of_Inspection = dateOnlyInspection;

                }
                ViewBag.Valuationdate = Convert.ToString(dtRequests.Rows[0]["Date_of_Valuation"]);
                if(ViewBag.Valuationdate!= "")
                { 
              //  var dateTimeNowValue = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Valuation"]); // Return 00/00/0000 00:00:00
              //  var dateOnlyValue = dateTimeNowValue.ToShortDateString();
                ViewBag.Date_of_Valuation = ViewBag.Valuationdate;
                }
                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                StringBuilder sb = new StringBuilder();
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                if(ViewBag.UnitNumber!="")
                {
                    sb.Append(ViewBag.UnitNumber);
                }


                ViewBag.PNo1 = Convert.ToString(dtRequests.Rows[0]["Plot_No1"]);
                if (ViewBag.PNo1 != "")
                {
                    sb.Append(" , Floor No. " + ViewBag.PNo1);
                }
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                if (ViewBag.Building_Name_RI != "")
                {
                    sb.Append(" , " + ViewBag.Building_Name_RI);
                }

              

                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                if (ViewBag.Project_Name != "")
                {
                    sb.Append(" , " + ViewBag.Project_Name);
                }

                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                if (ViewBag.Wing_Name_RI != "")
                {
                    sb.Append(" , " + ViewBag.Wing_Name_RI);
                }

                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                if (ViewBag.Street != "")
                {
                    sb.Append(" , " + ViewBag.Street);
                }


                ViewBag.PNo = Convert.ToString(dtRequests.Rows[0]["Plot_No"]);
                if (ViewBag.PNo != "")
                {
                    sb.Append(" , " + ViewBag.PNo);
                }
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                if (ViewBag.Survey_Number != "")
                {
                    sb.Append(" , " + ViewBag.Survey_Number);
                }

                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                if (ViewBag.Sub_Locality != "Please Select")
                {
                    sb.Append(" , " + ViewBag.Sub_Locality);
                }

                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                if (ViewBag.Locality != "Please Select")
                {
                    sb.Append(" , " + ViewBag.Locality);
                }
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                if (ViewBag.Village_Name != "")
                {
                    sb.Append(" , " + ViewBag.Village_Name);
                }

                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                if (ViewBag.City != "Please Select")
                {
                    sb.Append(" , " + ViewBag.City);
                }

                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                if (ViewBag.District != "Please Select")
                {
                    sb.Append(" , " + ViewBag.District);
                }


                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                if (ViewBag.State != "Please Select")
                {
                    sb.Append(" , " + ViewBag.State);
                }
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                if (ViewBag.PinCode != "")
                {
                    sb.Append(" , " + ViewBag.PinCode);
                }

                
              
              
             
                ViewBag.TotalAddress = sb.ToString();
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Balanced_Leased_Period = Convert.ToString(dtRequests.Rows[0]["Balanced_Leased_Period"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);

                ViewBag.LiftDetails = Convert.ToString(dtRequests.Rows[0]["Lift_Details"]);
                if (ViewBag.Occupancy_Details == "Self Occupied" || ViewBag.Occupancy_Details == "Vacant")
                {
                    ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                    ViewBag.Occupancy_Details1 = "NA";
                    ViewBag.Occupancy_Details2 = "NA";
                }
                if (ViewBag.Occupancy_Details == "Leased Hold")
                {
                    ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                    ViewBag.Occupancy_Details1 = "Yes";
                    ViewBag.Occupancy_Details2 = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);

                }
                ViewBag.txtwingsno = Convert.ToString(dtRequests.Rows[0]["noofwings"]);
                ViewBag.NoFloor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Unit_on_Floor"]);



                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);
                
                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);

                ViewBag.riskof = Convert.ToString(dtRequests.Rows[0]["riskof"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.DeviationPercentage = Convert.ToString(dtRequests.Rows[0]["DeviationPercentage"]);
                
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                ViewBag.distressValue = Convert.ToString(dtRequests.Rows[0]["distressValue"]);
                ViewBag.RealizableValue = Convert.ToString(dtRequests.Rows[0]["Realizable_Value"]);
                ViewBag.AssignedToEmpID = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Expected_Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Expected_Monthly_Rental"]);
                

                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }
                else
                {
                    ViewBag.IGRImage = "NA.jpg";
                }
                //int ImageTypeId = Convert.ToInt32(dtrequestIdGoogleMapImage.Rows[0]["Image_Type_Id"]);
               
                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {
                    //if(imageTypeId == 6)
                    //{
                        ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                    //}
                   
                }
                

                if (dtrequestIdApprovedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);
                    try
                    {

                        var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                        var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                        ViewBag.Date_Of_Approval = dateOnlyString1;
                    }
                    catch (Exception e)
                    {
                        ViewBag.Date_Of_Approval = "";

                    }
                }

                if (dtrequestIdConstructionDoc.Rows.Count > 0)
                {
                    DateTime dateTimeNow2;
                    string dateOnlyString2 ="" ;
                    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);
                    try
                    {
                         dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                         dateOnlyString2 = dateTimeNow2.ToShortDateString();
                        ViewBag.Date_Of_Approval_Constr = dateOnlyString2;
                    }
                    catch (Exception e)
                    {
                        ViewBag.Date_Of_Approval_Constr = null;

                    }
                    //Return 00/00/0000
                 
                }


                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();
                ViewBag.Count = 8;
                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + " , ";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }

               
                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#"); 
                ViewBag.ApprovedcarpetArea= getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                ViewBag.ParkingCharges = getUnitPrice(dtpricedetails, "#Parking_Charges#");
                ViewBag.DistressValuePer= getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                ViewBag.RealizableValuePer = getUnitPrice(dtpricedetails, "#Realizable_Value_%#");
                //ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                ViewBag.Area_as_per_Agreement = getUnitPrice(dtpricedetails, "#Area_as_per_Agreement#");
                ViewBag.Measured_Saleable_Area = getUnitPrice(dtpricedetails, "#Measured_Saleable_Area#");
                ViewBag.Salable_Area_as_per_Agreement = getUnitPrice(dtpricedetails, "#Salable_Area_as_per_Agreement#");
                ViewBag.Approved_Saleable_Area = getUnitPrice(dtpricedetails, "#Approved_Saleable_Area#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");
                ViewBag.RateRangeinLocality = getUnitPrice(dtpricedetails, "#Rate_Range_for_similar_Properties_in_the_locality#");
                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");












            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }

            return View(vm);
        }

        //HDFC Bank Report for Plot
        public ActionResult HDFCBankSingleUnitPlot(int ID)
        {
            //int ID = 111;
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);






                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                ViewBag.TotalPlotwords = Convert.ToString(dtRequests.Rows[0]["TotalPlotwords"]);
               
                ViewBag.PltNo = Convert.ToString(dtRequests.Rows[0]["Plot_No"]);
                ViewBag.WardNo = Convert.ToString(dtRequests.Rows[0]["Ward_No"]);
                ViewBag.MunicipalNo = Convert.ToString(dtRequests.Rows[0]["Municipal_No"]);
                ViewBag.external = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                if (ViewBag.Inspectiondate != "")
                {
                    var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                    var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                    ViewBag.Date_of_Inspection = dateOnlyInspection;
                }
                ViewBag.Valuationdate = Convert.ToString(dtRequests.Rows[0]["Date_of_Valuation"]);
                if (ViewBag.Valuationdate != "")
                {
                   // var dateTimeNowValue = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Valuation"]); // Return 00/00/0000 00:00:00
                   // var dateOnlyValue = dateTimeNowValue.ToShortDateString();
                    ViewBag.Date_of_Valuation = ViewBag.Valuationdate;
                }
                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Balanced_Leased_Period = Convert.ToString(dtRequests.Rows[0]["Balanced_Leased_Period"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);


                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                if (ViewBag.Occupancy_Details == "Self Occupied" || ViewBag.Occupancy_Details == "Vacant")
                {
                    ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                    ViewBag.Occupancy_Details1 = "NA";
                    ViewBag.Occupancy_Details2 = "NA";
                }
                if (ViewBag.Occupancy_Details == "Leased Hold")
                {
                    ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                    ViewBag.Occupancy_Details1 = "Yes";
                    ViewBag.Occupancy_Details2 = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);

                }
                ViewBag.txtwingsno = Convert.ToString(dtRequests.Rows[0]["noofwings"]);
                ViewBag.NoFloor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Unit_on_Floor"]);


                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);
                StringBuilder sb = new StringBuilder();
                ViewBag.PlotBunglowNumber = Convert.ToString(dtRequests.Rows[0]["Plot_Number"]);
                if (ViewBag.PlotBunglowNumber != "")
                {
                    sb.Append("," + ViewBag.PlotBunglowNumber);
                }

                ViewBag.BungalowNo = Convert.ToString(dtRequests.Rows[0]["Bunglow_Number"]);

                sb.Append(ViewBag.BungalowNo);

                ViewBag.PlotNumber = Convert.ToString(dtRequests.Rows[0]["Plot_Number"]);
                if(ViewBag.PlotNumber!="")
                {
                    sb.Append(ViewBag.PlotNumber);
                }
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                if(ViewBag.Survey_Number!="")
                {
                    sb.Append(" , "+ViewBag.Survey_Number);
                }
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                if (ViewBag.Village_Name != "")
                {
                    sb.Append(" , "+ViewBag.Village_Name);
                }
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                if (ViewBag.Street != "")
                {
                    sb.Append(" , " + ViewBag.Street);
                }
                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                if (ViewBag.Locality != "Please Select")
                {
                    sb.Append(" , " + ViewBag.Locality);
                }
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                if (ViewBag.Sub_Locality != "Please Select")
                {
                    sb.Append(" , " + ViewBag.Sub_Locality);
                }
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                if (ViewBag.City != "Please Select")
                {
                    sb.Append(" , " + ViewBag.City);
                }
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                if (ViewBag.District != "Please Select")
                {
                    sb.Append(" , " + ViewBag.District);
                }
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                if (ViewBag.State != "Please Select")
                {
                    sb.Append(" , " + ViewBag.State);
                }
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                if (ViewBag.PinCode != "")
                {
                    sb.Append(" , " + ViewBag.PinCode);
                }
                ViewBag.TotalAddress = sb.ToString();
                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);
                
                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResultplot"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.DeviationPercentage = Convert.ToString(dtRequests.Rows[0]["DeviationPercentage"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.ValueOfPlot = Convert.ToString(dtRequests.Rows[0]["Value_of_Plot"]);
                ViewBag.TotalValuation = Convert.ToString(dtRequests.Rows[0]["Total_Land_Plot_Value"]);
                ViewBag.Distress_Value_of_Plot = Convert.ToString(dtRequests.Rows[0]["Distress_Value_of_Plot"]);
                ViewBag.RealizableValue = Convert.ToString(dtRequests.Rows[0]["Realizable_Value"]);
                ViewBag.AssignedToEmpID = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Expected_Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Expected_Monthly_Rental"]);
                ViewBag.DistressValuePer = Convert.ToString(dtRequests.Rows[0]["Distressvalpercentplot"]);
                

                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }
                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }

                if (dtrequestIdApprovedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);


                    var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                    var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval = dateOnlyString1;
                }

                if (dtrequestIdConstructionDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);

                    var dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                    var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_Constr = dateOnlyString2;
                }



                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();

                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }

                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + " , ";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                //ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");

                ViewBag.Adopted_Plot_area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Plot_area_for_Valuation#");
                ViewBag.RateRangeinLocality = getUnitPrice(dtpricedetails, "#Land_/_Plot_Rate_Range_in_the_Locality#");
                ViewBag.AdoptedRate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");
                ViewBag.DistressValuePerOfPlot = getUnitPrice(dtpricedetails, "#Distress_Value_for_Plot%#");
                ViewBag.RealizableValuePer = getUnitPrice(dtpricedetails, "#Realizable_Value_%#");

                //ViewBag.DistressValueOfPlot = getUnitPrice(dtpricedetails, "#Distress_Value_of_Plot#");
                ViewBag.Approved_Plot_Area = getUnitPrice(dtpricedetails, "#Approved_Plot_Area#");
                ViewBag.Measured_Plot_Area = getUnitPrice(dtpricedetails, "#Measured_Plot_Area#");
                ViewBag.ApprovedOtherPlotArea = getUnitPrice(dtpricedetails, "#Approved_Other_Land/_Plot_Area_#");
                ViewBag.MeasuredOtherPlotArea = getUnitPrice(dtpricedetails, "#Measured_Other_Plot_Area#");
                ViewBag.AdoptedOtherPlotValuation = getUnitPrice(dtpricedetails, "#Adopted_Other_Plot_Area_for_Valuation#");
                ViewBag.AdoptedOtherPlotRate = getUnitPrice(dtpricedetails, "#Adopted_Other_Plot_/_Land_Rate#");


            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }

            return View(vm);
        }

        //HDFC Bank Report For Bungalow
        public ActionResult HDFCBankSingleUnit(int ID)
        {
            //int ID = 111;
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdFloorTypeTotal = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorTypeTotalByReqID", hInputPara);
                DataTable dtrequestIdAllFloorTypeSumTotal = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSumTotalAllFloorTypeByRequestid", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);
                //hInputPara.Add("@ID", ID);
                DataTable dtpricedetailsNew = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorWiseBreakupGroundRCCByRequestid", hInputPara);

                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);
                


                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
               
                ViewBag.PltNo = Convert.ToString(dtRequests.Rows[0]["Plot_No"]);
                ViewBag.WardNo = Convert.ToString(dtRequests.Rows[0]["Ward_No"]);
                ViewBag.MunicipalNo = Convert.ToString(dtRequests.Rows[0]["Municipal_No"]);
                ViewBag.external = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                if (ViewBag.Inspectiondate != "")
                {
                    try
                    {
                        DateTime dti = DateTime.ParseExact(ViewBag.Inspectiondate.ToString(), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);

                        string Inspectiondate = dti.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                        
                        ViewBag.Date_of_Inspection = Inspectiondate;
                    }
                    catch(Exception e)
                    {
                        string datestring1 = Convert.ToDateTime(ViewBag.Inspectiondate).ToString("dd/MM/yyyy");
                        ViewBag.Date_of_Inspection = datestring1;
                        
                    }
                    
                }
                ViewBag.Valuationdate = Convert.ToString(dtRequests.Rows[0]["Date_of_Valuation"]);
                if (ViewBag.Valuationdate != "")
                {
                    //     var dateTimeNowValue = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Valuation"]); // Return 00/00/0000 00:00:00
                    //      var dateOnlyValue = dateTimeNowValue.ToShortDateString();
                    ViewBag.Date_of_Valuation = ViewBag.Valuationdate;
                }
                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);

                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                //ViewBag.Balanced_Leased_Period ="11years";
                ViewBag.Balanced_Leased_Period = Convert.ToString(dtRequests.Rows[0]["Balanced_Leased_Period"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.RPer = Convert.ToString(dtRequests.Rows[0]["RealPercPlot"]);
                ViewBag.RPlot = Convert.ToString(dtRequests.Rows[0]["RealizablePlot"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                ViewBag.Risk_Demarcation = Convert.ToString(dtRequests.Rows[0]["Risk_Demarcation"]);
                ViewBag.TotalCostCons = Convert.ToString(dtRequests.Rows[0]["TotalCostCons"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                if (ViewBag.Occupancy_Details== "Self Occupied" || ViewBag.Occupancy_Details == "Vacant")
                {
                    ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                    ViewBag.Occupancy_Details1 = "NA";
                    ViewBag.Occupancy_Details2 = "NA";
                }
                if (ViewBag.Occupancy_Details == "Leased Hold")
                {
                    ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                    ViewBag.Occupancy_Details1 = "Yes";
                    ViewBag.Occupancy_Details2 = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);

                }
                ViewBag.txtwingsno = Convert.ToString(dtRequests.Rows[0]["noofwings"]);
                ViewBag.NoFloor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Unit_on_Floor"]);
                StringBuilder sb = new StringBuilder();
                ViewBag.PlotBunglowNumber = Convert.ToString(dtRequests.Rows[0]["Plot_Number"]);
                if (ViewBag.PlotBunglowNumber != "")
                {
                    sb.Append(" , " + ViewBag.PlotBunglowNumber);
                }

                ViewBag.BungalowNo = Convert.ToString(dtRequests.Rows[0]["Bunglow_Number"]);
               
                    sb.Append(ViewBag.BungalowNo);


                ViewBag.Plot_No = Convert.ToString(dtRequests.Rows[0]["Plot_No"]);
                if (ViewBag.Plot_No != "")
                {
                    sb.Append(" , " + ViewBag.Plot_No);
                }

                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                if (ViewBag.Survey_Number != "")
                {
                    sb.Append(","+ViewBag.Survey_Number);
                }

                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                if (ViewBag.Sub_Locality != "Please Select")
                {
                    sb.Append(" , " + ViewBag.Sub_Locality);
                }

                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                if (ViewBag.Locality != "Please Select")
                {
                    sb.Append(" , " + ViewBag.Locality);
                }
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                if (ViewBag.Village_Name != "")
                {
                    sb.Append(" , " + ViewBag.Village_Name);
                }

                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                if (ViewBag.City != "Please Select")
                {
                    sb.Append(" , " + ViewBag.City);
                }

                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                if (ViewBag.District != "Please Select")
                {
                    sb.Append(" , " + ViewBag.District);
                }


                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                if (ViewBag.State != "Please Select")
                {
                    sb.Append(" , " + ViewBag.State);
                }
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                if (ViewBag.PinCode != "")
                {
                    sb.Append(" , " + ViewBag.PinCode);
                }


                ViewBag.TotalAddress = sb.ToString();
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);
                
                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Fixtures"]);
                ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
               
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["TotalvaluepropertyBunglow"]);

                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
               
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.DeviationPercentage = Convert.ToString(dtRequests.Rows[0]["DeviationPercentage"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
           
                ViewBag.ValueOfPlot = Convert.ToString(dtRequests.Rows[0]["Valueoftheplotbungalow"]);
                ViewBag.Total_Land_Plot_Value = Convert.ToString(dtRequests.Rows[0]["Total_Land_Plot_Value"]);
                ViewBag.AssignedToEmpID = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.DistressValuePerct = Convert.ToString(dtRequests.Rows[0]["DistressValuePerct"]);
                ViewBag.RealizableValue = Convert.ToString(dtRequests.Rows[0]["Realizable_Value"]);
                ViewBag.DistressValueProperty = Convert.ToString(dtRequests.Rows[0]["DistressValueofPropertyBunglow"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Expected_Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Expected_Monthly_Rental"]);
                ViewBag.StageOfConstructionBunglow = Convert.ToString(dtRequests.Rows[0]["StageOfConstructionBunglow"]);
                ViewBag.GovRate = Convert.ToString(dtRequests.Rows[0]["Government_Rate"]);

                if (ViewBag.StageOfConstructionBunglow == "Ready")
                {
                    ViewBag.ToatalInWords = Convert.ToString(dtRequests.Rows[0]["TotalreadyBunglowWords"]);
                }
                else
                {
                    ViewBag.ToatalInWords = Convert.ToString(dtRequests.Rows[0]["TotalUCBunglowWords"]);
                }
                if (ViewBag.StageOfConstructionBunglow == "Ready")
                {
                    ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResultR"]);
                }
                else
                {
                    ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResultUC"]);
                }

                if (dtrequestIdFloorTypeTotal.Rows.Count > 0)
                {
                    ViewBag.TotalOfMeasured_Built_up_Area = Convert.ToString(dtrequestIdFloorTypeTotal.Rows[0]["Measured_Built_up_Area"]);
                    ViewBag.TotalOfApproved_Built_up_Area = Convert.ToString(dtrequestIdFloorTypeTotal.Rows[0]["Approved_Built_up_Area"]);
                    ViewBag.TotalOfAdopted_Cost_Construction = Convert.ToString(dtrequestIdFloorTypeTotal.Rows[0]["Adopted_Cost_Construction"]);
                }
                if(dtpricedetailsNew.Rows.Count > 0)
                {
                    ViewBag.TotalOfGround = Convert.ToString(dtpricedetailsNew.Rows[0]["Total"]);
                    ViewBag.TotalOfRCC = Convert.ToString(dtpricedetailsNew.Rows[1]["Total"]);
                    ViewBag.Approved_Built_up_AreaOfGround = Convert.ToString(dtpricedetailsNew.Rows[0]["Approved_Built_up_Area"]);
                    ViewBag.Approved_Built_up_AreaOfRCC= Convert.ToString(dtpricedetailsNew.Rows[1]["Approved_Built_up_Area"]);
                    ViewBag.Adopted_Cost_ConstructionForGround = Convert.ToString(dtpricedetailsNew.Rows[0]["Adopted_Cost_Construction"]);
                    ViewBag.Adopted_Cost_ConstructionForRCC = Convert.ToString(dtpricedetailsNew.Rows[1]["Adopted_Cost_Construction"]);

                }


                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }
                else
                {
                    ViewBag.IGRImage = "NA.jpg";
                }


                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }
                if (dtrequestIdApprovedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);


                    //var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval = dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"];
                }

                if (dtrequestIdConstructionDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);

                    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_Constr = dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"];
                }
                if (dtrequestIdAllFloorTypeSumTotal.Rows.Count > 0)
                {
                    ViewBag.Total_ValueOfConstruction = Convert.ToString(dtrequestIdAllFloorTypeSumTotal.Rows[0]["Sumtotal"]);                   
                }
                

                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();

                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }

                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + " , ";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                
                
                //ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                // ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                //ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                ViewBag.Measured_Plot_AreaTest = getUnitPrice(dtpricedetails, "#Measured_area_for_Plot#");
                ViewBag.LandArea = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");
                ViewBag.RecommandedRate = getUnitPrice(dtpricedetails, "#Land_/_Plot_Rate_Range_in_the_Locality#");
                ViewBag.AdoptedRate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");
                ViewBag.RealizableValuePer = getUnitPrice(dtpricedetails, "#Realizable_Value_%#");

                // ViewBag.DistressValuePerOfPlot = getUnitPrice(dtpricedetails, "#Distress_Value_for_Plot%#");
                //ViewBag.DistressValueOfPlot = getUnitPrice(dtpricedetails, "#Distress_Value_of_Plot#");
                //ViewBag.ActualBuaPremises = getUnitPrice(dtpricedetails, "#Approved_Other_Land/_Plot_Area_#");
                //ViewBag.BuaAsPerApproval = getUnitPrice(dtpricedetails, "#Measured_Plot_Area#");
                //ViewBag.ConstructionCost = getUnitPrice(dtpricedetails, "#Cost_of_Construction#");
                ViewBag.TotalValueOfConstruction = getUnitPrice(dtpricedetails, "#Gross_Estimated_cost_of_Construction_#");

                ViewBag.Approved_Plot_Area = getUnitPrice(dtpricedetails, "#Approved_Plot_Area#");
              
                //ViewBag.Measured_Plot_Area = getUnitPrice(dtpricedetails, "#Measured_Plot_Area#");
                ViewBag.Measured_Plot_Area = getUnitPrice(dtpricedetails, "#Measured_area_for_Plot#");
                ViewBag.ApprovedOtherPlotArea = getUnitPrice(dtpricedetails, "#Approved_Other_Land/_Plot_Area_#");
                ViewBag.MeasuredOtherPlotArea = getUnitPrice(dtpricedetails, "#Measured_Other_Plot_Area#");
                ViewBag.AdoptedOtherPlotValuation = getUnitPrice(dtpricedetails, "#Adopted_Other_Plot_Area_for_Valuation#");
                ViewBag.AdoptedOtherPlotRate = getUnitPrice(dtpricedetails, "#Adopted_Other_Plot_/_Land_Rate#");
                ViewBag.Approve_Builtup_Area = getUnitPrice(dtpricedetails, "#Approve_Built-up_Area#");
                ViewBag.Measured_Builtup_Area = getUnitPrice(dtpricedetails, "#Measured_Built-up_Area#");
                if (ViewBag.Approve_Builtup_Area!="" && ViewBag.Measured_Builtup_Area!="")
                {
                    double DeviationinConstruction1 = ViewBag.Measured_Builtup_Area / ViewBag.Approve_Builtup_Area;
                    ViewBag.DeviationinConstruction = DeviationinConstruction1;
                }
                vm.ival_LookupCategoryFloorTypes = GetFloorType();
                vm.ival_ProjectFloorWiseBreakUps = GetFloorwisebreakbyreqID();
            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }

            return View(vm);
        }

        //Yes Bank Report For Flat
        public ActionResult YESBankMultiUnit(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
               
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);





                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                    var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                    ViewBag.Date_of_Inspection = dateOnlyInspection;
                }
                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);

                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);
                
                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);
                

         ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);
               
                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                
                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.BranchName = Convert.ToString(dtRequests.Rows[0]["Branch_Name"]);
                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                ViewBag.AdjoiningRoad_Width1 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width1"]);
                ViewBag.AdjoiningRoad_Width2 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width2"]);
                ViewBag.AdjoiningRoad_Width3 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width3"]);
                ViewBag.AdjoiningRoad_Width4 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width4"]);
                ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                ViewBag.distressValue = Convert.ToString(dtRequests.Rows[0]["distressValue"]);
                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);



                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();

                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }

                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + ",";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");

                ViewBag.Area_as_per_Agreement = getUnitPrice(dtpricedetails, "#Area_as_per_Agreement#");
                ViewBag.Approved_Carpet_Area = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");
                ViewBag.Any_Other_Charges = getUnitPrice(dtpricedetails, "#Any_Other_Charges#");
                ViewBag.Cost_of_Construction = getUnitPrice(dtpricedetails, "#Cost_of_Construction#");
                ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                ViewBag.Stage_Of_construction_Per = getUnitPrice(dtpricedetails, "#Stage_Of_construction_%#");
                ViewBag.Disbursement_Recommand = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");

                ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");

            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //Yes Bank Report For Plot
        public ActionResult YESBankSingleUnitPlot(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
               
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);

                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                    var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                    ViewBag.Date_of_Inspection = dateOnlyInspection;
                }
                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);

                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);

                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);

                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);

                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.BranchName = Convert.ToString(dtRequests.Rows[0]["Branch_Name"]);
                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                ViewBag.AdjoiningRoad_Width1 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width1"]);
                ViewBag.AdjoiningRoad_Width2 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width2"]);
                ViewBag.AdjoiningRoad_Width3 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width3"]);
                ViewBag.AdjoiningRoad_Width4 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width4"]);
                ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                ViewBag.distressValue = Convert.ToString(dtRequests.Rows[0]["distressValue"]);
                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Value_of_Plot = Convert.ToString(dtRequests.Rows[0]["Value_of_Plot"]);
                ViewBag.Total_Land_Plot_Value = Convert.ToString(dtRequests.Rows[0]["Total_Land_Plot_Value"]);
                ViewBag.Distress_Value_of_Plot = Convert.ToString(dtRequests.Rows[0]["Distress_Value_of_Plot"]);
                ViewBag.Plot_Number = Convert.ToString(dtRequests.Rows[0]["Plot_Number"]);
                

                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();

                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }
                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + ",";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");

                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");

                ViewBag.Area_as_per_Agreement = getUnitPrice(dtpricedetails, "#Area_as_per_Agreement#");
                ViewBag.Approved_Carpet_Area = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");
                ViewBag.Any_Other_Charges = getUnitPrice(dtpricedetails, "#Any_Other_Charges#");
                ViewBag.Cost_of_Construction = getUnitPrice(dtpricedetails, "#Cost_of_Construction#");
                ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                ViewBag.Stage_Of_construction_Per = getUnitPrice(dtpricedetails, "#Stage_Of_construction_%#");
                ViewBag.Disbursement_Recommand = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Plot_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Plot_area_for_Valuation#");
                ViewBag.Adopted_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");
                ViewBag.Approved_Plot_Area = getUnitPrice(dtpricedetails, "#Approved_Plot_Area#");


            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //Yes Bank Report For Bungalow
        public ActionResult YESBankSingleUnit(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);

                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);

                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                    var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                    ViewBag.Date_of_Inspection = dateOnlyInspection;
                }
                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);

                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);

                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);

                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);

                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.BranchName = Convert.ToString(dtRequests.Rows[0]["Branch_Name"]);
                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstructionBunglow = Convert.ToString(dtRequests.Rows[0]["StageOfConstructionBunglow"]);
                ViewBag.StageOfConstructionPerct = Convert.ToString(dtRequests.Rows[0]["StageOfConstructionPerct"]);
                

                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                ViewBag.AdjoiningRoad_Width1 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width1"]);
                ViewBag.AdjoiningRoad_Width2 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width2"]);
                ViewBag.AdjoiningRoad_Width3 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width3"]);
                ViewBag.AdjoiningRoad_Width4 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width4"]);
                ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                ViewBag.distressValue = Convert.ToString(dtRequests.Rows[0]["distressValue"]);
                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Valueoftheplotbungalow = Convert.ToString(dtRequests.Rows[0]["Valueoftheplotbungalow"]);
                
                ViewBag.TotalvaluepropertyBunglow = Convert.ToString(dtRequests.Rows[0]["TotalvaluepropertyBunglow"]);
                ViewBag.Government_Rate = Convert.ToString(dtRequests.Rows[0]["Government_Rate"]);
                ViewBag.Work_Completed_Perc = Convert.ToString(dtRequests.Rows[0]["Work_Completed_Perc"]);
                ViewBag.DisbursementRecommendPerct = Convert.ToString(dtRequests.Rows[0]["DisbursementRecommendPerct"]);
                


                ViewBag.DistressValueofPropertyBunglow = Convert.ToString(dtRequests.Rows[0]["DistressValueofPropertyBunglow"]);

                ViewBag.Bunglow_Number = Convert.ToString(dtRequests.Rows[0]["Bunglow_Number"]);
                

                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();

                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }
                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + ",";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");

                
                //ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");

                ViewBag.Area_as_per_Agreement = getUnitPrice(dtpricedetails, "#Area_as_per_Agreement#");
                ViewBag.Approved_Carpet_Area = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");
                ViewBag.Any_Other_Charges = getUnitPrice(dtpricedetails, "#Any_Other_Charges#");
                ViewBag.Cost_of_Construction = getUnitPrice(dtpricedetails, "#Cost_of_Construction#");
                //ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                ViewBag.Stage_Of_construction_Per = getUnitPrice(dtpricedetails, "#Stage_Of_construction_%#");
                //ViewBag.Disbursement_Recommand = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");
                ViewBag.Adopted_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");
                ViewBag.Approved_Plot_Area = getUnitPrice(dtpricedetails, "#Approved_Plot_Area#");

                ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");

            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //L&T Bank Report For Flat
        public ActionResult LTMultiUnit(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdSaleDeedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSaleDeedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);
             






        TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.TotalFlatwords = Convert.ToString(dtRequests.Rows[0]["TotalFlatwords"]);

                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                    var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                    ViewBag.Date_of_Inspection = dateOnlyInspection;
                }
                ViewBag.Valuationdate = Convert.ToString(dtRequests.Rows[0]["Date_of_Valuation"]);
                if (ViewBag.Valuationdate != "")
                {
                   // var dateTimeNowValue = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Valuation"]); // Return 00/00/0000 00:00:00
                   // var dateOnlyValue = dateTimeNowValue.ToShortDateString();
                    ViewBag.Date_of_Valuation = ViewBag.Valuationdate;
                }

                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);
                


                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);
               
                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);
                
                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);
                

                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);
                

                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);
                
                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.BranchName = Convert.ToString(dtRequests.Rows[0]["Branch_Name"]);
                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                ViewBag.Department = Convert.ToString(dtRequests.Rows[0]["Department"]);
                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
  
                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }

                if (dtrequestIdApprovedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);
                   
                    //var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                }

                if (dtrequestIdConstructionDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_Constr = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);
                    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                }
                if (dtrequestIdSaleDeedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Authority"]);
                    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                }
              
                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                //vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();


                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + ",";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");
                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");

                ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");









            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //L&T Bank Report For Plot
        public ActionResult LTSingleUnitPlot(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdSaleDeedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSaleDeedDocumentByReqIDForPDF", hInputPara);






                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.TotalPlotwords = Convert.ToString(dtRequests.Rows[0]["TotalPlotwords"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                    var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                    ViewBag.Date_of_Inspection = dateOnlyInspection;
                }
                ViewBag.Valuationdate = Convert.ToString(dtRequests.Rows[0]["Date_of_Valuation"]);
                if (ViewBag.Valuationdate != "")
                {
                   // var dateTimeNowValue = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Valuation"]); // Return 00/00/0000 00:00:00
                    //var dateOnlyValue = dateTimeNowValue.ToShortDateString();
                    ViewBag.Date_of_Valuation = ViewBag.Valuationdate;
                }

                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);



                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);


                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);
                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);
                
                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.BranchName = Convert.ToString(dtRequests.Rows[0]["Branch_Name"]);
                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                ViewBag.Department = Convert.ToString(dtRequests.Rows[0]["Department"]);
                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Value_of_Plot = Convert.ToString(dtRequests.Rows[0]["Value_of_Plot"]);
                ViewBag.Total_Land_Plot_Value = Convert.ToString(dtRequests.Rows[0]["Total_Land_Plot_Value"]);
                




                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }

                if (dtrequestIdApprovedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);

                    //var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                }

                if (dtrequestIdConstructionDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_Constr = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);
                    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                }
                if (dtrequestIdSaleDeedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Authority"]);
                    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                }

                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                //vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();


                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + ",";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Plot_area_for_Valuation#");
                ViewBag.Adopted_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");









            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //L&T Bank Report For Bungalow
        public ActionResult LTSingleUnit(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdSaleDeedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSaleDeedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);





                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                    var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                    ViewBag.Date_of_Inspection = dateOnlyInspection;
                }
                ViewBag.Valuationdate = Convert.ToString(dtRequests.Rows[0]["Date_of_Valuation"]);
                if (ViewBag.Valuationdate != "")
                {
                   // var dateTimeNowValue = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Valuation"]); // Return 00/00/0000 00:00:00
                   // var dateOnlyValue = dateTimeNowValue.ToShortDateString();
                    ViewBag.Date_of_Valuation = ViewBag.Valuationdate;
                }

                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);
                ViewBag.StageOfConstructionBunglow = Convert.ToString(dtRequests.Rows[0]["StageOfConstructionBunglow"]);
                if (ViewBag.StageOfConstructionBunglow == "Ready")
                {
                    ViewBag.ToatalInWords = Convert.ToString(dtRequests.Rows[0]["TotalreadyBunglowWords"]);
                }
                else
                {
                    ViewBag.ToatalInWords = Convert.ToString(dtRequests.Rows[0]["TotalUCBunglowWords"]);
                }

                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);


                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);
                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);
                
                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.BranchName = Convert.ToString(dtRequests.Rows[0]["Branch_Name"]);
                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                ViewBag.Department = Convert.ToString(dtRequests.Rows[0]["Department"]);
                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Value_of_Plot = Convert.ToString(dtRequests.Rows[0]["Value_of_Plot"]);
                ViewBag.Total_Land_Plot_Value = Convert.ToString(dtRequests.Rows[0]["Total_Land_Plot_Value"]);





                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }

                if (dtrequestIdApprovedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);

                    //var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                }

                if (dtrequestIdConstructionDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_Constr = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);
                    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                }
                if (dtrequestIdSaleDeedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Authority"]);
                    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                }

                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                //vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.ival_ProjectFloorWiseBreakUps = GetFloorWiseBreakupByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();


                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + ",";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Plot_area_for_Valuation#");
                ViewBag.Adopted_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");

                ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");







            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //Hero Finance Report For Flat
        public ActionResult HeroFinanceMultiUnit(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();
            //string UserName = Session["UserName"].ToString();

            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            try
            {
               
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);

                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);


                
                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.TotalFlatwords = Convert.ToString(dtRequests.Rows[0]["TotalFlatwords"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    try
                    {
                        DateTime dti = DateTime.ParseExact(ViewBag.Inspectiondate.ToString(), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);

                        string Inspectiondate = dti.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                        //var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                        //var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                        ViewBag.Date_of_Inspection = Inspectiondate;
                    }
                    catch (Exception e)
                    {
                        string datestring1 = Convert.ToDateTime(ViewBag.Inspectiondate).ToString("dd/MM/yyyy");
                        ViewBag.Date_of_Inspection = datestring1;
                        //var format = "dd/MM/yyyy";
                        //var dateTime = DateTime.ParseExact(datestring1, format, CultureInfo.InvariantCulture);
                    }

                }

                try
                {
                    DateTime dtr = DateTime.ParseExact(Convert.ToString(dtRequests.Rows[0]["Request_Date"]), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);

                    string Requestdate = dtr.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                    //var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                    // var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                    ViewBag.RequestDate = Requestdate;

                }
                catch (Exception e)
                {
                    string datestring1 = Convert.ToDateTime(Convert.ToString(dtRequests.Rows[0]["Request_Date"])).ToString("dd/MM/yyyy");
                    ViewBag.RequestDate = datestring1;
                    //var format = "dd/MM/yyyy";
                    //var dateTime = DateTime.ParseExact(datestring1, format, CultureInfo.InvariantCulture);
                }

            
                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);
                StringBuilder sb = new StringBuilder();
                StringBuilder sb1 = new StringBuilder();
                StringBuilder sb2 = new StringBuilder();
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                if(ViewBag.UnitNumber!="")
                {
                    sb.Append(ViewBag.UnitNumber);
                }
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                if (ViewBag.Survey_Number != "")
                {
                    sb.Append(","+ViewBag.Survey_Number);
                }
                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                if (ViewBag.Project_Name != "")
                {
                    sb.Append("," + ViewBag.Project_Name);
                }
                ViewBag.Address1 = sb.ToString();

                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                if (ViewBag.Building_Name_RI != "")
                {
                    sb1.Append(ViewBag.Building_Name_RI);
                }
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                if (ViewBag.Wing_Name_RI != "")
                {
                    sb1.Append("," + ViewBag.Wing_Name_RI);
                }
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                if (ViewBag.Village_Name != "")
                {
                    sb1.Append("," + ViewBag.Village_Name);
                }
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                if (ViewBag.Street != "")
                {
                    sb1.Append("," + ViewBag.Street);
                }
                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                if (ViewBag.Locality != "")
                {
                    sb1.Append("," + ViewBag.Locality);
                }
                ViewBag.address2 = sb1.ToString();
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                if (ViewBag.Sub_Locality != "")
                {
                    sb2.Append(ViewBag.Sub_Locality);
                }
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                if (ViewBag.City != "")
                {
                    sb2.Append("," + ViewBag.City);
                }
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                if (ViewBag.District != "")
                {
                    sb2.Append("," + ViewBag.District);
                }
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                if (ViewBag.State != "")
                {
                    sb2.Append("," + ViewBag.State);
                }
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                if (ViewBag.PinCode != "")
                {
                    sb2.Append("," + ViewBag.PinCode);
                }
                ViewBag.address3 = sb2.ToString();
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                
               
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);



                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);


                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
               
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);
                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                
                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
               
               
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                
                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
               
                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.Government_Value = Convert.ToString(dtRequests.Rows[0]["Government_Value"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                StringBuilder sbRoad = new StringBuilder();
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                if(ViewBag.Road1!="")
                {
                    sbRoad.Append(ViewBag.Road1);
                }
                ViewBag.Road2 = Convert.ToString(dtRequests.Rows[0]["Road2"]);
                if (ViewBag.Road2 != "")
                {
                    sbRoad.Append(","+ViewBag.Road2);
                }
                ViewBag.Road3 = Convert.ToString(dtRequests.Rows[0]["Road3"]);
                if (ViewBag.Road3 != "")
                {
                    sbRoad.Append("," + ViewBag.Road3);
                }
                ViewBag.Road4 = Convert.ToString(dtRequests.Rows[0]["Road4"]);
                if (ViewBag.Road4 != "")
                {
                    sbRoad.Append("," + ViewBag.Road4);
                }
                ViewBag.WidthOfRoad = sbRoad.ToString();
                ViewBag.Actual_No_Floors_G = Convert.ToString(dtRequests.Rows[0]["Actual_No_Floors_G"]);
                //ViewBag.AdjoiningRoad_Width1 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width1"]);
                //ViewBag.AdjoiningRoad_Width2 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width2"]);
                //ViewBag.AdjoiningRoad_Width3 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width3"]);
                //ViewBag.AdjoiningRoad_Width4 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width4"]);




                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();
                vm.ival_Document_Uploaded_ListS = getUploadDocumentByReqId();

                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }
                else
                {
                    ViewBag.IGRImage = "NA.jpg";
                }

                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }

                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

               


                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");
                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");
                ViewBag.Any_Other_Charges = getUnitPrice(dtpricedetails, "#Any_Other_Charges#");
                ViewBag.Rate_Range_for_similar_Properties_in_the_locality = getUnitPrice(dtpricedetails, "#Rate_Range_for_similar_Properties_in_the_locality#");
                if (dtrequestIdUnitParaValue.Rows.Count > 0)
                {
                    ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                    ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                    ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                    ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");
                    ViewBag.NoofBalconies = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Balconies");
                    ViewBag.NoofToilets = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Toilets");
                    ViewBag.NoofParking = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Parking");

                }



            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //Hero Finance Report For Plot
        public ActionResult HeroFinanceSingleUnitPlot(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);

                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.TotalPlotwords = Convert.ToString(dtRequests.Rows[0]["TotalPlotwords"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                   
                    try
                    {
                        DateTime dti = DateTime.ParseExact(ViewBag.Inspectiondate.ToString(), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);

                        string Inspectiondate = dti.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                      
                        ViewBag.Date_of_Inspection = Inspectiondate;
                    }
                    catch (Exception e)
                    {
                        string datestring1 = Convert.ToDateTime(ViewBag.Inspectiondate).ToString("dd/MM/yyyy");
                        ViewBag.Date_of_Inspection = datestring1;

                    }
                }
                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

                
                try
                {
                   
                    DateTime dtr = DateTime.ParseExact(Convert.ToString(dtRequests.Rows[0]["Request_Date"]), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                    string Requestdate = dtr.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                    ViewBag.RequestDate = Requestdate;

                }
                catch (Exception e)
                {
                    string datestring1 = Convert.ToDateTime(Convert.ToString(dtRequests.Rows[0]["Request_Date"])).ToString("dd/MM/yyyy");
                    ViewBag.RequestDate = datestring1;

                }
                StringBuilder sb = new StringBuilder();
                StringBuilder sb1 = new StringBuilder();
                StringBuilder sb2 = new StringBuilder();
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                if (ViewBag.UnitNumber!="")
                {
                    sb.Append(ViewBag.UnitNumber);
                }
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                if (ViewBag.Survey_Number != "")
                {
                    sb.Append(","+ViewBag.Survey_Number);
                }
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                if (ViewBag.Village_Name != "")
                {
                    sb.Append("," + ViewBag.Village_Name);
                }
                ViewBag.Address1 = sb.ToString();
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                if (ViewBag.Street != "")
                {
                    sb1.Append(ViewBag.Street);
                }
                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                if (ViewBag.Locality != "")
                {
                    sb1.Append("," + ViewBag.Locality);
                }
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                if (ViewBag.Sub_Locality != "")
                {
                    sb1.Append("," + ViewBag.Sub_Locality);
                }
                ViewBag.Address2 = sb1.ToString();
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                if (ViewBag.City != "")
                {
                    sb2.Append(ViewBag.City);
                }
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                if (ViewBag.State != "")
                {
                    sb2.Append("," + ViewBag.State);
                }
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                if (ViewBag.District != "")
                {
                    sb2.Append("," + ViewBag.District);
                }
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                if (ViewBag.PinCode != "")
                {
                    sb2.Append("," + ViewBag.PinCode);
                }
                ViewBag.Address3 = sb2.ToString();
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
               
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
               
                
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);
                
                


                
                
               
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);


                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);

                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);

                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);
                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);

                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);

                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);

                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);

                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);

                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);

                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.Government_Value = Convert.ToString(dtRequests.Rows[0]["Government_Value"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                //ViewBag.AdjoiningRoad_Width1 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width1"]);
                //ViewBag.AdjoiningRoad_Width2 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width2"]);
                //ViewBag.AdjoiningRoad_Width3 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width3"]);
                //ViewBag.AdjoiningRoad_Width4 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width4"]);
                ViewBag.Value_of_Plot = Convert.ToString(dtRequests.Rows[0]["Value_of_Plot"]);
                ViewBag.Value_of_other_Plot = Convert.ToString(dtRequests.Rows[0]["Value_of_other_Plot"]);
                ViewBag.Total_Land_Plot_Value = Convert.ToString(dtRequests.Rows[0]["Total_Land_Plot_Value"]);
                ViewBag.Actual_No_Floors_G = Convert.ToString(dtRequests.Rows[0]["Actual_No_Floors_G"]);
                StringBuilder sbRoad = new StringBuilder();
                ViewBag.Road1= Convert.ToString(dtRequests.Rows[0]["Road1"]);
               if(ViewBag.Road1!="")
                {
                    sbRoad.Append(ViewBag.Road1);
                }
                ViewBag.Road2 = Convert.ToString(dtRequests.Rows[0]["Road2"]);
                if (ViewBag.Road2 != "")
                {
                    sbRoad.Append(","+ViewBag.Road2);
                }
                ViewBag.Road3 = Convert.ToString(dtRequests.Rows[0]["Road3"]);
                if (ViewBag.Road3 != "")
                {
                    sbRoad.Append("," + ViewBag.Road3);
                }
                ViewBag.Road4 = Convert.ToString(dtRequests.Rows[0]["Road4"]);
                if (ViewBag.Road4 != "")
                {
                    sbRoad.Append("," + ViewBag.Road4);
                }
                ViewBag.WidthOfRoad = sbRoad.ToString();
                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();
               
                vm.Image_Uploaded_List = getImagesByReqId();
                vm.ival_Document_Uploaded_ListS = getUploadDocumentByReqId();

                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }
                else
                {
                    ViewBag.IGRImage = "NA.jpg";
                }

                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }

                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }




                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");
                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");
                ViewBag.Any_Other_Charges = getUnitPrice(dtpricedetails, "#Any_Other_Charges#");
                ViewBag.Land_Plot_Rate_Range_in_the_Locality = getUnitPrice(dtpricedetails, "#Land_/_Plot_Rate_Range_in_the_Locality#");
                ViewBag.Adopted_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");
                ViewBag.Adopted_Plot_area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Plot_area_for_Valuation#");
                ViewBag.Adopted_Other_Plot_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Other_Plot_Area_for_Valuation#");
                ViewBag.Adopted_Other_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Other_Plot_/_Land_Rate#");

                if (dtrequestIdUnitParaValue.Rows.Count > 0)
                {
                    ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                    ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                    ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                    ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");
                    ViewBag.NoofBalconies = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Balconies");
                    ViewBag.NoofToilets = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Toilets");
                    ViewBag.NoofParking = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Parking");

                }






            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //Hero Finanace Report For Bungalow
        public ActionResult HeroFinanceSingleUnit(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdFloorWiseSumOfConstruction = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorWiseBreakupSumOfConstructionByRequestid", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);


                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    try
                    {
                        DateTime dti = DateTime.ParseExact(ViewBag.Inspectiondate.ToString(), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);

                        string Inspectiondate = dti.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                        //var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                        //var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                        ViewBag.Date_of_Inspection = Inspectiondate;
                    }
                    catch (Exception e)
                    {
                        string datestring1 = Convert.ToDateTime(ViewBag.Inspectiondate).ToString("dd/MM/yyyy");
                        ViewBag.Date_of_Inspection = datestring1;
                        
                    }

                    
                }
                try
                {

                    DateTime dtr = DateTime.ParseExact(Convert.ToString(dtRequests.Rows[0]["Request_Date"]), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                    string Requestdate = dtr.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                    ViewBag.RequestDate = Requestdate;

                }
                catch (Exception e)
                {
                    string datestring1 = Convert.ToDateTime(Convert.ToString(dtRequests.Rows[0]["Request_Date"])).ToString("dd/MM/yyyy");
                    ViewBag.RequestDate = datestring1;
                   
                }
                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
               
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
               

                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);
                StringBuilder sb = new StringBuilder();
                StringBuilder sb1 = new StringBuilder();
                StringBuilder sb2 = new StringBuilder();

                ViewBag.plotNumber = Convert.ToString(dtRequests.Rows[0]["Bunglow_number"]);
                if (ViewBag.plotNumber != "")
                {
                    sb.Append(ViewBag.plotNumber);
                }
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                if (ViewBag.Survey_Number != "")
                {
                    sb.Append(","+ViewBag.Survey_Number);
                }
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                if (ViewBag.Village_Name != "")
                {
                    sb.Append("," + ViewBag.Village_Name);
                }
                ViewBag.Address1 = sb.ToString();
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                if (ViewBag.Street != "")
                {
                    sb1.Append(ViewBag.Street);
                }
                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                if (ViewBag.Locality != "")
                {
                    sb1.Append("," + ViewBag.Locality);
                }

                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                if (ViewBag.Sub_Locality != "")
                {
                    sb1.Append("," + ViewBag.Sub_Locality);
                }
                ViewBag.Address2 = sb1.ToString();
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                if (ViewBag.City != "")
                {
                    sb2.Append(ViewBag.City);
                }
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                if (ViewBag.District != "")
                {
                    sb2.Append("," + ViewBag.District);
                }
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                if (ViewBag.State != "")
                {
                    sb2.Append("," + ViewBag.State);
                }
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                if (ViewBag.PinCode != "")
                {
                    sb2.Append("," + ViewBag.PinCode);
                }
                ViewBag.Address3 = sb2.ToString();
                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                
                
               

               
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);


                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);

                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);

                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);
                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);

                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);

                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);

                 ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);

                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["TotalvaluepropertyBunglow"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);

                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);

                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.Government_Value = Convert.ToString(dtRequests.Rows[0]["Government_Value"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                //ViewBag.AdjoiningRoad_Width1 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width1"]);
                //ViewBag.AdjoiningRoad_Width2 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width2"]);
                //ViewBag.AdjoiningRoad_Width3 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width3"]);

                ViewBag.StageOfConstructionBunglow = Convert.ToString(dtRequests.Rows[0]["StageOfConstructionBunglow"]);

                if (ViewBag.StageOfConstructionBunglow == "Ready")
                {
                    ViewBag.ToatalInWords = Convert.ToString(dtRequests.Rows[0]["TotalreadyBunglowWords"]);
                }
                else
                {
                    ViewBag.ToatalInWords = Convert.ToString(dtRequests.Rows[0]["TotalUCBunglowWords"]);
                }
                //ViewBag.AdjoiningRoad_Width4 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width4"]);
                StringBuilder sbRoad = new StringBuilder();

                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                if(ViewBag.Road1!="")
                {
                    sbRoad.Append(ViewBag.Road1);
                }
                ViewBag.Road2 = Convert.ToString(dtRequests.Rows[0]["Road2"]);
                if (ViewBag.Road2 != "")
                {
                    sbRoad.Append(","+ViewBag.Road2);
                }
                ViewBag.Road3 = Convert.ToString(dtRequests.Rows[0]["Road3"]);
                if (ViewBag.Road3 != "")
                {
                    sbRoad.Append("," + ViewBag.Road3);
                }
                ViewBag.Road4 = Convert.ToString(dtRequests.Rows[0]["Road4"]);
                if (ViewBag.Road4 != "")
                {
                    sbRoad.Append("," + ViewBag.Road4);
                }
                ViewBag.WidthOfRoad = sbRoad.ToString();
                ViewBag.Value_of_Plot = Convert.ToString(dtRequests.Rows[0]["Totalofthebungalow"]);
                ViewBag.PerWorkCompleted = Convert.ToString(dtRequests.Rows[0]["StageOfConstructionPerct"]);
                ViewBag.PerDisbursement = Convert.ToString(dtRequests.Rows[0]["DisbursementRecommendPerct"]);
                ViewBag.GovRate = Convert.ToString(dtRequests.Rows[0]["Government_Rate"]);

                ViewBag.Actual_No_Floors_G = Convert.ToString(dtRequests.Rows[0]["Actual_No_Floors_G"]);

                ViewBag.StageOfConstructionBunglow = Convert.ToString(dtRequests.Rows[0]["StageOfConstructionBunglow"]);

                if (ViewBag.StageOfConstructionBunglow == "Ready")
                {
                    ViewBag.ToatalInWords = Convert.ToString(dtRequests.Rows[0]["TotalreadyBunglowWords"]);
                }
                else
                {
                    ViewBag.ToatalInWords = Convert.ToString(dtRequests.Rows[0]["TotalUCBunglowWords"]);
                }

                if (dtrequestIdFloorWiseSumOfConstruction.Rows.Count > 0)
                {
                    ViewBag.TotalConstructionCost = Convert.ToString(dtrequestIdFloorWiseSumOfConstruction.Rows[0]["Adopted_Cost_Construction"]);
                }



                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.ival_ProjectFloorWiseBreakUps = GetFloorWiseBreakupWithTotalByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();
                vm.ival_Document_Uploaded_ListS = getUploadDocumentByReqId();
                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }
                else
                {
                    ViewBag.IGRImage = "NA.jpg";
                }

                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }

                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }




                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
               
                ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
              
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");
                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");
                ViewBag.Any_Other_Charges = getUnitPrice(dtpricedetails, "#Any_Other_Charges#");
                ViewBag.Land_Plot_Rate_Range_in_the_Locality = getUnitPrice(dtpricedetails, "#Land_/_Plot_Rate_Range_in_the_Locality#");
                ViewBag.Adopted_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");
                ViewBag.Adopted_Plot_area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");
                if (dtrequestIdUnitParaValue.Rows.Count > 0)
                {
                    ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                    ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                    ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                    ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");
                    ViewBag.NoofBalconies = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Balconies");
                    ViewBag.NoofToilets = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Toilets");
                    ViewBag.NoofParking = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Parking");

                }
            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //Ivalue Report For Flat
        public ActionResult IvalueReportFlat(int ID)
        {
            //int ID = 111;
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);






                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);


                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                    var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                    ViewBag.Date_of_Inspection = dateOnlyInspection;
                }
                ViewBag.Valuationdate = Convert.ToString(dtRequests.Rows[0]["Date_of_Valuation"]);
                if (ViewBag.Valuationdate != "")
                {
                   // var dateTimeNowValue = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Valuation"]); // Return 00/00/0000 00:00:00
                  //  var dateOnlyValue = dateTimeNowValue.ToShortDateString();
                    ViewBag.Date_of_Valuation = ViewBag.Valuationdate;
                }
                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);

                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);

                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                

                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.DeviationPercentage = Convert.ToString(dtRequests.Rows[0]["DeviationPercentage"]);

                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                ViewBag.distressValue = Convert.ToString(dtRequests.Rows[0]["distressValue"]);
                ViewBag.AssignedToEmpID = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Expected_Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Expected_Monthly_Rental"]);


                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }
                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }


                if (dtrequestIdApprovedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);


                    var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                    var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval = dateOnlyString1;
                }

                if (dtrequestIdConstructionDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);

                    var dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                    var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_Constr = dateOnlyString2;
                }


                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();

                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + ",";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                //ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                ViewBag.Area_as_per_Agreement = getUnitPrice(dtpricedetails, "#Area_as_per_Agreement#");
                //ViewBag.Measured_Saleable_Area = getUnitPrice(dtpricedetails, "#Measured_Saleable_Area#");
                ViewBag.Salable_Area_as_per_Agreement = getUnitPrice(dtpricedetails, "#Salable_Area_as_per_Agreement#");
                //ViewBag.Approved_Saleable_Area = getUnitPrice(dtpricedetails, "#Approved_Saleable_Area#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");
                ViewBag.RateRangeinLocality = getUnitPrice(dtpricedetails, "#Rate_Range_for_similar_Properties_in_the_locality#");
                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");


            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }

            return View(vm);
        }

        //Ivalue Report For Plot
        public ActionResult IValueReportPlot(int ID)
        {
            //int ID = 111;
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);






                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                    var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                    ViewBag.Date_of_Inspection = dateOnlyInspection;
                }
                ViewBag.Valuationdate = Convert.ToString(dtRequests.Rows[0]["Date_of_Valuation"]);
                if (ViewBag.Valuationdate != "")
                {
                   // var dateTimeNowValue = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Valuation"]); // Return 00/00/0000 00:00:00
                  //  var dateOnlyValue = dateTimeNowValue.ToShortDateString();
                    ViewBag.Date_of_Valuation = ViewBag.Valuationdate;
                }
                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);

                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);
                ViewBag.PlotNumber = Convert.ToString(dtRequests.Rows[0]["Plot_Number"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);

                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.DeviationPercentage = Convert.ToString(dtRequests.Rows[0]["DeviationPercentage"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.ValueOfPlot = Convert.ToString(dtRequests.Rows[0]["Value_of_Plot"]);
                ViewBag.TotalValuation = Convert.ToString(dtRequests.Rows[0]["Total_Land_Plot_Value"]);
                ViewBag.Distress_Value_of_Plot = Convert.ToString(dtRequests.Rows[0]["Distress_Value_of_Plot"]);
                ViewBag.AssignedToEmpID = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Expected_Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Expected_Monthly_Rental"]);

                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }
                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }

                if (dtrequestIdApprovedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);


                    var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                    var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval = dateOnlyString1;
                }

                if (dtrequestIdConstructionDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);

                    var dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                    var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_Constr = dateOnlyString2;
                }



                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();

                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }

                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + " , ";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");

                ViewBag.Adopted_Plot_area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Plot_area_for_Valuation#");
                ViewBag.RateRangeinLocality = getUnitPrice(dtpricedetails, "#Land_/_Plot_Rate_Range_in_the_Locality#");
                ViewBag.AdoptedRate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");
                ViewBag.DistressValuePerOfPlot = getUnitPrice(dtpricedetails, "#Distress_Value_for_Plot%#");
                //ViewBag.DistressValueOfPlot = getUnitPrice(dtpricedetails, "#Distress_Value_of_Plot#");
                ViewBag.Approved_Plot_Area = getUnitPrice(dtpricedetails, "#Approved_Plot_Area#");
                ViewBag.Measured_Plot_Area = getUnitPrice(dtpricedetails, "#Measured_Plot_Area#");
                ViewBag.ApprovedOtherPlotArea = getUnitPrice(dtpricedetails, "#Approved_Other_Land/_Plot_Area_#");
                ViewBag.MeasuredOtherPlotArea = getUnitPrice(dtpricedetails, "#Measured_Other_Plot_Area#");
                ViewBag.AdoptedOtherPlotValuation = getUnitPrice(dtpricedetails, "#Adopted_Other_Plot_Area_for_Valuation#");
                ViewBag.AdoptedOtherPlotRate = getUnitPrice(dtpricedetails, "#Adopted_Other_Plot_/_Land_Rate#");


            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }

            return View(vm);
        }

        //Ivalue Report For Bungalow
        public ActionResult IValueReportBungalow(int ID)
        {
            //int ID = 111;
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdFloorTypeTotal = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorTypeTotalByReqID", hInputPara);
                DataTable dtrequestIdAllFloorTypeSumTotal = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSumTotalAllFloorTypeByRequestid", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);


                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                    var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                    ViewBag.Date_of_Inspection = dateOnlyInspection;
                }
                ViewBag.Valuationdate = Convert.ToString(dtRequests.Rows[0]["Date_of_Valuation"]);
                if (ViewBag.Valuationdate != "")
                {
                    //var dateTimeNowValue = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Valuation"]); // Return 00/00/0000 00:00:00
                 //   var dateOnlyValue = dateTimeNowValue.ToShortDateString();
                    ViewBag.Date_of_Valuation = ViewBag.Valuationdate;
                }
                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                ViewBag.BungalowNo = Convert.ToString(dtRequests.Rows[0]["Bunglow_Number"]);

                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);

                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["TotalvaluepropertyBunglow"]);

                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.DeviationPercentage = Convert.ToString(dtRequests.Rows[0]["DeviationPercentage"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.ValueOfPlot = Convert.ToString(dtRequests.Rows[0]["Value_of_Plot"]);
                ViewBag.Total_Land_Plot_Value = Convert.ToString(dtRequests.Rows[0]["Total_Land_Plot_Value"]);
                ViewBag.AssignedToEmpID = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.DistressValuePerct = Convert.ToString(dtRequests.Rows[0]["DistressValuePerct"]);
                ViewBag.DistressValueProperty = Convert.ToString(dtRequests.Rows[0]["DistressValueofPropertyBunglow"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Expected_Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Expected_Monthly_Rental"]);

                if (dtrequestIdFloorTypeTotal.Rows.Count > 0)
                {
                    ViewBag.TotalOfMeasured_Built_up_Area = Convert.ToString(dtrequestIdFloorTypeTotal.Rows[0]["Measured_Built_up_Area"]);
                    ViewBag.TotalOfApproved_Built_up_Area = Convert.ToString(dtrequestIdFloorTypeTotal.Rows[0]["Approved_Built_up_Area"]);
                    ViewBag.TotalOfAdopted_Cost_Construction = Convert.ToString(dtrequestIdFloorTypeTotal.Rows[0]["Adopted_Cost_Construction"]);
                }



                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }
                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }
                if (dtrequestIdApprovedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);


                    var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                    var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval = dateOnlyString1;
                }

                if (dtrequestIdConstructionDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);

                    var dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                    var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_Constr = dateOnlyString2;
                }
                if (dtrequestIdAllFloorTypeSumTotal.Rows.Count > 0)
                {
                    ViewBag.Total_ValueOfConstruction = Convert.ToString(dtrequestIdAllFloorTypeSumTotal.Rows[0]["Sumtotal"]);


                }

                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();

                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }

                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + " , ";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                // ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                //ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");

                ViewBag.LandArea = getUnitPrice(dtpricedetails, "#Adopted_Plot_area_for_Valuation#");
                ViewBag.RecommandedRate = getUnitPrice(dtpricedetails, "#Land_/_Plot_Rate_Range_in_the_Locality#");
                ViewBag.AdoptedRate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");
                // ViewBag.DistressValuePerOfPlot = getUnitPrice(dtpricedetails, "#Distress_Value_for_Plot%#");
                //ViewBag.DistressValueOfPlot = getUnitPrice(dtpricedetails, "#Distress_Value_of_Plot#");
                //ViewBag.ActualBuaPremises = getUnitPrice(dtpricedetails, "#Approved_Other_Land/_Plot_Area_#");
                //ViewBag.BuaAsPerApproval = getUnitPrice(dtpricedetails, "#Measured_Plot_Area#");
                //ViewBag.ConstructionCost = getUnitPrice(dtpricedetails, "#Cost_of_Construction#");
                ViewBag.TotalValueOfConstruction = getUnitPrice(dtpricedetails, "#Gross_Estimated_cost_of_Construction_#");

                ViewBag.Approved_Plot_Area = getUnitPrice(dtpricedetails, "#Approved_Plot_Area#");

                ViewBag.Measured_Plot_Area = getUnitPrice(dtpricedetails, "#Measured_Plot_Area#");
                ViewBag.ApprovedOtherPlotArea = getUnitPrice(dtpricedetails, "#Approved_Other_Land/_Plot_Area_#");
                ViewBag.MeasuredOtherPlotArea = getUnitPrice(dtpricedetails, "#Measured_Other_Plot_Area#");
                ViewBag.AdoptedOtherPlotValuation = getUnitPrice(dtpricedetails, "#Adopted_Other_Plot_Area_for_Valuation#");
                ViewBag.AdoptedOtherPlotRate = getUnitPrice(dtpricedetails, "#Adopted_Other_Plot_/_Land_Rate#");
                ViewBag.Approve_Builtup_Area = getUnitPrice(dtpricedetails, "#Approve_Built-up_Area#");
                ViewBag.Measured_Builtup_Area = getUnitPrice(dtpricedetails, "#Measured_Built-up_Area#");
                if (ViewBag.Approve_Builtup_Area != "" && ViewBag.Measured_Builtup_Area != "")
                {
                    double DeviationinConstruction1 = ViewBag.Measured_Builtup_Area / ViewBag.Approve_Builtup_Area;
                    ViewBag.DeviationinConstruction = DeviationinConstruction1;
                }

            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }

            return View(vm);
        }

        //Sundaram Finance Report For Flat
        public ActionResult SundaramFinanceMultiUnit(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                //DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
               // DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
               // DataTable dtrequestIdSaleDeedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSaleDeedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);






                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.TotalFlatwords = Convert.ToString(dtRequests.Rows[0]["TotalFlatwords"]);

                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    try
                    {
                        DateTime dti = DateTime.ParseExact(ViewBag.Inspectiondate.ToString(), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);

                        string Inspectiondate = dti.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);

                        ViewBag.Date_of_Inspection = Inspectiondate;
                    }
                    catch (Exception e)
                    {
                        string datestring1 = Convert.ToDateTime(ViewBag.Inspectiondate).ToString("dd/MM/yyyy");
                        ViewBag.Date_of_Inspection = datestring1;

                    }
                   
                }
                try
                {
                    DateTime dtr = DateTime.ParseExact(Convert.ToString(dtRequests.Rows[0]["Request_Date"]), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);

                    string Requestdate = dtr.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                  
                    ViewBag.RequestDate = Requestdate;
                   
                }
                catch (Exception e)
                {
                    string datestring1 = Convert.ToDateTime(Convert.ToString(dtRequests.Rows[0]["Request_Date"])).ToString("dd/MM/yyyy");
                    ViewBag.RequestDate = datestring1;
                   
                }

                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

                StringBuilder sb = new StringBuilder();
                StringBuilder sb1 = new StringBuilder();
                StringBuilder sb2 = new StringBuilder();
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                if(ViewBag.UnitNumber!="")
                {
                    sb.Append(ViewBag.UnitNumber);
                }
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                if (ViewBag.Survey_Number != "")
                {
                    sb.Append(","+ViewBag.Survey_Number);
                }
                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                if (ViewBag.Project_Name != "")
                {
                    sb.Append("," + ViewBag.Project_Name);
                }
                ViewBag.Address1 = sb.ToString();
                
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                if (ViewBag.Building_Name_RI != "")
                {
                    sb1.Append(ViewBag.Building_Name_RI);
                }
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                if (ViewBag.Wing_Name_RI != "")
                {
                    sb1.Append("," + ViewBag.Wing_Name_RI);
                }
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                if (ViewBag.Village_Name != "")
                {
                    sb1.Append("," + ViewBag.Village_Name);
                }
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                if (ViewBag.Street != "")
                {
                    sb1.Append("," + ViewBag.Street);
                }
                ViewBag.Address2 = sb1.ToString();
                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                if(ViewBag.Locality!="")
                {
                    sb2.Append(ViewBag.Locality);
                }
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                if (ViewBag.City != "")
                {
                    sb2.Append(","+ViewBag.City);
                }
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                if (ViewBag.District != "")
                {
                    sb2.Append("," + ViewBag.District);
                }
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                if (ViewBag.State != "")
                {
                    sb2.Append("," + ViewBag.State);
                }
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                if (ViewBag.PinCode != "")
                {
                    sb2.Append("," + ViewBag.PinCode);
                }
                ViewBag.Address3 = sb2.ToString();
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
               
                
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);


               
                
                
                
                
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);


                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Road2 = Convert.ToString(dtRequests.Rows[0]["Road2"]);
                ViewBag.Road3 = Convert.ToString(dtRequests.Rows[0]["Road3"]);
                ViewBag.Road4 = Convert.ToString(dtRequests.Rows[0]["Road4"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);

                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.BranchName = Convert.ToString(dtRequests.Rows[0]["Branch_Name"]);
                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                ViewBag.Department = Convert.ToString(dtRequests.Rows[0]["Department"]);
                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Actual_No_Floors_G = Convert.ToString(dtRequests.Rows[0]["Actual_No_Floors_G"]);
                ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Government_Value = Convert.ToString(dtRequests.Rows[0]["Government_Value"]);
                

                ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);

                ViewBag.AdjoiningRoad_Width1 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width1"]);
                ViewBag.AdjoiningRoad_Width2 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width2"]);
                ViewBag.AdjoiningRoad_Width3 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width3"]);
                ViewBag.AdjoiningRoad_Width4 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width4"]);

                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }
                else
                {
                    ViewBag.IGRImage = "NA.jpg";
                }
                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }

                //if (dtrequestIdApprovedDoc.Rows.Count > 0)
                //{
                //    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);
                //    ViewBag.Approval_Authority = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);

                //    //var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                //    //var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                //    ViewBag.Date_Of_Approval = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                //}

                //if (dtrequestIdConstructionDoc.Rows.Count > 0)
                //{
                //    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);
                //    ViewBag.Approval_Authority_Constr = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);
                //    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                //    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                //    ViewBag.Date_Of_Approval_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                //}
                //if (dtrequestIdSaleDeedDoc.Rows.Count > 0)
                //{
                //    ViewBag.Approval_Number_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Number"]);
                //    ViewBag.Approval_Authority_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Authority"]);
                //    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                //    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                //    ViewBag.Date_Of_Approval_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                //}

                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();
                vm.ival_Document_Uploaded_ListS = getUploadDocumentByReqId();

                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + ",";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");
                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");
                ViewBag.Rate_range_in_Locality = getUnitPrice(dtpricedetails, "#Rate_Range_for_similar_Properties_in_the_locality#");


                ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");
                ViewBag.NoofBalconies = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Balconies");
                ViewBag.NoofToilets = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Toilets");
                ViewBag.NoofParking = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Parking");
                









            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //Sundaram Finance Report For Plot
        public ActionResult SundaramFinanceSingleUnitPlot(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                //DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                //DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
               // DataTable dtrequestIdSaleDeedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSaleDeedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);






                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.TotalPlotwords = Convert.ToString(dtRequests.Rows[0]["TotalPlotwords"]);

                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                   
                    try
                    {
                        DateTime dti = DateTime.ParseExact(ViewBag.Inspectiondate.ToString(), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);

                        string Inspectiondate = dti.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                       
                        ViewBag.Date_of_Inspection = Inspectiondate;
                    }
                    catch (Exception e)
                    {
                        string datestring1 = Convert.ToDateTime(ViewBag.Inspectiondate).ToString("dd/MM/yyyy");
                        ViewBag.Date_of_Inspection = datestring1;

                    }

                }
                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

                
                try
                {

                    DateTime dtr = DateTime.ParseExact(Convert.ToString(dtRequests.Rows[0]["Request_Date"]), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                    string Requestdate = dtr.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                    ViewBag.RequestDate = Requestdate;

                }
                catch (Exception e)
                {
                    string datestring1 = Convert.ToDateTime(Convert.ToString(dtRequests.Rows[0]["Request_Date"])).ToString("dd/MM/yyyy");
                    ViewBag.RequestDate = datestring1;

                }
                

                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);



                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);


                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Road2 = Convert.ToString(dtRequests.Rows[0]["Road2"]);
                ViewBag.Road3 = Convert.ToString(dtRequests.Rows[0]["Road3"]);
                ViewBag.Road4 = Convert.ToString(dtRequests.Rows[0]["Road4"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);

                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.BranchName = Convert.ToString(dtRequests.Rows[0]["Branch_Name"]);
                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                ViewBag.Department = Convert.ToString(dtRequests.Rows[0]["Department"]);
                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Actual_No_Floors_G = Convert.ToString(dtRequests.Rows[0]["Actual_No_Floors_G"]);
                ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Government_Value = Convert.ToString(dtRequests.Rows[0]["Government_Value"]);
                ViewBag.Plot_Number = Convert.ToString(dtRequests.Rows[0]["Plot_Number"]);
                ViewBag.Value_of_Plot = Convert.ToString(dtRequests.Rows[0]["Value_of_Plot"]);
                ViewBag.Value_of_other_Plot = Convert.ToString(dtRequests.Rows[0]["Value_of_other_Plot"]);
                ViewBag.Total_Land_Plot_Value = Convert.ToString(dtRequests.Rows[0]["Total_Land_Plot_Value"]);

                ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);

                ViewBag.AdjoiningRoad_Width1 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width1"]);
                ViewBag.AdjoiningRoad_Width2 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width2"]);
                ViewBag.AdjoiningRoad_Width3 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width3"]);
                ViewBag.AdjoiningRoad_Width4 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width4"]);

                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }
                else
                {
                    ViewBag.IGRImage = "NA.jpg";
                }
                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }

                //if (dtrequestIdApprovedDoc.Rows.Count > 0)
                //{
                //    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);
                //    ViewBag.Approval_Authority = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);

                //    //var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                //    //var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                //    ViewBag.Date_Of_Approval = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                //}

                //if (dtrequestIdConstructionDoc.Rows.Count > 0)
                //{
                //    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);
                //    ViewBag.Approval_Authority_Constr = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);
                //    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                //    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                //    ViewBag.Date_Of_Approval_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                //}
                //if (dtrequestIdSaleDeedDoc.Rows.Count > 0)
                //{
                //    ViewBag.Approval_Number_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Number"]);
                //    ViewBag.Approval_Authority_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Authority"]);
                //    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                //    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                //    ViewBag.Date_Of_Approval_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                //}

                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();
                vm.ival_Document_Uploaded_ListS = getUploadDocumentByReqId();

                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + ",";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");

                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
               
                ViewBag.Adopted_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");
                ViewBag.Adopted_Other_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Other_Plot_/_Land_Rate#");
                ViewBag.Land_Plot_Rate_Range_in_the_Locality = getUnitPrice(dtpricedetails, "#Land_/_Plot_Rate_Range_in_the_Locality#");
                ViewBag.Adopted_Plot_area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Plot_area_for_Valuation#");
                ViewBag.Adopted_Other_Plot_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Other_Plot_Area_for_Valuation#");
                

                ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");
                ViewBag.NoofBalconies = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Balconies");
                ViewBag.NoofToilets = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Toilets");
                ViewBag.NoofParking = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Parking");

            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //Sundaram Finance Report For Bungalow
        public ActionResult SundaramFinanceSingleUnit(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                //DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                //DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
               // DataTable dtrequestIdSaleDeedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSaleDeedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);
                DataTable dtrequestIdFloorWiseSumOfConstruction = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorWiseBreakupSumOfConstructionByRequestid", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);






                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    try
                    {
                        DateTime dti = DateTime.ParseExact(ViewBag.Inspectiondate.ToString(), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);

                        string Inspectiondate = dti.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);

                        ViewBag.Date_of_Inspection = Inspectiondate;
                    }
                    catch (Exception e)
                    {
                        string datestring1 = Convert.ToDateTime(ViewBag.Inspectiondate).ToString("dd/MM/yyyy");
                        ViewBag.Date_of_Inspection = datestring1;

                    }
                   
                }
                try
                {
                    DateTime dtr = DateTime.ParseExact(Convert.ToString(dtRequests.Rows[0]["Request_Date"]), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);

                    string Requestdate = dtr.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);

                    ViewBag.RequestDate = Requestdate;

                }
                catch (Exception e)
                {
                    string datestring1 = Convert.ToDateTime(Convert.ToString(dtRequests.Rows[0]["Request_Date"])).ToString("dd/MM/yyyy");
                    ViewBag.RequestDate = datestring1;

                }
                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

                
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);




                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);


                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Road2 = Convert.ToString(dtRequests.Rows[0]["Road2"]);
                ViewBag.Road3 = Convert.ToString(dtRequests.Rows[0]["Road3"]);
                ViewBag.Road4 = Convert.ToString(dtRequests.Rows[0]["Road4"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);

                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                StringBuilder sb = new StringBuilder();
                StringBuilder sb1 = new StringBuilder();
                StringBuilder sb2 = new StringBuilder();
                ViewBag.Bunglow_Number = Convert.ToString(dtRequests.Rows[0]["Bunglow_Number"]);
                if(ViewBag.Bunglow_Number!="")
                {
                    sb.Append(ViewBag.Bunglow_Number);
                }
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                if (ViewBag.Survey_Number != "")
                {
                    sb.Append(","+ViewBag.Survey_Number);
                }
                ViewBag.Address1 = sb.ToString();
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                if (ViewBag.Village_Name != "")
                {
                    sb1.Append(ViewBag.Village_Name);
                }
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                if (ViewBag.Street != "")
                {
                    sb1.Append(","+ViewBag.Street);
                }
                ViewBag.Address2 = sb1.ToString();
                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                if(ViewBag.Locality!="")
                {
                    sb2.Append(ViewBag.Locality);
                }
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                if (ViewBag.District != "")
                {
                    sb2.Append(","+ViewBag.District);
                }
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                if (ViewBag.State != "")
                {
                    sb2.Append("," + ViewBag.State);
                }

                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                if (ViewBag.PinCode != "")
                {
                    sb2.Append("," + ViewBag.PinCode);
                }
                ViewBag.Address3 = sb2.ToString();
                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                
               
               
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.BranchName = Convert.ToString(dtRequests.Rows[0]["Branch_Name"]);
                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstructionBunglow = Convert.ToString(dtRequests.Rows[0]["StageOfConstructionBunglow"]);
                if (ViewBag.StageOfConstructionBunglow == "Ready")
                {
                    ViewBag.ToatalInWords = Convert.ToString(dtRequests.Rows[0]["TotalreadyBunglowWords"]);
                }
                else
                {
                    ViewBag.ToatalInWords = Convert.ToString(dtRequests.Rows[0]["TotalUCBunglowWords"]);
                }

                

                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                ViewBag.Department = Convert.ToString(dtRequests.Rows[0]["Department"]);
                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Actual_No_Floors_G = Convert.ToString(dtRequests.Rows[0]["Actual_No_Floors_G"]);
                ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Government_Value = Convert.ToString(dtRequests.Rows[0]["Government_Value"]);
                
                ViewBag.Valueoftheplotbungalow = Convert.ToString(dtRequests.Rows[0]["Valueoftheplotbungalow"]);
                ViewBag.Work_Completed_Perc = Convert.ToString(dtRequests.Rows[0]["Work_Completed_Perc"]);
                ViewBag.DisbursementRecommendPerct = Convert.ToString(dtRequests.Rows[0]["DisbursementRecommendPerct"]);
                ViewBag.Government_Rate = Convert.ToString(dtRequests.Rows[0]["Government_Rate"]);

                ViewBag.TotalvaluepropertyBunglow = Convert.ToString(dtRequests.Rows[0]["TotalvaluepropertyBunglow"]);

                ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);

              

                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }
                else
                {
                    ViewBag.IGRImage = "NA.jpg";
                }

                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }
                //if (dtrequestIdApprovedDoc.Rows.Count > 0)
                //{
                //    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);
                //    ViewBag.Approval_Authority = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);

                //    //var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                //    //var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                //    ViewBag.Date_Of_Approval = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                //}

                //if (dtrequestIdConstructionDoc.Rows.Count > 0)
                //{
                //    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);
                //    ViewBag.Approval_Authority_Constr = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);
                //    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                //    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                //    ViewBag.Date_Of_Approval_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                //}
                //if (dtrequestIdSaleDeedDoc.Rows.Count > 0)
                //{
                //    ViewBag.Approval_Number_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Number"]);
                //    ViewBag.Approval_Authority_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Authority"]);
                //    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                //    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                //    ViewBag.Date_Of_Approval_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                //}
                if (dtrequestIdFloorWiseSumOfConstruction.Rows.Count > 0)
                {
                    ViewBag.TotalConstructionCost = Convert.ToString(dtrequestIdFloorWiseSumOfConstruction.Rows[0]["Adopted_Cost_Construction"]);
                }

                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.ival_ProjectFloorWiseBreakUps = GetFloorWiseBreakupWithTotalByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();
                vm.ival_Document_Uploaded_ListS = getUploadDocumentByReqId();

                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + ",";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");

                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");

                ViewBag.Adopted_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");
                ViewBag.Adopted_Other_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Other_Plot_/_Land_Rate#");
                ViewBag.Land_Plot_Rate_Range_in_the_Locality = getUnitPrice(dtpricedetails, "#Land_/_Plot_Rate_Range_in_the_Locality#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");






                ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");
                ViewBag.NoofBalconies = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Balconies");
                ViewBag.NoofToilets = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Toilets");
                ViewBag.NoofParking = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Parking");


            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //Edelweiss Finance Report For Flat
        public ActionResult EdelweissMultiUnit(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdSaleDeedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSaleDeedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdCommencementDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCommencementCertificateByReqIDForPDF", hInputPara);
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);



                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                //ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                //ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);

                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                    var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                    ViewBag.Date_of_Inspection = dateOnlyInspection;
                }
                ViewBag.Valuationdate = Convert.ToString(dtRequests.Rows[0]["Date_of_Valuation"]);
                if (ViewBag.Valuationdate != "")
                {
                 //   var dateTimeNowValue = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Valuation"]); // Return 00/00/0000 00:00:00
                 //   var dateOnlyValue = dateTimeNowValue.ToShortDateString();
                    ViewBag.Date_of_Valuation = ViewBag.Valuationdate;
                }


                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);


                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                //ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                //ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);



                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);


                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                //ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                //ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                //ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                //ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                //ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);

                //ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                //ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                //ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                //ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                //ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                //ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                //ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["TotalFlatWords"]);
                //ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks1 = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);

                //ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                //ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.BranchName = Convert.ToString(dtRequests.Rows[0]["Branch_Name"]);
                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                //ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                //ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                //ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                //ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                //ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                //ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                //ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                //ViewBag.Department = Convert.ToString(dtRequests.Rows[0]["Department"]);
                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                if (ViewBag.Boundaries_Matching == "Yes")
                    
                {
                  
                    ViewBag.Remark = "";
                
                }
               
                else
                
                {
                  
                    ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);

                }



                ViewBag.Actual_No_Floors_G = Convert.ToString(dtRequests.Rows[0]["Actual_No_Floors_G"]);
                //ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                //ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Government_Value = Convert.ToString(dtRequests.Rows[0]["Government_Value"]);
                
                //ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                //ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                //ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);


                ViewBag.AdjoiningRoad_Width1 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width1"]);
                ViewBag.AdjoiningRoad_Width2 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width2"]);
                ViewBag.AdjoiningRoad_Width3 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width3"]);
                ViewBag.AdjoiningRoad_Width4 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width4"]);
                ViewBag.Client_Name = Convert.ToString(dtRequests.Rows[0]["Client_Name"]);
                ViewBag.SeismicZone = Convert.ToString(dtRequests.Rows[0]["SeismicZone"]);
                ViewBag.FloodZone = Convert.ToString(dtRequests.Rows[0]["FloodZone"]);
                ViewBag.CycloneZone = Convert.ToString(dtRequests.Rows[0]["CycloneZone"]);
                ViewBag.CRZ = Convert.ToString(dtRequests.Rows[0]["CRZ"]);
                ViewBag.RERA_Registration_No = Convert.ToString(dtRequests.Rows[0]["RERA_Registration_No"]);
                ViewBag.distressValue = Convert.ToString(dtRequests.Rows[0]["distressValue"]);


                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }
                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }

                if (dtrequestIdApprovedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);

                    //var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                }

                if (dtrequestIdConstructionDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_Constr = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);
                    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                }
                if (dtrequestIdSaleDeedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Authority"]);
                    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                }
                if (dtrequestIdCommencementDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Approval_Authority"]);
                   
                    ViewBag.Date_Of_Approval_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Date_Of_Approval"]);
                }
                

                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();


                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                //if (dtrequestIdLocalTransport.Rows.Count > 0)
                //{
                //    string str1 = "";
                //    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                //    {
                //        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                //        str1 += Loacal + ",";

                //    }
                //    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                //}


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + ",";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                //ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                //ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                //ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                //ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                //ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");
                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");
                ViewBag.Rate_range_in_Locality = getUnitPrice(dtpricedetails, "#Rate_Range_for_similar_Properties_in_the_locality#");
                ViewBag.Any_Other_Charges = getUnitPrice(dtpricedetails, "#Any_Other_Charges#");



                //ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                //ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                //ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                //ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");
                //ViewBag.NoofBalconies = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Balconies");
                //ViewBag.NoofToilets = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Toilets");
                //ViewBag.NoofParking = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Parking");










            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //Edelweiss Finance Reort For Plot
        public ActionResult EdelweissSingleUnitPlot(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdSaleDeedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSaleDeedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdCommencementDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCommencementCertificateByReqIDForPDF", hInputPara);
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);
                DataTable dtrequestIdSumOfApprovedBuiltUpArea = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorWiseBreakupSumOfApproved_Built_up_AreaByRequestid", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);





                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                //ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                //ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);

                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                    var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                    ViewBag.Date_of_Inspection = dateOnlyInspection;
                }
                ViewBag.Valuationdate = Convert.ToString(dtRequests.Rows[0]["Date_of_Valuation"]);
                if (ViewBag.Valuationdate != "")
                {
                 //   var dateTimeNowValue = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Valuation"]); // Return 00/00/0000 00:00:00
                 //   var dateOnlyValue = dateTimeNowValue.ToShortDateString();
                    ViewBag.Date_of_Valuation = ViewBag.Valuationdate;
                }

                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);


                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                ViewBag.Plot_Number = Convert.ToString(dtRequests.Rows[0]["Plot_Number"]);
                
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                //ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                //ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);

                ViewBag.ToatalInWords = Convert.ToString(dtRequests.Rows[0]["TotalUCBunglowWords"]);


                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);


                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                //ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                //ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                //ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                //ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                //ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);

                //ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                //ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                //ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                //ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                //ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                //ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                //ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                //ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                //ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                //ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                //ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.BranchName = Convert.ToString(dtRequests.Rows[0]["Branch_Name"]);
                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                //ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                //ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                //ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                //ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                //ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                //ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                //ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                //ViewBag.Department = Convert.ToString(dtRequests.Rows[0]["Department"]);
                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Actual_No_Floors_G = Convert.ToString(dtRequests.Rows[0]["Actual_No_Floors_G"]);
                //ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                //ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Government_Value = Convert.ToString(dtRequests.Rows[0]["Government_Value"]);


                //ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                //ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                //ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);


                ViewBag.AdjoiningRoad_Width1 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width1"]);
                ViewBag.AdjoiningRoad_Width2 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width2"]);
                ViewBag.AdjoiningRoad_Width3 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width3"]);
                ViewBag.AdjoiningRoad_Width4 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width4"]);
                ViewBag.Client_Name = Convert.ToString(dtRequests.Rows[0]["Client_Name"]);
                ViewBag.SeismicZone = Convert.ToString(dtRequests.Rows[0]["SeismicZone"]);
                ViewBag.FloodZone = Convert.ToString(dtRequests.Rows[0]["FloodZone"]);
                ViewBag.CycloneZone = Convert.ToString(dtRequests.Rows[0]["CycloneZone"]);
                ViewBag.CRZ = Convert.ToString(dtRequests.Rows[0]["CRZ"]);
                ViewBag.RERA_Registration_No = Convert.ToString(dtRequests.Rows[0]["RERA_Registration_No"]);
                ViewBag.distressValue = Convert.ToString(dtRequests.Rows[0]["distressValue"]);
                ViewBag.Gross_Estimated_Cost_Construction = Convert.ToString(dtRequests.Rows[0]["Gross_Estimated_Cost_Construction"]);
                ViewBag.Total_Land_Plot_Value = Convert.ToString(dtRequests.Rows[0]["Total_Land_Plot_Value"]);
                ViewBag.TotalFlatwords = Convert.ToString(dtRequests.Rows[0]["TotalFlatwords"]);

                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }
                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }

                if (dtrequestIdApprovedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);

                    //var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                }

                if (dtrequestIdConstructionDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_Constr = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);
                    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                }
                if (dtrequestIdSaleDeedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Authority"]);
                    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                }
                if (dtrequestIdCommencementDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Approval_Authority"]);

                    ViewBag.Date_Of_Approval_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Date_Of_Approval"]);
                }
                if (dtrequestIdSumOfApprovedBuiltUpArea.Rows.Count > 0)
                {
                    ViewBag.Total_Approved_Built_up_Area = Convert.ToString(dtrequestIdSumOfApprovedBuiltUpArea.Rows[0]["Approved_Built_up_Area"]);
                }


                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();


                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + ",";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                //ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                //ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                //ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                //ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                //ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Plot_area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Plot_area_for_Valuation#");

                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");
                ViewBag.Rate_range_in_Locality = getUnitPrice(dtpricedetails, "#Land_/_Plot_Rate_Range_in_the_Locality#");
                ViewBag.Cost_of_Construction = getUnitPrice(dtpricedetails, "#Cost_of_Construction#");
                ViewBag.Adopted_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");

                ViewBag.Any_Other_Charges = getUnitPrice(dtpricedetails, "#Any_Other_Charges#");



                //ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                //ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                //ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                //ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");
                //ViewBag.NoofBalconies = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Balconies");
                //ViewBag.NoofToilets = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Toilets");
                //ViewBag.NoofParking = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Parking");










            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //Edelweiss Finance Report For Bungalow
        public ActionResult EdelweissSingleUnit(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdSaleDeedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSaleDeedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdCommencementDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCommencementCertificateByReqIDForPDF", hInputPara);
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);
                DataTable dtrequestIdSumOfApprovedBuiltUpArea = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorWiseBreakupSumOfApproved_Built_up_AreaByRequestid", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);





                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                //ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                //ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);

                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                    var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                    ViewBag.Date_of_Inspection = dateOnlyInspection;
                }
                ViewBag.Valuationdate = Convert.ToString(dtRequests.Rows[0]["Date_of_Valuation"]);
                if (ViewBag.Valuationdate != "")
                {
                   // var dateTimeNowValue = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Valuation"]); // Return 00/00/0000 00:00:00
                  //  var dateOnlyValue = dateTimeNowValue.ToShortDateString();
                    ViewBag.Date_of_Valuation = ViewBag.Valuationdate;
                }

                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);


                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                ViewBag.Bunglow_Number = Convert.ToString(dtRequests.Rows[0]["Bunglow_Number"]);
                


                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                //ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                //ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);



                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);


                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                //ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                //ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                //ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                //ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                //ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);

                //ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                //ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                //ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                //ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                //ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                //ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                //ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                //ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                //ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                //ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                //ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.BranchName = Convert.ToString(dtRequests.Rows[0]["Branch_Name"]);
                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                //ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                //ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                //ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                //ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                //ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                //ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                //ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                //ViewBag.Department = Convert.ToString(dtRequests.Rows[0]["Department"]);
                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Actual_No_Floors_G = Convert.ToString(dtRequests.Rows[0]["Actual_No_Floors_G"]);
                //ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                //ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Government_Value = Convert.ToString(dtRequests.Rows[0]["Government_Value"]);


                //ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                //ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                //ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);


                ViewBag.AdjoiningRoad_Width1 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width1"]);
                ViewBag.AdjoiningRoad_Width2 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width2"]);
                ViewBag.AdjoiningRoad_Width3 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width3"]);
                ViewBag.AdjoiningRoad_Width4 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width4"]);
                ViewBag.Client_Name = Convert.ToString(dtRequests.Rows[0]["Client_Name"]);
                ViewBag.SeismicZone = Convert.ToString(dtRequests.Rows[0]["SeismicZone"]);
                ViewBag.FloodZone = Convert.ToString(dtRequests.Rows[0]["FloodZone"]);
                ViewBag.CycloneZone = Convert.ToString(dtRequests.Rows[0]["CycloneZone"]);
                ViewBag.CRZ = Convert.ToString(dtRequests.Rows[0]["CRZ"]);
                ViewBag.RERA_Registration_No = Convert.ToString(dtRequests.Rows[0]["RERA_Registration_No"]);
                ViewBag.distressValue = Convert.ToString(dtRequests.Rows[0]["distressValue"]);
                ViewBag.Gross_Estimated_Cost_Construction = Convert.ToString(dtRequests.Rows[0]["Gross_Estimated_Cost_Construction"]);
                ViewBag.Valueoftheplotbungalow = Convert.ToString(dtRequests.Rows[0]["Valueoftheplotbungalow"]);
                ViewBag.TotalvaluepropertyBunglow = Convert.ToString(dtRequests.Rows[0]["TotalvaluepropertyBunglow"]);
                ViewBag.StageOfConstructionPerct = Convert.ToString(dtRequests.Rows[0]["StageOfConstructionPerct"]);
                ViewBag.DisbursementRecommendPerct = Convert.ToString(dtRequests.Rows[0]["DisbursementRecommendPerct"]);
                ViewBag.TotalPlotwords = Convert.ToString(dtRequests.Rows[0]["TotalPlotwords"]);

                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }
                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }

                if (dtrequestIdApprovedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);

                    //var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                }

                if (dtrequestIdConstructionDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_Constr = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);
                    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                }
                if (dtrequestIdSaleDeedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Authority"]);
                    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                }
                if (dtrequestIdCommencementDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Approval_Authority"]);

                    ViewBag.Date_Of_Approval_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Date_Of_Approval"]);
                }
                if (dtrequestIdSumOfApprovedBuiltUpArea.Rows.Count > 0)
                {
                    ViewBag.Total_Approved_Built_up_Area = Convert.ToString(dtrequestIdSumOfApprovedBuiltUpArea.Rows[0]["Approved_Built_up_Area"]);
                }


                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.ival_ProjectFloorWiseBreakUps = GetFloorWiseBreakupWithTotalByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();


                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + ",";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                //ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                //ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                //ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                //ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                //ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                //ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                //ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");

                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");
                ViewBag.Rate_range_in_Locality = getUnitPrice(dtpricedetails, "#Land_/_Plot_Rate_Range_in_the_Locality#");
                ViewBag.Cost_of_Construction = getUnitPrice(dtpricedetails, "#Cost_of_Construction#");
                ViewBag.Adopted_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");

                ViewBag.Any_Other_Charges = getUnitPrice(dtpricedetails, "#Any_Other_Charges#");



                //ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                //ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                //ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                //ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");
                //ViewBag.NoofBalconies = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Balconies");
                //ViewBag.NoofToilets = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Toilets");
                //ViewBag.NoofParking = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Parking");










            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //HDFC LTD Report For Flat
        public ActionResult HDFCLTDMultiUnit(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);


                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                vm.ival_Document_Uploaded_ListS = getUploadDocumentByReqId();

                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);

                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    try
                    {
                        DateTime dti = DateTime.ParseExact(ViewBag.Inspectiondate.ToString(), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);

                        string Inspectiondate = dti.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                        //var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                        //var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                        ViewBag.Date_of_Inspection = Inspectiondate;
                    }
                    catch(Exception e)
                    {
                        string datestring1 = Convert.ToDateTime(ViewBag.Inspectiondate).ToString("dd/MM/yyyy");
                        ViewBag.Date_of_Inspection = datestring1;
                        //var format = "dd/MM/yyyy";
                        //var dateTime = DateTime.ParseExact(datestring1, format, CultureInfo.InvariantCulture);
                    }

                }
                ViewBag.Bank = dtRequests.Rows[0]["Client_Name"].ToString();
                ViewBag.ValuationType = dtRequests.Rows[0]["ValuationType"].ToString();
                
                ViewBag.Branch = dtRequests.Rows[0]["Branch_Name"].ToString();
                ViewBag.Balanced_Leased_Period = Convert.ToString(dtRequests.Rows[0]["Balanced_Leased_Period"]);
                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.TotalFlatwords = Convert.ToString(dtRequests.Rows[0]["TotalFlatwords"]);

                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);
                try
                {
                    DateTime dtr = DateTime.ParseExact(Convert.ToString(dtRequests.Rows[0]["Request_Date"]), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                    string Requestdate = dtr.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                    ViewBag.RequestDate = Requestdate;
                    //var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                    //var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                    //ViewBag.RequestDate = dateOnlyString;
                }
                catch(Exception e)
                {
                    string datestring1 = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]).ToString("dd/MM/yyyy");
                    ViewBag.RequestDate = datestring1;
                }
                


                
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);



                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);


                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);

                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);

                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);
                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);

                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);

                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);

                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);

                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);

                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                string Remark = ViewBag.Remarks;
                var count = Remark.Length;
                ViewBag.Count = count;
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);

                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);

                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.Government_Value = Convert.ToString(dtRequests.Rows[0]["Government_Value"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Road2 = Convert.ToString(dtRequests.Rows[0]["Road2"]);
                ViewBag.Road3 = Convert.ToString(dtRequests.Rows[0]["Road3"]);
                ViewBag.Road4 = Convert.ToString(dtRequests.Rows[0]["Road4"]);
                ViewBag.Actual_No_Floors_G = Convert.ToString(dtRequests.Rows[0]["Actual_No_Floors_G"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);



                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();

                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }
                else
                {
                    ViewBag.IGRImage = "NA.jpg";
                }
                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }

                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }




                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");
                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");
                ViewBag.Any_Other_Charges = getUnitPrice(dtpricedetails, "#Any_Other_Charges#");
                ViewBag.Rate_Range_for_similar_Properties_in_the_locality = getUnitPrice(dtpricedetails, "#Rate_Range_for_similar_Properties_in_the_locality#");
                if(dtrequestIdUnitParaValue.Rows.Count>0)
                {
                    ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                    ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                    ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                    ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");
                    ViewBag.NoofBalconies = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Balconies");
                    ViewBag.NoofToilets = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Toilets");
                    ViewBag.NoofParking = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Parking");

                }



            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //HDFC LTD Report For Plot
        public ActionResult HDFCLTDSingleUnitPlot(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);


                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                vm.ival_Document_Uploaded_ListS = getUploadDocumentByReqId();

                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    try
                    {
                        DateTime dti = DateTime.ParseExact(ViewBag.Inspectiondate.ToString(), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);

                        string Inspectiondate = dti.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                        
                        ViewBag.Date_of_Inspection = Inspectiondate;
                    }
                    catch (Exception e)
                    {
                        string datestring1 = Convert.ToDateTime(ViewBag.Inspectiondate).ToString("dd/MM/yyyy");
                        ViewBag.Date_of_Inspection = datestring1;
                        
                    }
                    
                }
                try
                {
                    DateTime dtr = DateTime.ParseExact(Convert.ToString(dtRequests.Rows[0]["Request_Date"]), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);

                    string Requestdate = dtr.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                    
                    ViewBag.RequestDate = Requestdate;

                }
                catch (Exception e)
                {
                    string datestring1 = Convert.ToDateTime(Convert.ToString(dtRequests.Rows[0]["Request_Date"])).ToString("dd/MM/yyyy");
                    ViewBag.RequestDate = datestring1;
                   
                }
                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.TotalPlotwords = Convert.ToString(dtRequests.Rows[0]["TotalPlotwords"]);

                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

                //DateTime dtr = DateTime.ParseExact(Convert.ToString(dtRequests.Rows[0]["Request_Date"]), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                //string Requestdate = dtr.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                //ViewBag.RequestDate = Requestdate;
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);



                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);
                ViewBag.PlotNumber = Convert.ToString(dtRequests.Rows[0]["Plot_Number"]);
                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);


                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);

                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);

                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);
                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);

                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);

                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);

                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);

                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);

                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);

                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.Government_Value = Convert.ToString(dtRequests.Rows[0]["Government_Value"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Road2 = Convert.ToString(dtRequests.Rows[0]["Road2"]);
                ViewBag.Road3 = Convert.ToString(dtRequests.Rows[0]["Road3"]);
                ViewBag.Road4 = Convert.ToString(dtRequests.Rows[0]["Road4"]);
                ViewBag.Value_of_Plot = Convert.ToString(dtRequests.Rows[0]["Value_of_Plot"]);
                ViewBag.Value_of_other_Plot = Convert.ToString(dtRequests.Rows[0]["Value_of_other_Plot"]);
                ViewBag.Total_Land_Plot_Value = Convert.ToString(dtRequests.Rows[0]["Total_Land_Plot_Value"]);
                ViewBag.Actual_No_Floors_G = Convert.ToString(dtRequests.Rows[0]["Actual_No_Floors_G"]);
                

                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();

                vm.Image_Uploaded_List = getImagesByReqId();

                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }
                else
                {
                    ViewBag.IGRImage = "NA.jpg";
                }

                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }
                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }




                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");
                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");
                ViewBag.Any_Other_Charges = getUnitPrice(dtpricedetails, "#Any_Other_Charges#");
                ViewBag.Land_Plot_Rate_Range_in_the_Locality = getUnitPrice(dtpricedetails, "#Land_/_Plot_Rate_Range_in_the_Locality#");
                ViewBag.Adopted_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");
                ViewBag.Adopted_Plot_area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Plot_area_for_Valuation#");
                ViewBag.Adopted_Other_Plot_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Other_Plot_Area_for_Valuation#");
                ViewBag.Adopted_Other_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Other_Plot_/_Land_Rate#");
                if (dtrequestIdUnitParaValue.Rows.Count > 0)
                {
                    ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                    ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                    ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                    ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");
                    ViewBag.NoofBalconies = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Balconies");
                    ViewBag.NoofToilets = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Toilets");
                    ViewBag.NoofParking = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Parking");

                }
                
            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //HDFC LTD Report for Bungalow
        public ActionResult HDFCLTDSingleUnit(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdFloorWiseSumOfConstruction = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorWiseBreakupSumOfConstructionByRequestid", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);
                DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);



                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                vm.ival_Document_Uploaded_ListS = getUploadDocumentByReqId();

                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.Bunglow_Number = Convert.ToString(dtRequests.Rows[0]["Bunglow_Number"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    try
                    {
                        DateTime dti = DateTime.ParseExact(ViewBag.Inspectiondate.ToString(), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);

                        string Inspectiondate = dti.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                      
                        ViewBag.Date_of_Inspection = Inspectiondate;
                    }
                    catch (Exception e)
                    {
                        string datestring1 = Convert.ToDateTime(ViewBag.Inspectiondate).ToString("dd/MM/yyyy");
                        ViewBag.Date_of_Inspection = datestring1;
                       
                    }
                }

                try
                {
                    DateTime dtr = DateTime.ParseExact(Convert.ToString(dtRequests.Rows[0]["Request_Date"]), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                    string Requestdate = dtr.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                    ViewBag.RequestDate = Requestdate;
                    
                }
                catch (Exception e)
                {
                    string datestring1 = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]).ToString("dd/MM/yyyy");
                    ViewBag.RequestDate = datestring1;
                }

                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                //ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

               
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);

                
                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);
               


                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);

                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);

                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);
                ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);

                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);

                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);

                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);

                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["TotalvaluepropertyBunglow"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);

                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
             
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);

                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.Government_Value = Convert.ToString(dtRequests.Rows[0]["Government_Value"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Road2 = Convert.ToString(dtRequests.Rows[0]["Road2"]);
                ViewBag.Road3 = Convert.ToString(dtRequests.Rows[0]["Road3"]);
                ViewBag.Road4 = Convert.ToString(dtRequests.Rows[0]["Road4"]);
                ViewBag.Value_of_Plot = Convert.ToString(dtRequests.Rows[0]["Totalofthebungalow"]);
                ViewBag.Actual_No_Floors_G = Convert.ToString(dtRequests.Rows[0]["Actual_No_Floors_G"]);

                ViewBag.PerWorkCompleted = Convert.ToString(dtRequests.Rows[0]["StageOfConstructionPerct"]);
                ViewBag.PerDisbursement = Convert.ToString(dtRequests.Rows[0]["DisbursementRecommendPerct"]);
                ViewBag.GovRate = Convert.ToString(dtRequests.Rows[0]["Government_Rate"]);

                ViewBag.StageOfConstructionBunglow = Convert.ToString(dtRequests.Rows[0]["StageOfConstructionBunglow"]);

                if (ViewBag.StageOfConstructionBunglow == "Ready")
                {
                    ViewBag.ToatalInWords = Convert.ToString(dtRequests.Rows[0]["TotalreadyBunglowWords"]);
                }
                else
                {
                    ViewBag.ToatalInWords = Convert.ToString(dtRequests.Rows[0]["TotalUCBunglowWords"]);
                }

                if (dtrequestIdFloorWiseSumOfConstruction.Rows.Count > 0)
                {
                    ViewBag.TotalConstructionCost = Convert.ToString(dtrequestIdFloorWiseSumOfConstruction.Rows[0]["Adopted_Cost_Construction"]);
                }



                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.ival_ProjectFloorWiseBreakUps = GetFloorWiseBreakupWithTotalByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();
                if (dtrequestIdIGRImage.Rows.Count > 0)
                {

                    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                }
                else
                {
                    ViewBag.IGRImage = "NA.jpg";
                }


                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }

                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }




                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                
                ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
               
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");
                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");
                ViewBag.Any_Other_Charges = getUnitPrice(dtpricedetails, "#Any_Other_Charges#");
                ViewBag.Land_Plot_Rate_Range_in_the_Locality = getUnitPrice(dtpricedetails, "#Land_/_Plot_Rate_Range_in_the_Locality#");
                ViewBag.Adopted_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");
                //ViewBag.Adopted_Plot_area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Plot_area_for_Valuation#");
                if(dtrequestIdUnitParaValue.Rows.Count>0)
                {
                    ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                    ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                    ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                    ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");
                    ViewBag.NoofBalconies = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Balconies");
                    ViewBag.NoofToilets = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Toilets");
                    ViewBag.NoofParking = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Parking");

                }




            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //IDFC First Bank Report For Flat
        public ActionResult IDFCFirstMultiUnit(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                //DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdCommencementDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCommencementCertificateByReqIDForPDF", hInputPara);
                //DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
                // DataTable dtrequestIdSaleDeedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSaleDeedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);






                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                //ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                //ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                    var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                    ViewBag.Date_of_Inspection = dateOnlyInspection;
                }
                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                

                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                //ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                //ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                //ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);



                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                //ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                //ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                //ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);


                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                //ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                //ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                //ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                //ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);

                //ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                //ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                //ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                //ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                //ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                //ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                //ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                //ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                //ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                //ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                //ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                //ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                //ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                //ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.BranchName = Convert.ToString(dtRequests.Rows[0]["Branch_Name"]);
                //ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                //ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                //ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                //ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                //ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                //ViewBag.Department = Convert.ToString(dtRequests.Rows[0]["Department"]);
                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                //ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                //ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                //ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);
                //ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Road2 = Convert.ToString(dtRequests.Rows[0]["Road2"]);
                ViewBag.Road3 = Convert.ToString(dtRequests.Rows[0]["Road3"]);
                ViewBag.Road4 = Convert.ToString(dtRequests.Rows[0]["Road4"]);
                ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);

                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }

                if (dtrequestIdApprovedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);

                    //var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                }

                if (dtrequestIdCommencementDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Approval_Authority"]);

                    ViewBag.Date_Of_Approval_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Date_Of_Approval"]);
                }

                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();


                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + " , ";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                //ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                //ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                ViewBag.Area_as_per_Agreement = getUnitPrice(dtpricedetails, "#Area_as_per_Agreement#");

                //ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                //ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");
                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");

                //ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");









            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //IDFC First Bank Report For Plot
        public ActionResult IDFCFirstSingleUnitPlot(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                //DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdCommencementDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCommencementCertificateByReqIDForPDF", hInputPara);
                DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
                //DataTable dtrequestIdSaleDeedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSaleDeedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);

                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);






                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                //ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                //ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.TotalPlotwords = Convert.ToString(dtRequests.Rows[0]["TotalPlotwords"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                    var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                    ViewBag.Date_of_Inspection = dateOnlyInspection;
                }
                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);


                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                
                ViewBag.Plot_Number = Convert.ToString(dtRequests.Rows[0]["Plot_Number"]);
                //ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                //ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                //ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);



                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                //ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                //ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                //ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);


                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                //ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                //ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                //ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                //ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);

                //ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                //ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                //ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                //ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                //ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                //ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                //ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                //ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                //ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                //ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                //ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);

                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                //ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                //ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                //ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                //ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.BranchName = Convert.ToString(dtRequests.Rows[0]["Branch_Name"]);
                //ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                //ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                //ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                //ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                //ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                //ViewBag.Department = Convert.ToString(dtRequests.Rows[0]["Department"]);
                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                //ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                //ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                //ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);
                //ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Road2 = Convert.ToString(dtRequests.Rows[0]["Road2"]);
                ViewBag.Road3 = Convert.ToString(dtRequests.Rows[0]["Road3"]);
                ViewBag.Road4 = Convert.ToString(dtRequests.Rows[0]["Road4"]);
                ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                ViewBag.Value_of_Plot = Convert.ToString(dtRequests.Rows[0]["Value_of_Plot"]);
                ViewBag.Total_Land_Plot_Value = Convert.ToString(dtRequests.Rows[0]["Total_Land_Plot_Value"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Distress_Value_of_Plot = Convert.ToString(dtRequests.Rows[0]["Distress_Value_of_Plot"]);
                ViewBag.Gross_Estimated_Cost_Construction = Convert.ToString(dtRequests.Rows[0]["Gross_Estimated_Cost_Construction"]);



                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }

                if (dtrequestIdApprovedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);

                    //var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                }

                if (dtrequestIdConstructionDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_Constr = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);
                    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                }
                if (dtrequestIdCommencementDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Approval_Authority"]);

                    ViewBag.Date_Of_Approval_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Date_Of_Approval"]);
                }

                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();


                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + " , ";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                //ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                //ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");

                ViewBag.Adopted_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");
                ViewBag.Adopted_Plot_area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Plot_area_for_Valuation#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                ViewBag.Area_as_per_Agreement = getUnitPrice(dtpricedetails, "#Area_as_per_Agreement#");
                ViewBag.Measured_Built_up_Area = getUnitPrice(dtpricedetails, "#Measured_Built-up_Area#");
                ViewBag.Approved_Built_up_Area = getUnitPrice(dtpricedetails, "#Approved_Built-up_Area#");
                ViewBag.Cost_of_Construction = getUnitPrice(dtpricedetails, "#Cost_of_Construction#");
                ViewBag.Any_Other_Charges = getUnitPrice(dtpricedetails, "#Any_Other_Charges#");



                //ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                //ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");
                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");

                //ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                //ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                //ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");









            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //IDFC First Bank Report For Bungalow
        public ActionResult IDFCFirstSingleUnit(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                //DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdCommencementDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCommencementCertificateByReqIDForPDF", hInputPara);
                DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
                //DataTable dtrequestIdSaleDeedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSaleDeedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);






                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                //ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                //ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);
                ViewBag.Inspectiondate = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                if (ViewBag.Inspectiondate != "")
                {
                    var dateTimeNowInspec = Convert.ToDateTime(dtRequests.Rows[0]["Date_of_Inspection"]); // Return 00/00/0000 00:00:00
                    var dateOnlyInspection = dateTimeNowInspec.ToShortDateString();
                    ViewBag.Date_of_Inspection = dateOnlyInspection;
                }

                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);


                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                

                ViewBag.Bunglow_Number = Convert.ToString(dtRequests.Rows[0]["Bunglow_Number"]);
                //ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                //ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                //ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);

                if (ViewBag.StageOfConstructionBunglow == "Ready")
                {
                    ViewBag.ToatalInWords = Convert.ToString(dtRequests.Rows[0]["TotalreadyBunglowWords"]);
                }
                else
                {
                    ViewBag.ToatalInWords = Convert.ToString(dtRequests.Rows[0]["TotalUCBunglowWords"]);
                }

                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                //ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                //ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                //ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);


                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                //ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                //ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                //ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                //ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);

                //ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                //ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                //ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                //ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                //ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                //ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                //ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                //ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                //ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                //ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                //ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);

                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                //ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                //ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                //ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                //ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.BranchName = Convert.ToString(dtRequests.Rows[0]["Branch_Name"]);
                //ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                //ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                //ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                //ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                //ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                //ViewBag.Department = Convert.ToString(dtRequests.Rows[0]["Department"]);
                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                //ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                //ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                //ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);
                //ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Road2 = Convert.ToString(dtRequests.Rows[0]["Road2"]);
                ViewBag.Road3 = Convert.ToString(dtRequests.Rows[0]["Road3"]);
                ViewBag.Road4 = Convert.ToString(dtRequests.Rows[0]["Road4"]);
                ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                
                ViewBag.Valueoftheplotbungalow = Convert.ToString(dtRequests.Rows[0]["Valueoftheplotbungalow"]);

                ViewBag.TotalvaluepropertyBunglow = Convert.ToString(dtRequests.Rows[0]["TotalvaluepropertyBunglow"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                
                ViewBag.DistressValueofPropertyBunglow = Convert.ToString(dtRequests.Rows[0]["DistressValueofPropertyBunglow"]);
                ViewBag.Gross_Estimated_Cost_Construction = Convert.ToString(dtRequests.Rows[0]["Gross_Estimated_Cost_Construction"]);



                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }

                if (dtrequestIdApprovedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);

                    //var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                }

                if (dtrequestIdConstructionDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_Constr = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);
                    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                }
                if (dtrequestIdCommencementDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Approval_Authority"]);

                    ViewBag.Date_Of_Approval_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Date_Of_Approval"]);
                }

                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.ival_ProjectFloorWiseBreakUps = GetFloorWiseBreakupWithTotalByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();


                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + " , ";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                //ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                //ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");

                ViewBag.Adopted_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");
                ViewBag.Adopted_Plot_area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Plot_area_for_Valuation#");
                ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                ViewBag.Area_as_per_Agreement = getUnitPrice(dtpricedetails, "#Area_as_per_Agreement#");
                ViewBag.Measured_Built_up_Area = getUnitPrice(dtpricedetails, "#Measured_Built-up_Area#");
                ViewBag.Approved_Built_up_Area = getUnitPrice(dtpricedetails, "#Approved_Built-up_Area#");
                ViewBag.Cost_of_Construction = getUnitPrice(dtpricedetails, "#Cost_of_Construction#");
                ViewBag.Any_Other_Charges = getUnitPrice(dtpricedetails, "#Any_Other_Charges#");



                //ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                //ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");
                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");

                //ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                //ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                //ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");









            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //Bajaj Finance For Flat
        public ActionResult BajajMultiUnit(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);

                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                //ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                //ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);


                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);
                ////ViewBag.RequestDate = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);

                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                ViewBag.UnitNumber = Convert.ToString(dtRequests.Rows[0]["UnitNumber"]);
                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                //ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                //ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);



                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                //ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                //ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);

                //ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                //ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                //ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);


                //ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                //ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                //ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                //ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);

                //ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                //ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);

                //ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                //ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                //ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);
                //ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);

                //ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);

                //ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                //ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                //ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);

                ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                //ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                //ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                //ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);

                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                //ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                //ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                //ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                //ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);

                //ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                //ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                //ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                //ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                //ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                //ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                
                    ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);

                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.SeismicZone = Convert.ToString(dtRequests.Rows[0]["SeismicZone"]);
                ViewBag.FloodZone = Convert.ToString(dtRequests.Rows[0]["FloodZone"]);
                ViewBag.CRZ = Convert.ToString(dtRequests.Rows[0]["CRZ"]);
                



                //ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                //ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                //ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                //ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);
                //ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                //ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.Government_Value = Convert.ToString(dtRequests.Rows[0]["Government_Value"]);
                //ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                //ViewBag.AdjoiningRoad_Width1 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width1"]);
                //ViewBag.AdjoiningRoad_Width2 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width2"]);
                //ViewBag.AdjoiningRoad_Width3 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width3"]);
                //ViewBag.AdjoiningRoad_Width4 = Convert.ToString(dtRequests.Rows[0]["AdjoiningRoad_Width4"]);




                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();

                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }
                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }
                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + " , ";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }
                if (dtrequestIdApprovedDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);

                    var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                    var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                    ViewBag.Date_Of_Approval = dateOnlyString1;
                }




               ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                //ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                //ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                //ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                //ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                //ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");
                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");
                //ViewBag.Any_Other_Charges = getUnitPrice(dtpricedetails, "#Any_Other_Charges#");
                //ViewBag.Rate_Range_for_similar_Properties_in_the_locality = getUnitPrice(dtpricedetails, "#Rate_Range_for_similar_Properties_in_the_locality#");




            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //Bajaj Finance For Plot
        public ActionResult BajajSingleUnitPlot(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                //DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                //DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                //DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
                //DataTable dtrequestIdSaleDeedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSaleDeedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdCommencementDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCommencementCertificateByReqIDForPDF", hInputPara);
                //DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);
                //DataTable dtrequestIdSumOfApprovedBuiltUpArea = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorWiseBreakupSumOfApproved_Built_up_AreaByRequestid", hInputPara);

                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);




                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                //ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                //ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);


                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);


                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                ViewBag.Plot_Number = Convert.ToString(dtRequests.Rows[0]["Plot_Number"]);

                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                //ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                //ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);
                ViewBag.TotalPlotwords = Convert.ToString(dtRequests.Rows[0]["TotalPlotwords"]);


                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);


                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                //ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                //ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                //ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                //ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                //ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);

                //ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                //ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                //ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                //ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                //ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                //ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                //ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                //ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                //ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                //ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                //ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.BranchName = Convert.ToString(dtRequests.Rows[0]["Branch_Name"]);
                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                //ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                //ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                //ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                //ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                //ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                //ViewBag.Department = Convert.ToString(dtRequests.Rows[0]["Department"]);
                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Actual_No_Floors_G = Convert.ToString(dtRequests.Rows[0]["Actual_No_Floors_G"]);
                //ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                //ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Government_Value = Convert.ToString(dtRequests.Rows[0]["Government_Value"]);


                //ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                //ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                //ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);

                ViewBag.SeismicZone = Convert.ToString(dtRequests.Rows[0]["SeismicZone"]);
                ViewBag.FloodZone = Convert.ToString(dtRequests.Rows[0]["FloodZone"]);
                ViewBag.CRZ = Convert.ToString(dtRequests.Rows[0]["CRZ"]);

                ViewBag.distressValue = Convert.ToString(dtRequests.Rows[0]["distressValue"]);
                ViewBag.Gross_Estimated_Cost_Construction = Convert.ToString(dtRequests.Rows[0]["Gross_Estimated_Cost_Construction"]);
                ViewBag.Total_Land_Plot_Value = Convert.ToString(dtRequests.Rows[0]["Total_Land_Plot_Value"]);

                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }

                //if (dtrequestIdIGRImage.Rows.Count > 0)
                //{

                //    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                //}

                //if (dtrequestIdApprovedDoc.Rows.Count > 0)
                //{
                //    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);
                //    ViewBag.Approval_Authority = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);

                //    //var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                //    //var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                //    ViewBag.Date_Of_Approval = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                //}

                //if (dtrequestIdConstructionDoc.Rows.Count > 0)
                //{
                //    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);
                //    ViewBag.Approval_Authority_Constr = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);
                //    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                //    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                //    ViewBag.Date_Of_Approval_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                //}
                //if (dtrequestIdSaleDeedDoc.Rows.Count > 0)
                //{
                //    ViewBag.Approval_Number_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Number"]);
                //    ViewBag.Approval_Authority_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Authority"]);
                //    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                //    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                //    ViewBag.Date_Of_Approval_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                //}
                if (dtrequestIdCommencementDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Approval_Authority"]);

                    ViewBag.Date_Of_Approval_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Date_Of_Approval"]);
                }
                //if (dtrequestIdSumOfApprovedBuiltUpArea.Rows.Count > 0)
                //{
                //    ViewBag.Total_Approved_Built_up_Area = Convert.ToString(dtrequestIdSumOfApprovedBuiltUpArea.Rows[0]["Approved_Built_up_Area"]);
                //}


                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                //vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();


                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + ",";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                //ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                //ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                //ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                //ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                //ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Plot_area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Plot_area_for_Valuation#");

                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");
                ViewBag.Rate_range_in_Locality = getUnitPrice(dtpricedetails, "#Land_/_Plot_Rate_Range_in_the_Locality#");
                ViewBag.Cost_of_Construction = getUnitPrice(dtpricedetails, "#Cost_of_Construction#");
                ViewBag.Adopted_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");

                ViewBag.Any_Other_Charges = getUnitPrice(dtpricedetails, "#Any_Other_Charges#");



                //ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                //ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                //ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                //ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");
                //ViewBag.NoofBalconies = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Balconies");
                //ViewBag.NoofToilets = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Toilets");
                //ViewBag.NoofParking = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Parking");


            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //Bajaj Finance For Bungalow
        public ActionResult BajajSingleUnit(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewBag.RequestId = ID;
            ViewModelRequest vm = new ViewModelRequest();


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@ID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);
                DataTable dtrequestIdBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
                DataTable dtrequestIdLocalTransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqIDForPdf", hInputPara);
                //DataTable dtrequestIdIGRImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqIDForPdf", hInputPara);
                DataTable dtrequestIdDocList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);
                //DataTable dtrequestIdApprovedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDocumentByReqIDForPDF", hInputPara);
                //DataTable dtrequestIdConstructionDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetConstructionCompletionDocumentByReqIDForPDF", hInputPara);
                //DataTable dtrequestIdSaleDeedDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSaleDeedDocumentByReqIDForPDF", hInputPara);
                DataTable dtrequestIdCommencementDoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCommencementCertificateByReqIDForPDF", hInputPara);
                //DataTable dtrequestIdUnitParaValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterValueNewByRequestid", hInputPara);
                //DataTable dtrequestIdSumOfApprovedBuiltUpArea = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorWiseBreakupSumOfApproved_Built_up_AreaByRequestid", hInputPara);
                DataTable dtrequestIdGoogleMapImage = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesGoogleMapByReqIDForPdf", hInputPara);





                TempData["data"] = dtRequests;

                ViewBag.data = TempData["data"];
                DataTable dt = ViewBag.data;

                string UnitType;
                UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
                string PType = dt.Rows[0]["Property_Type"].ToString();

                // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
                string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


                hInputPara.Add("@PType", PType);
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);



                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);

                //ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                //ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Name = Convert.ToString(dtRequests.Rows[0]["Name"]);
                ViewBag.P_Contact_Num = Convert.ToString(dtRequests.Rows[0]["P_Contact_Num"]);


                ViewBag.TransactionType = Convert.ToString(dtRequests.Rows[0]["Loan_Type"]);


                var dateTimeNow = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]); // Return 00/00/0000 00:00:00
                var dateOnlyString = dateTimeNow.ToShortDateString(); //Return 00/00/0000
                ViewBag.RequestDate = dateOnlyString;
                ViewBag.Bunglow_Number = Convert.ToString(dtRequests.Rows[0]["Bunglow_Number"]);
                

                ViewBag.Building_Name_RI = Convert.ToString(dtRequests.Rows[0]["Building_Name_RI"]);
                ViewBag.City = Convert.ToString(dtRequests.Rows[0]["City"]);
                //ViewBag.StatusOfProperty = Convert.ToString(dtRequests.Rows[0]["StatusOfProperty"]);
                ViewBag.Property_Type = Convert.ToString(dtRequests.Rows[0]["Property_Type"]);
                ViewBag.State = Convert.ToString(dtRequests.Rows[0]["State"]);
                ViewBag.Street = Convert.ToString(dtRequests.Rows[0]["Street"]);
                ViewBag.Occupancy_Details = Convert.ToString(dtRequests.Rows[0]["Occupancy_Details"]);
                //ViewBag.Occupied_Since = Convert.ToString(dtRequests.Rows[0]["Occupied_Since"]);



                ViewBag.Locality = Convert.ToString(dtRequests.Rows[0]["Locality"]);
                ViewBag.Sub_Locality = Convert.ToString(dtRequests.Rows[0]["Sub_Locality"]);
                ViewBag.PinCode = Convert.ToString(dtRequests.Rows[0]["PinCode"]);
                ViewBag.NearBy_Landmark = Convert.ToString(dtRequests.Rows[0]["NearBy_Landmark"]);
                ViewBag.Type_of_Locality = Convert.ToString(dtRequests.Rows[0]["Type_of_Locality"]);
                ViewBag.Actual_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Actual_Usage_Of_Property"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.Plot_Demarcated_at_site = Convert.ToString(dtRequests.Rows[0]["Plot_Demarcated_at_site"]);
                ViewBag.Property_Identified_PName = Convert.ToString(dtRequests.Rows[0]["Property_Identified_PName"]);

                ViewBag.Type_Of_Structure = Convert.ToString(dtRequests.Rows[0]["Type_Of_Structure"]);

                ViewBag.Quality_Of_Construction = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Construction"]);
                ViewBag.Rough_Plaster = Convert.ToString(dtRequests.Rows[0]["Rough_Plaster"]);
                ViewBag.Tiling = Convert.ToString(dtRequests.Rows[0]["Tiling"]);

                if (ViewBag.StageOfConstructionBunglow == "Ready")
                {
                    ViewBag.ToatalInWords = Convert.ToString(dtRequests.Rows[0]["TotalreadyBunglowWords"]);
                }
                else
                {
                    ViewBag.ToatalInWords = Convert.ToString(dtRequests.Rows[0]["TotalUCBunglowWords"]);
                }
                ViewBag.Type_of_Flooring = Convert.ToString(dtRequests.Rows[0]["Type_of_Flooring"]);
                //ViewBag.Wall_Finish = Convert.ToString(dtRequests.Rows[0]["Wall_Finish"]);
                //ViewBag.Plumbing_Fitting = Convert.ToString(dtRequests.Rows[0]["Plumbing_Fitting"]);
                //ViewBag.Electrical_Fittings = Convert.ToString(dtRequests.Rows[0]["Electrical_Fittings"]);


                ViewBag.Current_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Age_Of_Property"]);
                ViewBag.Residual_Age_Of_Property = Convert.ToString(dtRequests.Rows[0]["Residual_Age_Of_Property"]);
                ViewBag.Cord_Latitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Latitude1"]);
                ViewBag.Cord_Longitude1 = Convert.ToString(dtRequests.Rows[0]["Cord_Longitude1"]);
                //ViewBag.Amenities = Convert.ToString(dtRequests.Rows[0]["Amenities"]);
                ViewBag.Class_of_Locality = Convert.ToString(dtRequests.Rows[0]["Class_of_Locality"]);
                ViewBag.Road_Finish = Convert.ToString(dtRequests.Rows[0]["Road_Finish"]);
                //ViewBag.Road1 = Convert.ToString(dtRequests.Rows[0]["Road1"]);
                ViewBag.Nearest_BusStop = Convert.ToString(dtRequests.Rows[0]["Nearest_BusStop"]);
                ViewBag.Nearest_RailwayStation = Convert.ToString(dtRequests.Rows[0]["Nearest_RailwayStation"]);
                ViewBag.Nearest_Market = Convert.ToString(dtRequests.Rows[0]["Nearest_Market"]);

                //ViewBag.Nearest_Hospital = Convert.ToString(dtRequests.Rows[0]["Nearest_Hospital"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                //ViewBag.Common_Areas = Convert.ToString(dtRequests.Rows[0]["Common_Areas"]);
                ViewBag.Facilities = Convert.ToString(dtRequests.Rows[0]["Facilities"]);
                //ViewBag.Anyother_Observation = Convert.ToString(dtRequests.Rows[0]["Anyother_Observation"]);
                ViewBag.Roofing_Terracing = Convert.ToString(dtRequests.Rows[0]["Roofing_Terracing"]);
                //ViewBag.Quality_Of_Fixture = Convert.ToString(dtRequests.Rows[0]["Quality_Of_Fixture"]);
                //ViewBag.No_Of_Lifts = Convert.ToString(dtRequests.Rows[0]["No_Of_Lifts"]);
                //ViewBag.Project_Name = Convert.ToString(dtRequests.Rows[0]["Project_Name"]);
                ViewBag.Wing_Name_RI = Convert.ToString(dtRequests.Rows[0]["Wing_Name_RI"]);
                //ViewBag.Survey_Number = Convert.ToString(dtRequests.Rows[0]["Survey_Number"]);
                ViewBag.Village_Name = Convert.ToString(dtRequests.Rows[0]["Village_Name"]);
                ViewBag.District = Convert.ToString(dtRequests.Rows[0]["District"]);
                //ViewBag.Renting_Potential = Convert.ToString(dtRequests.Rows[0]["Renting_Potential"]);
                ViewBag.Monthly_Rental = Convert.ToString(dtRequests.Rows[0]["Monthly_Rental"]);
                ViewBag.Approved_No_of_Floors = Convert.ToString(dtRequests.Rows[0]["Approved_No_of_Floors"]);
                ViewBag.Total_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Total_Value_of_Property"]);
                ViewBag.Marketability_Of_Property = Convert.ToString(dtRequests.Rows[0]["Marketability_Of_Property"]);
                ViewBag.Risk_of_Demolition = Convert.ToString(dtRequests.Rows[0]["Risk_of_Demolition"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks1"]);
                //ViewBag.ValuationOfResult = Convert.ToString(dtRequests.Rows[0]["ValuationResult"]);
                //ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description1"]);
                //ViewBag.Construction_as_per_plan = Convert.ToString(dtRequests.Rows[0]["Construction_as_per_plan"]);
                ViewBag.BranchName = Convert.ToString(dtRequests.Rows[0]["Branch_Name"]);
                ViewBag.SiteEngineer = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                //ViewBag.StageOfConstruction = Convert.ToString(dtRequests.Rows[0]["StageOfConstruction"]);
                ViewBag.Type_of_Property_Zone = Convert.ToString(dtRequests.Rows[0]["Type_of_Property_Zone"]);
                //ViewBag.DistFrom_City_Center = Convert.ToString(dtRequests.Rows[0]["DistFrom_City_Center"]);
                //ViewBag.Property_Description = Convert.ToString(dtRequests.Rows[0]["Property_Description"]);
                //ViewBag.Name_Of_Occupant = Convert.ToString(dtRequests.Rows[0]["Name_Of_Occupant"]);
                ViewBag.No_of_Floors = Convert.ToString(dtRequests.Rows[0]["No_of_Floors"]);
                ViewBag.Floor_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Floor_Of_Unit"]);
                //ViewBag.No_of_Units = Convert.ToString(dtRequests.Rows[0]["No_of_Units"]);
                ViewBag.Type_Of_Ownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                //ViewBag.Approved_Usage_Of_Property = Convert.ToString(dtRequests.Rows[0]["Approved_Usage_Of_Property"]);
                //ViewBag.Department = Convert.ToString(dtRequests.Rows[0]["Department"]);
                ViewBag.Boundaries_Matching = Convert.ToString(dtRequests.Rows[0]["Boundaries_Matching"]);
                ViewBag.Actual_No_Floors_G = Convert.ToString(dtRequests.Rows[0]["Actual_No_Floors_G"]);
                //ViewBag.MeasuredSaleableArea = Convert.ToString(dtRequests.Rows[0]["MeasuredSaleableArea"]);
                //ViewBag.ApprovedSaleableArea = Convert.ToString(dtRequests.Rows[0]["ApprovedSaleableArea"]);
                ViewBag.Current_Value_of_Property = Convert.ToString(dtRequests.Rows[0]["Current_Value_of_Property"]);
                ViewBag.Government_Value = Convert.ToString(dtRequests.Rows[0]["Government_Value"]);


                //ViewBag.Maintenance_Of_Property = Convert.ToString(dtRequests.Rows[0]["Maintenance_Of_Property"]);
                //ViewBag.Customer_Name_On_Board = Convert.ToString(dtRequests.Rows[0]["Customer_Name_On_Board"]);
                //ViewBag.Society_OR_Building_Name = Convert.ToString(dtRequests.Rows[0]["Society_OR_Building_Name"]);

                ViewBag.SeismicZone = Convert.ToString(dtRequests.Rows[0]["SeismicZone"]);
                ViewBag.FloodZone = Convert.ToString(dtRequests.Rows[0]["FloodZone"]);
                ViewBag.CRZ = Convert.ToString(dtRequests.Rows[0]["CRZ"]);

                ViewBag.distressValue = Convert.ToString(dtRequests.Rows[0]["distressValue"]);
                ViewBag.Gross_Estimated_Cost_Construction = Convert.ToString(dtRequests.Rows[0]["Gross_Estimated_Cost_Construction"]);
                ViewBag.TotalvaluepropertyBunglow = Convert.ToString(dtRequests.Rows[0]["TotalvaluepropertyBunglow"]);
                ViewBag.Work_Completed_Perc = Convert.ToString(dtRequests.Rows[0]["Work_Completed_Perc"]);
                ViewBag.DisbursementRecommendPerct = Convert.ToString(dtRequests.Rows[0]["DisbursementRecommendPerct"]);

                if (dtrequestIdGoogleMapImage.Rows.Count > 0)
                {

                    ViewBag.GoogleMapImage = Convert.ToString(dtrequestIdGoogleMapImage.Rows[0]["Image_Path"]);
                }
                //if (dtrequestIdIGRImage.Rows.Count > 0)
                //{

                //    ViewBag.IGRImage = Convert.ToString(dtrequestIdIGRImage.Rows[0]["Image_Path"]);
                //}

                //if (dtrequestIdApprovedDoc.Rows.Count > 0)
                //{
                //    ViewBag.Approval_Number = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Number"]);
                //    ViewBag.Approval_Authority = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);

                //    //var dateTimeNow1 = Convert.ToDateTime(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                //    //var dateOnlyString1 = dateTimeNow1.ToShortDateString(); //Return 00/00/0000
                //    ViewBag.Date_Of_Approval = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Date_Of_Approval"]);
                //}

                //if (dtrequestIdConstructionDoc.Rows.Count > 0)
                //{
                //    ViewBag.Approval_Number_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Approval_Number"]);
                //    ViewBag.Approval_Authority_Constr = Convert.ToString(dtrequestIdApprovedDoc.Rows[0]["Approval_Authority"]);
                //    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                //    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                //    ViewBag.Date_Of_Approval_Constr = Convert.ToString(dtrequestIdConstructionDoc.Rows[0]["Date_Of_Approval"]);
                //}
                //if (dtrequestIdSaleDeedDoc.Rows.Count > 0)
                //{
                //    ViewBag.Approval_Number_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Number"]);
                //    ViewBag.Approval_Authority_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Approval_Authority"]);
                //    //var dateTimeNow2 = Convert.ToDateTime(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                //    //var dateOnlyString2 = dateTimeNow2.ToShortDateString(); //Return 00/00/0000
                //    ViewBag.Date_Of_Approval_SaleDeed = Convert.ToString(dtrequestIdSaleDeedDoc.Rows[0]["Date_Of_Approval"]);
                //}
                if (dtrequestIdCommencementDoc.Rows.Count > 0)
                {
                    ViewBag.Approval_Number_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Approval_Number"]);
                    ViewBag.Approval_Authority_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Approval_Authority"]);

                    ViewBag.Date_Of_Approval_Commence = Convert.ToString(dtrequestIdCommencementDoc.Rows[0]["Date_Of_Approval"]);
                }
                //if (dtrequestIdSumOfApprovedBuiltUpArea.Rows.Count > 0)
                //{
                //    ViewBag.Total_Approved_Built_up_Area = Convert.ToString(dtrequestIdSumOfApprovedBuiltUpArea.Rows[0]["Approved_Built_up_Area"]);
                //}


                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                //vm.ival_ProjectSideMargins = GetSidearginsByReqID();
                vm.Image_Uploaded_List = getImagesByReqId();


                if (dtrequestIdBasicInfra.Rows.Count > 0)
                {
                    string str = "";
                    for (int u = 0; u < dtrequestIdBasicInfra.Rows.Count; u++)
                    {
                        string basic = dtrequestIdBasicInfra.Rows[u]["BasicInfra_type"].ToString();
                        str += basic + ",";

                    }
                    ViewBag.BasicInfra = str.Substring(0, str.Length - 1);
                }

                if (dtrequestIdLocalTransport.Rows.Count > 0)
                {
                    string str1 = "";
                    for (int u = 0; u < dtrequestIdLocalTransport.Rows.Count; u++)
                    {
                        string Loacal = dtrequestIdLocalTransport.Rows[u]["LocalTransport_ID"].ToString();
                        str1 += Loacal + ",";

                    }
                    ViewBag.LocalTransport = str1.Substring(0, str1.Length - 1);
                }


                if (dtrequestIdDocList.Rows.Count > 0)
                {
                    string str2 = "";
                    for (int u = 0; u < dtrequestIdDocList.Rows.Count; u++)
                    {
                        string Doc = dtrequestIdDocList.Rows[u]["Document_Name"].ToString();
                        str2 += Doc + ",";

                    }
                    ViewBag.DocumentName = str2.Substring(0, str2.Length - 1);
                }


                //ViewBag.LoadingPer = getUnitPrice(dtpricedetails, "#Loading_%#");
                //ViewBag.ApprovedcarpetArea = getUnitPrice(dtpricedetails, "#Approved_Carpet_Area#");
                ViewBag.GovRate = getUnitPrice(dtpricedetails, "#Government_Rate#");
                //ViewBag.DistressValuePer = getUnitPrice(dtpricedetails, "#Distress_Value_%#");
                //ViewBag.DistressValueProperty = getUnitPrice(dtpricedetails, "#Distress_Value_of_the_Property#");
                //ViewBag.MeasuredCarpetArea = getUnitPrice(dtpricedetails, "#Measured_Carpet_Area#");
                ViewBag.PerWorkCompleted = getUnitPrice(dtpricedetails, "#%_Work_Completed#");
                ViewBag.PerDisbursement = getUnitPrice(dtpricedetails, "#%_Disbursement_recommended#");
                ViewBag.Adopted_Area_for_Valuation = getUnitPrice(dtpricedetails, "#Adopted_Area_for_Valuation#");

                ViewBag.Adopted_Rate_for_the_Unit = getUnitPrice(dtpricedetails, "#Adopted_Rate_for_the_Unit#");
                ViewBag.Rate_range_in_Locality = getUnitPrice(dtpricedetails, "#Land_/_Plot_Rate_Range_in_the_Locality#");
                ViewBag.Cost_of_Construction = getUnitPrice(dtpricedetails, "#Cost_of_Construction#");
                ViewBag.Adopted_Plot_Land_Rate = getUnitPrice(dtpricedetails, "#Adopted_Plot_/_Land_Rate#");

                ViewBag.Any_Other_Charges = getUnitPrice(dtpricedetails, "#Any_Other_Charges#");



                //ViewBag.NoOfFloors = getUnitParameterValue(dtrequestIdUnitParaValue, "Floor of the Unit");
                //ViewBag.NoOfBedroms = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Bedrooms");
                //ViewBag.NoOfKitchens = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Kitchen");
                //ViewBag.NoOfHalls = getUnitParameterValue(dtrequestIdUnitParaValue, "Number of Halls");
                //ViewBag.NoofBalconies = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Balconies");
                //ViewBag.NoofToilets = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Toilets");
                //ViewBag.NoofParking = getUnitParameterValue(dtrequestIdUnitParaValue, "No of Parking");


            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);
        }

        //get Unit Price
        private string getUnitPrice(DataTable dtpricedetails, string name) {
            foreach (DataRow row in dtpricedetails.Select("LableName= '"+name+"'"))
             {
                return  row.Field<string>("UnitPriceParameter_Value");
            }
            return "";
            
        }


        //get Unit parameter value
        private string getUnitParameterValue(DataTable dtrequestIdUnitParaValue, string name)
        {
            foreach (DataRow row in dtrequestIdUnitParaValue.Select("Name1= '" + name + "'"))
            {
                return row.Field<string>("UnitParameter_Value");
                

            }
            return "";

        }
        [HttpPost]
        public List<ival_ProjectFloorWiseBreakUp> GetFloorwisebreakbyreqID()
        {
            List<ival_ProjectFloorWiseBreakUp> Listfloors = new List<ival_ProjectFloorWiseBreakUp>();
            try
            {
              
                hInputPara = new Hashtable();
                hInputPara1 = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                int RequestID = Convert.ToInt32(Session["RequestID"]);
                hInputPara.Add("@Request_ID", RequestID);
                DataTable dtprojectid = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprjIDByRequestID", hInputPara);
                hInputPara1.Add("@Project_ID", dtprojectid.Rows[0]["Project_ID"].ToString());
                DataTable dtfloor = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorwisebreakbyReqID", hInputPara1);

                for (int i = 0; i < dtfloor.Rows.Count; i++)
                {

                    Listfloors.Add(new ival_ProjectFloorWiseBreakUp
                    {
                        PFloor_ID = Convert.ToInt32(dtfloor.Rows[i]["PFloor_ID"]),
                        Floor_Type = Convert.ToString(dtfloor.Rows[i]["Floor_Type"]),
                        Approved_Built_up_Area = Convert.ToString(dtfloor.Rows[i]["Approved_Built_up_Area"]),
                        Approved_Carpet_Area = Convert.ToString(dtfloor.Rows[i]["Approved_Carpet_Area"]),
                        Measured_Built_up_Area = Convert.ToString(dtfloor.Rows[i]["Measured_Built_up_Area"]),
                        Measured_Carpet_Area = Convert.ToString(dtfloor.Rows[i]["Measured_Carpet_Area"]),
                        Adopted_Area_for_Valuation = Convert.ToString(dtfloor.Rows[i]["Adopted_Area_for_Valuation"]),
                        Adopted_Cost_Construction = Convert.ToString(dtfloor.Rows[i]["Adopted_Cost_Construction"])

                    });

                }
              
            }
            catch (Exception ex)
            {
            }
            return Listfloors;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetFloorType()
        {
            List<ival_LookupCategoryValues> ListFloorType = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtFloorType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorType");
            for (int i = 0; i < dtFloorType.Rows.Count; i++)
            {

                ListFloorType.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtFloorType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtFloorType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtFloorType.Rows[i]["Lookup_Value"])

                });

            }
            return ListFloorType;
        }
        [HttpPost]
        public List<ival_ProjectBoundaries> GetBoundariesByReqID()
        {
            List<ival_ProjectBoundaries> ListBoundaries = new List<ival_ProjectBoundaries>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtBoundaries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBoudriesByReqID", hInputPara);
            int l =1;
            int j = dtBoundaries.Rows.Count;
            int t = l - j;
            if (t <= l)
            {
                //  DataRow row = dtImages.NewRow();
                for (int m = 0; m < t; m++)
                {
                    DataRow row = dtBoundaries.NewRow();
                    row["Boundry_ID"] = Convert.ToInt32(0);
                    row["Boundry_Name"] = "";
                    row["AsPer_Document"] = "";
                    row["AsPer_Site"] = "";
                    row["Plot_Dimentions"] = "";
                    dtBoundaries.Rows.Add(row);

                    //dtImages.Rows.Add(new Object { Convert.ToInt32("0"), "NA" });
                }
            }



            for (int i = 0; i < dtBoundaries.Rows.Count; i++)
            {

                ListBoundaries.Add(new ival_ProjectBoundaries
                {
                    Boundry_ID = Convert.ToInt32(dtBoundaries.Rows[i]["Boundry_ID"]),
                    Boundry_Name = Convert.ToString(dtBoundaries.Rows[i]["Boundry_Name"]),
                    AsPer_Document = Convert.ToString(dtBoundaries.Rows[i]["AsPer_Document"]),
                    AsPer_Site = Convert.ToString(dtBoundaries.Rows[i]["AsPer_Site"]),
                    Plot_Dimentions = Convert.ToString(dtBoundaries.Rows[i]["Plot_Dimentions"]),



                });

            }
            return ListBoundaries;
        }

        [HttpPost]
        public List<ival_ProjectSideMargin> GetSidearginsByReqID()
        {
            List<ival_ProjectSideMargin> ListSideMargins = new List<ival_ProjectSideMargin>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtsidemargin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSideMarginByReqID", hInputPara);
            int l = 1;
            int j = dtsidemargin.Rows.Count;
            int t = l - j;
            if (t <= l)
            {
                //  DataRow row = dtImages.NewRow();
                for (int m = 0; m < t; m++)
                {
                    DataRow row = dtsidemargin.NewRow();
                    row["Side_Margin_ID"] = Convert.ToInt32(0);
                    row["Side_Margin_Description"] = "";
                    row["As_per_Approved"] = "";
                    row["As_per_Approvals"] = "";
                    row["As_per_Local_Bye_Laws"] = "";

                    row["As_Actuals"] = "";
                    row["Deviation_No"] = "";
                    row["Percentage_Deviation"] = "";
                    row["Floor_Of_Unit"] = "";
                    dtsidemargin.Rows.Add(row);

                    //dtImages.Rows.Add(new Object { Convert.ToInt32("0"), "NA" });
                }
            }
            for (int i = 0; i < dtsidemargin.Rows.Count; i++)
            {

                ListSideMargins.Add(new ival_ProjectSideMargin
                {
                    Side_Margin_ID = Convert.ToInt32(dtsidemargin.Rows[i]["Side_Margin_ID"]),
                    Side_Margin_Description = Convert.ToString(dtsidemargin.Rows[i]["Side_Margin_Description"]),
                    As_per_Approved = Convert.ToString(dtsidemargin.Rows[i]["As_per_Approved"]),
                    As_per_Measurement = Convert.ToString(dtsidemargin.Rows[i]["As_per_Measurement"]),
                    As_per_Approvals = Convert.ToString(dtsidemargin.Rows[i]["As_per_Approvals"]),
                    As_per_Local_Bye_Laws = Convert.ToString(dtsidemargin.Rows[i]["As_per_Local_Bye_Laws"]),
                    As_Actuals = Convert.ToString(dtsidemargin.Rows[i]["As_Actuals"]),
                    Deviation_No = Convert.ToString(dtsidemargin.Rows[i]["Deviation_No"]),
                    Percentage_Deviation = Convert.ToString(dtsidemargin.Rows[i]["Percentage_Deviation"]),
                    Floor_Of_Unit= Convert.ToString(dtsidemargin.Rows[i]["Floor_Of_Unit"]),
                });

            }
            return ListSideMargins;
        }

        [HttpPost]
        public List<Image_Uploaded_List> getImagesByReqId()
        {
            List<Image_Uploaded_List> ListImages = new List<Image_Uploaded_List>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtImages = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesByReqIDForPdf", hInputPara);
            int l = 6;
             int j    = dtImages.Rows.Count;
            int t = l - j;
            if (t <= l)
            {
              //  DataRow row = dtImages.NewRow();
                for (int m = 0; m< t; m++)
                {
                    DataRow row = dtImages.NewRow();
                    row["Image_ID"] = Convert.ToInt32("0");
                    row["Image_Path"] = "NA.jpg";

                    dtImages.Rows.Add(row);

                    //dtImages.Rows.Add(new Object { Convert.ToInt32("0"), "NA" });
                }
            }
            for (int ii = 0; ii < dtImages.Rows.Count ; ii++)
            {

                ListImages.Add(new Image_Uploaded_List
                {
                    Image_ID = Convert.ToInt32(dtImages.Rows[ii]["Image_ID"]),
                    Image_Path = Convert.ToString(dtImages.Rows[ii]["Image_Path"]),
                    



                });

            }

            return ListImages;
        }

        [HttpPost]
        public List<ival_ProjectFloorWiseBreakUp> GetFloorWiseBreakupByReqID()
        {
            List<ival_ProjectFloorWiseBreakUp> ListFloorWiseBreakup = new List<ival_ProjectFloorWiseBreakUp>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtFlooeWise = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorWiseBreakupNewByRequestid", hInputPara);

            for (int i = 0; i < dtFlooeWise.Rows.Count; i++)
            {

                ListFloorWiseBreakup.Add(new ival_ProjectFloorWiseBreakUp
                {
                    PFloor_ID = Convert.ToInt32(dtFlooeWise.Rows[i]["PFloor_ID"]),
                    Floor_Type = Convert.ToString(dtFlooeWise.Rows[i]["Floor_Type"]),
                    Approved_Carpet_Area= Convert.ToString(dtFlooeWise.Rows[i]["Approved_Carpet_Area"]),
                    Approved_Built_up_Area= Convert.ToString(dtFlooeWise.Rows[i]["Approved_Built_up_Area"]),
                    Measured_Carpet_Area= Convert.ToString(dtFlooeWise.Rows[i]["Measured_Carpet_Area"]),
                    Measured_Built_up_Area= Convert.ToString(dtFlooeWise.Rows[i]["Measured_Built_up_Area"]),
                    Adopted_Area_for_Valuation= Convert.ToString(dtFlooeWise.Rows[i]["Adopted_Area_for_Valuation"]),
                    Adopted_Cost_Construction= Convert.ToString(dtFlooeWise.Rows[i]["Adopted_Cost_Construction"]),
                    Total= Convert.ToString(dtFlooeWise.Rows[i]["Total"]),

                });

            }
            return ListFloorWiseBreakup;
        }

        [HttpPost]
        public List<ival_ProjectFloorWiseBreakUp> GetFloorWiseBreakupWithTotalByReqID()
        {
            List<ival_ProjectFloorWiseBreakUp> ListFloorWiseBreakup = new List<ival_ProjectFloorWiseBreakUp>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtFlooeWise = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorWiseBreakupNewWithTotalByRequestid", hInputPara);

            for (int i = 0; i < dtFlooeWise.Rows.Count; i++)
            {

                ListFloorWiseBreakup.Add(new ival_ProjectFloorWiseBreakUp
                {
                    PFloor_ID = Convert.ToInt32(dtFlooeWise.Rows[i]["PFloor_ID"]),
                    Floor_Type = Convert.ToString(dtFlooeWise.Rows[i]["Floor_Type"]),
                    Approved_Carpet_Area = Convert.ToString(dtFlooeWise.Rows[i]["Approved_Carpet_Area"]),
                    Approved_Built_up_Area = Convert.ToString(dtFlooeWise.Rows[i]["Approved_Built_up_Area"]),
                    Measured_Carpet_Area = Convert.ToString(dtFlooeWise.Rows[i]["Measured_Carpet_Area"]),
                    Measured_Built_up_Area = Convert.ToString(dtFlooeWise.Rows[i]["Measured_Built_up_Area"]),
                    Adopted_Area_for_Valuation = Convert.ToString(dtFlooeWise.Rows[i]["Adopted_Area_for_Valuation"]),
                    Adopted_Cost_Construction = Convert.ToString(dtFlooeWise.Rows[i]["Adopted_Cost_Construction"]),
                    Total = Convert.ToString(dtFlooeWise.Rows[i]["Total"]),

                });

            }
            return ListFloorWiseBreakup;
        }

        [HttpPost]
        public List<ival_Document_Uploaded_List> getUploadDocumentByReqId()
        {
            List<ival_Document_Uploaded_List> ListDoc = new List<ival_Document_Uploaded_List>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@ID", RequestID);
            DataTable dtdoclist = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentUploadedListByReqIDForPDF", hInputPara);

            for (int i = 0; i < dtdoclist.Rows.Count; i++)
            {
                ival_Document_Uploaded_List obj = new ival_Document_Uploaded_List();
                obj.Docupload_ID = Convert.ToInt32(dtdoclist.Rows[i]["Docupload_ID"]);
                obj.Document_Name = Convert.ToString(dtdoclist.Rows[i]["Document_Name"]);
                var date = dtdoclist.Rows[i]["Date_Of_Approval"].ToString();
                if(date!="")
                {
                    try
                    {
                        DateTime dti = DateTime.ParseExact(dtdoclist.Rows[i]["Date_Of_Approval"].ToString(), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);

                        string DateOFapproval = dti.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
                        obj.Date_Of_ApprovalNew = DateOFapproval;
                    }
                    catch (Exception e)
                    {
                        string datestring1 = Convert.ToDateTime(dtdoclist.Rows[i]["Date_Of_Approval"].ToString()).ToString("dd/MM/yyyy");
                        obj.Date_Of_ApprovalNew = datestring1;
                    }
                }
                
                //obj.Date_Of_Approval = Convert.ToDateTime(dtdoclist.Rows[i]["Date_Of_Approval"]);
                obj.Approval_Authority = Convert.ToString(dtdoclist.Rows[i]["Approval_Authority"]);
                obj.Approval_Number = Convert.ToString(dtdoclist.Rows[i]["Approval_Number"]);
                ListDoc.Add(obj);

                //ListDoc.Add(new ival_Document_Uploaded_List
                //{

                //    Docupload_ID = Convert.ToInt32(dtdoclist.Rows[i]["Docupload_ID"]),
                //    Document_Name = Convert.ToString(dtdoclist.Rows[i]["Document_Name"]),
                    
                //    Date_Of_Approval = Convert.ToDateTime(dtdoclist.Rows[i]["Date_Of_Approval"]),
                    
                //    Approval_Authority= Convert.ToString(dtdoclist.Rows[i]["Approval_Authority"]),
                //    Approval_Number = Convert.ToString(dtdoclist.Rows[i]["Approval_Number"]),


                //});

            }
            return ListDoc;
        }





        public ActionResult PrintViewToPdfForYes(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            //var report = new ActionAsPdf("TestReport");
            //return report;

            string header = "http://localhost:44304/Report/Header1"; //For Local Testinghttp://apps.brainlines.net/IvalueReport/Report/Header1 
            string footer = "http://localhost:44304/Report/Footer1"; //For Local Testinghttp://apps.brainlines.net/IvalueReport/Report/Footer1


            string customSwitches = string.Format("--print-media-type --header-html  \"{0}\" " +
                                   "--header-spacing \"0\" " +
                                   "--footer-html \"{1}\" " +
                                   "--footer-spacing \"10\" " +
                                   "--footer-font-size \"10\" " +
                                   "--header-font-size \"10\" ", header, footer);
            return new ActionAsPdf("YesBankReportFlat", new { Id = ID })
            {



                CustomSwitches = customSwitches,

                // FileName = "IvalueReport.pdf",
                PageSize = Size.A4,
                PageOrientation = Orientation.Portrait,
            };

            //return report;
        }

        public ActionResult PrintViewToPdfForHdfc(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            //var report = new ActionAsPdf("TestReport");
            //return report;

            string header = "http://localhost:44304/Report/Header1"; //For Local Testinghttp://apps.brainlines.net/IvalueReport/Report/Header1 
            string footer = "http://localhost:44304/Report/Footer1"; //For Local Testinghttp://apps.brainlines.net/IvalueReport/Report/Footer1


            string customSwitches = string.Format("--print-media-type --header-html  \"{0}\" " +
                                   "--header-spacing \"0\" " +
                                   "--footer-html \"{1}\" " +
                                   "--footer-spacing \"10\" " +
                                   "--footer-font-size \"10\" " +
                                   "--header-font-size \"10\" ", header, footer);
            return new ActionAsPdf("EdelweissFinanceMultiUnit", new { Id = ID })
            {



                CustomSwitches = customSwitches,

                // FileName = "IvalueReport.pdf",
                PageSize = Size.A4,
                PageOrientation = Orientation.Portrait,
            };

            //return report;
        }


        public ActionResult Footer1( string UserName)
        {
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            string cityName = "";
            string EmployeeId = "";
            string UserNameEmp = UserName;
            hInputPara1.Add("@userName", UserNameEmp);
            DataTable dtrequestCity = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetEmployeeCurrentCity", hInputPara1);
            hInputPara1.Clear();
            if(dtrequestCity.Rows.Count>0)
            {
                if (dtrequestCity.Rows[0]["Current_City"] != DBNull.Value)
                {
                    cityName = dtrequestCity.Rows[0]["Current_City"].ToString();
                }
                if (dtrequestCity.Rows[0]["Employee_ID"] != DBNull.Value)
                {
                    EmployeeId = dtrequestCity.Rows[0]["Employee_ID"].ToString();
                }
                //EmployeeId = dtrequestCity.Rows[0]["Employee_ID"].ToString();
                if (EmployeeId != "")
                {
                    ViewBag.city = cityName;
                }
                else
                {
                    ViewBag.city = "Pune";
                }

            }
            else
            {
                ViewBag.city = "Pune";
            }

            return View();
        }

        public ActionResult Header1()
        {
            return View();
        }

        public ActionResult ViewReportById()
        {
            return View();
        }
    }
}