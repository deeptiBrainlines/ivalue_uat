﻿using I_Value.WebApp.Models;
using IvalueDataAccess;
using IValuePublishProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace I_Value.WebApp.Controllers
{
    public class LookUpDataController : Controller
    {
        // GET: LookUpData
        SQLDataAccess sqlDataAccess;
        Hashtable hInputPara;
        public ActionResult Index()
        {
            VMLookupCategoryValues VMLookupCategoryValues = new VMLookupCategoryValues();
            VMLookupCategoryValues.ival_LookupCategoryValueList = GetLookupCategoryValues();
            return View(VMLookupCategoryValues);
        }
        public IEnumerable<ival_LookupCategoryValues> GetLookupCategoryValues()
        {
            List<ival_LookupCategoryValues> listLookupCategoryValues = new List<ival_LookupCategoryValues>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtLookupCategoryValuesList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLookupCategoryValuesListByID");
            for (int i = 0; i < dtLookupCategoryValuesList.Rows.Count; i++)
            {

                listLookupCategoryValues.Add(new ival_LookupCategoryValues
                {
                    Lookup_Type = Convert.ToString(dtLookupCategoryValuesList.Rows[i]["Lookup_Type"])

                });
            }
            return listLookupCategoryValues.AsEnumerable();
        }
        [HttpPost]
        public JsonResult GetLookUpList(String Lookup_Type)
        {
            VMLookupCategoryValues VMLookupCategoryValues = new VMLookupCategoryValues();
            VMLookupCategoryValues.ival_LookupCategoryList = GetLookupCategoryList(Lookup_Type);
            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string listLookupCategoryListfilter = serializer.Serialize(VMLookupCategoryValues.ival_LookupCategoryList);
            return Json(listLookupCategoryListfilter);
        }
        public IEnumerable<ival_LookupCategoryValues> GetLookupCategoryList(string Lookup_Type)
        {
            List<ival_LookupCategoryValues> listLookupCategoryList = new List<ival_LookupCategoryValues>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Lookup_Type", Lookup_Type);
                DataTable dtLookupCategoryList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLookupCategoryList", hInputPara);
                for (int i = 0; i < dtLookupCategoryList.Rows.Count; i++)
                {

                    listLookupCategoryList.Add(new ival_LookupCategoryValues
                    {
                        Lookup_ID = Convert.ToUInt16(dtLookupCategoryList.Rows[i]["Lookup_ID"]),
                        Lookup_Type = Convert.ToString(dtLookupCategoryList.Rows[i]["Lookup_Type"]),
                        Lookup_Value = Convert.ToString(dtLookupCategoryList.Rows[i]["Lookup_Value"]),
                        //Lookup_Description = Convert.ToString(dtLookupCategoryList.Rows[i]["Lookup_Description"])
                    });

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return listLookupCategoryList.AsEnumerable();
        }
        [HttpPost]
        public ActionResult AddNewCategory(string Lookup_Type)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            try
            {
                hInputPara.Add("@Lookup_Type", Lookup_Type);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertLookupCategory", hInputPara);
                if (Convert.ToString(result) == "1")
                {
                    return Json(new { success = true });

                }
                else if (Convert.ToString(result) == "Category already exist")
                {
                    return Json(new { success = false, message = "Category already exists" });
                }
                else
                {
                    return RedirectToAction("Index", "LookUpData");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }



        }
        [HttpPost]
        public ActionResult AddNewValue(string Lookup_Type, string Lookup_Value, string Lookup_Description)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();

            hInputPara.Add("@Lookup_Type", Lookup_Type);
            hInputPara.Add("@Lookup_Value", Lookup_Value);
            hInputPara.Add("@Created_By", "test");
            hInputPara.Add("@Lookup_Description", Lookup_Description);
            var result = sqlDataAccess.ExecuteStoreProcedure("usp_SetLookupValue", hInputPara);
            if (Convert.ToString(result) == "1")
            {
                return Json(new { success = true });
                //return Json(new { success = true}, JsonRequestBehavior.AllowGet);
                /*var jsonSerialiser = new JavaScriptSerializer();
                var jsonProjectpinMaster = jsonSerialiser.Serialize(LocList);
                return Json(jsonProjectpinMaster);*/
            }
            else if (Convert.ToString(result) == "Lookup Value already exists")
            {
                return Json(new { success = false, message = "Lookup Value already exists" });
            }
            else
            {
                return RedirectToAction("Index", "LookUpData");
            }
        }
        [HttpPost]
        public ActionResult EditValue(int Lookup_ID, string Lookup_Value, string Lookup_Type)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();

            hInputPara.Add("@Lookup_ID", Lookup_ID);
            hInputPara.Add("@Lookup_Value", Lookup_Value);
            hInputPara.Add("@Lookup_Type", Lookup_Type);
            var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateLookupValue", hInputPara);
            if (Convert.ToString(result) == "1")
            {
                return Json(new { success = true });

            }
            else if (Convert.ToString(result) == "Lookup Value already exists")
            {
                return Json(new { success = false, message = "Lookup Value already exists" });
            }
            else
            {
                return RedirectToAction("Index", "LookUpData");
            }
        }
        [HttpPost]
        public ActionResult Delete(int Lookup_ID, string Lookup_Type)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();

            hInputPara.Add("@Lookup_ID", Lookup_ID);
            hInputPara.Add("@Lookup_Type", Lookup_Type);
            var result = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteLookUpRecordByLookupID", hInputPara);
            if (Convert.ToInt32(result) == 1)
            {
                VMLookupCategoryValues VMLookupCategoryValues = new VMLookupCategoryValues();
                VMLookupCategoryValues.ival_LookupCategoryValueList = GetLookupCategoryValues();
                VMLookupCategoryValues.ival_LookupCategoryList = GetLookupCategoryList(Lookup_Type);
                return Json(new { success = true });
                return View(VMLookupCategoryValues);
            }
            else
            {
                return RedirectToAction("Index", "LookUpData");
            }
        }
    }
}