﻿using I_Value.WebApp.Models;
using IvalueDataAccess;
using IValuePublishProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IValuePublishProject.Controllers
{
    public class ProjectValuationRequestorController : Controller
    {
        Hashtable hInputPara;
        Hashtable hInputPara1;
        SQLDataAccess sqlDataAccess;
        SQLDataAccess sqlDataAccess1;
        public ActionResult Index()
        {
            return View();
        }  
        [HttpPost]
        public ActionResult AddProjectValuation(string rb1, string rb2)
        {
            return Json(new { success = true });
        }
        public ActionResult ProjectRequestorDetails(string rb1, string rb2)
        {
            VMBuilderGroupMaster vm = new VMBuilderGroupMaster();
            vm.ival_ClientMasters = GetClientName();
            vm.ival_LookupCategoryDepartment = GetDepartment();
            vm.BuilderGroupList = GetBuilderGroup();
            vm.ival_LookupCategoryValuesreporttype = GetReportType();
            vm.ival_LookupCategoryValuesmethodvaluation = Getmethodtype();
            vm.ival_LookupCategoryPropertyType = GetProjectTypeMaster();
            vm.projSiteEnginnerList=GetSiteEnginner();
            DataTable ValuationRequestID = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetValuationRequestID");
            ViewBag.ValuationRequestID = ValuationRequestID.Rows[0]["ValuationRequestID"].ToString();
            vm.RequestType = rb1;
            vm.ReportRequestType = rb2;
            vm.ival_DocumentListsNew = GetDocumentsEdit();
            //vm.ival_DocumentLists = GetDocuments();
            DataTable DocID = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1DOCIDListNew");
            ViewBag.Srno = DocID.Rows[0]["Document_ID"].ToString();
            DataTable SrNo = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1DoclListNewSrNoId");
            ViewBag.SrnoNew = SrNo.Rows[0]["SrNo"].ToString();
            return View(vm);
        }
        [HttpPost]
        public List<ival_ClientMaster> GetClientName()
        {
            List<ival_ClientMaster> ListReporttype = new List<ival_ClientMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientName");
            //DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientName");
            for (int i = 0; i < dtGetReportType.Rows.Count; i++)
            {

                ListReporttype.Add(new ival_ClientMaster
                {
                    //Client_ID = Convert.ToInt32(dtGetReportType.Rows[i]["Client_ID"]),
                    Client_Name = Convert.ToString(dtGetReportType.Rows[i]["Client_Name"]),
                    // Branch_Name = Convert.ToString(dtGetReportType.Rows[i]["Branch_Name"])

                });

            }
            return ListReporttype;
        } 
         [HttpPost]
        public List<ProjectSiteEnginner> GetSiteEnginner()
        {
            List<ProjectSiteEnginner> ListSiteEnginner = new List<ProjectSiteEnginner>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojsiteEnginner");
            //DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientName");
            for (int i = 0; i < dtGetReportType.Rows.Count; i++)
            {

                ListSiteEnginner.Add(new ProjectSiteEnginner
                {
                    Site_Engineer_ID = Convert.ToInt32(dtGetReportType.Rows[i]["Site_Engineer_ID"]),
                    Site_Engineer_Name = Convert.ToString(dtGetReportType.Rows[i]["Site_Engineer_Name"]),
                   
                });

            }
            return ListSiteEnginner;
        }

        [HttpPost]
        public List<ival_BranchMaster> GetBranchNameNew(string Bank_ID)
        {
            List<ival_BranchMaster> Listbranch = new List<ival_BranchMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@BankName", Bank_ID);
            DataTable dtbranch = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBranchListByBankNameNew", hInputPara);
            for (int i = 0; i < dtbranch.Rows.Count; i++)
            {

                Listbranch.Add(new ival_BranchMaster
                {

                    Branch_ID = Convert.ToInt32(dtbranch.Rows[i]["Client_ID"]),
                    Branch_Name = Convert.ToString(dtbranch.Rows[i]["Branch_Name"]),

                });

            }
            return Listbranch;
        }
        //added by punam
        [HttpPost]
        public JsonResult GetBranchListByBankNameNew(string Bank_ID)
        {


            List<ival_BranchMaster> unitList = new List<ival_BranchMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@BankName", Bank_ID);
            DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBranchListByBankNameNew", hInputPara);
            for (int i = 0; i < dtrptmaster.Rows.Count; i++)
            {

                unitList.Add(new ival_BranchMaster
                {
                    Branch_ID = Convert.ToInt32(dtrptmaster.Rows[i]["Client_ID"]),
                    Branch_Name = Convert.ToString(dtrptmaster.Rows[i]["Branch_Name"]),

                });
                ViewBag.SelectBranchName = Convert.ToString(dtrptmaster.Rows[i]["Branch_Name"]);
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonvMUnitTypes = jsonSerialiser.Serialize(unitList);
            return Json(jsonvMUnitTypes);
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetDepartment()
        {
            List<ival_LookupCategoryValues> ListReporttype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDepartment");
            for (int i = 0; i < dtGetReportType.Rows.Count; i++)
            {

                ListReporttype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtGetReportType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtGetReportType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtGetReportType.Rows[i]["Lookup_Value"])

                });

            }
            return ListReporttype;
        }  
        public List<ival_BuilderGroupMaster> GetBuilderGroup()
        {
            List<ival_BuilderGroupMaster> ListReporttype = new List<ival_BuilderGroupMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuilderGroupMaster");
            //DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientName");
            for (int i = 0; i < dtGetReportType.Rows.Count; i++)
            {

                ListReporttype.Add(new ival_BuilderGroupMaster
                {
                   Builder_Group_Master_ID = Convert.ToInt32(dtGetReportType.Rows[i]["Builder_Group_Master_ID"]),
                    BuilderGroupName = Convert.ToString(dtGetReportType.Rows[i]["BuilderGroupName"]),
                    

                });

            }
            return ListReporttype;

            
        }  
        [HttpPost]
        public JsonResult GetBuilderCompanyByGroupId(string BuilderGroupId)
        {
            List<ival_BuilderCompanyMasterNew> ListReporttype = new List<ival_BuilderCompanyMasterNew>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Builder_Group_Master_ID", Convert.ToInt32(BuilderGroupId));
            DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuliderCompanyNewByGpID",hInputPara);
            //DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientName");
            for (int i = 0; i < dtGetReportType.Rows.Count; i++)
            {

                ListReporttype.Add(new ival_BuilderCompanyMasterNew
                {
                    Builder_Group_Master_ID = Convert.ToInt32(dtGetReportType.Rows[i]["Builder_Group_Master_ID"]),
                    Builder_Company_Name = Convert.ToString(dtGetReportType.Rows[i]["Builder_Company_Name"]),
                    Builder_Company_Master_ID= Convert.ToInt32(dtGetReportType.Rows[i]["Builder_Company_Master_ID"]),

                });

            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonbCompanyTypes = jsonSerialiser.Serialize(ListReporttype);
            return Json(jsonbCompanyTypes);


        }
        [HttpPost]
        public JsonResult GetBuilderProjectByCompanyGroupId(string BuilderGroupId, string BuilderCompanyId)
        {
            List<ival_ProjectMasterDetails> ListReporttype = new List<ival_ProjectMasterDetails>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Builder_Group_Master_ID", Convert.ToInt32(BuilderGroupId));
            hInputPara.Add("@Builder_Company_Master_ID", Convert.ToInt32(BuilderCompanyId));
            DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuliderProjectByCompanyGroupID", hInputPara);
            //DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientName");
            for (int i = 0; i < dtGetReportType.Rows.Count; i++)
            {

                ListReporttype.Add(new ival_ProjectMasterDetails
                {
                    Project_ID = Convert.ToInt32(dtGetReportType.Rows[i]["Project_ID"]),
                    Project_Name = Convert.ToString(dtGetReportType.Rows[i]["Project_Name"]),
                   

                });

            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonbCompanyTypes = jsonSerialiser.Serialize(ListReporttype);
            return Json(jsonbCompanyTypes);
           

        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetReportType()
        {
            List<ival_LookupCategoryValues> ListReporttype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetReportType");
            for (int i = 0; i < dtGetReportType.Rows.Count; i++)
            {

                ListReporttype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtGetReportType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtGetReportType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtGetReportType.Rows[i]["Lookup_Value"])

                });

            }
            return ListReporttype;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> Getmethodtype()
        {
            List<ival_LookupCategoryValues> Listmethodtype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtmethodtype = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetMethodOfValuation");
            for (int i = 0; i < dtmethodtype.Rows.Count; i++)
            {

                Listmethodtype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtmethodtype.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtmethodtype.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtmethodtype.Rows[i]["Lookup_Value"])

                });

            }
            return Listmethodtype;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetProjectTypeMaster()
        {
            List<ival_LookupCategoryValues> Listprojtype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtprojecttypermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectType");
            for (int i = 0; i < dtprojecttypermaster.Rows.Count; i++)
            {

                Listprojtype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtprojecttypermaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtprojecttypermaster.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtprojecttypermaster.Rows[i]["Lookup_Value"])

                });


            }
            return Listprojtype;
        }
        
        public int GetClientId(string clientName, string BranchName)
        {
            int client_id = 0;
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Client_Name", clientName);
            hInputPara.Add("@Branch_Name", BranchName);
            DataTable list1 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientIDByClientName", hInputPara);
            for (int i = 0; i < list1.Rows.Count; i++)
            {
                //changes by punam
                client_id = Convert.ToInt32(list1.Rows[i]["Client_ID"]);
            }
            hInputPara.Clear();
            return client_id;

        } 
        [HttpPost]
        public ActionResult SaveProjectValuationRequest(string str, string strBuil,string strWing,string rb,string projectIdNew)
        {
            try
            {
                //if(strBuil!=null)
                //{
                //    DataTable ProjBuilding = (DataTable)JsonConvert.DeserializeObject(strBuil, (typeof(DataTable)));
                //    Session["ProjBuildingId"] = ProjBuilding;
                //}
                //if(strWing!=null)
                //{
                //    DataTable ProjWing = (DataTable)JsonConvert.DeserializeObject(strWing, (typeof(DataTable)));
                //    Session["ProjWingId"] = ProjWing;
                //}
                if(rb!="")
                {
                    Session["ProjRadioValue"] = rb;
                }
             DataTable projectList = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                if (projectList != null && projectList.Rows.Count > 0)
                {
                    for (int i = 0; i < projectList.Rows.Count; i++)
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        
                        
                        
                        int client_id = 0;
                        if (Convert.ToString(projectList.Rows[i]["NameOfBank"]) != "")
                        {
                            string client_name = Convert.ToString(projectList.Rows[i]["NameOfBank"]);
                            string Branch_name = Convert.ToString(projectList.Rows[i]["BranchName"]);

                            client_id = GetClientId(client_name, Branch_name);
                        }
                        hInputPara.Add("@RequestFlag", "New_Request");
                        hInputPara.Add("@clientId", client_id);
                        
                         hInputPara.Add("@projectId", Convert.ToInt32(projectList.Rows[i]["BuilderProject"]));
                        int projectId = Convert.ToInt32(projectList.Rows[i]["BuilderProject"]);
                        Session["ProjectId"] = projectId;


                        hInputPara.Add("@requestType", Convert.ToString(projectList.Rows[i]["RequestType1"]));
                        hInputPara.Add("@reportRequestType", Convert.ToString(projectList.Rows[i]["ReportRequestType"]));
                        try
                        {
                            var dateString = projectList.Rows[i]["DateOfRequest"].ToString();
                            var format = "dd/MM/yyyy";
                            if (dateString != "")
                            {
                                var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                hInputPara.Add("@dateOfRequest", dateTime);
                            }
                        }
                       catch(Exception e)
                        {
                            string dateString = projectList.Rows[i]["DateOfRequest"].ToString();
                            if (dateString != "")
                            {

                                string datestring1 = Convert.ToDateTime(dateString).ToString("dd/MM/yyyy");
                                var format = "dd/MM/yyyy";
                                var dateTime = DateTime.ParseExact(datestring1, format, CultureInfo.InvariantCulture);
                                hInputPara.Add("@dateOfRequest", dateTime);
                            }
                        }

           
                        hInputPara.Add("@requestorName", Convert.ToString(projectList.Rows[i]["nameOfRequestor"]));
                        hInputPara.Add("@department", Convert.ToString(projectList.Rows[i]["department"]));
                        hInputPara.Add("@valuationRequestId", Convert.ToString(projectList.Rows[i]["valuationRequestId"]));
                        if (projectList.Rows[i]["BuilderGroup"].ToString() != "")
                        {
                            hInputPara.Add("@builderGroupMasterId", Convert.ToInt32(projectList.Rows[i]["BuilderGroup"]));
                        }
                        if (projectList.Rows[i]["BuilderCompany"].ToString() != "")
                        {
                            hInputPara.Add("@builderCompanyMasterId", Convert.ToInt32(projectList.Rows[i]["BuilderCompany"]));
                        }
                           
                        hInputPara.Add("@methodOfValuation", Convert.ToString(projectList.Rows[i]["methodOfValuation"]));
                        hInputPara.Add("@propertyType", Convert.ToString(projectList.Rows[i]["PropertyType"]));
                        hInputPara.Add("@UserId",Session["UserName"].ToString());
                        hInputPara.Add("@ReportType",Convert.ToString(projectList.Rows[i]["reportType"]));
                        hInputPara.Add("@createdBy", Session["UserName"].ToString());
                        string result=sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectRequestorDetails", hInputPara);
                        hInputPara.Clear();

                        sqlDataAccess = new SQLDataAccess();

                        hInputPara.Add("@UserID", Session["UserName"].ToString());
                        DataTable dtREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1RequestIDForProjectRequest", hInputPara);
                        hInputPara.Clear();

                        //insert data into ProjValrequestLog Table
                        int RequestId= Convert.ToInt32(dtREQ.Rows[0]["Request_ID"]);
                        Session["ProjRequestIdSave"] = RequestId;
                        int ProjId = Convert.ToInt32(projectIdNew);
                        if(rb == "Entire Project")
                        {
                            List<ival_ProjBldgWingMaster> list = GetProjBuilWing(ProjId);
                            foreach (var data in list)
                            {
                                sqlDataAccess = new SQLDataAccess();
                                hInputPara = new Hashtable();
                                hInputPara.Add("@RequestId", RequestId);
                                hInputPara.Add("@ProjectId", ProjId);
                                hInputPara.Add("@buildingId", data.Building_ID);
                                hInputPara.Add("@wingId", data.Wing_ID);
                                if (rb == "Entire Project")
                                {
                                    hInputPara.Add("@IsEntireProje", true);
                                }

                                hInputPara.Add("@createdBy", Session["UserName"].ToString());
                                string result1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectValRequestLog", hInputPara);
                                hInputPara.Clear();
                            }

                        }
                        if(rb== "Part Of Project")
                        {
                            DataTable ProjBuilding = (DataTable)JsonConvert.DeserializeObject(strBuil, (typeof(DataTable)));
                            Session["ProjBuildingId"] = ProjBuilding;
                            DataTable ProjWing = (DataTable)JsonConvert.DeserializeObject(strWing, (typeof(DataTable)));
                            Session["ProjWingId"] = ProjWing;

                            //for (int j = 0; j < ProjBuilding.Rows.Count; j++)
                            //{

                            for (int k = 0; k < ProjWing.Rows.Count; k++)
                                {
                                    sqlDataAccess = new SQLDataAccess();
                                    hInputPara = new Hashtable();
                                    hInputPara1 = new Hashtable();
                                hInputPara1.Add("@projectId", ProjId);
                                hInputPara1.Add("@wingId",ProjWing.Rows[k]["wingVal"]);
                                DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ProjBuildingIdByWingId", hInputPara1);
                                int BuildingIdNew = Convert.ToInt32(dtdoc.Rows[0]["Building_ID"]);
                                hInputPara1.Clear();

                                    hInputPara.Add("@RequestId", RequestId);
                                    hInputPara.Add("@ProjectId", ProjId);
                                   hInputPara.Add("@buildingId",BuildingIdNew);
                                    hInputPara.Add("@IsEntireProje", false);
                                    hInputPara.Add("@createdBy", Session["UserName"].ToString());

                                    hInputPara.Add("@wingId", ProjWing.Rows[k]["wingVal"]);
                                    string result1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectValRequestLog", hInputPara);
                                    hInputPara.Clear();

                                }
                            //}
                           
                            
                            
                        }



                        return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);

                    }
                }
               
            }
            catch(Exception e)
            {
                return Json(new { success = false, Message = "Does Not Inserted Successfully" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, Message = "Does Not Inserted Successfully" }, JsonRequestBehavior.AllowGet);
        }
        public List<ival_ProjBldgWingMaster> GetProjBuilWing(int projectId)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@projectId",projectId);
            List<ival_ProjBldgWingMaster> List = new List<ival_ProjBldgWingMaster>();
            DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ProjBuilWingByProjectId",hInputPara);
            hInputPara.Clear();
            for (int i = 0; i < dtdoc.Rows.Count; i++)
            {
                ival_ProjBldgWingMaster list = new ival_ProjBldgWingMaster();
                list.Building_ID = Convert.ToInt32(dtdoc.Rows[i]["Building_ID"]);
                list.Wing_ID = Convert.ToInt32(dtdoc.Rows[i]["Wing_ID"]);
                List.Add(list);
            }
            return List;
        }


        //[HttpPost]
        //public List<ival_Document_Uploaded_List> GetDocuments()
        //{
        //    List<ival_Document_Uploaded_List> listdoc = new List<ival_Document_Uploaded_List>();
        //    hInputPara = new Hashtable();
        //    sqlDataAccess = new SQLDataAccess();
        //    DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectUploadDocLists");
        //    for (int i = 0; i < dtdoc.Rows.Count; i++)
        //    {
        //        listdoc.Add(new ival_Document_Uploaded_List
        //        {
        //            Document_ID = Convert.ToInt32(dtdoc.Rows[i]["Document_ID"]),
        //            Document_Name = Convert.ToString(dtdoc.Rows[i]["Document_Name"]),

        //            = Convert.ToString(dtdoc.Rows[i]["Project_Type"]),
        //        });

        //    }
        //    return listdoc;
        //}
        [HttpPost]
        public JsonResult UploadPDFFiles(string DocId)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@UserID", Session["UserName"].ToString());
            DataTable dtREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1RequestIDForProjectRequest",hInputPara);
            hInputPara.Clear();

            string id = Convert.ToString(dtREQ.Rows[0]["Request_ID"]);
            Session["Request_ID"] = id;
            string a = DocId.PadLeft(2, '0');
            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                                                            //Use the following properties to get file's name, size and MIMEType
                int fileSize = file.ContentLength;
                string fileName = file.FileName;
                string mimeType = file.ContentType;
                System.IO.Stream fileContent = file.InputStream;
                //string path = "~/UploadFiles/" + fileName;
                //file.SaveAs(Server.MapPath("~/UploadFiles/"+ fileName));
                //To save file, use SaveAs method
                //changes by punam
                var Request_ID = Session["Request_ID"];
                var pdffile = Request_ID + "_" + a + "_" + fileName;

                file.SaveAs(Server.MapPath("../UploadFiles/") + pdffile); //File will be saved in application root
                Session["File"] = pdffile;
            }

            return Json("Uploaded " + Request.Files.Count + " files");
        }
        [HttpPost]
        public JsonResult UploadPDFFilesEdit(string DocId)
        {
            string a = DocId.PadLeft(2, '0');
            string ReqID = Session["RequestIDProj"].ToString();
            //string file=ReqID+

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                                                            //Use the following properties to get file's name, size and MIMEType
                int fileSize = file.ContentLength;
                string fileName = file.FileName;
                string mimeType = file.ContentType;
                // System.IO.Stream fileContent = file.InputStream;

                String FileSave = ReqID + "_" + a + "_" + fileName;
                //if (!FileSave.Contains(".pdf"))
                //{
                //    return Json("Please select PDF file only");
                //}
                var getFile = Directory.GetFiles(Server.MapPath("~/UploadFiles"));
                //var b = VirtualPathUtility.ToAbsolute("~/UploadFiles/" + fileName);
                foreach (string filename in getFile)
                {
                    if (filename.Contains(FileSave))
                    {
                        var Get = filename;
                        if (System.IO.File.Exists(filename))
                        {
                            System.IO.File.Delete(filename);
                            //Console.WriteLine("file Deleted");
                            file.SaveAs(Server.MapPath("../UploadFiles/") + FileSave);

                        }
                    }
                    else
                    {

                        file.SaveAs(Server.MapPath("../UploadFiles/") + FileSave);

                        Dispose();
                    }

                }

                Session["FileEdit"] = FileSave;
                TempData["TempFileEdit"] = FileSave;
            }

            return Json("Uploaded " + Request.Files.Count + " files");
        }

        public JsonResult Download()
        {
         
            var FilePath = Session["File"].ToString();
            var folderPath = Server.MapPath("~/UploadFiles");
            var fileCount = Directory.GetFiles(Server.MapPath("~/UploadFiles"));
            if (fileCount.Length > 1) // exactly only 1 file
            {
                string embed = "<object data=\"{0}\" type=\"application/pdf\"width=\"1000px\" height=\"600px\">";
                embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
                embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                embed += "</object>";
                TempData["Embed"] = string.Format(embed, VirtualPathUtility.ToAbsolute("~/UploadFiles/" + FilePath));
                return Json("Exist", JsonRequestBehavior.AllowGet);
            }
           
            else
            {
                Process.Start(folderPath);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult DownloadPDF()
        {
            return View();
        }
        public JsonResult DownloadImage()
        {
           // UploadImage(i);
            var FilePath = Session["ImageSave"].ToString();
            var folderPath = Server.MapPath("~/images");
            var fileCount = Directory.GetFiles(Server.MapPath("~/images"));
            if (fileCount.Length > 1) // exactly only 1 file
            {
                string embed = "<object data=\"{0}\" type=\"application/pdf\"width=\"2000px\" height=\"1000px\">";
                embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
                embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                embed += "</object>";
                TempData["EmbedImage"] = string.Format(embed, VirtualPathUtility.ToAbsolute("~/images/" + FilePath));
                return Json("Exist", JsonRequestBehavior.AllowGet);
            }

            else
            {
                Process.Start(folderPath);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }
        public JsonResult DownloadImageEdit(string imageTypeId,string seqId)
        {
           
                var FilePath = "";
                hInputPara = new Hashtable();
                hInputPara1 = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                int RequestID = Convert.ToInt32(Session["RequestIDProj"]);
                var NewFilePath = TempData["TempImageEdit"];
            int ImageTypeId = Convert.ToInt32(imageTypeId);
                int SeqIdNew = Convert.ToInt32(seqId);
               
            hInputPara1.Add("@Request_ID", Convert.ToInt32(RequestID));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            string projectId = Convert.ToString(ProjId.Rows[0]["Project_ID"]);

            hInputPara.Add("@RequestId", RequestID);
            hInputPara.Add("@ImageTypeId", ImageTypeId);
            hInputPara.Add("@seqId", SeqIdNew);
            hInputPara.Add("@projectId", Convert.ToInt32(projectId));
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadedImageName", hInputPara);
                if (dtRequests.Rows.Count > 0)
                {
                    if (dtRequests.Rows[0]["Image_Path"].ToString() != string.Empty && (dtRequests.Rows[0]["Image_Path"].ToString()) != null)
                    {
                        string fileName = Convert.ToString(dtRequests.Rows[0]["Image_Path"]);
                        if (NewFilePath != null)
                        {
                            FilePath = NewFilePath.ToString();
                        }
                        else
                        {
                            FilePath = fileName;
                        }

                    }

                }
                else
                {
                    var FileGet = Session["ImageSaveEdit"];
                FilePath = FileGet.ToString();
                }


                var folderPath = Server.MapPath("~/images");
            var fileCount = Directory.GetFiles(Server.MapPath("~/images"));
            if (fileCount.Length > 1) // exactly only 1 file
            {
                string embed = "<object data=\"{0}\" type=\"application/pdf\"width=\"2000px\" height=\"1000px\">";
                embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
                embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                embed += "</object>";
                TempData["EmbedImage"] = string.Format(embed, VirtualPathUtility.ToAbsolute("~/images/" + FilePath));
                return Json("Exist", JsonRequestBehavior.AllowGet);
            }

            else
                {
                    Process.Start(folderPath);
                }

                return Json("", JsonRequestBehavior.AllowGet);
            

        }

        public ActionResult DownloadImageView()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UploadFiles(string str, string str1)
        {
            try
            {
                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                DataTable list2 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));
                //Session["ApprovalDetails"] = list2;
                
                hInputPara = new Hashtable();
                hInputPara1 = new Hashtable();
                sqlDataAccess1 = new SQLDataAccess();

                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@UserID", Session["UserName"].ToString());
                DataTable dtREQ = sqlDataAccess1.GetDatatableExecuteStoreProcedure("usp_Gettop1RequestIDForProjectRequest", hInputPara);
                hInputPara.Clear();

                string id = Convert.ToString(dtREQ.Rows[0]["Request_ID"]);
                Session["Request_ID1"] = id;
                if (list1.Rows.Count > 0)
                {
                    for (int i = 0; i < list1.Rows.Count; i++)
                    {
                        string fileName = "";
                        //get file name for adding prefix of request and documentId
                        string file = list1.Rows[i]["filename"].ToString();
                        
                        if (file != "")
                        {
                            string[] newValue = file.Split('\\');
                            if (newValue != null)
                            {
                                string docId = list1.Rows[i]["docid"].ToString();
                                string DocID = docId.PadLeft(2, '0');
                                string Name = newValue[newValue.Length - 1];
                                fileName = id + "_" + DocID + "_" + Name;
                            }
                        }

                        if ((list1.Rows[i]["apprnum"].ToString()) != String.Empty && (list1.Rows[i]["apprnum"].ToString()) != null)
                        {
                            int docId = Convert.ToInt32(list1.Rows[i]["docid"]);
                            hInputPara.Add("@Document_ID_New", Convert.ToInt32(list1.Rows[i]["docid"]));
                            hInputPara.Add("@RequestID_New", Convert.ToInt32(id));
                            string result = sqlDataAccess1.ExecuteStoreProcedure("usp_InsertNewUploadDocument", hInputPara);
                             if (result == "document already exist")
                            {
                                if (list1.Rows[i]["filename"].ToString() != string.Empty && (list1.Rows[i]["filename"].ToString()) != null)
                                {
                                    hInputPara1.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]));
                                    hInputPara1.Add("@RequestID", Convert.ToInt32(id));

                                    //hInputPara1.Add("@Document_Name", Convert.ToString(list1.Rows[i]["filename"]));

                                    hInputPara1.Add("@Document_Name", fileName);

                                    hInputPara1.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));

                                    try
                                    {
                                        var dateString = list1.Rows[i]["dateofapp"].ToString();
                                        var format = "dd/MM/yyyy";
                                        if (dateString != "")
                                        {
                                            var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                            hInputPara1.Add("@Date_Of_Approval", dateTime);
                                        }
                                    }
                                   catch(Exception e)
                                    {
                                        string dateString = list1.Rows[i]["dateofapp"].ToString();
                                        if (dateString != "")
                                        {

                                            string datestring1 = Convert.ToDateTime(dateString).ToString("dd/MM/yyyy");
                                            var format = "dd/MM/yyyy";
                                            var dateTime = DateTime.ParseExact(datestring1, format, CultureInfo.InvariantCulture);
                                            hInputPara1.Add("@Date_Of_Approval", dateTime);
                                        }
                                    }
                                    

            //  hInputPara.Add("@Date_Of_Approval", DateTime.Parse(list1.Rows[i]["dateofapp"].ToString()));
                                 hInputPara1.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));
                                 sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_UpdateNewUploadDocument", hInputPara1);
                                }
                                else
                                {
                                    hInputPara1.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]));
                                    hInputPara1.Add("@RequestID", Convert.ToInt32(id));

                                    hInputPara1.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));
                                    try
                                    {
                                        var dateString = list1.Rows[i]["dateofapp"].ToString();
                                        var format = "dd/MM/yyyy";
                                        if (dateString != "")
                                        {
                                            var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                            hInputPara1.Add("@Date_Of_Approval", dateTime);
                                        }

                                    }
                                    catch(Exception e)
                                    {
                                        string dateString = list1.Rows[i]["dateofapp"].ToString();
                                        if (dateString != "")
                                        {

                                            string datestring1 = Convert.ToDateTime(dateString).ToString("dd/MM/yyyy");
                                            var format = "dd/MM/yyyy";
                                            var dateTime = DateTime.ParseExact(datestring1, format, CultureInfo.InvariantCulture);
                                            hInputPara1.Add("@Date_Of_Approval", dateTime);
                                        }
                                    }

                                    hInputPara1.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));
                                    sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_UpdateBlankDocument", hInputPara1);
                                }
                            }
                            else
                            {

                                hInputPara1.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]));

                                hInputPara1.Add("@Document_Name", fileName);
                                hInputPara1.Add("@RequestID", Convert.ToInt32(id));

                                hInputPara1.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));
                                try
                                {
                                    string dateString = list1.Rows[i]["dateofapp"].ToString();
                                    if (dateString != "")
                                    {

                                        string datestring1 = Convert.ToDateTime(dateString).ToString("dd/MM/yyyy");
                                        var format = "dd/MM/yyyy";
                                        var dateTime = DateTime.ParseExact(datestring1, format, CultureInfo.InvariantCulture);
                                        hInputPara1.Add("@Date_Of_Approval", dateTime);
                                    }
                                }
                                catch (Exception e)
                                {

                                    var dateString = list1.Rows[i]["dateofapp"].ToString();
                                    var format = "dd/MM/yyyy";
                                    if (dateString != "")
                                    {
                                        var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                        hInputPara1.Add("@Date_Of_Approval", dateTime);
                                    }

                                }
                                hInputPara1.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));
                                sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_InsertUploadDocMaster", hInputPara1);
                                //sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_InsertNewUploadDocument", hInputPara1);
                            }

                        }

                        hInputPara.Clear();
                        hInputPara1.Clear();
                        if ((list1.Rows[i]["docname"].ToString()) != null && (list1.Rows[i]["docname"].ToString()) != string.Empty)
                        {
                            hInputPara1.Add("@Document_Name", Convert.ToString(list1.Rows[i]["docname"]));
                            //  hInputPara1.Add("@Document_ID", Convert.ToString(list1.Rows[i]["docid"]));
                            sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjDocListNew", hInputPara1);
                        }
                        hInputPara1.Clear();
                    }
                   
                    return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new { success = true, Message = "NO records found" }, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public ActionResult DeleteImagePath(string ImageTypeId)
        {
            //011
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            string Request_ID = Session["Request_ID1"].ToString();
            string ImageID = ImageTypeId;
            hInputPara1.Add("@Request_ID", Convert.ToInt32(Request_ID));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            string projectId = Convert.ToString(ProjId.Rows[0]["Project_ID"]);
            string a = ImageID.PadLeft(2, '0');
            string ImagePath= Request_ID + "_" + projectId + "_" + a + "_";

            var filePath = Directory.GetFiles(Server.MapPath("~/images"));
            foreach (string filename in filePath)
            {
                if (filename.Contains(ImagePath))
                {
                    var Get = filename;
                    if (System.IO.File.Exists(filename))
                    {
                        System.IO.File.Delete(filename);
                        //Console.WriteLine("file Deleted");
                       // file.SaveAs(Server.MapPath("../images/") + FileSave);

                    }
                }
            }
            return RedirectToAction("ProjectUnitValuation", routeValues: new { controller = "ProjectValuationRequestor", activetab = "Uploadphotos" });
            return RedirectToAction("ProjectUnitValuation");
            return Json(new { success = true, Message = "Data Deleted Successfully" }, JsonRequestBehavior.AllowGet);

        }
        //upload Images
        [HttpPost]
        public JsonResult UploadImage(string ImageTypeId)
        {
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            string Request_ID = Session["Request_ID1"].ToString();
            string ImageID = ImageTypeId;
            hInputPara1.Add("@Request_ID", Convert.ToInt32(Request_ID));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            string projectId = Convert.ToString(ProjId.Rows[0]["Project_ID"]);
            string a = ImageID.PadLeft(2, '0');
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];

                var ImgName = Path.GetFileName(file.FileName);
                string fileName = ImgName;
                // Find its length and convert it to byte array
                int ContentLength = file.ContentLength;

                // Create Byte Array
                byte[] bytImg = new byte[ContentLength];

                // Read Uploaded file in Byte Array
                file.InputStream.Read(bytImg, 0, ContentLength);

                var ImageFile = Request_ID + "_" + projectId + "_" + a + "_" + fileName;

                var path = Path.Combine(Server.MapPath("~/images/"), ImageFile);
                file.SaveAs(path);
                
                Session["ImageSave"] = ImageFile;
                
            }

            return Json("Uploaded " + Request.Files.Count + " Images");
        }
        //upload Images
        [HttpPost]
        public JsonResult UploadImageEdit(string ImageTypeId)
        {
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            string Request_ID = Session["RequestIDProj"].ToString();
            string ImageID = ImageTypeId;
            hInputPara1.Add("@Request_ID", Convert.ToInt32(Request_ID));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            string projectId = Convert.ToString(ProjId.Rows[0]["Project_ID"]);
            string a = ImageID.PadLeft(2, '0');
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];

                var ImgName = Path.GetFileName(file.FileName);
                string fileName = ImgName;
                // Find its length and convert it to byte array
                int ContentLength = file.ContentLength;

                // Create Byte Array
                byte[] bytImg = new byte[ContentLength];

                // Read Uploaded file in Byte Array
                file.InputStream.Read(bytImg, 0, ContentLength);

                var ImageFile = Request_ID + "_" + projectId + "_" + a + "_" + fileName;
                var getFile = Directory.GetFiles(Server.MapPath("~/images"));
                foreach (string filename in getFile)
                {
                    if (filename.Contains(ImageFile))
                    {
                        var Get = filename;
                        if (System.IO.File.Exists(filename))
                        {
                            System.IO.File.Delete(filename);
                            //Console.WriteLine("file Deleted");
                            file.SaveAs(Server.MapPath("../images/") + ImageFile);

                        }
                    }
                    else
                    {

                        file.SaveAs(Server.MapPath("../images/") + ImageFile);

                        Dispose();
                    }

                }
                Session["ImageSaveEdit"] = ImageFile;
                TempData["TempImageEdit"] = ImageFile;

                //var path = Path.Combine(Server.MapPath("~/images/"), ImageFile);
                //file.SaveAs(path);

                //Session["ImageSave"] = ImageFile;

            }

            return Json("Uploaded " + Request.Files.Count + " Images");
        }
        [HttpPost]
        public ActionResult SaveAssignedEmp(string str)
        {
            try
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                //DataTable dtREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1RequestID");
                string Request_ID = Session["Request_ID1"].ToString();
                //changes by punam
                //string Request_ID = Session["RequestID"].ToString();
                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                for (int i = 0; i < list1.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@AssignedEmp", Convert.ToString(list1.Rows[i]["AssignedEmp"]));
                    //changes by punam
                    hInputPara.Add("@Request_ID", Convert.ToInt32(Request_ID));

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectAssignedEMP", hInputPara);

                }
                return Json(new { success = true, Message = "Employee Assigned Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 

        public ActionResult Edit(int? id)
        {
            Session["RequestIDProj"] = id;
            string ReqId = Session["RequestIDProj"].ToString();
            //if (rb != "")
            //{
            //    Session["ProjRadioValueEdit"] = rb;
            //}

            VMBuilderGroupMaster vm = new VMBuilderGroupMaster();
            vm.ival_ClientMasters = GetClientName();
            vm.ival_LookupCategoryDepartment = GetDepartment();
            vm.BuilderGroupList = GetBuilderGroup();
            vm.ival_LookupCategoryValuesreporttype = GetReportType();
            vm.ival_LookupCategoryValuesmethodvaluation = Getmethodtype();
            vm.ival_LookupCategoryPropertyType = GetProjectTypeMaster();
            vm.ival_BuilderCompanyMasterNewList = GetBuilderCompany();
            vm.ival_projectMasterDetailsList = GetBuilderProject();
            vm.projSiteEnginnerList = GetSiteEnginner();
            vm.ival_DocumentListsNew = GetDocumentsEdit();
            vm.ival_Document_Uploaded_ListS_New = GetUploadedFilesListNew(ReqId);
            DataTable DocID = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1DOCIDListNew");
            ViewBag.Srno = DocID.Rows[0]["Document_ID"].ToString();
            DataTable SrNo = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1DoclListNewSrNoId");
            ViewBag.SrnoNew = SrNo.Rows[0]["SrNo"].ToString();
            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@RequestID",Convert.ToInt32(id));
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectDetailsByRequestId",hInputPara);
                if (dtRequests.Rows.Count > 0)
                {

                    string date = Convert.ToDateTime(dtRequests.Rows[0]["dateOfRequest"]).ToString("dd/MM/yyyy");
                    ViewBag.Request_Date = date;
                    ViewBag.Valuation_Request_ID = Convert.ToString(dtRequests.Rows[0]["Valuation_Request_ID"]);
                    ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                    IEnumerable<ival_BuilderCompanyMasterNew> ival_BuilderCompanyMasterNewList = GetBuilderCompany();

                    vm.Builder_Company_Master_IDl = Convert.ToInt32(dtRequests.Rows[0]["Builder_Company_Master_ID"]);
                    
                    IEnumerable<ival_ProjectMasterDetails> ival_projectMasterDetailsList = GetBuilderProject();
                    vm.Builder_Project_Master_ID = Convert.ToInt32(dtRequests.Rows[0]["Project_ID"]);
                    IEnumerable<ProjectSiteEnginner> projSiteEnginnerList = GetSiteEnginner();
                    if (projSiteEnginnerList.ToList().Count > 0 && projSiteEnginnerList != null)
                    {
                        var siteEnginner = projSiteEnginnerList.Where(s => s.Site_Engineer_Name == dtRequests.Rows[0]["AssignedToEmpID"].ToString()).Select(s => s.Site_Engineer_ID);

                        vm.siteEnginnerId = Convert.ToInt32(siteEnginner.FirstOrDefault());

                    }
                    IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesreporttype = GetReportType();
                    if (ival_LookupCategoryValuesreporttype.ToList().Count > 0 && ival_LookupCategoryValuesreporttype != null)
                    {
                        var reportType = ival_LookupCategoryValuesreporttype.Where(s => s.Lookup_Value == dtRequests.Rows[0]["ReportType"].ToString()).Select(s => s.Lookup_ID);

                        vm.Selectedreporttype = Convert.ToInt32(reportType.FirstOrDefault());

                    }
                    IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesmethodvaluation =Getmethodtype();
                    if (ival_LookupCategoryValuesmethodvaluation.ToList().Count > 0 && ival_LookupCategoryValuesmethodvaluation != null)
                    {
                        var methodOfVal = ival_LookupCategoryValuesmethodvaluation.Where(s => s.Lookup_Value == dtRequests.Rows[0]["Method_Of_Valuation"].ToString()).Select(s => s.Lookup_ID);

                        vm.Selectedmethodvaluation = Convert.ToInt32(methodOfVal.FirstOrDefault());

                    }
                    IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryPropertyType = GetProjectTypeMaster();
                    if (ival_LookupCategoryPropertyType.ToList().Count > 0 && ival_LookupCategoryPropertyType != null)
                    {
                        var propertyType = ival_LookupCategoryPropertyType.Where(s => s.Lookup_Value == dtRequests.Rows[0]["PropertyType"].ToString()).Select(s => s.Lookup_ID);

                        vm.SelectPropertyType = Convert.ToInt32(propertyType.FirstOrDefault());

                    }

                    ViewBag.AssignedToEmpID = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                    //changes by punam
                    if (dtRequests.Rows[0]["Client_ID"] == DBNull.Value)
                    {
                        dtRequests.Rows[0]["Client_ID"] = 0;
                    }

                    if (Convert.ToInt32(dtRequests.Rows[0]["Client_ID"]) != 0)
                    {
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara = new Hashtable();
                        hInputPara.Add("@Client_ID", Convert.ToInt32(dtRequests.Rows[0]["Client_ID"].ToString()));
                        DataTable dtClient = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientMasterByID", hInputPara);
                        if (dtClient != null && dtClient.Rows.Count > 0)
                        {
                            ViewBag.Client_Name = dtClient.Rows[0]["Client_Name"].ToString();
                            ViewBag.SelectBankID = dtClient.Rows[0]["Client_Name"].ToString();
                            ViewBag.Branch_Name = dtClient.Rows[0]["Branch_Name"].ToString();
                            ViewBag.SelectBranchName = dtClient.Rows[0]["Branch_Name"].ToString();
                            vm.ival_BranchMasters = GetBranchNameNew(ViewBag.Client_Name);
                            // vm.ival_BranchMasters = GetBranchListByBankNameNew(ViewBag.Client_Name);
                            ViewBag.SelectBranchName = ViewBag.Branch_Name;
                        }

                    }
                    IEnumerable<ival_BuilderGroupMaster> BuilderGroupList = GetBuilderGroup();
                    vm.SelectedBuilderGroupMasterID = Convert.ToInt32(dtRequests.Rows[0]["Builder_Group_Master_ID"]);
                    //if (BuilderGroupList.ToList().Count > 0 && BuilderGroupList != null)
                    //{
                    //    var BuilderGroup = BuilderGroupList.Where(s => s.BuilderGroupName == dtRequests.Rows[0]["BuilderGroupName"].ToString()).Select(s => s.Builder_Group_Master_ID);

                    //    vm.SelectedBuilderGroupMasterID = Convert.ToInt32(BuilderGroup.FirstOrDefault());

                    //}
                    
                    if (dtRequests.Rows[0]["Department"].ToString() != "")
                    {
                        ViewBag.SelectDepartmentID = dtRequests.Rows[0]["Department"].ToString();
                    }
                    if (dtRequests.Rows[0]["Loan_Type"].ToString() != "")
                    {
                        ViewBag.SelectLoanTypeID = dtRequests.Rows[0]["Loan_Type"].ToString().Trim();
                    }
                    if (dtRequests.Rows[0]["Requestor_Name"].ToString() != "")
                    {
                        ViewBag.Requestor_Name = dtRequests.Rows[0]["Requestor_Name"].ToString();
                    }

                    if (Convert.ToInt32(dtRequests.Rows[0]["Client_ID"]) != 0)
                    {
                        ViewBag.Client_ID = Convert.ToInt32(dtRequests.Rows[0]["Client_ID"].ToString());

                    }

                }
            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);

        } 
        [HttpPost]
        public ActionResult Edit(string str,string rb,string strBuil,string strWing,int? projectId)
        {
            try
            {
                if(rb!="")
                {
                    Session["RadioProjValEdit"] = rb;
                }

                DataTable projectList = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                if (projectList != null && projectList.Rows.Count > 0)
                {
                    for (int i = 0; i < projectList.Rows.Count; i++)
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();

                        int requestId = Convert.ToInt32(Session["RequestIDProj"]);
                      
                        int client_id = 0;
                        if (Convert.ToString(projectList.Rows[i]["NameOfBank"]) != "")
                        {
                            string client_name = Convert.ToString(projectList.Rows[i]["NameOfBank"]);
                            string Branch_name = Convert.ToString(projectList.Rows[i]["BranchName"]);

                            client_id = GetClientId(client_name, Branch_name);
                        }
                        hInputPara.Add("@Request_Flag", "Update Request");
                        hInputPara.Add("@Client_ID", client_id);
                        hInputPara.Add("@Request_ID", requestId);
                        
                        hInputPara.Add("@ProjectId", Convert.ToInt32(projectList.Rows[i]["BuilderProject"]));
                        
                        
                        try
                        {
                            var dateString = projectList.Rows[i]["DateOfRequest"].ToString();
                            var format = "dd/MM/yyyy";
                            if (dateString != "")
                            {
                                var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                hInputPara.Add("@Request_Date", dateTime);
                            }
                        }
                        catch (Exception e)
                        {
                            string dateString = projectList.Rows[i]["DateOfRequest"].ToString();
                            if (dateString != "")
                            {

                                string datestring1 = Convert.ToDateTime(dateString).ToString("dd/MM/yyyy");
                                var format = "dd/MM/yyyy";
                                var dateTime = DateTime.ParseExact(datestring1, format, CultureInfo.InvariantCulture);
                                hInputPara.Add("@Request_Date", dateTime);
                            }
                        }


                        hInputPara.Add("@Requestor_Name", Convert.ToString(projectList.Rows[i]["nameOfRequestor"]));
                        hInputPara.Add("@Department", Convert.ToString(projectList.Rows[i]["department"]));
                        hInputPara.Add("@Valuation_Request_ID", Convert.ToString(projectList.Rows[i]["valuationRequestId"]));
                        if (projectList.Rows[i]["BuilderGroup"].ToString() != "")
                        {
                            hInputPara.Add("@BuilderGroupId", Convert.ToInt32(projectList.Rows[i]["BuilderGroup"]));
                        }
                        if (projectList.Rows[i]["BuilderCompany"].ToString() != "")
                        {
                            hInputPara.Add("@BuilderCompanyId", Convert.ToInt32(projectList.Rows[i]["BuilderCompany"]));
                        }

                        hInputPara.Add("@Method_of_Valuation", Convert.ToString(projectList.Rows[i]["methodOfValuation"]));
                        hInputPara.Add("@PropertyType", Convert.ToString(projectList.Rows[i]["PropertyType"]));
                        //hInputPara.Add("@UserId", Session["UserName"].ToString());
                        hInputPara.Add("@ReportType", Convert.ToString(projectList.Rows[i]["reportType"]));
                        hInputPara.Add("@UpdatedBy", Session["UserName"].ToString());
                        sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectRequestorDetails", hInputPara);

                        if (rb== "Part Of Project")
                        {
                            DataTable ProjBuilding = (DataTable)JsonConvert.DeserializeObject(strBuil, (typeof(DataTable)));
                            Session["ProjBuildingId"] = ProjBuilding;
                            DataTable ProjWing = (DataTable)JsonConvert.DeserializeObject(strWing, (typeof(DataTable)));
                            Session["ProjWingId"] = ProjWing;
                            for (int k = 0; k < ProjWing.Rows.Count; k++)
                            {
                                sqlDataAccess = new SQLDataAccess();
                                hInputPara = new Hashtable();
                                hInputPara1 = new Hashtable();
                                hInputPara1.Add("@projectId", projectId);
                                hInputPara1.Add("@wingId", ProjWing.Rows[k]["wingVal"]);
                                DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ProjBuildingIdByWingId", hInputPara1);
                                int BuildingIdNew = Convert.ToInt32(dtdoc.Rows[0]["Building_ID"]);
                                hInputPara1.Clear();
                                hInputPara.Add("@RequestId", requestId);
                                hInputPara.Add("@ProjectId", projectId);
                                hInputPara.Add("@buildingId", BuildingIdNew);
                                hInputPara.Add("@wingId", ProjWing.Rows[k]["wingVal"]);
                                string result2 = sqlDataAccess.ExecuteStoreProcedure("usp_GetProjValRequestLogUnique", hInputPara);
                                hInputPara.Clear();
                                if(result2!="document already exist")
                                {
                                    hInputPara.Add("@RequestId", requestId);
                                    hInputPara.Add("@ProjectId", projectId);
                                    hInputPara.Add("@buildingId", BuildingIdNew);
                                    hInputPara.Add("@IsEntireProje", false);
                                    hInputPara.Add("@createdBy", Session["UserName"].ToString());

                                    hInputPara.Add("@wingId", ProjWing.Rows[k]["wingVal"]);
                                    string result1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectValRequestLog", hInputPara);
                                    hInputPara.Clear();

                                }

                            }

                        }

                        return Json(new { success = true, Message = "Update Successfully" }, JsonRequestBehavior.AllowGet);

                    }
                }

            }
            catch (Exception e)
            {
                return Json(new { success = false, Message = "Does Not Updated Successfully" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, Message = "Does Not Updated Successfully" }, JsonRequestBehavior.AllowGet);

        }
        public List<ival_BuilderCompanyMasterNew> GetBuilderCompany()
        {
            List<ival_BuilderCompanyMasterNew> ListReporttype = new List<ival_BuilderCompanyMasterNew>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuilderCompanyMaster");
            //DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientName");
            for (int i = 0; i < dtGetReportType.Rows.Count; i++)
            {

                ListReporttype.Add(new ival_BuilderCompanyMasterNew
                {
                    Builder_Company_Master_ID = Convert.ToInt32(dtGetReportType.Rows[i]["Builder_Company_Master_ID"]),
                    Builder_Company_Name = Convert.ToString(dtGetReportType.Rows[i]["Builder_Company_Name"]),


                });

            }
            return ListReporttype;


        }
        public List<ival_ProjectMasterDetails> GetBuilderProject()
        {
            List<ival_ProjectMasterDetails> ListReporttype = new List<ival_ProjectMasterDetails>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuilderProjectMaster");
            //DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientName");
            for (int i = 0; i < dtGetReportType.Rows.Count; i++)
            {

                ListReporttype.Add(new ival_ProjectMasterDetails
                {
                    Project_ID = Convert.ToInt32(dtGetReportType.Rows[i]["Project_ID"]),
                    Project_Name = Convert.ToString(dtGetReportType.Rows[i]["Project_Name"]),


                });

            }
            return ListReporttype;


        }
        [HttpPost]
        public List<iva_UploadDocumentListNew> GetUploadedFilesListNew(string RequestID)
        {

            //List<ival_Document_Uploaded_List> listfiles = new List<ival_Document_Uploaded_List>();
            List<iva_UploadDocumentListNew> listfiles = new List<iva_UploadDocumentListNew>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@RequestId", RequestID);
            DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadedListNewForProject", hInputPara);
            for (int i = 0; i < dtdoc.Rows.Count; i++)
            {
                //ival_Document_Uploaded_List obj = new ival_Document_Uploaded_List();
                iva_UploadDocumentListNew obj = new iva_UploadDocumentListNew();
                //listfiles.Add(new ival_Document_Uploaded_List
                //{
                if ((dtdoc.Rows[i]["Docupload_ID"]) != DBNull.Value)
                {
                    obj.Docupload_ID = Convert.ToInt32(dtdoc.Rows[i]["Docupload_ID"]);
                }
                obj.Document_ID = Convert.ToInt32(dtdoc.Rows[i]["Document_ID"]);
                obj.SrNo = Convert.ToInt32(dtdoc.Rows[i]["SrNo"]);
                obj.Document_Name = Convert.ToString(dtdoc.Rows[i]["Document_Name"]);
                if ((dtdoc.Rows[i]["Uploaded_Document"]) != DBNull.Value)
                {
                    string uploaddoc = Convert.ToString(dtdoc.Rows[i]["Uploaded_Document"]);
                    string value = uploaddoc;
                    if (value != "")
                    {
                        //string[] newValue = value.Split('\\');
                        //if (newValue != null)
                        //{

                        string fileName = value;

                        obj.FileName = fileName;
                        var filePath = Directory.GetFiles(Server.MapPath("~/UploadFiles"));
                        foreach (string filename in filePath)
                        {
                            if (filename.Contains(fileName))
                            {
                                var Get = filename;
                                obj.Uploaded_Document = Get;
                            }
                        }
                        // }
                    }

                }
                if ((dtdoc.Rows[i]["Approval_Authority"]) != DBNull.Value)
                {
                    obj.Approval_Authority = Convert.ToString(dtdoc.Rows[i]["Approval_Authority"]);
                }
                if ((dtdoc.Rows[i]["Date_Of_Approval"]) != DBNull.Value)
                {
                    // var format = "dd/MM/yyyy";
                    // var date= dtdoc.Rows[i]["Date_Of_Approval"].ToString();
                    //var dateofapp= DateTime.ParseExact(date,format, CultureInfo.InvariantCulture);
                    // obj.Date_Of_Approval = dateofapp;
                    var a = Convert.ToDateTime(dtdoc.Rows[i]["Date_Of_Approval"]).Date;
                    var b = a.ToString("dd/MM/yyyy");
                    obj.Date_Of_Approval_New = b;
                    // obj.Date_Of_Approval = Convert.ToDateTime(dtdoc.Rows[i]["Date_Of_Approval"]).Date;
                    //obj.Date_Of_Approval =Convert.ToDateTime(b);
                }
                if ((dtdoc.Rows[i]["Approval_Number"]) != DBNull.Value)
                {
                    obj.Approval_Number = Convert.ToString(dtdoc.Rows[i]["Approval_Number"]);
                }
                listfiles.Add(obj);
                //});

            }
            return listfiles;
        }
        [HttpPost]
        public List<UploadedImageAndImageMaster> GetUploadedImageListEdit(string RequestID,string projectId)
        {

            //List<ival_Document_Uploaded_List> listfiles = new List<ival_Document_Uploaded_List>();
            List<UploadedImageAndImageMaster> listfiles = new List<UploadedImageAndImageMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@RequestId", RequestID);
            hInputPara.Add("@ProjectId", projectId);
            DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadedImagesForProjectEdit", hInputPara);
            for (int i = 0; i < dtdoc.Rows.Count; i++)
            {
                //ival_Document_Uploaded_List obj = new ival_Document_Uploaded_List();
                UploadedImageAndImageMaster obj = new UploadedImageAndImageMaster();
                //listfiles.Add(new ival_Document_Uploaded_List
                //{
                if ((dtdoc.Rows[i]["ImageUpload_ID"]) != DBNull.Value)
                {
                    obj.ImageUpload_ID = Convert.ToInt32(dtdoc.Rows[i]["ImageUpload_ID"]);
                }
                if ((dtdoc.Rows[i]["Seq_No"]) != DBNull.Value)
                {
                    string seqNo= dtdoc.Rows[i]["Seq_No"].ToString();
                    string Sequence= seqNo.PadLeft(2, '0');
                    obj.Seq_No = Convert.ToInt32(Sequence);
                }
                obj.Image_Type_ID = Convert.ToInt32(dtdoc.Rows[i]["Image_Type_ID"]);
                obj.Image_Type = Convert.ToString(dtdoc.Rows[i]["Image_Type"]);

                if ((dtdoc.Rows[i]["Image_Path"]) != DBNull.Value)
                {
                    string uploaddoc = Convert.ToString(dtdoc.Rows[i]["Image_Path"]);
                    string value = uploaddoc;
                    if (value != "")
                    {
                        //string[] newValue = value.Split('\\');
                        //if (newValue != null)
                        //{

                        string fileName = value;

                        obj.FilePath = fileName;
                        var filePath = Directory.GetFiles(Server.MapPath("~/images"));
                        foreach (string filename in filePath)
                        {
                            if (filename.Contains(fileName))
                            {
                                var Get = filename;
                                obj.Image_Path = Get;
                            }
                        }
                        // }
                    }

                }
               
                listfiles.Add(obj);
                //});

            }
            return listfiles;
        }
        [HttpPost]
        public List<UploadedImageAndImageMaster> GetUploadedImageListEditTest(string RequestID, string projectId)
        {

            //List<ival_Document_Uploaded_List> listfiles = new List<ival_Document_Uploaded_List>();
            
            List<UploadedImageAndImageMaster> listfiles = new List<UploadedImageAndImageMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtdoc1 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ProjectUploadImgeName");
            for (int j = 0; j < dtdoc1.Rows.Count; j++)
            {
                UploadedImageAndImageMaster obj = new UploadedImageAndImageMaster();
                List<ival_Project_Image_UploadedList> imageList = new List<ival_Project_Image_UploadedList>();
                hInputPara.Add("@RequestId", RequestID);
                hInputPara.Add("@ProjectId", projectId);
                int imageId = Convert.ToInt32(dtdoc1.Rows[j]["Image_Type_ID"]);
                obj.Image_Type_ID = imageId;
                obj.Image_Type = Convert.ToString(dtdoc1.Rows[j]["Image_Type"]);
                hInputPara.Add("@imageTypeId", imageId);
                
                DataTable dtdoc2 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadedImagesForProjectEditNew", hInputPara);
                hInputPara.Clear();
                for (int i = 0; i < dtdoc2.Rows.Count; i++)
                {
                    ival_Project_Image_UploadedList obj1 = new ival_Project_Image_UploadedList();
                    obj1.Seq_No= Convert.ToInt32(dtdoc2.Rows[i]["Seq_No"]);
                    if ((dtdoc2.Rows[i]["Image_Path"]) != DBNull.Value)
                    {
                        string uploaddoc = Convert.ToString(dtdoc2.Rows[i]["Image_Path"]);
                        string value = uploaddoc;
                        if (value != "")
                        {
                            //string[] newValue = value.Split('\\');
                            //if (newValue != null)
                            //{

                            string fileName = value;

                            obj1.FilePath = fileName;
                            var filePath = Directory.GetFiles(Server.MapPath("~/images"));
                            foreach (string filename in filePath)
                            {
                                if (filename.Contains(fileName))
                                {
                                    var Get = filename;
                                    obj1.Image_Path = Get;
                                }
                            }
                            // }
                        }

                    }
                    imageList.Add(obj1);
                }
                obj.uploadImageList = imageList;
                listfiles.Add(obj);
                hInputPara.Clear();
                hInputPara1.Clear();
            }
           
           
            return listfiles;
        }

        [HttpPost]
        public List<iva_UploadDocumentListNew> GetDocumentsEdit()
        {
            List<iva_UploadDocumentListNew> listdoc = new List<iva_UploadDocumentListNew>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadDocListsNewProjEdit");
            for (int i = 0; i < dtdoc.Rows.Count; i++)
            {
                listdoc.Add(new iva_UploadDocumentListNew
                {
                    Document_ID = Convert.ToInt32(dtdoc.Rows[i]["Document_ID"]),
                    Document_Name = Convert.ToString(dtdoc.Rows[i]["Document_Name"]),
                    SrNo = Convert.ToInt32(dtdoc.Rows[i]["SrNo"]),

                });

            }
            return listdoc;
        }
        public ActionResult DownloadNewEdit(string value)
        {
            var FilePath = "";
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestIDProj"]);
            var NewFilePath = TempData["TempFileEdit"];
            int documentId = Convert.ToInt32(value);
            hInputPara.Add("@RequestId", RequestID);
            hInputPara.Add("@DocumentId", documentId);
            DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadedFileName", hInputPara);
            if (dtRequests.Rows.Count > 0)
            {
                if (dtRequests.Rows[0]["Uploaded_Document"].ToString() != string.Empty && (dtRequests.Rows[0]["Uploaded_Document"].ToString()) != null)
                {
                    string fileName = Convert.ToString(dtRequests.Rows[0]["Uploaded_Document"]);
                    if (NewFilePath != null)
                    {
                        FilePath = NewFilePath.ToString();
                    }
                    else
                    {
                        FilePath = fileName;
                    }

                }

            }
            else
            {
                var FileGet = Session["FileEdit"];
                FilePath = FileGet.ToString();
            }


            var folderPath = Server.MapPath("~/UploadFiles");
            var fileCount = Directory.GetFiles(Server.MapPath("~/UploadFiles"));
            if (fileCount.Length > 1) // exactly only 1 file
            {
                string embed = "<object data=\"{0}\" type=\"application/pdf\"width=\"2000px\" height=\"1000px\">";
                embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
                embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                embed += "</object>";

                // TempData["Embed"] = string.Format(embed,"D://Punam Gat//04092020BackUp_IValue//ivalue//ivalue_codefirst//I_Value.WebApp//IValue//UploadFiles//" + FilePath);
                TempData["Embed"] = string.Format(embed, VirtualPathUtility.ToAbsolute("~/UploadFiles/" + FilePath));

                return Json("Exist", JsonRequestBehavior.AllowGet);
            }

            else
            {
                Process.Start(folderPath);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }
        //changes by punam
        [HttpPost]
        public ActionResult UpdateUploadFiles(string str, string str1)
        {
            try
            {

                //changes by punam
                DataTable list1 = new DataTable();
                DataTable list2 = new DataTable();
                //int b = str[0];
                if (str != null)
                {
                    list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                }
                if (str1 != null)
                {
                    list2 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));

                }

                hInputPara = new Hashtable();
                hInputPara1 = new Hashtable();

                sqlDataAccess1 = new SQLDataAccess();

                sqlDataAccess = new SQLDataAccess();
                int id = Convert.ToInt32(Session["RequestIDProj"]);


                if (list1.Rows.Count > 0)
                {

                    for (int i = 0; i < list1.Rows.Count; i++)
                    {
                        string fileName = "";
                        //get file name for adding prefix of request and documentId
                        string file = list1.Rows[i]["filename"].ToString();
                        if (file != "")
                        {
                            string[] newValue = file.Split('\\');
                            if (newValue != null)
                            {
                                string docId = list1.Rows[i]["docid"].ToString();
                                string DocID = docId.PadLeft(2, '0');
                                string Name = newValue[newValue.Length - 1];
                                fileName = id + "_" + DocID + "_" + Name;
                            }
                        }



                        if ((list1.Rows[i]["apprnum"].ToString()) != String.Empty && (list1.Rows[i]["apprnum"].ToString()) != null)
                        {
                            int docId = Convert.ToInt32(list1.Rows[i]["docid"]);
                            hInputPara.Add("@Document_ID_New", Convert.ToInt32(list1.Rows[i]["docid"]));
                            hInputPara.Add("@RequestID_New", Convert.ToInt32(id));
                            string result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertNewUploadDocument", hInputPara);
                            if (result == "document already exist")
                            {
                                sqlDataAccess = new SQLDataAccess();

                                if (list1.Rows[i]["filename"].ToString() != string.Empty && (list1.Rows[i]["filename"].ToString()) != null)
                                {
                                    hInputPara1.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]));
                                    hInputPara1.Add("@RequestID", Convert.ToInt32(id));

                                    //hInputPara1.Add("@Document_Name", Convert.ToString(list1.Rows[i]["filename"]));

                                    hInputPara1.Add("@Document_Name", fileName);

                                    hInputPara1.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));


                                    //hInputPara.Add("@Date_Of_Approval", Convert.ToDateTime(list1.Rows[i]["dateofapp"].ToString()));
                                    string dateString = list1.Rows[i]["dateofapp"].ToString();
                                    //DateTime date = Convert.ToDateTime(dateString);
                                    //string dateString1 = date.ToString("dd/MM/yyyy");
                                    var format = "dd/MM/yyyy";
                                    if (dateString != "")
                                    {
                                        var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                        hInputPara1.Add("@Date_Of_Approval", dateTime);
                                    }


                                    //  hInputPara.Add("@Date_Of_Approval", DateTime.Parse(list1.Rows[i]["dateofapp"].ToString()));
                                    hInputPara1.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));
                                    sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_UpdateNewUploadDocument", hInputPara1);
                                }
                                else
                                {
                                    hInputPara1.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]));
                                    hInputPara1.Add("@RequestID", Convert.ToInt32(id));

                                    hInputPara1.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));

                                    var dateString = list1.Rows[i]["dateofapp"].ToString();
                                    var format = "dd/MM/yyyy";
                                    if (dateString != "")
                                    {
                                        var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                        hInputPara1.Add("@Date_Of_Approval", dateTime);
                                    }

                                    hInputPara1.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));
                                    sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_UpdateBlankDocument", hInputPara1);
                                }
                            }
                            else
                            {
                                sqlDataAccess = new SQLDataAccess();

                                hInputPara1.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]));

                                hInputPara1.Add("@Document_Name", fileName);
                                hInputPara1.Add("@RequestID", Convert.ToInt32(id));

                                hInputPara1.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));
                                try
                                {
                                    string dateString = list1.Rows[i]["dateofapp"].ToString();
                                    if (dateString != "")
                                    {

                                        string datestring1 = Convert.ToDateTime(dateString).ToString("dd/MM/yyyy");
                                        var format = "dd/MM/yyyy";
                                        var dateTime = DateTime.ParseExact(datestring1, format, CultureInfo.InvariantCulture);
                                        hInputPara1.Add("@Date_Of_Approval", dateTime);
                                    }
                                }
                                catch (Exception e)

                                {

                                    var dateString = list1.Rows[i]["dateofapp"].ToString();
                                    var format = "dd/MM/yyyy";
                                    if (dateString != "")
                                    {
                                        var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                        hInputPara1.Add("@Date_Of_Approval", dateTime);
                                    }

                                }

                                hInputPara1.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));
                                sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_InsertUploadDocMaster", hInputPara1);
                                //sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_InsertNewUploadDocument", hInputPara1);
                            }

                        }

                        hInputPara.Clear();
                        hInputPara1.Clear();
                        if ((list1.Rows[i]["docname"].ToString()) != null && (list1.Rows[i]["docname"].ToString()) != string.Empty)
                        {
                            sqlDataAccess = new SQLDataAccess();

                            hInputPara1.Add("@Document_Name", Convert.ToString(list1.Rows[i]["docname"]));
                            //  hInputPara1.Add("@Document_ID", Convert.ToString(list1.Rows[i]["docid"]));
                            sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjDocListNew", hInputPara1);
                        }
                        hInputPara1.Clear();
                    }

                }
                //code for delete
                if (list2.Rows.Count > 0)
                {
                    sqlDataAccess = new SQLDataAccess();

                    var list = GetUploadedDocName(list2);
                    foreach (var c in list)
                    {
                        hInputPara1.Clear();
                        int documentId = c.Document_ID;
                        hInputPara1.Add("@DocumentID", documentId);
                        hInputPara1.Add("@RequestID", id);
                        sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_DeleteUploadedDocument", hInputPara1);

                    }

                }



                return Json(new { success = true, Message = "Insert,Update and Delete Successfully" }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                throw ex;
            }



        }
        [HttpPost]
        public List<ival_DocumentList> GetUploadedDocName(DataTable list2)
        {
            List<ival_DocumentList> listdoc = new List<ival_DocumentList>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            for (int i = 0; i < list2.Rows.Count; i++)
            {
                string a = list2.Rows[i]["value3"].ToString();

                hInputPara.Add("@DocumentName", list2.Rows[i]["value3"]);
                DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentIdProjNew", hInputPara);
                for (int j = 0; j < dtdoc.Rows.Count; j++)
                {
                    listdoc.Add(new ival_DocumentList
                    {
                        Document_ID = Convert.ToInt32(dtdoc.Rows[j]["Document_ID"]),
                        // Document_Name = Convert.ToString(dtdoc.Rows[i]["Document_Name"]),

                    });

                }
                hInputPara.Clear();
            }
            return listdoc;
        }
        /// <summary>
        /// this method is used for update assignemp
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>

        [HttpPost]
        public ActionResult SaveAssignedEmpEdit(string str)
        {
            try
            {

                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                //DataTable dtREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1RequestID");
                string Request_ID = Session["RequestIDProj"].ToString();
                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                for (int i = 0; i < list1.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@AssignedEmp", Convert.ToString(list1.Rows[i]["AssignedEmp"]));
                    //changes by punam
                    hInputPara.Add("@Request_ID", Convert.ToInt32(Request_ID));

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectAssignedEMP", hInputPara);

                }
                return Json(new { success = true, Message = "Employee Assigned Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 
        
        public ActionResult ViewProjectRequest(int ?id)
        {
            Session["RequestIDProj"] = id;
            string ReqId = Session["RequestIDProj"].ToString();
            VMBuilderGroupMaster vm = new VMBuilderGroupMaster();
            vm.ival_ClientMasters = GetClientName();
            vm.ival_LookupCategoryDepartment = GetDepartment();
            vm.BuilderGroupList = GetBuilderGroup();
            vm.ival_LookupCategoryValuesreporttype = GetReportType();
            vm.ival_LookupCategoryValuesmethodvaluation = Getmethodtype();
            vm.ival_LookupCategoryPropertyType = GetProjectTypeMaster();
            vm.ival_BuilderCompanyMasterNewList = GetBuilderCompany();
            vm.ival_projectMasterDetailsList = GetBuilderProject();
            vm.ival_Document_Uploaded_ListS = GetFilesList(ReqId); 
            //vm.ival_DocumentListsNew = GetDocumentsEdit();
            //vm.ival_Document_Uploaded_ListS_New = GetUploadedFilesListNew(ReqId);
            DataTable DocID = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1DOCIDListNew");
            ViewBag.Srno = DocID.Rows[0]["Document_ID"].ToString();
            DataTable SrNo = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1DoclListNewSrNoId");
            ViewBag.SrnoNew = SrNo.Rows[0]["SrNo"].ToString();
            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@RequestID", Convert.ToInt32(id));
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectDetailsByRequestId", hInputPara);
                if (dtRequests.Rows.Count > 0)
                {

                    string date = Convert.ToDateTime(dtRequests.Rows[0]["dateOfRequest"]).ToString("dd/MM/yyyy");
                    ViewBag.Request_Date = date;
                    ViewBag.Valuation_Request_ID = Convert.ToString(dtRequests.Rows[0]["Valuation_Request_ID"]);
                    ViewBag.RequestorName = Convert.ToString(dtRequests.Rows[0]["Requestor_Name"]);
                    vm.Builder_Company_Master_IDl = Convert.ToInt32(dtRequests.Rows[0]["Builder_Company_Master_ID"]);
                    IEnumerable<ival_BuilderCompanyMasterNew> ival_BuilderCompanyMasterNewList = GetBuilderCompany();

                    IEnumerable<ival_ProjectMasterDetails> ival_projectMasterDetailsList = GetBuilderProject();
                    vm.Builder_Project_Master_ID = Convert.ToInt32(dtRequests.Rows[0]["Project_ID"]);
                    //if (ival_projectMasterDetailsList.ToList().Count > 0 && ival_projectMasterDetailsList != null)
                    //{
                    //    var builderProject = ival_projectMasterDetailsList.Where(s => s.Project_Name == dtRequests.Rows[0]["Project_Name"].ToString()).Select(s => s.Project_ID);

                    //    vm.Builder_Project_Master_ID = Convert.ToInt32(builderProject.FirstOrDefault());


                    //}
                    IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesreporttype = GetReportType();
                    if (ival_LookupCategoryValuesreporttype.ToList().Count > 0 && ival_LookupCategoryValuesreporttype != null)
                    {
                        var reportType = ival_LookupCategoryValuesreporttype.Where(s => s.Lookup_Value == dtRequests.Rows[0]["ReportType"].ToString()).Select(s => s.Lookup_ID);

                        vm.Selectedreporttype = Convert.ToInt32(reportType.FirstOrDefault());

                    }
                    IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesmethodvaluation = Getmethodtype();
                    if (ival_LookupCategoryValuesmethodvaluation.ToList().Count > 0 && ival_LookupCategoryValuesmethodvaluation != null)
                    {
                        var methodOfVal = ival_LookupCategoryValuesmethodvaluation.Where(s => s.Lookup_Value == dtRequests.Rows[0]["Method_Of_Valuation"].ToString()).Select(s => s.Lookup_ID);

                        vm.Selectedmethodvaluation = Convert.ToInt32(methodOfVal.FirstOrDefault());

                    }
                    IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryPropertyType = GetProjectTypeMaster();
                    if (ival_LookupCategoryPropertyType.ToList().Count > 0 && ival_LookupCategoryPropertyType != null)
                    {
                        var propertyType = ival_LookupCategoryPropertyType.Where(s => s.Lookup_Value == dtRequests.Rows[0]["PropertyType"].ToString()).Select(s => s.Lookup_ID);

                        vm.SelectPropertyType = Convert.ToInt32(propertyType.FirstOrDefault());

                    }

                    ViewBag.AssignedToEmpID = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                    //changes by punam
                    if (dtRequests.Rows[0]["Client_ID"] == DBNull.Value)
                    {
                        dtRequests.Rows[0]["Client_ID"] = 0;
                    }

                    if (Convert.ToInt32(dtRequests.Rows[0]["Client_ID"]) != 0)
                    {
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara = new Hashtable();
                        hInputPara.Add("@Client_ID", Convert.ToInt32(dtRequests.Rows[0]["Client_ID"].ToString()));
                        DataTable dtClient = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientMasterByID", hInputPara);
                        if (dtClient != null && dtClient.Rows.Count > 0)
                        {
                            ViewBag.Client_Name = dtClient.Rows[0]["Client_Name"].ToString();
                            ViewBag.SelectBankID = dtClient.Rows[0]["Client_Name"].ToString();
                            ViewBag.Branch_Name = dtClient.Rows[0]["Branch_Name"].ToString();
                            ViewBag.SelectBranchName = dtClient.Rows[0]["Branch_Name"].ToString();
                            vm.ival_BranchMasters = GetBranchNameNew(ViewBag.Client_Name);
                            // vm.ival_BranchMasters = GetBranchListByBankNameNew(ViewBag.Client_Name);
                            ViewBag.SelectBranchName = ViewBag.Branch_Name;
                        }

                    }
                    IEnumerable<ival_BuilderGroupMaster> BuilderGroupList = GetBuilderGroup();
                    vm.SelectedBuilderGroupMasterID = Convert.ToInt32(dtRequests.Rows[0]["Builder_Group_Master_ID"]);
                    //if (BuilderGroupList.ToList().Count > 0 && BuilderGroupList != null)
                    //{
                    //    var BuilderGroup = BuilderGroupList.Where(s => s.BuilderGroupName == dtRequests.Rows[0]["BuilderGroupName"].ToString()).Select(s => s.Builder_Group_Master_ID);

                    //    vm.SelectedBuilderGroupMasterID = Convert.ToInt32(BuilderGroup.FirstOrDefault());

                    //}

                    if (dtRequests.Rows[0]["Department"].ToString() != "")
                    {
                        ViewBag.SelectDepartmentID = dtRequests.Rows[0]["Department"].ToString();
                    }
                    if (dtRequests.Rows[0]["Loan_Type"].ToString() != "")
                    {
                        ViewBag.SelectLoanTypeID = dtRequests.Rows[0]["Loan_Type"].ToString().Trim();
                    }
                    if (dtRequests.Rows[0]["Requestor_Name"].ToString() != "")
                    {
                        ViewBag.Requestor_Name = dtRequests.Rows[0]["Requestor_Name"].ToString();
                    }

                    if (Convert.ToInt32(dtRequests.Rows[0]["Client_ID"]) != 0)
                    {
                        ViewBag.Client_ID = Convert.ToInt32(dtRequests.Rows[0]["Client_ID"].ToString());

                    }

                }
            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return View(vm);

        }
        [HttpPost]
        public ActionResult ViewProjectRadioButton(string rb, string strBuil, string strWing, int? projectId)
        {
            try
            {
                if (rb != "")
                {
                    Session["RadioProjValView"] = rb;
                }

               if(rb== "Part Of Project")
                {
                    DataTable ProjBuilding = (DataTable)JsonConvert.DeserializeObject(strBuil, (typeof(DataTable)));
                    Session["ProjBuildingIdView"] = ProjBuilding;
                    DataTable ProjWing = (DataTable)JsonConvert.DeserializeObject(strWing, (typeof(DataTable)));
                    Session["ProjWingIdView"] = ProjWing;

                }
                //if (rb == "Part Of Project")
                //{
                //    DataTable ProjBuilding = (DataTable)JsonConvert.DeserializeObject(strBuil, (typeof(DataTable)));
                //    Session["ProjBuildingId"] = ProjBuilding;
                //    DataTable ProjWing = (DataTable)JsonConvert.DeserializeObject(strWing, (typeof(DataTable)));
                //    Session["ProjWingId"] = ProjWing;
                //    for (int k = 0; k < ProjWing.Rows.Count; k++)
                //    {
                //        sqlDataAccess = new SQLDataAccess();
                //        hInputPara = new Hashtable();
                //        hInputPara1 = new Hashtable();
                //        hInputPara1.Add("@projectId", projectId);
                //        hInputPara1.Add("@wingId", ProjWing.Rows[k]["wingVal"]);
                //        DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ProjBuildingIdByWingId", hInputPara1);
                //        int BuildingIdNew = Convert.ToInt32(dtdoc.Rows[0]["Building_ID"]);
                //        hInputPara1.Clear();
                //        hInputPara.Add("@RequestId", requestId);
                //        hInputPara.Add("@ProjectId", projectId);
                //        hInputPara.Add("@buildingId", BuildingIdNew);
                //        hInputPara.Add("@wingId", ProjWing.Rows[k]["wingVal"]);
                //        string result2 = sqlDataAccess.ExecuteStoreProcedure("usp_GetProjValRequestLogUnique", hInputPara);
                //        hInputPara.Clear();
                //        if (result2 != "document already exist")
                //        {
                //            hInputPara.Add("@RequestId", requestId);
                //            hInputPara.Add("@ProjectId", projectId);
                //            hInputPara.Add("@buildingId", BuildingIdNew);
                //            hInputPara.Add("@IsEntireProje", false);
                //            hInputPara.Add("@createdBy", Session["UserName"].ToString());

                //            hInputPara.Add("@wingId", ProjWing.Rows[k]["wingVal"]);
                //            string result1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectValRequestLog", hInputPara);
                //            hInputPara.Clear();

                //        }

                //    }

                //}

                return Json(new { success = true, Message = "Update Successfully" }, JsonRequestBehavior.AllowGet);

                    
                

            }
            catch (Exception e)
            {
                return Json(new { success = false, Message = "Does Not Updated Successfully" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, Message = "Does Not Updated Successfully" }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public List<ival_Document_Uploaded_List> GetFilesList(string RequestID)
        {

            List<ival_Document_Uploaded_List> listfiles = new List<ival_Document_Uploaded_List>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@RequestId", RequestID);
            DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadedProjectFileName", hInputPara);
            for (int i = 0; i < dtdoc.Rows.Count; i++)
            {
                ival_Document_Uploaded_List obj = new ival_Document_Uploaded_List();

                //listfiles.Add(new ival_Document_Uploaded_List
                //{
                if ((dtdoc.Rows[i]["Docupload_ID"]) != DBNull.Value)
                {
                    obj.Docupload_ID = Convert.ToInt32(dtdoc.Rows[i]["Docupload_ID"]);
                }
                obj.Document_ID = Convert.ToInt32(dtdoc.Rows[i]["Document_ID"]);
                obj.Document_Name = Convert.ToString(dtdoc.Rows[i]["Document_Name"]);
                if ((dtdoc.Rows[i]["Uploaded_Document"]) != DBNull.Value)
                {
                    obj.Uploaded_Document = Convert.ToString(dtdoc.Rows[i]["Uploaded_Document"]);
                }
                if ((dtdoc.Rows[i]["Approval_Authority"]) != DBNull.Value)
                {
                    obj.Approval_Authority = Convert.ToString(dtdoc.Rows[i]["Approval_Authority"]);
                }
                if ((dtdoc.Rows[i]["Date_Of_Approval"]) != DBNull.Value)
                {

                    obj.Date_Of_Approval = Convert.ToDateTime(dtdoc.Rows[i]["Date_Of_Approval"]).Date;

                }
                if ((dtdoc.Rows[i]["Approval_Number"]) != DBNull.Value)
                {
                    obj.Approval_Number = Convert.ToString(dtdoc.Rows[i]["Approval_Number"]);
                }
                listfiles.Add(obj);
                //});

            }
            return listfiles;
        }
        public JsonResult DownloadView(string docname)
        {


            var FilePath = docname;
            var folderPath = Server.MapPath("~/UploadFiles");
            var fileCount = Directory.GetFiles(Server.MapPath("~/UploadFiles"));
            if (fileCount.Length > 1) // exactly only 1 file
            {
                string embed = "<object data=\"{0}\" type=\"application/pdf\"width=\"2000px\" height=\"1000px\">";
                embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
                embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                embed += "</object>";
                TempData["Embed"] = string.Format(embed, VirtualPathUtility.ToAbsolute("~/UploadFiles/" + FilePath));
                return Json("Exist", JsonRequestBehavior.AllowGet);
            }
            
            else
            {
                Process.Start(folderPath);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }  
        public ActionResult ProjectUnitValuation()
        {
      
             string projectId=(Session["ProjectId"]).ToString();
            string radiovalues=Session["ProjRadioValue"].ToString();
            VMBuilderGroupMaster vm = new VMBuilderGroupMaster();
            vm.ival_ProjectUnitDescriptionsList=GetProjectUnitDescription();
            vm.ival_LookupCategoryValuesRiskofdemolition = Getdemolition();
            vm.ival_lookupCategoryAmenities=GetProjectAmenitiesDescription();
            vm.ival_lookupProjectMaintainceProperty=GetMaintanceProperty();
            vm.ival_lookupProjectQualityConstruction=GetQualityOfConstruction();
            vm.ival_ProjectImageName=GetProjectMasterImageName();
            if (radiovalues== "Entire Project")
            {
                vm.ival_ProjectBuildingMasters = GetBuildingsByEntireProject(projectId);
            }
            else
            {
               vm.ival_ProjectBuildingMasters=GetBuildingForPartofProj(projectId);
            }


            vm.ival_lookupProjectSideMargin = GetSideMargin();
            return View(vm);
        }
        //Insert Project Valuation Details
        [HttpPost]
        public ActionResult ProjectUnitValuation(string str,string str1,string str2,string str3,string str4,string str5,string str6,string str7)
        {
            //int projectId = Convert.ToInt32(Session["ProjectId"]);
           // string Request_ID = Session["RequestIDProj"].ToString();
            string Request_ID = Session["Request_ID1"].ToString();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(Request_ID));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            int projectId = Convert.ToInt32(ProjId.Rows[0]["Project_ID"]);
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            //DataTable list2 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));
            //DataTable list3 = (DataTable)JsonConvert.DeserializeObject(str2, (typeof(DataTable)));
            //DataTable list4 = (DataTable)JsonConvert.DeserializeObject(str3, (typeof(DataTable)));
            DataTable list5 = (DataTable)JsonConvert.DeserializeObject(str4, (typeof(DataTable)));
            DataTable list6 = (DataTable)JsonConvert.DeserializeObject(str5, (typeof(DataTable)));
            DataTable list7 = (DataTable)JsonConvert.DeserializeObject(str6, (typeof(DataTable)));
            DataTable list8 = (DataTable)JsonConvert.DeserializeObject(str7, (typeof(DataTable)));
            for (int i = 0; i < list1.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@projectId", projectId);
                try
                {
                    var dateString = list1.Rows[i]["dateOfInspection"].ToString();
                    var format = "dd/MM/yyyy";
                    if (dateString != "")
                    {
                        var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                        hInputPara.Add("@Date_Of_Inspection", dateTime);
                    }
                    else
                    {
                        hInputPara.Add("@Date_Of_Inspection", DateTime.Now);
                    }
                }
                catch (Exception e)
                {
                    string dateString = list1.Rows[i]["dateOfInspection"].ToString();
                    if (dateString != "")
                    {

                        string datestring1 = Convert.ToDateTime(dateString).ToString("dd/MM/yyyy");
                        var format = "dd/MM/yyyy";
                        var dateTime = DateTime.ParseExact(datestring1, format, CultureInfo.InvariantCulture);
                        hInputPara.Add("@Date_Of_Inspection", dateTime);
                    }
                    else
                    {
                        hInputPara.Add("@Date_Of_Inspection", DateTime.Now);
                    }
                }

                // hInputPara.Add("@Date_Of_Inspection", Convert.ToInt32(list1.Rows[i]["dateOfInspection"]));
                hInputPara.Add("@Engineer_Visiting_Site", Convert.ToString(list1.Rows[i]["EnginnerVisitingTheSite"]));
                hInputPara.Add("@Person_Met_at_Site", Convert.ToString(list1.Rows[i]["personMetatsite"]));
                if (list1.Rows[i]["contactNumberatperson"].ToString() == "")
                {
                    var contactNumber = 0;
                    hInputPara.Add("@Person_Contact_Number", contactNumber);
                }
                else
                {
                    hInputPara.Add("@Person_Contact_Number", Convert.ToString(list1.Rows[i]["contactNumberatperson"]));
                }

                hInputPara.Add("@Any_Deviation_observed", Convert.ToString(list1.Rows[i]["anyDeviationObserved"]));
                hInputPara.Add("@Risk_of_Demolition", Convert.ToString(list1.Rows[i]["riskOfDemoliation"]));
                hInputPara.Add("@Deviation_Description", Convert.ToString(list1.Rows[i]["deviationDescription"]));
                if (list1.Rows[i]["RateRangeInLocality"].ToString() == "")
                {
                    var RateRange = 0;
                    hInputPara.Add("@rateRangeInLocality", RateRange);
                }
                else
                {

                    hInputPara.Add("@rateRangeInLocality", Convert.ToString(list1.Rows[i]["RateRangeInLocality"]));
                }
                string GstApplicable = list1.Rows[i]["GstApplicable"].ToString();
                if (GstApplicable == "No")
                {
                    hInputPara.Add("@GstApplicable", false);
                }
                if (GstApplicable == "Yes")
                {
                    hInputPara.Add("@GstApplicable", true);
                }
                if (list1.Rows[i]["parkingCharges"].ToString() == "")
                {
                    var parkingCharges = 0;
                    hInputPara.Add("@parkingCharges", parkingCharges);
                }
                else
                {
                    hInputPara.Add("@parkingCharges", Convert.ToString(list1.Rows[i]["parkingCharges"]));
                }
                if (list1.Rows[i]["ClubMembershipCharges"].ToString() == "")
                {
                    var clubCharges = 0;
                    hInputPara.Add("@clubMembershipCharges", clubCharges);
                }
                else
                {

                    hInputPara.Add("@clubMembershipCharges", Convert.ToString(list1.Rows[i]["ClubMembershipCharges"]));
                }
                if (list1.Rows[i]["OneTimeMaintanceCharges"].ToString() == "")
                {
                    var oneTimeMaintanceCharges = 0;
                    hInputPara.Add("@oneTimeMaintanceCharges", oneTimeMaintanceCharges);
                }
                else
                {

                    hInputPara.Add("@oneTimeMaintanceCharges", Convert.ToString(list1.Rows[i]["OneTimeMaintanceCharges"]));
                }
                if (list1.Rows[i]["AnyOtherCharges"].ToString() == "")
                {
                    var anyOtherCharges = 0;
                    hInputPara.Add("@anyOtherCharges", anyOtherCharges);
                }
                else
                {

                    hInputPara.Add("@anyOtherCharges", Convert.ToString(list1.Rows[i]["AnyOtherCharges"]));
                }
                if (list1.Rows[i]["GovermentRateOfUnit"].ToString() == "")
                {
                    var GovRate = 0;
                    hInputPara.Add("@GovermentRateOfUnit", GovRate);
                }
                else
                {

                    hInputPara.Add("@GovermentRateOfUnit", Convert.ToString(list1.Rows[i]["GovermentRateOfUnit"]));
                }
                hInputPara.Add("@remark", Convert.ToString(list1.Rows[i]["Remark"]));
                if (list1.Rows[i]["AdpotedBaseRate"].ToString() == "")
                {
                    var AdpotedBaseRate = 0;
                    hInputPara.Add("@adpotedBaseRate", AdpotedBaseRate);
                }
                else
                {

                    hInputPara.Add("@adpotedBaseRate", Convert.ToString(list1.Rows[i]["AdpotedBaseRate"]));
                }
                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectVisitDetails", hInputPara);
                hInputPara.Clear();
                
                

            }
            
            
            //Amenities Details
            for (int i = 0; i < list5.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@projectId", projectId);
                hInputPara.Add("@RequestId", Convert.ToInt32(Request_ID));
                hInputPara.Add("@AmenityDescription", Convert.ToString(list5.Rows[i]["descriptionOfAmenities"]));
                hInputPara.Add("@DescriptionOfStageConstruction", Convert.ToString(list5.Rows[i]["DescriptionOfstage"]));
                if(list5.Rows[i]["PerWorkcomplet"].ToString()=="")
                {
                    int workComplete = 0;
                    hInputPara.Add("@percentageWorkComplete", workComplete);
                }
                else
                {
                    hInputPara.Add("@percentageWorkComplete", Convert.ToInt32(list5.Rows[i]["PerWorkcomplet"]));
                }
                if (list5.Rows[i]["DisbursementRecomm"].ToString() == "")
                {
                    int disBursementRecomm = 0;
                    hInputPara.Add("@percentageDisbursementRecommended", disBursementRecomm);
                }
                else
                {
                    hInputPara.Add("@percentageDisbursementRecommended", Convert.ToInt32(list5.Rows[i]["DisbursementRecomm"]));
                }
               

                hInputPara.Add("@maintenanceOfProperty", Convert.ToString(list5.Rows[i]["maintanceOfProperty"]));
                hInputPara.Add("@QualityOfConstruction", Convert.ToString(list5.Rows[i]["QualityOfConstr"]));
                
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectValuationAmenities", hInputPara);
                hInputPara.Clear();
                if ((list5.Rows[i]["descriptionOfAmenities"].ToString()) != null && (list5.Rows[i]["descriptionOfAmenities"].ToString()) != string.Empty)
                {
                    hInputPara1 = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara1.Add("@lookUpValue", Convert.ToString(list5.Rows[i]["descriptionOfAmenities"]));
                    //  hInputPara1.Add("@Document_ID", Convert.ToString(list1.Rows[i]["docid"]));
                    sqlDataAccess.ExecuteStoreProcedure("usp_InsertLookUpAmenities", hInputPara1);
                }
            }
            //Broker Details
            for (int i = 0; i < list6.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@Project_ID", projectId);
                hInputPara.Add("@RequestId", Convert.ToInt32(Request_ID));
                if(Convert.ToString(list6.Rows[i]["nameOfBroker"])!="")
                {
                    hInputPara.Add("@Broker_Name", Convert.ToString(list6.Rows[i]["nameOfBroker"]));
                    hInputPara.Add("@Firm_Name", Convert.ToString(list6.Rows[i]["nameOfFirm"]));

                    hInputPara.Add("@Contact_Num", Convert.ToString(list6.Rows[i]["contactNo"]));
                    hInputPara.Add("@Rate_Quoted_For_Land", Convert.ToString(list6.Rows[i]["rateRangeQuotedforLand"]));
                    hInputPara.Add("@Rate_Quoted_For_Flat", Convert.ToString(list6.Rows[i]["rateRangeQuotedforFlat"]));
                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertBrokerDetailsNew", hInputPara);

                }
                hInputPara.Clear();
               
            }
            //Comparable Details
            for (int i = 0; i < list7.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@Project_ID", projectId);
                hInputPara.Add("@RequestId", Convert.ToInt32(Request_ID));
                if(Convert.ToString(list7.Rows[i]["nameOfProj"])!="")
                {
                    hInputPara.Add("@Project_Name", Convert.ToString(list7.Rows[i]["nameOfProj"]));
                    hInputPara.Add("@Dist_Subject_Property", Convert.ToString(list7.Rows[i]["distaneSub"]));

                    hInputPara.Add("@Comparable_Unit_Details", Convert.ToString(list7.Rows[i]["unitDetailsComparable"]));
                    hInputPara.Add("@Quality_And_Amenities", Convert.ToString(list7.Rows[i]["quality"]));
                    hInputPara.Add("@Ongoing_Rate", Convert.ToString(list7.Rows[i]["ongoingRate"]));
                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertCompPropNew", hInputPara);
                }
               
                hInputPara.Clear();

            } 
            //for UploadImages
            for(int i=0;i<list8.Rows.Count;i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                if (list8.Rows[i]["Imagepath"].ToString() != string.Empty && list8.Rows[i]["Imagepath"].ToString() != null)
                {
                    string ImagePath = "";
                    hInputPara.Add("@Project_ID", projectId);
                    hInputPara.Add("@RequestId", Convert.ToInt32(Request_ID));
                    hInputPara.Add("@ImageTypeId", Convert.ToInt32(list8.Rows[i]["ImageTypeId"]));
                    string ImageName = list8.Rows[i]["Imagepath"].ToString();
                    if(ImageName!="")
                    {
                        string[] newValue = ImageName.Split('\\');
                        if (newValue != null)
                        {
                            string ImageTypeId = Convert.ToString(list8.Rows[i]["ImageTypeId"]);
                            string imagesqNO = list8.Rows[i]["ImageSequNo"].ToString();
                            string ImageTypeIdNew = imagesqNO + ImageTypeId;
                            string ImTypeId = ImageTypeIdNew.PadLeft(2, '0');
                            string Name = newValue[newValue.Length - 1];
                            ImagePath = Request_ID + "_" + projectId + "_" + ImTypeId + "_" + Name;
                        }
                    }
                    hInputPara.Add("@ImagePath", ImagePath);
                    hInputPara.Add("@SequenceNo", Convert.ToInt32(list8.Rows[i]["ImageSequNo"]));
                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjUploadImages", hInputPara);
                    hInputPara.Clear();
                }

            }
            return Json(new { success = true, Message = "Inserted Successfully" }, JsonRequestBehavior.AllowGet);

            
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> Getdemolition()
        {
            List<ival_LookupCategoryValues> ListDemo = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtdemolition = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRiskOfDemolition");
            for (int i = 0; i < dtdemolition.Rows.Count; i++)
            {

                ListDemo.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtdemolition.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtdemolition.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtdemolition.Rows[i]["Lookup_Value"])

                });

            }
            return ListDemo;
        }
        public List<ival_ProjectUnitDescription> GetProjectUnitDescription()
        {
            List<ival_ProjectUnitDescription> projectDes = new List<ival_ProjectUnitDescription>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtGetProjDes = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ProjectUnitDescription");
            //DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientName");
            for (int i = 0; i < dtGetProjDes.Rows.Count; i++)
            {
                ival_ProjectUnitDescription list = new ival_ProjectUnitDescription();
                list.Desc_Unit_ID= Convert.ToInt32(dtGetProjDes.Rows[i]["Desc_Unit_ID"]);
                list.Description_of_Units = Convert.ToString(dtGetProjDes.Rows[i]["Description_of_Units"]);
                list.Type = Convert.ToString(dtGetProjDes.Rows[i]["Type"]);
                
                projectDes.Add(list);
            }
            return projectDes;

        }
        public List<ival_ImageMaster> GetProjectMasterImageName()
        {
            
            List<ival_ImageMaster> projectImage = new List<ival_ImageMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtGetProjDes = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ProjectUploadImgeName");
            //DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientName");
            for (int i = 0; i < dtGetProjDes.Rows.Count; i++)
            {
                ival_ImageMaster list = new ival_ImageMaster();
                list.Image_Type_ID = Convert.ToInt32(dtGetProjDes.Rows[i]["Image_Type_ID"]);
                list.Image_Type = Convert.ToString(dtGetProjDes.Rows[i]["Image_Type"]);

                projectImage.Add(list);
            }
            return projectImage;

        }

        public List<ival_LookupCategoryValues> GetProjectAmenitiesDescription()
        {
            List<ival_LookupCategoryValues> projectDes = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtGetProjDes = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBuildingAmenitiesDes");
            //DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientName");
            for (int i = 0; i < dtGetProjDes.Rows.Count; i++)
            {
                ival_LookupCategoryValues list = new ival_LookupCategoryValues();
                list.Lookup_ID = Convert.ToInt32(dtGetProjDes.Rows[i]["Lookup_ID"]);
                list.Lookup_Type = Convert.ToString(dtGetProjDes.Rows[i]["Lookup_Type"]);
                list.Lookup_Value = Convert.ToString(dtGetProjDes.Rows[i]["Lookup_Value"]);

                projectDes.Add(list);
            }
            return projectDes;

        }
        [HttpPost]
        public JsonResult GetBuildingByProjectId(int? Id)
        {
            if (Id != null)
            {
                try
                {
                    
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@projectId", Id);
                    DataTable dtBuilder = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ProjectBuildingByProjectId", hInputPara);

                    List<ival_ProjectBuildingMaster> ival_BuilderList = new List<ival_ProjectBuildingMaster>();
                 
                    if (dtBuilder.Rows.Count > 0 && dtBuilder != null)

                    {
                        for (int i = 0; i < dtBuilder.Rows.Count; i++)
                        {

                            ival_BuilderList.Add(new ival_ProjectBuildingMaster
                            {
                                Building_ID = Convert.ToInt32(dtBuilder.Rows[i]["Building_ID"]),
                               
                                Building_Name = Convert.ToString(dtBuilder.Rows[i]["Building_Name"]),
                               

                            });

     
                        }
                    }
                    
                    var jsonSerialiser = new JavaScriptSerializer();
                    var jsonBuilder = jsonSerialiser.Serialize(ival_BuilderList);
                    return Json(jsonBuilder);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("Error in Link", JsonRequestBehavior.AllowGet);
            }

        }
        //get building list for project Id Edit
        [HttpPost]
        public JsonResult GetBuildingByProjectIdEdit()
        {
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            string ReqId = Session["RequestIDProj"].ToString();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(ReqId));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            string ProjectId = Convert.ToString(ProjId.Rows[0]["Project_ID"]);
            hInputPara1.Clear();

            if (ProjectId != "")
            {
                try
                {

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@projectId",Convert.ToInt32(ProjectId));
                    hInputPara.Add("@requestId", Convert.ToInt32(ReqId));
                    DataTable dtBuilder = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectBuildingPartOfProjListEdit", hInputPara);
                    hInputPara.Clear();
                    List<ProjectValBuildingRequestLogEdit> ival_BuilderList = new List<ProjectValBuildingRequestLogEdit>();

                    if (dtBuilder.Rows.Count > 0 && dtBuilder != null)

                    {
                        for (int i = 0; i < dtBuilder.Rows.Count; i++)
                        {
                            ProjectValBuildingRequestLogEdit data = new ProjectValBuildingRequestLogEdit();
                            if(dtBuilder.Rows[i]["Building_ID"]!=DBNull.Value)
                            {
                                data.BuildingId = Convert.ToInt32(dtBuilder.Rows[i]["Building_ID"]);
                            }
                            
                            data.BuildingName = Convert.ToString(dtBuilder.Rows[i]["Building_Name"]);
                            if (dtBuilder.Rows[i]["ReqBuildingId"] != DBNull.Value)
                            {
                                data.ProjectValuationRequestID = Convert.ToInt32(dtBuilder.Rows[i]["ReqBuildingId"]);
                            }
                            
                            ival_BuilderList.Add(data);

                        }
                    }

                    var jsonSerialiser = new JavaScriptSerializer();
                    var jsonBuilder = jsonSerialiser.Serialize(ival_BuilderList);
                    return Json(jsonBuilder);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("Error in Link", JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public JsonResult GetWingsByBuilding(string strCheck, int? projectId)
        {
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(strCheck, (typeof(DataTable)));
            for (int i = 0; i < list1.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
               
                try
                {

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@projectId", projectId);

                    int buildingId = Convert.ToInt32(list1.Rows[i]["value3"]);
                    hInputPara.Add("@buildingID", buildingId);
                    DataTable dtBuilder = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBuilWingsByProjBuilding", hInputPara);

                    List<ProjectBuildingWIngDetails> ival_BuilderList = new List<ProjectBuildingWIngDetails>();

                    if (dtBuilder.Rows.Count > 0 && dtBuilder != null)

                    {
                        for (int j = 0; j < dtBuilder.Rows.Count; j++)
                        {

                            ival_BuilderList.Add(new ProjectBuildingWIngDetails
                            {
                                WingID = Convert.ToInt32(dtBuilder.Rows[j]["Wing_ID"]),

                                WingName = Convert.ToString(dtBuilder.Rows[j]["Wing_Name"]),
                                BuildingId = Convert.ToInt32(dtBuilder.Rows[j]["Building_ID"]),
                                BuildingName = Convert.ToString(dtBuilder.Rows[j]["Building_Name"]),

                            });


                        }
                    }

                    var jsonSerialiser = new JavaScriptSerializer();
                    var jsonBuilder = jsonSerialiser.Serialize(ival_BuilderList);
                    return Json(jsonBuilder);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            return Json("");

        }
        [HttpPost]
        public JsonResult GetWingsByBuildingTest(string strCheck, int? projectId)
        {
            var jsonBuilder = "";
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(strCheck, (typeof(DataTable)));
            List<ProjectBuildingWIngDetails> Plist = new List<ProjectBuildingWIngDetails>();

            for (int i = 0; i < list1.Rows.Count; i++)
            {
                List<ival_ProjBldgWingMaster> bldWingList = new List<ival_ProjBldgWingMaster>();

                ProjectBuildingWIngDetails obj = new ProjectBuildingWIngDetails();

                hInputPara1 = new Hashtable();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@projectId", projectId);

                int buildingId = Convert.ToInt32(list1.Rows[i]["value3"]);
                //string BuildingName= Convert.ToString(list1.Rows[i]["value4"]);
                obj.BuildingId = buildingId;
                hInputPara1.Add("@buildingID", buildingId);
                DataTable dtBuilder1 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBuilndingNameByID", hInputPara1);
                obj.BuildingName = Convert.ToString(dtBuilder1.Rows[0]["Building_Name"]);
                hInputPara.Add("@buildingID", buildingId);
                DataTable dtBuilder = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBuilWingsByProjBuilding", hInputPara);

                if (dtBuilder.Rows.Count > 0 && dtBuilder != null)

                {
                    for (int j = 0; j < dtBuilder.Rows.Count; j++)
                    {
                        //obj.BuildingName = Convert.ToString(dtBuilder.Rows[j]["Building_Name"]);
                       // obj.BuildingId= Convert.ToInt32(dtBuilder.Rows[j]["Building_ID"]);

                        ival_ProjBldgWingMaster wingObj = new ival_ProjBldgWingMaster();
                        
                        wingObj.Wing_ID = Convert.ToInt32(dtBuilder.Rows[j]["Wing_ID"]);
                        wingObj.Wing_Name = Convert.ToString(dtBuilder.Rows[j]["Wing_Name"]);


                        bldWingList.Add(wingObj);
                    }
                }
                obj.bldWingListNew = bldWingList;
                Plist.Add(obj);

                hInputPara.Clear();
                hInputPara1.Clear();
                var jsonSerialiser = new JavaScriptSerializer();
                jsonBuilder = jsonSerialiser.Serialize(Plist);
                
            }
            return Json(jsonBuilder);

        }
        //GetBuildingnadWings PartOf Proj Edit//
        [HttpPost]
        public JsonResult GetWingsByBuildingPartOfProjEdit(int? projectId)
        {
            var jsonBuilder = "";
            hInputPara1 = new Hashtable();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
           // List<ival_ProjBldgWingMaster> bldWingList = new List<ival_ProjBldgWingMaster>();

            //DataTable list1 = (DataTable)JsonConvert.DeserializeObject(strCheck, (typeof(DataTable)));
            List<ProjectBuildingWIngDetails> Plist = new List<ProjectBuildingWIngDetails>();
            int requestId = Convert.ToInt32(Session["RequestIDProj"]);
            hInputPara.Add("@projectId", projectId);
            hInputPara.Add("@RequestID", requestId);
            DataTable dtBuilder3 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjValBuildingIdForPartOfproj", hInputPara);
            hInputPara.Clear();
            for(int j=0;j<dtBuilder3.Rows.Count;j++)
            {
                int buildingId=Convert.ToInt32(dtBuilder3.Rows[j]["Building_ID"]);

                hInputPara.Add("@projectId", projectId);
                hInputPara.Add("@RequestID", requestId);
                hInputPara.Add("@buildingId", buildingId);
                DataTable dtBuilder2 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjValWingsForPartOfProj", hInputPara);
                hInputPara.Clear();
                for (int i = 0; i < dtBuilder2.Rows.Count; i++)
                {
                    //List<ival_ProjBldgWingMaster> bldWingList = new List<ival_ProjBldgWingMaster>();

                    ProjectBuildingWIngDetails obj = new ProjectBuildingWIngDetails();

                    hInputPara1 = new Hashtable();
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    //hInputPara.Add("@projectId", projectId);

                    

                    //string BuildingName= Convert.ToString(list1.Rows[i]["value4"]);
                    obj.BuildingId = buildingId;
                    hInputPara1.Add("@buildingID", buildingId);
                    DataTable dtBuilder1 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBuilndingNameByID", hInputPara1);
                    obj.BuildingName = Convert.ToString(dtBuilder1.Rows[0]["Building_Name"]);
                    if (dtBuilder2.Rows[i]["Wing_ID"] != DBNull.Value)
                    {
                        obj.WingID = Convert.ToInt32(dtBuilder2.Rows[i]["Wing_ID"]);
                    }

                    obj.WingName = Convert.ToString(dtBuilder2.Rows[i]["Wing_Name"]);
                    if (dtBuilder2.Rows[i]["ProjectValuationLogID"] != DBNull.Value)
                    {
                        obj.ProjectValuationLogId = Convert.ToInt32(dtBuilder2.Rows[i]["ProjectValuationLogID"]);

                    }

                    Plist.Add(obj);

                    hInputPara.Clear();
                    hInputPara1.Clear();
                    var jsonSerialiser = new JavaScriptSerializer();
                    jsonBuilder = jsonSerialiser.Serialize(Plist);

                }

            }





            return Json(jsonBuilder);

        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetMaintanceProperty()
        {
            List<ival_LookupCategoryValues> ListDemo = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetMaintenanceOfProperty");
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                ListDemo.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dt.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dt.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dt.Rows[i]["Lookup_Value"])

                });

            }
            return ListDemo;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetQualityOfConstruction()
        {
            List<ival_LookupCategoryValues> ListDemo = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetQualityOfConstruction");
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                ListDemo.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dt.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dt.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dt.Rows[i]["Lookup_Value"])

                });

            }
            return ListDemo;
        }

        public ActionResult EditProjectValuation()
        {
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            VMBuilderGroupMaster vm = new VMBuilderGroupMaster();
            vm.ival_ProjectUnitDescriptionsList = GetProjectUnitDescription();
            vm.ival_LookupCategoryValuesRiskofdemolition = Getdemolition();
            vm.ival_lookupCategoryAmenities = GetProjectAmenitiesDescription();
            vm.ival_lookupProjectMaintainceProperty = GetMaintanceProperty();
            vm.ival_lookupProjectQualityConstruction = GetQualityOfConstruction();
            vm.ival_lookupProjectSideMargin = GetSideMargin();
            vm.ival_ProjectImageName = GetProjectMasterImageName();
           
            string radiovalues = Session["RadioProjValEdit"].ToString();
            
            string ReqId = Session["RequestIDProj"].ToString();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(ReqId));
            DataTable ProjId= sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId",hInputPara1);
            string ProjectId= Convert.ToString(ProjId.Rows[0]["Project_ID"]);
            hInputPara1.Clear();
            //vm.UploadedImagesList = GetUploadedImageListEditTest(ReqId, ProjectId);
            vm.UploadedImagesList = GetUploadedImageListEdit(ReqId, ProjectId);
            foreach (var data in vm.UploadedImagesList)
            {
                if(data.Image_Type== "Internal View")
                {
                    if(data.Seq_No==1)
                    {
                        ViewBag.FilePathIn1 = data.FilePath;
                        ViewBag.ImagePathIn1 = data.Image_Path;
                        //TempData["ImagePathIn1"] = data.Image_Path;
                    }
                    if (data.Seq_No == 2)
                    {
                        ViewBag.FilePathIn2 = data.FilePath;
                        ViewBag.ImagePathIn2 = data.Image_Path;
                    }
                    if (data.Seq_No == 3)
                    {
                        ViewBag.FilePathIn3 = data.FilePath;
                        ViewBag.ImagePathIn3 = data.Image_Path;
                    }
                    if (data.Seq_No == 4)
                    {
                        ViewBag.FilePathIn4 = data.FilePath;
                        ViewBag.ImagePathIn4 = data.Image_Path;
                    }

                }
                if (data.Image_Type == "External View ")
                {
                    if (data.Seq_No == 1)
                    {
                        ViewBag.FilePathEx1 = data.FilePath;
                        ViewBag.ImagePathEx1 = data.Image_Path;
                    }
                    if (data.Seq_No == 2)
                    {
                        ViewBag.FilePathEx2 = data.FilePath;
                        ViewBag.ImagePathEx2 = data.Image_Path;
                    }
                    if (data.Seq_No == 3)
                    {
                        ViewBag.FilePathEx3 = data.FilePath;
                        ViewBag.ImagePathEx3 = data.Image_Path;
                    }
                    if (data.Seq_No == 4)
                    {
                        ViewBag.FilePathEx4 = data.FilePath;
                        ViewBag.ImagePathEx4 = data.Image_Path;
                    }

                }
                if (data.Image_Type == "Approach Road")
                {
                    if (data.Seq_No == 1)
                    {
                        ViewBag.FilePathAR1 = data.FilePath;
                        ViewBag.ImagePathAR1 = data.Image_Path;
                    }
                    if (data.Seq_No == 2)
                    {
                        ViewBag.FilePathAR2 = data.FilePath;
                        ViewBag.ImagePathAR2 = data.Image_Path;
                    }
                    if (data.Seq_No == 3)
                    {
                        ViewBag.FilePathAR3 = data.FilePath;
                        ViewBag.ImagePathAR3 = data.Image_Path;
                    }
                    if (data.Seq_No == 4)
                    {
                        ViewBag.FilePathAR4 = data.FilePath;
                        ViewBag.ImagePathAR4 = data.Image_Path;
                    }

                }
                if (data.Image_Type == "View from property")
                {
                    if (data.Seq_No == 1)
                    {
                        ViewBag.FilePathv1 = data.FilePath;
                        ViewBag.ImagePathv1 = data.Image_Path;
                    }
                    if (data.Seq_No == 2)
                    {
                        ViewBag.FilePathv2 = data.FilePath;
                        ViewBag.ImagePathv2 = data.Image_Path;
                    }
                    if (data.Seq_No == 3)
                    {
                        ViewBag.FilePathv3 = data.FilePath;
                        ViewBag.ImagePathv3 = data.Image_Path;
                    }
                    if (data.Seq_No == 4)
                    {
                        ViewBag.FilePathv4 = data.FilePath;
                        ViewBag.ImagePathv4 = data.Image_Path;
                    }

                }
                if(data.Image_Type == "IGR Photo")
                {
                    if (data.Seq_No == 1)
                    {
                        ViewBag.FilePathIg = data.FilePath;
                        ViewBag.ImagePathIg = data.Image_Path;
                    }
                }
                if (data.Image_Type == "Location Map")
                {
                    if (data.Seq_No == 1)
                    {
                        ViewBag.FilePathlm = data.FilePath;
                        ViewBag.ImagePathlm = data.Image_Path;
                    }
                }
            }
            if (radiovalues == "Entire Project")
            {
                vm.ival_ProjectBuildingMasters = GetBuildingsByEntireProject(ProjectId);
            }
            else
            {
                vm.ival_ProjectBuildingMasters = GetBuildingForPartofProjEdit(ProjectId);
            }
            // vm.ival_ProjectBuildingMasters = GetBuildingsByEntireProject(ProjectId);
            hInputPara.Add("@ProjectId", Convert.ToInt32(ProjectId));
            //get visit Details
            DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectVistDetails", hInputPara);
            if (dtRequests.Rows.Count > 0)
            {
                if(dtRequests.Rows[0]["Date_Of_Inspection"].ToString()!="")
                {
                    string date = Convert.ToDateTime(dtRequests.Rows[0]["Date_Of_Inspection"]).ToString("dd/MM/yyyy");
                    ViewBag.DateOfInspection = date;
                }
               
                ViewBag.EnginnerVisitSide = Convert.ToString(dtRequests.Rows[0]["Engineer_Visiting_Site"]);
                ViewBag.PersonMetAtSide = Convert.ToString(dtRequests.Rows[0]["Person_Met_at_Site"]);
                ViewBag.personContactNo = Convert.ToString(dtRequests.Rows[0]["Person_Contact_Number"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description"]);
                ViewBag.Any_Deviation_observed = Convert.ToString(dtRequests.Rows[0]["Any_Deviation_observed"]);
                ViewBag.Rate_Range_In_Locality = Convert.ToString(dtRequests.Rows[0]["Rate_Range_In_Locality"]);
                ViewBag.Adopted_Base_Rate = Convert.ToString(dtRequests.Rows[0]["Adopted_Base_Rate"]);
                if(dtRequests.Rows[0]["GST_Applicability"].ToString()=="True")
                {
                    ViewBag.GST_Applicability = "Yes";
                }
                if (dtRequests.Rows[0]["GST_Applicability"].ToString() == "False")
                {
                    ViewBag.GST_Applicability = "No";
                }

                ViewBag.Parking_Charges = Convert.ToString(dtRequests.Rows[0]["Parking_Charges"]);
                ViewBag.Club_Membership_Charges = Convert.ToString(dtRequests.Rows[0]["Club_Membership_Charges"]);
                ViewBag.One_Time_Maintenance_Charges = Convert.ToString(dtRequests.Rows[0]["One_Time_Maintenance_Charges"]);
                ViewBag.Any_Other_Charges = Convert.ToString(dtRequests.Rows[0]["Any_Other_Charges"]);
                ViewBag.Government_Rate_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Government_Rate_Of_Unit"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks"]);
                IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesRiskofdemolition = Getdemolition();
                if (ival_LookupCategoryValuesRiskofdemolition.ToList().Count > 0 && ival_LookupCategoryValuesRiskofdemolition != null)
                {
                    var riskOfDenomination = ival_LookupCategoryValuesRiskofdemolition.Where(s => s.Lookup_Value == dtRequests.Rows[0]["Risk_of_Demolition"].ToString()).Select(s => s.Lookup_ID);

                    vm.SelectedModelRiskDemo = Convert.ToInt32(riskOfDenomination.FirstOrDefault());

                }

            }
            hInputPara.Clear();
            //project Amenities Details
            vm.ival_ProjectValuationAmenities = GetProjectAmenitiesEdit(ProjectId,ReqId);

            hInputPara1.Add("@projecId",Convert.ToInt32(ProjectId));
            DataTable dt1 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetMaintancePropertyIdByLookUp", hInputPara1);
            if (dt1.Rows.Count > 0)
            {
                int maintainceId = Convert.ToInt32(dt1.Rows[0]["Lookup_ID"]);
                vm.MaintanceOfProperty = maintainceId;
            }
            hInputPara1.Clear();
            hInputPara1.Add("@projecId", Convert.ToInt32(ProjectId));
            DataTable dt2 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetQualityOfConIdByLookUp", hInputPara1);
            if (dt2.Rows.Count > 0)
            {
                int qualityOfCon = Convert.ToInt32(dt2.Rows[0]["Lookup_ID"]);
                vm.QualityOfConstruction = qualityOfCon;
            }
            hInputPara1.Clear();
            hInputPara.Clear();
            //project Broker Details
            hInputPara.Add("@Project_ID", Convert.ToInt32(ProjectId));
            hInputPara.Add("@Request_ID", Convert.ToInt32(ReqId));
            DataTable dtBroker = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectValBrokerDetails", hInputPara);
            if (dtBroker.Rows.Count > 0)
            {
                
                    ViewBag.BrokerName1 = Convert.ToString(dtBroker.Rows[0]["Broker_Name"]);
                
                
                    ViewBag.BrokerName2 = Convert.ToString(dtBroker.Rows[1]["Broker_Name"]);
                
                ViewBag.FirmName1 = Convert.ToString(dtBroker.Rows[0]["Firm_Name"]);
                ViewBag.FirmName2 = Convert.ToString(dtBroker.Rows[1]["Firm_Name"]);
                ViewBag.ContactNum1 = Convert.ToString(dtBroker.Rows[0]["Contact_Num"]);
                ViewBag.ContactNum2 = Convert.ToString(dtBroker.Rows[1]["Contact_Num"]);
                ViewBag.Rate_Quoted_For_Land1 = Convert.ToString(dtBroker.Rows[0]["Rate_Quoted_For_Land"]);
                ViewBag.Rate_Quoted_For_Land2 = Convert.ToString(dtBroker.Rows[1]["Rate_Quoted_For_Land"]);
                ViewBag.Rate_Quoted_For_Flat1 = Convert.ToString(dtBroker.Rows[0]["Rate_Quoted_For_Flat"]);
                ViewBag.Rate_Quoted_For_Flat2 = Convert.ToString(dtBroker.Rows[1]["Rate_Quoted_For_Flat"]);
                ViewBag.BrokerId1= Convert.ToInt32(dtBroker.Rows[0]["Broker_ID"]);
                ViewBag.BrokerId2 = Convert.ToInt32(dtBroker.Rows[1]["Broker_ID"]);
            }
            hInputPara.Clear();
            //project Comparable Details
            hInputPara.Add("@Project_ID", Convert.ToInt32(ProjectId));
            hInputPara.Add("@Request_ID", Convert.ToInt32(ReqId));
            DataTable dtComparable = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectValComparableDetails", hInputPara);
            if (dtComparable.Rows.Count > 0)
            {
                ViewBag.Project_Name1 = Convert.ToString(dtComparable.Rows[0]["Project_Name"]);
                ViewBag.Project_Name2 = Convert.ToString(dtComparable.Rows[1]["Project_Name"]);
                ViewBag.Dist_Subject_Property1 = Convert.ToString(dtComparable.Rows[0]["Dist_Subject_Property"]);
                ViewBag.Dist_Subject_Property2 = Convert.ToString(dtComparable.Rows[1]["Dist_Subject_Property"]);
                ViewBag.Comparable_Unit_Details1 = Convert.ToString(dtComparable.Rows[0]["Comparable_Unit_Details"]);
                ViewBag.Comparable_Unit_Details2 = Convert.ToString(dtComparable.Rows[1]["Comparable_Unit_Details"]);
                ViewBag.Quality_And_Amenities1 = Convert.ToString(dtComparable.Rows[0]["Quality_And_Amenities"]);
                ViewBag.Quality_And_Amenities2 = Convert.ToString(dtComparable.Rows[1]["Quality_And_Amenities"]);
                ViewBag.Ongoing_Rate1 = Convert.ToString(dtComparable.Rows[0]["Ongoing_Rate"]);
                ViewBag.Ongoing_Rate2 = Convert.ToString(dtComparable.Rows[1]["Ongoing_Rate"]);
                ViewBag.Property_ID1 = Convert.ToInt32(dtComparable.Rows[0]["Property_ID"]);
                ViewBag.Property_ID2 = Convert.ToInt32(dtComparable.Rows[1]["Property_ID"]);
            }
            hInputPara.Clear();

            return View(vm);
        }
        [HttpPost]
        public List<ival_ProjectValuationAmenitiesInfrastructureProgress> GetProjectAmenitiesEdit(string projectId, string RequestId)
        {
            List<ival_ProjectValuationAmenitiesInfrastructureProgress> ListDemo = new List<ival_ProjectValuationAmenitiesInfrastructureProgress>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Project_ID",Convert.ToInt32(projectId));
            hInputPara.Add("@Request_ID", Convert.ToInt32(RequestId));
            DataTable dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectValAmenitiesDetailsEdit",hInputPara);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
               // ival_ProjectValuationAmenitiesInfrastructureProgress list = new ival_ProjectValuationAmenitiesInfrastructureProgress();

                ListDemo.Add(new ival_ProjectValuationAmenitiesInfrastructureProgress
                {
                    Amenity_Description = Convert.ToString(dt.Rows[i]["Amenity_Description"]),
                    Description_Of_Stage_Of_Construction = Convert.ToString(dt.Rows[i]["Description_Of_Stage_Of_Construction"]),
                    Percentage_Work_Completed = Convert.ToDecimal(dt.Rows[i]["Percentage_Work_Completed"]),
                    Percentage_Disbursement_Recommended = Convert.ToDecimal(dt.Rows[i]["Percentage_Disbursement_Recommended"]),
                    Maintenance_Of_Property= Convert.ToString(dt.Rows[i]["Maintenance_Of_Property"]),

                    Quality_Of_Construction = Convert.ToString(dt.Rows[i]["Quality_Of_Construction"]),

                });
                            }
            return ListDemo;
        }
        [HttpPost]
        public ActionResult EditProjectValuation(string str,string str1,string str2,string str3,string str7)
        {
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            string ReqId = Session["RequestIDProj"].ToString();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(ReqId));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            string ProjectId = Convert.ToString(ProjId.Rows[0]["Project_ID"]);
           
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            DataTable list2 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));
            DataTable list6 = (DataTable)JsonConvert.DeserializeObject(str2, (typeof(DataTable)));
            DataTable list7 = (DataTable)JsonConvert.DeserializeObject(str3, (typeof(DataTable)));
            DataTable list8 = (DataTable)JsonConvert.DeserializeObject(str7, (typeof(DataTable)));
            //visit Details
            for (int i = 0; i < list1.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@projectId",Convert.ToInt32(ProjectId));
                try
                {
                    var dateString = list1.Rows[i]["dateOfInspection"].ToString();
                    var format = "dd/MM/yyyy";
                    if (dateString != "")
                    {
                        var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                        hInputPara.Add("@Date_Of_Inspection", dateTime);
                    }
                    else
                    {
                        hInputPara.Add("@Date_Of_Inspection", DateTime.Now);
                    }
                }
                catch (Exception e)
                {
                    string dateString = list1.Rows[i]["dateOfInspection"].ToString();
                    if (dateString != "")
                    {

                        string datestring1 = Convert.ToDateTime(dateString).ToString("dd/MM/yyyy");
                        var format = "dd/MM/yyyy";
                        var dateTime = DateTime.ParseExact(datestring1, format, CultureInfo.InvariantCulture);
                        hInputPara.Add("@Date_Of_Inspection", dateTime);
                    }
                    else
                    {
                        hInputPara.Add("@Date_Of_Inspection", DateTime.Now);
                    }
                }

                // hInputPara.Add("@Date_Of_Inspection", Convert.ToInt32(list1.Rows[i]["dateOfInspection"]));
                hInputPara.Add("@Engineer_Visiting_Site", Convert.ToString(list1.Rows[i]["EnginnerVisitingTheSite"]));
                hInputPara.Add("@Person_Met_at_Site", Convert.ToString(list1.Rows[i]["personMetatsite"]));
                if (list1.Rows[i]["contactNumberatperson"].ToString() == "")
                {
                    var contactNumber = 0;
                    hInputPara.Add("@Person_Contact_Number", contactNumber);
                }
                else
                {
                    hInputPara.Add("@Person_Contact_Number", Convert.ToString(list1.Rows[i]["contactNumberatperson"]));
                }
                hInputPara.Add("@Any_Deviation_observed", Convert.ToString(list1.Rows[i]["anyDeviationObserved"]));
                hInputPara.Add("@Risk_of_Demolition", Convert.ToString(list1.Rows[i]["riskOfDemoliation"]));
                hInputPara.Add("@Deviation_Description", Convert.ToString(list1.Rows[i]["deviationDescription"]));
                if (list1.Rows[i]["RateRangeInLocality"].ToString() == "")
                {
                    var RateRange = 0;
                    hInputPara.Add("@rateRangeInLocality", RateRange);
                }
                else
                {

                    hInputPara.Add("@rateRangeInLocality", Convert.ToString(list1.Rows[i]["RateRangeInLocality"]));
                }
                string GstApplicable = list1.Rows[i]["GstApplicable"].ToString();
                if (GstApplicable == "No")
                {
                    hInputPara.Add("@GstApplicable", false);
                }
                if (GstApplicable == "Yes")
                {
                    hInputPara.Add("@GstApplicable", true);
                }
                if (list1.Rows[i]["parkingCharges"].ToString() == "")
                {
                    var parkingCharges = 0;
                    hInputPara.Add("@parkingCharges", parkingCharges);
                }
                else
                {
                    hInputPara.Add("@parkingCharges", Convert.ToString(list1.Rows[i]["parkingCharges"]));
                }
                if (list1.Rows[i]["ClubMembershipCharges"].ToString() == "")
                {
                    var clubCharges = 0;
                    hInputPara.Add("@clubMembershipCharges", clubCharges);
                }
                else
                {

                    hInputPara.Add("@clubMembershipCharges", Convert.ToString(list1.Rows[i]["ClubMembershipCharges"]));
                }
                if (list1.Rows[i]["OneTimeMaintanceCharges"].ToString() == "")
                {
                    var oneTimeMaintanceCharges = 0;
                    hInputPara.Add("@oneTimeMaintanceCharges", oneTimeMaintanceCharges);
                }
                else
                {

                    hInputPara.Add("@oneTimeMaintanceCharges", Convert.ToString(list1.Rows[i]["OneTimeMaintanceCharges"]));
                }
                if (list1.Rows[i]["AnyOtherCharges"].ToString() == "")
                {
                    var anyOtherCharges = 0;
                    hInputPara.Add("@anyOtherCharges", anyOtherCharges);
                }
                else
                {

                    hInputPara.Add("@anyOtherCharges", Convert.ToString(list1.Rows[i]["AnyOtherCharges"]));
                }
                if (list1.Rows[i]["GovermentRateOfUnit"].ToString() == "")
                {
                    var GovRate = 0;
                    hInputPara.Add("@GovermentRateOfUnit", GovRate);
                }
                else
                {

                    hInputPara.Add("@GovermentRateOfUnit", Convert.ToString(list1.Rows[i]["GovermentRateOfUnit"]));
                }
                hInputPara.Add("@remark", Convert.ToString(list1.Rows[i]["Remark"]));
                if (list1.Rows[i]["AdpotedBaseRate"].ToString() == "")
                {
                    var AdpotedBaseRate = 0;
                    hInputPara.Add("@adpotedBaseRate", AdpotedBaseRate);
                }
                else
                {

                    hInputPara.Add("@adpotedBaseRate", Convert.ToString(list1.Rows[i]["AdpotedBaseRate"]));
                }
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectVisitDetails", hInputPara);
                hInputPara.Clear();



            }
            //Amenities Details
            for(int i = 0; i < list2.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@projectId", Convert.ToInt32(ProjectId));
                hInputPara.Add("@Request_ID", Convert.ToInt32(ReqId));
                hInputPara.Add("@Description", Convert.ToString(list2.Rows[i]["descriptionOfAmenities"]));
                var result2=sqlDataAccess.ExecuteStoreProcedure("usp_GetDescriptionForProjValAmenities", hInputPara);
                hInputPara.Clear();
                if(result2== "document already exist")
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@projectId", Convert.ToInt32(ProjectId));
                    hInputPara.Add("@Request_Id", Convert.ToInt32(ReqId));
                    hInputPara.Add("@AmenityDescription", Convert.ToString(list2.Rows[i]["descriptionOfAmenities"]));
                    hInputPara.Add("@DescriptionOfStageConstruction", Convert.ToString(list2.Rows[i]["DescriptionOfstage"]));
                    if (list2.Rows[i]["PerWorkcomplet"].ToString() == "")
                    {
                        int workComplete = 0;
                        hInputPara.Add("@percentageWorkComplete", workComplete);
                    }
                    else
                    {
                        hInputPara.Add("@percentageWorkComplete", Convert.ToInt32(list2.Rows[i]["PerWorkcomplet"]));
                    }
                    if (list2.Rows[i]["DisbursementRecomm"].ToString() == "")
                    {
                        int disBursementRecomm = 0;
                        hInputPara.Add("@percentageDisbursementRecommended", disBursementRecomm);
                    }
                    else
                    {
                        hInputPara.Add("@percentageDisbursementRecommended", Convert.ToInt32(list2.Rows[i]["DisbursementRecomm"]));
                    }


                    hInputPara.Add("@maintenanceOfProperty", Convert.ToString(list2.Rows[i]["maintanceOfProperty"]));
                    hInputPara.Add("@QualityOfConstruction", Convert.ToString(list2.Rows[i]["QualityOfConstr"]));

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectValuationAmenities", hInputPara);
                    hInputPara.Clear();

                }
                else
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@projectId", Convert.ToInt32(ProjectId));
                    hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
                    hInputPara.Add("@AmenityDescription", Convert.ToString(list2.Rows[i]["descriptionOfAmenities"]));
                    hInputPara.Add("@DescriptionOfStageConstruction", Convert.ToString(list2.Rows[i]["DescriptionOfstage"]));
                    if (list2.Rows[i]["PerWorkcomplet"].ToString() == "")
                    {
                        int workComplete = 0;
                        hInputPara.Add("@percentageWorkComplete", workComplete);
                    }
                    else
                    {
                        hInputPara.Add("@percentageWorkComplete", Convert.ToInt32(list2.Rows[i]["PerWorkcomplet"]));
                    }
                    if (list2.Rows[i]["DisbursementRecomm"].ToString() == "")
                    {
                        int disBursementRecomm = 0;
                        hInputPara.Add("@percentageDisbursementRecommended", disBursementRecomm);
                    }
                    else
                    {
                        hInputPara.Add("@percentageDisbursementRecommended", Convert.ToInt32(list2.Rows[i]["DisbursementRecomm"]));
                    }


                    hInputPara.Add("@maintenanceOfProperty", Convert.ToString(list2.Rows[i]["maintanceOfProperty"]));
                    hInputPara.Add("@QualityOfConstruction", Convert.ToString(list2.Rows[i]["QualityOfConstr"]));

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectValuationAmenities", hInputPara);
                    hInputPara.Clear();
                }

                if ((list2.Rows[i]["descriptionOfAmenities"].ToString()) != null && (list2.Rows[i]["descriptionOfAmenities"].ToString()) != string.Empty)
                {
                    hInputPara1 = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara1.Add("@lookUpValue", Convert.ToString(list2.Rows[i]["descriptionOfAmenities"]));
                    
                   var result= sqlDataAccess.ExecuteStoreProcedure("usp_InsertLookUpAmenities", hInputPara1);
                }
                hInputPara1.Clear();
            }
            //Broker Details
            for (int i = 0; i < list6.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                string BrokerId = list6.Rows[i]["BrokerId"].ToString();
             //check record is already exist or not
                hInputPara.Add("@projectId", Convert.ToInt32(ProjectId));
                hInputPara.Add("@Request_ID", Convert.ToInt32(ReqId));
                hInputPara.Add("@brokerName", Convert.ToString(list6.Rows[i]["nameOfBroker"]));
                var result2 = sqlDataAccess.ExecuteStoreProcedure("usp_CheckProjValBrokerExist", hInputPara);
                hInputPara.Clear();
                if (result2 == "Record already exist")
                {
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", ProjectId);
                    hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
                    if (BrokerId != "")
                    {
                        hInputPara.Add("@BrokerId", Convert.ToInt32(BrokerId));
                    }

                    if (Convert.ToString(list6.Rows[i]["nameOfBroker"]) != "")
                    {
                        hInputPara.Add("@Broker_Name", Convert.ToString(list6.Rows[i]["nameOfBroker"]));
                        hInputPara.Add("@Firm_Name", Convert.ToString(list6.Rows[i]["nameOfFirm"]));

                        hInputPara.Add("@Contact_Num", Convert.ToString(list6.Rows[i]["contactNo"]));
                        hInputPara.Add("@Rate_Quoted_For_Land", Convert.ToString(list6.Rows[i]["rateRangeQuotedforLand"]));
                        hInputPara.Add("@Rate_Quoted_For_Flat", Convert.ToString(list6.Rows[i]["rateRangeQuotedforFlat"]));
                        var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectValBrokerDetails", hInputPara);

                    }
                    hInputPara.Clear();

                }
                else
                {
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", ProjectId);
                    hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
                    if (Convert.ToString(list6.Rows[i]["nameOfBroker"]) != "")
                    {
                        hInputPara.Add("@Broker_Name", Convert.ToString(list6.Rows[i]["nameOfBroker"]));
                        hInputPara.Add("@Firm_Name", Convert.ToString(list6.Rows[i]["nameOfFirm"]));

                        hInputPara.Add("@Contact_Num", Convert.ToString(list6.Rows[i]["contactNo"]));
                        hInputPara.Add("@Rate_Quoted_For_Land", Convert.ToString(list6.Rows[i]["rateRangeQuotedforLand"]));
                        hInputPara.Add("@Rate_Quoted_For_Flat", Convert.ToString(list6.Rows[i]["rateRangeQuotedforFlat"]));
                        var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertBrokerDetailsNew", hInputPara);

                    }
                    hInputPara.Clear();

                }

            }
            //Comparable Details
            for (int i = 0; i < list7.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                //check record is already exist or not
                hInputPara.Add("@projectId", Convert.ToInt32(ProjectId));
                hInputPara.Add("@Request_ID", Convert.ToInt32(ReqId));
                hInputPara.Add("@projectName", Convert.ToString(list7.Rows[i]["nameOfProj"]));
                var result2 = sqlDataAccess.ExecuteStoreProcedure("usp_CheckProjValComparableExist", hInputPara);
                hInputPara.Clear();
                if(result2== "Record already exist")
                {
                    sqlDataAccess = new SQLDataAccess();
                    string CompaId = list7.Rows[i]["ComparableId"].ToString();
                    hInputPara.Add("@Project_ID", ProjectId);
                    hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
                    if (CompaId != "")
                    {
                        hInputPara.Add("@ComparableId", Convert.ToInt32(CompaId));
                    }

                    if (Convert.ToString(list7.Rows[i]["nameOfProj"]) != "")
                    {
                        hInputPara.Add("@Project_Name", Convert.ToString(list7.Rows[i]["nameOfProj"]));
                        hInputPara.Add("@Dist_Subject_Property", Convert.ToString(list7.Rows[i]["distaneSub"]));

                        hInputPara.Add("@Comparable_Unit_Details", Convert.ToString(list7.Rows[i]["unitDetailsComparable"]));
                        hInputPara.Add("@Quality_And_Amenities", Convert.ToString(list7.Rows[i]["quality"]));
                        hInputPara.Add("@Ongoing_Rate", Convert.ToString(list7.Rows[i]["ongoingRate"]));
                        var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectValComparableDetails", hInputPara);
                    }

                    hInputPara.Clear();

                }
                else
                {
                   
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", ProjectId);
                    hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
                    if (Convert.ToString(list7.Rows[i]["nameOfProj"]) != "")
                    {
                        hInputPara.Add("@Project_Name", Convert.ToString(list7.Rows[i]["nameOfProj"]));
                        hInputPara.Add("@Dist_Subject_Property", Convert.ToString(list7.Rows[i]["distaneSub"]));

                        hInputPara.Add("@Comparable_Unit_Details", Convert.ToString(list7.Rows[i]["unitDetailsComparable"]));
                        hInputPara.Add("@Quality_And_Amenities", Convert.ToString(list7.Rows[i]["quality"]));
                        hInputPara.Add("@Ongoing_Rate", Convert.ToString(list7.Rows[i]["ongoingRate"]));
                        var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertCompPropNew", hInputPara);
                    }

                    hInputPara.Clear();

                }
            }
            //UploadImages
            for (int i = 0; i < list8.Rows.Count; i++)
            {
                string ImageFile = "";
                //get file name for adding prefix of request and documentId
                string file = list8.Rows[i]["Imagepath"].ToString();
                string imageTypeId = list8.Rows[i]["ImageTypeId"].ToString();
                string seqNO = list8.Rows[i]["ImageSequNo"].ToString();
                string imagetypeIDNew = seqNO + imageTypeId;
                string a = imagetypeIDNew.PadLeft(2, '0');
                if (file != "")
                {
                    string[] newValue = file.Split('\\');
                    if (newValue != null)
                    {
                        string Name = newValue[newValue.Length - 1];
                        ImageFile = ReqId + "_" + ProjectId + "_" + a + "_" + Name;


                    }
                }
                if ((list8.Rows[i]["ImageTypeId"].ToString()) != String.Empty && (list8.Rows[i]["ImageTypeId"].ToString()) != null)
                {
                    hInputPara.Add("@Request_ID", Convert.ToInt32(ReqId));
                    hInputPara.Add("@projectId", Convert.ToInt32(ProjectId));
                    hInputPara.Add("@imageTypeId", Convert.ToInt32(list8.Rows[i]["ImageTypeId"]));
                    hInputPara.Add("@SeqId", Convert.ToInt32(list8.Rows[i]["ImageSequNo"]));
                    string result = sqlDataAccess.ExecuteStoreProcedure("usp_checkUploadImageExist", hInputPara);
                    hInputPara.Clear();
                    if (result == "document already exist")
                    {
                        sqlDataAccess = new SQLDataAccess();

                        if (list8.Rows[i]["Imagepath"].ToString() != string.Empty && (list8.Rows[i]["Imagepath"].ToString()) != null)
                        {
                            hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
                            hInputPara.Add("@projectId", Convert.ToInt32(ProjectId));
                            hInputPara.Add("@imageTypeId", Convert.ToInt32(list8.Rows[i]["ImageTypeId"]));
                            hInputPara.Add("@segId", Convert.ToInt32(list8.Rows[i]["ImageSequNo"]));
                            hInputPara.Add("@imagePath", ImageFile);
                            hInputPara.Add("@updatedBy", Session["UserName"].ToString());
                            sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_UpdateProjectImageUploaded", hInputPara);
                            hInputPara.Clear();
                        }
                        else
                        {
                            hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
                            hInputPara.Add("@projectId", Convert.ToInt32(ProjectId));
                            hInputPara.Add("@imageTypeId", Convert.ToInt32(list8.Rows[i]["ImageTypeId"]));
                            hInputPara.Add("@segId", Convert.ToInt32(list8.Rows[i]["ImageSequNo"]));
                            //hInputPara.Add("@imagePath", Convert.ToString(list8.Rows[i]["Imagepath"]));
                            hInputPara.Add("@updatedBy", Session["UserName"].ToString());
                            sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_UpdateProjectBlankImagePathUploaded", hInputPara);
                            hInputPara.Clear();
                        }
                    }
                    else
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        if (list8.Rows[i]["Imagepath"].ToString() != string.Empty && list8.Rows[i]["Imagepath"].ToString() != null)
                        {
                            string ImagePath = "";
                            hInputPara.Add("@Project_ID", ProjectId);
                            hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
                            hInputPara.Add("@ImageTypeId", Convert.ToInt32(list8.Rows[i]["ImageTypeId"]));
                            string ImageName = list8.Rows[i]["Imagepath"].ToString();
                            if (ImageName != "")
                            {
                                string[] newValue = ImageName.Split('\\');
                                if (newValue != null)
                                {
                                    string ImageTypeId = Convert.ToString(list8.Rows[i]["ImageTypeId"]);
                                    string imagesqNO = list8.Rows[i]["ImageSequNo"].ToString();
                                    string ImageTypeIdNew = imagesqNO + ImageTypeId;
                                    string ImTypeId = ImageTypeIdNew.PadLeft(2, '0');
                                    string Name = newValue[newValue.Length - 1];
                                    ImagePath = ReqId + "_" + ProjectId + "_" + ImTypeId + "_" + Name;
                                }
                            }
                            hInputPara.Add("@ImagePath", ImagePath);
                            hInputPara.Add("@SequenceNo", Convert.ToInt32(list8.Rows[i]["ImageSequNo"]));
                            var result1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjUploadImages", hInputPara);
                            hInputPara.Clear();
                        }

                    }
                }
            }

            return Json(new { success = true, Message = "Update Successfully" }, JsonRequestBehavior.AllowGet);
        }
        //[HttpPost]
        //public ActionResult GetProjectId(int? Id)
        //{
        //    if(Id!=null)
        //    {
        //        string projectId = Id.ToString();
        //        Session["EProjectId"] = projectId;
        //    }
        //    return Json(new { success = true });
        //}
        [HttpPost]
        public ActionResult GetBuildingId(string strCheck,int?id)
        {
            if (id != null)
            {
                string projectId = id.ToString();
                DataTable list4 = (DataTable)JsonConvert.DeserializeObject(strCheck, (typeof(DataTable)));
                Session["BuildingId"] = list4;
            }
            return Json(new { success = true });
        }
        [HttpPost]
        public List<ival_ProjectBuildingMaster> GetBuildingsByEntireProject(string Id)
        {
           
            List<ival_ProjectBuildingMaster> ival_BuilderList = new List<ival_ProjectBuildingMaster>();
            if (Id != null)
            {
                try
                {

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@projectId", Id);
                    DataTable dtBuilder = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ProjectBuildingByProjectId", hInputPara);

                    hInputPara.Clear();

                    if (dtBuilder.Rows.Count > 0 && dtBuilder != null)

                    {
                        for (int i = 0; i < dtBuilder.Rows.Count; i++)
                        {

                            ival_BuilderList.Add(new ival_ProjectBuildingMaster
                            {
                                Building_ID = Convert.ToInt32(dtBuilder.Rows[i]["Building_ID"]),

                                Building_Name = Convert.ToString(dtBuilder.Rows[i]["Building_Name"]),


                            });

                        }
                    }

                    return ival_BuilderList;
                }
                catch (Exception ex)
                {
                    
                }
            }
            return ival_BuilderList;
        }
        [HttpPost]
        public JsonResult GetBuildingWing(string buildingID)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            string ProjRadio = Session["ProjRadioValue"].ToString();
            if(ProjRadio== "Entire Project")
            {
                List<ival_ProjBldgWingMaster> wingList = new List<ival_ProjBldgWingMaster>();

                hInputPara.Add("@buildingId", buildingID);
                DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBuilWingDetailsByBuildingId", hInputPara);
                for (int i = 0; i < dtrptmaster.Rows.Count; i++)
                {

                    wingList.Add(new ival_ProjBldgWingMaster
                    {
                        Wing_ID = Convert.ToInt32(dtrptmaster.Rows[i]["Wing_ID"]),
                        Wing_Name = Convert.ToString(dtrptmaster.Rows[i]["Wing_Name"]),

                    });
                    // ViewBag.SelectBranchName = Convert.ToString(dtrptmaster.Rows[i]["Branch_Name"]);
                }
                var jsonSerialiser = new JavaScriptSerializer();
                var jsonvMUnitTypes = jsonSerialiser.Serialize(wingList);
                return Json(jsonvMUnitTypes);


            }
            else
            {
                List<ival_ProjBldgWingMaster> wingList = new List<ival_ProjBldgWingMaster>();
                /*3 values*/
                hInputPara.Add("@buildingId", buildingID);
                DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBuilWingDetailsByBuildingId", hInputPara);
                for (int j = 0; j < dtrptmaster.Rows.Count; j++)
                {
                    DataTable builWing = Session["ProjWingId"] as DataTable;  //.....2 values
                    for (int i = 0; i < builWing.Rows.Count; i++)
                    {
                        ival_ProjBldgWingMaster obj = new ival_ProjBldgWingMaster();
                       // Wing_Name = Convert.ToString(dtrptmaster.Rows[i]["Wing_Name"]),
                        string wingId = dtrptmaster.Rows[j]["Wing_ID"].ToString();
                        string wingIdNew= builWing.Rows[i]["wingVal"].ToString();
                        if(wingId.Contains(wingIdNew))
                        {
                            obj.Wing_ID = Convert.ToInt32(wingIdNew);
                            obj.Wing_Name = Convert.ToString(dtrptmaster.Rows[j]["Wing_Name"]);
                            wingList.Add(obj);
                        }

                    }
                   
                }
                var jsonSerialiser = new JavaScriptSerializer();
                var jsonvMUnitTypes = jsonSerialiser.Serialize(wingList);
                return Json(jsonvMUnitTypes);


            }
            return Json("");
        }
        //for Edit Get Building Wings
        [HttpPost]
        public JsonResult GetBuildingWingEdit(string buildingID)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            string ProjRadio = Session["RadioProjValEdit"].ToString();
            if (ProjRadio == "Entire Project")
            {
                List<ival_ProjBldgWingMaster> wingList = new List<ival_ProjBldgWingMaster>();

                hInputPara.Add("@buildingId", buildingID);
                DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBuilWingDetailsByBuildingId", hInputPara);
                for (int i = 0; i < dtrptmaster.Rows.Count; i++)
                {

                    wingList.Add(new ival_ProjBldgWingMaster
                    {
                        Wing_ID = Convert.ToInt32(dtrptmaster.Rows[i]["Wing_ID"]),
                        Wing_Name = Convert.ToString(dtrptmaster.Rows[i]["Wing_Name"]),

                    });
                    // ViewBag.SelectBranchName = Convert.ToString(dtrptmaster.Rows[i]["Branch_Name"]);
                }
                var jsonSerialiser = new JavaScriptSerializer();
                var jsonvMUnitTypes = jsonSerialiser.Serialize(wingList);
                return Json(jsonvMUnitTypes);


            }
            else
            {
                List<ival_ProjBldgWingMaster> wingList = new List<ival_ProjBldgWingMaster>();
                /*3 values*/
                hInputPara.Add("@buildingId", buildingID);
                DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBuilWingDetailsByBuildingId", hInputPara);
                for (int j = 0; j < dtrptmaster.Rows.Count; j++)
                {
                    DataTable builWing = Session["ProjWingId"] as DataTable;  //.....2 values
                    for (int i = 0; i < builWing.Rows.Count; i++)
                    {
                        ival_ProjBldgWingMaster obj = new ival_ProjBldgWingMaster();
                        // Wing_Name = Convert.ToString(dtrptmaster.Rows[i]["Wing_Name"]),
                        string wingId = dtrptmaster.Rows[j]["Wing_ID"].ToString();
                        string wingIdNew = builWing.Rows[i]["wingVal"].ToString();
                        if (wingId.Contains(wingIdNew))
                        {
                            obj.Wing_ID = Convert.ToInt32(wingIdNew);
                            obj.Wing_Name = Convert.ToString(dtrptmaster.Rows[j]["Wing_Name"]);
                            wingList.Add(obj);
                        }

                    }

                }
                var jsonSerialiser = new JavaScriptSerializer();
                var jsonvMUnitTypes = jsonSerialiser.Serialize(wingList);
                return Json(jsonvMUnitTypes);


            }
            return Json("");
        }
        //for View Get Building Wings
        [HttpPost]
        public JsonResult GetBuildingWingView(string buildingID)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            string ProjRadio = Session["RadioProjValView"].ToString();
            if (ProjRadio == "Entire Project")
            {
                List<ival_ProjBldgWingMaster> wingList = new List<ival_ProjBldgWingMaster>();

                hInputPara.Add("@buildingId", buildingID);
                DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBuilWingDetailsByBuildingId", hInputPara);
                for (int i = 0; i < dtrptmaster.Rows.Count; i++)
                {

                    wingList.Add(new ival_ProjBldgWingMaster
                    {
                        Wing_ID = Convert.ToInt32(dtrptmaster.Rows[i]["Wing_ID"]),
                        Wing_Name = Convert.ToString(dtrptmaster.Rows[i]["Wing_Name"]),

                    });
                    // ViewBag.SelectBranchName = Convert.ToString(dtrptmaster.Rows[i]["Branch_Name"]);
                }
                var jsonSerialiser = new JavaScriptSerializer();
                var jsonvMUnitTypes = jsonSerialiser.Serialize(wingList);
                return Json(jsonvMUnitTypes);


            }
            else
            {
                List<ival_ProjBldgWingMaster> wingList = new List<ival_ProjBldgWingMaster>();
                /*3 values*/
                hInputPara.Add("@buildingId", buildingID);
                DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBuilWingDetailsByBuildingId", hInputPara);
                for (int j = 0; j < dtrptmaster.Rows.Count; j++)
                {
                    DataTable builWing = Session["ProjWingIdView"] as DataTable;  //.....2 values
                    for (int i = 0; i < builWing.Rows.Count; i++)
                    {
                        ival_ProjBldgWingMaster obj = new ival_ProjBldgWingMaster();
                        // Wing_Name = Convert.ToString(dtrptmaster.Rows[i]["Wing_Name"]),
                        string wingId = dtrptmaster.Rows[j]["Wing_ID"].ToString();
                        string wingIdNew = builWing.Rows[i]["wingVal"].ToString();
                        if (wingId.Contains(wingIdNew))
                        {
                            obj.Wing_ID = Convert.ToInt32(wingIdNew);
                            obj.Wing_Name = Convert.ToString(dtrptmaster.Rows[j]["Wing_Name"]);
                            wingList.Add(obj);
                        }

                    }

                }
                var jsonSerialiser = new JavaScriptSerializer();
                var jsonvMUnitTypes = jsonSerialiser.Serialize(wingList);
                return Json(jsonvMUnitTypes);


            }
            return Json("");
        }

        //get building for Part Of Project
        public List<ival_ProjectBuildingMaster> GetBuildingForPartofProj(string projId)
        {
            List<ival_ProjectBuildingMaster> ListDemo = new List<ival_ProjectBuildingMaster>();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int projectId = Convert.ToInt32(projId);
            int requestId = Convert.ToInt32(Session["ProjRequestIdSave"]);
            hInputPara1.Add("@projectId", projectId);
            hInputPara1.Add("@RequestId", requestId);
            DataTable dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ProjRequestLogForPartOfProj", hInputPara1);
            hInputPara1.Clear();
            //DataTable dt = Session["ProjBuildingId"] as DataTable;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int id = Convert.ToInt32(dt.Rows[i]["Building_ID"]);
                hInputPara.Add("@buildingId", id);
                hInputPara.Add("@projectId", projectId);
                DataTable dtProjBuild = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBuildingName",hInputPara);
                hInputPara.Clear();
                for (int j = 0; j < dtProjBuild.Rows.Count; j++)
                {

                    ListDemo.Add(new ival_ProjectBuildingMaster
                    {
                        Building_ID = Convert.ToInt32(dtProjBuild.Rows[j]["Building_ID"]),
                        Building_Name = Convert.ToString(dtProjBuild.Rows[j]["Building_Name"]),
                    });

                }

            }
            
            return ListDemo;
        }
        //get building for Part Of Project Edit
        public List<ival_ProjectBuildingMaster> GetBuildingForPartofProjEdit(string projId)
        {
            List<ival_ProjectBuildingMaster> ListDemo = new List<ival_ProjectBuildingMaster>();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int projectId = Convert.ToInt32(projId);
            int requestId = Convert.ToInt32(Session["RequestIDProj"]);
            hInputPara1.Add("@projectId", projectId);
            hInputPara1.Add("@RequestId", requestId);
            DataTable dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ProjRequestLogForPartOfProj", hInputPara1);
            hInputPara1.Clear();
            //DataTable dt = Session["ProjBuildingId"] as DataTable;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int id = Convert.ToInt32(dt.Rows[i]["Building_ID"]);
                hInputPara.Add("@buildingId", id);
                hInputPara.Add("@projectId", projectId);
                DataTable dtProjBuild = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBuildingName", hInputPara);
                hInputPara.Clear();
                for (int j = 0; j < dtProjBuild.Rows.Count; j++)
                {

                    ListDemo.Add(new ival_ProjectBuildingMaster
                    {
                        Building_ID = Convert.ToInt32(dtProjBuild.Rows[j]["Building_ID"]),
                        Building_Name = Convert.ToString(dtProjBuild.Rows[j]["Building_Name"]),
                    });

                }

            }

            return ListDemo;
        }
        //insertAreaDetails
        [HttpPost]
        public ActionResult SaveAreaDetails(string str)
        {
            string Request_ID = Session["Request_ID1"].ToString();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(Request_ID));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            int projectId = Convert.ToInt32(ProjId.Rows[0]["Project_ID"]);
            DataTable list4 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            //Area Details
            for (int i = 0; i < list4.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@projectId", projectId);
                hInputPara.Add("@DescriptionOfUnit", Convert.ToString(list4.Rows[i]["descriptionOfUnit"]));
                if (list4.Rows[i]["ApprovedCarpetArea"].ToString() == "")
                {
                    int appCarpetArea = 0;
                    hInputPara.Add("@ApprovedCarpetarea", appCarpetArea);
                }
                else
                {
                    hInputPara.Add("@ApprovedCarpetarea", Convert.ToDouble(list4.Rows[i]["ApprovedCarpetArea"]));
                }
                if (list4.Rows[i]["areaAsPerPlan"].ToString() == "")
                {
                    int areaAsPerPlan = 0;
                    hInputPara.Add("@AreaAsperPlan", areaAsPerPlan);
                }
                else
                {
                    hInputPara.Add("@AreaAsperPlan", Convert.ToDouble(list4.Rows[i]["areaAsPerPlan"]));
                }
                if (list4.Rows[i]["NoOfUnits"].ToString() == "")
                {
                    int noOfunits = 0;
                    hInputPara.Add("@NoOfUnits", noOfunits);
                }
                else
                {
                    hInputPara.Add("@NoOfUnits", Convert.ToDouble(list4.Rows[i]["NoOfUnits"]));
                }

                hInputPara.Add("@DetailsOfUnitDeviation", Convert.ToString(list4.Rows[i]["DetailsOfUnitDeviation"]));
                hInputPara.Add("@BuildingId", Convert.ToInt32(list4.Rows[i]["BuildingID"]));
                hInputPara.Add("@wingId", Convert.ToInt32(list4.Rows[i]["Wing_ID"]));
                hInputPara.Add("@RequestId", Convert.ToInt32(Request_ID));
                //if (list4.Rows[i]["BuildingID"].ToString() !="Please Select")
                //{
                //    int BuildingId = 0;
                //    hInputPara.Add("@BuildingId", BuildingId);
                //}
                //else
                //{
                //    hInputPara.Add("@BuildingId", Convert.ToInt32(list4.Rows[i]["BuildingID"]));
                //}
                //if (list4.Rows[i]["Wing_ID"].ToString() !="Please Select")
                //{
                //    int WingID = 0;
                //    hInputPara.Add("@wingId", WingID);
                //}
                //else
                //{
                //    hInputPara.Add("@wingId", Convert.ToInt32(list4.Rows[i]["Wing_ID"]));
                //}
                //if (list4.Rows[i]["TotalAreaAsPerPlan"].ToString() != "")
                //{
                //    double totalAsPerPlan = 0.0;
                //    hInputPara.Add("@TotalAreaAsperPlan",totalAsPerPlan);
                //}
                //else
                //{
                //    hInputPara.Add("@TotalAreaAsperPlan", Convert.ToDouble(list4.Rows[i]["TotalAreaAsPerPlan"]));
                //}
                //if (list4.Rows[i]["TotalNoOfUnits"].ToString() != "")
                //{
                //    double totalNoOfUnits = 0.0;
                //    hInputPara.Add("@TotalNoOfUnits",totalNoOfUnits);
                //}
                //else
                //{
                //    hInputPara.Add("@TotalNoOfUnits", Convert.ToDouble(list4.Rows[i]["TotalNoOfUnits"]));
                //}

                //hInputPara.Add("@totalDetailsOfUnitDeviation", Convert.ToString(list4.Rows[i]["TotalDetailsOfUnitDeviation"]));
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectAreaDetails", hInputPara);
                hInputPara.Clear();
                if ((list4.Rows[i]["descriptionOfUnit"].ToString()) != null && (list4.Rows[i]["descriptionOfUnit"].ToString()) != string.Empty)
                {
                    hInputPara1 = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara1.Add("@Unitdescription", Convert.ToString(list4.Rows[i]["descriptionOfUnit"]));
                    //  hInputPara1.Add("@Document_ID", Convert.ToString(list1.Rows[i]["docid"]));
                    sqlDataAccess.ExecuteStoreProcedure("usp_InsertAreaDtailsDes", hInputPara1);
                }
            }
            return Json(new { success = true, Message = "Inserted Successfully" }, JsonRequestBehavior.AllowGet);
        }
        //insertProjectConstrDetails
        [HttpPost]
        public ActionResult SaveProjectConstructionDetails(string str)
        {
            string Request_ID = Session["Request_ID1"].ToString();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(Request_ID));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            int projectId = Convert.ToInt32(ProjId.Rows[0]["Project_ID"]);
            DataTable list3 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            //Project Construction WorkProcess
            for (int i = 0; i < list3.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@projectId", projectId);
                hInputPara.Add("@RequestId", Convert.ToInt32(Request_ID));
                hInputPara.Add("@nameOfBuilding", Convert.ToString(list3.Rows[i]["nameOfBuilding"]));
                hInputPara.Add("@nameOfWing", Convert.ToString(list3.Rows[i]["nameofWing"]));
                hInputPara.Add("@descriptionofStage", Convert.ToString(list3.Rows[i]["descriptionofstageOfConst"]));
                hInputPara.Add("@workCompleted", Convert.ToString(list3.Rows[i]["workCompleted"]));
                hInputPara.Add("@disbursementRecommended", Convert.ToString(list3.Rows[i]["dibursementrecom"]));
                hInputPara.Add("@BuildingId", Convert.ToInt32(list3.Rows[i]["BuildingId"]));
                hInputPara.Add("@WingId", Convert.ToInt32(list3.Rows[i]["WingId"]));
                //hInputPara.Add("@createdBy", Convert.ToString(list1.Rows[i]["personMetatsite"]));
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertprojValConstrWorkProcess", hInputPara);
                hInputPara.Clear();
            }
            return Json(new { success = true, Message = "Inserted Successfully" }, JsonRequestBehavior.AllowGet);
        }
        //insertProjectPrizing
        [HttpPost]
        public ActionResult SaveProjectPrizingDetails(string str)
        {
            string Request_ID = Session["Request_ID1"].ToString();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(Request_ID));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            int projectId = Convert.ToInt32(ProjId.Rows[0]["Project_ID"]);
            DataTable list3 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            //Project Prizing 
            for (int i = 0; i < list3.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@projectId", projectId);
                hInputPara.Add("@RequestId", Convert.ToInt32(Request_ID));
               // hInputPara.Add("@nameOfBuilding", Convert.ToString(list3.Rows[i]["nameOfBuilding"]));
                hInputPara.Add("@nameOfWing", Convert.ToString(list3.Rows[i]["nameofWing"]));
                hInputPara.Add("@BuildingId", Convert.ToInt32(list3.Rows[i]["BuildingId"]));
                hInputPara.Add("@WingId", Convert.ToInt32(list3.Rows[i]["WingId"]));
                hInputPara.Add("@CarpetBaseRate", Convert.ToString(list3.Rows[i]["BaseRate"]));
                hInputPara.Add("@FloorWiseRate", Convert.ToString(list3.Rows[i]["FloorRiseRsPerSqFt"]));
                hInputPara.Add("@PLCCharges", Convert.ToString(list3.Rows[i]["PLCCharges"]));
                hInputPara.Add("@FloorWiseApplicable", Convert.ToString(list3.Rows[i]["FloorRiseApplicable"]));
                hInputPara.Add("@createdBy", Session["UserName"].ToString());
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertprojValPrizingDetails", hInputPara);
                hInputPara.Clear();
            }
            return Json(new { success = true, Message = "Inserted Successfully" }, JsonRequestBehavior.AllowGet);
        }
        //Save SideMargin Details
        [HttpPost]
        public ActionResult SaveSideMarginDetails(string str)
        {
            string Request_ID = Session["Request_ID1"].ToString();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            sqlDataAccess1 = new SQLDataAccess();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(Request_ID));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            int projectId = Convert.ToInt32(ProjId.Rows[0]["Project_ID"]);
            hInputPara1.Clear();
            DataTable list2 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            //hInputPara = new Hashtable();
            //sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Project_ID", projectId);
            hInputPara.Add("@RequestId", Convert.ToInt32(Request_ID));
            hInputPara.Add("@BuildingId", Convert.ToInt32(list2.Rows[0]["BuildingID"]));
            hInputPara.Add("@WingId", Convert.ToInt32(list2.Rows[0]["Wing_ID"]));
            hInputPara.Add("@nameOfBuilding", Convert.ToString(list2.Rows[0]["BuildingName"]));
            hInputPara.Add("@nameOfWing", Convert.ToString(list2.Rows[0]["WingName"]));
            //side margin and deviation
            for (int i = 0; i < list2.Rows.Count; i++)
            {
               sqlDataAccess = new SQLDataAccess();

                string MarginRequirments = Convert.ToString(list2.Rows[i]["MarginRequirments"]);
                if(MarginRequirments=="Front Margin")
                {
                    hInputPara.Add("@ApprovedFrontSideMargin", Convert.ToString(list2.Rows[i]["ApprovedMargin"]));
                    hInputPara.Add("@ActualFrontSideMargin", Convert.ToString(list2.Rows[i]["ActualMargin"]));
                }
                if (MarginRequirments =="Rear Margin")
                {
                    hInputPara.Add("@ApprovedRearSideMargin", Convert.ToString(list2.Rows[i]["ApprovedMargin"]));
                    hInputPara.Add("@ActualRearSideMargin", Convert.ToString(list2.Rows[i]["ActualMargin"]));
                }
                if (MarginRequirments =="Left Side")
                {
                    hInputPara.Add("@ApprovedLeftSideMargin", Convert.ToString(list2.Rows[i]["ApprovedMargin"]));
                    hInputPara.Add("@ActualLeftSideMargin", Convert.ToString(list2.Rows[i]["ActualMargin"]));
                }
                if (MarginRequirments =="Right Side")
                {
                    hInputPara.Add("@ApprovedRightSideMargin", Convert.ToString(list2.Rows[i]["ApprovedMargin"]));
                    hInputPara.Add("@ActualRightSideMargin", Convert.ToString(list2.Rows[i]["ActualMargin"]));
                }

                //for update data into projectSideMargin table
                hInputPara1 = new Hashtable();
                //sqlDataAccess1 = new SQLDataAccess();
                hInputPara1.Add("@Project_ID", projectId);
                
                hInputPara1.Add("@Building_ID", Convert.ToInt32(list2.Rows[0]["BuildingID"]));
                hInputPara1.Add("@Wing_ID", Convert.ToInt32(list2.Rows[0]["Wing_ID"]));
                
                hInputPara1.Add("@Side_Margin_Description", Convert.ToString(list2.Rows[i]["MarginRequirments"]));
                hInputPara1.Add("@As_per_Approved", Convert.ToString(list2.Rows[i]["ApprovedMargin"]));
                hInputPara1.Add("@as_Actuals", Convert.ToString(list2.Rows[i]["ActualMargin"]));
                var result1 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectSideMarginsNew", hInputPara1);
                hInputPara1.Clear();
            }
            sqlDataAccess = new SQLDataAccess();
            var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectValuationSideMargins", hInputPara);
            hInputPara.Clear();
            return Json(new { success = true, Message = "Inserted Successfully" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetWIngApprovedMarginDetails(string buildingId,string WingId)
        {
            string Request_ID = Session["Request_ID1"].ToString();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(Request_ID));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            int projectId = Convert.ToInt32(ProjId.Rows[0]["Project_ID"]);

            List<ival_ProjectSideMargin> approvedList = new List<ival_ProjectSideMargin>();
            
            hInputPara.Add("@Building_ID", Convert.ToInt32(buildingId));
            hInputPara.Add("@ProjectId",projectId);
            hInputPara.Add("@Wing_ID", Convert.ToInt32(WingId));
            DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GeProjBuilWingApprovedMargin", hInputPara);
            for (int i = 0; i < dtrptmaster.Rows.Count; i++)
            {

                approvedList.Add(new ival_ProjectSideMargin
                {
                    Wing_ID = Convert.ToInt32(dtrptmaster.Rows[i]["Wing_ID"]),
                    Side_Margin_Description = Convert.ToString(dtrptmaster.Rows[i]["Side_Margin_Description"]),
                    As_per_Approved = Convert.ToString(dtrptmaster.Rows[i]["As_per_Approved"]),
                });
                
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonApproved = jsonSerialiser.Serialize(approvedList);
            return Json(jsonApproved);

        }

        [HttpPost]
        public JsonResult GetWIngApprovedMarginDetailsEdit(string buildingId, string WingId)
        {
            string Request_ID = Session["RequestIDProj"].ToString();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(Request_ID));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            int projectId = Convert.ToInt32(ProjId.Rows[0]["Project_ID"]);

            List<ival_ProjectSideMargin> approvedList = new List<ival_ProjectSideMargin>();

            hInputPara.Add("@Building_ID", Convert.ToInt32(buildingId));
            hInputPara.Add("@ProjectId", projectId);
            hInputPara.Add("@Wing_ID", Convert.ToInt32(WingId));
            DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GeProjBuilWingApprovedMargin", hInputPara);
            for (int i = 0; i < dtrptmaster.Rows.Count; i++)
            {

                approvedList.Add(new ival_ProjectSideMargin
                {
                    Wing_ID = Convert.ToInt32(dtrptmaster.Rows[i]["Wing_ID"]),
                    Side_Margin_Description = Convert.ToString(dtrptmaster.Rows[i]["Side_Margin_Description"]),
                    As_per_Approved = Convert.ToString(dtrptmaster.Rows[i]["As_per_Approved"]),
                });

            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonApproved = jsonSerialiser.Serialize(approvedList);
            return Json(jsonApproved);

        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetSideMargin()
        {
            List<ival_LookupCategoryValues> ListReporttype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSideMargin");
            for (int i = 0; i < dtGetReportType.Rows.Count; i++)
            {

                ListReporttype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtGetReportType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtGetReportType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtGetReportType.Rows[i]["Lookup_Value"])

                });

            }
            return ListReporttype;
        }
        [HttpPost]
        public JsonResult GetProjAreaDetailsEdit(string buildingId, string WingId)
        {
            string ReqId = Session["RequestIDProj"].ToString();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(ReqId));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            int projectId = Convert.ToInt32(ProjId.Rows[0]["Project_ID"]);

            List<ival_ProjectValuationAreaDetails> approvedList = new List<ival_ProjectValuationAreaDetails>();

            hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
            hInputPara.Add("@BuildingId", Convert.ToInt32(buildingId));
            hInputPara.Add("@ProjectId", projectId);
            hInputPara.Add("@WingId", Convert.ToInt32(WingId));
            DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjValAreaDetailsEdit", hInputPara);
            for (int i = 0; i < dtrptmaster.Rows.Count; i++)
            {
                ival_ProjectValuationAreaDetails obj = new ival_ProjectValuationAreaDetails();
                obj.Description_Of_Unit = Convert.ToString(dtrptmaster.Rows[i]["Description_Of_Unit"]);
                if(dtrptmaster.Rows[i]["Approved_Carpet_Area"]!=DBNull.Value)
                {
                    obj.Measured_Carpet_Area = Convert.ToDecimal(dtrptmaster.Rows[i]["Approved_Carpet_Area"]);

                }
                obj.Area_As_Per_Plan = Convert.ToDecimal(dtrptmaster.Rows[i]["Area_As_Per_Plan"]);
                obj.No_Of_Units = Convert.ToInt32(dtrptmaster.Rows[i]["No_Of_Units"]);
                obj.Details_Of_Unit_Deviation = Convert.ToString(dtrptmaster.Rows[i]["Details_Of_Unit_Deviation"]);

                approvedList.Add(obj);
            }
            hInputPara.Clear();
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonApproved = jsonSerialiser.Serialize(approvedList);
            return Json(jsonApproved);

        }

        [HttpPost]
        public JsonResult GetProjConsWorkProcDetailsEdit(string buildingId, string WingId)
        {
            string ReqId = Session["RequestIDProj"].ToString();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(ReqId));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            int projectId = Convert.ToInt32(ProjId.Rows[0]["Project_ID"]);

            List<ival_ProjectConstructionWorkProgress> approvedList = new List<ival_ProjectConstructionWorkProgress>();

            hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
            hInputPara.Add("@BuildingId", Convert.ToInt32(buildingId));
            hInputPara.Add("@ProjectId", projectId);
            hInputPara.Add("@WingId", Convert.ToInt32(WingId));
            DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjConstructionWorkProcessEdit", hInputPara);
            for (int i = 0; i < dtrptmaster.Rows.Count; i++)
            {
                ival_ProjectConstructionWorkProgress obj = new ival_ProjectConstructionWorkProgress();
                obj.Description_Of_Stage_Of_Construction = Convert.ToString(dtrptmaster.Rows[i]["Description_Of_Stage_Of_Construction"]);
                if (dtrptmaster.Rows[i]["Percentage_Work_Completed"] != DBNull.Value)
                {
                    obj.Percentage_Work_Completed = Convert.ToDecimal(dtrptmaster.Rows[i]["Percentage_Work_Completed"]);

                }
                if (dtrptmaster.Rows[i]["Percentage_Disbursement_Recommended"] != DBNull.Value)
                {
                    obj.Percentage_Disbursement_Recommended = Convert.ToDecimal(dtrptmaster.Rows[i]["Percentage_Disbursement_Recommended"]);

                }

                approvedList.Add(obj);
            }
            hInputPara.Clear();
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonApproved = jsonSerialiser.Serialize(approvedList);
            return Json(jsonApproved);

        }

        //get prizing Details
        [HttpPost]
        public JsonResult GetProjPrizingDetailsEdit(string buildingId, string WingId)
        {
            string ReqId = Session["RequestIDProj"].ToString();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(ReqId));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            int projectId = Convert.ToInt32(ProjId.Rows[0]["Project_ID"]);

            List<ival_ProjectValuationPricing> approvedList = new List<ival_ProjectValuationPricing>();

            hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
            hInputPara.Add("@BuildingId", Convert.ToInt32(buildingId));
            hInputPara.Add("@ProjectId", projectId);
            hInputPara.Add("@WingId", Convert.ToInt32(WingId));
            DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjPricingEdit", hInputPara);
            for (int i = 0; i < dtrptmaster.Rows.Count; i++)
            {
                ival_ProjectValuationPricing obj = new ival_ProjectValuationPricing();
                if (dtrptmaster.Rows[i]["Carpet_Base_Rate"] != DBNull.Value)
                {
                    obj.Carpet_Base_Rate = Convert.ToDecimal(dtrptmaster.Rows[i]["Carpet_Base_Rate"]);
                }
                if (dtrptmaster.Rows[i]["FloorWise_Rate"] != DBNull.Value)
                {
                    obj.FloorWise_Rate = Convert.ToDecimal(dtrptmaster.Rows[i]["FloorWise_Rate"]);

                }
                if (dtrptmaster.Rows[i]["PLC_Charges"] != DBNull.Value)
                {
                    obj.PLC_Charges = Convert.ToDecimal(dtrptmaster.Rows[i]["PLC_Charges"]);

                }
                if (dtrptmaster.Rows[i]["FloorWise_Applicable_Floor_No"] != DBNull.Value)
                {
                    obj.FloorWise_Applicable_Floor_No = Convert.ToDecimal(dtrptmaster.Rows[i]["FloorWise_Applicable_Floor_No"]);

                }
                
                approvedList.Add(obj);
            }
            hInputPara.Clear();
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonApproved = jsonSerialiser.Serialize(approvedList);
            return Json(jsonApproved);

        }

        [HttpPost]
        public JsonResult GetProjValSideMarginDetailsEdit(string buildingId, string WingId) 
        {
            string ReqId = Session["RequestIDProj"].ToString();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(ReqId));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            int projectId = Convert.ToInt32(ProjId.Rows[0]["Project_ID"]);

            List<ival_ProjectValuationSideMargin> approvedList = new List<ival_ProjectValuationSideMargin>();

            hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
            hInputPara.Add("@BuildingId", Convert.ToInt32(buildingId));
            hInputPara.Add("@ProjectId", projectId);
            hInputPara.Add("@WingId", Convert.ToInt32(WingId));
            DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjValSideMarginEdit", hInputPara);
            for (int i = 0; i < dtrptmaster.Rows.Count; i++)
            {
                ival_ProjectValuationSideMargin obj = new ival_ProjectValuationSideMargin();
                obj.Approved_Front_Side_Margin = Convert.ToString(dtrptmaster.Rows[i]["Approved_Front_Side_Margin"]);
                obj.Approved_Left_Side_Margin = Convert.ToString(dtrptmaster.Rows[i]["Approved_Left_Side_Margin"]);
                obj.Approved_Right_Side_Margin = Convert.ToString(dtrptmaster.Rows[i]["Approved_Right_Side_Margin"]);
                obj.Approved_Rear_Side_Margin = Convert.ToString(dtrptmaster.Rows[i]["Approved_Rear_Side_Margin"]);
                obj.Actual_Front_Side_Margin = Convert.ToString(dtrptmaster.Rows[i]["Actual_Front_Side_Margin"]);
                obj.Actual_Left_Side_Margin = Convert.ToString(dtrptmaster.Rows[i]["Actual_Left_Side_Margin"]);
                obj.Actual_Rear_Side_Margin = Convert.ToString(dtrptmaster.Rows[i]["Actual_Rear_Side_Margin"]);
                obj.Actual_Right_Side_Margin = Convert.ToString(dtrptmaster.Rows[i]["Actual_Right_Side_Margin"]);

                approvedList.Add(obj);
            }
            hInputPara.Clear();
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonApproved = jsonSerialiser.Serialize(approvedList);
            return Json(jsonApproved);

        }

        //UpdateAreaDetails
        [HttpPost]
        public ActionResult UpdateAreaDetails(string str)
        {
            //string Request_ID = Session["Request_ID1"].ToString();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            string ReqId = Session["RequestIDProj"].ToString();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(ReqId));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            string ProjectId = Convert.ToString(ProjId.Rows[0]["Project_ID"]);

            DataTable list4 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            //Area Details
            for (int i = 0; i < list4.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@projectId", ProjectId);
                hInputPara.Add("@BuildingId", Convert.ToInt32(list4.Rows[i]["BuildingID"]));
                hInputPara.Add("@wingId", Convert.ToInt32(list4.Rows[i]["Wing_ID"]));
                hInputPara.Add("@Request_ID", Convert.ToInt32(ReqId));
                hInputPara.Add("@DescriptionOfUnit", Convert.ToString(list4.Rows[i]["descriptionOfUnit"]));
                var result2 = sqlDataAccess.ExecuteStoreProcedure("usp_GetDescriptionOfUnitForProjValAreaDetils", hInputPara);
                hInputPara.Clear();
                if(result2== "document already exist")
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@projectId", ProjectId);
                    hInputPara.Add("@DescriptionOfUnit", Convert.ToString(list4.Rows[i]["descriptionOfUnit"]));
                    if (list4.Rows[i]["ApprovedCarpetArea"].ToString() == "")
                    {
                        int appCarpetArea = 0;
                        hInputPara.Add("@ApprovedCarpetarea", appCarpetArea);
                    }
                    else
                    {
                        hInputPara.Add("@ApprovedCarpetarea", Convert.ToDouble(list4.Rows[i]["ApprovedCarpetArea"]));
                    }
                    if (list4.Rows[i]["areaAsPerPlan"].ToString() == "")
                    {
                        int areaAsPerPlan = 0;
                        hInputPara.Add("@AreaAsperPlan", areaAsPerPlan);
                    }
                    else
                    {
                        hInputPara.Add("@AreaAsperPlan", Convert.ToDouble(list4.Rows[i]["areaAsPerPlan"]));
                    }
                    if (list4.Rows[i]["NoOfUnits"].ToString() == "")
                    {
                        int noOfunits = 0;
                        hInputPara.Add("@NoOfUnits", noOfunits);
                    }
                    else
                    {
                        hInputPara.Add("@NoOfUnits", Convert.ToDouble(list4.Rows[i]["NoOfUnits"]));
                    }

                    hInputPara.Add("@DetailsOfUnitDeviation", Convert.ToString(list4.Rows[i]["DetailsOfUnitDeviation"]));
                    hInputPara.Add("@BuildingId", Convert.ToInt32(list4.Rows[i]["BuildingID"]));
                    hInputPara.Add("@wingId", Convert.ToInt32(list4.Rows[i]["Wing_ID"]));
                    hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectvalAreaDetails", hInputPara);
                    hInputPara.Clear();

                }
                else
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@projectId", ProjectId);
                    hInputPara.Add("@DescriptionOfUnit", Convert.ToString(list4.Rows[i]["descriptionOfUnit"]));
                    if (list4.Rows[i]["ApprovedCarpetArea"].ToString() == "")
                    {
                        int appCarpetArea = 0;
                        hInputPara.Add("@ApprovedCarpetarea", appCarpetArea);
                    }
                    else
                    {
                        hInputPara.Add("@ApprovedCarpetarea", Convert.ToDouble(list4.Rows[i]["ApprovedCarpetArea"]));
                    }
                    if (list4.Rows[i]["areaAsPerPlan"].ToString() == "")
                    {
                        int areaAsPerPlan = 0;
                        hInputPara.Add("@AreaAsperPlan", areaAsPerPlan);
                    }
                    else
                    {
                        hInputPara.Add("@AreaAsperPlan", Convert.ToDouble(list4.Rows[i]["areaAsPerPlan"]));
                    }
                    if (list4.Rows[i]["NoOfUnits"].ToString() == "")
                    {
                        int noOfunits = 0;
                        hInputPara.Add("@NoOfUnits", noOfunits);
                    }
                    else
                    {
                        hInputPara.Add("@NoOfUnits", Convert.ToDouble(list4.Rows[i]["NoOfUnits"]));
                    }

                    hInputPara.Add("@DetailsOfUnitDeviation", Convert.ToString(list4.Rows[i]["DetailsOfUnitDeviation"]));
                    hInputPara.Add("@BuildingId", Convert.ToInt32(list4.Rows[i]["BuildingID"]));
                    hInputPara.Add("@wingId", Convert.ToInt32(list4.Rows[i]["Wing_ID"]));
                    hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
                    
                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectAreaDetails", hInputPara);
                    hInputPara.Clear();
                }
                if ((list4.Rows[i]["descriptionOfUnit"].ToString()) != null && (list4.Rows[i]["descriptionOfUnit"].ToString()) != string.Empty)
                {
                    hInputPara1 = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara1.Add("@Unitdescription", Convert.ToString(list4.Rows[i]["descriptionOfUnit"]));
                    //  hInputPara1.Add("@Document_ID", Convert.ToString(list1.Rows[i]["docid"]));
                    sqlDataAccess.ExecuteStoreProcedure("usp_InsertAreaDtailsDes", hInputPara1);
                }
            }
            return Json(new { success = true, Message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
        }
        //UpdateProjectConstrDetails
        [HttpPost]
        public ActionResult UpdateProjectConstructionDetails(string str)
        {
            
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            string ReqId = Session["RequestIDProj"].ToString();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(ReqId));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            string ProjectId = Convert.ToString(ProjId.Rows[0]["Project_ID"]);
            hInputPara1.Clear();
            
            DataTable list3 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            //Project Construction WorkProcess
            for (int i = 0; i < list3.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                //check projectFor Request Exist Or Not
                hInputPara.Add("@BuildingId", Convert.ToInt32(list3.Rows[i]["BuildingId"]));
                hInputPara.Add("@WingId", Convert.ToInt32(list3.Rows[i]["WingId"]));
                hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
                hInputPara.Add("@projectId", Convert.ToInt32(ProjectId));
                var result1 = sqlDataAccess.ExecuteStoreProcedure("usp_CheckProjValConsWorkProcExist", hInputPara);
                hInputPara.Clear();
                if (result1 == "document already exist")
                {

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();

                    hInputPara.Add("@projectId", ProjectId);
                    hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
                    hInputPara.Add("@nameOfBuilding", Convert.ToString(list3.Rows[i]["nameOfBuilding"]));
                    hInputPara.Add("@nameOfWing", Convert.ToString(list3.Rows[i]["nameofWing"]));
                    hInputPara.Add("@descriptionofStage", Convert.ToString(list3.Rows[i]["descriptionofstageOfConst"]));
                    hInputPara.Add("@workCompleted", Convert.ToString(list3.Rows[i]["workCompleted"]));
                    hInputPara.Add("@disbursementRecommended", Convert.ToString(list3.Rows[i]["dibursementrecom"]));
                    hInputPara.Add("@BuildingId", Convert.ToInt32(list3.Rows[i]["BuildingId"]));
                    hInputPara.Add("@WingId", Convert.ToInt32(list3.Rows[i]["WingId"]));
                    //hInputPara.Add("@createdBy", Convert.ToString(list1.Rows[i]["personMetatsite"]));
                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectvalConstructionWorkProcess", hInputPara);
                    hInputPara.Clear();
                }
                else
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@projectId", ProjectId);
                    hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
                    hInputPara.Add("@nameOfBuilding", Convert.ToString(list3.Rows[i]["nameOfBuilding"]));
                    hInputPara.Add("@nameOfWing", Convert.ToString(list3.Rows[i]["nameofWing"]));
                    hInputPara.Add("@descriptionofStage", Convert.ToString(list3.Rows[i]["descriptionofstageOfConst"]));
                    hInputPara.Add("@workCompleted", Convert.ToString(list3.Rows[i]["workCompleted"]));
                    hInputPara.Add("@disbursementRecommended", Convert.ToString(list3.Rows[i]["dibursementrecom"]));
                    hInputPara.Add("@BuildingId", Convert.ToInt32(list3.Rows[i]["BuildingId"]));
                    hInputPara.Add("@WingId", Convert.ToInt32(list3.Rows[i]["WingId"]));
                    
                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertprojValConstrWorkProcess", hInputPara);
                    hInputPara.Clear();
                }
            }
            return Json(new { success = true, Message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
        }
        //Update SideMargin Details
        [HttpPost]
        public ActionResult UpdateSideMarginDetails(string str)
        {           
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            sqlDataAccess1 = new SQLDataAccess();
            string ReqId = Session["RequestIDProj"].ToString();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(ReqId));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            string ProjectId = Convert.ToString(ProjId.Rows[0]["Project_ID"]);

            hInputPara1.Clear();
            DataTable list2 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

            //check record alreday exist or not
            hInputPara.Add("@projectId", ProjectId);
            hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
            hInputPara.Add("@BuildingId", Convert.ToInt32(list2.Rows[0]["BuildingID"]));
            hInputPara.Add("@WingId", Convert.ToInt32(list2.Rows[0]["Wing_ID"]));
            var result2 = sqlDataAccess1.ExecuteStoreProcedure("usp_CheckProjValSiteMarginExist", hInputPara);
            hInputPara.Clear();
            if(result2== "Record already exist")
            {
                hInputPara.Add("@Project_ID", ProjectId);
                hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
                hInputPara.Add("@BuildingId", Convert.ToInt32(list2.Rows[0]["BuildingID"]));
                hInputPara.Add("@WingId", Convert.ToInt32(list2.Rows[0]["Wing_ID"]));
                hInputPara.Add("@nameOfBuilding", Convert.ToString(list2.Rows[0]["BuildingName"]));
                hInputPara.Add("@nameOfWing", Convert.ToString(list2.Rows[0]["WingName"]));
                //side margin and deviation
                for (int i = 0; i < list2.Rows.Count; i++)
                {
                    sqlDataAccess = new SQLDataAccess();

                    string MarginRequirments = Convert.ToString(list2.Rows[i]["MarginRequirments"]);
                    if (MarginRequirments == "Front Margin")
                    {
                        hInputPara.Add("@ApprovedFrontSideMargin", Convert.ToString(list2.Rows[i]["ApprovedMargin"]));
                        hInputPara.Add("@ActualFrontSideMargin", Convert.ToString(list2.Rows[i]["ActualMargin"]));
                    }
                    if (MarginRequirments == "Rear Margin")
                    {
                        hInputPara.Add("@ApprovedRearSideMargin", Convert.ToString(list2.Rows[i]["ApprovedMargin"]));
                        hInputPara.Add("@ActualRearSideMargin", Convert.ToString(list2.Rows[i]["ActualMargin"]));
                    }
                    if (MarginRequirments == "Left Side")
                    {
                        hInputPara.Add("@ApprovedLeftSideMargin", Convert.ToString(list2.Rows[i]["ApprovedMargin"]));
                        hInputPara.Add("@ActualLeftSideMargin", Convert.ToString(list2.Rows[i]["ActualMargin"]));
                    }
                    if (MarginRequirments == "Right Side")
                    {
                        hInputPara.Add("@ApprovedRightSideMargin", Convert.ToString(list2.Rows[i]["ApprovedMargin"]));
                        hInputPara.Add("@ActualRightSideMargin", Convert.ToString(list2.Rows[i]["ActualMargin"]));
                    }

                    //for update data into projectSideMargin table
                    hInputPara1 = new Hashtable();
                    //sqlDataAccess1 = new SQLDataAccess();
                    hInputPara1.Add("@Project_ID", ProjectId);

                    hInputPara1.Add("@Building_ID", Convert.ToInt32(list2.Rows[0]["BuildingID"]));
                    hInputPara1.Add("@Wing_ID", Convert.ToInt32(list2.Rows[0]["Wing_ID"]));

                    hInputPara1.Add("@Side_Margin_Description", Convert.ToString(list2.Rows[i]["MarginRequirments"]));
                    hInputPara1.Add("@As_per_Approved", Convert.ToString(list2.Rows[i]["ApprovedMargin"]));
                    hInputPara1.Add("@as_Actuals", Convert.ToString(list2.Rows[i]["ActualMargin"]));
                    var result1 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectSideMarginsNew", hInputPara1);
                    hInputPara1.Clear();
                }
                sqlDataAccess = new SQLDataAccess();
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectvalSideMargin", hInputPara);
                hInputPara.Clear();
            }
            else
            {
                hInputPara.Add("@Project_ID", ProjectId);
                hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
                hInputPara.Add("@BuildingId", Convert.ToInt32(list2.Rows[0]["BuildingID"]));
                hInputPara.Add("@WingId", Convert.ToInt32(list2.Rows[0]["Wing_ID"]));
                hInputPara.Add("@nameOfBuilding", Convert.ToString(list2.Rows[0]["BuildingName"]));
                hInputPara.Add("@nameOfWing", Convert.ToString(list2.Rows[0]["WingName"]));
                //side margin and deviation
                for (int i = 0; i < list2.Rows.Count; i++)
                {
                    sqlDataAccess = new SQLDataAccess();

                    string MarginRequirments = Convert.ToString(list2.Rows[i]["MarginRequirments"]);
                    if (MarginRequirments == "Front Margin")
                    {
                        hInputPara.Add("@ApprovedFrontSideMargin", Convert.ToString(list2.Rows[i]["ApprovedMargin"]));
                        hInputPara.Add("@ActualFrontSideMargin", Convert.ToString(list2.Rows[i]["ActualMargin"]));
                    }
                    if (MarginRequirments == "Rear Margin")
                    {
                        hInputPara.Add("@ApprovedRearSideMargin", Convert.ToString(list2.Rows[i]["ApprovedMargin"]));
                        hInputPara.Add("@ActualRearSideMargin", Convert.ToString(list2.Rows[i]["ActualMargin"]));
                    }
                    if (MarginRequirments == "Left Side")
                    {
                        hInputPara.Add("@ApprovedLeftSideMargin", Convert.ToString(list2.Rows[i]["ApprovedMargin"]));
                        hInputPara.Add("@ActualLeftSideMargin", Convert.ToString(list2.Rows[i]["ActualMargin"]));
                    }
                    if (MarginRequirments == "Right Side")
                    {
                        hInputPara.Add("@ApprovedRightSideMargin", Convert.ToString(list2.Rows[i]["ApprovedMargin"]));
                        hInputPara.Add("@ActualRightSideMargin", Convert.ToString(list2.Rows[i]["ActualMargin"]));
                    }

                    //for update data into projectSideMargin table
                    hInputPara1 = new Hashtable();
                    //sqlDataAccess1 = new SQLDataAccess();
                    hInputPara1.Add("@Project_ID", ProjectId);

                    hInputPara1.Add("@Building_ID", Convert.ToInt32(list2.Rows[0]["BuildingID"]));
                    hInputPara1.Add("@Wing_ID", Convert.ToInt32(list2.Rows[0]["Wing_ID"]));

                    hInputPara1.Add("@Side_Margin_Description", Convert.ToString(list2.Rows[i]["MarginRequirments"]));
                    hInputPara1.Add("@As_per_Approved", Convert.ToString(list2.Rows[i]["ApprovedMargin"]));
                    hInputPara1.Add("@as_Actuals", Convert.ToString(list2.Rows[i]["ActualMargin"]));
                    var result1 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectSideMarginsNew", hInputPara1);
                    hInputPara1.Clear();
                }
                sqlDataAccess = new SQLDataAccess();
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectValuationSideMargins", hInputPara);
                hInputPara.Clear();

            }

            return Json(new { success = true, Message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ProjectUnitValuationView()
        {
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            VMBuilderGroupMaster vm = new VMBuilderGroupMaster();
            vm.ival_ProjectUnitDescriptionsList = GetProjectUnitDescription();
            vm.ival_LookupCategoryValuesRiskofdemolition = Getdemolition();
            vm.ival_lookupCategoryAmenities = GetProjectAmenitiesDescription();
            vm.ival_lookupProjectMaintainceProperty = GetMaintanceProperty();
            vm.ival_lookupProjectQualityConstruction = GetQualityOfConstruction();
            vm.ival_lookupProjectSideMargin = GetSideMargin();
            vm.ival_ProjectImageName = GetProjectMasterImageName();
            string radiovalues=Session["RadioProjValView"].ToString();

            string ReqId = Session["RequestIDProj"].ToString();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(ReqId));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            string ProjectId = Convert.ToString(ProjId.Rows[0]["Project_ID"]);
            hInputPara1.Clear();
            vm.UploadedImagesList = GetUploadedImageListEdit(ReqId, ProjectId);
            foreach (var data in vm.UploadedImagesList)
            {
                if (data.Image_Type == "Internal View")
                {
                    if (data.Seq_No == 1)
                    {
                        ViewBag.FilePathIn1 = data.FilePath;
                        ViewBag.ImagePathIn1 = data.Image_Path;
                        //TempData["ImagePathIn1"] = data.Image_Path;
                    }
                    if (data.Seq_No == 2)
                    {
                        ViewBag.FilePathIn2 = data.FilePath;
                        ViewBag.ImagePathIn2 = data.Image_Path;
                    }
                    if (data.Seq_No == 3)
                    {
                        ViewBag.FilePathIn3 = data.FilePath;
                        ViewBag.ImagePathIn3 = data.Image_Path;
                    }
                    if (data.Seq_No == 4)
                    {
                        ViewBag.FilePathIn4 = data.FilePath;
                        ViewBag.ImagePathIn4 = data.Image_Path;
                    }

                }
                if (data.Image_Type == "External View ")
                {
                    if (data.Seq_No == 1)
                    {
                        ViewBag.FilePathEx1 = data.FilePath;
                        ViewBag.ImagePathEx1 = data.Image_Path;
                    }
                    if (data.Seq_No == 2)
                    {
                        ViewBag.FilePathEx2 = data.FilePath;
                        ViewBag.ImagePathEx2 = data.Image_Path;
                    }
                    if (data.Seq_No == 3)
                    {
                        ViewBag.FilePathEx3 = data.FilePath;
                        ViewBag.ImagePathEx3 = data.Image_Path;
                    }
                    if (data.Seq_No == 4)
                    {
                        ViewBag.FilePathEx4 = data.FilePath;
                        ViewBag.ImagePathEx4 = data.Image_Path;
                    }

                }
                if (data.Image_Type == "Approach Road")
                {
                    if (data.Seq_No == 1)
                    {
                        ViewBag.FilePathAR1 = data.FilePath;
                        ViewBag.ImagePathAR1 = data.Image_Path;
                    }
                    if (data.Seq_No == 2)
                    {
                        ViewBag.FilePathAR2 = data.FilePath;
                        ViewBag.ImagePathAR2 = data.Image_Path;
                    }
                    if (data.Seq_No == 3)
                    {
                        ViewBag.FilePathAR3 = data.FilePath;
                        ViewBag.ImagePathAR3 = data.Image_Path;
                    }
                    if (data.Seq_No == 4)
                    {
                        ViewBag.FilePathAR4 = data.FilePath;
                        ViewBag.ImagePathAR4 = data.Image_Path;
                    }

                }
                if (data.Image_Type == "View from property")
                {
                    if (data.Seq_No == 1)
                    {
                        ViewBag.FilePathv1 = data.FilePath;
                        ViewBag.ImagePathv1 = data.Image_Path;
                    }
                    if (data.Seq_No == 2)
                    {
                        ViewBag.FilePathv2 = data.FilePath;
                        ViewBag.ImagePathv2 = data.Image_Path;
                    }
                    if (data.Seq_No == 3)
                    {
                        ViewBag.FilePathv3 = data.FilePath;
                        ViewBag.ImagePathv3 = data.Image_Path;
                    }
                    if (data.Seq_No == 4)
                    {
                        ViewBag.FilePathv4 = data.FilePath;
                        ViewBag.ImagePathv4 = data.Image_Path;
                    }

                }
                if (data.Image_Type == "IGR Photo")
                {
                    if (data.Seq_No == 1)
                    {
                        ViewBag.FilePathIg = data.FilePath;
                        ViewBag.ImagePathIg = data.Image_Path;
                    }
                }
                if (data.Image_Type == "Location Map")
                {
                    if (data.Seq_No == 1)
                    {
                        ViewBag.FilePathlm = data.FilePath;
                        ViewBag.ImagePathlm = data.Image_Path;
                    }
                }
            }
            if (radiovalues == "Entire Project")
            {
                vm.ival_ProjectBuildingMasters = GetBuildingsByEntireProject(ProjectId);
            }
            else
            {
                vm.ival_ProjectBuildingMasters = GetBuildingForPartofProjEdit(ProjectId);
            }
            hInputPara.Add("@ProjectId", Convert.ToInt32(ProjectId));
            //get visit Details
            DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectVistDetails", hInputPara);
            if (dtRequests.Rows.Count > 0)
            {
                if (dtRequests.Rows[0]["Date_Of_Inspection"].ToString() != "")
                {
                    string date = Convert.ToDateTime(dtRequests.Rows[0]["Date_Of_Inspection"]).ToString("dd/MM/yyyy");
                    ViewBag.DateOfInspection = date;
                }

                ViewBag.EnginnerVisitSide = Convert.ToString(dtRequests.Rows[0]["Engineer_Visiting_Site"]);
                ViewBag.PersonMetAtSide = Convert.ToString(dtRequests.Rows[0]["Person_Met_at_Site"]);
                ViewBag.personContactNo = Convert.ToString(dtRequests.Rows[0]["Person_Contact_Number"]);
                ViewBag.Deviation_Description = Convert.ToString(dtRequests.Rows[0]["Deviation_Description"]);
                ViewBag.Any_Deviation_observed = Convert.ToString(dtRequests.Rows[0]["Any_Deviation_observed"]);
                ViewBag.Rate_Range_In_Locality = Convert.ToString(dtRequests.Rows[0]["Rate_Range_In_Locality"]);
                ViewBag.Adopted_Base_Rate = Convert.ToString(dtRequests.Rows[0]["Adopted_Base_Rate"]);
                if (dtRequests.Rows[0]["GST_Applicability"].ToString() == "True")
                {
                    ViewBag.GST_Applicability = "Yes";
                }
                if (dtRequests.Rows[0]["GST_Applicability"].ToString() == "False")
                {
                    ViewBag.GST_Applicability = "No";
                }

                ViewBag.Parking_Charges = Convert.ToString(dtRequests.Rows[0]["Parking_Charges"]);
                ViewBag.Club_Membership_Charges = Convert.ToString(dtRequests.Rows[0]["Club_Membership_Charges"]);
                ViewBag.One_Time_Maintenance_Charges = Convert.ToString(dtRequests.Rows[0]["One_Time_Maintenance_Charges"]);
                ViewBag.Any_Other_Charges = Convert.ToString(dtRequests.Rows[0]["Any_Other_Charges"]);
                ViewBag.Government_Rate_Of_Unit = Convert.ToString(dtRequests.Rows[0]["Government_Rate_Of_Unit"]);
                ViewBag.Remarks = Convert.ToString(dtRequests.Rows[0]["Remarks"]);
                IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesRiskofdemolition = Getdemolition();
                if (ival_LookupCategoryValuesRiskofdemolition.ToList().Count > 0 && ival_LookupCategoryValuesRiskofdemolition != null)
                {
                    var riskOfDenomination = ival_LookupCategoryValuesRiskofdemolition.Where(s => s.Lookup_Value == dtRequests.Rows[0]["Risk_of_Demolition"].ToString()).Select(s => s.Lookup_ID);

                    vm.SelectedModelRiskDemo = Convert.ToInt32(riskOfDenomination.FirstOrDefault());

                }

            }
            hInputPara.Clear();
            //project Amenities Details
            vm.ival_ProjectValuationAmenities = GetProjectAmenitiesEdit(ProjectId, ReqId);

            hInputPara1.Add("@projecId", Convert.ToInt32(ProjectId));
            DataTable dt1 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetMaintancePropertyIdByLookUp", hInputPara1);
            if (dt1.Rows.Count > 0)
            {
                int maintainceId = Convert.ToInt32(dt1.Rows[0]["Lookup_ID"]);
                vm.MaintanceOfProperty = maintainceId;
            }
            hInputPara1.Clear();
            hInputPara1.Add("@projecId", Convert.ToInt32(ProjectId));
            DataTable dt2 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetQualityOfConIdByLookUp", hInputPara1);
            if (dt2.Rows.Count > 0)
            {
                int qualityOfCon = Convert.ToInt32(dt2.Rows[0]["Lookup_ID"]);
                vm.QualityOfConstruction = qualityOfCon;
            }
            hInputPara1.Clear();
            hInputPara.Clear();
            //project Broker Details
            hInputPara.Add("@Project_ID", Convert.ToInt32(ProjectId));
            hInputPara.Add("@Request_ID", Convert.ToInt32(ReqId));
            DataTable dtBroker = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectValBrokerDetails", hInputPara);
            if (dtBroker.Rows.Count > 0)
            {
                ViewBag.BrokerName1 = Convert.ToString(dtBroker.Rows[0]["Broker_Name"]);
                ViewBag.BrokerName2 = Convert.ToString(dtBroker.Rows[1]["Broker_Name"]);
                ViewBag.FirmName1 = Convert.ToString(dtBroker.Rows[0]["Firm_Name"]);
                ViewBag.FirmName2 = Convert.ToString(dtBroker.Rows[1]["Firm_Name"]);
                ViewBag.ContactNum1 = Convert.ToString(dtBroker.Rows[0]["Contact_Num"]);
                ViewBag.ContactNum2 = Convert.ToString(dtBroker.Rows[1]["Contact_Num"]);
                ViewBag.Rate_Quoted_For_Land1 = Convert.ToString(dtBroker.Rows[0]["Rate_Quoted_For_Land"]);
                ViewBag.Rate_Quoted_For_Land2 = Convert.ToString(dtBroker.Rows[1]["Rate_Quoted_For_Land"]);
                ViewBag.Rate_Quoted_For_Flat1 = Convert.ToString(dtBroker.Rows[0]["Rate_Quoted_For_Flat"]);
                ViewBag.Rate_Quoted_For_Flat2 = Convert.ToString(dtBroker.Rows[1]["Rate_Quoted_For_Flat"]);
                ViewBag.BrokerId1 = Convert.ToInt32(dtBroker.Rows[0]["Broker_ID"]);
                ViewBag.BrokerId2 = Convert.ToInt32(dtBroker.Rows[1]["Broker_ID"]);
            }
            hInputPara.Clear();
            //project Comparable Details
            hInputPara.Add("@Project_ID", Convert.ToInt32(ProjectId));
            hInputPara.Add("@Request_ID", Convert.ToInt32(ReqId));
            DataTable dtComparable = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectValComparableDetails", hInputPara);
            if (dtComparable.Rows.Count > 0)
            {
                ViewBag.Project_Name1 = Convert.ToString(dtComparable.Rows[0]["Project_Name"]);
                ViewBag.Project_Name2 = Convert.ToString(dtComparable.Rows[1]["Project_Name"]);
                ViewBag.Dist_Subject_Property1 = Convert.ToString(dtComparable.Rows[0]["Dist_Subject_Property"]);
                ViewBag.Dist_Subject_Property2 = Convert.ToString(dtComparable.Rows[1]["Dist_Subject_Property"]);
                ViewBag.Comparable_Unit_Details1 = Convert.ToString(dtComparable.Rows[0]["Comparable_Unit_Details"]);
                ViewBag.Comparable_Unit_Details2 = Convert.ToString(dtComparable.Rows[1]["Comparable_Unit_Details"]);
                ViewBag.Quality_And_Amenities1 = Convert.ToString(dtComparable.Rows[0]["Quality_And_Amenities"]);
                ViewBag.Quality_And_Amenities2 = Convert.ToString(dtComparable.Rows[1]["Quality_And_Amenities"]);
                ViewBag.Ongoing_Rate1 = Convert.ToString(dtComparable.Rows[0]["Ongoing_Rate"]);
                ViewBag.Ongoing_Rate2 = Convert.ToString(dtComparable.Rows[1]["Ongoing_Rate"]);
                ViewBag.Property_ID1 = Convert.ToInt32(dtComparable.Rows[0]["Property_ID"]);
                ViewBag.Property_ID2 = Convert.ToInt32(dtComparable.Rows[1]["Property_ID"]);
            }
            hInputPara.Clear();

            return View(vm);
        }
        [HttpPost]
        public ActionResult DeletePartOfProjWingsDetails(int?projectId,int?WingId)
        {
            try
            {

                hInputPara = new Hashtable();

                sqlDataAccess = new SQLDataAccess();
                string ReqId = Session["RequestIDProj"].ToString();
                hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
                hInputPara.Add("@ProjectId", Convert.ToInt32(projectId));
                hInputPara.Add("@wingId", Convert.ToInt32(WingId));
                string Result = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteProjValPartOfProj", hInputPara);

                return Json(new { success = true, Message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                return Json(new { success = false, Message = "Exist" }, JsonRequestBehavior.AllowGet);
            }
        }
        //UpdateProjectConstrDetails
        [HttpPost]
        public ActionResult UpdateProjectPricingDetails(string str)
        {

            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            string ReqId = Session["RequestIDProj"].ToString();
            hInputPara1.Add("@Request_ID", Convert.ToInt32(ReqId));
            DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            string ProjectId = Convert.ToString(ProjId.Rows[0]["Project_ID"]);
            hInputPara1.Clear();

            DataTable list3 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
           
            for (int i = 0; i < list3.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                //check projectFor Request Exist Or Not
                hInputPara.Add("@BuildingId", Convert.ToInt32(list3.Rows[i]["BuildingId"]));
                hInputPara.Add("@WingId", Convert.ToInt32(list3.Rows[i]["WingId"]));
                hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
                hInputPara.Add("@projectId", Convert.ToInt32(ProjectId));
                var result1 = sqlDataAccess.ExecuteStoreProcedure("usp_CheckProjValPricingExist", hInputPara);
                hInputPara.Clear();
                if (result1 == "document already exist")
                {

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();

                    hInputPara.Add("@projectId", ProjectId);
                    hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
                    hInputPara.Add("@BuildingId", Convert.ToInt32(list3.Rows[i]["BuildingId"]));
                    hInputPara.Add("@WingId", Convert.ToInt32(list3.Rows[i]["WingId"]));

                    //hInputPara.Add("@nameOfBuilding", Convert.ToString(list3.Rows[i]["nameOfBuilding"]));
                    hInputPara.Add("@nameOfWing", Convert.ToString(list3.Rows[i]["nameofWing"]));
                    hInputPara.Add("@CarpetBaseRate", Convert.ToString(list3.Rows[i]["BaseRate"]));
                    hInputPara.Add("@FloorWiseRate", Convert.ToString(list3.Rows[i]["FloorRiseRsPerSqFt"]));
                    hInputPara.Add("@PLCCharges", Convert.ToString(list3.Rows[i]["PLCCharges"]));
                    hInputPara.Add("@FloorWiseApplicable", Convert.ToString(list3.Rows[i]["FloorRiseApplicable"]));
                    hInputPara.Add("@UpdatedBy", Session["UserName"].ToString());
                    
                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectvalPricing", hInputPara);
                    hInputPara.Clear();
                }
                else
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@projectId", ProjectId);
                    hInputPara.Add("@RequestId", Convert.ToInt32(ReqId));
                    // hInputPara.Add("@nameOfBuilding", Convert.ToString(list3.Rows[i]["nameOfBuilding"]));
                    hInputPara.Add("@nameOfWing", Convert.ToString(list3.Rows[i]["nameofWing"]));
                    hInputPara.Add("@BuildingId", Convert.ToInt32(list3.Rows[i]["BuildingId"]));
                    hInputPara.Add("@WingId", Convert.ToInt32(list3.Rows[i]["WingId"]));

                    hInputPara.Add("@CarpetBaseRate", Convert.ToString(list3.Rows[i]["BaseRate"]));
                    hInputPara.Add("@FloorWiseRate", Convert.ToString(list3.Rows[i]["FloorRiseRsPerSqFt"]));
                    hInputPara.Add("@PLCCharges", Convert.ToString(list3.Rows[i]["PLCCharges"]));
                    hInputPara.Add("@FloorWiseApplicable", Convert.ToString(list3.Rows[i]["FloorRiseApplicable"]));
                    hInputPara.Add("@createdBy", Session["UserName"].ToString());
                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertprojValPrizingDetails", hInputPara);
                    hInputPara.Clear();
                }
            }
            return Json(new { success = true, Message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
        }
    }

}

