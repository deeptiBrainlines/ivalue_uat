﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using I_Value.WebApp.Models;
using IvalueDataAccess;
using IValuePublishProject.Models;
using Newtonsoft.Json;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Diagnostics;
//using iTextSharp.tool.xml;
namespace IValuePublishProject.Controllers
{
    public class NewRequestorSingleUnitController : Controller
    {
        

        SQLDataAccess sqlDataAccess;

        SQLDataAccess sqlDataAccess1;
        Hashtable hInputPara;
        Hashtable hInputPara1;


        IvalueDataModel Datamodel = new IvalueDataModel();
        // GET: NewRequestor
        public ActionResult Index(int? page, string SearchString)
        {
            try
            {
                PersonViewModel obj = new PersonViewModel();
                ViewModelRequest vmreq = new ViewModelRequest();
                int pageNumber = (page ?? 1);
                var CountToShowPerPage = 20;
                ViewBag.CurrentFilter = SearchString;

                if (Session["Role"].ToString() != null)
                {
                    vmreq.ival_LookupCategoryValuesRequestType = GetRequesttype();
                    vmreq.ival_LookupCategoryValuesRequestReportType = GetRequest_Report_Type();
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@PageNo", pageNumber);
                    hInputPara.Add("@RowCountPerPage", CountToShowPerPage);
                    hInputPara.Add("@Keyword", ViewBag.CurrentFilter);
                    hInputPara1 = new Hashtable();
                    hInputPara1.Add("@Keyword", ViewBag.CurrentFilter);
                    DataTable dtRequests, dtRequests1;
                    if (Convert.ToInt32(Session["Role"]) == 2)
                    {
                        hInputPara.Add("@FullName", Convert.ToString(Session["UserName"]));
                        hInputPara1.Add("@FullName", Convert.ToString(Session["UserName"]));
                        dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRequestIndex_Data", hInputPara);
                        dtRequests1 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRequestIndexCountD", hInputPara1);

                    }
                    else if (Convert.ToInt32(Session["Role"]) == 3)
                    {
                        hInputPara.Add("@Name", Session["Name"].ToString());
                        hInputPara1.Add("@Name", Session["Name"].ToString());
                        dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRequest_NewReqS", hInputPara);
                        dtRequests1 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRequestIndexCountSite", hInputPara1);

                    }
                    else
                    {
                        dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRequest_NewReq", hInputPara);
                        dtRequests1 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRequestIndexCount", hInputPara1);
                    }
                    List<VMRequestIndex> Listrequests = new List<VMRequestIndex>();


                    if (dtRequests.Rows.Count > 0)

                    {
                        for (int i = 0; i < dtRequests.Rows.Count; i++)
                        {
                            if (dtRequests.Rows[i]["D"] == DBNull.Value)
                            {
                                dtRequests.Rows[i]["D"] = 0;
                            }
                            if (dtRequests.Rows[i]["M"] == DBNull.Value)
                            {
                                dtRequests.Rows[i]["M"] = 0;
                            }
                            if (dtRequests.Rows[i]["Y"] == DBNull.Value)
                            {
                                dtRequests.Rows[i]["Y"] = 0;
                            }
                            Listrequests.Add(new VMRequestIndex
                            {
                                Request_ID = Convert.ToInt32(dtRequests.Rows[i]["Request_ID"]),
                                Request_Flag = Convert.ToString(dtRequests.Rows[i]["Request_Flag"]),
                                Requesttype = Convert.ToString(dtRequests.Rows[i]["Report_Request_Type"]),
                                Reporttype = Convert.ToString(dtRequests.Rows[i]["Report_Request_Type"]),
                                Customer_Name = Convert.ToString(dtRequests.Rows[i]["Customer_Name"]),
                                Client_Name = Convert.ToString(dtRequests.Rows[i]["Client_Name"]),
                                Property_Type = Convert.ToString(dtRequests.Rows[i]["Property_Type"]),
                                Typeofselect = Convert.ToString(dtRequests.Rows[i]["TypeOfSelect"]),
                                Typeofunit = Convert.ToString(dtRequests.Rows[i]["TypeOfUnit"]),
                                Status = Convert.ToString(dtRequests.Rows[i]["status"]),

                                //Project_Name = Convert.ToString(dtRequests.Rows[i]["Project_Name"]),
                                Legal_City = Convert.ToString(dtRequests.Rows[i]["City"]),
                                Legal_State = Convert.ToString(dtRequests.Rows[i]["State"]),
                                D = Convert.ToInt32(dtRequests.Rows[i]["D"]),
                                M = Convert.ToInt32(dtRequests.Rows[i]["M"]),
                                Y = Convert.ToInt32(dtRequests.Rows[i]["Y"]),

                            });
                        }
                    }

                    // obj.VMRequestIndex = Listrequests;
                    //ViewBag.TotalCount = 500;
                    var pager = new Pager((dtRequests != null && dtRequests.Rows.Count > 0) ? Convert.ToInt32(dtRequests1.Rows[0]["TotalRecords"]) : 0, page, CountToShowPerPage);

                    ////Save Current page number
                    //ViewBag.CurrentPage = pageNumber;
                    obj.ListPerson = Listrequests;
                    //vmreq.ListPerson = lstPerson;
                    obj.pager = pager;
                    return View(obj);
                }
                else
                {
                    return RedirectToAction("Index1", "Login");
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index1", "Login");
                //return RedirectToAction("Login", "Account");
            }
        }


        public ActionResult Index1test()
        {
            return View();
        }


        [HttpPost]
        public ActionResult ddlselectedvals(string str)
        {
            try
            {
                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

                return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public ActionResult AddNewRequestorDetails()
        {
            return View();
        }


        public ActionResult AddNewRequestor()
        {
            return View();
        }


        [HttpPost]
        public ActionResult AddRequestorDetails(string str)
        {
            try
            {
                DataTable list = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                TempData["values"] = list;
                TempData["CustomerName"] = list.Rows[0]["CustomerName"].ToString();
                // TempData["ValuationRequestId"] = list.Rows[0]["ValuationRequestId"].ToString();
                TempData["CustomerId"] = list.Rows[0]["CustomerId"].ToString();
                //TempData["reqname"] = list.Rows[0]["reqname"].ToString();
                //TempData["datepicker1"] = list.Rows[0]["datepicker1"].ToString();
                //TempData["txtnamdocholder"] = list.Rows[0]["txtnamdocholder"].ToString();
            }
            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return Json(Url.Action("RequestorDetails", "NewRequestor"));
        }


        public ActionResult RequestorDetails(string rb1, string rb2)
        {

            sqlDataAccess = new SQLDataAccess();

            DataTable dtCustID = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetcustID");
            ViewBag.APPID = dtCustID.Rows[0]["CustomerID"].ToString();
            DataTable ValuationRequestID = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetValuationRequestID");

            ViewBag.ValuationRequestID = ValuationRequestID.Rows[0]["ValuationRequestID"].ToString();
            Session["ValuationRequestID"] = ViewBag.ValuationRequestID;
            hInputPara = new Hashtable();




            DataTable DocID = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1DOCID");
            ViewBag.Srno = DocID.Rows[0]["Document_ID"].ToString();


            DataTable SrNo = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1DocSrNoId");
            ViewBag.SrnoNew = SrNo.Rows[0]["SrNo"].ToString();
            //if (TempData["CustomerName"].ToString()=="")
            //{
            //    ViewBag.CustomerName = "";
            //}
            //else
            //{
            ViewBag.CustomerName = TempData["CustomerName"];

            // }
            //ViewBag.ValuationRequestId = TempData["ValuationRequestId"];
            ViewBag.CustomerId = TempData["CustomerId"];
            //ViewBag.reqname = TempData["reqname"];
            //ViewBag.datepicker1 = TempData["datepicker1"];
            //ViewBag.txtnamdocholder = TempData["txtnamdocholder"];
            //ViewBag.RequestorName = TempData["reqname"];
            ViewModelRequest vm = new ViewModelRequest();
            vm.RequestType = rb1;
            vm.ReportRequestType = rb2;
            if (rb1 == "Bank/NBFC")
            {
                vm.IsBankNBFC = true;

            }
            else
            {
                vm.IsBankNBFC = false;
            }
            vm.ival_LookupCategoryValuestypeofownership = GetTypeofOwnership();
            //vm.ival_LookupCategoryValuestypeofstructure = Gettypestructure();
            vm.ival_LookupCategoryValuesvaluationtype = Getvaltype();
            vm.ival_LookupCategoryValuesmethodvaluation = Getmethodtype();
            vm.ival_LookupCategoryValuesreporttype = GetReportType();
            vm.ival_LookupCategoryDepartment = GetDepartment();
            vm.ival_LookupCategoryLoanType = GetLoanType();
            vm.ClientList = GetClientList();
            vm.ival_DocumentLists = GetDocuments();
            vm.ival_LookupEmp = GetEmp();
            return View(vm);
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetRelationsWithEmp1()
        {
            List<ival_LookupCategoryValues> ListFloorType1 = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtFloorType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Comparable");
            for (int i = 0; i < dtFloorType.Rows.Count; i++)
            {

                ListFloorType1.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtFloorType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtFloorType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtFloorType.Rows[i]["Lookup_Value"])

                });

            }
            return ListFloorType1;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetRelationsWithEmp2()
        {
            List<ival_LookupCategoryValues> ListFloorType1 = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtFloorType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_quality");
            for (int i = 0; i < dtFloorType.Rows.Count; i++)
            {

                ListFloorType1.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtFloorType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtFloorType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtFloorType.Rows[i]["Lookup_Value"])

                });

            }
            return ListFloorType1;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetRequesttype()
        {
            List<ival_LookupCategoryValues> Listreqtype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtreqtype = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRequestType");
            for (int i = 0; i < dtreqtype.Rows.Count; i++)
            {

                Listreqtype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtreqtype.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtreqtype.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtreqtype.Rows[i]["Lookup_Value"])

                });

            }
            return Listreqtype;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetRequest_Report_Type()
        {
            List<ival_LookupCategoryValues> Listreqrpttype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtreqrpttype = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRequest_Report_Type");
            for (int i = 0; i < dtreqrpttype.Rows.Count; i++)
            {

                Listreqrpttype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtreqrpttype.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtreqrpttype.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtreqrpttype.Rows[i]["Lookup_Value"])

                });

            }
            return Listreqrpttype;
        }





        [HttpPost]
        public List<ival_LookupCategoryValues> GetTypeofOwnership()
        {
            List<ival_LookupCategoryValues> Listtypeofowner = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dttypeofowner = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTypeOfOwnership");
            for (int i = 0; i < dttypeofowner.Rows.Count; i++)
            {

                Listtypeofowner.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dttypeofowner.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dttypeofowner.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dttypeofowner.Rows[i]["Lookup_Value"])

                });

            }
            return Listtypeofowner;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> Getvaltype()
        {
            List<ival_LookupCategoryValues> Listvaltype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtvaltype = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetValuationType");
            for (int i = 0; i < dtvaltype.Rows.Count; i++)
            {

                Listvaltype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtvaltype.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtvaltype.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtvaltype.Rows[i]["Lookup_Value"])

                });

            }
            return Listvaltype;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetReportType()
        {
            List<ival_LookupCategoryValues> ListReporttype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetReportType");
            for (int i = 0; i < dtGetReportType.Rows.Count; i++)
            {

                ListReporttype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtGetReportType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtGetReportType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtGetReportType.Rows[i]["Lookup_Value"])

                });

            }
            return ListReporttype;
        }


        [HttpPost]
        public List<ival_LookupCategoryValues> Getmethodtype()
        {
            List<ival_LookupCategoryValues> Listmethodtype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtmethodtype = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetMethodOfValuation");
            for (int i = 0; i < dtmethodtype.Rows.Count; i++)
            {

                Listmethodtype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtmethodtype.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtmethodtype.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtmethodtype.Rows[i]["Lookup_Value"])

                });

            }
            return Listmethodtype;
        }




        //to save requests by client
        [HttpPost]
        public ActionResult SaveRequestorDetails(string str)
        {
            try
            {
                string[] sourceValues = str.Split('.');

                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

                for (int i = 0; i < list1.Rows.Count; i++)
                {

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();

                    hInputPara.Add("@Request_Flag", "New_Request");
                    hInputPara.Add("@Requestor_Name", Convert.ToString(list1.Rows[i]["reqname"]));
                    hInputPara.Add("@Customer_Application_ID", Convert.ToString(list1.Rows[i]["CustomerId"]));
                    hInputPara.Add("@Customer_Name", Convert.ToString(list1.Rows[i]["CustomerName"]));
                    hInputPara.Add("@Customer_Name1", Convert.ToString(list1.Rows[i]["CustomerName1"]));
                    hInputPara.Add("@Customer_Contact_No_1", Convert.ToString(list1.Rows[i]["txtcontactno1"]));
                    hInputPara.Add("@Customer_Contact_No_2", Convert.ToString(list1.Rows[i]["txtcontactno2"]));
                    hInputPara.Add("@Request_Date", CDateTime(list1.Rows[i]["datepicker1"].ToString()));
                    hInputPara.Add("@Date_of_Inspection", CDateTime(list1.Rows[i]["datepicker2"].ToString()));
                    hInputPara.Add("@Date_of_Valuation", CDateTime(list1.Rows[i]["datepicker3"].ToString()));
                    hInputPara.Add("@Email_ID", Convert.ToString(list1.Rows[i]["txtemilid"]));
                    hInputPara.Add("@Valuation_Request_ID", Convert.ToString(list1.Rows[i]["ValuationRequestId"]));
                    hInputPara.Add("@Document_Holder_Name", Convert.ToString(list1.Rows[i]["txtnamdocholder"]));
                    hInputPara.Add("@Document_Holder_Name1", Convert.ToString(list1.Rows[i]["txtnamdocholder1"]));
                    hInputPara.Add("@Type_Of_Ownership", Convert.ToString(list1.Rows[i]["ddlowner"]));
                    hInputPara.Add("@ValuationType", Convert.ToString(list1.Rows[i]["ddlvaltypepropertymaster"]));
                    hInputPara.Add("@ReportType", Convert.ToString(list1.Rows[i]["ddlreporttype"]));
                    hInputPara.Add("@Method_of_Valuation", Convert.ToString(list1.Rows[i]["ddlmethodval"]));

                    hInputPara.Add("@RequestType", Convert.ToString(list1.Rows[i]["RequestType1"]));
                    hInputPara.Add("@ReportRequestType", Convert.ToString(list1.Rows[i]["ReportRequestType"]));
                    //  hInputPara.Add("@AssignedToEmpID", Convert.ToString(list1.Rows[i]["ddlsiteeng"]));
                    hInputPara.Add("@UserName", Convert.ToString(Session["UserName"]));
                    sqlDataAccess.ExecuteStoreProcedure("usp_InsertNewRequestorDetails", hInputPara);

                }

                //return Redirect("NewRequestBank");
                //return Json(Url.Ac tion("Index", "Request"));
                return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //to save requests by client
        [HttpPost]
        public ActionResult SaveSubsequentRequestorDetails(string str)
        {
            try
            {
                string[] sourceValues = str.Split('.');

                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

                for (int i = 0; i < list1.Rows.Count; i++)
                {

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();

                    hInputPara.Add("@Request_Flag", "Subsequent_Request");
                    hInputPara.Add("@Requestor_Name", Convert.ToString(list1.Rows[i]["reqname"]));
                    hInputPara.Add("@Customer_Application_ID", Convert.ToString(list1.Rows[i]["CustomerId"]));
                    hInputPara.Add("@Customer_Name", Convert.ToString(list1.Rows[i]["CustomerName"]));
                    hInputPara.Add("@Customer_Name1", Convert.ToString(list1.Rows[i]["CustomerName1"]));
                    hInputPara.Add("@Customer_Contact_No_1", Convert.ToString(list1.Rows[i]["txtcontactno1"]));
                    hInputPara.Add("@Customer_Contact_No_2", Convert.ToString(list1.Rows[i]["txtcontactno2"]));
                    hInputPara.Add("@Request_Date", CDateTime(list1.Rows[i]["datepicker1"].ToString()));
                    hInputPara.Add("@Email_ID", Convert.ToString(list1.Rows[i]["txtemilid"]));
                    hInputPara.Add("@Valuation_Request_ID", Convert.ToString(list1.Rows[i]["ValuationRequestId"]));
                    hInputPara.Add("@Document_Holder_Name", Convert.ToString(list1.Rows[i]["txtnamdocholder"]));
                    hInputPara.Add("@Document_Holder_Name1", Convert.ToString(list1.Rows[i]["txtnamdocholder1"]));
                    hInputPara.Add("@Type_Of_Ownership", Convert.ToString(list1.Rows[i]["ddlowner"]));
                    hInputPara.Add("@ValuationType", Convert.ToString(list1.Rows[i]["ddlvaltypepropertymaster"]));
                    hInputPara.Add("@ReportType", Convert.ToString(list1.Rows[i]["ddlreporttype"]));
                    hInputPara.Add("@Method_of_Valuation", Convert.ToString(list1.Rows[i]["ddlmethodval"]));
                    hInputPara.Add("@RequestType", Convert.ToString(list1.Rows[i]["RequestType1"]));
                    hInputPara.Add("@ReportRequestType", Convert.ToString(list1.Rows[i]["ReportRequestType"]));
                    //  hInputPara.Add("@AssignedToEmpID", Convert.ToString(list1.Rows[i]["ddlsiteeng"]));
                    hInputPara.Add("@UserName", Convert.ToString(Session["UserName"]));
                    sqlDataAccess.ExecuteStoreProcedure("usp_InsertNewRequestorDetails", hInputPara);

                }

                //return Redirect("NewRequestBank");
                //return Json(Url.Ac tion("Index", "Request"));
                return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        //[HttpPost]
        //public ActionResult SaveSubSequentRequestorDetails(string str)
        //{
        //    try
        //    {
        //        DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
        //        for (int i = 0; i < list1.Rows.Count; i++)
        //        {

        //            hInputPara = new Hashtable();
        //            sqlDataAccess = new SQLDataAccess();

        //            hInputPara.Add("@Request_Flag", "Subsequent Request");
        //            //   hInputPara.Add("@Requestor_Name", Convert.ToString(list1.Rows[i]["requestorname"]));
        //            hInputPara.Add("@Customer_Application_ID", Convert.ToString(list1.Rows[i]["CustomerId"]));
        //            hInputPara.Add("@Customer_Name", Convert.ToString(list1.Rows[i]["CustomerName"]));
        //            hInputPara.Add("@Customer_Contact_No_1", Convert.ToString(list1.Rows[i]["txtcontactno1"]));
        //            hInputPara.Add("@Customer_Contact_No_2", Convert.ToString(list1.Rows[i]["txtcontactno2"]));
        //            hInputPara.Add("@Request_Date", Convert.ToString(list1.Rows[i]["datepicker1"]));
        //            hInputPara.Add("@Email_ID", Convert.ToString(list1.Rows[i]["txtemilid"]));
        //            hInputPara.Add("@Valuation_Request_ID", Convert.ToString(list1.Rows[i]["ValuationRequestId"]));
        //            hInputPara.Add("@Document_Holder_Name", Convert.ToString(list1.Rows[i]["txtnamdocholder"]));
        //            hInputPara.Add("@Type_Of_Ownership", Convert.ToString(list1.Rows[i]["ddlowner"]));
        //            hInputPara.Add("@ValuationType", Convert.ToString(list1.Rows[i]["ddlvaltypepropertymaster"]));
        //            hInputPara.Add("@ReportType", Convert.ToString(list1.Rows[i]["ddlreporttype"]));
        //            hInputPara.Add("@Method_of_Valuation", Convert.ToString(list1.Rows[i]["ddlmethodval"]));
        //            // hInputPara.Add("@AssignedToEmpID", Convert.ToString(list1.Rows[i]["ddlsiteeng"]));
        //            sqlDataAccess.ExecuteStoreProcedure("usp_InsertNewRequestorDetails", hInputPara);

        //        }

        //        //return Redirect("NewRequestBank");
        //        //return Json(Url.Ac tion("Index", "Request"));
        //        return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

        [HttpPost]
        public ActionResult UpdateRequestorDetails(string str)
        {
            try
            {
                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                for (int i = 0; i < list1.Rows.Count; i++)
                {

                    //changes by punam
                    int client_id = 0;
                    if (Convert.ToString(list1.Rows[i]["Client_ID"]) != "")
                    {
                        string client_name = Convert.ToString(list1.Rows[i]["Client_ID"]);
                        string Branch_name = Convert.ToString(list1.Rows[i]["BranchName"]);

                        client_id = GetClientId(client_name, Branch_name);
                    }


                    hInputPara = new Hashtable();

                    sqlDataAccess = new SQLDataAccess();
                    //sqlDataAccess.ExecuteStoreProcedure("usp_GetClientIDByClientName", client_id);
                    int RequestID = Convert.ToInt32(Session["RequestID"].ToString());
                    hInputPara.Add("@Request_ID", RequestID);
                    hInputPara.Add("@Request_Flag", "Update Request");
                    //hInputPara.Add("@Requestor_Name", Convert.ToString(list1.Rows[i]["requestorname"]));
                    hInputPara.Add("@Customer_Application_ID", Convert.ToString(list1.Rows[i]["CustomerId"]));
                    hInputPara.Add("@Customer_Name", Convert.ToString(list1.Rows[i]["CustomerName"]));
                    hInputPara.Add("@Customer_Name_1", Convert.ToString(list1.Rows[i]["CustomerName1"]));
                    hInputPara.Add("@Customer_Contact_No_1", Convert.ToString(list1.Rows[i]["txtcontactno1"]));
                    hInputPara.Add("@Customer_Contact_No_2", Convert.ToString(list1.Rows[i]["txtcontactno2"]));
                    try
                    {
                        var dateString = list1.Rows[i]["datepicker1"].ToString();
                        //var format = "dd/MM/yyyy";
                        if (dateString != "")
                        {
                            var dateTime = CDateTime(dateString);
                            hInputPara.Add("@Request_Date", dateTime);
                        }
                        // hInputPara.Add("@Request_Date", Convert.ToDateTime(list1.Rows[i]["datepicker1"]));
                    }
                    catch (Exception e)
                    {
                        try
                        {


                            string dateString = list1.Rows[i]["datepicker1"].ToString();
                            if (dateString != "")
                            {

                                var dateTime = CDateTime(dateString);
                                hInputPara.Add("@Request_Date", dateTime);
                            }
                        }
                        catch (Exception ee)
                        {

                        }

                    }
                    try
                    {
                        var dateString = list1.Rows[i]["DateOfInspection"].ToString();

                        if (dateString != "")
                        {
                            var dateTime = CDateTime(dateString);
                            hInputPara.Add("@DateOfInspection", dateTime);
                        }
                        // hInputPara.Add("@Request_Date", Convert.ToDateTime(list1.Rows[i]["datepicker1"]));
                    }
                    catch (Exception e)
                    {
                        try
                        {


                            string dateString = list1.Rows[i]["DateOfInspection"].ToString();
                            if (dateString != "")
                            {

                                var dateTime = CDateTime(dateString);
                                hInputPara.Add("@DateOfInspection", dateTime);
                            }
                        }
                        catch (Exception ee)
                        {

                        }

                    }
                    try
                    {
                        var dateString = list1.Rows[i]["DateOfvaluation"].ToString();
                        //var format = "dd/MM/yyyy";
                        if (dateString != "")
                        {
                            var dateTime = CDateTime(dateString);
                            hInputPara.Add("@DateOfValuation", dateTime);
                        }
                        // hInputPara.Add("@Request_Date", Convert.ToDateTime(list1.Rows[i]["datepicker1"]));
                    }
                    catch (Exception e)
                    {
                        try
                        {


                            string dateString = list1.Rows[i]["DateOfvaluation"].ToString();
                            if (dateString != "")
                            {

                                // string datestring1 = Convert.ToDateTime(dateString).ToString("dd/MM/yyyy");
                                //    var format = "dd/MM/yyyy";
                                var dateTime = CDateTime(dateString);
                                hInputPara.Add("@DateOfValuation", dateTime);
                            }
                        }
                        catch (Exception ee)
                        {

                        }

                    }
                    hInputPara.Add("@Email_ID", Convert.ToString(list1.Rows[i]["txtemilid"]));
                    hInputPara.Add("@Valuation_Request_ID", Convert.ToString(list1.Rows[i]["ValuationRequestId"]));
                    hInputPara.Add("@Document_Holder_Name", Convert.ToString(list1.Rows[i]["txtnamdocholder"]));
                    hInputPara.Add("@Document_Holder_Name_1", Convert.ToString(list1.Rows[i]["txtnamdocholder1"]));
                    hInputPara.Add("@Requestor_Name", Convert.ToString(list1.Rows[i]["txtnameofrequestor"]));

                    hInputPara.Add("@ValuationType", Convert.ToString(list1.Rows[i]["ddlvaltypepropertymaster"]));
                    hInputPara.Add("@ReportType", Convert.ToString(list1.Rows[i]["ddlreporttype"]));
                    hInputPara.Add("@Method_of_Valuation", Convert.ToString(list1.Rows[i]["ddlmethodval"]));
                    //hInputPara.Add("@Client_ID", Convert.ToString(list1.Rows[i]["Client_ID"]));

                    hInputPara.Add("@Client_ID", client_id);
                    hInputPara.Add("@Department", Convert.ToString(list1.Rows[i]["Department"]));
                    hInputPara.Add("@LoanType", Convert.ToString(list1.Rows[i]["LoanType"]));
                    // hInputPara.Add("@AssignedToEmpID", Convert.ToString(list1.Rows[i]["ddlsiteeng"]));
                    sqlDataAccess.ExecuteStoreProcedure("usp_UpdateNewRequestorDetails", hInputPara);

                    //changes by punam handle while got exception
                    return Json(new { success = true, Message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);

                }
                return Json(new { success = false, Message = "Does Not Updated Successfully" }, JsonRequestBehavior.AllowGet);


                //return Redirect("NewRequestBank");
                //return Json(Url.Ac tion("Index", "Request"));


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        //method Added by punam
        public DateTime CDateTime(string SEDateTime)
        {



            int seconds = DateTime.Now.Second;
            int min = DateTime.Now.Minute;
            int hr = DateTime.Now.Hour;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-CA", true);
            string st = SEDateTime;

            string[] sdatesplit = st.Split('/');


            string startdate = sdatesplit[2] + "-" + sdatesplit[1] + "-" + sdatesplit[0] + ' ' + hr + ':' + min + ':' + seconds;
            DateTime dt = DateTime.Parse(startdate, provider, System.Globalization.DateTimeStyles.NoCurrentDateDefault);
            return dt;
        }
        public int GetClientId(string clientName, string BranchName)
        {
            int client_id = 0;
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Client_Name", clientName);
            hInputPara.Add("@Branch_Name", BranchName);
            DataTable list1 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientIDByClientName", hInputPara);
            for (int i = 0; i < list1.Rows.Count; i++)
            {
                //changes by punam
                client_id = Convert.ToInt32(list1.Rows[i]["Client_ID"]);
            }
            return client_id;

        }

        [HttpPost]
        public ActionResult UpdatePropertyDetails(string str, string str4, string stringBasicInfra, string stringSocialDev, string stringfloor, string stringupper, string str3, string strground, string str5, string str6)
        {
            try
            {
                hInputPara = new Hashtable();
                hInputPara1 = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                DataTable list4 = (DataTable)JsonConvert.DeserializeObject(str4, (typeof(DataTable)));
                DataTable listBasic = (DataTable)JsonConvert.DeserializeObject(stringBasicInfra, (typeof(DataTable)));
                DataTable listSocialDEv = (DataTable)JsonConvert.DeserializeObject(stringSocialDev, (typeof(DataTable)));
                DataTable listfloor = (DataTable)JsonConvert.DeserializeObject(stringfloor, (typeof(DataTable)));
                DataTable listupper = (DataTable)JsonConvert.DeserializeObject(stringupper, (typeof(DataTable)));
                DataTable listBasement = (DataTable)JsonConvert.DeserializeObject(str3, (typeof(DataTable)));
                DataTable listG = (DataTable)JsonConvert.DeserializeObject(strground, (typeof(DataTable)));
                DataTable listS = (DataTable)JsonConvert.DeserializeObject(str5, (typeof(DataTable)));
                DataTable listsecond = (DataTable)JsonConvert.DeserializeObject(str6, (typeof(DataTable)));
                int RequestID = Convert.ToInt32(Session["RequestID"]);
                hInputPara.Add("@Request_ID", RequestID);
                DataTable dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRetailPropertyByReqID", hInputPara);
                DataTable dtprojectid = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprjIDByRequestID", hInputPara);

                for (int i = 0; i < list1.Rows.Count; i++)
                {

                    hInputPara.Add("@RequestID", RequestID);

                    hInputPara.Add("@PlotBunglowNumber", Convert.ToString(list1.Rows[i]["PlotNumber"]));
                    hInputPara.Add("@BunglowNumber", Convert.ToString(list1.Rows[i]["BunglowNumber"]));
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@Survey_Number", Convert.ToString(list1.Rows[i]["SurveyNumberSingle"]));
                    }
                    else
                    {
                        hInputPara.Add("@Survey_Number", Convert.ToString(list1.Rows[i]["SurveyNumber"]));
                    }

                    hInputPara.Add("@UnitNumber", Convert.ToString(list1.Rows[i]["UnitNumber"]));
                    hInputPara.Add("@Project_Name", Convert.ToString(list1.Rows[i]["ProjectName"]));
                    hInputPara.Add("@Building_Name_RI", Convert.ToString(list1.Rows[i]["BuildingName"]));
                    hInputPara.Add("@Wing_Name_RI", Convert.ToString(list1.Rows[i]["WingName"]));

                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@Village_Name", Convert.ToString(list1.Rows[i]["VillageNameSingle"]));
                    }
                    else
                    {
                        hInputPara.Add("@Village_Name", Convert.ToString(list1.Rows[i]["VillageName"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@Street", Convert.ToString(list1.Rows[i]["StreetSingle"]));
                    }
                    else
                    {
                        hInputPara.Add("@Street", Convert.ToString(list1.Rows[i]["Street"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@Locality", Convert.ToString(list1.Rows[i]["LocalitySingle"]));
                    }
                    else
                    {
                        hInputPara.Add("@Locality", Convert.ToString(list1.Rows[i]["Locality"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@Sub_Locality", Convert.ToString(list1.Rows[i]["SubLocalitySingle"]));
                    }
                    else
                    {
                        hInputPara.Add("@Sub_Locality", Convert.ToString(list1.Rows[i]["SubLocality"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@City", Convert.ToString(list1.Rows[i]["CitySingle"]));
                    }
                    else
                    {
                        hInputPara.Add("@City", Convert.ToString(list1.Rows[i]["City"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@District", Convert.ToString(list1.Rows[i]["DistrictSingle"]));
                    }
                    else
                    {
                        hInputPara.Add("@District", Convert.ToString(list1.Rows[i]["District"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@State", Convert.ToString(list1.Rows[i]["StateSingle"]));
                    }
                    else
                    {
                        hInputPara.Add("@State", Convert.ToString(list1.Rows[i]["State"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@PinCode", Convert.ToString(list1.Rows[i]["PincodeSingle"]));
                    }
                    else
                    {
                        hInputPara.Add("@PinCode", Convert.ToString(list1.Rows[i]["Pincode"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@NearBy_Landmark", list1.Rows[i]["NearByLandmarkPlot"].ToString());
                    }
                    else
                    {
                        hInputPara.Add("@NearBy_Landmark", list1.Rows[i]["NearByLandmark"].ToString());
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Nearest_RailwayStation", list1.Rows[i]["NearRailwayStationPlot"].ToString());
                    }
                    else
                    {
                        hInputPara.Add("@Nearest_RailwayStation", list1.Rows[i]["NearRailwayStation"].ToString());
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Nearest_BusStop", list1.Rows[i]["NearBusStopPlot"].ToString());
                    }
                    else
                    {
                        hInputPara.Add("@Nearest_BusStop", list1.Rows[i]["NearBusStop"].ToString());
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Nearest_Hospital", list1.Rows[i]["NearHospitalPlot"].ToString());
                    }
                    else
                    {
                        hInputPara.Add("@Nearest_Hospital", list1.Rows[i]["NearHospital"].ToString());
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Nearest_Market", list1.Rows[i]["NearMarketPlot"].ToString());
                    }
                    else
                    {
                        hInputPara.Add("@Nearest_Market", list1.Rows[i]["NearMarket"].ToString());
                    }
                    hInputPara.Add("@RERA_Approval_Num", Convert.ToString(list1.Rows[i]["RegistrationNumber"]));
                    hInputPara.Add("@Approval_Flag", Convert.ToString(list1.Rows[i]["ApprovalFlag"]));
                    hInputPara.Add("@Actual_No_Floors_G", Convert.ToString(list1.Rows[i]["ActualNumberOfFloars"]));
                    hInputPara.Add("@Floor_Of_Unit", Convert.ToString(list1.Rows[i]["FloorOfUnits"]));
                    hInputPara.Add("@Property_Description", Convert.ToString(list1.Rows[i]["PropertyDescription"]));
                    hInputPara.Add("@Type_Of_Structure", Convert.ToString(list1.Rows[i]["TypeOfStructure"]));
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Approved_Usage_Of_Property", list1.Rows[i]["ApprovedUsageOfPropertyPlot"].ToString());
                    }
                    else
                    {
                        hInputPara.Add("@Approved_Usage_Of_Property", list1.Rows[i]["ApprovedUsageOfProperty"].ToString());
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Actual_Usage_Of_Property", list1.Rows[i]["ActualUsageOfPropertyPlot"].ToString());
                    }
                    else
                    {
                        hInputPara.Add("@Actual_Usage_Of_Property", list1.Rows[i]["ActualUsageOfProperty"].ToString());
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Current_Age_Of_Property", Convert.ToString(list1.Rows[i]["CurrentAgePropertyPlot"]));
                    }
                    else
                    {
                        hInputPara.Add("@Current_Age_Of_Property", Convert.ToString(list1.Rows[i]["CurrentAgeProperty"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Residual_Age_Of_Property", Convert.ToString(list1.Rows[i]["ResidualAgePropertyPlot"]));
                    }
                    else
                    {
                        hInputPara.Add("@Residual_Age_Of_Property", Convert.ToString(list1.Rows[i]["ResidualAgeProperty"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Common_Areas", Convert.ToString(list1.Rows[i]["CommonAreaPlot"]));
                    }
                    else
                    {
                        hInputPara.Add("@Common_Areas", Convert.ToString(list1.Rows[i]["CommonArea"]));
                    }
                    hInputPara.Add("@Facilities", Convert.ToString(list1.Rows[i]["Facilities"]));
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Anyother_Observation", Convert.ToString(list1.Rows[i]["AnyOtherObservationPlot"]));
                    }
                    else
                    {
                        hInputPara.Add("@Anyother_Observation", Convert.ToString(list1.Rows[i]["AnyOtherObservation"]));
                    }
                    hInputPara.Add("@Roofing_Terracing", Convert.ToString(list1.Rows[i]["RoofingandTerrecing"]));
                    hInputPara.Add("@Quality_Of_Fixture", Convert.ToString(list1.Rows[i]["QualityOfFixture"]));
                    hInputPara.Add("@Quality_Of_Construction", Convert.ToString(list1.Rows[i]["QualityOfConstruction"]));
                    hInputPara.Add("@Maintenance_Of_Property", Convert.ToString(list1.Rows[i]["MaintenceOfProperty"]));
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Marketability_Of_Property", Convert.ToString(list1.Rows[i]["MarketabilityOfPropertyPlot"]));
                    }
                    else
                    {
                        hInputPara.Add("@Marketability_Of_Property", Convert.ToString(list1.Rows[i]["MarketabilityOfProperty"]));
                    }
                    hInputPara.Add("@Occupancy_Details", Convert.ToString(list1.Rows[i]["OccupancyDetail"]));
                    hInputPara.Add("@Renting_Potential", Convert.ToString(list1.Rows[i]["RenetingPotential"]));
                    hInputPara.Add("@Expected_Monthly_Rental", Convert.ToString(list1.Rows[i]["ExpectedMonthlyRental"]));
                    hInputPara.Add("@Occupied_Since", Convert.ToString(list1.Rows[i]["OccupiedSince"]));
                    hInputPara.Add("@Monthly_Rental", Convert.ToString(list1.Rows[i]["MonthlyRental"]));
                    hInputPara.Add("@Balanced_Leased_Period", Convert.ToString(list1.Rows[i]["BalancedLeasedPeriod"]));
                    hInputPara.Add("@Class_of_Locality", Convert.ToString(list1.Rows[i]["ClassOfLocality"]));
                    hInputPara.Add("@Type_of_Locality", Convert.ToString(list1.Rows[i]["TypeOfLocality"]));
                    hInputPara.Add("@DistFrom_City_Center", Convert.ToString(list1.Rows[i]["DistanceFromCityCenter"]));
                    hInputPara.Add("@Locality_Remarks", Convert.ToString(list1.Rows[i]["Remark"]));

                    sqlDataAccess.ExecuteStoreProcedure("usp_UpdatePropertyDetails", hInputPara);

                }

                for (int i4 = 0; i4 < listSocialDEv.Rows.Count; i4++)
                {
                    //int RequestID = Convert.ToInt32(Session["RequestID"].ToString());

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dtprojectid.Rows[0]["Project_ID"]));
                    hInputPara.Add("@Social_Development_Type", Convert.ToString(listSocialDEv.Rows[i4]["value"]));
                    hInputPara.Add("@Created_By", "Branch Head");
                    var res3 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectUnitTypes", hInputPara);
                }

                for (int i4 = 0; i4 < list4.Rows.Count; i4++)
                {
                    //DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1ProjectIdFromProjectMasterNew");

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dtprojectid.Rows[0]["Project_ID"]));
                    hInputPara.Add("@LocalTransport_ID", Convert.ToString(list4.Rows[i4]["value"]));
                    hInputPara.Add("@Created_By", "Branch Head");
                    var res4 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateLocaltrnasport", hInputPara);
                }


                for (int i4 = 0; i4 < listBasic.Rows.Count; i4++)
                {
                    DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1ProjectIdFromProjectMasterNew");

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dtprojectid.Rows[0]["Project_ID"]));
                    hInputPara.Add("@BasicInfra_type", Convert.ToString(listBasic.Rows[i4]["value1"]));
                    hInputPara.Add("@Created_By", "Branch Head");
                    var res5 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateBasicInfra", hInputPara);
                }

                //DataTable dttop1projectIdforfloor = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1ProjectIdFromProjectMasterNew");
                //for (int u = 0; u < listfloor.Rows.Count; u++)
                //{
                //    hInputPara = new Hashtable();
                //    sqlDataAccess = new SQLDataAccess();
                //    hInputPara.Add("@Project_ID", Convert.ToInt32(dttop1projectIdforfloor.Rows[0]["Project_ID"]));
                //    hInputPara.Add("@FloorType_ID", listfloor.Rows[0]["v"].ToString());
                //    string FloorTypeId = listfloor.Rows[0]["v"].ToString();
                //    if (FloorTypeId == "Podium")
                //    {

                //        hInputPara.Add("@Number_Of_Floors", Session["Podium"].ToString());
                //    }

                //    var res6 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingFloorDetails", hInputPara);
                //}

                //for (int u = 0; u < listupper.Rows.Count; u++)
                //{
                //    hInputPara = new Hashtable();
                //    sqlDataAccess = new SQLDataAccess();
                //    hInputPara.Add("@Project_ID", Convert.ToInt32(dttop1projectIdforfloor.Rows[0]["Project_ID"]));
                //    hInputPara.Add("@FloorType_ID", listupper.Rows[0]["v"].ToString());
                //    string FloorTypeId = listupper.Rows[0]["v"].ToString();
                //    if (FloorTypeId == "Upper Habitable Floors")
                //    {

                //        hInputPara.Add("@Number_Of_Floors", Session["Upper"]);
                //    }

                //    var res7 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingFloorDetails", hInputPara);
                //}
                //for (int u = 0; u < listBasement.Rows.Count; u++)
                //{
                //    hInputPara = new Hashtable();
                //    sqlDataAccess = new SQLDataAccess();
                //    hInputPara.Add("@Project_ID", Convert.ToInt32(dttop1projectIdforfloor.Rows[0]["Project_ID"]));
                //    hInputPara.Add("@FloorType_ID", listBasement.Rows[0]["v"].ToString());
                //    string FloorTypeId = listBasement.Rows[0]["v"].ToString();
                //    if (FloorTypeId == "Basement")
                //    {
                //        hInputPara.Add("@Number_Of_Floors", Session["Basement"].ToString());
                //    }

                //    var res8 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingFloorDetails", hInputPara);
                //}
                //for (int u = 0; u < listG.Rows.Count; u++)
                //{
                //    hInputPara = new Hashtable();
                //    sqlDataAccess = new SQLDataAccess();
                //    hInputPara.Add("@Project_ID", Convert.ToInt32(dttop1projectIdforfloor.Rows[0]["Project_ID"]));
                //    hInputPara.Add("@FloorType_ID", listG.Rows[0]["v"].ToString());
                //    string FloorTypeId = listG.Rows[0]["v"].ToString();
                //    if (FloorTypeId == "Ground")
                //    {

                //        hInputPara.Add("@Number_Of_Floors", Session["Ground"].ToString());
                //    }

                //    var res9 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingFloorDetails", hInputPara);
                //}
                //for (int u = 0; u < listS.Rows.Count; u++)
                //{
                //    hInputPara = new Hashtable();
                //    sqlDataAccess = new SQLDataAccess();
                //    hInputPara.Add("@Project_ID", Convert.ToInt32(dttop1projectIdforfloor.Rows[0]["Project_ID"]));
                //    hInputPara.Add("@FloorType_ID", listS.Rows[0]["v"].ToString());
                //    string FloorTypeId = listS.Rows[0]["v"].ToString();
                //    if (FloorTypeId == "Stilt")
                //    {

                //        hInputPara.Add("@Number_Of_Floors", Session["Stilt"].ToString());
                //    }

                //    var res10 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingFloorDetails", hInputPara);
                //}
                //for (int u = 0; u < listsecond.Rows.Count; u++)
                //{
                //    hInputPara = new Hashtable();
                //    sqlDataAccess = new SQLDataAccess();
                //    hInputPara.Add("@Project_ID", Convert.ToInt32(dttop1projectIdforfloor.Rows[0]["Project_ID"]));
                //    hInputPara.Add("@FloorType_ID", listsecond.Rows[0]["v"].ToString());
                //    string FloorTypeId = listsecond.Rows[0]["v"].ToString();
                //    if (FloorTypeId == "second floor")
                //    {

                //        hInputPara.Add("@Number_Of_Floors", Session["secondfloor"].ToString());
                //    }

                //    var res11 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingFloorDetails", hInputPara);
                //}


                return Json(new { success = true, Message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        //Update Property Details method for Test
        [HttpPost]
        public ActionResult UpdatePropertyDetailsNew(string str, string str4, string stringBasicInfra, string stringSocialDev, string stringfloor, string stringupper, string str3, string strground, string str5, string str6)
        {
            try
            {
                hInputPara = new Hashtable();
                hInputPara1 = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                DataTable list4 = (DataTable)JsonConvert.DeserializeObject(str4, (typeof(DataTable)));
                DataTable listBasic = (DataTable)JsonConvert.DeserializeObject(stringBasicInfra, (typeof(DataTable)));
                DataTable listSocialDEv = (DataTable)JsonConvert.DeserializeObject(stringSocialDev, (typeof(DataTable)));
                DataTable listfloor = (DataTable)JsonConvert.DeserializeObject(stringfloor, (typeof(DataTable)));
                DataTable listupper = (DataTable)JsonConvert.DeserializeObject(stringupper, (typeof(DataTable)));
                DataTable listBasement = (DataTable)JsonConvert.DeserializeObject(str3, (typeof(DataTable)));
                DataTable listG = (DataTable)JsonConvert.DeserializeObject(strground, (typeof(DataTable)));
                DataTable listS = (DataTable)JsonConvert.DeserializeObject(str5, (typeof(DataTable)));
                DataTable listsecond = (DataTable)JsonConvert.DeserializeObject(str6, (typeof(DataTable)));
                int RequestID = Convert.ToInt32(Session["RequestID"]);
                hInputPara1.Add("@Request_ID", RequestID);
                DataTable dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRetailPropertyByReqID", hInputPara1);
                DataTable dtprojectid = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprjIDByRequestID", hInputPara1);

                for (int i = 0; i < list1.Rows.Count; i++)
                {

                    hInputPara.Add("@RequestID", RequestID);

                    hInputPara.Add("@PlotBunglowNumber", Convert.ToString(list1.Rows[i]["PlotNumber"]));
                    hInputPara.Add("@BunglowNumber", Convert.ToString(list1.Rows[i]["BunglowNumber"]));
                    //if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    //{
                    //    hInputPara.Add("@Survey_Number", Convert.ToString(list1.Rows[i]["SurveyNumberSingle"]));
                    //}
                    //else
                    //{
                    //    hInputPara.Add("@Survey_Number", Convert.ToString(list1.Rows[i]["SurveyNumber"]));
                    //}

                    hInputPara.Add("@UnitNumber", Convert.ToString(list1.Rows[i]["UnitNumber"]));
                    hInputPara.Add("@Project_Name", Convert.ToString(list1.Rows[i]["ProjectName"]));
                    hInputPara.Add("@Building_Name_RI", Convert.ToString(list1.Rows[i]["BuildingName"]));
                    hInputPara.Add("@Wing_Name_RI", Convert.ToString(list1.Rows[i]["WingName"]));
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@Village_Name", Convert.ToString(list1.Rows[i]["VillageNameSingle"]));
                    }
                    else
                    {
                        hInputPara.Add("@Village_Name", Convert.ToString(list1.Rows[i]["VillageName"]));
                    }
                    //if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    //{
                    //    hInputPara.Add("@Street", Convert.ToString(list1.Rows[i]["StreetSingle"]));
                    //}
                    //else
                    //{
                    //    hInputPara.Add("@Street", Convert.ToString(list1.Rows[i]["Street"]));
                    //}
                    //if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    //{
                    //    hInputPara.Add("@Locality", Convert.ToString(list1.Rows[i]["LocalitySingle"]));
                    //}
                    //else
                    //{
                    //    hInputPara.Add("@Locality", Convert.ToString(list1.Rows[i]["Locality"]));
                    //}
                    //if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    //{
                    //    hInputPara.Add("@Sub_Locality", Convert.ToString(list1.Rows[i]["SubLocalitySingle"]));
                    //}
                    //else
                    //{
                    //    hInputPara.Add("@Sub_Locality", Convert.ToString(list1.Rows[i]["SubLocality"]));
                    //}
                    //if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    //{
                    //    hInputPara.Add("@City", Convert.ToString(list1.Rows[i]["CitySingle"]));
                    //}
                    //else
                    //{
                    //    hInputPara.Add("@City", Convert.ToString(list1.Rows[i]["City"]));
                    //}
                    //if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    //{
                    //    hInputPara.Add("@District", Convert.ToString(list1.Rows[i]["DistrictSingle"]));
                    //}
                    //else
                    //{
                    //    hInputPara.Add("@District", Convert.ToString(list1.Rows[i]["District"]));
                    //}
                    //if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    //{
                    //    hInputPara.Add("@State", Convert.ToString(list1.Rows[i]["StateSingle"]));
                    //}
                    //else
                    //{
                    //    hInputPara.Add("@State", Convert.ToString(list1.Rows[i]["State"]));
                    //}
                    //if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    //{
                    //    hInputPara.Add("@PinCode", Convert.ToString(list1.Rows[i]["PincodeSingle"]));
                    //}
                    //else
                    //{
                    //    hInputPara.Add("@PinCode", Convert.ToString(list1.Rows[i]["Pincode"]));
                    //}


                    sqlDataAccess.ExecuteStoreProcedure("usp_UpdatePropertyDetailsNew", hInputPara);

                }


                return Json(new { success = true, Message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public List<ival_DocumentList> GetDocuments()
        {
            List<ival_DocumentList> listdoc = new List<ival_DocumentList>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadDocLists");
            for (int i = 0; i < dtdoc.Rows.Count; i++)
            {
                listdoc.Add(new ival_DocumentList
                {
                    Document_ID = Convert.ToInt32(dtdoc.Rows[i]["Document_ID"]),
                    Document_Name = Convert.ToString(dtdoc.Rows[i]["Document_Name"]),

                });

            }
            return listdoc;
        }

        [HttpPost]
        public List<ival_empList> GetEmp()
        {
            List<ival_empList> listdoc = new List<ival_empList>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetEmp");
            for (int i = 0; i < dtdoc.Rows.Count; i++)
            {
                listdoc.Add(new ival_empList
                {
                    Emp_ID = Convert.ToInt32(dtdoc.Rows[i]["Employee_ID"]),
                    Emp_Name = Convert.ToString(dtdoc.Rows[i]["fullName"]),

                });

            }
            return listdoc;
        }
        //Changes by punam
        //[HttpPost]
        //public List<iva_UploadDocumentListNew> GetDocumentsEdit()
        //{
        //    List<iva_UploadDocumentListNew> listdoc = new List<iva_UploadDocumentListNew>();
        //    hInputPara = new Hashtable();
        //    sqlDataAccess = new SQLDataAccess();
        //    DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadDocListsNew");
        //    for (int i = 0; i < dtdoc.Rows.Count; i++)
        //    {
        //        listdoc.Add(new iva_UploadDocumentListNew
        //        {
        //            Document_ID = Convert.ToInt32(dtdoc.Rows[i]["Document_ID"]),
        //            Document_Name = Convert.ToString(dtdoc.Rows[i]["Document_Name"]),
        //            SrNo = Convert.ToInt32(dtdoc.Rows[i]["SrNo"]),

        //        });

        //    }
        //    return listdoc;
        //}
        //changes by punam

        [HttpPost]
        public List<ival_DocumentList> GetUploadedDocName(DataTable list2)
        {
            List<ival_DocumentList> listdoc = new List<ival_DocumentList>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            for (int i = 0; i < list2.Rows.Count; i++)
            {
                string a = list2.Rows[i]["value3"].ToString();

                hInputPara.Add("@DocumentName", list2.Rows[i]["value3"]);
                DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDocumentIdNew", hInputPara);
                for (int j = 0; j < dtdoc.Rows.Count; j++)
                {
                    listdoc.Add(new ival_DocumentList
                    {
                        Document_ID = Convert.ToInt32(dtdoc.Rows[j]["Document_ID"]),
                        // Document_Name = Convert.ToString(dtdoc.Rows[i]["Document_Name"]),

                    });

                }
                hInputPara.Clear();
            }
            return listdoc;
        }
        [HttpPost]
        public List<ival_Document_Uploaded_List> GetFilesList(string RequestID)
        {

            List<ival_Document_Uploaded_List> listfiles = new List<ival_Document_Uploaded_List>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Request_Id", RequestID);
            DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadFilesLists", hInputPara);
            for (int i = 0; i < dtdoc.Rows.Count; i++)
            {
                ival_Document_Uploaded_List obj = new ival_Document_Uploaded_List();

                //listfiles.Add(new ival_Document_Uploaded_List
                //{
                if ((dtdoc.Rows[i]["Docupload_ID"]) != DBNull.Value)
                {
                    obj.Docupload_ID = Convert.ToInt32(dtdoc.Rows[i]["Docupload_ID"]);
                }
                obj.Document_ID = Convert.ToInt32(dtdoc.Rows[i]["Document_ID"]);
                obj.Document_Name = Convert.ToString(dtdoc.Rows[i]["Document_Name"]);
                if ((dtdoc.Rows[i]["Uploaded_Document"]) != DBNull.Value)
                {
                    obj.Uploaded_Document = Convert.ToString(dtdoc.Rows[i]["Uploaded_Document"]);
                }
                if ((dtdoc.Rows[i]["Approval_Authority"]) != DBNull.Value)
                {
                    obj.Approval_Authority = Convert.ToString(dtdoc.Rows[i]["Approval_Authority"]);
                }
                if ((dtdoc.Rows[i]["Date_Of_Approval"]) != DBNull.Value)
                {

                    obj.Date_Of_ApprovalNew = Convert.ToString(dtdoc.Rows[i]["Date_Of_Approval"]);

                }
                if ((dtdoc.Rows[i]["Approval_Number"]) != DBNull.Value)
                {
                    obj.Approval_Number = Convert.ToString(dtdoc.Rows[i]["Approval_Number"]);
                }
                listfiles.Add(obj);
                //});

            }
            return listfiles;
        }
        //changes by punam
        //[HttpPost]
        //public List<iva_UploadDocumentListNew> GetUploadedFilesListNew(string RequestID)
        //{

        //    //List<ival_Document_Uploaded_List> listfiles = new List<ival_Document_Uploaded_List>();
        //    List<iva_UploadDocumentListNew> listfiles = new List<iva_UploadDocumentListNew>();
        //    hInputPara = new Hashtable();
        //    sqlDataAccess = new SQLDataAccess();
        //    hInputPara.Add("@RequestId", RequestID);
        //    DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadedListNew", hInputPara);
        //    for (int i = 0; i < dtdoc.Rows.Count; i++)
        //    {
        //        //ival_Document_Uploaded_List obj = new ival_Document_Uploaded_List();
        //        iva_UploadDocumentListNew obj = new iva_UploadDocumentListNew();
        //        //listfiles.Add(new ival_Document_Uploaded_List
        //        //{
        //        if ((dtdoc.Rows[i]["Docupload_ID"]) != DBNull.Value)
        //        {
        //            obj.Docupload_ID = Convert.ToInt32(dtdoc.Rows[i]["Docupload_ID"]);
        //        }
        //        obj.Document_ID = Convert.ToInt32(dtdoc.Rows[i]["Document_ID"]);
        //        obj.SrNo = Convert.ToInt32(dtdoc.Rows[i]["SrNo"]);
        //        obj.Document_Name = Convert.ToString(dtdoc.Rows[i]["Document_Name"]);
        //        if ((dtdoc.Rows[i]["Uploaded_Document"]) != DBNull.Value)
        //        {
        //            string uploaddoc = Convert.ToString(dtdoc.Rows[i]["Uploaded_Document"]);
        //            string value = uploaddoc;
        //            if (value != "")
        //            {
        //                //string[] newValue = value.Split('\\');
        //                //if (newValue != null)
        //                //{

        //                string fileName = value;

        //                obj.FileName = fileName;
        //                var filePath = Directory.GetFiles(Server.MapPath("~/UploadFiles"));
        //                foreach (string filename in filePath)
        //                {
        //                    if (filename.Contains(fileName))
        //                    {
        //                        var Get = filename;
        //                        obj.Uploaded_Document = Get;
        //                    }
        //                }
        //                // }
        //            }

        //            //var filePath = Directory.GetFiles(@"D:\Punam Gat\04092020BackUp_IValue\ivalue\ivalue_codefirst\I_Value.WebApp\IValue\UploadFiles\GS99999101_Report (1).pdf");


        //        }
        //        if ((dtdoc.Rows[i]["Approval_Authority"]) != DBNull.Value)
        //        {
        //            obj.Approval_Authority = Convert.ToString(dtdoc.Rows[i]["Approval_Authority"]);
        //        }
        //        if ((dtdoc.Rows[i]["Date_Of_Approval"]) != DBNull.Value)
        //        {
        //            // var format = "dd/MM/yyyy";
        //            // var date= dtdoc.Rows[i]["Date_Of_Approval"].ToString();
        //            //var dateofapp= DateTime.ParseExact(date,format, CultureInfo.InvariantCulture);
        //            // obj.Date_Of_Approval = dateofapp;
        //            var a = Convert.ToDateTime(dtdoc.Rows[i]["Date_Of_Approval"]).Date;
        //            var b = a.ToString("dd/MM/yyyy");
        //            obj.Date_Of_Approval_New = b;
        //            // obj.Date_Of_Approval = Convert.ToDateTime(dtdoc.Rows[i]["Date_Of_Approval"]).Date;
        //            //obj.Date_Of_Approval =Convert.ToDateTime(b);
        //        }
        //        if ((dtdoc.Rows[i]["Approval_Number"]) != DBNull.Value)
        //        {
        //            obj.Approval_Number = Convert.ToString(dtdoc.Rows[i]["Approval_Number"]);
        //        }
        //        listfiles.Add(obj);
        //        //});

        //    }
        //    return listfiles;
        //}
        [HttpPost]
        public JsonResult GetDocumentsold()
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_DocumentList> listdoc = new List<ival_DocumentList>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadDocLists");
            for (int i = 0; i < dtdoc.Rows.Count; i++)
            {
                listdoc.Add(new ival_DocumentList
                {
                    Document_ID = Convert.ToInt32(dtdoc.Rows[i]["Document_ID"]),
                    Document_Name = Convert.ToString(dtdoc.Rows[i]["Document_Name"]),








                });

            }
            ViewBag.unitdetails = listdoc;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(listdoc);
            return Json(jsonProjectBuildingMaster);
        }

        [HttpPost]
        public JsonResult UploadPDFFiles(string DocId)
        {
            string strfilename = "";
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@UserID", Convert.ToString(Session["UserName"]));
            DataTable dtREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1RequestID", hInputPara);
            hInputPara.Clear();

            string id = Convert.ToString(dtREQ.Rows[0]["Request_ID"]);
            Session["Request_ID"] = id;
            string a = DocId.PadLeft(2, '0');
            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                                                            //Use the following properties to get file's name, size and MIMEType
                int fileSize = file.ContentLength;
                string fileName = file.FileName;
                string mimeType = file.ContentType;
                System.IO.Stream fileContent = file.InputStream;
                //string path = "~/UploadFiles/" + fileName;
                //file.SaveAs(Server.MapPath("~/UploadFiles/"+ fileName));
                //To save file, use SaveAs method
                //changes by punam
                var Request_ID = Session["Request_ID"];
                var pdffile = Request_ID + "_" + a + "_" + fileName;

                file.SaveAs(Server.MapPath("../UploadFiles/") + pdffile); //File will be saved in application root
                Session["File"] = pdffile;
                TempData["File"] = fileName;
                strfilename = fileName;
            }
            // return strfilename;
            return Json("Uploaded " + Request.Files.Count + " files");
        }
        //changes by punam
        [HttpPost]
        public JsonResult UploadPDFFilesEdit(string DocId)
        {
            string a = DocId.PadLeft(2, '0');
            string ReqID = Session["RequestID"].ToString();
            //string file=ReqID+

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                                                            //Use the following properties to get file's name, size and MIMEType
                int fileSize = file.ContentLength;
                string fileName = file.FileName;
                string mimeType = file.ContentType;
                // System.IO.Stream fileContent = file.InputStream;

                String FileSave = ReqID + "_" + a + "_" + fileName;

                var getFile = Directory.GetFiles(Server.MapPath("~/UploadFiles"));
                //var b = VirtualPathUtility.ToAbsolute("~/UploadFiles/" + fileName);
                foreach (string filename in getFile)
                {
                    if (filename.Contains(FileSave))
                    {
                        var Get = filename;
                        if (System.IO.File.Exists(filename))
                        {
                            System.IO.File.Delete(filename);
                            //Console.WriteLine("file Deleted");
                            file.SaveAs(Server.MapPath("../UploadFiles/") + FileSave);

                        }
                    }
                    else
                    {

                        file.SaveAs(Server.MapPath("../UploadFiles/") + FileSave);

                        Dispose();
                    }

                }

                Session["FileEdit"] = FileSave;
                TempData["TempFileEdit"] = FileSave;
                //file.SaveAs(Server.MapPath("../UploadFiles/") + FileSave); //File will be saved in application root
                // Session["File"] = fileName;
            }

            return Json("Uploaded " + Request.Files.Count + " files");
        }

        public FileResult ShowDocument(string FilePath)
        {
            FilePath = Session["File"].ToString();
            return File(Server.MapPath("~/UploadFiles/") + FilePath, GetMimeType(FilePath));
        }
        private string GetMimeType(string fileName)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }

        [HttpPost]
        public ActionResult SaveNewDoc(string str)
        {
            try
            {

                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                for (int i = 0; i < list1.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Document_Name", Convert.ToString(list1.Rows[i]["docname"]));
                    sqlDataAccess.ExecuteStoreProcedure("usp_InsertDocMaster", hInputPara);

                }
                return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult UploadFiles(string str, string str1)
        {
            try
            {
                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                DataTable list2 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));
                Session["ApprovalDetails"] = list2;
                //for (int i = 0; i < list2.Rows.Count; i++)
                //{
                //    if( (list2.Rows[i]["apprnum"].ToString())!=String.Empty && (list2.Rows[i]["apprnum"].ToString())!=null)
                //    {
                //        String apprnum = list2.Rows[i]["apprnum"].ToString();
                //        String approvingauth = list2.Rows[i]["approvingauth"].ToString();
                //    }
                //}
                hInputPara = new Hashtable();
                hInputPara1 = new Hashtable();
                sqlDataAccess1 = new SQLDataAccess();

                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@UserID", Convert.ToString(Session["UserName"]));
                DataTable dtREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1RequestID", hInputPara);
                hInputPara.Clear();

                //string CustomerId = Convert.ToString(list1.Rows[0]["CustomerId"]);
                //hInputPara1.Add("Customer_Application_ID", CustomerId);
                //  DataTable getreqid = sqlDataAccess1.GetDatatableExecuteStoreProcedure("usp_GetRequestIDByRequestorname", hInputPara1);
                string id = Convert.ToString(dtREQ.Rows[0]["Request_ID"]);
                Session["Request_ID"] = id;
                if (list1.Rows.Count > 0)
                {
                    for (int i = 0; i < list1.Rows.Count; i++)
                    {
                        string fileName = "";
                        //get file name for adding prefix of request and documentId
                        string file = list1.Rows[i]["filename"].ToString();
                        //string oldpath ="E:/Projects/ivalue_codefirst/I_Value.WebApp/IValue/UploadFiles/85_01_GS99999101_Report.pdf";
                        //string newPathAndName = "E:/Projects/ivalue_codefirst/I_Value.WebApp/IValue/UploadFiles/350_01_GS99999101_Report.pdf";
                        //System.IO.File.Copy(oldpath, newPathAndName);
                        if (file != "")
                        {
                            string[] newValue = file.Split('\\');
                            if (newValue != null)
                            {
                                string docId = list1.Rows[i]["docid"].ToString();
                                string DocID = docId.PadLeft(2, '0');
                                string Name = newValue[newValue.Length - 1];
                                fileName = id + "_" + DocID + "_" + Name;
                            }
                        }



                        if ((list1.Rows[i]["apprnum"].ToString()) != String.Empty && (list1.Rows[i]["apprnum"].ToString()) != null)
                        {
                            int docId = Convert.ToInt32(list1.Rows[i]["docid"]);
                            hInputPara.Add("@Document_ID_New", Convert.ToInt32(list1.Rows[i]["docid"]));
                            hInputPara.Add("@RequestID_New", Convert.ToInt32(id));
                            string result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertNewUploadDocument", hInputPara);
                            if (result == "document already exist")
                            {
                                if (list1.Rows[i]["filename"].ToString() != string.Empty && (list1.Rows[i]["filename"].ToString()) != null)
                                {
                                    hInputPara1.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]));
                                    hInputPara1.Add("@RequestID", Convert.ToInt32(id));

                                    //hInputPara1.Add("@Document_Name", Convert.ToString(list1.Rows[i]["filename"]));

                                    hInputPara1.Add("@Document_Name", fileName);

                                    hInputPara1.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));


                                    //hInputPara.Add("@Date_Of_Approval", Convert.ToDateTime(list1.Rows[i]["dateofapp"].ToString()));
                                    var dateString = list1.Rows[i]["dateofapp"].ToString();
                                    var format = "dd/MM/yyyy";
                                    if (dateString != "")
                                    {
                                        var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                        hInputPara1.Add("@Date_Of_Approval", dateTime);
                                    }


                                    //  hInputPara.Add("@Date_Of_Approval", DateTime.Parse(list1.Rows[i]["dateofapp"].ToString()));
                                    hInputPara1.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));
                                    sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_UpdateNewUploadDocument", hInputPara1);
                                }
                                else
                                {
                                    hInputPara1.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]));
                                    hInputPara1.Add("@RequestID", Convert.ToInt32(id));

                                    hInputPara1.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));

                                    var dateString = list1.Rows[i]["dateofapp"].ToString();
                                    var format = "dd/MM/yyyy";
                                    if (dateString != "")
                                    {
                                        var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                        hInputPara1.Add("@Date_Of_Approval", dateTime);
                                    }

                                    hInputPara1.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));
                                    sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_UpdateBlankDocument", hInputPara1);
                                }
                            }
                            else
                            {

                                hInputPara1.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]));

                                hInputPara1.Add("@Document_Name", fileName);
                                hInputPara1.Add("@RequestID", Convert.ToInt32(id));

                                hInputPara1.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));
                                try
                                {
                                    string dateString = list1.Rows[i]["dateofapp"].ToString();
                                    if (dateString != "")
                                    {

                                        string datestring1 = Convert.ToDateTime(dateString).ToString("dd/MM/yyyy");
                                        var format = "dd/MM/yyyy";
                                        var dateTime = DateTime.ParseExact(datestring1, format, CultureInfo.InvariantCulture);
                                        hInputPara1.Add("@Date_Of_Approval", dateTime);
                                    }
                                }
                                catch (Exception e)

                                {

                                    var dateString = list1.Rows[i]["dateofapp"].ToString();
                                    var format = "dd/MM/yyyy";
                                    if (dateString != "")
                                    {
                                        var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                        hInputPara1.Add("@Date_Of_Approval", dateTime);
                                    }

                                }
                                //var dateString = list1.Rows[i]["dateofapp"].ToString();

                                //var format = "dd/MM/yyyy";
                                //if (dateString != "")
                                //{
                                //    var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                //    hInputPara1.Add("@Date_Of_Approval", dateTime);
                                //}


                                //  hInputPara.Add("@Date_Of_Approval", DateTime.Parse(list1.Rows[i]["dateofapp"].ToString()));
                                hInputPara1.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));
                                sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_InsertUploadDocMaster", hInputPara1);
                                //sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_InsertNewUploadDocument", hInputPara1);
                            }

                        }

                        hInputPara.Clear();
                        hInputPara1.Clear();
                        if ((list1.Rows[i]["docname"].ToString()) != null && (list1.Rows[i]["docname"].ToString()) != string.Empty)
                        {
                            hInputPara1.Add("@Document_Name", Convert.ToString(list1.Rows[i]["docname"]));
                            //  hInputPara1.Add("@Document_ID", Convert.ToString(list1.Rows[i]["docid"]));
                            sqlDataAccess.ExecuteStoreProcedure("usp_InsertDocMasterNew", hInputPara1);
                        }
                        hInputPara1.Clear();
                    }
                    //for (int i = 0; i < list1.Rows.Count; i++)
                    //{
                    //    //    if (list1.Rows[i]["filename"] == DBNull.Value)
                    //    //        list1.Rows[i].Delete();

                    //    //list1.AcceptChanges();
                    //    if ((list2.Rows[i]["apprnum"].ToString()) != String.Empty && (list2.Rows[i]["apprnum"].ToString()) != null)
                    //    {
                    //        //hInputPara.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]).ToString().Replace(@"\", string.Empty));
                    //        //hInputPara.Add("@Document_Name", Convert.ToString(list1.Rows[i]["filename"]).Replace(@"\", string.Empty));
                    //        hInputPara.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]).ToString());
                    //        hInputPara.Add("@Document_Name", Convert.ToString(list1.Rows[i]["filename"]));

                    //        hInputPara.Add("@RequestID", Convert.ToInt32(id));
                    //        hInputPara.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));
                    //        //hInputPara.Add("@Date_Of_Approval", Convert.ToDateTime(list1.Rows[i]["dateofapp"].ToString()));
                    //        var dateString = list1.Rows[i]["dateofapp"].ToString().Replace(@"yyyy/MM/dd", "dd/MM/yyyy");
                    //        var format = "dd/MM/yyyy";
                    //        var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                    //        hInputPara.Add("@Date_Of_Approval", dateTime);

                    //        //  hInputPara.Add("@Date_Of_Approval", DateTime.Parse(list1.Rows[i]["dateofapp"].ToString()));
                    //        hInputPara.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));

                    //        sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_InsertUploadDocMaster", hInputPara);
                    //    }
                    //    if ((list1.Rows[i]["valuedocname"].ToString()) != null && (list1.Rows[i]["valuedocname"].ToString()) != string.Empty)
                    //    {
                    //        hInputPara1.Add("@Document_Name", Convert.ToString(list1.Rows[i]["valuedocname"]).Replace(@"\", string.Empty));
                    //        hInputPara1.Add("@Document_ID", Convert.ToString(list1.Rows[i]["docid"]).Replace(@"\", string.Empty));
                    //        sqlDataAccess.ExecuteStoreProcedure("usp_InsertDocMaster", hInputPara1);
                    //    }
                    //    hInputPara.Clear();
                    //    hInputPara1.Clear();

                    //}
                    return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new { success = true, Message = "NO records found" }, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPost]
        public ActionResult SubUploadFiles(string str, string str1)
        {
            try
            {
                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                DataTable list2 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));
                Session["ApprovalDetails"] = list2;
                //for (int i = 0; i < list2.Rows.Count; i++)
                //{
                //    if( (list2.Rows[i]["apprnum"].ToString())!=String.Empty && (list2.Rows[i]["apprnum"].ToString())!=null)
                //    {
                //        String apprnum = list2.Rows[i]["apprnum"].ToString();
                //        String approvingauth = list2.Rows[i]["approvingauth"].ToString();
                //    }
                //}
                hInputPara = new Hashtable();
                hInputPara1 = new Hashtable();
                sqlDataAccess1 = new SQLDataAccess();

                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@UserID", Convert.ToString(Session["UserName"]));
                DataTable dtREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1RequestID", hInputPara);
                hInputPara.Clear();
                string id = Convert.ToString(dtREQ.Rows[0]["Request_ID"]);
                Session["Request_ID"] = id;
                if (list1.Rows.Count > 0)
                {
                    for (int i = 0; i < list1.Rows.Count; i++)
                    {
                        string fileName = "";
                        //get file name for adding prefix of request and documentId
                        string file = list1.Rows[i]["filename"].ToString();


                        if (file != "")
                        {
                            string[] newValue = file.Split('_');
                            if (newValue != null)
                            {
                                string docId = list1.Rows[i]["docid"].ToString();
                                string DocID = docId.PadLeft(2, '0');
                                string Name1 = newValue[newValue.Length - 1];
                                string Name = newValue[newValue.Length - 2];
                                fileName = id + "_" + DocID + "_" + Name1;
                            }
                        }

                        //string oldpath = "E:/Projects/ivalue_codefirst/I_Value.WebApp/IValue/UploadFiles/" + file;
                        //string newPathAndName = "E:/Projects/ivalue_codefirst/I_Value.WebApp/IValue/UploadFiles/" + fileName;
                        string oldpath = "http://brainlines.co.in/IvalueDEV/" + file;
                        string newPathAndName = "http://brainlines.co.in/IvalueDEV/" + fileName;
                        System.IO.File.Copy(oldpath, newPathAndName);

                        // var folderPath = Server.MapPath("../UploadFiles");
                        //string oldpath1 = folderPath + file;
                        //string newPathAndName1 = folderPath + fileName;
                        //   System.IO.File.Copy(oldpath1, newPathAndName1);
                        if ((list1.Rows[i]["apprnum"].ToString()) != String.Empty && (list1.Rows[i]["apprnum"].ToString()) != null)
                        {
                            int docId = Convert.ToInt32(list1.Rows[i]["docid"]);
                            hInputPara.Add("@Document_ID_New", Convert.ToInt32(list1.Rows[i]["docid"]));
                            hInputPara.Add("@RequestID_New", Convert.ToInt32(id));
                            string result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertNewUploadDocument", hInputPara);
                            if (result == "document already exist")
                            {
                                if (list1.Rows[i]["filename"].ToString() != string.Empty && (list1.Rows[i]["filename"].ToString()) != null)
                                {
                                    hInputPara1.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]));
                                    hInputPara1.Add("@RequestID", Convert.ToInt32(id));

                                    //hInputPara1.Add("@Document_Name", Convert.ToString(list1.Rows[i]["filename"]));

                                    hInputPara1.Add("@Document_Name", fileName);

                                    hInputPara1.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));


                                    //hInputPara.Add("@Date_Of_Approval", Convert.ToDateTime(list1.Rows[i]["dateofapp"].ToString()));
                                    var dateString = list1.Rows[i]["dateofapp"].ToString();
                                    var format = "dd/MM/yyyy";
                                    if (dateString != "")
                                    {
                                        var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                        hInputPara1.Add("@Date_Of_Approval", dateTime);
                                    }


                                    //  hInputPara.Add("@Date_Of_Approval", DateTime.Parse(list1.Rows[i]["dateofapp"].ToString()));
                                    hInputPara1.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));
                                    sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_UpdateNewUploadDocument", hInputPara1);
                                }
                                else
                                {
                                    hInputPara1.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]));
                                    hInputPara1.Add("@RequestID", Convert.ToInt32(id));

                                    hInputPara1.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));

                                    var dateString = list1.Rows[i]["dateofapp"].ToString();
                                    var format = "dd/MM/yyyy";
                                    if (dateString != "")
                                    {
                                        var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                        hInputPara1.Add("@Date_Of_Approval", dateTime);
                                    }

                                    hInputPara1.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));
                                    sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_UpdateBlankDocument", hInputPara1);
                                }
                            }
                            else
                            {

                                hInputPara1.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]));

                                hInputPara1.Add("@Document_Name", fileName);
                                hInputPara1.Add("@RequestID", Convert.ToInt32(id));

                                hInputPara1.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));

                                var dateString = list1.Rows[i]["dateofapp"].ToString();
                                //var format = "dd/MM/yyyy";
                                //if (dateString != "")
                                //{
                                //    //var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                //    hInputPara1.Add("@Date_Of_Approval", dateString);

                                //}

                                var format = "dd/MM/yyyy";
                                if (dateString != "")
                                {
                                    var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                    hInputPara1.Add("@Date_Of_Approval", dateTime);
                                }


                                //  hInputPara.Add("@Date_Of_Approval", DateTime.Parse(list1.Rows[i]["dateofapp"].ToString()));
                                hInputPara1.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));
                                sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_InsertUploadDocMaster", hInputPara1);
                                //sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_InsertNewUploadDocument", hInputPara1);
                            }

                        }

                        hInputPara.Clear();
                        hInputPara1.Clear();
                        if ((list1.Rows[i]["docname"].ToString()) != null && (list1.Rows[i]["docname"].ToString()) != string.Empty)
                        {
                            hInputPara1.Add("@Document_Name", Convert.ToString(list1.Rows[i]["docname"]));
                            //  hInputPara1.Add("@Document_ID", Convert.ToString(list1.Rows[i]["docid"]));
                            sqlDataAccess.ExecuteStoreProcedure("usp_InsertDocMasterNew", hInputPara1);
                        }
                        hInputPara1.Clear();
                    }
                    //for (int i = 0; i < list1.Rows.Count; i++)
                    //{
                    //    //    if (list1.Rows[i]["filename"] == DBNull.Value)
                    //    //        list1.Rows[i].Delete();

                    //    //list1.AcceptChanges();
                    //    if ((list2.Rows[i]["apprnum"].ToString()) != String.Empty && (list2.Rows[i]["apprnum"].ToString()) != null)
                    //    {
                    //        //hInputPara.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]).ToString().Replace(@"\", string.Empty));
                    //        //hInputPara.Add("@Document_Name", Convert.ToString(list1.Rows[i]["filename"]).Replace(@"\", string.Empty));
                    //        hInputPara.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]).ToString());
                    //        hInputPara.Add("@Document_Name", Convert.ToString(list1.Rows[i]["filename"]));

                    //        hInputPara.Add("@RequestID", Convert.ToInt32(id));
                    //        hInputPara.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));
                    //        //hInputPara.Add("@Date_Of_Approval", Convert.ToDateTime(list1.Rows[i]["dateofapp"].ToString()));
                    //        var dateString = list1.Rows[i]["dateofapp"].ToString().Replace(@"yyyy/MM/dd", "dd/MM/yyyy");
                    //        var format = "dd/MM/yyyy";
                    //        var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                    //        hInputPara.Add("@Date_Of_Approval", dateTime);

                    //        //  hInputPara.Add("@Date_Of_Approval", DateTime.Parse(list1.Rows[i]["dateofapp"].ToString()));
                    //        hInputPara.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));

                    //        sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_InsertUploadDocMaster", hInputPara);
                    //    }
                    //    if ((list1.Rows[i]["valuedocname"].ToString()) != null && (list1.Rows[i]["valuedocname"].ToString()) != string.Empty)
                    //    {
                    //        hInputPara1.Add("@Document_Name", Convert.ToString(list1.Rows[i]["valuedocname"]).Replace(@"\", string.Empty));
                    //        hInputPara1.Add("@Document_ID", Convert.ToString(list1.Rows[i]["docid"]).Replace(@"\", string.Empty));
                    //        sqlDataAccess.ExecuteStoreProcedure("usp_InsertDocMaster", hInputPara1);
                    //    }
                    //    hInputPara.Clear();
                    //    hInputPara1.Clear();

                    //}
                    return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new { success = true, Message = "NO records found" }, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        //changes by punam
        [HttpPost]
        public ActionResult UpdateUploadFiles(string str, string str1)
        {
            try
            {

                //changes by punam
                DataTable list1 = new DataTable();
                DataTable list2 = new DataTable();
                //int b = str[0];
                if (str != null)
                {
                    list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                }
                if (str1 != null)
                {
                    list2 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));

                }

                hInputPara = new Hashtable();
                hInputPara1 = new Hashtable();

                sqlDataAccess1 = new SQLDataAccess();

                sqlDataAccess = new SQLDataAccess();
                int id = Convert.ToInt32(Session["RequestID"]);


                if (list1.Rows.Count > 0)
                {

                    for (int i = 0; i < list1.Rows.Count; i++)
                    {
                        string fileName = "";
                        //get file name for adding prefix of request and documentId
                        string file = list1.Rows[i]["filename"].ToString();
                        if (file != "")
                        {
                            string[] newValue = file.Split('\\');
                            if (newValue != null)
                            {
                                string docId = list1.Rows[i]["docid"].ToString();
                                string DocID = docId.PadLeft(2, '0');
                                string Name = newValue[newValue.Length - 1];
                                fileName = id + "_" + DocID + "_" + Name;
                            }
                        }



                        if ((list1.Rows[i]["apprnum"].ToString()) != String.Empty && (list1.Rows[i]["apprnum"].ToString()) != null)
                        {
                            int docId = Convert.ToInt32(list1.Rows[i]["docid"]);
                            hInputPara.Add("@Document_ID_New", Convert.ToInt32(list1.Rows[i]["docid"]));
                            hInputPara.Add("@RequestID_New", Convert.ToInt32(id));
                            string result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertNewUploadDocument", hInputPara);
                            if (result == "document already exist")
                            {
                                sqlDataAccess = new SQLDataAccess();

                                if (list1.Rows[i]["filename"].ToString() != string.Empty && (list1.Rows[i]["filename"].ToString()) != null)
                                {
                                    hInputPara1.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]));
                                    hInputPara1.Add("@RequestID", Convert.ToInt32(id));

                                    //hInputPara1.Add("@Document_Name", Convert.ToString(list1.Rows[i]["filename"]));

                                    hInputPara1.Add("@Document_Name", fileName);

                                    hInputPara1.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));


                                    //hInputPara.Add("@Date_Of_Approval", Convert.ToDateTime(list1.Rows[i]["dateofapp"].ToString()));
                                    string dateString = list1.Rows[i]["dateofapp"].ToString();
                                    //DateTime date = Convert.ToDateTime(dateString);
                                    //string dateString1 = date.ToString("dd/MM/yyyy");
                                    var format = "dd/MM/yyyy";
                                    if (dateString != "")
                                    {
                                        var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                        hInputPara1.Add("@Date_Of_Approval", dateTime);
                                    }


                                    //  hInputPara.Add("@Date_Of_Approval", DateTime.Parse(list1.Rows[i]["dateofapp"].ToString()));
                                    hInputPara1.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));
                                    sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_UpdateNewUploadDocument", hInputPara1);
                                }
                                else
                                {

                                    hInputPara1.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]));
                                    hInputPara1.Add("@RequestID", Convert.ToInt32(id));

                                    hInputPara1.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));

                                    var dateString = list1.Rows[i]["dateofapp"].ToString();
                                    var format = "dd/MM/yyyy";
                                    if (dateString != "")
                                    {
                                        var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                        hInputPara1.Add("@Date_Of_Approval", dateTime);
                                    }

                                    hInputPara1.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));
                                    sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_UpdateBlankDocument", hInputPara1);
                                }
                            }
                            else
                            {
                                sqlDataAccess = new SQLDataAccess();
                                hInputPara1.Add("@Document_ID", Convert.ToInt32(list1.Rows[i]["docid"]));

                                hInputPara1.Add("@Document_Name", fileName);
                                hInputPara1.Add("@RequestID", Convert.ToInt32(id));

                                hInputPara1.Add("@Approval_Authority", Convert.ToString(list1.Rows[i]["approvingauth"]));
                                try
                                {
                                    string dateString = list1.Rows[i]["dateofapp"].ToString();
                                    if (dateString != "")
                                    {

                                        string datestring1 = Convert.ToDateTime(dateString).ToString("dd/MM/yyyy");
                                        var format = "dd/MM/yyyy";
                                        var dateTime = DateTime.ParseExact(datestring1, format, CultureInfo.InvariantCulture);
                                        hInputPara1.Add("@Date_Of_Approval", dateTime);
                                    }
                                }
                                catch (Exception e)

                                {

                                    var dateString = list1.Rows[i]["dateofapp"].ToString();
                                    var format = "dd/MM/yyyy";
                                    if (dateString != "")
                                    {
                                        var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                                        hInputPara1.Add("@Date_Of_Approval", dateTime);
                                    }

                                }

                                hInputPara1.Add("@Approval_Number", Convert.ToString(list1.Rows[i]["apprnum"]));
                                sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_InsertUploadDocMaster", hInputPara1);
                                //sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_InsertNewUploadDocument", hInputPara1);
                            }

                        }

                        hInputPara.Clear();
                        hInputPara1.Clear();
                        if ((list1.Rows[i]["docname"].ToString()) != null && (list1.Rows[i]["docname"].ToString()) != string.Empty)
                        {
                            sqlDataAccess = new SQLDataAccess();
                            hInputPara1.Add("@Document_Name", Convert.ToString(list1.Rows[i]["docname"]));
                            //  hInputPara1.Add("@Document_ID", Convert.ToString(list1.Rows[i]["docid"]));
                            sqlDataAccess.ExecuteStoreProcedure("usp_InsertDocMasterNew", hInputPara1);
                        }
                        hInputPara1.Clear();
                    }

                }
                //code for delete
                if (list2.Rows.Count > 0)
                {
                    sqlDataAccess = new SQLDataAccess();
                    var list = GetUploadedDocName(list2);
                    foreach (var c in list)
                    {
                        hInputPara1.Clear();
                        int documentId = c.Document_ID;
                        hInputPara1.Add("@DocumentID", documentId);
                        hInputPara1.Add("@RequestID", id);
                        sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_DeleteUploadedDocument", hInputPara1);

                    }

                }



                return Json(new { success = true, Message = "Insert,Update and Delete Successfully" }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                throw ex;
            }



        }


        [HttpPost]
        public JsonResult GetIndRetailorDetails(string Serchname)
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_RequestDetails_New> SearchLists = new List<ival_RequestDetails_New>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@strSearch", Serchname);
            DataTable dtsearch = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_SearchReuestorsdetails", hInputPara);
            for (int i = 0; i < dtsearch.Rows.Count; i++)
            {
                SearchLists.Add(new ival_RequestDetails_New
                {
                    Customer_Name = Convert.ToString(dtsearch.Rows[i]["Customer_Name"]),
                    Customer_Application_ID = Convert.ToString(dtsearch.Rows[i]["Customer_Application_ID"]),
                    Requestor_Name = Convert.ToString(dtsearch.Rows[i]["Requestor_Name"]),
                    Request_Date = Convert.ToString(dtsearch.Rows[i]["Request_Date"]),
                    Request_ID = Convert.ToInt32(dtsearch.Rows[i]["Request_ID"]),
                    Document_Holder_Name = Convert.ToString(dtsearch.Rows[i]["Document_Holder_Name"]),

                });
            }
            ViewBag.unitdetails = SearchLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(SearchLists);
            return Json(jsonProjectBuildingMaster);
        }

        [HttpPost]
        public JsonResult GetIndRetailorDetailsbank(string bankname, string custappid)
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_RequestDetails_New> SearchLists = new List<ival_RequestDetails_New>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@BankName", bankname);
            hInputPara.Add("@Customer_Application_ID", custappid);
            DataTable dtsearchbank = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_SearchReqestorsdetailBank", hInputPara);
            for (int i = 0; i < dtsearchbank.Rows.Count; i++)
            {
                SearchLists.Add(new ival_RequestDetails_New
                {
                    Valuation_Request_ID = Convert.ToString(dtsearchbank.Rows[i]["Valuation_Request_ID"]),
                    Customer_Name = Convert.ToString(dtsearchbank.Rows[i]["Customer_Name"]),
                    Customer_Application_ID = Convert.ToString(dtsearchbank.Rows[i]["Customer_Application_ID"]),
                    Requestor_Name = Convert.ToString(dtsearchbank.Rows[i]["Requestor_Name"]),
                    Request_Date = Convert.ToString(dtsearchbank.Rows[i]["Request_Date"]),
                    Request_ID = Convert.ToInt32(dtsearchbank.Rows[i]["Request_ID"]),
                    Document_Holder_Name = Convert.ToString(dtsearchbank.Rows[i]["Document_Holder_Name"]),


                });
            }
            ViewBag.unitdetails = SearchLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(SearchLists);
            return Json(jsonProjectBuildingMaster);
        }


        [HttpPost]
        public JsonResult GetIndRetailorDetailsByAppID(string Serchname)
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_RequestDetails_New> SearchLists = new List<ival_RequestDetails_New>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@strSearch", Serchname);
            DataTable dtsearch = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_SearchReuestorsdetailsByAppID", hInputPara);
            for (int i = 0; i < dtsearch.Rows.Count; i++)
            {
                SearchLists.Add(new ival_RequestDetails_New
                {
                    Customer_Name = Convert.ToString(dtsearch.Rows[i]["Customer_Name"]),
                    Customer_Application_ID = Convert.ToString(dtsearch.Rows[i]["Customer_Application_ID"]),
                    Requestor_Name = Convert.ToString(dtsearch.Rows[i]["Requestor_Name"]),
                    Request_Date = Convert.ToString(dtsearch.Rows[i]["Request_Date"]),
                    Request_ID = Convert.ToInt32(dtsearch.Rows[i]["Request_ID"]),
                    Document_Holder_Name = Convert.ToString(dtsearch.Rows[i]["Document_Holder_Name"]),

                });
            }
            ViewBag.unitdetails = SearchLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(SearchLists);
            return Json(jsonProjectBuildingMaster);
        }

        [HttpPost]
        public JsonResult GetRequestorDetailsByReqID(int Request_ID)
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_RequestDetails_New> REQLists = new List<ival_RequestDetails_New>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@RequestID", Request_ID);
            Session["SubRequest_ID"] = Request_ID;
            DataTable dtREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetReuestorsdetailsByReqID", hInputPara);
            TempData["data"] = dtREQ;
            for (int i = 0; i < dtREQ.Rows.Count; i++)
            {
                REQLists.Add(new ival_RequestDetails_New
                {
                    Customer_Name = Convert.ToString(dtREQ.Rows[i]["Customer_Name"]),
                    Customer_Application_ID = Convert.ToString(dtREQ.Rows[i]["Customer_Application_ID"]),
                    Email_ID = Convert.ToString(dtREQ.Rows[i]["Email_ID"]),
                    Customer_Contact_No_1 = Convert.ToString(dtREQ.Rows[i]["Customer_Contact_No_1"]),
                    Customer_Contact_No_2 = Convert.ToString(dtREQ.Rows[i]["Customer_Contact_No_2"]),
                    Requestor_Name = Convert.ToString(dtREQ.Rows[i]["Requestor_Name"]),
                    Request_Date = Convert.ToString(dtREQ.Rows[i]["Request_Date"]),
                    Request_ID = Convert.ToInt32(dtREQ.Rows[i]["Request_ID"]),
                    Document_Holder_Name = Convert.ToString(dtREQ.Rows[i]["Document_Holder_Name"]),
                    Type_Of_Ownership = Convert.ToString(dtREQ.Rows[i]["Type_Of_Ownership"]),
                    ValuationType = Convert.ToString(dtREQ.Rows[i]["ValuationType"]),
                    ReportType = Convert.ToString(dtREQ.Rows[i]["ReportType"]),
                    Method_of_Valuation = Convert.ToString(dtREQ.Rows[i]["Method_of_Valuation"]),

                });
            }
            ViewBag.unitdetails = REQLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(REQLists);
            return Json(jsonProjectBuildingMaster);
        }
        //Changes by punam
        [HttpPost]
        public List<iva_UploadDocumentListNew> GetDocumentsEdit()
        {
            List<iva_UploadDocumentListNew> listdoc = new List<iva_UploadDocumentListNew>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadDocListsNew");
            for (int i = 0; i < dtdoc.Rows.Count; i++)
            {
                listdoc.Add(new iva_UploadDocumentListNew
                {
                    Document_ID = Convert.ToInt32(dtdoc.Rows[i]["Document_ID"]),
                    Document_Name = Convert.ToString(dtdoc.Rows[i]["Document_Name"]),
                    SrNo = Convert.ToInt32(dtdoc.Rows[i]["SrNo"]),

                });

            }
            return listdoc;
        }
        //changes by punam
        [HttpPost]
        public List<iva_UploadDocumentListNew> GetUploadedFilesListNew(string RequestID)
        {

            //List<ival_Document_Uploaded_List> listfiles = new List<ival_Document_Uploaded_List>();
            List<iva_UploadDocumentListNew> listfiles = new List<iva_UploadDocumentListNew>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@RequestId", RequestID);
            DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadedListNew", hInputPara);
            for (int i = 0; i < dtdoc.Rows.Count; i++)
            {
                //ival_Document_Uploaded_List obj = new ival_Document_Uploaded_List();
                iva_UploadDocumentListNew obj = new iva_UploadDocumentListNew();
                //listfiles.Add(new ival_Document_Uploaded_List
                //{
                if ((dtdoc.Rows[i]["Docupload_ID"]) != DBNull.Value)
                {
                    obj.Docupload_ID = Convert.ToInt32(dtdoc.Rows[i]["Docupload_ID"]);
                }
                obj.Document_ID = Convert.ToInt32(dtdoc.Rows[i]["Document_ID"]);
                obj.SrNo = Convert.ToInt32(dtdoc.Rows[i]["SrNo"]);
                obj.Document_Name = Convert.ToString(dtdoc.Rows[i]["Document_Name"]);
                if ((dtdoc.Rows[i]["Uploaded_Document"]) != DBNull.Value)
                {
                    string uploaddoc = Convert.ToString(dtdoc.Rows[i]["Uploaded_Document"]);
                    string value = uploaddoc;
                    if (value != "")
                    {
                        //string[] newValue = value.Split('\\');
                        //if (newValue != null)
                        //{

                        string fileName = value;

                        obj.FileName = fileName;
                        var filePath = Directory.GetFiles(Server.MapPath("~/UploadFiles"));
                        foreach (string filename in filePath)
                        {
                            if (filename.Contains(fileName))
                            {
                                var Get = filename;
                                obj.Uploaded_Document = Get;
                            }
                        }
                        // }
                    }

                    //var filePath = Directory.GetFiles(@"D:\Punam Gat\04092020BackUp_IValue\ivalue\ivalue_codefirst\I_Value.WebApp\IValue\UploadFiles\GS99999101_Report (1).pdf");


                }
                if ((dtdoc.Rows[i]["Approval_Authority"]) != DBNull.Value)
                {
                    obj.Approval_Authority = Convert.ToString(dtdoc.Rows[i]["Approval_Authority"]);
                }
                if ((dtdoc.Rows[i]["Date_Of_Approval"]) != DBNull.Value)
                {
                    // var format = "dd/MM/yyyy";
                    // var date= dtdoc.Rows[i]["Date_Of_Approval"].ToString();
                    //var dateofapp= DateTime.ParseExact(date,format, CultureInfo.InvariantCulture);
                    // obj.Date_Of_Approval = dateofapp;
                    var a = Convert.ToDateTime(dtdoc.Rows[i]["Date_Of_Approval"]).Date;
                    var b = a.ToString("dd/MM/yyyy");
                    obj.Date_Of_Approval_New = b;
                    // obj.Date_Of_Approval = Convert.ToDateTime(dtdoc.Rows[i]["Date_Of_Approval"]).Date;
                    //obj.Date_Of_Approval =Convert.ToDateTime(b);
                }
                if ((dtdoc.Rows[i]["Approval_Number"]) != DBNull.Value)
                {
                    obj.Approval_Number = Convert.ToString(dtdoc.Rows[i]["Approval_Number"]);
                }
                listfiles.Add(obj);
                //});

            }
            return listfiles;
        }

        public ActionResult SubsequesntRequestorDetails()
        {
            ViewBag.data = TempData["data"];
            DataTable dt = ViewBag.data;
            ViewModelRequest vm = new ViewModelRequest();
            vm.ival_LookupCategoryValuestypeofownership = GetTypeofOwnership();
            //vm.ival_LookupCategoryValuestypeofstructure = Gettypestructure();
            vm.ival_LookupCategoryValuesvaluationtype = Getvaltype();
            vm.ival_LookupCategoryValuesmethodvaluation = Getmethodtype();
            vm.ival_LookupCategoryValuesreporttype = GetReportType();
            vm.ival_LookupCategoryDepartment = GetDepartment();
            vm.ival_LookupCategoryLoanType = GetLoanType();
            vm.ival_ClientMasters = GetClientName();  //changes by punam

            //vm.ival_DocumentLists = GetDocuments();
            vm.ival_DocumentListsNew = GetDocumentsEdit();
            // vm.ival_Document_Uploaded_ListS = GetFilesList(ReqID);
            vm.ival_Document_Uploaded_ListS_New = GetUploadedFilesListNew(Session["SubRequest_ID"].ToString());
            //for adding new document sr.no created automatic
            DataTable DocID = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1DOCID");
            ViewBag.Srno = DocID.Rows[0]["Document_ID"].ToString();
            DataTable SrNo = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1DocSrNoId");
            ViewBag.SrnoNew = SrNo.Rows[0]["SrNo"].ToString();

            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@RequestID", Session["SubRequest_ID"].ToString());
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetReuestorsdetailsByReqID", hInputPara);
                if (dtRequests.Rows.Count > 0)
                {
                    ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                    ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                    ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);
                    ViewBag.Email_ID = Convert.ToString(dtRequests.Rows[0]["Email_ID"]);
                    ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                    ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                    ViewBag.Request_Date = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);
                    ViewBag.Valuation_Request_ID = Convert.ToString(dtRequests.Rows[0]["Valuation_Request_ID"]);
                    ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                    ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                    ViewBag.Selectedtypeofownership = dtRequests.Rows[0]["Type_Of_Ownership"].ToString();
                    ViewBag.Selectedvaluationtype = dtRequests.Rows[0]["ValuationType"].ToString();
                    ViewBag.Selectedreporttype = dtRequests.Rows[0]["ReportType"].ToString();
                    ViewBag.Selectedmethodvaluation = dtRequests.Rows[0]["Method_of_Valuation"].ToString();
                    ViewBag.AssignedToEmpID = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                    //changes by punam
                    if (dtRequests.Rows[0]["Client_ID"] == DBNull.Value)
                    {
                        dtRequests.Rows[0]["Client_ID"] = 0;
                    }

                    if (Convert.ToInt32(dtRequests.Rows[0]["Client_ID"]) != 0)
                    {
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara = new Hashtable();
                        hInputPara.Add("@Client_ID", Convert.ToInt32(dtRequests.Rows[0]["Client_ID"].ToString()));
                        DataTable dtClient = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientMasterByID", hInputPara);
                        if (dtClient != null && dtClient.Rows.Count > 0)
                        {
                            ViewBag.Client_Name = dtClient.Rows[0]["Client_Name"].ToString();
                            ViewBag.SelectBankID = dtClient.Rows[0]["Client_Name"].ToString();
                            ViewBag.Branch_Name = dtClient.Rows[0]["Branch_Name"].ToString();
                            ViewBag.SelectBranchName = dtClient.Rows[0]["Branch_Name"].ToString();
                            vm.ival_BranchMasters = GetBranchNameNew(ViewBag.Client_Name);
                            // vm.ival_BranchMasters = GetBranchListByBankNameNew(ViewBag.Client_Name);
                            // ViewBag.SelectBranchName = ViewBag.Branch_Name;
                        }

                        IEnumerable<ival_BranchMaster> ival_BranchMaster = GetBranchNameNew(ViewBag.Client_Name);
                        if (ival_BranchMaster.ToList().Count > 0 && ival_BranchMaster != null)
                        {
                            var BuilderState = ival_BranchMaster.Where(s => s.Branch_Name == dtClient.Rows[0]["Branch_Name"].ToString()).Select(s => s.Branch_ID);

                            vm.SelectBranchName = Convert.ToString(BuilderState.FirstOrDefault());

                        }

                    }


                    if (dtRequests.Rows[0]["Department"].ToString() != "")
                    {
                        ViewBag.SelectDepartmentID = dtRequests.Rows[0]["Department"].ToString();
                    }
                    if (dtRequests.Rows[0]["Loan_Type"].ToString() != "")
                    {
                        ViewBag.SelectLoanTypeID = dtRequests.Rows[0]["Loan_Type"].ToString().Trim();
                    }
                    if (dtRequests.Rows[0]["Requestor_Name"].ToString() != "")
                    {
                        ViewBag.Requestor_Name = dtRequests.Rows[0]["Requestor_Name"].ToString();
                    }

                    if (Convert.ToInt32(dtRequests.Rows[0]["Client_ID"]) != 0)
                    {
                        ViewBag.Client_ID = Convert.ToInt32(dtRequests.Rows[0]["Client_ID"].ToString());

                    }

                }
            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }


            return View(vm);


        }
        [HttpPost]
        public List<ProjectSiteEnginner> GetSiteEnginner()
        {
            List<ProjectSiteEnginner> ListSiteEnginner = new List<ProjectSiteEnginner>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojsiteEnginner");
            //DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientName");
            for (int i = 0; i < dtGetReportType.Rows.Count; i++)
            {

                ListSiteEnginner.Add(new ProjectSiteEnginner
                {
                    Site_Engineer_ID = Convert.ToInt32(dtGetReportType.Rows[i]["Site_Engineer_ID"]),
                    Site_Engineer_Name = Convert.ToString(dtGetReportType.Rows[i]["Site_Engineer_Name"]),

                });

            }
            return ListSiteEnginner;
        }

        [HttpGet]
        public ActionResult Edit(int? ID)
        {
            //Session["RequestID"] = 11;
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            //string ReqID = "11";
            ViewModelRequest vm = new ViewModelRequest();

            vm.projSiteEnginnerList = GetSiteEnginner();
            vm.ival_LookupCategoryValuestypeofownership = GetTypeofOwnership();
            //vm.ival_LookupCategoryValuestypeofstructure = Gettypestructure();
            vm.ival_LookupCategoryValuesvaluationtype = Getvaltype();
            vm.ival_LookupCategoryValuesmethodvaluation = Getmethodtype();
            vm.ival_LookupCategoryValuesreporttype = GetReportType();
            vm.ival_LookupCategoryDepartment = GetDepartment();
            vm.ival_LookupCategoryLoanType = GetLoanType();
            vm.ival_ClientMasters = GetClientName();  //changes by punam

            //vm.ival_DocumentLists = GetDocuments();
            vm.ival_DocumentListsNew = GetDocumentsEdit();
            // vm.ival_Document_Uploaded_ListS = GetFilesList(ReqID);
            vm.ival_Document_Uploaded_ListS_New = GetUploadedFilesListNew(ReqID);
            //for adding new document sr.no created automatic
            DataTable DocID = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1DOCID");
            ViewBag.Srno = DocID.Rows[0]["Document_ID"].ToString();
            DataTable SrNo = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1DocSrNoId");
            ViewBag.SrnoNew = SrNo.Rows[0]["SrNo"].ToString();

            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@RequestID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetReuestorsdetailsByReqID", hInputPara);
                if (dtRequests.Rows.Count > 0)
                {
                    ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                    ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                    ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);
                    ViewBag.Email_ID = Convert.ToString(dtRequests.Rows[0]["Email_ID"]);
                    ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                    ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                    //string date = Convert.ToDateTime(dtRequests.Rows[0]["Request_Date"]).ToString("dd/MM/yyyy");
                    try
                    {
                        string date = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);
                        if (date != "")
                        {
                            string format = "dd/MM/yyyy";
                            string date1 = Convert.ToDateTime(date).ToString("dd/MM/yyyy");
                            var dateTime = DateTime.ParseExact(date1, format, CultureInfo.InvariantCulture);
                            ViewBag.Request_Date = date;
                        }
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            string date = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);
                            if (date != "")
                            {
                                string format = "dd/MM/yyyy";
                                /// string date1 = Convert.ToDateTime(date).ToString("dd/MM/yyyy");
                                var dateTime = DateTime.ParseExact(date, format, CultureInfo.InvariantCulture);
                                ViewBag.Request_Date = date;
                            }


                        }
                        catch (Exception ee)
                        {
                            ViewBag.Request_Date = dtRequests.Rows[0]["Request_Date"].ToString();
                        }
                    }
                    try
                    {
                        string date = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                        if (date != "")
                        {
                            string format = "dd/MM/yyyy";
                            string date1 = Convert.ToDateTime(date).ToString("dd/MM/yyyy");
                            var dateTime = DateTime.ParseExact(date1, format, CultureInfo.InvariantCulture);
                            ViewBag.DateOfInspection = date;
                        }
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            string date = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                            if (date != "")
                            {
                                string format = "dd/MM/yyyy";
                                /// string date1 = Convert.ToDateTime(date).ToString("dd/MM/yyyy");
                                var dateTime = DateTime.ParseExact(date, format, CultureInfo.InvariantCulture);
                                ViewBag.DateOfInspection = date;
                            }


                        }
                        catch (Exception ee)
                        {
                            ViewBag.DateOfInspection = dtRequests.Rows[0]["Date_of_Inspection"].ToString();
                        }
                    }
                    try
                    {
                        string date = Convert.ToString(dtRequests.Rows[0]["Date_of_Valuation"]);
                        if (date != "")
                        {
                            string format = "dd/MM/yyyy";
                            string date1 = Convert.ToDateTime(date).ToString("dd/MM/yyyy");
                            var dateTime = DateTime.ParseExact(date1, format, CultureInfo.InvariantCulture);
                            ViewBag.DateOfValuation = date;
                        }
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            string date = Convert.ToString(dtRequests.Rows[0]["Date_of_Valuation"]);
                            if (date != "")
                            {
                                string format = "dd/MM/yyyy";
                                /// string date1 = Convert.ToDateTime(date).ToString("dd/MM/yyyy");
                                var dateTime = DateTime.ParseExact(date, format, CultureInfo.InvariantCulture);
                                ViewBag.DateOfValuation = date;
                            }


                        }
                        catch (Exception ee)
                        {
                            ViewBag.DateOfValuation = dtRequests.Rows[0]["Date_of_Valuation"].ToString();
                        }
                    }

                    //ViewBag.Request_Date = date;


                    ViewBag.Valuation_Request_ID = Convert.ToString(dtRequests.Rows[0]["Valuation_Request_ID"]);
                    ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                    ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                    ViewBag.Selectedtypeofownership = dtRequests.Rows[0]["Type_Of_Ownership"].ToString();
                    ViewBag.Selectedvaluationtype = dtRequests.Rows[0]["ValuationType"].ToString();
                    ViewBag.Selectedreporttype = dtRequests.Rows[0]["ReportType"].ToString();
                    ViewBag.Selectedmethodvaluation = dtRequests.Rows[0]["Method_of_Valuation"].ToString();
                    ViewBag.AssignedToEmpID = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                    //changes by punam
                    if (dtRequests.Rows[0]["Client_ID"] == DBNull.Value)
                    {
                        dtRequests.Rows[0]["Client_ID"] = 0;
                    }

                    if (Convert.ToInt32(dtRequests.Rows[0]["Client_ID"]) != 0)
                    {
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara = new Hashtable();
                        hInputPara.Add("@Client_ID", Convert.ToInt32(dtRequests.Rows[0]["Client_ID"].ToString()));
                        DataTable dtClient = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientMasterByID", hInputPara);
                        if (dtClient != null && dtClient.Rows.Count > 0)
                        {
                            ViewBag.Client_Name = dtClient.Rows[0]["Client_Name"].ToString();
                            ViewBag.SelectBankID = dtClient.Rows[0]["Client_Name"].ToString();
                            ViewBag.Branch_Name = dtClient.Rows[0]["Branch_Name"].ToString();
                            ViewBag.SelectBranchName = dtClient.Rows[0]["Branch_Name"].ToString();
                            vm.ival_BranchMasters = GetBranchNameNew(ViewBag.Client_Name);
                            // vm.ival_BranchMasters = GetBranchListByBankNameNew(ViewBag.Client_Name);
                            ViewBag.SelectBranchName = ViewBag.Branch_Name;
                        }

                    }
                    if (dtRequests.Rows[0]["Department"].ToString() != "")
                    {
                        ViewBag.SelectDepartmentID = dtRequests.Rows[0]["Department"].ToString();
                    }
                    if (dtRequests.Rows[0]["Loan_Type"].ToString() != "")
                    {
                        ViewBag.SelectLoanTypeID = dtRequests.Rows[0]["Loan_Type"].ToString().Trim();
                    }
                    if (dtRequests.Rows[0]["Requestor_Name"].ToString() != "")
                    {
                        ViewBag.Requestor_Name = dtRequests.Rows[0]["Requestor_Name"].ToString();
                    }

                    if (Convert.ToInt32(dtRequests.Rows[0]["Client_ID"]) != 0)
                    {
                        ViewBag.Client_ID = Convert.ToInt32(dtRequests.Rows[0]["Client_ID"].ToString());

                    }
                    IEnumerable<ProjectSiteEnginner> projSiteEnginnerList = GetSiteEnginner();
                    if (projSiteEnginnerList.ToList().Count > 0 && projSiteEnginnerList != null)
                    {
                        var siteEnginner = projSiteEnginnerList.Where(s => s.Site_Engineer_Name == dtRequests.Rows[0]["AssignedToEmpID"].ToString()).Select(s => s.Site_Engineer_ID);

                        vm.siteEnginnerId = Convert.ToInt32(siteEnginner.FirstOrDefault());

                    }

                }
            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }


            return View(vm);
        }

        [HttpPost]
        public ActionResult SaveAssignedEmp(string str)
        {
            try
            {

                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                //DataTable dtREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1RequestID");
                string Request_ID = Session["Request_ID"].ToString();
                Session["RequestID"] = Session["Request_ID"].ToString();
                //changes by punam
                //string Request_ID = Session["RequestID"].ToString();
                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                for (int i = 0; i < list1.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@AssignedEmp", Convert.ToString(list1.Rows[i]["AssignedEmp"]));
                    //changes by punam
                    hInputPara.Add("@Request_ID", Convert.ToInt32(Request_ID));

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertAssignedEMP", hInputPara);

                }
                return Json(new { success = true, Message = "Employee Assigned Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //changes by punam SaveAssignEmpEdit
        [HttpPost]
        public ActionResult SaveAssignedEmpEdit(string str)
        {
            try
            {

                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                //DataTable dtREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1RequestID");
                string Request_ID = Session["RequestID"].ToString();
                Session["Request_ID"] = Session["RequestID"].ToString();
                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                for (int i = 0; i < list1.Rows.Count; i++)
                {

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@AssignedEmp", Convert.ToString(list1.Rows[i]["AssignedEmp"]));
                    //changes by punam
                    hInputPara.Add("@Request_ID", Convert.ToInt32(Request_ID));
                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertAssignedEMP", hInputPara);
                    hInputPara.Clear();
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Request_ID", Convert.ToInt32(Session["RequestID"].ToString()));
                    DataTable dtpin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProperty", hInputPara);
                    if (dtpin.Rows.Count > 0)
                    {
                        if (dtpin.Rows[0]["property_type"].ToString() != "" || dtpin.Rows[0]["property_type"].ToString() != null)
                        {
                            return RedirectToAction("EditPropertydetailsViewNew", "Project", new { area = "Project" });
                        }
                        else
                        {
                            return RedirectToAction("SavePropertyDetails", "Project", new { area = "Project" });
                        }
                    }
                    else
                    {
                        return RedirectToAction("SavePropertyDetails", "Project", new { area = "Project" });
                    }

                }
                return Json(new { success = true, Message = "Employee Assigned Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult SingleUnitdetails()
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            ViewModelRequest vm = new ViewModelRequest();

            DataTable dtunit = new DataTable();
            // string RequestID = Session["Request_ID"].ToString();

            if (Session["dt"] == null)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@Request_ID", Convert.ToInt32(Session["RequestID"].ToString()));
                DataTable dtpin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPropertyNew", hInputPara);
                ViewBag.UnitType = dtpin.Rows[0]["TypeOfUnit"].ToString();
                ViewBag.PType = dtpin.Rows[0]["property_type"].ToString();
                Session["Projectidretail"] = dtpin.Rows[0]["Project_ID"].ToString();
                Session["UnitType"] = ViewBag.UnitType;
                Session["PType"] = ViewBag.PType;
                Session["TypeOfSelect"] = ViewBag.SelectType;
                //ViewBag.UnitType = "Bungalow";
                hInputPara.Clear();
                hInputPara1 = new Hashtable();

                // DataTable dtREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1RequestID");
                hInputPara1.Add("@Project_ID", Convert.ToString(Session["ValuationRequestID"]));
                DataTable ValuationRequestID1 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRequestIDNew", hInputPara1);
                for (int i = 0; i < ValuationRequestID1.Rows.Count; i++)
                {

                    Session["GetInspDate"] = ValuationRequestID1.Rows[0]["Date_of_Inspection"].ToString();
                    Session["GetEmpId"] = ValuationRequestID1.Rows[0]["AssignedToEmpID"].ToString();
                }
                ViewBag.GetInspDate = Session["GetInspDate"].ToString();
                ViewBag.GetEmpId = Session["GetEmpId"].ToString();

                Hashtable hInputPara22 = new Hashtable();
                hInputPara22.Add("@Project_ID", Session["Projectidretail"]);
                DataTable ValuationRequestID2 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorwisebreakUpperFloor", hInputPara22);
                // Number_Of_Floors = Convert.ToString(dtsocialinfra.Rows[i]["Number_Of_Floors"]),
                //ViewBag.floorCount = Convert.ToInt32(ValuationRequestID2.Rows[0]["Number_Of_Floors"]);
                if (ValuationRequestID2.Rows.Count > 0)
                {
                    ViewBag.floorCount = Convert.ToInt32(ValuationRequestID2.Rows[0]["Number_Of_Floors"]);
                }
                else
                {
                    ViewBag.floorCount = 0;
                }
                //ViewBag.PType = "Residential";
                //DataTable dtREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1RequestID");
                string Unitdetails = Convert.ToString(Session["ApprovalDetails"]);
                vm.ival_ProjectImageName = GetProjectMasterImageName();

                //vm.ival_UnitParameterMasters = GetUnitDetails(unitname);
                vm.ival_ProjectBuildingWingFloorDetails = GetFloorListbyreqIDNew();
               vm.ival_LookupCategoryFloorTypes = GetFloorType();
                vm.ival_ApprovalType = GetApprovalType();
                vm.ival_LookupCategoryBoundaries = GetBoundaries();
                vm.ival_LookupCategorySideMargins = GetSideMargins();
                vm.ival_LookupCategoryValuesviewfromunit = GetviewUnits();
                vm.ival_LookupCategoryValuesRiskofdemolition = Getdemolition();
                vm.ival_LookupCategoryValuesdemarcation = GetDemarcation();
                vm.ival_LookupCategoryValuesdemarcation1 = GetDemarcation1();
                vm.ival_LookupCategoryValuesRoadFinish = GetRoadFinish();
                vm.ival_LookupCategoryValuesPropZone = GetPropZone();
                vm.ival_LookupCategoryLiftTypes = GetLiftType();
                vm.ival_ProjectApprovalDetails = GetApprovalTypesnew();
                vm.ival_ImageMasters = GetImageMaster();
                //ViewBag.DateofInsp = Session["GetInspDate"].ToString();
                //ViewBag.GetEmpId = Session["GetEmpId"].ToString();

                vm.ival_Lookupflooring = Getfloring();
                vm.ival_Lookupwall = Getwall();
                vm.ival_Lookupplumbing = Getplumbing();
                vm.ival_Lookupelec = Getelect();
                vm.ival_Lookupother = Getother();//qualtlity
                vm.ival_Lookupexter = Getextr();
                vm.relationWithEmpList1 = GetRelationsWithEmp1();
                vm.relationWithEmpList2 = GetRelationsWithEmp2();


               // Hashtable hInputPara22 = new Hashtable();

                // DataTable dtREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1RequestID");
                //hInputPara22.Add("@Project_ID", Convert.ToString(Session["ValuationRequestID"]));
               


            }
            else
            {

                //Response.Redirect("Unitdetailssearch");
            }
            return View(vm);

        }
        public JsonResult DownloadImage1()
        {
            // UploadImage(i);
            var FilePath = Session["ImageSave"].ToString();
            var folderPath = Server.MapPath("~/images");
            var fileCount = Directory.GetFiles(Server.MapPath("~/images"));
            if (fileCount.Length > 1) // exactly only 1 file
            {
                string embed = "<object data=\"{0}\" type=\"application/pdf\"width=\"2000px\" height=\"1000px\">";
                embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
                embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                embed += "</object>";
                TempData["EmbedImage1"] = string.Format(embed, VirtualPathUtility.ToAbsolute("~/images/" + FilePath));
                return Json("Exist", JsonRequestBehavior.AllowGet);
            }

            else
            {
                Process.Start(folderPath);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }
        public JsonResult DownloadImageTest1(string imageTypeId, string seqId)
        {
            string Request_ID = Session["Request_ID" + ""].ToString();
            string SeqId = seqId.PadLeft(2, '0');
            String ImageTypeId = SeqId + imageTypeId;
            String FileSave = Request_ID + "_" + ImageTypeId;//2771_011
            var FilePath = "";
            var getFile = Directory.GetFiles(Server.MapPath("~/images"));
            //var b = VirtualPathUtility.ToAbsolute("~/UploadFiles/" + fileName);
            foreach (string filename in getFile)
            {
                if (filename.Contains(FileSave))
                {
                    var Get = filename;
                    FilePath = Get;

                }
            }

            // UploadImage(i);
            var FilePath1 = Session["ImageSave"].ToString();
            var folderPath = Server.MapPath("~/images");
            var fileCount = Directory.GetFiles(Server.MapPath("~/images"));
            string[] newValue = FilePath.Split('\\');
            if (newValue != null)
            {
                var FilePathNew = newValue[newValue.Length - 1];
                ViewBag.Images = FilePathNew;

                if (fileCount.Length > 1) // exactly only 1 file
                {
                    string embed = "<object data=\"{0}\" type=\"application/pdf\"width=\"876px\" height=\"550px\">";
                    embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
                    embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                    embed += "</object>";
                    TempData["EmbedImage1"] = string.Format(embed, VirtualPathUtility.ToAbsolute("~/images/" + FilePathNew));
                    return Json("Exist", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                Process.Start(folderPath);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }
        public JsonResult DownloadImageEdit(string imageTypeId, string seqId)
        {

            var FilePath = "";
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            var NewFilePath = TempData["TempImageUnitEdit"];
            int ImageTypeId = Convert.ToInt32(imageTypeId);
            int SeqIdNew = Convert.ToInt32(seqId);

            // hInputPara1.Add("@Request_ID", Convert.ToInt32(RequestID));
            // DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            //string projectId = Convert.ToString(ProjId.Rows[0]["Project_ID"]);

            hInputPara.Add("@RequestId", RequestID);
            hInputPara.Add("@ImageTypeId", ImageTypeId);
            hInputPara.Add("@seqId", SeqIdNew);
            // hInputPara.Add("@projectId", Convert.ToInt32(projectId));
            DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadedImageNameEdit", hInputPara);
            if (dtRequests.Rows.Count > 0)
            {
                if (dtRequests.Rows[0]["Image_Path"].ToString() != string.Empty && (dtRequests.Rows[0]["Image_Path"].ToString()) != null)
                {
                    string fileName = Convert.ToString(dtRequests.Rows[0]["Image_Path"]);
                    if (NewFilePath != null)
                    {
                        FilePath = NewFilePath.ToString();
                    }
                    else
                    {
                        FilePath = fileName;
                    }

                }

            }
            else
            {
                var FileGet = Session["ImageSaveUnitEdit"];
                FilePath = FileGet.ToString();
            }


            var folderPath = Server.MapPath("~/images");
            var fileCount = Directory.GetFiles(Server.MapPath("~/images"));
            if (fileCount.Length > 1) // exactly only 1 file
            {
                string embed = "<object data=\"{0}\" type=\"application/pdf\"width=\"2000px\" height=\"1000px\">";
                embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
                embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                embed += "</object>";
                TempData["EmbedImage1"] = string.Format(embed, VirtualPathUtility.ToAbsolute("~/images/" + FilePath));
                return Json("Exist", JsonRequestBehavior.AllowGet);
            }

            else
            {
                Process.Start(folderPath);
            }

            return Json("", JsonRequestBehavior.AllowGet);


        }
        public JsonResult DownloadImageView(string imageTypeId, string seqId)
        {

            var FilePath = "";
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            var NewFilePath = TempData["TempImageUnitEdit"];
            int ImageTypeId = Convert.ToInt32(imageTypeId);
            int SeqIdNew = Convert.ToInt32(seqId);


            hInputPara.Add("@RequestId", RequestID);
            hInputPara.Add("@ImageTypeId", ImageTypeId);
            hInputPara.Add("@seqId", SeqIdNew);
            // hInputPara.Add("@projectId", Convert.ToInt32(projectId));
            DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadedImageNameEdit", hInputPara);
            if (dtRequests.Rows.Count > 0)
            {
                if (dtRequests.Rows[0]["Image_Path"].ToString() != string.Empty && (dtRequests.Rows[0]["Image_Path"].ToString()) != null)
                {
                    string fileName = Convert.ToString(dtRequests.Rows[0]["Image_Path"]);
                    if (NewFilePath != null)
                    {
                        FilePath = NewFilePath.ToString();
                    }
                    else
                    {
                        FilePath = fileName;
                    }

                }

            }
            else
            {
                var FileGet = Session["ImageSaveUnitEdit"];
                FilePath = FileGet.ToString();
            }


            var folderPath = Server.MapPath("~/images");
            var fileCount = Directory.GetFiles(Server.MapPath("~/images"));
            if (fileCount.Length > 1) // exactly only 1 file
            {
                string embed = "<object data=\"{0}\" type=\"application/pdf\"width=\"100%\" height=\"600px\">";
                embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
                embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                embed += "</object>";
                TempData["EmbedImage1"] = string.Format(embed, VirtualPathUtility.ToAbsolute("~/images/" + FilePath));
                return Json("Exist", JsonRequestBehavior.AllowGet);
            }

            else
            {
                Process.Start(folderPath);
            }

            return Json("", JsonRequestBehavior.AllowGet);


        }
        public ActionResult DownloadImageView1()
        {
            return View();
        }
        //upload Images
        [HttpPost]
        public JsonResult UploadImage1(string ImageTypeId)
        {
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            string Request_ID = Session["Request_ID" + ""].ToString();
            //string Request_ID = "2707";
            string ImageID = ImageTypeId;
            //hInputPara1.Add("@Request_ID", Convert.ToInt32(Request_ID));
            //DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprjIDByRequestID", hInputPara1);
            //string projectId = Convert.ToString(ProjId.Rows[0]["Project_ID"]);
            string a = ImageID.PadLeft(2, '0');
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];

                var ImgName = Path.GetFileName(file.FileName);
                string fileName = ImgName;
                // Find its length and convert it to byte array
                int ContentLength = file.ContentLength;

                // Create Byte Array
                byte[] bytImg = new byte[ContentLength];

                // Read Uploaded file in Byte Array
                file.InputStream.Read(bytImg, 0, ContentLength);

                //var ImageFile = Request_ID + "_" + projectId + "_" + a + "_" + fileName;
                var ImageFile = Request_ID + "_" + a + "_" + fileName;
                var path = Path.Combine(Server.MapPath("~/images/"), ImageFile);
                file.SaveAs(path);
                //Session["ImageSave"] = null;
                Session["ImageSave"] = ImageFile;
                TempData["ImageS"] = ImageFile;
                ViewBag.image = ImageFile;
            }

            return Json("Uploaded " + Request.Files.Count + " Images");
        }
        //upload Images
        [HttpPost]
        public JsonResult UploadImageEdit(string ImageTypeId)
        {
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            string Request_ID = Session["RequestID"].ToString();
            string ImageID = ImageTypeId;
            //hInputPara1.Add("@Request_ID", Convert.ToInt32(Request_ID));
            //DataTable ProjId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprojectValIDByRequestId", hInputPara1);
            //string projectId = Convert.ToString(ProjId.Rows[0]["Project_ID"]);
            string a = ImageID.PadLeft(2, '0');
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];

                var ImgName = Path.GetFileName(file.FileName);
                string fileName = ImgName;
                // Find its length and convert it to byte array
                int ContentLength = file.ContentLength;

                // Create Byte Array
                byte[] bytImg = new byte[ContentLength];

                // Read Uploaded file in Byte Array
                file.InputStream.Read(bytImg, 0, ContentLength);

                var ImageFile = Request_ID + "_" + a + "_" + fileName;
                var getFile = Directory.GetFiles(Server.MapPath("~/images"));
                foreach (string filename in getFile)
                {
                    if (filename.Contains(ImageFile))
                    {
                        var Get = filename;
                        if (System.IO.File.Exists(filename))
                        {
                            System.IO.File.Delete(filename);
                            //Console.WriteLine("file Deleted");
                            file.SaveAs(Server.MapPath("../images/") + ImageFile);

                        }
                    }
                    else
                    {

                        file.SaveAs(Server.MapPath("../images/") + ImageFile);

                        Dispose();
                    }

                }
                Session["ImageSaveUnitEdit"] = ImageFile;
                TempData["TempImageUnitEdit"] = ImageFile;

                //var path = Path.Combine(Server.MapPath("~/images/"), ImageFile);
                //file.SaveAs(path);

                //Session["ImageSave"] = ImageFile;

            }

            return Json("Uploaded " + Request.Files.Count + " Images");
        }
        public List<ival_ImageMaster> GetProjectMasterImageName()
        {

            List<ival_ImageMaster> projectImage = new List<ival_ImageMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtGetProjDes = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ProjectUploadImgeName");
            //DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientName");
            for (int i = 0; i < dtGetProjDes.Rows.Count; i++)
            {
                ival_ImageMaster list = new ival_ImageMaster();
                list.Image_Type_ID = Convert.ToInt32(dtGetProjDes.Rows[i]["Image_Type_ID"]);
                list.Image_Type = Convert.ToString(dtGetProjDes.Rows[i]["Image_Type"]);

                projectImage.Add(list);
            }
            return projectImage;

        }
        public ActionResult Unitdetailssearch()
        {
            //int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();


            int Project_ID = Convert.ToInt32(Session["Projectidretail"].ToString());
            //int Project_ID = 154;
            hInputPara1.Add("@Project_ID", Project_ID);
            DataTable dtunitserachreq = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRequestIDByProjectID", hInputPara1);
            // int Request_ID = 157;
            int Request_ID = Convert.ToInt32(dtunitserachreq.Rows[0]["RequestID"].ToString());
            hInputPara.Add("@Request_ID", Request_ID);
            Session["RequestID"] = Request_ID;
            DataTable dtunitREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllunitsBYReqID", hInputPara);
            ViewBag.UnitType = dtunitREQ.Rows[0]["UType"].ToString();
            ViewBag.PType = dtunitREQ.Rows[0]["PTypeName"].ToString();
            ViewBag.Deviation_Description = dtunitREQ.Rows[0]["Deviation_Description"].ToString();
            ViewBag.Construction_as_per_plan = dtunitREQ.Rows[0]["Construction_as_per_plan"].ToString();
            ViewBag.Risk_of_Demolition = dtunitREQ.Rows[0]["Risk_of_Demolition"].ToString();
            ViewBag.Type_of_Flooring = dtunitREQ.Rows[0]["Type_of_Flooring"].ToString();
            ViewBag.Wall_Finish = dtunitREQ.Rows[0]["Wall_Finish"].ToString();
            ViewBag.Plumbing_Fitting = dtunitREQ.Rows[0]["Plumbing_Fitting"].ToString();
            ViewBag.Electrical_Fittings = dtunitREQ.Rows[0]["Electrical_Fittings"].ToString();
            ViewBag.Rough_Plaster = dtunitREQ.Rows[0]["Rough_Plaster"].ToString();
            ViewBag.Tiling = dtunitREQ.Rows[0]["Tiling"].ToString();
            ViewBag.Cord_Longitude = dtunitREQ.Rows[0]["Cord_Longitude"].ToString();
            ViewBag.Cord_Latitude = dtunitREQ.Rows[0]["Cord_Latitude"].ToString();
            ViewBag.Road1 = dtunitREQ.Rows[0]["Road1"].ToString();
            ViewBag.Road2 = dtunitREQ.Rows[0]["Road2"].ToString();
            ViewBag.Road3 = dtunitREQ.Rows[0]["Road3"].ToString();
            ViewBag.Road4 = dtunitREQ.Rows[0]["Road4"].ToString();
            ViewBag.SeismicZone = dtunitREQ.Rows[0]["SeismicZone"].ToString();
            ViewBag.FloodZone = dtunitREQ.Rows[0]["FloodZone"].ToString();
            ViewBag.CycloneZone = dtunitREQ.Rows[0]["CycloneZone"].ToString();
            ViewBag.CRZ = dtunitREQ.Rows[0]["CRZ"].ToString();
            ViewBag.Remarks = dtunitREQ.Rows[0]["Remarks"].ToString();
            ViewBag.Plot_Demarcated_at_site = dtunitREQ.Rows[0]["Plot_Demarcated_at_site"].ToString();
            ViewBag.Type_of_Demarcation = dtunitREQ.Rows[0]["Type_of_Demarcation"].ToString();

            ViewBag.Boundaries_Matching = dtunitREQ.Rows[0]["Boundaries_Matching"].ToString();
            ViewBag.Road_Finish = dtunitREQ.Rows[0]["Road_Finish"].ToString();
            ViewBag.AreBoundariesremarks = dtunitREQ.Rows[0]["AreBoundariesremarks"].ToString();

            ViewBag.Name = dtunitREQ.Rows[0]["Name"].ToString();
            ViewBag.Property_Identified_PName = dtunitREQ.Rows[0]["Property_Identified_PName"].ToString();
            ViewBag.P_Contact_Num = dtunitREQ.Rows[0]["P_Contact_Num"].ToString();
            ViewBag.Customer_Name_On_Board = dtunitREQ.Rows[0]["CustomerDisplayBoard"].ToString();
            ViewBag.Society_OR_Building_Name = dtunitREQ.Rows[0]["Society_OR_Building_Name"].ToString();

            ViewBag.Current_Value_of_Property = dtunitREQ.Rows[0]["Current_Value_of_Property"].ToString();
            ViewBag.Total_Value_of_Property = dtunitREQ.Rows[0]["Total_Value_of_Property"].ToString();
            ViewBag.Government_Value = dtunitREQ.Rows[0]["Government_Value"].ToString();
            ViewBag.Cost_of_Interior = dtunitREQ.Rows[0]["Cost_of_Interior"].ToString();
            ViewBag.MeasuredSaleableArea = dtunitREQ.Rows[0]["MeasuredSaleableArea"].ToString();
            ViewBag.ApprovedSaleableArea = dtunitREQ.Rows[0]["ApprovedSaleableArea"].ToString();
            ViewBag.distressValue = dtunitREQ.Rows[0]["distressValue"].ToString();
            ViewBag.StageOfConstruction = dtunitREQ.Rows[0]["StageOfConstruction"].ToString();
            ViewBag.IsInteriorworkcarriedout = dtunitREQ.Rows[0]["IsInteriorworkcarriedout"].ToString();
            ViewBag.ValuationResult = dtunitREQ.Rows[0]["ValuationResult"].ToString();
            ViewBag.DescStageofconstruction = dtunitREQ.Rows[0]["DescStageofconstruction"].ToString();
            ViewBag.No_Of_Lifts = dtunitREQ.Rows[0]["No_Of_Lifts"].ToString();
            ViewBag.IsLifts_Present = dtunitREQ.Rows[0]["IsLifts_Present"].ToString();
            ViewBag.ViewFromUnit = dtunitREQ.Rows[0]["ViewFromUnit"].ToString();
            ViewBag.MeasuredApprovedareamatchatsite = dtunitREQ.Rows[0]["MeasuredApprovedareamatchatsite"].ToString();
            ViewBag.DeviationPercentage = dtunitREQ.Rows[0]["DeviationPercentage"].ToString();
            ViewBag.Value_of_Plot = dtunitREQ.Rows[0]["Value_of_Plot"].ToString();
            ViewBag.Total_Land_Plot_Value = dtunitREQ.Rows[0]["Total_Land_Plot_Value"].ToString();
            ViewBag.Value_of_other_Plot = dtunitREQ.Rows[0]["Value_of_other_Plot"].ToString();
            ViewBag.Distress_Value_of_Plot = dtunitREQ.Rows[0]["Distress_Value_of_Plot"].ToString();
            ViewBag.OtherPlotType = dtunitREQ.Rows[0]["OtherPlotType"].ToString();
            ViewBag.Extra_Cost_Valuation = dtunitREQ.Rows[0]["Extra_Cost_Valuation"].ToString();

            ViewBag.Work_Completed_Perc = dtunitREQ.Rows[0]["Work_Completed_Perc"].ToString();
            ViewBag.StageOfConstructionBunglow = dtunitREQ.Rows[0]["StageOfConstructionBunglow"].ToString();
            ViewBag.StageOfConstructionPerct = dtunitREQ.Rows[0]["StageOfConstructionPerct"].ToString();
            ViewBag.DisbursementRecommendPerct = dtunitREQ.Rows[0]["DisbursementRecommendPerct"].ToString();
            ViewBag.IsInteriorworkcarriedout = dtunitREQ.Rows[0]["IsInteriorworkcarriedout"].ToString();
            ViewBag.DistressValuePerct = dtunitREQ.Rows[0]["DistressValuePerct"].ToString();
            ViewBag.Government_Rate = dtunitREQ.Rows[0]["Government_Rate"].ToString();
            ViewBag.DescStageofconstructionBunglow = dtunitREQ.Rows[0]["DescStageofconstructionBunglow"].ToString();
            ViewBag.Gross_Estimated_Cost_Construction = dtunitREQ.Rows[0]["Gross_Estimated_Cost_Construction"].ToString();
            ViewBag.Cost_Construction_PerSqFT = dtunitREQ.Rows[0]["Cost_Construction_PerSqFT"].ToString();
            ViewBag.Does_this_Matches_UnderConstruction = dtunitREQ.Rows[0]["Does_this_Matches_UnderConstruction"].ToString();
            ViewBag.Does_this_Cost_Remark = dtunitREQ.Rows[0]["Does_this_Cost_Remark"].ToString();

            ViewBag.Valueoftheplotbungalow = dtunitREQ.Rows[0]["Valueoftheplotbungalow"].ToString();
            ViewBag.Valueoftheotherplotbungalow = dtunitREQ.Rows[0]["Valueoftheotherplotbungalow"].ToString();
            ViewBag.Totalofthebungalow = dtunitREQ.Rows[0]["Totalofthebungalow"].ToString();
            ViewBag.TotalvaluepropertyBunglow = dtunitREQ.Rows[0]["TotalvaluepropertyBunglow"].ToString();
            ViewBag.DistressValueofPropertyBunglow = dtunitREQ.Rows[0]["DistressValueofPropertyBunglow"].ToString();
            ViewBag.TotalvaluepropertyBunglowUC = dtunitREQ.Rows[0]["TotalvaluepropertyBunglowUC"].ToString();
            ViewBag.Current_Value_of_Property_UC = dtunitREQ.Rows[0]["Current_Value_of_Property_UC"].ToString();
            ViewBag.ValuationResultR = dtunitREQ.Rows[0]["ValuationResultR"].ToString();
            ViewBag.ValuationResultUC = dtunitREQ.Rows[0]["ValuationResultUC"].ToString();
            ViewBag.Cost_of_InteriorReady = dtunitREQ.Rows[0]["Cost_of_InteriorReady"].ToString();
            ViewBag.StageOfConstructionUC = dtunitREQ.Rows[0]["StageOfConstructionUC"].ToString();
            ViewBag.DisbursementRecommendPerctUC = dtunitREQ.Rows[0]["DisbursementRecommendPerctUC"].ToString();
            ViewBag.ValuationResultplot = dtunitREQ.Rows[0]["ValuationResultplot"].ToString();

            ViewBag.MeasuredAreaFt = dtunitREQ.Rows[0]["MeasuredAreaFt"].ToString();
            ViewBag.ApprovedAreaF = dtunitREQ.Rows[0]["ApprovedAreaF"].ToString();
            ViewBag.AdoptedAValFt = dtunitREQ.Rows[0]["AdoptedAValFt"].ToString();
            ViewBag.AdoptedLandRFt = dtunitREQ.Rows[0]["AdoptedLandRFt"].ToString();

            ViewBag.MeasuredAreaMt = dtunitREQ.Rows[0]["MeasuredAreaMt"].ToString();
            ViewBag.ApprovedAreaMt = dtunitREQ.Rows[0]["ApprovedAreaMt"].ToString();
            ViewBag.AdoptedAValMt = dtunitREQ.Rows[0]["AdoptedAValMt"].ToString();
            ViewBag.AdoptedLandRMt = dtunitREQ.Rows[0]["AdoptedLandRMt"].ToString();

            ViewBag.MeasuredOtherPAFt = dtunitREQ.Rows[0]["MeasuredOtherPAFt"].ToString();
            ViewBag.ApproveOtherPAFt = dtunitREQ.Rows[0]["ApproveOtherPAFt"].ToString();
            ViewBag.AdoptedOtherPAValFt = dtunitREQ.Rows[0]["AdoptedOtherPAValFt"].ToString();
            ViewBag.AdoptedOtherPLandRFt = dtunitREQ.Rows[0]["AdoptedOtherPLandRFt"].ToString();

            ViewBag.MeasuredOtherPAMt = dtunitREQ.Rows[0]["MeasuredOtherPAMt"].ToString();
            ViewBag.ApproveOtherPAMt = dtunitREQ.Rows[0]["ApproveOtherPAMt"].ToString();
            ViewBag.AdoptedOtherPAValMt = dtunitREQ.Rows[0]["AdoptedOtherPAValMt"].ToString();
            ViewBag.AdoptedOtherPLandRMt = dtunitREQ.Rows[0]["AdoptedOtherPLandRMt"].ToString();

            ViewBag.Permissible = dtunitREQ.Rows[0]["Permissible"].ToString();
            ViewBag.LandRateRangeLoc = dtunitREQ.Rows[0]["LandRateRangeLoc"].ToString();
            ViewBag.PermissibleOtherplot = dtunitREQ.Rows[0]["PermissibleOtherplot"].ToString();
            ViewBag.GovtRateSF = dtunitREQ.Rows[0]["GovtRateSF"].ToString();
            ViewBag.GovtvalueS = dtunitREQ.Rows[0]["GovtvalueS"].ToString();

            ViewBag.MatchAt = dtunitREQ.Rows[0]["MatchAt"].ToString();
            ViewBag.nill = dtunitREQ.Rows[0]["RiskOf"].ToString();
            ViewBag.DevDeatils = dtunitREQ.Rows[0]["DevDetails"].ToString();
            ViewBag.DevUnit = dtunitREQ.Rows[0]["DevUnit"].ToString();
            ViewModelRequest vm = new ViewModelRequest();
            vm.ival_ProjectFloorWiseBreakUps = GetFloorwisebreakbyreqID();
            vm.ival_ProjectApprovalDetails = GetApprovalTypesnew();
            vm.ival_ProjectBoundaries = GetBoundariesByReqID();
            vm.ival_ProjectSideMargins = GetSidearginsByReqID();
            vm.ival_LookupCategoryValuesviewfromunit = GetviewUnits();
            vm.ival_LookupCategoryValuesRiskofdemolition = Getdemolition();
            vm.ival_LookupCategoryValuesdemarcation = GetDemarcation();
            vm.ival_LookupCategoryValuesdemarcation1 = GetDemarcation1();
            vm.ival_LookupCategoryValuesRoadFinish = GetRoadFinish();
            vm.ival_LookupCategoryValuesPropZone = GetPropZone();
            vm.ival_BrokerDetails = GetbrokerBYreqID();
            vm.iva_Comparable_Properties = GetComprabledetreqID();


            vm.ival_LookupCategoryFloorTypes = GetFloorType();
            vm.ival_ApprovalType = GetApprovalType();
            vm.ival_LookupCategoryBoundaries = GetBoundaries();
            vm.ival_LookupCategorySideMargins = GetSideMargins();
            vm.ival_LookupCategoryValuesviewfromunit = GetviewUnits();
            vm.ival_LookupCategoryValuesRiskofdemolition = Getdemolition();
            vm.ival_LookupCategoryValuesdemarcation = GetDemarcation();
            vm.ival_LookupCategoryValuesRoadFinish = GetRoadFinish();
            vm.ival_LookupCategoryValuesPropZone = GetPropZone();
            vm.ival_LookupCategoryLiftTypes = GetLiftType();
            vm.ival_ProjectApprovalDetails = GetApprovalTypesnew();
            vm.ival_ImageMasters = GetImageMaster();
            return View(vm);
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetLiftType()
        {
            List<ival_LookupCategoryValues> ListLiftType = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtLiftType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLiftType");
            for (int i = 0; i < dtLiftType.Rows.Count; i++)
            {

                ListLiftType.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtLiftType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtLiftType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtLiftType.Rows[i]["Lookup_Value"])

                });

            }
            return ListLiftType;
        }
        [HttpPost]
        public List<ival_ImageMaster> GetImageMaster()
        {
            List<ival_ImageMaster> ListIMG = new List<ival_ImageMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtIMG = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImageMasters");
            for (int i = 0; i < dtIMG.Rows.Count; i++)
            {

                ListIMG.Add(new ival_ImageMaster
                {
                    Image_Type_ID = Convert.ToInt32(dtIMG.Rows[i]["Image_Type_ID"]),
                    Image_Type = Convert.ToString(dtIMG.Rows[i]["Image_Type"]),


                });

            }
            return ListIMG;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> Getdemolition()
        {
            List<ival_LookupCategoryValues> ListDemo = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtdemolition = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRiskOfDemolition");
            for (int i = 0; i < dtdemolition.Rows.Count; i++)
            {

                ListDemo.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtdemolition.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtdemolition.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtdemolition.Rows[i]["Lookup_Value"])

                });

            }
            return ListDemo;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetDemarcation()
        {
            List<ival_LookupCategoryValues> ListDemarcation = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtdemarcation = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTypeOfDemarcation");
            for (int i = 0; i < dtdemarcation.Rows.Count; i++)
            {

                ListDemarcation.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtdemarcation.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtdemarcation.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtdemarcation.Rows[i]["Lookup_Value"])

                });

            }
            return ListDemarcation;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetDemarcation1()
        {
            List<ival_LookupCategoryValues> ListDemarcation = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtdemarcation = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTypeOfRiskofDemolition");
            for (int i = 0; i < dtdemarcation.Rows.Count; i++)
            {

                ListDemarcation.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtdemarcation.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtdemarcation.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtdemarcation.Rows[i]["Lookup_Value"])

                });

            }
            return ListDemarcation;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetRoadFinish()
        {
            List<ival_LookupCategoryValues> ListRoad = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtroad = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRoadFinish");
            for (int i = 0; i < dtroad.Rows.Count; i++)
            {

                ListRoad.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtroad.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtroad.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtroad.Rows[i]["Lookup_Value"])

                });

            }
            return ListRoad;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetPropZone()
        {
            List<ival_LookupCategoryValues> Listzone = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtzone = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTypeOfPropertyZone");
            for (int i = 0; i < dtzone.Rows.Count; i++)
            {

                Listzone.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtzone.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtzone.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtzone.Rows[i]["Lookup_Value"])

                });

            }
            return Listzone;
        }


        [HttpPost]
        public List<ival_LookupCategoryValues> GetviewUnits()
        {
            List<ival_LookupCategoryValues> ListView = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtunitview = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetViewUnit");
            for (int i = 0; i < dtunitview.Rows.Count; i++)
            {

                ListView.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtunitview.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtunitview.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtunitview.Rows[i]["Lookup_Value"])

                });

            }
            return ListView;
        }



        [HttpPost]
        public List<ival_LookupCategoryValues> GetSideMargins()
        {
            List<ival_LookupCategoryValues> ListSideMargins = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtSideMargins = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSideMargin");
            for (int i = 0; i < dtSideMargins.Rows.Count; i++)
            {

                ListSideMargins.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtSideMargins.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtSideMargins.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtSideMargins.Rows[i]["Lookup_Value"])

                });

            }
            return ListSideMargins;
        }


        [HttpPost]
        public List<ival_LookupCategoryValues> GetBoundaries()
        {
            List<ival_LookupCategoryValues> ListBoundaries = new List<ival_LookupCategoryValues>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBoundaries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBoundryType");
            for (int i = 0; i < dtBoundaries.Rows.Count; i++)
            {

                ListBoundaries.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtBoundaries.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Value"])

                });

            }
            return ListBoundaries;
        }


        [HttpPost]
        public List<ival_LookupCategoryValues> Getfloring()
        {
            List<ival_LookupCategoryValues> ListBoundaries = new List<ival_LookupCategoryValues>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBoundaries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFlooring");
            for (int i = 0; i < dtBoundaries.Rows.Count; i++)
            {

                ListBoundaries.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtBoundaries.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Value"])

                });

            }
            return ListBoundaries;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> Getwall()
        {
            List<ival_LookupCategoryValues> ListBoundaries = new List<ival_LookupCategoryValues>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBoundaries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_wall");
            for (int i = 0; i < dtBoundaries.Rows.Count; i++)
            {

                ListBoundaries.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtBoundaries.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Value"])

                });

            }
            return ListBoundaries;
        }


        [HttpPost]
        public List<ival_LookupCategoryValues> Getplumbing()
        {
            List<ival_LookupCategoryValues> ListBoundaries = new List<ival_LookupCategoryValues>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBoundaries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_plumbing");
            for (int i = 0; i < dtBoundaries.Rows.Count; i++)
            {

                ListBoundaries.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtBoundaries.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Value"])

                });

            }
            return ListBoundaries;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> Getelect()
        {
            List<ival_LookupCategoryValues> ListBoundaries = new List<ival_LookupCategoryValues>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBoundaries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_elect");
            for (int i = 0; i < dtBoundaries.Rows.Count; i++)
            {

                ListBoundaries.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtBoundaries.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Value"])

                });

            }
            return ListBoundaries;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> Getother()
        {
            List<ival_LookupCategoryValues> ListBoundaries = new List<ival_LookupCategoryValues>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBoundaries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetOtherfixtures");
            for (int i = 0; i < dtBoundaries.Rows.Count; i++)
            {

                ListBoundaries.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtBoundaries.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Value"])

                });

            }
            return ListBoundaries;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> Getextr()
        {
            List<ival_LookupCategoryValues> ListBoundaries = new List<ival_LookupCategoryValues>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBoundaries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Getexterfixtures");
            for (int i = 0; i < dtBoundaries.Rows.Count; i++)
            {

                ListBoundaries.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtBoundaries.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Value"])

                });

            }
            return ListBoundaries;
        }

        [HttpPost]
        public List<ival_ProjectApprovalDetails> GetApprovalTypesnew()
        {
            List<ival_ProjectApprovalDetails> ListApprovalType = new List<ival_ProjectApprovalDetails>();
            hInputPara1 = new Hashtable();
            hInputPara1.Clear();
            sqlDataAccess = new SQLDataAccess();
            CultureInfo provider = CultureInfo.InvariantCulture;
            // DataTable dtREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1RequestID");
            string str = Convert.ToString(Session["ValuationRequestID"]);
            string req = Session["RequestID"].ToString();
            hInputPara1.Add("@Request_ID", req);

            DataTable ValuationRequestID1 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRequestIDNew1", hInputPara1);
            for (int i = 0; i < ValuationRequestID1.Rows.Count; i++)
            {
                string s = ValuationRequestID1.Rows[0]["RequestID"].ToString();
                Session["RequestIDN"] = ValuationRequestID1.Rows[0]["RequestID"].ToString();

                Session["GetInspDate"] = ValuationRequestID1.Rows[0]["Date_of_Inspection"].ToString();
                Session["GetEmpId"] = ValuationRequestID1.Rows[0]["AssignedToEmpID"].ToString();
            }

            hInputPara = new Hashtable();
            hInputPara.Add("@Request_ID", Convert.ToInt32(Session["Request_ID"]));
            //   hInputPara.Add("@Request_ID", 2698);

            DataTable dtApprovalType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDetailsbyReqIDNew", hInputPara);

            for (int i = 0; i < dtApprovalType.Rows.Count; i++)
            {
                if ((dtApprovalType.Rows[i]["Approval_Number"].ToString()) != String.Empty && (dtApprovalType.Rows[i]["Approval_Number"].ToString()) != null)
                {
                    ListApprovalType.Add(new ival_ProjectApprovalDetails
                    {
                        Approval_Type = Convert.ToString(dtApprovalType.Rows[i]["Document_Name"]),
                        Approving_Authority = Convert.ToString(dtApprovalType.Rows[i]["Approval_Authority"]),
                        Date_of_ApprovalNew = Convert.ToString(dtApprovalType.Rows[i]["Date_Of_Approval"]),
                        Approval_Num = Convert.ToString(dtApprovalType.Rows[i]["Approval_Number"]),
                    });
                }
            }
            return ListApprovalType;
        }





        [HttpPost]
        public List<ival_ProjectApprovalDetails> GetApprovalTypesnew1()
        {
            List<ival_ProjectApprovalDetails> ListApprovalType = new List<ival_ProjectApprovalDetails>();

            sqlDataAccess = new SQLDataAccess();
            CultureInfo provider = CultureInfo.InvariantCulture;
            // DataTable dtREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1RequestID");


            hInputPara = new Hashtable();
            hInputPara.Add("@Request_ID", Convert.ToInt32(Session["RequestID"]));
            //   hInputPara.Add("@Request_ID", 2698);

            DataTable dtApprovalType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovedDetailsbyReqIDNew", hInputPara);

            for (int i = 0; i < dtApprovalType.Rows.Count; i++)
            {
                if ((dtApprovalType.Rows[i]["Approval_Number"].ToString()) != String.Empty && (dtApprovalType.Rows[i]["Approval_Number"].ToString()) != null)
                {
                    ListApprovalType.Add(new ival_ProjectApprovalDetails
                    {
                        Approval_Type = Convert.ToString(dtApprovalType.Rows[i]["Document_Name"]),
                        Approving_Authority = Convert.ToString(dtApprovalType.Rows[i]["Approval_Authority"]),
                        Date_of_ApprovalNew = Convert.ToString(dtApprovalType.Rows[i]["Date_Of_Approval"]),
                        Approval_Num = Convert.ToString(dtApprovalType.Rows[i]["Approval_Number"]),
                    });
                }
            }
            return ListApprovalType;
        }








        [HttpPost]
        public List<ival_LookupCategoryValues> GetApprovalType()
        {
            List<ival_LookupCategoryValues> ListApprovalType = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtApprovalType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovalType");
            for (int i = 0; i < dtApprovalType.Rows.Count; i++)
            {

                ListApprovalType.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtApprovalType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtApprovalType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtApprovalType.Rows[i]["Lookup_Value"])

                });

            }
            return ListApprovalType;
        }
        //[HttpPost]
        //public List<ival_UnitParameterMaster> GetUnitDetails(string UnitType)
        //{
        //    List<ival_UnitParameterMaster> listunit = new List<ival_UnitParameterMaster>();
        //    hInputPara = new Hashtable();
        //    sqlDataAccess = new SQLDataAccess();
        //    //string unitname = "Flat";
        //    hInputPara.Add("@UnitType", UnitType);

        //    DataTable dtunits = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitMaster",hInputPara);
        //    for (int i = 0; i < dtunits.Rows.Count; i++)
        //    {
        //        listunit.Add(new ival_UnitParameterMaster
        //        {
        //            UnitParamterID = Convert.ToInt32(dtunits.Rows[i]["UnitParamterID"]),
        //            Name = Convert.ToString(dtunits.Rows[i]["Name"]),

        //        });

        //    }
        //    return listunit;
        //}
        [HttpPost]
        public JsonResult GetUnitDetails(string UnitType)
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_UnitParameterMaster> ProjectUnitLists = new List<ival_UnitParameterMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@UnitType", UnitType);

            DataTable dtunits = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitMaster", hInputPara);
            for (int i = 0; i < dtunits.Rows.Count; i++)
            {


                ProjectUnitLists.Add(new ival_UnitParameterMaster
                {
                    UnitParamterID = Convert.ToInt32(dtunits.Rows[i]["UnitParamterID"]),
                    Name = Convert.ToString(dtunits.Rows[i]["Name"]),
                });

            }
            ViewBag.unitdetails = ProjectUnitLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(ProjectUnitLists);
            return Json(ProjectUnitLists);


        }



        [HttpPost]
        public List<ival_LookupCategoryValues> GetFloorType()
        {
            List<ival_LookupCategoryValues> ListFloorType = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtFloorType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorType");
            for (int i = 0; i < dtFloorType.Rows.Count; i++)
            {

                ListFloorType.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtFloorType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtFloorType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtFloorType.Rows[i]["Lookup_Value"])

                });

            }
            return ListFloorType;
        }



        [HttpPost]
        public ActionResult SaveNewFloor(string str)
        {
            try
            {

                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                for (int i = 0; i < list1.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@FloorName", Convert.ToString(list1.Rows[i]["floorname"]));
                    sqlDataAccess.ExecuteStoreProcedure("usp_InsertFloorTypes", hInputPara);

                }
                return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult SaveAll(string str, string str1, string str2, string str3, string str4, string str5, string str6, string UnitType)
        {
            try
            {

                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                DataTable list2 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));
                DataTable list3 = (DataTable)JsonConvert.DeserializeObject(str2, (typeof(DataTable)));
                DataTable list4 = (DataTable)JsonConvert.DeserializeObject(str3, (typeof(DataTable)));
                DataTable list5 = (DataTable)JsonConvert.DeserializeObject(str4, (typeof(DataTable)));
                DataTable list6 = (DataTable)JsonConvert.DeserializeObject(str5, (typeof(DataTable)));
                DataTable list7 = (DataTable)JsonConvert.DeserializeObject(str6, (typeof(DataTable)));

                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                //DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1Project");
                //Session["Project_ID"] = dttop1project.Rows[0]["Project_ID"].ToString();

                string Request_ID = Session["Request_ID"].ToString();


                hInputPara.Add("@RequestID", Request_ID);
                DataTable req = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1ProjectN", hInputPara);

                string Project_ID = req.Rows[0]["Project_ID"].ToString();

                Session["Project_ID"] = Project_ID;
                hInputPara.Clear();

                //string Request_ID = "2698";
                //string Project_ID = "2589";
                //for (int i = 0; i < list1.Rows.Count; i++)
                //{
                //    hInputPara = new Hashtable();
                //    sqlDataAccess = new SQLDataAccess();

                //    hInputPara.Add("@Project_ID", Convert.ToString(dttop1project.Rows[0]["Project_ID"]));
                //    hInputPara.Add("@Floor_Type", Convert.ToString(list1.Rows[i]["UnitParameters"]));
                //    hInputPara.Add("@Approved_Carpet_Area", Convert.ToString(list1.Rows[i]["carpetarea"]));
                //    hInputPara.Add("@Approved_Built_up_Area", Convert.ToString(list1.Rows[i]["apprbuiltuparea"]));
                //    hInputPara.Add("@Measured_Carpet_Area", Convert.ToString(list1.Rows[i]["measuredcarpet"]));
                //    hInputPara.Add("@Measured_Built_up_Area", Convert.ToString(list1.Rows[i]["meabuiltuparea"]));
                //    hInputPara.Add("@Adopted_Area", Convert.ToString(list1.Rows[i]["adoptedsqft"]));
                //   var res1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertFloorWiseBreakUp", hInputPara);
                //}



                for (int i1 = 0; i1 < list2.Rows.Count; i1++)
                {

                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@Building_ID", 1);
                    hInputPara.Add("@Wing_ID", 1);
                    hInputPara.Add("@Approval_Type", Convert.ToString(list2.Rows[i1]["ApprovalType"]));
                    hInputPara.Add("@Approving_Authority", Convert.ToString(list2.Rows[i1]["Approving_Authority"]));
                    string dateofapprl = Convert.ToString(list2.Rows[i1]["Approving_Date"]);
                    string newOdd = dateofapprl.Replace('-', '/');
                    //DateTime oDate = DateTime.Parse(dateofapprl);
                    DateTime dt = DateTime.ParseExact(newOdd, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    //string dateString = null;
                    ///dateString = "2-12-2015";
                    //CultureInfo provider = CultureInfo.InvariantCulture;
                    // Output: 10/22/2015 12:00:00 AM  
                    //DateTime dateTime16 = DateTime.ParseExact(dateString, new string[] { "MM.dd.yyyy", "MM-dd-yyyy", "MM/dd/yyyy" }, provider, DateTimeStyles.None);
                    //string pattern = "MM-dd-yy";
                    //DateTime parsedDate;
                    //DateTime.TryParseExact(dateofapprl, pattern, null,
                    //               DateTimeStyles.None, out parsedDate);
                    //  DateTime date= Convert.ToDateTime(DateTime.ParseExact(dateofapprl, "MM/dd/yyyy", CultureInfo.InvariantCulture));
                    //Convert.ToDateTime(DateTime.ParseExact("YouDateString", "MM/dd/yyyy", CultureInfo.InvariantCulture));
                    hInputPara.Add("@Date_of_Approval", dt);

                    //hInputPara.Add("@Date_of_Approval", Convert.ToString(list2.Rows[i1]["Approving_Date"]));
                    //DateTime date = DateTime.ParseExact(Convert.ToString(list2.Rows[i1]["Approving_Num"]), "MM/dd/yyyy", null);
                    hInputPara.Add("@Approval_Num", Convert.ToString(list2.Rows[i1]["Approving_Num"]));
                    var res2 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectApprovalDetails", hInputPara);
                    hInputPara.Clear();
                }

                for (int i2 = 0; i2 < list3.Rows.Count; i2++)
                {

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@Building_ID", "");
                    hInputPara.Add("@Wing_ID", "");
                    hInputPara.Add("@Unit_ID", "");
                    hInputPara.Add("@Side_Margin_Description", list3.Rows[i2]["SidemarginsType"].ToString());
                    hInputPara.Add("@As_per_Approvals", list3.Rows[i2]["Asperapprovedplan"].ToString());
                    hInputPara.Add("@As_per_Local_Bye_Laws", list3.Rows[i2]["AsperLocalByLaws"].ToString());
                    hInputPara.Add("@As_Actuals", list3.Rows[i2]["AsActuals"].ToString());
                    hInputPara.Add("@Deviation_No", list3.Rows[i2]["Deviation"].ToString());
                    hInputPara.Add("@Percentage_Deviation", list3.Rows[i2]["perdevistion"].ToString());

                    var res3 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectSideMargins", hInputPara);
                    hInputPara.Clear();
                }


                for (int i3 = 0; i3 < list4.Rows.Count; i3++)
                {


                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@Building_ID", 1);
                    hInputPara.Add("@Wing_ID", 1);
                    hInputPara.Add("@Unit_ID", 1);
                    hInputPara.Add("@Boundry_Name", list4.Rows[i3]["BoundariesType"].ToString());
                    hInputPara.Add("@AsPer_Document", list4.Rows[i3]["asperdoc"].ToString());
                    hInputPara.Add("@AsPer_Site", list4.Rows[i3]["aspersite"].ToString());
                    if (UnitType == "Plot" || UnitType == "Bungalow" || UnitType == "Mall" || UnitType == "Shopping Complex" || UnitType == "Standalone Building")
                    {
                        hInputPara.Add("@Dimension", list4.Rows[i3]["Dimension"].ToString());
                    }
                    var res4 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectBoundaries", hInputPara);
                    hInputPara.Clear();
                }

                for (int i = 0; i < list6.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    //DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1Project");

                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@Deviation_Description", Convert.ToString(list6.Rows[i]["txtdevdesc"]));
                    hInputPara.Add("@Risk_of_Demolition", Convert.ToString(list6.Rows[i]["ddlriskdemolition"]));
                    hInputPara.Add("@Type_of_Flooring", Convert.ToString(list6.Rows[i]["txttypefloor"]));
                    hInputPara.Add("@Wall_Finish", Convert.ToString(list6.Rows[i]["txtwallfinish"]));
                    hInputPara.Add("@Plumbing_Fitting", Convert.ToString(list6.Rows[i]["txtplumbfit"]));
                    hInputPara.Add("@Electrical_Fittings", Convert.ToString(list6.Rows[i]["txtelecfitt"]));
                    hInputPara.Add("@Quality_Fixtures", Convert.ToString(list6.Rows[i]["txtqltyf"]));

                    hInputPara.Add("@Rough_Plaster", Convert.ToString(list6.Rows[i]["txtroughplaster"]));
                    hInputPara.Add("@Tiling", Convert.ToString(list6.Rows[i]["txttiling"]));
                    hInputPara.Add("@Cord_Latitude", Convert.ToString(list6.Rows[i]["txtlatitude"]));
                    hInputPara.Add("@Cord_Longitude", Convert.ToString(list6.Rows[i]["txtlongitude"]));
                    hInputPara.Add("@Road1", Convert.ToString(list6.Rows[i]["txtroad1"]));
                    hInputPara.Add("@Road2", Convert.ToString(list6.Rows[i]["txtroad2"]));
                    hInputPara.Add("@Road3", Convert.ToString(list6.Rows[i]["txtroad3"]));
                    hInputPara.Add("@Road4", Convert.ToString(list6.Rows[i]["txtroad4"]));
                    hInputPara.Add("@Plot_Demarcated_at_site", Convert.ToString(list6.Rows[i]["plotdemosite"]));
                    hInputPara.Add("@Type_of_Demarcation", Convert.ToString(list6.Rows[i]["ddldemarcation"]));
                    hInputPara.Add("@Risk_Demarcation", Convert.ToString(list6.Rows[i]["ddldemarcation1"]));
                    hInputPara.Add("@Constr_As", Convert.ToString(list6.Rows[i]["ddlconstruct"]));
                    hInputPara.Add("@Boundaries_Matching", Convert.ToString(list6.Rows[i]["boundmatc"]));
                    hInputPara.Add("@AreBoundariesremarks", Convert.ToString(list6.Rows[i]["areboundaryremarks"]));
                    hInputPara.Add("@Road_Finish", Convert.ToString(list6.Rows[i]["ddlroadfinish"]));
                    //hInputPara.Add("@Remarks", Convert.ToString(list6.Rows[i]["remarks"]));
                    hInputPara.Add("@SeismicZone", Convert.ToString(list6.Rows[i]["txtseismic"]));
                    hInputPara.Add("@FloodZone", Convert.ToString(list6.Rows[i]["txtflood"]));
                    hInputPara.Add("@CycloneZone", Convert.ToString(list6.Rows[i]["txtcyclone"]));
                    hInputPara.Add("@CRZ", Convert.ToString(list6.Rows[i]["txtCRZ"]));
                    hInputPara.Add("@No_Of_Lifts", Convert.ToString(list6.Rows[i]["txtnumberoflifts"]));//Added By Rupali
                    hInputPara.Add("@Lift_Details", Convert.ToString(list6.Rows[i]["txtliftsdet"]));// Ganesh

                    hInputPara.Add("@PType", Convert.ToString(list6.Rows[i]["PType"]));
                    hInputPara.Add("@UType", Convert.ToString(list6.Rows[i]["UnitType"]));//Added By Rupali
                    hInputPara.Add("@MeasuredApprovedareamatchatsite", Convert.ToString(list6.Rows[i]["ddlmeasuredareamatch"]));
                    hInputPara.Add("@DeviationPercentage", Convert.ToString(list6.Rows[i]["txtDeviation"]));
                    hInputPara.Add("@IsLifts_Present", Convert.ToString(list6.Rows[i]["ddlLifttype"]));
                    hInputPara.Add("@ViewFromUnit", Convert.ToString(list6.Rows[i]["SelectedViewFromUnitDdl"]));

                    var res5 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertRequestUnitMaster", hInputPara);

                    //Hashtable hInputPara22 = new Hashtable();
                    //hInputPara22.Add("@Project_ID", Project_ID);
                    //hInputPara22.Add("@Remarks", string98);
                    //var res2 = sqlDataAccess.ExecuteStoreProcedure("usp_updateRemarks", hInputPara22);
                    hInputPara.Clear();
                    //hInputPara22.Clear();

                }


                for (int i1 = 0; i1 < list5.Rows.Count; i1++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    DataTable dttop1unit = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1UnitID");
                    Session["Unit_ID"] = Convert.ToString(dttop1unit.Rows[0]["Unit_ID"]);
                    hInputPara.Add("@Unit_ID", Convert.ToString(dttop1unit.Rows[0]["Unit_ID"]));

                    hInputPara.Add("@UnitParameter_ID", list5.Rows[i1]["ParamID"].ToString());
                    hInputPara.Add("@UnitParameter_Value", list5.Rows[i1]["Value"].ToString());
                    var res6 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertUnitParametersValue", hInputPara);
                    hInputPara.Clear();
                }



                return Json(new { success = true, Message = "Unit Details Inserted Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetUnitPriceDetails(string UnitType, string PType)
        {
            try
            {
                List<ival_UniPriceParameterMaster> Listunits = new List<ival_UniPriceParameterMaster>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@UnitType", UnitType);
                hInputPara.Add("@PType", PType);

                DataTable dtUniPriceParameter = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitPriceMaster", hInputPara);
                if (dtUniPriceParameter.Rows.Count > 0 && dtUniPriceParameter != null)
                {
                    for (int i = 0; i < dtUniPriceParameter.Rows.Count; i++)
                    {

                        Listunits.Add(new ival_UniPriceParameterMaster
                        {
                            UnitPriceParameterID = Convert.ToInt32(dtUniPriceParameter.Rows[i]["UnitPriceParameterID"]),
                            Name = Convert.ToString(dtUniPriceParameter.Rows[i]["Name"])

                        });

                    }

                }
                var jsonSerialiser = new JavaScriptSerializer();
                var jsonvUnitParameterMaster = jsonSerialiser.Serialize(Listunits);
                return Json(jsonvUnitParameterMaster);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetUnitPriceDetailsOthers(string UnitType, string PType)
        {
            try
            {
                List<ival_UniPriceParameterMaster> Listunits = new List<ival_UniPriceParameterMaster>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@UnitType", UnitType);
                hInputPara.Add("@PType", PType);

                DataTable dtUniPriceParameter = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitPriceMasterOther", hInputPara);
                if (dtUniPriceParameter.Rows.Count > 0 && dtUniPriceParameter != null)
                {
                    for (int i = 0; i < dtUniPriceParameter.Rows.Count; i++)
                    {

                        Listunits.Add(new ival_UniPriceParameterMaster
                        {
                            UnitPriceParameterID = Convert.ToInt32(dtUniPriceParameter.Rows[i]["UnitPriceParameterID"]),
                            Name = Convert.ToString(dtUniPriceParameter.Rows[i]["Name"])

                        });

                    }

                }
                var jsonSerialiser = new JavaScriptSerializer();
                var jsonvUnitParameterMaster = jsonSerialiser.Serialize(Listunits);
                return Json(jsonvUnitParameterMaster);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public void UploadImages()
        {
            string Request_ID = "";

            Request_ID = Session["Request_ID"].ToString();
            string Project_ID = Session["Project_ID"].ToString();
            string Unit_ID = Session["Unit_ID"].ToString();
            if (Request.Files.Count != 0)
            {

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];

                    var ImgName = Path.GetFileName(file.FileName);
                    string fileName = Request_ID + "_" + ImgName;
                    // Find its length and convert it to byte array
                    int ContentLength = file.ContentLength;

                    // Create Byte Array
                    byte[] bytImg = new byte[ContentLength];

                    // Read Uploaded file in Byte Array
                    file.InputStream.Read(bytImg, 0, ContentLength);

                    var path = Path.Combine(Server.MapPath("~/images/"), fileName);
                    file.SaveAs(path);

                    hInputPara = new Hashtable();
                    hInputPara1 = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();


                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@Unit_ID", Unit_ID);
                    hInputPara.Add("@Request_ID", Request_ID);
                    hInputPara.Add("@Uploaded_Image", bytImg);

                    hInputPara.Add("@Image_Path", fileName);

                    var res = sqlDataAccess.ExecuteStoreProcedure("usp_UploadImage", hInputPara);
                    hInputPara.Clear();

                }

            }

        }

        [HttpPost]
        public void SubUploadImages()
        {

            int Request_ID = Convert.ToInt32(Session["SubRequest_ID"]);
            string Project_ID = Session["Project_ID"].ToString();
            string Unit_ID = Session["Unit_ID"].ToString();
            if (Request.Files.Count != 0)
            {

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];

                    var ImgName = Path.GetFileName(file.FileName);
                    string fileName = Request_ID + "_" + ImgName;
                    // Find its length and convert it to byte array
                    int ContentLength = file.ContentLength;

                    // Create Byte Array
                    byte[] bytImg = new byte[ContentLength];

                    // Read Uploaded file in Byte Array
                    file.InputStream.Read(bytImg, 0, ContentLength);

                    var path = Path.Combine(Server.MapPath("~/images/"), fileName);
                    file.SaveAs(path);

                    hInputPara = new Hashtable();
                    hInputPara1 = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();


                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@Unit_ID", Unit_ID);
                    hInputPara.Add("@Request_ID", Request_ID);
                    hInputPara.Add("@Uploaded_Image", bytImg);

                    hInputPara.Add("@Image_Path", fileName);

                    var res = sqlDataAccess.ExecuteStoreProcedure("usp_UploadImage", hInputPara);
                    hInputPara.Clear();

                }

            }

        }

        [HttpPost]
        public void UploadImagesIGR()
        {
            string Request_ID = Session["Request_ID"].ToString();
            string Project_ID = Session["Project_ID"].ToString();
            string Unit_ID = Session["Unit_ID"].ToString();

            //string Request_ID = "470";
            //string Project_ID = "342";
            //string Unit_ID = "258";
            if (Request.Files.Count != 0)
            {

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];

                    var fileName1 = Path.GetFileName(file.FileName);

                    string fileName = Request_ID + "_" + fileName1;
                    // Find its length and convert it to byte array
                    int ContentLength = file.ContentLength;

                    // Create Byte Array
                    byte[] bytImg = new byte[ContentLength];

                    // Read Uploaded file in Byte Array
                    file.InputStream.Read(bytImg, 0, ContentLength);

                    var path = Path.Combine(Server.MapPath("~/images/"), fileName);
                    file.SaveAs(path);

                    hInputPara = new Hashtable();
                    hInputPara1 = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();


                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@Unit_ID", Unit_ID);
                    hInputPara.Add("@Request_ID", Request_ID);
                    hInputPara.Add("@Uploaded_IGR_Image", bytImg);

                    hInputPara.Add("@Image_Path", fileName);

                    var res = sqlDataAccess.ExecuteStoreProcedure("usp_UploadImage", hInputPara);
                    hInputPara.Clear();

                }

            }

        }

        [HttpPost]
        public void UploadImagesIGRLoc()
        {
            string Request_ID = Session["Request_ID"].ToString();
            string Project_ID = Session["Project_ID"].ToString();
            string Unit_ID = Session["Unit_ID"].ToString();

            //string Request_ID = "470";
            //string Project_ID = "342";
            //string Unit_ID = "258";
            if (Request.Files.Count != 0)
            {

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];

                    var fileName1 = Path.GetFileName(file.FileName);

                    string fileName = Request_ID + "_" + fileName1;
                    // Find its length and convert it to byte array
                    int ContentLength = file.ContentLength;

                    // Create Byte Array
                    byte[] bytImg = new byte[ContentLength];

                    // Read Uploaded file in Byte Array
                    file.InputStream.Read(bytImg, 0, ContentLength);

                    var path = Path.Combine(Server.MapPath("~/images/"), fileName);
                    file.SaveAs(path);

                    hInputPara = new Hashtable();
                    hInputPara1 = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();


                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@Unit_ID", Unit_ID);
                    hInputPara.Add("@Request_ID", Request_ID);

                    hInputPara.Add("@Uploaded_IGR_Loc", bytImg);
                    hInputPara.Add("@Image_Path", fileName);

                    var res = sqlDataAccess.ExecuteStoreProcedure("usp_UploadImage", hInputPara);
                    hInputPara.Clear();

                }

            }

        }
        [HttpPost]
        public ActionResult SaveUnitPrice(string str, string str1, string str2, string str3, string str4, string radioValue, string str5, string str6, string str7)
        {
            try
            {
                hInputPara = new Hashtable();
                hInputPara1 = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                // DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1Project");
                // Session["Project_ID"] = dttop1project.Rows[0]["Project_ID"].ToString();
                // //Session["Project_ID"] = 89;

                string Request_ID = Session["Request_ID"].ToString();
                hInputPara.Add("@RequestID", Request_ID);
                DataTable req = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1ProjectN", hInputPara);

                string Project_ID = req.Rows[0]["Project_ID"].ToString();

                Session["Project_ID"] = Project_ID;
                hInputPara.Clear();
                //string Request_ID ="173";
                //string Project_ID = "186";
                // Session["Unit_ID"] = "96";
                hInputPara1.Add("@Project_ID", Project_ID);
                DataTable dtunitid = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitIDbyPRJID", hInputPara1);

                Session["Unit_ID"] = dtunitid.Rows[0]["Unit_ID"].ToString();
                var UnitId = Session["Unit_ID"].ToString();
                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                DataTable list2 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));
                DataTable list3 = (DataTable)JsonConvert.DeserializeObject(str2, (typeof(DataTable)));
                DataTable list4 = (DataTable)JsonConvert.DeserializeObject(str3, (typeof(DataTable)));
                DataTable list5 = (DataTable)JsonConvert.DeserializeObject(str4, (typeof(DataTable)));
                DataTable list6 = (DataTable)JsonConvert.DeserializeObject(str5, (typeof(DataTable)));
                DataTable list7 = (DataTable)JsonConvert.DeserializeObject(str6, (typeof(DataTable)));
                DataTable list8 = (DataTable)JsonConvert.DeserializeObject(str7, (typeof(DataTable)));


                hInputPara1.Clear();
                for (int i = 0; i < list2.Rows.Count; i++)
                {
                    //hInputPara = new Hashtable();
                    //sqlDataAccess = new SQLDataAccess();
                    //DataTable dttopproject = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1Project");
                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                    //hInputPara.Add("@Project_ID",23);
                    //hInputPara.Add("@Unit_ID", 2052);
                    hInputPara.Add("@Project_Name", Convert.ToString(list2.Rows[i]["txtname"].ToString()));
                    hInputPara.Add("@Dist_Subject_Property", Convert.ToString(list2.Rows[i]["txtdist"].ToString()));
                    hInputPara.Add("@Comparable_Unit_Details", Convert.ToString(list2.Rows[i]["txtunitdetails"].ToString()));
                    hInputPara.Add("@Quality_And_Amenities", Convert.ToString(list2.Rows[i]["txtqualituamenities"].ToString()));
                    hInputPara.Add("@Ongoing_Rate", Convert.ToString(list2.Rows[i]["txtongoing"].ToString()));

                    var res = sqlDataAccess.ExecuteStoreProcedure("usp_InsertCompProp", hInputPara);
                    hInputPara.Clear();
                }
                //broker details
                for (int i = 0; i < list1.Rows.Count; i++)
                {
                    if (list1.Rows[i]["txtbrkname"].ToString() != "")
                    {
                        hInputPara.Add("@Project_ID", Project_ID);
                        hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                        //hInputPara.Add("@Project_ID", 23);
                        //hInputPara.Add("@Unit_ID", 2052);
                        hInputPara.Add("@Broker_Name", Convert.ToString(list1.Rows[i]["txtbrkname"].ToString()));
                        hInputPara.Add("@Firm_Name", Convert.ToString(list1.Rows[i]["txtfirm"].ToString()));
                        hInputPara.Add("@Contact_Num", Convert.ToString(list1.Rows[i]["txtctcno"].ToString()));
                        hInputPara.Add("@Rate_Quoted_For_Land", Convert.ToString(list1.Rows[i]["txtrateland"].ToString()));
                        hInputPara.Add("@Rate_Quoted_For_Flat", Convert.ToString(list1.Rows[i]["txtrateflat"].ToString()));

                        var res2 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertBrokerDetails", hInputPara);
                    }
                    hInputPara.Clear();
                }

                for (int i1 = 0; i1 < list4.Rows.Count; i1++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    var UnitParameter_IDVal = list4.Rows[i1]["UnitPriceParameter_ID"];
                    var UnitParameter_Value = list4.Rows[i1]["UnitPriceParameter_Value"];
                    if (UnitParameter_IDVal.ToString() != "" && UnitParameter_Value.ToString() != "")
                    {
                        //hInputPara.Add("@Project_ID", 23);
                        //hInputPara.Add("@Unit_ID", 2052);
                        hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                        hInputPara.Add("@UnitPriceParaValue_ID", Convert.ToInt32(list4.Rows[i1]["UnitPriceParameter_ID"].ToString()));

                        hInputPara.Add("@UnitPriceParameter_Value", list4.Rows[i1]["UnitPriceParameter_Value"].ToString());
                        hInputPara.Add("@UnitPriceSQMt_Value", list4.Rows[i1]["UnitPriceParameterChange_Value"].ToString());
                        var r1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertUnitPriceParametersValue", hInputPara);
                        hInputPara.Clear();
                    }

                }

                for (int i1 = 0; i1 < list6.Rows.Count; i1++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    var UnitParameter_IDVal = list6.Rows[i1]["UnitPriceParameter_ID"];
                    var UnitParameter_Value = list6.Rows[i1]["UnitPriceParameter_Value"];
                    if (UnitParameter_IDVal.ToString() != "" && UnitParameter_Value.ToString() != "")
                    {
                        //hInputPara.Add("@Project_ID", 23);
                        //hInputPara.Add("@Unit_ID", 2052);
                        hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                        hInputPara.Add("@UnitPriceParaValue_ID", Convert.ToInt32(list6.Rows[i1]["UnitPriceParameter_ID"].ToString()));

                        hInputPara.Add("@UnitPriceParameter_Value", list6.Rows[i1]["UnitPriceParameter_Value"].ToString());
                        hInputPara.Add("@UnitPriceSQMt_Value", list6.Rows[i1]["UnitPriceParameterChange_Value"].ToString());
                        var r1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertUnitPriceParametersValue", hInputPara);
                        hInputPara.Clear();
                    }

                }
                for (int i1 = 0; i1 < list3.Rows.Count; i1++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    //hInputPara.Add("@Project_ID", 23);
                    //hInputPara.Add("@Unit_ID", 2052);
                    hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                    hInputPara.Add("@Property_Identified_PName", Convert.ToString(list3.Rows[i1]["propidentity"].ToString()));
                    hInputPara.Add("@Name", list3.Rows[i1]["txtname"].ToString());
                    hInputPara.Add("@P_Contact_Num", list3.Rows[i1]["txtcontno"].ToString());
                    hInputPara.Add("@Society_OR_Building_Name", Convert.ToString(list3.Rows[i1]["society"].ToString()));
                    hInputPara.Add("@CustomerDisplayBoard", list3.Rows[i1]["custdisboard"].ToString());
                    hInputPara.Add("@StageOfConstruction", Convert.ToString(list3.Rows[i1]["ddlstage"].ToString()));
                    hInputPara.Add("@IsInteriorworkcarriedout", list3.Rows[i1]["ddlinterior"].ToString());
                    hInputPara.Add("@DescStageofconstruction", list3.Rows[i1]["descConstruction"].ToString());
                    var r1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertretailPrices", hInputPara);
                    hInputPara.Clear();

                }

                for (int i1 = 0; i1 < list5.Rows.Count; i1++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    //hInputPara.Add("@Project_ID", 23);
                    //hInputPara.Add("@Unit_ID", 2052);["@TotalvaluepropertyBunglowUC"] = "1000000"
                    double Amount1 = 0;
                    double txtvalueoftheplot = 0;
                    hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                    hInputPara.Add("@Total_Value_of_Property", Convert.ToString(list5.Rows[i1]["txttotalvalue"]));
                    hInputPara.Add("@TotalvaluepropertyBunglow", Convert.ToString(list5.Rows[i1]["txttotalbanglowready"]));
                    hInputPara.Add("@TotalvaluepropertyBunglowUC", Convert.ToString(list5.Rows[i1]["txttotvalpropunder"]));
                    hInputPara.Add("@Current_Value_of_Property", Convert.ToString(list5.Rows[i1]["currentValueOfPropertyUnderConstructionUnitId"]));
                    hInputPara.Add("@Current_Value_of_Property_UC", Convert.ToString(list5.Rows[i1]["txtcurrentvalundercons"]));
                    hInputPara.Add("@Value_of_Plot", Convert.ToString(list5.Rows[i1]["txtvalueoftheplot"]));
                    hInputPara.Add("@Valueoftheplotbungalow", Convert.ToString(list5.Rows[i1]["txtvalueofthebunglow"]));
                    hInputPara.Add("@Value_of_other_Plot", Convert.ToString(list5.Rows[i1]["txtvalueoftheotherplot"]));
                    hInputPara.Add("@Valueoftheotherplotbungalow", Convert.ToString(list5.Rows[i1]["txtvalueoftheotherbunglow"]));
                    hInputPara.Add("@Total_Land_Plot_Value", Convert.ToString(list5.Rows[i1]["txttotallandval"]));
                    hInputPara.Add("@Totalofthebungalow", Convert.ToString(list5.Rows[i1]["txtlandvaluation"]));
                    hInputPara.Add("@Distress_Value_of_Plot", Convert.ToString(list5.Rows[i1]["txtdistressval"]));
                    hInputPara.Add("@DistressValueofPropertyBunglow", Convert.ToString(list5.Rows[i1]["txtdistressvalprop"]));
                    hInputPara.Add("@ValuationResult", Convert.ToString(list5.Rows[i1]["ddlvaluation"]));
                    hInputPara.Add("@ValuationResultplot", Convert.ToString(list5.Rows[i1]["ddlvaluationplot"]));
                    hInputPara.Add("@ValuationResultR", Convert.ToString(list5.Rows[i1]["txtvalaresult"]));
                    hInputPara.Add("@ValuationResultUC", Convert.ToString(list5.Rows[i1]["valresultundercons"]));
                    hInputPara.Add("@Cost_of_Interior", Convert.ToString(list5.Rows[i1]["txtcostofinterior"]));
                    hInputPara.Add("@Government_Value", Convert.ToString(list5.Rows[i1]["txtgovtvalue"]));
                    hInputPara.Add("@MeasuredSaleableArea", Convert.ToString(list5.Rows[i1]["txtMeasuredSaleableArea"]));
                    hInputPara.Add("@ApprovedSaleableArea", Convert.ToString(list5.Rows[i1]["txtApprovedSaleableArea"]));
                    hInputPara.Add("@distressValue", Convert.ToString(list5.Rows[i1]["txtdistress"]));
                    hInputPara.Add("@Realizable_Value", Convert.ToString(list5.Rows[i1]["txtrealizable"]));


                    hInputPara.Add("@MatchAt", Convert.ToString(list5.Rows[i1]["MatchAt"]));
                    hInputPara.Add("@DevDetails", Convert.ToString(list5.Rows[i1]["txtdev"]));
                    hInputPara.Add("@DevUnit", Convert.ToString(list5.Rows[i1]["txtdevunit"]));
                    hInputPara.Add("@Riskof", Convert.ToString(list5.Rows[i1]["txtnill"]));




                    hInputPara.Add("@OtherPlotType", radioValue);
                    hInputPara.Add("@Extra_Cost_Valuation", Convert.ToString(list5.Rows[i1]["txtextracost"]));
                    hInputPara.Add("@Work_Completed_Perc", Convert.ToString(list5.Rows[i1]["txtPerworkcompleted"]));
                    hInputPara.Add("@StageOfConstructionBunglow", Convert.ToString(list5.Rows[i1]["ddlstagecons"]));
                    hInputPara.Add("@StageOfConstructionPerct", Convert.ToString(list5.Rows[i1]["txtstgconsperc"]));
                    hInputPara.Add("@DisbursementRecommendPerct", Convert.ToString(list5.Rows[i1]["txtdisbrecom"]));
                    hInputPara.Add("@IsInteriorworkcarriedout", Convert.ToString(list5.Rows[i1]["isinteriorwork"]));
                    hInputPara.Add("@DistressValuePerct", Convert.ToString(list5.Rows[i1]["txtdistressvalperc"]));
                    hInputPara.Add("@Government_Rate", Convert.ToString(list5.Rows[i1]["txtgovtrate"]));
                    hInputPara.Add("@DescStageofconstructionBunglow", Convert.ToString(list5.Rows[i1]["txtdescundercons"]));
                    hInputPara.Add("@Gross_Estimated_Cost_Construction", Convert.ToString(list5.Rows[i1]["txtgrossestimate"]));
                    hInputPara.Add("@Cost_Construction_PerSqFT", Convert.ToString(list5.Rows[i1]["txtcostcons"]));
                    hInputPara.Add("@Does_this_Matches_UnderConstruction", Convert.ToString(list5.Rows[i1]["ddladoptedcostmatch"]));
                    hInputPara.Add("@Does_this_Cost_Remark", Convert.ToString(list5.Rows[i1]["txtremarkundercons"]));
                    hInputPara.Add("@Cost_of_InteriorReady", Convert.ToString(list5.Rows[i1]["txtcostinterior"]));
                    hInputPara.Add("@StageOfConstructionUC", Convert.ToString(list5.Rows[i1]["txtstgconspercUC"]));
                    hInputPara.Add("@DisbursementRecommendPerctUC", Convert.ToString(list5.Rows[i1]["txtdisbrecomUC"]));
                    hInputPara.Add("@TotalFlatwords", Convert.ToString(list5.Rows[i1]["txttotalflatwords"]));
                    hInputPara.Add("@TotalPlotwords", Convert.ToString(list5.Rows[i1]["txttotalplotwords"]));
                    hInputPara.Add("@TotalreadyBunglowWords", Convert.ToString(list5.Rows[i1]["txttotalbungowreadywords"]));
                    hInputPara.Add("@TotalUCBunglowWords", Convert.ToString(list5.Rows[i1]["txttotalbungowUCwords"]));
                    hInputPara.Add("@TotalCostCons", Convert.ToString(list5.Rows[i1]["sum7"]));
                    hInputPara.Add("@Distressvalpercentplot", Convert.ToString(list5.Rows[i1]["txtdistressvalpercentplot"]));
                    //new
                    hInputPara.Add("@MeasuredAreaFt", Convert.ToString(list5.Rows[i1]["txtMeasuredAreaFt"]));
                    hInputPara.Add("@ApprovedAreaF", Convert.ToString(list5.Rows[i1]["txtApprovedAreaFt"]));
                    hInputPara.Add("@AdoptedAValFt", Convert.ToString(list5.Rows[i1]["txtAdoptedAValFt"]));
                    hInputPara.Add("@AdoptedLandRFt", Convert.ToString(list5.Rows[i1]["txtAdoptedLandRFt"]));
                    hInputPara.Add("@MeasuredAreaMt", Convert.ToString(list5.Rows[i1]["txtMeasuredAreaMt"]));
                    hInputPara.Add("@ApprovedAreaMt", Convert.ToString(list5.Rows[i1]["txtApprovedAreaMt"]));
                    hInputPara.Add("@AdoptedAValMt", Convert.ToString(list5.Rows[i1]["txtAdoptedAValMt"]));
                    hInputPara.Add("@AdoptedLandRMt", Convert.ToString(list5.Rows[i1]["txtAdoptedLandRMt"]));

                    hInputPara.Add("@MeasuredOtherPAFt", Convert.ToString(list5.Rows[i1]["txtMeasuredOtherPAFt"]));
                    hInputPara.Add("@ApproveOtherPAFt", Convert.ToString(list5.Rows[i1]["txtApproveOtherPAFt"]));
                    hInputPara.Add("@AdoptedOtherPAValFt", Convert.ToString(list5.Rows[i1]["txtAdoptedOtherPAValFt"]));
                    hInputPara.Add("@AdoptedOtherPLandRFt", Convert.ToString(list5.Rows[i1]["txtAdoptedOtherPLandRFt"]));
                    hInputPara.Add("@MeasuredOtherPAMt", Convert.ToString(list5.Rows[i1]["txtMeasuredOtherPAMt"]));
                    hInputPara.Add("@ApproveOtherPAMt", Convert.ToString(list5.Rows[i1]["txtApproveOtherPAMt"]));
                    hInputPara.Add("@AdoptedOtherPAValMt", Convert.ToString(list5.Rows[i1]["txtAdoptedOtherPAValMt"]));
                    hInputPara.Add("@AdoptedOtherPLandRMt", Convert.ToString(list5.Rows[i1]["txtAdoptedOtherPLandRMt"]));

                    hInputPara.Add("@Permissible", Convert.ToString(list5.Rows[i1]["txtPermissible"]));
                    hInputPara.Add("@LandRateRangeLoc", Convert.ToString(list5.Rows[i1]["txtLandRateRangeLoc"]));
                    hInputPara.Add("@PermissibleOtherplot", Convert.ToString(list5.Rows[i1]["txtPermissibleOtherplot"]));
                    hInputPara.Add("@GovtRateSF", Convert.ToString(list5.Rows[i1]["txtgovtrateftnew"]));
                    hInputPara.Add("@GovtvalueS", Convert.ToString(list5.Rows[i1]["txtgovtratevalue"]));







                    if (Convert.ToString(list5.Rows[i1]["txtrealpercent"]) == "")
                    {
                        hInputPara.Add("@RealPercPlot", Convert.ToString(list5.Rows[i1]["txtrealpercent1"]));
                        hInputPara.Add("@RealizablePlot", Convert.ToString(list5.Rows[i1]["txtrealizablevalue1"]));
                    }
                    else
                    {
                        hInputPara.Add("@RealPercPlot", Convert.ToString(list5.Rows[i1]["txtrealpercent"]));
                        hInputPara.Add("@RealizablePlot", Convert.ToString(list5.Rows[i1]["txtrealizablevalue"]));
                    }
                    var r1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertretailunitTotalsSingleUnit", hInputPara);

                    hInputPara.Clear();






                    Hashtable hInputPara22 = new Hashtable();
                    hInputPara22.Add("@Project_ID", Project_ID);
                    hInputPara22.Add("@Remarks", Convert.ToString(list5.Rows[i1]["remarks"]));
                    var res2 = sqlDataAccess.ExecuteStoreProcedure("usp_updateRemarks", hInputPara22);

                    hInputPara22.Clear();
                }

                for (int i = 0; i < list7.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    hInputPara1 = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();

                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@Floor_Type", Convert.ToString(list7.Rows[i]["UnitParameters"]));
                    hInputPara.Add("@Approved_Carpet_Area", Convert.ToString(list7.Rows[i]["carpetarea"]));
                    hInputPara.Add("@Approved_Built_up_Area", Convert.ToString(list7.Rows[i]["apprbuiltuparea"]));
                    hInputPara.Add("@Measured_Carpet_Area", Convert.ToString(list7.Rows[i]["measuredcarpet"]));
                    hInputPara.Add("@Measured_Built_up_Area", Convert.ToString(list7.Rows[i]["meabuiltuparea"]));
                    hInputPara.Add("@Adopted_Area_for_Valuation", Convert.ToString(list7.Rows[i]["adoptedsqft"]));
                    hInputPara.Add("@Adopted_Cost_Construction", Convert.ToString(list7.Rows[i]["adoptedsqftcost"]));
                    // hInputPara.Add("@Total_Cost_Construction", Convert.ToString(list7.Rows[i]["totalcost"]));
                    var res1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertFloorWiseBreakUp", hInputPara);
                    hInputPara1.Add("@FloorName", Convert.ToString(list7.Rows[i]["UnitParameters"]));
                    var res2 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertFloorTypes", hInputPara1);
                    hInputPara.Clear();
                    hInputPara1.Clear();
                    //Session.Abandon();          
                    //Session.Clear();
                    //Session["Request_ID"] = null;
                    //Session["Unit_ID"] = null;
                    //Session["Project_ID"] = null;
                }
                //upload Images
                for (int i = 0; i < list8.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    if (list8.Rows[i]["Imagepath"].ToString() != string.Empty && list8.Rows[i]["Imagepath"].ToString() != null)
                    {
                        string ImagePath = "";
                        hInputPara.Add("@Project_ID", Project_ID);
                        hInputPara.Add("@Unit_ID", UnitId);
                        hInputPara.Add("@RequestId", Convert.ToInt32(Request_ID));
                        hInputPara.Add("@ImageTypeId", Convert.ToInt32(list8.Rows[i]["ImageTypeId"]));
                        string ImageName = list8.Rows[i]["Imagepath"].ToString();
                        if (ImageName != "")
                        {
                            string[] newValue = ImageName.Split('\\');
                            if (newValue != null)
                            {
                                string ImageTypeId = Convert.ToString(list8.Rows[i]["ImageTypeId"]);
                                string imagesqNO = list8.Rows[i]["ImageSequNo"].ToString();
                                string ImageTypeIdNew = imagesqNO + ImageTypeId;
                                string ImTypeId = ImageTypeIdNew.PadLeft(2, '0');
                                string Name = newValue[newValue.Length - 1];
                                ImagePath = Request_ID + "_" + ImTypeId + "_" + Name;
                            }
                        }
                        hInputPara.Add("@ImagePath", ImagePath);
                        hInputPara.Add("@SequenceNo", Convert.ToInt32(list8.Rows[i]["ImageSequNo"]));
                        var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertImageUploadImagesNew", hInputPara);
                        hInputPara.Clear();


                        //Hashtable hInputPara22 = new Hashtable();
                        //hInputPara22.Add("@Project_ID", Project_ID);
                        //hInputPara22.Add("@Remarks", string98);
                        //var res2 = sqlDataAccess.ExecuteStoreProcedure("usp_updateRemarks", hInputPara22);

                        //hInputPara22.Clear();

                    }

                }

                return Json(new { success = true, Message = "Unit Details Inserted Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult SubSaveUnitPrice(string str, string str1, string str2, string str3, string str4, string radioValue, string str5, string str6, string str7, string str8)
        {
            try
            {
                hInputPara = new Hashtable();
                hInputPara1 = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                // DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1Project");
                // Session["Project_ID"] = dttop1project.Rows[0]["Project_ID"].ToString();
                // //Session["Project_ID"] = 89;

                string Request_ID = Session["Request_ID"].ToString();
                string Project_ID = Session["Project_ID"].ToString();

                //string Request_ID ="173";
                //string Project_ID = "186";
                //Session["Unit_ID"] = "96";
                hInputPara1.Add("@Project_ID", Project_ID);
                DataTable dtunitid = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitIDbyPRJID", hInputPara1);

                Session["Unit_ID"] = dtunitid.Rows[0]["Unit_ID"].ToString();

                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                DataTable list2 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));
                DataTable list3 = (DataTable)JsonConvert.DeserializeObject(str2, (typeof(DataTable)));
                DataTable list4 = (DataTable)JsonConvert.DeserializeObject(str3, (typeof(DataTable)));
                DataTable list5 = (DataTable)JsonConvert.DeserializeObject(str4, (typeof(DataTable)));
                DataTable list6 = (DataTable)JsonConvert.DeserializeObject(str5, (typeof(DataTable)));
                DataTable list7 = (DataTable)JsonConvert.DeserializeObject(str6, (typeof(DataTable)));
                DataTable list8 = (DataTable)JsonConvert.DeserializeObject(str7, (typeof(DataTable)));
                DataTable list9 = (DataTable)JsonConvert.DeserializeObject(str8, (typeof(DataTable)));


                hInputPara1.Clear();
                for (int i = 0; i < list2.Rows.Count; i++)
                {
                    //hInputPara = new Hashtable();
                    //sqlDataAccess = new SQLDataAccess();
                    //DataTable dttopproject = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1Project");
                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                    //hInputPara.Add("@Project_ID",23);
                    //hInputPara.Add("@Unit_ID", 2052);
                    hInputPara.Add("@Project_Name", Convert.ToString(list2.Rows[i]["txtname"].ToString()));
                    hInputPara.Add("@Dist_Subject_Property", Convert.ToString(list2.Rows[i]["txtdist"].ToString()));
                    hInputPara.Add("@Comparable_Unit_Details", Convert.ToString(list2.Rows[i]["txtunitdetails"].ToString()));
                    hInputPara.Add("@Quality_And_Amenities", Convert.ToString(list2.Rows[i]["txtqualituamenities"].ToString()));
                    hInputPara.Add("@Ongoing_Rate", Convert.ToString(list2.Rows[i]["txtongoing"].ToString()));

                    var res = sqlDataAccess.ExecuteStoreProcedure("usp_InsertCompProp", hInputPara);
                    hInputPara.Clear();
                }

                for (int i = 0; i < list1.Rows.Count; i++)
                {
                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                    //hInputPara.Add("@Project_ID", 23);
                    //hInputPara.Add("@Unit_ID", 2052);
                    hInputPara.Add("@Broker_Name", Convert.ToString(list1.Rows[i]["txtbrkname"].ToString()));
                    hInputPara.Add("@Firm_Name", Convert.ToString(list1.Rows[i]["txtfirm"].ToString()));
                    hInputPara.Add("@Contact_Num", Convert.ToString(list1.Rows[i]["txtctcno"].ToString()));
                    hInputPara.Add("@Rate_Quoted_For_Land", Convert.ToString(list1.Rows[i]["txtrateland"].ToString()));
                    hInputPara.Add("@Rate_Quoted_For_Flat", Convert.ToString(list1.Rows[i]["txtrateflat"].ToString()));

                    var res2 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertBrokerDetails", hInputPara);
                    hInputPara.Clear();
                }

                for (int i1 = 0; i1 < list4.Rows.Count; i1++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    var UnitParameter_IDVal = list4.Rows[i1]["UnitPriceParameter_ID"];
                    var UnitParameter_Value = list4.Rows[i1]["UnitPriceParameter_Value"];
                    if (UnitParameter_IDVal.ToString() != "" && UnitParameter_Value.ToString() != "")
                    {
                        //hInputPara.Add("@Project_ID", 23);
                        //hInputPara.Add("@Unit_ID", 2052);
                        hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                        hInputPara.Add("@UnitPriceParaValue_ID", Convert.ToInt32(list4.Rows[i1]["UnitPriceParameter_ID"].ToString()));

                        hInputPara.Add("@UnitPriceParameter_Value", list4.Rows[i1]["UnitPriceParameter_Value"].ToString());
                        var r1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertUnitPriceParametersValue", hInputPara);
                        hInputPara.Clear();
                    }

                }

                for (int i1 = 0; i1 < list6.Rows.Count; i1++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    var UnitParameter_IDVal = list6.Rows[i1]["UnitPriceParameter_ID"];
                    var UnitParameter_Value = list6.Rows[i1]["UnitPriceParameter_Value"];
                    if (UnitParameter_IDVal.ToString() != "" && UnitParameter_Value.ToString() != "")
                    {
                        //hInputPara.Add("@Project_ID", 23);
                        //hInputPara.Add("@Unit_ID", 2052);
                        hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                        hInputPara.Add("@UnitPriceParaValue_ID", Convert.ToInt32(list6.Rows[i1]["UnitPriceParameter_ID"].ToString()));

                        hInputPara.Add("@UnitPriceParameter_Value", list6.Rows[i1]["UnitPriceParameter_Value"].ToString());
                        var r1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertUnitPriceParametersValue", hInputPara);
                        hInputPara.Clear();
                    }

                }
                for (int i1 = 0; i1 < list3.Rows.Count; i1++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    //hInputPara.Add("@Project_ID", 23);
                    //hInputPara.Add("@Unit_ID", 2052);
                    hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                    hInputPara.Add("@Property_Identified_PName", Convert.ToString(list3.Rows[i1]["propidentity"].ToString()));
                    hInputPara.Add("@Name", list3.Rows[i1]["txtname"].ToString());
                    hInputPara.Add("@P_Contact_Num", list3.Rows[i1]["txtcontno"].ToString());
                    hInputPara.Add("@Society_OR_Building_Name", Convert.ToString(list3.Rows[i1]["society"].ToString()));
                    hInputPara.Add("@CustomerDisplayBoard", list3.Rows[i1]["custdisboard"].ToString());
                    hInputPara.Add("@StageOfConstruction", Convert.ToString(list3.Rows[i1]["ddlstage"].ToString()));
                    hInputPara.Add("@IsInteriorworkcarriedout", list3.Rows[i1]["ddlinterior"].ToString());
                    hInputPara.Add("@DescStageofconstruction", list3.Rows[i1]["descConstruction"].ToString());
                    var r1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertretailPrices", hInputPara);
                    hInputPara.Clear();

                }

                for (int i1 = 0; i1 < list5.Rows.Count; i1++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    //hInputPara.Add("@Project_ID", 23);
                    //hInputPara.Add("@Unit_ID", 2052);
                    double Amount1 = 0;
                    double txtvalueoftheplot = 0;
                    hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                    hInputPara.Add("@Total_Value_of_Property", Convert.ToString(list5.Rows[i1]["txttotalvalue"]));
                    hInputPara.Add("@TotalvaluepropertyBunglow", Convert.ToString(list5.Rows[i1]["txttotalbanglowready"]));
                    hInputPara.Add("@TotalvaluepropertyBunglowUC", Convert.ToString(list5.Rows[i1]["txttotvalpropunder"]));
                    hInputPara.Add("@Current_Value_of_Property", Convert.ToString(list5.Rows[i1]["currentValueOfPropertyUnderConstructionUnitId"]));
                    hInputPara.Add("@Current_Value_of_Property_UC", Convert.ToString(list5.Rows[i1]["txtcurrentvalundercons"]));
                    hInputPara.Add("@Value_of_Plot", Convert.ToString(list5.Rows[i1]["txtvalueoftheplot"]));
                    hInputPara.Add("@Valueoftheplotbungalow", Convert.ToString(list5.Rows[i1]["txtvalueofthebunglow"]));
                    hInputPara.Add("@Value_of_other_Plot", Convert.ToString(list5.Rows[i1]["txtvalueoftheotherplot"]));
                    hInputPara.Add("@Valueoftheotherplotbungalow", Convert.ToString(list5.Rows[i1]["txtvalueoftheotherbunglow"]));
                    hInputPara.Add("@Total_Land_Plot_Value", Convert.ToString(list5.Rows[i1]["txttotallandval"]));
                    hInputPara.Add("@Totalofthebungalow", Convert.ToString(list5.Rows[i1]["txtlandvaluation"]));
                    hInputPara.Add("@Distress_Value_of_Plot", Convert.ToString(list5.Rows[i1]["txtdistressval"]));
                    hInputPara.Add("@DistressValueofPropertyBunglow", Convert.ToString(list5.Rows[i1]["txtdistressvalprop"]));
                    //hInputPara.Add("@ValuationResult", Convert.ToString(list5.Rows[i1]["ddlvaluation"]));
                    hInputPara.Add("@ValuationResultplot", Convert.ToString(list5.Rows[i1]["ddlvaluationplot"]));
                    hInputPara.Add("@ValuationResultR", Convert.ToString(list5.Rows[i1]["txtvalaresult"]));
                    hInputPara.Add("@ValuationResultUC", Convert.ToString(list5.Rows[i1]["valresultundercons"]));
                    hInputPara.Add("@Cost_of_Interior", Convert.ToString(list5.Rows[i1]["txtcostofinterior"]));
                    hInputPara.Add("@Government_Value", Convert.ToString(list5.Rows[i1]["txtgovtvalue"]));
                    //hInputPara.Add("@MeasuredSaleableArea", Convert.ToString(list5.Rows[i1]["txtMeasuredSaleableArea"]));
                    //hInputPara.Add("@ApprovedSaleableArea", Convert.ToString(list5.Rows[i1]["txtApprovedSaleableArea"]));
                    hInputPara.Add("@distressValue", Convert.ToString(list5.Rows[i1]["txtdistress"]));
                    hInputPara.Add("@OtherPlotType", radioValue);
                    hInputPara.Add("@Extra_Cost_Valuation", Convert.ToString(list5.Rows[i1]["txtextracost"]));
                    hInputPara.Add("@Work_Completed_Perc", Convert.ToString(list5.Rows[i1]["txtPerworkcompleted"]));
                    hInputPara.Add("@StageOfConstructionBunglow", Convert.ToString(list5.Rows[i1]["ddlstagecons"]));
                    hInputPara.Add("@StageOfConstructionPerct", Convert.ToString(list5.Rows[i1]["txtstgconsperc"]));
                    hInputPara.Add("@DisbursementRecommendPerct", Convert.ToString(list5.Rows[i1]["txtdisbrecom"]));
                    hInputPara.Add("@IsInteriorworkcarriedout", Convert.ToString(list5.Rows[i1]["isinteriorwork"]));
                    hInputPara.Add("@DistressValuePerct", Convert.ToString(list5.Rows[i1]["txtdistressvalperc"]));
                    hInputPara.Add("@Government_Rate", Convert.ToString(list5.Rows[i1]["txtgovtrate"]));
                    hInputPara.Add("@DescStageofconstructionBunglow", Convert.ToString(list5.Rows[i1]["txtdescundercons"]));
                    hInputPara.Add("@Gross_Estimated_Cost_Construction", Convert.ToString(list5.Rows[i1]["txtgrossestimate"]));
                    hInputPara.Add("@Cost_Construction_PerSqFT", Convert.ToString(list5.Rows[i1]["txtcostcons"]));
                    hInputPara.Add("@Does_this_Matches_UnderConstruction", Convert.ToString(list5.Rows[i1]["ddladoptedcostmatch"]));
                    hInputPara.Add("@Does_this_Cost_Remark", Convert.ToString(list5.Rows[i1]["txtremarkundercons"]));
                    hInputPara.Add("@Cost_of_InteriorReady", Convert.ToString(list5.Rows[i1]["txtcostinterior"]));
                    hInputPara.Add("@StageOfConstructionUC", Convert.ToString(list5.Rows[i1]["txtstgconspercUC"]));
                    hInputPara.Add("@DisbursementRecommendPerctUC", Convert.ToString(list5.Rows[i1]["txtdisbrecomUC"]));

                    //hInputPara.Add("@MeasuredAreaFt", Convert.ToString(list5.Rows[i1]["txtMeasuredAreaFt"]));
                    //hInputPara.Add("@ApprovedAreaF", Convert.ToString(list5.Rows[i1]["txtApprovedAreaFt"]));
                    //hInputPara.Add("@AdoptedAValFt", Convert.ToString(list5.Rows[i1]["txtAdoptedAValFt"]));
                    //hInputPara.Add("@AdoptedLandRFt", Convert.ToString(list5.Rows[i1]["txtAdoptedLandRFt"]));
                    //hInputPara.Add("@MeasuredAreaMt", Convert.ToString(list5.Rows[i1]["txtMeasuredAreaMt"]));
                    //hInputPara.Add("@ApprovedAreaMt", Convert.ToString(list5.Rows[i1]["txtApprovedAreaMt"]));
                    //hInputPara.Add("@AdoptedAValMt", Convert.ToString(list5.Rows[i1]["txtAdoptedAValMt"]));
                    //hInputPara.Add("@AdoptedLandRMt", Convert.ToString(list5.Rows[i1]["txtAdoptedLandRMt"]));

                    //hInputPara.Add("@MeasuredOtherPAFt", Convert.ToString(list5.Rows[i1]["txtMeasuredOtherPAFt"]));
                    //hInputPara.Add("@ApproveOtherPAFt", Convert.ToString(list5.Rows[i1]["txtApproveOtherPAFt"]));
                    //hInputPara.Add("@AdoptedOtherPAValFt", Convert.ToString(list5.Rows[i1]["txtAdoptedOtherPAValFt"]));
                    //hInputPara.Add("@AdoptedOtherPLandRFt", Convert.ToString(list5.Rows[i1]["txtAdoptedOtherPLandRFt"]));
                    //hInputPara.Add("@MeasuredOtherPAMt", Convert.ToString(list5.Rows[i1]["txtMeasuredOtherPAMt"]));
                    //hInputPara.Add("@ApproveOtherPAMt", Convert.ToString(list5.Rows[i1]["txtApproveOtherPAMt"]));
                    //hInputPara.Add("@AdoptedOtherPAValMt", Convert.ToString(list5.Rows[i1]["txtAdoptedOtherPAValMt"]));
                    //hInputPara.Add("@AdoptedOtherPLandRMt", Convert.ToString(list5.Rows[i1]["txtAdoptedOtherPLandRMt"]));

                    //hInputPara.Add("@Permissible", Convert.ToString(list5.Rows[i1]["txtPermissible"]));
                    //hInputPara.Add("@LandRateRangeLoc", Convert.ToString(list5.Rows[i1]["txtLandRateRangeLoc"]));
                    //hInputPara.Add("@PermissibleOtherplot", Convert.ToString(list5.Rows[i1]["txtPermissibleOtherplot"]));
                    var r1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertretailunitTotals", hInputPara);

                    hInputPara.Clear();
                }

                for (int i = 0; i < list7.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();

                    hInputPara.Add("@Project_ID", Project_ID);
                    hInputPara.Add("@Floor_Type", Convert.ToString(list7.Rows[i]["UnitParameters"]));
                    hInputPara.Add("@Approved_Carpet_Area", Convert.ToString(list7.Rows[i]["carpetarea"]));
                    hInputPara.Add("@Approved_Built_up_Area", Convert.ToString(list7.Rows[i]["apprbuiltuparea"]));
                    hInputPara.Add("@Measured_Carpet_Area", Convert.ToString(list7.Rows[i]["measuredcarpet"]));
                    hInputPara.Add("@Measured_Built_up_Area", Convert.ToString(list7.Rows[i]["meabuiltuparea"]));
                    hInputPara.Add("@Adopted_Area_for_Valuation", Convert.ToString(list7.Rows[i]["adoptedsqft"]));
                    hInputPara.Add("@Adopted_Cost_Construction", Convert.ToString(list7.Rows[i]["adoptedsqftcost"]));
                    var res1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertFloorWiseBreakUp", hInputPara);
                }

                for (int j = 0; j < list8.Rows.Count; j++)
                {
                    if (list8.Rows.Count > 0)
                    {
                        // var file = Convert.ToString(list8.Rows[j]["image"]);
                        var newfile = "";
                        var fileName = Convert.ToString(list8.Rows[j]["imagename"]);

                        string[] newValue = fileName.Split('_');
                        if (newValue != null)
                        {


                            string Name = newValue[newValue.Length - 1];
                            newfile = Request_ID + "_" + Name;
                        }


                        //string oldpath = "E:/Projects/ivalue_codefirst/I_Value.WebApp/IValue/images/" + fileName;
                        //string newPathAndName = "E:/Projects/ivalue_codefirst/I_Value.WebApp/IValue/images/" + newfile;


                        string oldpath = "E:/Projects/ivalue_codefirst/I_Value.WebApp/IValue/images/" + fileName;
                        string newPathAndName = "E:/Projects/ivalue_codefirst/I_Value.WebApp/IValue/images/" + newfile;
                        System.IO.File.Copy(oldpath, newPathAndName);

                        hInputPara = new Hashtable();
                        hInputPara1 = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara.Add("@Project_ID", Project_ID);
                        hInputPara.Add("@Unit_ID", Session["Unit_ID"]);
                        hInputPara.Add("@Request_ID", Request_ID);
                        // hInputPara.Add("@Uploaded_Image", bytImg);

                        hInputPara.Add("@Image_Path", newfile);

                        var res = sqlDataAccess.ExecuteStoreProcedure("usp_UploadImage", hInputPara);
                        hInputPara.Clear();


                    }
                }

                for (int j = 0; j < list9.Rows.Count; j++)
                {
                    if (list9.Rows.Count > 0)
                    {
                        var newfile = "";
                        var fileName = Convert.ToString(list8.Rows[j]["imagenameigr"]);
                        string[] newValue = fileName.Split('_');
                        if (newValue != null)
                        {


                            string Name = newValue[newValue.Length - 1];
                            newfile = Request_ID + "_" + Name;
                        }

                        string oldpath = "http://brainlines.co.in/IvalueDEV/" + fileName;
                        string newPathAndName = "http://brainlines.co.in/IvalueDEV/" + newfile;

                        //string oldpath = ConfigurationManager.AppSettings["oldpathimg"] + fileName;
                        //string newPathAndName = ConfigurationManager.AppSettings["newPathAndNameimg"] + newfile;

                        System.IO.File.Copy(oldpath, newPathAndName);

                        hInputPara = new Hashtable();
                        hInputPara1 = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara.Add("@Project_ID", Project_ID);
                        hInputPara.Add("@Unit_ID", Session["Unit_ID"]);
                        hInputPara.Add("@Request_ID", Request_ID);
                        // hInputPara.Add("@Uploaded_Image", bytImg);

                        hInputPara.Add("@Image_Path", newfile);

                        var res = sqlDataAccess.ExecuteStoreProcedure("usp_UploadImage", hInputPara);
                        hInputPara.Clear();


                    }
                }

                return Json(new { success = true, Message = "Unit Details Inserted Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Rupali Jadhav For PDF

        public byte[] GetPDF(string pHTML)
        {
            byte[] bPDF = null;

            MemoryStream ms = new MemoryStream();
            TextReader txtReader = new StringReader(pHTML);

            // 1: create object of a itextsharp document class  
            Document doc = new Document(PageSize.A4, 25, 25, 25, 25);

            // 2: we create a itextsharp pdfwriter that listens to the document and directs a XML-stream to a file  
            PdfWriter oPdfWriter = PdfWriter.GetInstance(doc, ms);

            // 3: we create a worker parse the document  
            HTMLWorker htmlWorker = new HTMLWorker(doc);

            // 4: we open document and start the worker on the document  
            doc.Open();
            htmlWorker.StartDocument();


            // 5: parse the html into the document  
            htmlWorker.Parse(txtReader);

            // 6: close the document and the worker  
            htmlWorker.EndDocument();
            htmlWorker.Close();
            doc.Close();

            bPDF = ms.ToArray();

            return bPDF;
        }



        public ActionResult GeneratePdf(int ID)
        {
            // string flag;
            // flag = "FourBoundaries";
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@ID", ID);
            DataTable dtrequest = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetNewReqretailDetailsByRequestid", hInputPara);

            DataTable dtrequestId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicByRequestId", hInputPara);
            DataTable dtReqstIdFourBoundaries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFourBoundariesByRequestId", hInputPara);
            TempData["data"] = dtrequest;

            ViewBag.data = TempData["data"];
            DataTable dt = ViewBag.data;

            string UnitType;
            UnitType = dt.Rows[0]["TypeOfUnit"].ToString();
            string PType = dt.Rows[0]["Property_Type"].ToString();

            // string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();
            string Unit_ID = dt.Rows[0]["UnitNumber"].ToString();


            hInputPara.Add("@PType", PType);
            hInputPara.Add("@UnitType", UnitType);
            //hInputPara.Add("@Unit_ID", Unit_ID);

            DataTable dtpricedetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitpriceDetailsForPdf", hInputPara);

            StringBuilder sbPdfBody = new StringBuilder();

            sbPdfBody.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
            sbPdfBody.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
            sbPdfBody.Append("<body>");

            sbPdfBody.Append("<table width='100%' align='center'cellpadding='0'cellspacing='0'>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td valign='top'>");
            sbPdfBody.Append("<table width='100%' height='311' align='center'cellpadding='10'cellspacing='10'bgcolor:'#FFFFFF'>");

            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='15' colspan ='2' style='background-color:#999999; padding:o; margin:0; line-height:1;'></td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='120' style='background-color:#FFFFFF;padding:o;margin:0; line-height:1;'>");
            sbPdfBody.Append("<div align='center'><img src='" + Server.MapPath("~/images") + "/logo-home.png'/></div>");
            sbPdfBody.Append("</td>");


            sbPdfBody.Append("<td style='background-color:#FFFFFF; margin-top:30px; line-height:1; font-size:18px;color:#000000;'>");
            sbPdfBody.Append("<div align='center'><strong>i-Value Advisroy LLP</strong></div>");
            sbPdfBody.Append("</td>");

            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='15' colspan='2'style='background-color:#999999;padding:o;margin:0;line-height:1;'></td>");

            sbPdfBody.Append("</tr>");


            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='26' colspan='2' style='background-color:#FFFFFF; padding:o; margin:0; line-height:1;'>");
            sbPdfBody.Append("<div align='center'><span style='color:'#000000'; font-size:14px;> Date of valuation Report </ span >< span ></ span > &nbsp; &nbsp; &nbsp;<span style='font-size:14px'> 24 - June, 2020 </span></div>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");

            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='41' colspan='2' style='background-color:#FFFFFF; padding:o; margin:0; line-height:1;font-size:18px; color:#000000;'>");
            sbPdfBody.Append("<div align='center'><strong> STANDARD VALUATION REPORT FORMAT</strong></div>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");


            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='50'colspan='2'style='background-color:#FFFFFF;padding:o;margin:0; line-height:1; font-size:28px; color:#000000;'>");
            sbPdfBody.Append("<table width='90%' border='1' align ='center' height='914' cellpadding ='10' cellspacing ='10' bgcolor ='#FFFFFF' style='margin-bottom:30px;'>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='31'colspan='4' style='color:#000000;font-size:10px;background-color:#dce6f1;'>");
            sbPdfBody.Append("<div align='center'><strong> Address of property</strong></div>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align = 'justify'><strong> Name of the<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/> customer </strong></p>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("<td width ='25%' style ='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='justify'>" + dtrequest.Rows[0]["Customer_Name"].ToString() + "</div></td>");
            sbPdfBody.Append("<td width ='22%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align ='justify'><strong> Site visit Date</strong></div></td>");
            sbPdfBody.Append("<td width = '28%' style='color:#000000; font-size:10px;'>" + dtrequest.Rows[0]["Request_Date"].ToString() + ";</td>");
            //sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");

            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td style='color:#000000; font-size:10px;line-height:1;'>");
            sbPdfBody.Append("<p> &nbsp;</p>");
            sbPdfBody.Append("<p><strong>Customer contact<br/><br/><br/><br/><br/><br/><br/><br/><br/>Details</strong></p>");
            sbPdfBody.Append("<p> &nbsp;</p></td>");

            sbPdfBody.Append("<td style ='color:#000000; font-size:10px;line-height:1;'> " + dtrequest.Rows[0]["Customer_Contact_No_1"].ToString() + "</td>");

            sbPdfBody.Append("<td style ='color:#000000; font-size:10px;line-height:1;'><strong>Report release date</strong></td>");

            sbPdfBody.Append("<td style ='color:#000000; font-size:10px;line-height:1;'>" + dtrequest.Rows[0]["Request_Date"].ToString() + "</td>");

            sbPdfBody.Append("</tr>");

            sbPdfBody.Append("</table>");
            //Personal information
            sbPdfBody.Append("<table width='90%' border='1' align ='center' height='914' cellpadding ='10' cellspacing ='10' bgcolor ='#FFFFFF' style='margin-bottom:30px;'>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='31'colspan='4' style='color:#000000; font-size:10px; background-color:#dce6f1;'>");
            sbPdfBody.Append("<div align='center'><strong> Personal information</strong></div>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align = 'justify'><strong> Application no </strong></p>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("<td width ='25%' style ='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='justify'>" + dtrequest.Rows[0]["Customer_Application_ID"].ToString() + "</div>");
            sbPdfBody.Append("<td width ='22%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align ='justify'><strong> Transaction Type</strong></div></td>");
            sbPdfBody.Append("<td width = '28%' style='color:#000000; font-size:10px;'> ----</td>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");

            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td style='color:#000000; font-size:10px;line-height:1;'>");
            sbPdfBody.Append("<p> &nbsp;</p>");
            sbPdfBody.Append("<p><strong> Name of the<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/> customer</strong></ p >");
            sbPdfBody.Append("<p> &nbsp;</p></td>");

            sbPdfBody.Append("<td style ='color:#000000; font-size:10px;line-height:1;'>" + dtrequest.Rows[0]["Customer_Name"].ToString() + "</td>");

            sbPdfBody.Append("<td style ='color:#000000; font-size:10px;line-height:1;'><strong>Contact No </strong ></td>");

            sbPdfBody.Append("<td style ='color:#000000; font-size:10px;line-height:1;'>" + dtrequest.Rows[0]["Customer_Contact_No_2"].ToString() + "</td>");

            sbPdfBody.Append("</tr>");

            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='40' colspan='3' style='color:#000000;font-size:10px;'>");
            sbPdfBody.Append("<p> &nbsp;</p>");
            sbPdfBody.Append("<p><strong> Person met at site </strong></p>");
            sbPdfBody.Append("<p> &nbsp;</p></td>");
            sbPdfBody.Append("<td height='40' style='color:#000000;font-size:10x;'>" + "Customer Representative" + "</td>");
            sbPdfBody.Append("</tr>");


            sbPdfBody.Append("</table>");
            //Address
            sbPdfBody.Append("<table width='90%' border='1' align ='center' height='914' cellpadding ='10' cellspacing ='10' bgcolor ='#FFFFFF' style='margin-bottom:30px;'>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='31'colspan='4' style='color:#000000; font-size:10px; background-color:#dce6f1;'>");
            sbPdfBody.Append("<div align='center'><strong> Address</strong></div>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align = 'justify'><strong>As per Legal Documents</strong></p>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("<td width ='25%' style ='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='justify'>" + dtrequest.Rows[0]["UnitNumber"].ToString() + "</div>");
            sbPdfBody.Append("<td width ='22%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align ='justify'><strong> " + dtrequest.Rows[0]["Building_Name_RI"].ToString() + "</strong></div></td>");
            sbPdfBody.Append("<td width = '28%' style='color:#000000; font-size:10px;'> " + dtrequest.Rows[0]["City"].ToString() + "</td>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");

            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td style='color:#000000; font-size:10px;line-height:1;'>");
            sbPdfBody.Append("<p> &nbsp;</p>");
            sbPdfBody.Append("<p><strong>As per Actual at Site</strong></p ></td>");

            sbPdfBody.Append("<td style ='color:#000000; font-size:10px;line-height:1;'>" + dtrequest.Rows[0]["UnitNumber"].ToString() + "</td>");

            sbPdfBody.Append("<td style ='color:#000000; font-size:10px;line-height:1;'><strong>" + dtrequest.Rows[0]["Building_Name_RI"].ToString() + "</strong ></td>");

            sbPdfBody.Append("<td style ='color:#000000; font-size:10px;line-height:1;'>" + dtrequest.Rows[0]["City"].ToString() + "</td>");

            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("</table>");//address of table end.

            //Location & Property Details (As per site visit)
            sbPdfBody.Append("<table width='90%' border='1' align ='center' height='914' cellpadding ='10' cellspacing ='10' bgcolor ='#FFFFFF' style='margin-bottom:30px;'>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='31'colspan='4' style='color:#000000; font-size:10px; background-color:#dce6f1;'>");
            sbPdfBody.Append("<div align='center'><strong> Location & Property Details (As per site visit)</strong></div>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align = 'justify'><strong>Status of Holding</strong></p>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("<td width ='25%' style ='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='justify'>" + dtrequest.Rows[0]["StatusOfProperty"].ToString() + "</div>");
            sbPdfBody.Append("<td width ='22%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align ='justify'><strong>Delivery Agency </strong></div></td>");
            sbPdfBody.Append("<td width = '28%' style='color:#000000; font-size:10px;'> " + "Ivalue Advisory LLP" + "</td>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");

            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td style='color:#000000; font-size:10px;line-height:1;'>");
            sbPdfBody.Append("<p> &nbsp;</p>");
            sbPdfBody.Append("<p><strong>Type of Property</strong></ p >");
            //sbPdfBody.Append("<p> " + dtrequest.Rows[0]["Property_Type"].ToString() + "</p></td>");

            sbPdfBody.Append("<td style ='color:#000000; font-size:10px;line-height:1;'> " + dtrequest.Rows[0]["Property_Type"].ToString() + "</td>");

            sbPdfBody.Append("<td style ='color:#000000; font-size:10px;line-height:1;'><strong>Name of the State</strong ></td>");

            sbPdfBody.Append("<td style ='color:#000000; font-size:10px;line-height:1;'>" + dtrequest.Rows[0]["State"].ToString() + "</td>");

            sbPdfBody.Append("</tr>");


            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align = 'justify'><strong>Street on which Property is Located </strong></p>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("<td width ='25%' style ='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='justify'>" + dtrequest.Rows[0]["Street"].ToString() + "</div>");
            sbPdfBody.Append("<td width ='22%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align ='justify'><strong>Occupation Status </strong></div></td>");
            sbPdfBody.Append("<td width = '28%' style='color:#000000; font-size:10px;'> " + dtrequest.Rows[0]["Occupancy_Details"].ToString() + "</td>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");

            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align = 'justify'><strong>Main Locality</strong></p>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("<td width ='25%' style ='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='justify'>" + dtrequest.Rows[0]["Locality"].ToString() + "</div>");
            sbPdfBody.Append("<td width ='22%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align ='justify'><strong>Sub Locality</strong></div></td>");
            sbPdfBody.Append("<td width = '28%' style='color:#000000; font-size:10px;'> " + dtrequest.Rows[0]["Sub_Locality"].ToString() + "</td>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");

            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align = 'justify'><strong>Pin Code</strong></p>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("<td width ='25%' style ='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='justify'>" + dtrequest.Rows[0]["PinCode"].ToString() + "</div>");
            sbPdfBody.Append("<td width ='22%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align ='justify'><strong>Nearest Landmark </strong></div></td>");
            sbPdfBody.Append("<td width = '28%' style='color:#000000; font-size:10px;'> " + dtrequest.Rows[0]["NearBy_Landmark"].ToString() + "</td>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");


            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align = 'justify'><strong>Type of Locality as per Latest DP </strong></p>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("<td width ='25%' style ='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='justify'>" + dtrequest.Rows[0]["Type_of_Locality"].ToString() + "</div></td>");
            sbPdfBody.Append("<td width ='22%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align ='justify'><strong>Property Usage </strong></div></td>");
            sbPdfBody.Append("<td width = '28%' style='color:#000000; font-size:10px;'> " + dtrequest.Rows[0]["Actual_Usage_Of_Property"].ToString() + "</td>");
            sbPdfBody.Append("</tr>");

            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align = 'justify'><strong>Is property Identifiable</strong></p>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("<td width ='25%' style ='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='justify'>" + "YES" + "</div>");
            sbPdfBody.Append("<td width ='22%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align ='justify'><strong>Is Property Demarcated Separately </strong></div></td>");
            sbPdfBody.Append("<td width = '28%' style='color:#000000; font-size:10px;'> " + dtrequest.Rows[0]["Plot_Demarcated_at_site"].ToString() + "</td>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align = 'justify'><strong>Identified Through</strong></p>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("<td width ='25%' style ='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='justify'>" + dtrequest.Rows[0]["Property_Identified_PName"].ToString() + " </div>");
            sbPdfBody.Append("<td width ='22%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align ='justify'><strong>Name of city/Town</strong></div></td>");
            sbPdfBody.Append("<td width = '28%' style='color:#000000; font-size:10px;'> " + dtrequest.Rows[0]["City"].ToString() + "</td>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align = 'justify'><strong>Roof Construction</strong></p>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("<td width ='25%' style ='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='justify'>" + dtrequest.Rows[0]["Roofing_Terracing"].ToString() + " </div>");
            sbPdfBody.Append("<td width ='22%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align ='justify'><strong>Type of Structure</strong></div></td>");
            sbPdfBody.Append("<td width = '28%' style='color:#000000; font-size:10px;'> " + dtrequest.Rows[0]["Type_Of_Structure"].ToString() + "</td>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align = 'justify'><strong>No. of floors in the bungalow </strong></p>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("<td width ='25%' style ='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='justify'>" + dtrequest.Rows[0]["Approved_No_of_Floors"].ToString() + "</div>");
            sbPdfBody.Append("<td width ='22%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align ='justify'><strong>Located on floor no.</strong></div></td>");
            sbPdfBody.Append("<td width = '28%' style='color:#000000; font-size:10px;'>" + dtrequest.Rows[0]["Floor_Of_Unit"].ToString() + "</td>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");
            /////
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align = 'justify'><strong>Units in Building</strong></p>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("<td width ='25%' style ='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='justify'>" + dtrequest.Rows[0]["Approved_No_of_Units"].ToString() + "</div>");
            sbPdfBody.Append("<td width ='22%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align ='justify'><strong>Quality of Construction </strong></div></td>");
            sbPdfBody.Append("<td width = '28%' style='color:#000000; font-size:10px;'> " + dtrequest.Rows[0]["Quality_Of_Construction"].ToString() + "</td>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align = 'justify'><strong>External Finish</strong></p>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("<td width ='25%' style ='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='justify'>" + "Rough Cement Plaster" + "</div>");
            sbPdfBody.Append("<td width ='22%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align ='justify'><strong>Type of Flooring </strong></div></td>");
            sbPdfBody.Append("<td width = '28%' style='color:#000000; font-size:10px;'>" + dtrequest.Rows[0]["Type_of_Flooring"].ToString() + " </td>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align = 'justify'><strong>Age of Property</strong></p>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("<td width ='25%' style ='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='justify'>" + dtrequest.Rows[0]["Current_Age_Of_Property"].ToString() + "</div>");
            sbPdfBody.Append("<td width ='22%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align ='justify'><strong>Life of Property(in future) </strong></div></td>");
            sbPdfBody.Append("<td width = '28%' style='color:#000000; font-size:10px;'>" + dtrequest.Rows[0]["Residual_Age_Of_Property"].ToString() + "</td>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align = 'justify'><strong>Latitude</strong></p>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("<td width ='25%' style ='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='justify'>" + dtrequest.Rows[0]["Cord_Latitude"].ToString() + "</div>");
            sbPdfBody.Append("<td width ='22%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align ='justify'><strong>Longitude </strong></div></td>");
            sbPdfBody.Append("<td width = '28%' style='color:#000000; font-size:10px;'> " + dtrequest.Rows[0]["Cord_Longitude"].ToString() + "</td>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");

            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='40' colspan='3' style='color:#000000;font-size:10px;'>");
            sbPdfBody.Append("<p> &nbsp;</p>");
            sbPdfBody.Append("<p><strong> Extra Amenities Available</strong></p>");
            sbPdfBody.Append("<p> &nbsp;</p></td>");
            sbPdfBody.Append("<td height='40' style='color:#000000;font-size:10x;'>" + dtrequest.Rows[0]["Amenities"].ToString() + "</td>");
            sbPdfBody.Append("</tr>");


            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='31'colspan='4' style='color:#000000;font-size:10px;background-color:#dce6f1;'>");
            sbPdfBody.Append("<div align='center'><strong>Zone/Location/Surrounding/Civic Amenities</strong></div>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align = 'justify'><strong>Infrastructure in the Surrounding</strong></p>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("<td width ='25%' style ='color:#000000; font-size:10px;'>");
            for (int u = 0; u < dtrequestId.Rows.Count; u++)
            {
                sbPdfBody.Append("<div align='justify'>" + dtrequestId.Rows[u]["BasicInfra_type"].ToString() + "</div>");
            }
            sbPdfBody.Append("<td width ='22%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align ='justify'><strong>Class of Locality</strong></div></td>");
            sbPdfBody.Append("<td width = '28%' style='color:#000000; font-size:10px;'> " + dtrequest.Rows[0]["Class_of_Locality"].ToString() + "</td>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");

            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td style='color:#000000; font-size:10px;line-height:1;'>");
            sbPdfBody.Append("<p> &nbsp;</p>");
            sbPdfBody.Append("<p><strong>Type of Road</strong></ p >");
            sbPdfBody.Append("<p> &nbsp;</p></td>");

            sbPdfBody.Append("<td style ='color:#000000; font-size:10px;line-height:1;'>" + dtrequest.Rows[0]["Road_Finish"].ToString() + " </td>");

            sbPdfBody.Append("<td style ='color:#000000; font-size:10px;line-height:1;'><strong>Width of Road</strong></td>");

            sbPdfBody.Append("<td style ='color:#000000; font-size:10px;line-height:1;'>" + dtrequest.Rows[0]["Road1"].ToString() + " </td>");

            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='31'colspan='4' style='color:#000000;font-size:10px;background-color:#dce6f1;'>");
            sbPdfBody.Append("<div align='center'><strong>Connectivity(landmark details)</strong></div>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align = 'justify'><strong>Distance from Local Bus Stand</strong></p>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("<td width ='25%' style ='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='justify'>" + dtrequest.Rows[0]["Nearest_BusStop"].ToString() + "</div>");
            sbPdfBody.Append("<td width ='22%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align ='justify'><strong>Distance from Railway station</strong></div></td>");
            sbPdfBody.Append("<td width = '28%' style='color:#000000; font-size:10px;'> " + dtrequest.Rows[0]["Nearest_RailwayStation"].ToString() + "</td>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");

            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td style='color:#000000; font-size:10px;line-height:1;'>");
            sbPdfBody.Append("<p> &nbsp;</p>");
            sbPdfBody.Append("<p><strong>Distance from Main<br/><br/><br/><br/> <br/><br/><br/><br/><br/><br/><br/>Market</strong></ p >");
            sbPdfBody.Append("<p> &nbsp;</p></td>");

            sbPdfBody.Append("<td style ='color:#000000; font-size:10px;line-height:1;'> " + dtrequest.Rows[0]["Nearest_Market"].ToString() + "</td>");

            sbPdfBody.Append("<td style ='color:#000000; font-size:10px;line-height:1;'><strong>Distance from<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>Hospital</strong></td>");

            sbPdfBody.Append("<td style ='color:#000000; font-size:10px;line-height:1;'>" + dtrequest.Rows[0]["Nearest_Hospital"].ToString() + "</td>");

            sbPdfBody.Append("</tr>");

            sbPdfBody.Append("</table><br/><br/><br/><br/><br/><br/>");
            //4june2020
            sbPdfBody.Append("<table width='90%' border='1' align ='center' height='914' cellpadding ='10' cellspacing ='10' bgcolor ='#FFFFFF' style='margin-bottom:30px;'>");

            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='24' colspan='5' style='color:#000000; font-size:10px; background-color:#dce6f1;'>");
            sbPdfBody.Append("<p align='center'>&nbsp;</p>");
            sbPdfBody.Append(" <p align='center'><strong>Four Boundaries of </strong><strong>the Property</strong></p></td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='24' colspan='3' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align='center'>&nbsp;</p>");
            sbPdfBody.Append("<p align='center'>&nbsp;</p>");
            sbPdfBody.Append("<p align='center'>&nbsp;</p>");

            sbPdfBody.Append(" <p align='center'><strong>Four Boundaries </strong></p></td>");
            sbPdfBody.Append("<td width='20%' colspan='-1' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align='center'><strong>As Per</strong></p>");
            sbPdfBody.Append("<p align='center'>&nbsp;</p>");
            sbPdfBody.Append("<p align='center'><strong> Documents</strong></p></td>");
            sbPdfBody.Append("<td width='20%' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align='center'><strong>Actual as</strong></p>");
            sbPdfBody.Append("<p align='center'><strong>Actual as</strong></p>");
            sbPdfBody.Append("<p align='center'><strong>per </strong><strong>Site</strong></p></td>");
            sbPdfBody.Append(" </tr>");

            for (int i = 0; i < dtReqstIdFourBoundaries.Rows.Count; i++)
            {
                sbPdfBody.Append("<tr>");

                string Boundaries = dtReqstIdFourBoundaries.Rows[i]["Boundry_Name"].ToString();


                sbPdfBody.Append("<td height='40' colspan='3' style='color:#000000; font-size:10px;'>");

                sbPdfBody.Append(" <div align='center'><strong>" + dtReqstIdFourBoundaries.Rows[i]["Boundry_Name"].ToString() + "</strong></div></td>");

                sbPdfBody.Append(" <td colspan='-1' style='color:#000000; font-size:10px;'>" + dtReqstIdFourBoundaries.Rows[i]["AsPer_Document"].ToString() + "</td>");

                sbPdfBody.Append("<td style='color:#000000; font-size:10px;'>" + dtReqstIdFourBoundaries.Rows[i]["AsPer_Site"].ToString() + " </td>");
                sbPdfBody.Append("</tr>");

            }

            sbPdfBody.Append("</table>");
            //Side Margin.
            sbPdfBody.Append("<table width='90%' border='1' align ='center' height='914' cellpadding ='10' cellspacing ='10' bgcolor ='#FFFFFF' style='margin-bottom:30px;'>");

            sbPdfBody.Append(" <tr>");
            sbPdfBody.Append("<td height='40' colspan='5' style='color:#000000; font-size:10px; background-color:#dce6f1;'>");
            sbPdfBody.Append("<div align='center'><strong>Side Margins</strong></div></td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append(" <tr>");
            sbPdfBody.Append("<td width='20%' height='40' style='color:#000000; font-size:10px;'><strong>Side Margins</strong></td>");
            sbPdfBody.Append(" <td width='20%' style='color:#000000; font-size:10px;'><strong>Front Margin</strong></td>");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong>Rear Margin</strong></td>");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong>Left Side</strong></td>");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'><strong>Right Side</strong></td>");
            sbPdfBody.Append("</tr>");
            //sbPdfBody.Append(" <tr>");
            //sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'><strong>As Per Approvals</strong></td>");
            //sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>Approved plan</td>");
            //sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'>Plan Approved</td>");
            //sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'>Plan Approved</td>");
            //sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>Plan approved</td>");
            //sbPdfBody.Append("</tr>");
            sbPdfBody.Append(" <tr>");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'><strong></strong></td>");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'></td>");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'></td>");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'></td>");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'></td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append(" <tr>");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p><strong>As per  Local  Bye Laws </strong></p> </td> ");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td>");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td>");
            sbPdfBody.Append("  <td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td>");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td> ");
            sbPdfBody.Append("</tr> ");
            sbPdfBody.Append(" <tr>");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'><strong>As Actuals</strong></td>");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td>");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td>");
            sbPdfBody.Append("  <td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td>");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td> ");
            sbPdfBody.Append("</tr> ");

            sbPdfBody.Append(" <tr>");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p><strong>Deviation</strong></p> </td> ");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td>");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td>");
            sbPdfBody.Append("  <td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td>");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td> ");
            sbPdfBody.Append("</tr> ");

            sbPdfBody.Append(" <tr>");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p><strong>Deviation %</strong></p> </td> ");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td>");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td>");
            sbPdfBody.Append("  <td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td>");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td> ");
            sbPdfBody.Append("</tr> ");
            sbPdfBody.Append("</table>");
            //Area and accommodation Details (Sq. Ft.)

            sbPdfBody.Append("<table width='90%' border='1' align ='center' height='914' cellpadding ='10' cellspacing ='10' bgcolor ='#FFFFFF' style='margin-bottom:30px;'>");
            sbPdfBody.Append("<tr> ");
            sbPdfBody.Append("<td height='40' colspan='4' style='color:#000000; font-size:10px; background-color:#dce6f1;'>");
            sbPdfBody.Append("<p align='center'>&nbsp;</p> ");
            sbPdfBody.Append(" <p align='center'><strong>Area and accommodation Details (Sq. Ft.)</strong></p></td> ");
            sbPdfBody.Append(" </tr>");
            sbPdfBody.Append(" <tr> ");
            sbPdfBody.Append("<td width='25%' height='40' style='color:#000000; font-size:10px;'><strong>Flat No..</strong></td> ");
            sbPdfBody.Append(" <td width='25%' style='color:#000000; font-size:10px;'> ");
            sbPdfBody.Append(" <p><strong>Measured Built up  Area </strong></p> ");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'> ");
            sbPdfBody.Append("<p><strong>Agreement Built up Area </strong></p></td> ");
            sbPdfBody.Append(" <td width='25%' style='color:#000000; font-size:10px;'> ");
            sbPdfBody.Append("<p><strong> Adopted area for valuation (35% Loading over Carpet area)</strong></p></td> ");
            sbPdfBody.Append(" </tr>");

            sbPdfBody.Append(" <tr> ");
            sbPdfBody.Append("<td width='25%' height='40' style='color:#000000; font-size:10px;'><strong>" + Unit_ID.ToString() + "</strong></td> ");
            sbPdfBody.Append(" <td width='25%' style='color:#000000; font-size:10px;'> ");
            sbPdfBody.Append(" <p><strong> #Measured_Built-up_Area# </strong></p> </td>");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'> #Area_as_per_Agreement# </td>");
            //sbPdfBody.Append("<p><strong> #Measured_Carpet_Area# </strong></p></td> ");
            sbPdfBody.Append("<td width='25%' style='color:#000000; font-size:10px;'> #Adopted_Area_for_Valuation# </td>");
            //sbPdfBody.Append("<p><strong> #Government_Rate# </strong></p></td> ");
            sbPdfBody.Append(" </tr>");


            sbPdfBody.Append(" <tr>");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td> ");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td> ");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td> ");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td> ");
            sbPdfBody.Append(" </tr> ");


            sbPdfBody.Append("</table><br/><br/><br/><br/><br/><br/><br/><br/>");

            sbPdfBody.Append("<table width='90%' border='1' align ='center' height='914' cellpadding ='10' cellspacing ='10' bgcolor ='#FFFFFF' style='margin-bottom:30px;'>");

            sbPdfBody.Append(" <tr> ");
            sbPdfBody.Append(" <td height='40' colspan='4' style='color:#000000; font-size:10px; background-color:#dce6f1;'> ");
            sbPdfBody.Append("<div align='center'><strong>Property Valuation - Market Value</strong></div></td>");
            sbPdfBody.Append(" </tr> ");

            sbPdfBody.Append("<tr>");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'><strong>Plot Area (in Sq. Ft.)</strong></td> ");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong> #Measured_Carpet_Area# </ strong></td> ");


            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><p><strong>Rate Range in the locality  (Rs. per Sq. Ft.)</strong></p></td>");
            //sbPdfBody.Append(" <p><strong>Rate Range in the locality  (Rs. per Sq. Ft.)</strong></p></td>  ");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong> #Measured_Saleable_Area# </ strong></td> ");


            sbPdfBody.Append("</tr> ");
            sbPdfBody.Append("<tr> ");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'> ");
            sbPdfBody.Append(" <p><strong>Rate Considered (Rs per Sq. Ft.)</strong></p></td> ");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong> #Adopted_Area_for_Valuation# </ strong></td> ");



            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append(" <p><strong>Total Market Value of  Land (in RS)</strong></p></td>");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong> #Rate_on_carpet_area# </ strong></td> ");


            sbPdfBody.Append("</tr> ");
            sbPdfBody.Append(" <tr> ");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'> ");
            sbPdfBody.Append(" <p><strong>Adopted built up area  (in  Sq. Ft.)</strong></p> </td> ");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong> #Rate_on_saleable_area# </ strong></td> ");

            //for (int x = 0; x < dtpricedetails.Rows.Count; x++)
            //{
            //    string name = dtpricedetails.Rows[x]["Name"].ToString();

            //    if (name == "Rate on saleable area")
            //    {
            //        sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong>" + dtpricedetails.Rows[x]["UnitPriceParameter_Value"].ToString() + "</strong></td> ");

            //    }
            //}
            //sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td>");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'> ");
            sbPdfBody.Append(" <p><strong>Rate considered for  construction (Rs per  Sq.Ft)</strong></p></td> ");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong> #PLC_Charges# </ strong></td> ");

            //for (int x = 0; x < dtpricedetails.Rows.Count; x++)
            //{
            //    string name = dtpricedetails.Rows[x]["Name"].ToString();

            //    if (name == "PLC Charges")
            //    {
            //        sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong>" + dtpricedetails.Rows[x]["UnitPriceParameter_Value"].ToString() + "</strong></td> ");

            //    }
            //}
            //sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'>484.38</td> ");
            sbPdfBody.Append("</tr> ");
            sbPdfBody.Append("  <tr> ");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>");

            sbPdfBody.Append(" <p><strong>Total Value built up  (in RS)</strong></p></td> ");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong> #Floor_Rise_Charges# </ strong></td> ");

            //for (int x = 0; x < dtpricedetails.Rows.Count; x++)
            //{
            //    string name = dtpricedetails.Rows[x]["Name"].ToString();

            //    if (name == "Floor Rise Charges")
            //    {
            //        sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong>" + dtpricedetails.Rows[x]["UnitPriceParameter_Value"].ToString() + "</strong></td> ");

            //    }
            //}
            //sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td> ");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append(" <p><strong>Total Value of  property (in Rs)</strong></p></td>");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong> #Extra_Amenities# </ strong></td> ");


            //for (int x = 0; x < dtpricedetails.Rows.Count; x++)
            //{
            //    string name = dtpricedetails.Rows[x]["Name"].ToString();

            //    if (name == "Extra Amenities")
            //    {
            //        sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong>" + dtpricedetails.Rows[x]["UnitPriceParameter_Value"].ToString() + "</strong></td> ");

            //    }
            //}
            //sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>452.09</td> ");
            sbPdfBody.Append(" </tr> ");
            sbPdfBody.Append("<tr> ");
            sbPdfBody.Append("<td height='40' colspan='3' style='color:#000000; font-size:10px;'><strong>In Words</strong></td> ");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong> #Parking_Cost# </ strong></td> ");

            //for (int x = 0; x < dtpricedetails.Rows.Count; x++)
            //{
            //    string name = dtpricedetails.Rows[x]["Name"].ToString();

            //    if (name == "Parking Cost")
            //    {
            //        sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong>" + dtpricedetails.Rows[x]["UnitPriceParameter_Value"].ToString() + "</strong></td> ");

            //    }
            //}
            //sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'>129.17</td>");
            sbPdfBody.Append(" </tr>");
            sbPdfBody.Append("  <tr> ");
            sbPdfBody.Append(" <td height='40' colspan='4' style='color:#000000; font-size:10px; background-color:#dce6f1;'> ");
            sbPdfBody.Append("<div align='center'><strong>Distress Sale Value</strong></div> ");
            sbPdfBody.Append("</td> ");
            sbPdfBody.Append(" </tr> ");
            sbPdfBody.Append(" <tr> ");
            sbPdfBody.Append(" <td height='40' colspan='3' style='color:#000000; font-size:10px;'><strong>80 % of Market Value </strong></td> ");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong> #Electrical/water_connection_chagres# </ strong></td> ");

            //for (int x = 0; x < dtpricedetails.Rows.Count; x++)
            //{
            //    string name = dtpricedetails.Rows[x]["Name"].ToString();

            //    if (name == "Electrical/water connection chagres")
            //    {
            //        sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong>" + dtpricedetails.Rows[x]["UnitPriceParameter_Value"].ToString() + "</strong></td> ");

            //    }
            //}
            //sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'>441.32</td> ");
            sbPdfBody.Append(" </tr>");
            sbPdfBody.Append("  <tr> ");
            sbPdfBody.Append(" <td height='40' colspan='4' style='color:#000000; font-size:10px; background-color:#dce6f1;'>");
            sbPdfBody.Append("<div align='center'><strong>Satge Of Construction</strong></div></td> ");
            sbPdfBody.Append(" </tr> ");
            sbPdfBody.Append(" <tr> ");
            sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'><strong>% Completion</strong></td> ");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong> #Maintenance_Cost# </ strong></td> ");

            //for (int x = 0; x < dtpricedetails.Rows.Count; x++)
            //{
            //    string name = dtpricedetails.Rows[x]["Name"].ToString();

            //    if (name == "Maintenance Cost")
            //    {
            //        sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong>" + dtpricedetails.Rows[x]["UnitPriceParameter_Value"].ToString() + "</strong></td> ");

            //    }
            //}
            //sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>441.32</td> ");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong>% Recommendation</strong></td>");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong> #Transfer_Charges_if_any# </ strong></td> ");

            //for (int x = 0; x < dtpricedetails.Rows.Count; x++)
            //{
            //    string name = dtpricedetails.Rows[x]["Name"].ToString();

            //    if (name == "Transfer Charges if any")
            //    {
            //        sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong>" + dtpricedetails.Rows[x]["UnitPriceParameter_Value"].ToString() + "</strong></td> ");

            //    }
            //}
            //sbPdfBody.Append(" <td height='40' style='color:#000000; font-size:10px;'>559.73</td> ");
            sbPdfBody.Append(" </tr> ");
            sbPdfBody.Append(" <tr> ");
            sbPdfBody.Append(" <td height='40' colspan='4' style='color:#000000; font-size:10px; background-color:#dce6f1;'> ");
            sbPdfBody.Append("<div align='center'><strong>Guidline &amp; Distress/Forced Sale Value</strong></div></td> ");
            sbPdfBody.Append(" </tr> ");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p><strong>Government Guideline/Circle Rate For Residential (Rs per Sq.ft)</strong></p></td> ");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'><strong> #Any_Other_Charges# </ strong></td> ");


            //sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>129.17</td> ");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'> ");
            sbPdfBody.Append("<p><strong>Residential Value as per  Government Rate (Rs)</strong></p></td>");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'> #Government_Rate# </td> ");
            sbPdfBody.Append(" </tr> ");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='40' colspan='3' style='color:#000000; font-size:10px;'><strong>Remarks</strong></td>");
            sbPdfBody.Append("<td height='40' style='color:#000000; font-size:10px;'>&nbsp;</td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td width='100%' height='78' style='color:#000000; font-size:10px; background-color:#dce6f1;'>");
            sbPdfBody.Append("<div align='center'><strong>Property Location (19.843281,74.043931)</strong></div></td>");
            sbPdfBody.Append("</tr> ");
            sbPdfBody.Append("<tr> ");
            sbPdfBody.Append("<td height='78' style='color:#000000; font-size:10px;'>");
            //sbPdfBody.Append("<div align='center'><img class='img-responsive' src='images/location.jpg' width='400' height='500'></div></td>");
            sbPdfBody.Append("<div align='center'><img src='" + Server.MapPath("~/images") + "/logo-home.png'/></div></td>");


            sbPdfBody.Append("</tr> ");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append(" <td height='78' style='color:#000000; font-size:10px; background-color:#dce6f1;'>");
            sbPdfBody.Append("<div align='center'><strong>Property Photographs</strong></div></td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append("<td height='50' colspan='2' style='background-color:#FFFFFF; padding:o; margin:0; line-height:0; font-size:28px; color:#000000;'>");
            sbPdfBody.Append("<table width='80%' border='1' align='center' height='80' cellpadding='0' cellspacing='0' bgcolor='#FFFFFF' style='margin-bottom:30px; margin-top:10px;'>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append(" <td width='42%' height='78' style='color:#000000; font-size:10px;'>");
            //sbPdfBody.Append("<div align='center'><img src='images/1.jpg' width='335' height='397'></div></td>");
            sbPdfBody.Append("<div align='center'><img src='" + Server.MapPath("~/images") + "/logo-home.png'/></div></td>");

            sbPdfBody.Append("<td width='39%' style='color:#000000; font-size:10px;'>");
            //sbPdfBody.Append("<div align='center'><img src='images/2.jpg' width='329' height='396'></div></td>");
            // sbPdfBody.Append("<div align='center'><img src='" + Server.MapPath("~/images") + "/logo-home.png'/></div></td>");

            sbPdfBody.Append("<td width='19%' style='color:#000000; font-size:10px;'>");
            //sbPdfBody.Append("  <div align='center'><img src='images/3.jpg' width='330' height='395'></div></td>");
            //sbPdfBody.Append("<div align='center'><img src='" + Server.MapPath("~/images") + "/logo-home.png'/></div></td>");

            sbPdfBody.Append("</tr>");
            sbPdfBody.Append(" <tr>");
            //sbPdfBody.Append(" <td height='78' style='color:#000000; font-size:10px;'><img src='images/4.jpg' width='332' height='400'></td>");
            //sbPdfBody.Append("<td><div align='center'><img src='" + Server.MapPath("~/images") + "/logo-home.png'/></div></td>");

            //sbPdfBody.Append("  <td style='color:#000000; font-size:10px;'><img src='images/5.jpg' width='332' height='400'></td>");
            //sbPdfBody.Append("  <td style='color:#000000; font-size:10px;'><img src='images/6.jpg' width='332' height='400'></td>");
            //sbPdfBody.Append("<td><div align='center'><img src='" + Server.MapPath("~/images") + "/logo-home.png'/></div></td>");
            //sbPdfBody.Append("<td><div align='center'><img src='" + Server.MapPath("~/images") + "/logo-home.png'/></div></td>");


            sbPdfBody.Append("</tr>");
            sbPdfBody.Append(" </table>");
            sbPdfBody.Append("</td>");
            sbPdfBody.Append(" </tr>");
            sbPdfBody.Append(" <tr>");
            sbPdfBody.Append("<td height='36' colspan='4' bgcolor='#FFFFFF' style='color:#000000; font-size:10px; background-color:#dce6f1;'>");
            sbPdfBody.Append("<p align='center'><strong>Documents Provided</strong></p></td>");
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("<tr>");
            sbPdfBody.Append(" <td height='108' colspan='2' bgcolor='#FFFFFF' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p align='center'><strong>Document </strong></p></td>");
            sbPdfBody.Append("<td width='25%' bgcolor='#FFFFFF' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append(" <div align='center'><strong>Date </strong></div></td>");
            sbPdfBody.Append("  <td width='21%' bgcolor='#FFFFFF' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='center'><strong>Authority</strong></div></td>");
            sbPdfBody.Append(" </tr>");
            sbPdfBody.Append(" <tr>");
            sbPdfBody.Append("<td height='109' colspan='2' bgcolor='#FFFFFF' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='center'><strong>Sale Deed</strong></div></td>");
            sbPdfBody.Append(" <td height='109' bgcolor='#FFFFFF' style='color:#000000; font-size:10px;'>document</td>");
            sbPdfBody.Append("<td height='109' bgcolor='#FFFFFF' style='color:#000000; font-size:10px;'> document </td>");
            sbPdfBody.Append(" </tr>");
            sbPdfBody.Append(" <tr>");
            sbPdfBody.Append("<td height='217' colspan='2' bgcolor='#FFFFFF' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<div align='center'><strong>Construction Plan</strong></div></td>");
            sbPdfBody.Append(" <td height='217' bgcolor='#FFFFFF' style='color:#000000; font-size:10px;'>Document</td>");
            sbPdfBody.Append("<td height='217' bgcolor='#FFFFFF' style='color:#000000; font-size:10px;'> Document </td>");
            sbPdfBody.Append(" </tr>");
            sbPdfBody.Append(" <tr>");
            sbPdfBody.Append(" <td height='217' colspan='4' bgcolor='#FFFFFF' style='color:#000000; font-size:10px; background-color:#dce6f1;'>");
            sbPdfBody.Append("<div align='center'><strong>Disclaimer</strong></div></td>");
            sbPdfBody.Append(" </tr>");
            sbPdfBody.Append(" <tr>");
            sbPdfBody.Append(" <td height='217' colspan='4' bgcolor='#FFFFFF' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("  <p>1. We have no direct or indirect interest in the property valued. </p>");
            sbPdfBody.Append("<p>  2. The information furnished in the report is true and correct to the best of our knowledge and belief.</p>");

            sbPdfBody.Append("<p>3. We have relied on the Documents Provided by the Client &amp; We have assumed that clients have verified the same. <br></p>");
            sbPdfBody.Append("<p>4.We have not carried out any legal due diligence of the property, any disputes regarding   ownership, title, marketability of the property will change the valuation substantially</p>");
            sbPdfBody.Append(" <p>5. The fair market value indicated in the report is an opinion of the value prevailing on the date of  the said report and is based on market feedback on values of similar properties. The fair, market  value of such properties/localities may increase or decrease, depending on the market conditions and scenarios.  </p>");
            sbPdfBody.Append(" <p>6. This report is valid for 30 days from the date of this report.</p></td>");
            sbPdfBody.Append(" </tr>");
            sbPdfBody.Append(" <tr>");
            sbPdfBody.Append(" <td width='27%' height='217' bgcolor='#FFFFFF' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p><strong>Name of Engineer  Visited  the Site </strong></p></td>");
            sbPdfBody.Append("<td width='27%' bgcolor='#FFFFFF' style='color:#000000; font-size:10px;'>nameofengineer</td>");
            sbPdfBody.Append("<td height='217' bgcolor='#FFFFFF' style='color:#000000; font-size:10px;'>");
            sbPdfBody.Append("<p><strong>Authorized Signatory  Name &amp; Signature</strong></p></td>");
            sbPdfBody.Append("<td height='217' bgcolor='#FFFFFF' style='color:#000000; font-size:10px;'>signature</td>");
            sbPdfBody.Append(" </tr>");
            sbPdfBody.Append(" <tr>");
            sbPdfBody.Append("<td height='217' colspan='4' bgcolor='#FFFFFF' style='color:#000000; font-size:10px; padding-top:10px; padding-bottom:10px;'>");
            //sbPdfBody.Append("<div align='center'><img src='images/stamp.jpg' width='600' height='300'></div></td>");
            sbPdfBody.Append("<div align='center'><img src='" + Server.MapPath("~/images") + "/logo-home.png'/></div></td>");

            sbPdfBody.Append(" </tr>");

            sbPdfBody.Append(" </tr> ");

            sbPdfBody.Append("</table>");

            //}

            sbPdfBody.Append("</td>");
            sbPdfBody.Append("</tr>");



            sbPdfBody.Append("</table>");//2nd top table
            sbPdfBody.Append("</td>");//Main td tr
            sbPdfBody.Append("</tr>");
            sbPdfBody.Append("</table>");
            sbPdfBody.Append("</body>");
            sbPdfBody.Append("</html>");

            for (int x = 0; x < dtpricedetails.Rows.Count; x++)
            {
                if (dtpricedetails.Rows[x]["name"].ToString().Trim().Equals(string.Empty))
                {
                    sbPdfBody.Replace(dtpricedetails.Rows[x]["LableName"].ToString().Trim(), string.Empty);
                }
                else
                {
                    sbPdfBody.Replace(dtpricedetails.Rows[x]["LableName"].ToString().Trim(), dtpricedetails.Rows[x]["UnitPriceParameter_Value"].ToString().Trim());
                }

            }

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + "PDFfile.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(GetPDF(sbPdfBody.ToString()));
            Response.End();
            return this.RedirectToAction(GetPDF("pHTML").ToString());
        }
        public string RemoveBetween(string sourceString, string startTag, string endTag)
        {
            Regex regex = new Regex(string.Format("{0}(.*){1}", Regex.Escape(startTag), Regex.Escape(endTag)));
            return regex.Replace(sourceString, string.Empty);
        }
        public string ReplaceWholeWord(string original, string wordToFind, string replacement, RegexOptions regexOptions = RegexOptions.None)
        {
            string pattern = String.Format(@"\b{0}\b", wordToFind);
            string ret = Regex.Replace(original, pattern, replacement, regexOptions);
            return ret;
        }
        [HttpPost]
        public ActionResult RequestorChange(string str)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            string j = Convert.ToString(list1.Rows[0]["reqID"]);
            string m = Convert.ToString(list1.Rows[0]["stat"]);
            hInputPara.Add("@RequestID", j);
            hInputPara.Add("@Status", m);
            var res = sqlDataAccess.ExecuteStoreProcedure("usp_updatestatus", hInputPara);
            hInputPara.Clear();
            return Json(new { success = true, Message = "Successfully" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RequestorView(int ID)
        {
            Session["RequestID"] = ID;
            string ReqID = Session["RequestID"].ToString();
            ViewModelRequest vm = new ViewModelRequest();


            vm.ival_LookupCategoryValuestypeofownership = GetTypeofOwnership();
            //vm.ival_LookupCategoryValuestypeofstructure = Gettypestructure();
            vm.ival_LookupCategoryValuesvaluationtype = Getvaltype();
            vm.ival_LookupCategoryValuesmethodvaluation = Getmethodtype();
            vm.ival_LookupCategoryValuesreporttype = GetReportType();

            vm.ival_DocumentLists = GetDocuments();
            vm.ival_Document_Uploaded_ListS = GetFilesList(ReqID);


            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@RequestID", ID);
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetReuestorsdetailsByReqID", hInputPara);
                ViewBag.Customer_Name = Convert.ToString(dtRequests.Rows[0]["Customer_Name"]);
                ViewBag.Customer_Name_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Name_1"]);
                ViewBag.Customer_Application_ID = Convert.ToString(dtRequests.Rows[0]["Customer_Application_ID"]);
                ViewBag.Email_ID = Convert.ToString(dtRequests.Rows[0]["Email_ID"]);
                ViewBag.Customer_Contact_No_1 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_1"]);
                ViewBag.Customer_Contact_No_2 = Convert.ToString(dtRequests.Rows[0]["Customer_Contact_No_2"]);
                ViewBag.Request_Date = Convert.ToString(dtRequests.Rows[0]["Request_Date"]);
                //string[] formats = { "dd/MM/yyyy" };
                //ViewBag.Request_Date = DateTime.ParseExact(Convert.ToString(dtRequests.Rows[0]["Request_Date"]), formats, new CultureInfo("en-US"), DateTimeStyles.None);

                ViewBag.Valuation_Request_ID = Convert.ToString(dtRequests.Rows[0]["Valuation_Request_ID"]);
                ViewBag.Document_Holder_Name_1 = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name_1"]);
                ViewBag.Document_Holder_Name = Convert.ToString(dtRequests.Rows[0]["Document_Holder_Name"]);
                ViewBag.Selectedtypeofownership = Convert.ToString(dtRequests.Rows[0]["Type_Of_Ownership"]);
                ViewBag.ValuationType = Convert.ToString(dtRequests.Rows[0]["ValuationType"]);
                ViewBag.ReportType = Convert.ToString(dtRequests.Rows[0]["ReportType"]);
                ViewBag.Method_of_Valuation = Convert.ToString(dtRequests.Rows[0]["Method_of_Valuation"]);
                ViewBag.AssignedToEmpID = Convert.ToString(dtRequests.Rows[0]["AssignedToEmpID"]);
                ViewBag.Date_of_Inspection = Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]);
                ViewBag.Date_of_Valuation = Convert.ToString(dtRequests.Rows[0]["Date_of_Valuation"]);
                //ViewBag.Date_of_Inspection = DateTime.ParseExact(Convert.ToString(dtRequests.Rows[0]["Date_of_Inspection"]), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                //ViewBag.Date_of_Valuation = DateTime.ParseExact(Convert.ToString(dtRequests.Rows[0]["Date_of_Valuation"]), formats, new CultureInfo("en-US"), DateTimeStyles.None);



                if (dtRequests.Rows[0]["Client_ID"].ToString() != "")
                {
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara = new Hashtable();
                    hInputPara.Add("@Client_ID", Convert.ToInt32(dtRequests.Rows[0]["Client_ID"].ToString()));
                    DataTable dtClient = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientMasterByID", hInputPara);
                    if (dtClient != null && dtClient.Rows.Count > 0)
                    {
                        ViewBag.Client_Name = dtClient.Rows[0]["Client_Name"].ToString();
                        ViewBag.Branch_Name = dtClient.Rows[0]["Branch_Name"].ToString();
                    }

                }
                if (dtRequests.Rows[0]["Department"].ToString() != "")
                {
                    ViewBag.Department = dtRequests.Rows[0]["Department"].ToString();
                }
                if (dtRequests.Rows[0]["Loan_Type"].ToString() != "")
                {
                    ViewBag.Loan_Type = dtRequests.Rows[0]["Loan_Type"].ToString();
                }
                if (dtRequests.Rows[0]["Requestor_Name"].ToString() != "")
                {
                    ViewBag.Requestor_Name = dtRequests.Rows[0]["Requestor_Name"].ToString();
                }

                if (dtRequests.Rows[0]["Client_ID"].ToString() != "")
                {
                    ViewBag.Client_ID = Convert.ToInt32(dtRequests.Rows[0]["Client_ID"].ToString());
                }
                IEnumerable<ival_LookupCategoryValues> GetTypeofOwnerships = GetTypeofOwnership();

                if (GetTypeofOwnerships.ToList().Count > 0 && GetTypeofOwnerships != null)
                {
                    var Owner = GetTypeofOwnerships.AsEnumerable().Where(row => row.Lookup_Value == dtRequests.Rows[0]["Type_Of_Ownership"].ToString()).Select(row => row.Lookup_ID);
                    vm.Selectedtypeofownership = Convert.ToInt32(Owner.FirstOrDefault());

                }

            }

            catch (Exception e)
            {
                e.InnerException.ToString();
            }


            return View(vm);
        }



        public JsonResult DownloadView(string docname)
        {


            var FilePath = docname;
            var folderPath = Server.MapPath("~/UploadFiles");
            var fileCount = Directory.GetFiles(Server.MapPath("~/UploadFiles"));
            if (fileCount.Length > 1) // exactly only 1 file
            {
                string embed = "<object data=\"{0}\" type=\"application/pdf\"width=\"100%\" height=\"600px\">";
                embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
                embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                embed += "</object>";
                TempData["Embed"] = string.Format(embed, VirtualPathUtility.ToAbsolute("~/UploadFiles/" + FilePath));
                return Json("Exist", JsonRequestBehavior.AllowGet);
            }
            //else if (fileCount.Length > 1) // more than 1 file
            //{
            //    Process.Start(folderPath);
            //}
            else
            {
                Process.Start(folderPath);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetFilePathUploadDocEdit()
        {
            string value = ConfigurationManager.AppSettings["UploadPDFFileEdit"];
            return Json(value);
        }



        public ActionResult SingleUnitDetailsView()
        {
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            ViewBag.UnitType = Session["TypeOfUnit"].ToString();
            ViewBag.PType = Session["Property_Type"].ToString();
            Session["TypeOfSelect"] = ViewBag.SelectType;
            //ViewBag.UnitType = "Flat";
            //ViewBag.PType = "Residential";
            hInputPara.Add("@Request_ID", RequestID);
            ViewModelRequest vm = new ViewModelRequest();
            DataTable dtunitREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllunitsBYReqID", hInputPara);
            if (dtunitREQ.Rows.Count > 0)
            {

                vm.relationWithEmpList1 = GetRelationsWithEmp1();
                vm.relationWithEmpList2 = GetRelationsWithEmp2();
                ViewBag.Deviation_Description = dtunitREQ.Rows[0]["Deviation_Description"].ToString();
                ViewBag.Construction_as_per_plan = dtunitREQ.Rows[0]["Construction_as_per_plan"].ToString();
                ViewBag.Risk_of_Demolition = dtunitREQ.Rows[0]["Risk_of_Demolition"].ToString();
                ViewBag.Type_of_Flooring = dtunitREQ.Rows[0]["Type_of_Flooring"].ToString();
                ViewBag.Wall_Finish = dtunitREQ.Rows[0]["Wall_Finish"].ToString();
                ViewBag.Plumbing_Fitting = dtunitREQ.Rows[0]["Plumbing_Fitting"].ToString();
                ViewBag.Electrical_Fittings = dtunitREQ.Rows[0]["Electrical_Fittings"].ToString();
                ViewBag.quality = dtunitREQ.Rows[0]["quality_fixtures"].ToString();
                ViewBag.Rough_Plaster = dtunitREQ.Rows[0]["Rough_Plaster"].ToString();
                ViewBag.Tiling = dtunitREQ.Rows[0]["Tiling"].ToString();
                ViewBag.Cord_Longitude = dtunitREQ.Rows[0]["Cord_Longitude"].ToString();
                ViewBag.Cord_Latitude = dtunitREQ.Rows[0]["Cord_Latitude"].ToString();
                ViewBag.Road1 = dtunitREQ.Rows[0]["Road1"].ToString();
                ViewBag.Road2 = dtunitREQ.Rows[0]["Road2"].ToString();
                ViewBag.Road3 = dtunitREQ.Rows[0]["Road3"].ToString();
                ViewBag.Road4 = dtunitREQ.Rows[0]["Road4"].ToString();
                ViewBag.SeismicZone = dtunitREQ.Rows[0]["SeismicZone"].ToString();
                ViewBag.FloodZone = dtunitREQ.Rows[0]["FloodZone"].ToString();
                ViewBag.CycloneZone = dtunitREQ.Rows[0]["CycloneZone"].ToString();
                ViewBag.CRZ = dtunitREQ.Rows[0]["CRZ"].ToString();
                ViewBag.Remarks = dtunitREQ.Rows[0]["Remarks"].ToString();
                ViewBag.Plot_Demarcated_at_site = dtunitREQ.Rows[0]["Plot_Demarcated_at_site"].ToString();
                ViewBag.Type_of_Demarcation = dtunitREQ.Rows[0]["Type_of_Demarcation"].ToString();

                ViewBag.Boundaries_Matching = dtunitREQ.Rows[0]["Boundaries_Matching"].ToString();
                ViewBag.Road_Finish = dtunitREQ.Rows[0]["Road_Finish"].ToString();
                ViewBag.AreBoundariesremarks = dtunitREQ.Rows[0]["AreBoundariesremarks"].ToString();

                ViewBag.Name = dtunitREQ.Rows[0]["Name"].ToString();
                ViewBag.Property_Identified_PName = dtunitREQ.Rows[0]["Property_Identified_PName"].ToString();
                ViewBag.P_Contact_Num = dtunitREQ.Rows[0]["P_Contact_Num"].ToString();
                ViewBag.Customer_Name_On_Board = dtunitREQ.Rows[0]["CustomerDisplayBoard"].ToString();
                ViewBag.Society_OR_Building_Name = dtunitREQ.Rows[0]["Society_OR_Building_Name"].ToString();

                ViewBag.Current_Value_of_Property = dtunitREQ.Rows[0]["Current_Value_of_Property"].ToString();
                ViewBag.Total_Value_of_Property = dtunitREQ.Rows[0]["Total_Value_of_Property"].ToString();
                ViewBag.Government_Value = dtunitREQ.Rows[0]["Government_Value"].ToString();
                ViewBag.Cost_of_Interior = dtunitREQ.Rows[0]["Cost_of_Interior"].ToString();
                ViewBag.MeasuredSaleableArea = dtunitREQ.Rows[0]["MeasuredSaleableArea"].ToString();
                ViewBag.ApprovedSaleableArea = dtunitREQ.Rows[0]["ApprovedSaleableArea"].ToString();
                ViewBag.distressValue = dtunitREQ.Rows[0]["distressValue"].ToString();
                ViewBag.StageOfConstruction = dtunitREQ.Rows[0]["StageOfConstruction"].ToString();
                ViewBag.IsInteriorworkcarriedout = dtunitREQ.Rows[0]["IsInteriorworkcarriedout"].ToString();
                ViewBag.ValuationResult = dtunitREQ.Rows[0]["ValuationResult"].ToString();
                ViewBag.DescStageofconstruction = dtunitREQ.Rows[0]["DescStageofconstruction"].ToString();
                ViewBag.No_Of_Lifts = dtunitREQ.Rows[0]["No_Of_Lifts"].ToString();


                ViewBag.Lift_Details = dtunitREQ.Rows[0]["Lift_Details"].ToString();

                ViewBag.Date_of_Inspection = dtunitREQ.Rows[0]["Date_Of_Inspection"].ToString();

                ViewBag.AssignedToEmpID = dtunitREQ.Rows[0]["AssignedToEmpID"].ToString();
                ViewBag.Construct = dtunitREQ.Rows[0]["Constr_As"].ToString();
                ViewBag.Risk = dtunitREQ.Rows[0]["Risk_demarcation"].ToString();
                ViewBag.Quality_Fixtures = dtunitREQ.Rows[0]["Quality_Fixtures"].ToString();

                ViewBag.Relizable_Value = dtunitREQ.Rows[0]["Realizable_Value"].ToString();
                ViewBag.txtrealpercent = dtunitREQ.Rows[0]["RealPercPlot"].ToString();

                ViewBag.txtrealizablevalue = dtunitREQ.Rows[0]["RealizablePlot"].ToString();
                ViewBag.IsLifts_Present = dtunitREQ.Rows[0]["IsLifts_Present"].ToString();
                ViewBag.ViewFromUnit = dtunitREQ.Rows[0]["ViewFromUnit"].ToString();
                ViewBag.MeasuredApprovedareamatchatsite = dtunitREQ.Rows[0]["MeasuredApprovedareamatchatsite"].ToString();
                ViewBag.DeviationPercentage = dtunitREQ.Rows[0]["DeviationPercentage"].ToString();
                ViewBag.Value_of_Plot = dtunitREQ.Rows[0]["Value_of_Plot"].ToString();
                ViewBag.Total_Land_Plot_Value = dtunitREQ.Rows[0]["Total_Land_Plot_Value"].ToString();
                ViewBag.Value_of_other_Plot = dtunitREQ.Rows[0]["Value_of_other_Plot"].ToString();
                ViewBag.Distress_Value_of_Plot = dtunitREQ.Rows[0]["Distress_Value_of_Plot"].ToString();
                ViewBag.OtherPlotType = dtunitREQ.Rows[0]["OtherPlotType"].ToString();
                ViewBag.Extra_Cost_Valuation = dtunitREQ.Rows[0]["Extra_Cost_Valuation"].ToString();

                ViewBag.Work_Completed_Perc = dtunitREQ.Rows[0]["Work_Completed_Perc"].ToString();
                ViewBag.StageOfConstructionBunglow = dtunitREQ.Rows[0]["StageOfConstructionBunglow"].ToString();
                ViewBag.StageOfConstructionPerct = dtunitREQ.Rows[0]["StageOfConstructionPerct"].ToString();
                ViewBag.DisbursementRecommendPerct = dtunitREQ.Rows[0]["DisbursementRecommendPerct"].ToString();
                ViewBag.IsInteriorworkcarriedout = dtunitREQ.Rows[0]["IsInteriorworkcarriedout"].ToString();
                ViewBag.DistressValuePerct = dtunitREQ.Rows[0]["DistressValuePerct"].ToString();
                ViewBag.Government_Rate = dtunitREQ.Rows[0]["Government_Rate"].ToString();
                ViewBag.DescStageofconstructionBunglow = dtunitREQ.Rows[0]["DescStageofconstructionBunglow"].ToString();
                ViewBag.Gross_Estimated_Cost_Construction = dtunitREQ.Rows[0]["Gross_Estimated_Cost_Construction"].ToString();
                ViewBag.Cost_Construction_PerSqFT = dtunitREQ.Rows[0]["Cost_Construction_PerSqFT"].ToString();
                ViewBag.Does_this_Matches_UnderConstruction = dtunitREQ.Rows[0]["Does_this_Matches_UnderConstruction"].ToString();
                ViewBag.Does_this_Cost_Remark = dtunitREQ.Rows[0]["Does_this_Cost_Remark"].ToString();

                ViewBag.Valueoftheplotbungalow = dtunitREQ.Rows[0]["Valueoftheplotbungalow"].ToString();
                ViewBag.Valueoftheotherplotbungalow = dtunitREQ.Rows[0]["Valueoftheotherplotbungalow"].ToString();
                ViewBag.Totalofthebungalow = dtunitREQ.Rows[0]["Totalofthebungalow"].ToString();
                ViewBag.TotalvaluepropertyBunglow = dtunitREQ.Rows[0]["TotalvaluepropertyBunglow"].ToString();
                ViewBag.DistressValueofPropertyBunglow = dtunitREQ.Rows[0]["DistressValueofPropertyBunglow"].ToString();
                ViewBag.TotalvaluepropertyBunglowUC = dtunitREQ.Rows[0]["TotalvaluepropertyBunglowUC"].ToString();
                ViewBag.Current_Value_of_Property_UC = dtunitREQ.Rows[0]["Current_Value_of_Property_UC"].ToString();
                ViewBag.ValuationResultR = dtunitREQ.Rows[0]["ValuationResultR"].ToString();
                ViewBag.ValuationResultUC = dtunitREQ.Rows[0]["ValuationResultUC"].ToString();
                ViewBag.Cost_of_InteriorReady = dtunitREQ.Rows[0]["Cost_of_InteriorReady"].ToString();
                ViewBag.StageOfConstructionUC = dtunitREQ.Rows[0]["StageOfConstructionUC"].ToString();
                ViewBag.DisbursementRecommendPerctUC = dtunitREQ.Rows[0]["DisbursementRecommendPerctUC"].ToString();
                ViewBag.ValuationResultplot = dtunitREQ.Rows[0]["ValuationResultplot"].ToString();
                ViewBag.TotalFlatwords = dtunitREQ.Rows[0]["TotalFlatwords"].ToString();
                ViewBag.TotalPlotwords = dtunitREQ.Rows[0]["TotalPlotwords"].ToString();
                ViewBag.TotalreadyBunglowWords = dtunitREQ.Rows[0]["TotalreadyBunglowWords"].ToString();
                ViewBag.TotalUCBunglowWords = dtunitREQ.Rows[0]["TotalUCBunglowWords"].ToString();
                ViewBag.TotalCostCons = dtunitREQ.Rows[0]["TotalCostCons"].ToString();
                ViewBag.Distressvalpercentplot = dtunitREQ.Rows[0]["Distressvalpercentplot"].ToString();

                //new
                ViewBag.MeasuredAreaFt = dtunitREQ.Rows[0]["MeasuredAreaFt"].ToString();
                ViewBag.ApprovedAreaF = dtunitREQ.Rows[0]["ApprovedAreaF"].ToString();
                ViewBag.AdoptedAValFt = dtunitREQ.Rows[0]["AdoptedAValFt"].ToString();
                ViewBag.AdoptedLandRFt = dtunitREQ.Rows[0]["AdoptedLandRFt"].ToString();
                    
                ViewBag.MeasuredAreaMt = dtunitREQ.Rows[0]["MeasuredAreaMt"].ToString();
                ViewBag.ApprovedAreaMt = dtunitREQ.Rows[0]["ApprovedAreaMt"].ToString();
                ViewBag.AdoptedAValMt = dtunitREQ.Rows[0]["AdoptedAValMt"].ToString();
                ViewBag.AdoptedLandRMt = dtunitREQ.Rows[0]["AdoptedLandRMt"].ToString();
                    
                ViewBag.MeasuredOtherPAFt = dtunitREQ.Rows[0]["MeasuredOtherPAFt"].ToString();
                ViewBag.ApproveOtherPAFt = dtunitREQ.Rows[0]["ApproveOtherPAFt"].ToString();
                ViewBag.AdoptedOtherPAValFt = dtunitREQ.Rows[0]["AdoptedOtherPAValFt"].ToString();
                ViewBag.AdoptedOtherPLandRFt = dtunitREQ.Rows[0]["AdoptedOtherPLandRFt"].ToString();

                ViewBag.MeasuredOtherPAMt = dtunitREQ.Rows[0]["MeasuredOtherPAMt"].ToString();
                ViewBag.ApproveOtherPAMt = dtunitREQ.Rows[0]["ApproveOtherPAMt"].ToString();
                ViewBag.AdoptedOtherPAValMt = dtunitREQ.Rows[0]["AdoptedOtherPAValMt"].ToString();
                ViewBag.AdoptedOtherPLandRMt = dtunitREQ.Rows[0]["AdoptedOtherPLandRMt"].ToString();

                ViewBag.Permissible = dtunitREQ.Rows[0]["Permissible"].ToString();
                ViewBag.LandRateRangeLoc = dtunitREQ.Rows[0]["LandRateRangeLoc"].ToString();
                ViewBag.PermissibleOtherplot = dtunitREQ.Rows[0]["PermissibleOtherplot"].ToString();
                ViewBag.GovtRateSF = dtunitREQ.Rows[0]["GovtRateSF"].ToString();
                ViewBag.GovtvalueS = dtunitREQ.Rows[0]["GovtvalueS"].ToString();

                ViewBag.MatchAt = dtunitREQ.Rows[0]["MatchAt"].ToString();
                ViewBag.nill = dtunitREQ.Rows[0]["RiskOf"].ToString();
                ViewBag.DevDeatils = dtunitREQ.Rows[0]["DevDetails"].ToString();
                ViewBag.DevUnit = dtunitREQ.Rows[0]["DevUnit"].ToString();
                ViewBag.Project_ID = dtunitREQ.Rows[0]["Project_ID"].ToString();

                Hashtable hInputPara22 = new Hashtable();
                hInputPara22.Add("@Project_ID", ViewBag.Project_ID);
                DataTable ValuationRequestID2 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorwisebreakUpperFloor", hInputPara22);
                // Number_Of_Floors = Convert.ToString(dtsocialinfra.Rows[i]["Number_Of_Floors"]),
                // ViewBag.floorCount = Convert.ToInt32(ValuationRequestID2.Rows[0]["Number_Of_Floors"]);

                if (ValuationRequestID2.Rows.Count > 0)
                {
                    ViewBag.floorCount = Convert.ToInt32(ValuationRequestID2.Rows[0]["Number_Of_Floors"]);
                }
                else
                {
                    ViewBag.floorCount = 0;
                }
            }
            else
            {

                return RedirectToAction("Index", "NewRequestor", new { area = "Index" });
            }
           //new
            vm.ival_ProjectFloorWiseBreakUpstotal = GetFloorwisebreakbyreqIDtotal();

            vm.ival_ProjectFloorWiseBreakUpstotal = GetFloorwisebreakbyreqIDtotal();
            vm.ival_ProjectImageName = GetProjectMasterImageName();
            //new
            //vm.ival_UnitParameterMasters = GetUnitDetails(unitname);
            vm.ival_ProjectFloorWiseBreakUps = GetFloorwisebreakbyreqID();
            vm.ival_ProjectApprovalDetails = GetApprovalTypesnew1();
            //    vm.ival_ProjectApprovalDetails = GetApprovalTypesnew();
            vm.ival_ProjectBoundaries = GetBoundariesByReqID();
            vm.ival_ProjectSideMargins = GetSidearginsByReqID();
            vm.ival_LookupCategoryValuesviewfromunit = GetviewUnits();
            vm.ival_LookupCategoryValuesRiskofdemolition = Getdemolition();
            vm.ival_LookupCategoryValuesdemarcation = GetDemarcation();
            vm.ival_LookupCategoryValuesdemarcation1 = GetDemarcation1();
            vm.ival_LookupCategoryValuesRoadFinish = GetRoadFinish();
            vm.ival_LookupCategoryValuesPropZone = GetPropZone();
            vm.ival_BrokerDetails = GetbrokerBYreqID();
            vm.iva_Comparable_Properties = GetComprabledetreqID();
            vm.ival_ProjectImageName = GetProjectMasterImageName();
            vm.UploadedImagesList = GetUploadedImageListEdit(RequestID.ToString());
            foreach (var data in vm.UploadedImagesList)
            {
                if (data.Image_Type == "Internal View")
                {
                    if (data.Seq_No == 1)
                    {
                        ViewBag.FilePathIn1 = data.FilePath;
                        ViewBag.ImagePathIn1 = data.Image_Path;
                        //TempData["ImagePathIn1"] = data.Image_Path;
                    }
                    if (data.Seq_No == 2)
                    {
                        ViewBag.FilePathIn2 = data.FilePath;
                        ViewBag.ImagePathIn2 = data.Image_Path;
                    }
                    if (data.Seq_No == 3)
                    {
                        ViewBag.FilePathIn3 = data.FilePath;
                        ViewBag.ImagePathIn3 = data.Image_Path;
                    }
                    if (data.Seq_No == 4)
                    {
                        ViewBag.FilePathIn4 = data.FilePath;
                        ViewBag.ImagePathIn4 = data.Image_Path;
                    }

                }
                if (data.Image_Type == "External View ")
                {
                    if (data.Seq_No == 1)
                    {
                        ViewBag.FilePathEx1 = data.FilePath;
                        ViewBag.ImagePathEx1 = data.Image_Path;
                    }
                    if (data.Seq_No == 2)
                    {
                        ViewBag.FilePathEx2 = data.FilePath;
                        ViewBag.ImagePathEx2 = data.Image_Path;
                    }
                    if (data.Seq_No == 3)
                    {
                        ViewBag.FilePathEx3 = data.FilePath;
                        ViewBag.ImagePathEx3 = data.Image_Path;
                    }
                    if (data.Seq_No == 4)
                    {
                        ViewBag.FilePathEx4 = data.FilePath;
                        ViewBag.ImagePathEx4 = data.Image_Path;
                    }

                }
                if (data.Image_Type == "Approach Road")
                {
                    if (data.Seq_No == 1)
                    {
                        ViewBag.FilePathAR1 = data.FilePath;
                        ViewBag.ImagePathAR1 = data.Image_Path;
                    }
                    if (data.Seq_No == 2)
                    {
                        ViewBag.FilePathAR2 = data.FilePath;
                        ViewBag.ImagePathAR2 = data.Image_Path;
                    }
                    if (data.Seq_No == 3)
                    {
                        ViewBag.FilePathAR3 = data.FilePath;
                        ViewBag.ImagePathAR3 = data.Image_Path;
                    }
                    if (data.Seq_No == 4)
                    {
                        ViewBag.FilePathAR4 = data.FilePath;
                        ViewBag.ImagePathAR4 = data.Image_Path;
                    }

                }
                if (data.Image_Type == "View from property ")
                {
                    if (data.Seq_No == 1)
                    {
                        ViewBag.FilePathv1 = data.FilePath;
                        ViewBag.ImagePathv1 = data.Image_Path;
                    }
                    if (data.Seq_No == 2)
                    {
                        ViewBag.FilePathv2 = data.FilePath;
                        ViewBag.ImagePathv2 = data.Image_Path;
                    }
                    if (data.Seq_No == 3)
                    {
                        ViewBag.FilePathv3 = data.FilePath;
                        ViewBag.ImagePathv3 = data.Image_Path;
                    }
                    if (data.Seq_No == 4)
                    {
                        ViewBag.FilePathv4 = data.FilePath;
                        ViewBag.ImagePathv4 = data.Image_Path;
                    }

                }
                if (data.Image_Type == "IGR Photo")
                {
                    if (data.Seq_No == 1)
                    {
                        ViewBag.FilePathIg = data.FilePath;
                        ViewBag.ImagePathIg = data.Image_Path;
                    }
                }
                if (data.Image_Type == "Location Map")
                {
                    if (data.Seq_No == 1)
                    {
                        ViewBag.FilePathlm = data.FilePath;
                        ViewBag.ImagePathlm = data.Image_Path;
                    }
                }
            }


            return View(vm);
        }

        [HttpPost]
        public JsonResult GetUnitDetailsView()
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_UnitParametersValue> ViewUnitLists = new List<ival_UnitParametersValue>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtunits = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitDetailsbyReqID", hInputPara);
            for (int i = 0; i < dtunits.Rows.Count; i++)
            {
                ViewUnitLists.Add(new ival_UnitParametersValue
                {
                    UnitParameter_ID = Convert.ToInt32(dtunits.Rows[i]["UnitParameter_ID"]),
                    Name = Convert.ToString(dtunits.Rows[i]["Name"]),
                    UnitParameter_Value = Convert.ToString(dtunits.Rows[i]["UnitParameter_Value"]),
                });
            }
            //ViewBag.unitdetails = ProjectUnitLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(ViewUnitLists);
            return Json(ViewUnitLists);

        }

        [HttpPost]
        public JsonResult GetUnitDetailsViewsearch()
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_UnitParametersValue> ViewUnitLists = new List<ival_UnitParametersValue>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["Request_ID"]);
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtunits = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitDetailsbyReqID", hInputPara);
            for (int i = 0; i < dtunits.Rows.Count; i++)
            {
                ViewUnitLists.Add(new ival_UnitParametersValue
                {
                    UnitParameter_ID = Convert.ToInt32(dtunits.Rows[i]["UnitParameter_ID"]),
                    Name = Convert.ToString(dtunits.Rows[i]["Name"]),
                    UnitParameter_Value = Convert.ToString(dtunits.Rows[i]["UnitParameter_Value"]),
                });
            }
            //ViewBag.unitdetails = ProjectUnitLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(ViewUnitLists);
            return Json(ViewUnitLists);

        }
        [HttpPost]
        public List<ival_ProjectApprovalDetails> GetApprovalTypebyreqID()
        {
            List<ival_ProjectApprovalDetails> ListApprovalType = new List<ival_ProjectApprovalDetails>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtApprovalType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovalDetailsbyReqID", hInputPara);
            for (int i = 0; i < dtApprovalType.Rows.Count; i++)
            {

                ListApprovalType.Add(new ival_ProjectApprovalDetails
                {
                    PApproval_ID = Convert.ToInt32(dtApprovalType.Rows[i]["PApproval_ID"]),
                    Approval_Type = Convert.ToString(dtApprovalType.Rows[i]["Approval_Type"]),
                    Approving_Authority = Convert.ToString(dtApprovalType.Rows[i]["Approving_Authority"]),
                    Date_of_ApprovalNew = Convert.ToString(dtApprovalType.Rows[i]["Date_of_Approval"]),


                    Approval_Num = Convert.ToString(dtApprovalType.Rows[i]["Approval_Num"])

                });

            }
            return ListApprovalType;
        }

        [HttpPost]
        public List<ival_ProjectFloorWiseBreakUp> GetFloorwisebreakbyreqID()
        {
            List<ival_ProjectFloorWiseBreakUp> Listfloors = new List<ival_ProjectFloorWiseBreakUp>();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtprojectid = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprjIDByRequestID", hInputPara);
            if (dtprojectid.Rows.Count > 0)
            {


                hInputPara1.Add("@Project_ID", dtprojectid.Rows[0]["Project_ID"].ToString());
                DataTable dtfloor = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorwisebreakbyReqID", hInputPara1);

                for (int i = 0; i < dtfloor.Rows.Count; i++)
                {

                    Listfloors.Add(new ival_ProjectFloorWiseBreakUp
                    {
                        PFloor_ID = Convert.ToInt32(dtfloor.Rows[i]["PFloor_ID"]),
                        Floor_Type = Convert.ToString(dtfloor.Rows[i]["Floor_Type"]),
                        Approved_Built_up_Area = Convert.ToString(dtfloor.Rows[i]["Approved_Built_up_Area"]),
                        Approved_Carpet_Area = Convert.ToString(dtfloor.Rows[i]["Approved_Carpet_Area"]),
                        Measured_Built_up_Area = Convert.ToString(dtfloor.Rows[i]["Measured_Built_up_Area"]),
                        Measured_Carpet_Area = Convert.ToString(dtfloor.Rows[i]["Measured_Carpet_Area"]),
                        Adopted_Area_for_Valuation = Convert.ToString(dtfloor.Rows[i]["Adopted_Area_for_Valuation"]),
                        Adopted_Cost_Construction = Convert.ToString(dtfloor.Rows[i]["Adopted_Cost_Construction"])

                    });

                }
            }
            return Listfloors;
        }

        [HttpPost]
        public List<ival_ProjectBuildingWingFloorDetails> GetFloorListbyreqIDNew()
        {
            List<ival_ProjectBuildingWingFloorDetails> Listsocialinfra = new List<ival_ProjectBuildingWingFloorDetails>();
            hInputPara = new Hashtable();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            sqlDataAccess = new SQLDataAccess();
            DataTable dtsocialinfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorDetailsByReqID", hInputPara);
            if (dtsocialinfra.Rows.Count > 0)
            {
                for (int i = 0; i < dtsocialinfra.Rows.Count; i++)
                {

                    Listsocialinfra.Add(new ival_ProjectBuildingWingFloorDetails
                    {
                        BuildingFloor_ID = Convert.ToInt32(dtsocialinfra.Rows[i]["BuildingFloor_ID"]),
                        FloorType_ID = Convert.ToString(dtsocialinfra.Rows[i]["FloorType_ID"]),
                        Number_Of_Floors = Convert.ToString(dtsocialinfra.Rows[i]["Number_Of_Floors"]),

                    });
                }
            }
            else
            {

            }
            return Listsocialinfra;
        }
        [HttpPost]
        public List<ival_ProjectFloorWiseBreakUp> GetFloorwisebreakbyreqIDtotal()
        {
            List<ival_ProjectFloorWiseBreakUp> Listfloors = new List<ival_ProjectFloorWiseBreakUp>();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtprojectid = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprjIDByRequestID", hInputPara);
            hInputPara1.Add("@Project_ID", dtprojectid.Rows[0]["Project_ID"].ToString());
            DataTable dtfloor = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorwisebreakbyReqIDtotal", hInputPara1);

            for (int i = 0; i < dtfloor.Rows.Count; i++)
            {

                Listfloors.Add(new ival_ProjectFloorWiseBreakUp
                {
                    PFloor_ID = Convert.ToInt32(dtfloor.Rows[i]["PFloor_ID"]),
                    Floor_Type = Convert.ToString(dtfloor.Rows[i]["Floor_Type"]),
                    Approved_Built_up_Area = Convert.ToString(dtfloor.Rows[i]["Approved_Built_up_Area"]),
                    Approved_Carpet_Area = Convert.ToString(dtfloor.Rows[i]["Approved_Carpet_Area"]),
                    Measured_Built_up_Area = Convert.ToString(dtfloor.Rows[i]["Measured_Built_up_Area"]),
                    Measured_Carpet_Area = Convert.ToString(dtfloor.Rows[i]["Measured_Carpet_Area"]),
                    Adopted_Area_for_Valuation = Convert.ToString(dtfloor.Rows[i]["Adopted_Area_for_Valuation"]),
                    Adopted_Cost_Construction = Convert.ToString(dtfloor.Rows[i]["Adopted_Cost_Construction"])

                });

            }
            return Listfloors;
        }

        [HttpPost]
        public List<ival_ProjectBoundaries> GetBoundariesByReqID()
        {
            List<ival_ProjectBoundaries> ListBoundaries = new List<ival_ProjectBoundaries>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtBoundaries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBoudriesByReqID", hInputPara);

            for (int i = 0; i < dtBoundaries.Rows.Count; i++)
            {

                ListBoundaries.Add(new ival_ProjectBoundaries
                {
                    Boundry_ID = Convert.ToInt32(dtBoundaries.Rows[i]["Boundry_ID"]),
                    Boundry_Name = Convert.ToString(dtBoundaries.Rows[i]["Boundry_Name"]),
                    AsPer_Document = Convert.ToString(dtBoundaries.Rows[i]["AsPer_Document"]),
                    AsPer_Site = Convert.ToString(dtBoundaries.Rows[i]["AsPer_Site"]),
                    Plot_Dimentions = Convert.ToString(dtBoundaries.Rows[i]["Plot_Dimentions"]),

                });

            }
            return ListBoundaries;
        }



        [HttpPost]
        public List<ival_ProjectSideMargin> GetSidearginsByReqID()
        {
            List<ival_ProjectSideMargin> ListSideMargins = new List<ival_ProjectSideMargin>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtsidemargin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSideMarginByReqID", hInputPara);

            for (int i = 0; i < dtsidemargin.Rows.Count; i++)
            {

                ListSideMargins.Add(new ival_ProjectSideMargin
                {
                    Side_Margin_ID = Convert.ToInt32(dtsidemargin.Rows[i]["Side_Margin_ID"]),
                    Side_Margin_Description = Convert.ToString(dtsidemargin.Rows[i]["Side_Margin_Description"]),
                    As_per_Approvals = Convert.ToString(dtsidemargin.Rows[i]["As_per_Approvals"]),
                    As_per_Local_Bye_Laws = Convert.ToString(dtsidemargin.Rows[i]["As_per_Local_Bye_Laws"]),
                    As_Actuals = Convert.ToString(dtsidemargin.Rows[i]["As_Actuals"]),
                    Deviation_No = Convert.ToString(dtsidemargin.Rows[i]["Deviation_No"]),
                    Percentage_Deviation = Convert.ToString(dtsidemargin.Rows[i]["Percentage_Deviation"]),

                });

            }
            return ListSideMargins;
        }


        public JsonResult GetUnitPriceDetailsView(string UnitType, string PType)
        {
            try
            {
                List<ival_UnitPriceParametersValue> Listunits = new List<ival_UnitPriceParametersValue>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                int RequestID = Convert.ToInt32(Session["RequestID"]);
                // int RequestID = 45;
                hInputPara.Add("@Request_ID", RequestID);
                hInputPara.Add("@UnitType", UnitType);
                hInputPara.Add("@PType", PType);
                DataTable dtUniPriceParameter = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitPricedetailsBYReqID", hInputPara);
                if (dtUniPriceParameter.Rows.Count > 0 && dtUniPriceParameter != null)
                {
                    for (int i = 0; i < dtUniPriceParameter.Rows.Count; i++)
                    {

                        Listunits.Add(new ival_UnitPriceParametersValue
                        {
                            UnitPriceParameter_ID = Convert.ToInt32(dtUniPriceParameter.Rows[i]["UnitPriceParameterID"]),
                            UnitPriceParaValue_ID = Convert.ToInt32(dtUniPriceParameter.Rows[i]["UnitPriceParaValue_ID"]),
                            Name = Convert.ToString(dtUniPriceParameter.Rows[i]["Name"]),
                            UnitPriceParameter_Value = Convert.ToString(dtUniPriceParameter.Rows[i]["UnitPriceParameter_Value"]),

                        });

                    }

                }
                var jsonSerialiser = new JavaScriptSerializer();
                var jsonvUnitParameterMaster = jsonSerialiser.Serialize(Listunits);
                return Json(jsonvUnitParameterMaster);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetUnitPriceDetailsEditView(string UnitType, string PType)
        {
            try
            {
                List<ival_UnitPriceParametersValue> Listunits = new List<ival_UnitPriceParametersValue>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                int RequestID = Convert.ToInt32(Session["RequestID"]);
                // int RequestID = 45;
                hInputPara.Add("@Request_ID", RequestID);
                hInputPara.Add("@UnitType", UnitType);
                hInputPara.Add("@PType", PType);
                DataTable dtUniPriceParameter = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitPricedetailsEditBYReqID", hInputPara);
                if (dtUniPriceParameter.Rows.Count > 0 && dtUniPriceParameter != null)
                {
                    for (int i = 0; i < dtUniPriceParameter.Rows.Count; i++)
                    {

                        Listunits.Add(new ival_UnitPriceParametersValue
                        {
                            UnitPriceParameter_ID = Convert.ToInt32(dtUniPriceParameter.Rows[i]["UnitPriceParameterID"]),
                            UnitPriceParaValue_ID = Convert.ToInt32(dtUniPriceParameter.Rows[i]["UnitPriceParaValue_ID"]),
                            Name = Convert.ToString(dtUniPriceParameter.Rows[i]["Name"]),
                            UnitPriceParameter_Value = Convert.ToString(dtUniPriceParameter.Rows[i]["UnitPriceParameter_Value"]),
                            UnitPriceSQMt_Value = Convert.ToString(dtUniPriceParameter.Rows[i]["UnitPriceSQMt_Value"]),
                        });

                    }

                }
                var jsonSerialiser = new JavaScriptSerializer();
                var jsonvUnitParameterMaster = jsonSerialiser.Serialize(Listunits);
                return Json(jsonvUnitParameterMaster);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetUnitPriceOtherDetailsView(string UnitType, string PType)
        {
            try
            {
                List<ival_UnitPriceParametersValue> Listunits = new List<ival_UnitPriceParametersValue>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                int RequestID = Convert.ToInt32(Session["RequestID"]);
                // int RequestID = 45;
                hInputPara.Add("@Request_ID", RequestID);
                hInputPara.Add("@UnitType", UnitType);
                hInputPara.Add("@PType", PType);
                DataTable dtUniPriceParameter = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitOtherPricedetailsBYReqID", hInputPara);
                if (dtUniPriceParameter.Rows.Count > 0 && dtUniPriceParameter != null)
                {
                    for (int i = 0; i < dtUniPriceParameter.Rows.Count; i++)
                    {

                        Listunits.Add(new ival_UnitPriceParametersValue
                        {
                            UnitPriceParameter_ID = Convert.ToInt32(dtUniPriceParameter.Rows[i]["UnitPriceParameterID"]),
                            UnitPriceParaValue_ID = Convert.ToInt32(dtUniPriceParameter.Rows[i]["UnitPriceParaValue_ID"]),
                            Name = Convert.ToString(dtUniPriceParameter.Rows[i]["Name"]),
                            UnitPriceParameter_Value = Convert.ToString(dtUniPriceParameter.Rows[i]["UnitPriceParameter_Value"]),
                            UnitPriceSQMt_Value = Convert.ToString(dtUniPriceParameter.Rows[i]["UnitPriceSQMt_Value"]),
                        });

                    }

                }
                var jsonSerialiser = new JavaScriptSerializer();
                var jsonvUnitParameterMaster = jsonSerialiser.Serialize(Listunits);
                return Json(jsonvUnitParameterMaster);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetUnitPriceOtherDetailsViewEdit(string UnitType, string PType)
        {
            try
            {
                List<ival_UnitPriceParametersValue> Listunits = new List<ival_UnitPriceParametersValue>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                int RequestID = Convert.ToInt32(Session["RequestID"]);
                // int RequestID = 45;
                hInputPara.Add("@Request_ID", RequestID);
                hInputPara.Add("@UnitType", UnitType);
                hInputPara.Add("@PType", PType);
                DataTable dtUniPriceParameter = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitOtherPricedetailsBYReqID", hInputPara);
                if (dtUniPriceParameter.Rows.Count > 0 && dtUniPriceParameter != null)
                {
                    for (int i = 0; i < dtUniPriceParameter.Rows.Count; i++)
                    {

                        Listunits.Add(new ival_UnitPriceParametersValue
                        {
                            UnitPriceParameter_ID = Convert.ToInt32(dtUniPriceParameter.Rows[i]["UnitPriceParameterID"]),
                            UnitPriceParaValue_ID = Convert.ToInt32(dtUniPriceParameter.Rows[i]["UnitPriceParaValue_ID"]),
                            Name = Convert.ToString(dtUniPriceParameter.Rows[i]["Name"]),
                            UnitPriceParameter_Value = Convert.ToString(dtUniPriceParameter.Rows[i]["UnitPriceParameter_Value"]),
                            UnitPriceSQMt_Value = Convert.ToString(dtUniPriceParameter.Rows[i]["UnitPriceSQMt_Value"]),
                        });

                    }

                }
                var jsonSerialiser = new JavaScriptSerializer();
                var jsonvUnitParameterMaster = jsonSerialiser.Serialize(Listunits);
                return Json(jsonvUnitParameterMaster);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }



        [HttpPost]
        public List<ival_BrokerDetails> GetbrokerBYreqID()
        {
            List<ival_BrokerDetails> Listbrk = new List<ival_BrokerDetails>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtbroker = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBrokerDetailsBYReqIDView", hInputPara);
            for (int i = 0; i < dtbroker.Rows.Count; i++)
            {

                Listbrk.Add(new ival_BrokerDetails
                {
                    Broker_IDs = Convert.ToInt32(dtbroker.Rows[i]["Broker_IDs"]),
                    Broker_ID = Convert.ToInt32(dtbroker.Rows[i]["Broker_ID"]),
                    Unit_ID = Convert.ToInt32(dtbroker.Rows[i]["Unit_ID"]),
                    Project_ID = Convert.ToInt32(dtbroker.Rows[i]["Project_ID"]),
                    Broker_Name = Convert.ToString(dtbroker.Rows[i]["Broker_Name"]),
                    Firm_Name = Convert.ToString(dtbroker.Rows[i]["Firm_Name"]),
                    Rate_Quoted_For_Flat = Convert.ToString(dtbroker.Rows[i]["Rate_Quoted_For_Flat"]),
                    Rate_Quoted_For_Land = Convert.ToString(dtbroker.Rows[i]["Rate_Quoted_For_Land"]),
                    Contact_Num = Convert.ToString(dtbroker.Rows[i]["Contact_Num"])
                });

            }
            return Listbrk;
        }

        [HttpPost]
        public List<iva_Comparable_Properties> GetComprabledetreqID()
        {
            List<iva_Comparable_Properties> Listcomp = new List<iva_Comparable_Properties>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtcomp = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetComparabledetailsViewBYReqID", hInputPara);
            //DataTable dtbroker = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBrokerDetailsBYReqID", hInputPara);

            for (int i = 0; i < dtcomp.Rows.Count; i++)
            {

                Listcomp.Add(new iva_Comparable_Properties
                {
                    Property_ID = Convert.ToInt32(dtcomp.Rows[i]["Property_ID"]),
                    Property_IDs = Convert.ToInt32(dtcomp.Rows[i]["Property_IDs"]),
                    Project_Name = Convert.ToString(dtcomp.Rows[i]["Project_Name"]),
                    Dist_Subject_Property = Convert.ToString(dtcomp.Rows[i]["Dist_Subject_Property"]),
                    Comparable_Unit_Details = Convert.ToString(dtcomp.Rows[i]["Comparable_Unit_Details"]),
                    Quality_And_Amenities = Convert.ToString(dtcomp.Rows[i]["Quality_And_Amenities"]),
                    Ongoing_Rate = Convert.ToString(dtcomp.Rows[i]["Ongoing_Rate"])
                });

            }
            return Listcomp;
        }

        public ActionResult PropertydetailsView()
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRetailPropertyByReqID", hInputPara);
            if (dt.Rows.Count > 0 && dt.Columns.Count > 0)
            {
                if (DBNull.Value.Equals(dt.Rows[0]["RequestID"]))
                {
                    ViewBag.RequestID = Convert.ToInt32(!DBNull.Value.Equals(dt.Rows[0]["RequestID"]));
                }
                else
                {
                    ViewBag.RequestID = Convert.ToInt32(dt.Rows[0]["RequestID"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Builder_Group_ID"]))
                {
                    ViewBag.Builder_Group_ID = Convert.ToInt32(!DBNull.Value.Equals(dt.Rows[0]["Builder_Group_ID"]));

                }
                else
                {
                    ViewBag.Builder_Group_ID = Convert.ToInt32(dt.Rows[0]["Builder_Group_ID"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Builder_Company_ID"]))
                {
                    ViewBag.Builder_Company_ID = Convert.ToInt32(!DBNull.Value.Equals(dt.Rows[0]["Builder_Company_ID"]));

                }
                else
                {
                    ViewBag.Builder_Company_ID = Convert.ToInt32(dt.Rows[0]["Builder_Company_ID"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Is_Active"]))
                {
                    ViewBag.Is_Active = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Is_Active"]));

                }
                else
                {
                    ViewBag.Is_Active = Convert.ToString(dt.Rows[0]["Is_Active"]);


                }
                if (DBNull.Value.Equals(dt.Rows[0]["Is_Retail_Individual"]))
                {
                    ViewBag.Is_Retail_Individual = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Is_Retail_Individual"]));

                }
                else
                {
                    ViewBag.Is_Retail_Individual = Convert.ToString(dt.Rows[0]["Is_Retail_Individual"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Property_Type"]))
                {
                    ViewBag.Property_Type = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Property_Type"]));

                }
                else
                {
                    ViewBag.Property_Type = Convert.ToString(dt.Rows[0]["Property_Type"]);
                    Session["Property_Type"] = ViewBag.Property_Type;
                }
                if (DBNull.Value.Equals(dt.Rows[0]["TypeOfSelect"]))
                {
                    ViewBag.TypeOfSelect = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["TypeOfSelect"]));

                }
                else
                {
                    ViewBag.TypeOfSelect = Convert.ToString(dt.Rows[0]["TypeOfSelect"]);
                    Session["TypeOfSelect"] = ViewBag.TypeOfSelect;

                }
                if (DBNull.Value.Equals(dt.Rows[0]["noofwings"]))
                {
                    ViewBag.txtwingsno = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["noofwings"]));

                }
                else
                {
                    ViewBag.txtwingsno = Convert.ToString(dt.Rows[0]["noofwings"]);

                }



                if (DBNull.Value.Equals(dt.Rows[0]["TypeOfUnit"]))
                {
                    ViewBag.TypeOfUnit = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["TypeOfUnit"]));
                }
                else
                {
                    ViewBag.TypeOfUnit = Convert.ToString(dt.Rows[0]["TypeOfUnit"]);
                    Session["TypeOfUnit"] = ViewBag.TypeOfUnit;
                }

                if (DBNull.Value.Equals(dt.Rows[0]["StatusOfProperty"]))
                {
                    ViewBag.StatusOfProperty = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["StatusOfProperty"]));

                }
                else
                {
                    ViewBag.StatusOfProperty = Convert.ToString(dt.Rows[0]["StatusOfProperty"]);
                    Session["StatusOfProperty"] = ViewBag.StatusOfProperty;
                }

                if (DBNull.Value.Equals(dt.Rows[0]["Plot_Number"]))
                {
                    ViewBag.PlotBunglowNumber = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Plot_Number"]));

                }
                else
                {
                    ViewBag.PlotBunglowNumber = Convert.ToString(dt.Rows[0]["Plot_Number"]);

                }


                if (DBNull.Value.Equals(dt.Rows[0]["Ward_No"]))
                {
                    ViewBag.Ward_Number = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Ward_No"]));

                }
                else
                {
                    ViewBag.Ward_Number = Convert.ToString(dt.Rows[0]["Ward_No"]);

                }


                if (DBNull.Value.Equals(dt.Rows[0]["Municipal_No"]))
                {
                    ViewBag.Municipal_Number = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Municipal_No"]));

                }
                else
                {
                    ViewBag.Municipal_Number = Convert.ToString(dt.Rows[0]["Municipal_No"]);

                }


                if (DBNull.Value.Equals(dt.Rows[0]["Plot_No"]))
                {
                    ViewBag.Plot_Number = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Plot_No"]));

                }
                else
                {
                    ViewBag.Plot_Number = Convert.ToString(dt.Rows[0]["Plot_No"]);

                }


                if (DBNull.Value.Equals(dt.Rows[0]["Type_Ownership"]))
                {
                    ViewBag.Type_Ownership = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Type_Ownership"]));

                }
                else
                {
                    ViewBag.Type_Ownership = Convert.ToString(dt.Rows[0]["Type_Ownership"]);

                }


                if (DBNull.Value.Equals(dt.Rows[0]["Leased_Year"]))
                {
                    ViewBag.Lease_YearN = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Leased_Year"]));

                }
                else
                {
                    ViewBag.Lease_YearN = Convert.ToString(dt.Rows[0]["Leased_Year"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Unit_On_floor"]))
                {
                    ViewBag.UF = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Unit_On_floor"]));

                }
                else
                {
                    ViewBag.UF = Convert.ToString(dt.Rows[0]["Unit_On_floor"]);

                }




                if (DBNull.Value.Equals(dt.Rows[0]["Bunglow_Number"]))
                {
                    ViewBag.BunglowNumber = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Bunglow_Number"]));

                }
                else
                {
                    ViewBag.BunglowNumber = Convert.ToString(dt.Rows[0]["Bunglow_Number"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["UnitNumber"]))
                {
                    ViewBag.UnitNumber = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["UnitNumber"]));
                }
                else
                {
                    ViewBag.UnitNumber = Convert.ToString(dt.Rows[0]["UnitNumber"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Project_Name"]))
                {
                    ViewBag.Project_Name = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Project_Name"]));

                }
                else
                {
                    ViewBag.Project_Name = Convert.ToString(dt.Rows[0]["Project_Name"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Building_Name_RI"]))
                {
                    ViewBag.Building_Name_RI = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Building_Name_RI"]));

                }
                else
                {
                    ViewBag.Building_Name_RI = Convert.ToString(dt.Rows[0]["Building_Name_RI"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Wing_Name_RI"]))
                {
                    ViewBag.Wing_Name_RI = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Wing_Name_RI"]));
                }
                else
                {
                    ViewBag.Wing_Name_RI = Convert.ToString(dt.Rows[0]["Wing_Name_RI"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Survey_Number"]))
                {
                    ViewBag.Survey_Number = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Survey_Number"]));

                }
                else
                {
                    ViewBag.Survey_Number = Convert.ToString(dt.Rows[0]["Survey_Number"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Street"]))
                {
                    ViewBag.Street = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Street"]));
                }
                else
                {
                    ViewBag.Street = Convert.ToString(dt.Rows[0]["Street"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Locality"]))
                {

                    ViewBag.Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Locality"]));
                }
                else
                {

                    ViewBag.Locality = Convert.ToString(dt.Rows[0]["Locality"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["City"]))
                {
                    ViewBag.City = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["City"]));
                }
                else
                {
                    ViewBag.City = Convert.ToString(dt.Rows[0]["City"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["District"]))
                {
                    ViewBag.District = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["District"]));
                }
                else
                {
                    ViewBag.District = Convert.ToString(dt.Rows[0]["District"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["State"]))
                {
                    ViewBag.State = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["State"]));
                }
                else
                {
                    ViewBag.State = Convert.ToString(dt.Rows[0]["State"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["PinCode"]))
                {
                    ViewBag.PinCode = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["PinCode"]));
                }
                else
                {
                    ViewBag.PinCode = Convert.ToString(dt.Rows[0]["PinCode"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["NearBy_Landmark"]))
                {
                    ViewBag.NearBy_Landmark = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["NearBy_Landmark"]));

                }
                else
                {
                    ViewBag.NearBy_Landmark = Convert.ToString(dt.Rows[0]["NearBy_Landmark"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Nearest_RailwayStation"]))
                {
                    ViewBag.Nearest_RailwayStation = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Nearest_RailwayStation"]));

                }
                else
                {
                    ViewBag.Nearest_RailwayStation = Convert.ToString(dt.Rows[0]["Nearest_RailwayStation"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Nearest_BusStop"]))
                {
                    ViewBag.Nearest_BusStop = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Nearest_BusStop"]));
                }
                else
                {
                    ViewBag.Nearest_BusStop = Convert.ToString(dt.Rows[0]["Nearest_BusStop"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Nearest_Market"]))
                {
                    ViewBag.Nearest_Market = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Nearest_Market"]));
                }
                else
                {
                    ViewBag.Nearest_Market = Convert.ToString(dt.Rows[0]["Nearest_Market"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["Nearest_Hospital"]))
                {
                    ViewBag.Nearest_Hospital = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Nearest_Hospital"]));
                }
                else
                {
                    ViewBag.Nearest_Hospital = Convert.ToString(dt.Rows[0]["Nearest_Hospital"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Address1"]))
                {
                    ViewBag.Legal_Address1 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Address1"]));
                }
                else
                {
                    ViewBag.Legal_Address1 = Convert.ToString(dt.Rows[0]["Legal_Address1"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Address2"]))
                {
                    ViewBag.Legal_Address2 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Address2"]));
                }
                else
                {
                    ViewBag.Legal_Address2 = Convert.ToString(dt.Rows[0]["Legal_Address2"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Locality"]))
                {
                    ViewBag.Legal_Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Locality"]));
                }
                else
                {
                    ViewBag.Legal_Locality = Convert.ToString(dt.Rows[0]["Legal_Locality"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Street"]))
                {
                    ViewBag.Legal_Street = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Street"]));
                }
                else
                {
                    ViewBag.Legal_Street = Convert.ToString(dt.Rows[0]["Legal_Street"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_City"]))
                {
                    ViewBag.Legal_City = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_City"]));
                }
                else
                {
                    ViewBag.Legal_City = Convert.ToString(dt.Rows[0]["Legal_City"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_District"]))
                {
                    ViewBag.Legal_District = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_District"]));
                }
                else
                {
                    ViewBag.Legal_District = Convert.ToString(dt.Rows[0]["Legal_District"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_State"]))
                {
                    ViewBag.Legal_State = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_State"]));
                }
                else
                {
                    ViewBag.Legal_State = Convert.ToString(dt.Rows[0]["Legal_State"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Pincode"]))
                {
                    ViewBag.Legal_Pincode = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Pincode"]));
                }
                else
                {
                    ViewBag.Legal_Pincode = Convert.ToString(dt.Rows[0]["Legal_Pincode"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["RERA_Approval_Num"]))
                {
                    ViewBag.RERA_Approval_Num = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["RERA_Approval_Num"]));
                }
                else
                {
                    ViewBag.RERA_Approval_Num = Convert.ToString(dt.Rows[0]["RERA_Approval_Num"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approval_Flag"]))
                {
                    ViewBag.Approval_Flag = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approval_Flag"]));

                }
                else
                {
                    ViewBag.Approval_Flag = Convert.ToString(dt.Rows[0]["Approval_Flag"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Total_Land_Area"]))
                {
                    ViewBag.Total_Land_Area = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Total_Land_Area"]));

                }
                else
                {
                    ViewBag.Total_Land_Area = Convert.ToString(dt.Rows[0]["Total_Land_Area"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Total_Permissible_FSI"]))
                {
                    ViewBag.Total_Permissible_FSI = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Total_Permissible_FSI"]));

                }
                else
                {
                    ViewBag.Total_Permissible_FSI = Convert.ToString(dt.Rows[0]["Total_Permissible_FSI"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_Builtup_Area"]))
                {
                    ViewBag.Approved_Builtup_Area = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_Builtup_Area"]));

                }
                else
                {
                    ViewBag.Approved_Builtup_Area = Convert.ToString(dt.Rows[0]["Approved_Builtup_Area"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["Postal_Address1"]))
                {

                    ViewBag.Postal_Address1 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Address1"]));
                }
                else
                {
                    ViewBag.Postal_Address1 = Convert.ToString(dt.Rows[0]["Postal_Address1"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Buildings"]))
                {
                    ViewBag.Approved_No_of_Buildings = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Buildings"]));
                }
                else
                {
                    ViewBag.Approved_No_of_Buildings = Convert.ToString(dt.Rows[0]["Approved_No_of_Buildings"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_plotsorflats"]))
                {
                    ViewBag.Approved_No_of_plotsorflats = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_plotsorflats"]));
                }
                else
                {
                    ViewBag.Approved_No_of_plotsorflats = Convert.ToString(dt.Rows[0]["Approved_No_of_plotsorflats"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Wings"]))
                {
                    ViewBag.Approved_No_of_Wings = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Wings"]));
                }
                else
                {
                    ViewBag.Approved_No_of_Wings = Convert.ToString(dt.Rows[0]["Approved_No_of_Wings"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Floors"]))
                {

                    ViewBag.Approved_No_of_Floors = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Floors"]));
                }
                else
                {

                    ViewBag.Approved_No_of_Floors = Convert.ToString(dt.Rows[0]["Approved_No_of_Floors"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Units"]))
                {
                    ViewBag.Approved_No_of_Units = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Units"]));
                }
                else
                {
                    ViewBag.Approved_No_of_Units = Convert.ToString(dt.Rows[0]["Approved_No_of_Units"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Actual_No_Floors_G"]))
                {
                    ViewBag.Actual_No_Floors_G = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Actual_No_Floors_G"]));
                }
                else
                {
                    ViewBag.Actual_No_Floors_G = Convert.ToString(dt.Rows[0]["Actual_No_Floors_G"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Floor_Of_Unit"]))
                {
                    ViewBag.Floor_Of_Unit = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Floor_Of_Unit"]));
                }
                else
                {
                    ViewBag.Floor_Of_Unit = Convert.ToString(dt.Rows[0]["Floor_Of_Unit"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Property_Description"]))
                {
                    ViewBag.Property_Description = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Property_Description"]));
                }
                else
                {
                    ViewBag.Property_Description = Convert.ToString(dt.Rows[0]["Property_Description"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Type_Of_Structure"]))
                {
                    ViewBag.Type_Of_Structure = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Type_Of_Structure"]));
                }
                else
                {
                    ViewBag.Type_Of_Structure = Convert.ToString(dt.Rows[0]["Type_Of_Structure"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_Usage_Of_Property"]))
                {
                    ViewBag.Approved_Usage_Of_Property = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_Usage_Of_Property"]));
                }
                else
                {
                    ViewBag.Approved_Usage_Of_Property = Convert.ToString(dt.Rows[0]["Approved_Usage_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Actual_Usage_Of_Property"]))
                {
                    ViewBag.Actual_Usage_Of_Property = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Actual_Usage_Of_Property"]));
                }
                else
                {
                    ViewBag.Actual_Usage_Of_Property = Convert.ToString(dt.Rows[0]["Actual_Usage_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Current_Age_Of_Property"]))
                {
                    ViewBag.Current_Age_Of_Property = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Current_Age_Of_Property"]));
                }
                else
                {
                    ViewBag.Current_Age_Of_Property = Convert.ToString(dt.Rows[0]["Current_Age_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Residual_Age_Of_Property"]))
                {
                    ViewBag.Residual_Age_Of_Property = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Residual_Age_Of_Property"]));
                }
                else
                {
                    ViewBag.Residual_Age_Of_Property = Convert.ToString(dt.Rows[0]["Residual_Age_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Common_Areas"]))
                {
                    ViewBag.Common_Areas = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Common_Areas"]));
                }
                else
                {
                    ViewBag.Common_Areas = Convert.ToString(dt.Rows[0]["Common_Areas"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Facilities"]))
                {
                    ViewBag.Facilities = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Facilities"]));
                }
                else
                {
                    ViewBag.Facilities = Convert.ToString(dt.Rows[0]["Facilities"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Anyother_Observation"]))
                {
                    ViewBag.Anyother_Observation = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Anyother_Observation"]));
                }
                else
                {
                    ViewBag.Anyother_Observation = Convert.ToString(dt.Rows[0]["Anyother_Observation"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Roofing_Terracing"]))
                {
                    ViewBag.Roofing_Terracing = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Roofing_Terracing"]));
                }
                else
                {
                    ViewBag.Roofing_Terracing = Convert.ToString(dt.Rows[0]["Roofing_Terracing"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Quality_Of_Fixture"]))
                {
                    ViewBag.Quality_Of_Fixture = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Quality_Of_Fixture"]));
                }
                else
                {
                    ViewBag.Quality_Of_Fixture = Convert.ToString(dt.Rows[0]["Quality_Of_Fixture"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Quality_Of_Construction"]))
                {
                    ViewBag.Quality_Of_Construction = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Quality_Of_Construction"]));
                }
                else
                {
                    ViewBag.Quality_Of_Construction = Convert.ToString(dt.Rows[0]["Quality_Of_Construction"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Maintenance_Of_Property"]))
                {

                    ViewBag.Maintenance_Of_Property = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Maintenance_Of_Property"]));
                }
                else
                {

                    ViewBag.Maintenance_Of_Property = Convert.ToString(dt.Rows[0]["Maintenance_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Marketability_Of_Property"]))
                {
                    ViewBag.Marketability_Of_Property = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Marketability_Of_Property"]));
                }
                else
                {
                    ViewBag.Marketability_Of_Property = Convert.ToString(dt.Rows[0]["Marketability_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Occupancy_Details"]))
                {
                    ViewBag.Occupancy_Details = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Occupancy_Details"]));
                }
                else
                {

                    ViewBag.Occupancy_Details = Convert.ToString(dt.Rows[0]["Occupancy_Details"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Renting_Potential"]))
                {
                    ViewBag.Renting_Potential = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Renting_Potential"]));
                }
                else
                {
                    ViewBag.Renting_Potential = Convert.ToString(dt.Rows[0]["Renting_Potential"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Expected_Monthly_Rental"]))
                {
                    ViewBag.Expected_Monthly_Rental = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Expected_Monthly_Rental"]));
                }
                else
                {
                    ViewBag.Expected_Monthly_Rental = Convert.ToString(dt.Rows[0]["Expected_Monthly_Rental"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Name_Of_Occupant"]))
                {
                    ViewBag.Name_Of_Occupant = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Name_Of_Occupant"]));
                }
                else
                {
                    ViewBag.Name_Of_Occupant = Convert.ToString(dt.Rows[0]["Name_Of_Occupant"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Occupied_Since"]))
                {
                    ViewBag.Occupied_Since = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Occupied_Since"]));
                }
                else
                {
                    ViewBag.Occupied_Since = Convert.ToString(dt.Rows[0]["Occupied_Since"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Monthly_Rental"]))
                {
                    ViewBag.Monthly_Rental = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Monthly_Rental"]));
                }
                else
                {
                    ViewBag.Monthly_Rental = Convert.ToString(dt.Rows[0]["Monthly_Rental"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Balanced_Leased_Period"]))
                {
                    ViewBag.Balanced_Leased_Period = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Balanced_Leased_Period"]));
                }
                else
                {
                    ViewBag.Balanced_Leased_Period = Convert.ToString(dt.Rows[0]["Balanced_Leased_Period"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Specifications"]))
                {
                    ViewBag.Specifications = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Specifications"]));
                }
                else
                {
                    ViewBag.Specifications = Convert.ToString(dt.Rows[0]["Specifications"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Amenities"]))
                {
                    ViewBag.Amenities = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Amenities"]));
                }
                else
                {
                    ViewBag.Amenities = Convert.ToString(dt.Rows[0]["Amenities"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Deviation_Description"]))
                {
                    ViewBag.Deviation_Description = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Deviation_Description"]));

                }
                else
                {
                    ViewBag.Deviation_Description = Convert.ToString(dt.Rows[0]["Deviation_Description"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Cord_Latitude"]))
                {
                    ViewBag.Cord_Latitude = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Cord_Latitude"]));
                }
                else
                {
                    ViewBag.Cord_Latitude = Convert.ToString(dt.Rows[0]["Cord_Latitude"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Cord_Longitude"]))
                {
                    ViewBag.Cord_Longitude = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Cord_Longitude"]));
                }
                else
                {
                    ViewBag.Cord_Longitude = Convert.ToString(dt.Rows[0]["Cord_Longitude"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width1"]))
                {
                    ViewBag.AdjoiningRoad_Width1 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width1"]));
                }
                else
                {
                    ViewBag.AdjoiningRoad_Width1 = Convert.ToString(dt.Rows[0]["AdjoiningRoad_Width1"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width2"]))
                {
                    ViewBag.AdjoiningRoad_Width2 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width2"]));
                }
                else
                {
                    ViewBag.AdjoiningRoad_Width2 = Convert.ToString(dt.Rows[0]["AdjoiningRoad_Width2"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width3"]))
                {
                    ViewBag.AdjoiningRoad_Width3 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width3"]));
                }
                else
                {
                    ViewBag.AdjoiningRoad_Width3 = Convert.ToString(dt.Rows[0]["AdjoiningRoad_Width3"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width4"]))
                {
                    ViewBag.AdjoiningRoad_Width4 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width4"]));
                }
                else
                {
                    ViewBag.AdjoiningRoad_Width4 = Convert.ToString(dt.Rows[0]["AdjoiningRoad_Width4"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Social_Development"]))
                {
                    ViewBag.Social_Development = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Social_Development"]));
                }
                else
                {
                    ViewBag.Social_Development = Convert.ToString(dt.Rows[0]["Social_Development"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Social_Infrastructure"]))
                {
                    ViewBag.Social_Infrastructure = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Social_Infrastructure"]));
                }
                else
                {
                    ViewBag.Social_Infrastructure = Convert.ToString(dt.Rows[0]["Social_Infrastructure"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Type_of_Locality"]))
                {
                    ViewBag.Type_of_Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Type_of_Locality"]));
                }
                else
                {
                    ViewBag.Type_of_Locality = Convert.ToString(dt.Rows[0]["Type_of_Locality"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Class_of_Locality"]))
                {

                    ViewBag.Class_of_Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Class_of_Locality"]));
                }
                else
                {

                    ViewBag.Class_of_Locality = Convert.ToString(dt.Rows[0]["Class_of_Locality"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["RERA_Registration_No"]))
                {
                    ViewBag.RERA_Registration_No = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["RERA_Registration_No"]));
                }
                else
                {
                    ViewBag.RERA_Registration_No = Convert.ToString(dt.Rows[0]["RERA_Registration_No"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["DistFrom_City_Center"]))
                {
                    ViewBag.DistFrom_City_Center = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["DistFrom_City_Center"]));

                }
                else
                {
                    ViewBag.DistFrom_City_Center = Convert.ToString(dt.Rows[0]["DistFrom_City_Center"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Locality_Remarks"]))
                {
                    ViewBag.LocRemarks = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Locality_Remarks"]));

                }
                else
                {
                    ViewBag.LocRemarks = Convert.ToString(dt.Rows[0]["Locality_Remarks"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["Village_Name"]))
                {
                    ViewBag.Village_Name = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Village_Name"]));

                }
                else
                {
                    ViewBag.Village_Name = Convert.ToString(dt.Rows[0]["Village_Name"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["Sub_Locality"]))
                {
                    ViewBag.Sub_Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Sub_Locality"]));

                }
                else
                {
                    ViewBag.Sub_Locality = Convert.ToString(dt.Rows[0]["Sub_Locality"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["Sub_Locality"]))
                {
                    ViewBag.Locality_Remarks = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Sub_Locality"]));

                }
                else
                {
                    ViewBag.Locality_Remarks = Convert.ToString(dt.Rows[0]["Sub_Locality"]);

                }

                //ViewBag.Postal_Address2 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Address2"]));
                //ViewBag.Postal_Street = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Street"]));
                //ViewBag.Postal_Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Locality"]));
                //ViewBag.Postal_City = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_City"]));
                //ViewBag.Postal_District = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_District"]));
                //ViewBag.Postal_State = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_State"]));
                //ViewBag.Postal_Pincode = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Pincode"]));
                //ViewBag.Postal_NearbyLandmark = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_NearbyLandmark"]));






            }
            else
            {
                return RedirectToAction("Index", "NewRequestor", new { area = "Index" });
            }
            ViewModel vm = new ViewModel();
            vm.ival_LocalTransportForProject = Gettransport();
            vm.ival_SocialDevelopmentForProject = GetSocialInfra();
            vm.ival_BasicInfraAvailability = GetSocialBasicInfra();
            vm.ival_ProjectBuildingWingFloorDetails = GetFloorListbyreqID();
            return View(vm);
        }

        public ActionResult EditPropertydetailsView()
        {
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRetailPropertyByReqID", hInputPara);
            DataTable dtprojectid = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprjIDByRequestID", hInputPara);

            //changes by punam
            DataTable dtsocial = new DataTable();
            DataTable dtbasicinfra = new DataTable();
            DataTable dtlocaltransport = new DataTable();
            if (dtprojectid.Rows.Count > 0)
            {
                hInputPara1.Add("@Project_ID", dtprojectid.Rows[0]["Project_ID"].ToString());
                dtsocial = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectSocialDev", hInputPara1);
                dtbasicinfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectBasicInfraAvailability", hInputPara1);
                dtlocaltransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalTransport", hInputPara1);

            }

            if (dt.Rows.Count > 0 && dt.Columns.Count > 0)
            {
                if (DBNull.Value.Equals(dt.Rows[0]["RequestID"]))
                {
                    ViewBag.RequestID = Convert.ToInt32(!DBNull.Value.Equals(dt.Rows[0]["RequestID"]));
                }
                else
                {
                    ViewBag.RequestID = Convert.ToInt32(dt.Rows[0]["RequestID"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Builder_Group_ID"]))
                {
                    ViewBag.Builder_Group_ID = Convert.ToInt32(!DBNull.Value.Equals(dt.Rows[0]["Builder_Group_ID"]));

                }
                else
                {
                    ViewBag.Builder_Group_ID = Convert.ToInt32(dt.Rows[0]["Builder_Group_ID"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Builder_Company_ID"]))
                {
                    ViewBag.Builder_Company_ID = Convert.ToInt32(!DBNull.Value.Equals(dt.Rows[0]["Builder_Company_ID"]));

                }
                else
                {
                    ViewBag.Builder_Company_ID = Convert.ToInt32(dt.Rows[0]["Builder_Company_ID"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Is_Active"]))
                {
                    ViewBag.Is_Active = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Is_Active"]));

                }
                else
                {
                    ViewBag.Is_Active = Convert.ToString(dt.Rows[0]["Is_Active"]);


                }
                if (DBNull.Value.Equals(dt.Rows[0]["Is_Retail_Individual"]))
                {
                    ViewBag.Is_Retail_Individual = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Is_Retail_Individual"]));

                }
                else
                {
                    ViewBag.Is_Retail_Individual = Convert.ToString(dt.Rows[0]["Is_Retail_Individual"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Property_Type"]))
                {
                    ViewBag.Property_Type = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Property_Type"]));

                }
                else
                {
                    ViewBag.Property_Type = Convert.ToString(dt.Rows[0]["Property_Type"]);
                    Session["Property_Type"] = ViewBag.Property_Type;
                }
                if (DBNull.Value.Equals(dt.Rows[0]["TypeOfSelect"]))
                {
                    ViewBag.TypeOfSelect = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["TypeOfSelect"]));

                }
                else
                {
                    ViewBag.TypeOfSelect = Convert.ToString(dt.Rows[0]["TypeOfSelect"]);
                    Session["TypeOfSelect"] = ViewBag.TypeOfSelect;

                }
                if (DBNull.Value.Equals(dt.Rows[0]["TypeOfUnit"]))
                {
                    ViewBag.TypeOfUnit = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["TypeOfUnit"]));
                }
                else
                {
                    ViewBag.TypeOfUnit = Convert.ToString(dt.Rows[0]["TypeOfUnit"]);
                    Session["TypeOfUnit"] = ViewBag.TypeOfUnit;
                }

                if (DBNull.Value.Equals(dt.Rows[0]["StatusOfProperty"]))
                {
                    ViewBag.StatusOfProperty = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["StatusOfProperty"]));

                }
                else
                {
                    ViewBag.StatusOfProperty = Convert.ToString(dt.Rows[0]["StatusOfProperty"]);
                    Session["StatusOfProperty"] = ViewBag.StatusOfProperty;
                }

                if (DBNull.Value.Equals(dt.Rows[0]["Plot_Number"]))
                {
                    ViewBag.PlotBunglowNumber = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Plot_Number"]));

                }
                else
                {
                    ViewBag.PlotBunglowNumber = Convert.ToString(dt.Rows[0]["Plot_Number"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Bunglow_Number"]))
                {
                    ViewBag.BunglowNumber = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Bunglow_Number"]));

                }
                else
                {
                    ViewBag.BunglowNumber = Convert.ToString(dt.Rows[0]["Bunglow_Number"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["UnitNumber"]))
                {
                    ViewBag.UnitNumber = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["UnitNumber"]));
                }
                else
                {
                    ViewBag.UnitNumber = Convert.ToString(dt.Rows[0]["UnitNumber"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Project_Name"]))
                {
                    ViewBag.Project_Name = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Project_Name"]));

                }
                else
                {
                    ViewBag.Project_Name = Convert.ToString(dt.Rows[0]["Project_Name"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Building_Name_RI"]))
                {
                    ViewBag.Building_Name_RI = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Building_Name_RI"]));

                }
                else
                {
                    ViewBag.Building_Name_RI = Convert.ToString(dt.Rows[0]["Building_Name_RI"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Wing_Name_RI"]))
                {
                    ViewBag.Wing_Name_RI = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Wing_Name_RI"]));
                }
                else
                {
                    ViewBag.Wing_Name_RI = Convert.ToString(dt.Rows[0]["Wing_Name_RI"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Survey_Number"]))
                {
                    ViewBag.Survey_Number = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Survey_Number"]));

                }
                else
                {
                    ViewBag.Survey_Number = Convert.ToString(dt.Rows[0]["Survey_Number"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Street"]))
                {
                    ViewBag.Street = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Street"]));
                }
                else
                {
                    ViewBag.Street = Convert.ToString(dt.Rows[0]["Street"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Locality"]))
                {

                    ViewBag.Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Locality"]));
                }
                else
                {

                    ViewBag.Locality = Convert.ToString(dt.Rows[0]["Locality"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["City"]))
                {
                    ViewBag.City = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["City"]));
                }
                else
                {
                    ViewBag.City = Convert.ToString(dt.Rows[0]["City"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["District"]))
                {
                    ViewBag.District = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["District"]));
                }
                else
                {
                    ViewBag.District = Convert.ToString(dt.Rows[0]["District"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["State"]))
                {
                    ViewBag.SelectedModelstates = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["State"]));
                }
                else
                {
                    ViewBag.SelectedModelstates = Convert.ToString(dt.Rows[0]["State"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["PinCode"]))
                {
                    ViewBag.SelectedModelpincode = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["PinCode"]));
                }
                else
                {
                    ViewBag.SelectedModelpincode = Convert.ToString(dt.Rows[0]["PinCode"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["NearBy_Landmark"]))
                {
                    ViewBag.NearBy_Landmark = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["NearBy_Landmark"]));

                }
                else
                {
                    ViewBag.NearBy_Landmark = Convert.ToString(dt.Rows[0]["NearBy_Landmark"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Nearest_RailwayStation"]))
                {
                    ViewBag.Nearest_RailwayStation = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Nearest_RailwayStation"]));

                }
                else
                {
                    ViewBag.Nearest_RailwayStation = Convert.ToString(dt.Rows[0]["Nearest_RailwayStation"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Nearest_BusStop"]))
                {
                    ViewBag.Nearest_BusStop = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Nearest_BusStop"]));
                }
                else
                {
                    ViewBag.Nearest_BusStop = Convert.ToString(dt.Rows[0]["Nearest_BusStop"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Nearest_Market"]))
                {
                    ViewBag.Nearest_Market = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Nearest_Market"]));
                }
                else
                {
                    ViewBag.Nearest_Market = Convert.ToString(dt.Rows[0]["Nearest_Market"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["Nearest_Hospital"]))
                {
                    ViewBag.Nearest_Hospital = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Nearest_Hospital"]));
                }
                else
                {
                    ViewBag.Nearest_Hospital = Convert.ToString(dt.Rows[0]["Nearest_Hospital"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["Type_ownership"]))
                {
                    ViewBag.Type_ownership = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Type_ownership"]));
                }
                else
                {
                    ViewBag.Type_ownership = Convert.ToString(dt.Rows[0]["Type_ownership"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["leased_year"]))
                {
                    ViewBag.leased_year = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["leased_year"]));
                }
                else
                {
                    ViewBag.leased_year = Convert.ToString(dt.Rows[0]["leased_year"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Address2"]))
                {
                    ViewBag.Legal_Address2 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Address2"]));
                }
                else
                {
                    ViewBag.Legal_Address2 = Convert.ToString(dt.Rows[0]["Legal_Address2"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Locality"]))
                {
                    ViewBag.Legal_Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Locality"]));
                }
                else
                {
                    ViewBag.Legal_Locality = Convert.ToString(dt.Rows[0]["Legal_Locality"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Street"]))
                {
                    ViewBag.Legal_Street = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Street"]));
                }
                else
                {
                    ViewBag.Legal_Street = Convert.ToString(dt.Rows[0]["Legal_Street"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_City"]))
                {
                    ViewBag.Legal_City = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_City"]));
                }
                else
                {
                    ViewBag.Legal_City = Convert.ToString(dt.Rows[0]["Legal_City"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_District"]))
                {
                    ViewBag.Legal_District = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_District"]));
                }
                else
                {
                    ViewBag.Legal_District = Convert.ToString(dt.Rows[0]["Legal_District"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_State"]))
                {
                    ViewBag.Legal_State = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_State"]));
                }
                else
                {
                    ViewBag.Legal_State = Convert.ToString(dt.Rows[0]["Legal_State"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Pincode"]))
                {
                    ViewBag.Legal_Pincode = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Pincode"]));
                }
                else
                {
                    ViewBag.Legal_Pincode = Convert.ToString(dt.Rows[0]["Legal_Pincode"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["RERA_Approval_Num"]))
                {
                    ViewBag.RERA_Approval_Num = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["RERA_Approval_Num"]));
                }
                else
                {
                    ViewBag.RERA_Approval_Num = Convert.ToString(dt.Rows[0]["RERA_Approval_Num"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approval_Flag"]))
                {
                    ViewBag.Approval_Flag = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approval_Flag"]));

                }
                else
                {
                    ViewBag.Approval_Flag = Convert.ToString(dt.Rows[0]["Approval_Flag"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Total_Land_Area"]))
                {
                    ViewBag.Total_Land_Area = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Total_Land_Area"]));

                }
                else
                {
                    ViewBag.Total_Land_Area = Convert.ToString(dt.Rows[0]["Total_Land_Area"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Total_Permissible_FSI"]))
                {
                    ViewBag.Total_Permissible_FSI = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Total_Permissible_FSI"]));

                }
                else
                {
                    ViewBag.Total_Permissible_FSI = Convert.ToString(dt.Rows[0]["Total_Permissible_FSI"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_Builtup_Area"]))
                {
                    ViewBag.Approved_Builtup_Area = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_Builtup_Area"]));

                }
                else
                {
                    ViewBag.Approved_Builtup_Area = Convert.ToString(dt.Rows[0]["Approved_Builtup_Area"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["Postal_Address1"]))
                {

                    ViewBag.Postal_Address1 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Address1"]));
                }
                else
                {
                    ViewBag.Postal_Address1 = Convert.ToString(dt.Rows[0]["Postal_Address1"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Buildings"]))
                {
                    ViewBag.Approved_No_of_Buildings = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Buildings"]));
                }
                else
                {
                    ViewBag.Approved_No_of_Buildings = Convert.ToString(dt.Rows[0]["Approved_No_of_Buildings"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_plotsorflats"]))
                {
                    ViewBag.Approved_No_of_plotsorflats = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_plotsorflats"]));
                }
                else
                {
                    ViewBag.Approved_No_of_plotsorflats = Convert.ToString(dt.Rows[0]["Approved_No_of_plotsorflats"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Wings"]))
                {
                    ViewBag.Approved_No_of_Wings = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Wings"]));
                }
                else
                {
                    ViewBag.Approved_No_of_Wings = Convert.ToString(dt.Rows[0]["Approved_No_of_Wings"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Floors"]))
                {

                    ViewBag.Approved_No_of_Floors = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Floors"]));
                }
                else
                {

                    ViewBag.Approved_No_of_Floors = Convert.ToString(dt.Rows[0]["Approved_No_of_Floors"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Units"]))
                {
                    ViewBag.Approved_No_of_Units = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Units"]));
                }
                else
                {
                    ViewBag.Approved_No_of_Units = Convert.ToString(dt.Rows[0]["Approved_No_of_Units"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Actual_No_Floors_G"]))
                {
                    ViewBag.Actual_No_Floors_G = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Actual_No_Floors_G"]));
                }
                else
                {
                    ViewBag.Actual_No_Floors_G = Convert.ToString(dt.Rows[0]["Actual_No_Floors_G"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Floor_Of_Unit"]))
                {
                    ViewBag.Floor_Of_Unit = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Floor_Of_Unit"]));
                }
                else
                {
                    ViewBag.Floor_Of_Unit = Convert.ToString(dt.Rows[0]["Floor_Of_Unit"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Property_Description"]))
                {
                    ViewBag.Property_Description = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Property_Description"]));
                }
                else
                {
                    ViewBag.Property_Description = Convert.ToString(dt.Rows[0]["Property_Description"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Type_Of_Structure"]))
                {
                    ViewBag.SelectedModelRetailTypeOfStructure = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Type_Of_Structure"]));
                }
                else
                {
                    ViewBag.SelectedModelRetailTypeOfStructure = Convert.ToString(dt.Rows[0]["Type_Of_Structure"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_Usage_Of_Property"]))
                {
                    ViewBag.SelectedApprovedusage = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_Usage_Of_Property"]));
                }
                else
                {
                    ViewBag.SelectedApprovedusage = Convert.ToString(dt.Rows[0]["Approved_Usage_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Actual_Usage_Of_Property"]))
                {
                    ViewBag.SelectedActualusage = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Actual_Usage_Of_Property"]));
                }
                else
                {
                    ViewBag.SelectedActualusage = Convert.ToString(dt.Rows[0]["Actual_Usage_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Current_Age_Of_Property"]))
                {
                    ViewBag.Current_Age_Of_Property = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Current_Age_Of_Property"]));
                }
                else
                {
                    ViewBag.Current_Age_Of_Property = Convert.ToString(dt.Rows[0]["Current_Age_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Residual_Age_Of_Property"]))
                {
                    ViewBag.Residual_Age_Of_Property = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Residual_Age_Of_Property"]));
                }
                else
                {
                    ViewBag.Residual_Age_Of_Property = Convert.ToString(dt.Rows[0]["Residual_Age_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Common_Areas"]))
                {
                    ViewBag.Common_Areas = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Common_Areas"]));
                }
                else
                {
                    ViewBag.Common_Areas = Convert.ToString(dt.Rows[0]["Common_Areas"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Facilities"]))
                {
                    ViewBag.Facilities = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Facilities"]));
                }
                else
                {
                    ViewBag.Facilities = Convert.ToString(dt.Rows[0]["Facilities"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Anyother_Observation"]))
                {
                    ViewBag.Anyother_Observation = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Anyother_Observation"]));
                }
                else
                {
                    ViewBag.Anyother_Observation = Convert.ToString(dt.Rows[0]["Anyother_Observation"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Roofing_Terracing"]))
                {
                    ViewBag.Roofing_Terracing = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Roofing_Terracing"]));
                }
                else
                {
                    ViewBag.Roofing_Terracing = Convert.ToString(dt.Rows[0]["Roofing_Terracing"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Quality_Of_Fixture"]))
                {
                    ViewBag.Quality_Of_Fixture = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Quality_Of_Fixture"]));
                }
                else
                {
                    ViewBag.Quality_Of_Fixture = Convert.ToString(dt.Rows[0]["Quality_Of_Fixture"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Quality_Of_Construction"]))
                {
                    ViewBag.SelectedModelRetailQualityOfConstruction = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Quality_Of_Construction"]));
                }
                else
                {
                    ViewBag.SelectedModelRetailQualityOfConstruction = Convert.ToString(dt.Rows[0]["Quality_Of_Construction"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Maintenance_Of_Property"]))
                {

                    ViewBag.SelectedModelRetailMaintenanceOfProp = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Maintenance_Of_Property"]));
                }
                else
                {

                    ViewBag.SelectedModelRetailMaintenanceOfProp = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Maintenance_Of_Property"]));
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Marketability_Of_Property"]))
                {
                    ViewBag.Marketability_Of_Property = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Marketability_Of_Property"]));
                }
                else
                {
                    ViewBag.Marketability_Of_Property = Convert.ToString(dt.Rows[0]["Marketability_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Occupancy_Details"]))
                {
                    ViewBag.SelectedModelRetailOccupancyDetails = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Occupancy_Details"]));
                }
                else
                {

                    ViewBag.SelectedModelRetailOccupancyDetails = Convert.ToString(dt.Rows[0]["Occupancy_Details"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Renting_Potential"]))
                {
                    ViewBag.Renting_Potential = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Renting_Potential"]));
                }
                else
                {
                    ViewBag.Renting_Potential = Convert.ToString(dt.Rows[0]["Renting_Potential"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Expected_Monthly_Rental"]))
                {
                    ViewBag.Expected_Monthly_Rental = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Expected_Monthly_Rental"]));
                }
                else
                {
                    ViewBag.Expected_Monthly_Rental = Convert.ToString(dt.Rows[0]["Expected_Monthly_Rental"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Name_Of_Occupant"]))
                {
                    ViewBag.Name_Of_Occupant = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Name_Of_Occupant"]));
                }
                else
                {
                    ViewBag.Name_Of_Occupant = Convert.ToString(dt.Rows[0]["Name_Of_Occupant"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Occupied_Since"]))
                {
                    ViewBag.Occupied_Since = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Occupied_Since"]));
                }
                else
                {
                    ViewBag.Occupied_Since = Convert.ToString(dt.Rows[0]["Occupied_Since"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Monthly_Rental"]))
                {
                    ViewBag.Monthly_Rental = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Monthly_Rental"]));
                }
                else
                {
                    ViewBag.Monthly_Rental = Convert.ToString(dt.Rows[0]["Monthly_Rental"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Balanced_Leased_Period"]))
                {
                    ViewBag.Balanced_Leased_Period = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Balanced_Leased_Period"]));
                }
                else
                {
                    ViewBag.Balanced_Leased_Period = Convert.ToString(dt.Rows[0]["Balanced_Leased_Period"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Specifications"]))
                {
                    ViewBag.Specifications = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Specifications"]));
                }
                else
                {
                    ViewBag.Specifications = Convert.ToString(dt.Rows[0]["Specifications"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Amenities"]))
                {
                    ViewBag.Amenities = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Amenities"]));
                }
                else
                {
                    ViewBag.Amenities = Convert.ToString(dt.Rows[0]["Amenities"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Deviation_Description"]))
                {
                    ViewBag.Deviation_Description = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Deviation_Description"]));

                }
                else
                {
                    ViewBag.Deviation_Description = Convert.ToString(dt.Rows[0]["Deviation_Description"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Cord_Latitude"]))
                {
                    ViewBag.Cord_Latitude = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Cord_Latitude"]));
                }
                else
                {
                    ViewBag.Cord_Latitude = Convert.ToString(dt.Rows[0]["Cord_Latitude"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Cord_Longitude"]))
                {
                    ViewBag.Cord_Longitude = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Cord_Longitude"]));
                }
                else
                {
                    ViewBag.Cord_Longitude = Convert.ToString(dt.Rows[0]["Cord_Longitude"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width1"]))
                {
                    ViewBag.AdjoiningRoad_Width1 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width1"]));
                }
                else
                {
                    ViewBag.AdjoiningRoad_Width1 = Convert.ToString(dt.Rows[0]["AdjoiningRoad_Width1"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width2"]))
                {
                    ViewBag.AdjoiningRoad_Width2 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width2"]));
                }
                else
                {
                    ViewBag.AdjoiningRoad_Width2 = Convert.ToString(dt.Rows[0]["AdjoiningRoad_Width2"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width3"]))
                {
                    ViewBag.AdjoiningRoad_Width3 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width3"]));
                }
                else
                {
                    ViewBag.AdjoiningRoad_Width3 = Convert.ToString(dt.Rows[0]["AdjoiningRoad_Width3"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width4"]))
                {
                    ViewBag.AdjoiningRoad_Width4 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width4"]));
                }
                else
                {
                    ViewBag.AdjoiningRoad_Width4 = Convert.ToString(dt.Rows[0]["AdjoiningRoad_Width4"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Social_Development"]))
                {
                    ViewBag.Social_Development = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Social_Development"]));
                }
                else
                {
                    ViewBag.Social_Development = Convert.ToString(dt.Rows[0]["Social_Development"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Social_Infrastructure"]))
                {
                    ViewBag.Social_Infrastructure = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Social_Infrastructure"]));
                }
                else
                {
                    ViewBag.Social_Infrastructure = Convert.ToString(dt.Rows[0]["Social_Infrastructure"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Type_of_Locality"]))
                {
                    ViewBag.SelectedModellookuptypeOfLocality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Type_of_Locality"]));
                }
                else
                {
                    ViewBag.SelectedModellookuptypeOfLocality = Convert.ToString(dt.Rows[0]["Type_of_Locality"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Class_of_Locality"]))
                {

                    ViewBag.SelectedModellookupclassOfLocality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Class_of_Locality"]));
                }
                else
                {

                    ViewBag.SelectedModellookupclassOfLocality = Convert.ToString(dt.Rows[0]["Class_of_Locality"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["RERA_Registration_No"]))
                {
                    ViewBag.RERA_Registration_No = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["RERA_Registration_No"]));
                }
                else
                {
                    ViewBag.RERA_Registration_No = Convert.ToString(dt.Rows[0]["RERA_Registration_No"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["DistFrom_City_Center"]))
                {
                    ViewBag.DistFrom_City_Center = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["DistFrom_City_Center"]));

                }
                else
                {
                    ViewBag.DistFrom_City_Center = Convert.ToString(dt.Rows[0]["DistFrom_City_Center"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Locality_Remarks"]))
                {
                    ViewBag.Locality_Remarks = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Locality_Remarks"]));

                }
                else
                {
                    ViewBag.Locality_Remarks = Convert.ToString(dt.Rows[0]["Locality_Remarks"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["Village_Name"]))
                {
                    ViewBag.Village_Name = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Village_Name"]));

                }
                else
                {
                    ViewBag.Village_Name = Convert.ToString(dt.Rows[0]["Village_Name"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["Sub_Locality"]))
                {
                    ViewBag.Sub_Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Sub_Locality"]));

                }
                else
                {
                    ViewBag.Sub_Locality = Convert.ToString(dt.Rows[0]["Sub_Locality"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["Sub_Locality"]))
                {
                    ViewBag.Sub_Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Sub_Locality"]));

                }
                else
                {
                    ViewBag.Sub_Locality = Convert.ToString(dt.Rows[0]["Sub_Locality"]);

                }

                //ViewBag.Postal_Address2 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Address2"]));
                //ViewBag.Postal_Street = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Street"]));
                //ViewBag.Postal_Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Locality"]));
                //ViewBag.Postal_City = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_City"]));
                //ViewBag.Postal_District = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_District"]));
                //ViewBag.Postal_State = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_State"]));
                //ViewBag.Postal_Pincode = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Pincode"]));
                //ViewBag.Postal_NearbyLandmark = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_NearbyLandmark"]));






            }
            else
            {

            }
            ViewModel vm = new ViewModel();
            // vm.ival_LocalTransportForProject = Gettransport();
            //vm.ival_SocialDevelopmentForProject = GetSocialInfra();
            //vm.ival_BasicInfraAvailability = GetSocialBasicInfra();
            vm.ival_ProjectBuildingWingFloorDetails = GetFloorListbyreqID();
            vm.ival_States = GetStates();
            vm.ival_LookupCategoryValuespincode = Getpincode();
            vm.ival_LookupCategoryValuesTypeOfStrycture = GetTypeofstructure();
            vm.ival_LookupCategoryValuesQualityOfConstruction = GetQualityOfConstruction();
            vm.ival_LookupCategoryValuesMaintenanceOfproperty = GetMaintenanceProperty();
            vm.ival_LookupCategoryValuesOccupancyDetails = GetOccupancyDetails();
            vm.ival_LookupCategoryValuesClassOfLocality = GetClassoflocality();
            vm.ival_LookupCategoryValuesTypeOfLocality = GetTypeOfLocality();
            vm.ival_LookupCategoryValuesProjecttype = GetProjectTypeMaster();

            vm.ival_LookupCategoryValuesSocialDev = GetSocialDevelopment();
            vm.ival_LookupCategoryValuesbasicInfra = GetBasicInfra();
            vm.ival_LookupCategoryValuesconnectivity = GetConnectivity();


            vm.ival_LookupCategoryValuesFloorType = GetFloorType();

            if (dtsocial.Rows.Count > 0 && dtsocial != null)
            {
                List<ival_SocialDevelopmentForProject> Listsocial = new List<ival_SocialDevelopmentForProject>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                for (int i = 0; i < dtsocial.Rows.Count; i++)
                {
                    Listsocial.Add(new ival_SocialDevelopmentForProject
                    {
                        Project_ID = Convert.ToInt32(dtsocial.Rows[i]["Project_ID"]),
                        Social_Dev_ID = Convert.ToInt32(dtsocial.Rows[i]["Social_Dev_ID"]),
                        Social_Development_Type = Convert.ToString(dtsocial.Rows[i]["Social_Development_Type"]),


                    });
                }
                //test
                ViewBag.Listsocial = JsonConvert.SerializeObject(Listsocial);
            }
            if (dtlocaltransport.Rows.Count > 0 && dtlocaltransport != null)
            {
                List<ival_LocalTransportForProject> Listlocal = new List<ival_LocalTransportForProject>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                for (int i = 0; i < dtlocaltransport.Rows.Count; i++)
                {
                    Listlocal.Add(new ival_LocalTransportForProject
                    {
                        Project_ID = Convert.ToInt32(dtlocaltransport.Rows[i]["Project_ID"]),
                        ProjectLocalTransport_ID = Convert.ToInt32(dtlocaltransport.Rows[i]["ProjectLocalTransport_ID"]),
                        LocalTransport_ID = Convert.ToString(dtlocaltransport.Rows[i]["LocalTransport_ID"]),


                    });
                }
                //test
                ViewBag.Listlocaltransport = JsonConvert.SerializeObject(Listlocal);
            }

            if (dtbasicinfra.Rows.Count > 0 && dtbasicinfra != null)
            {
                List<ival_BasicInfraAvailability> Listbinfra = new List<ival_BasicInfraAvailability>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                for (int i = 0; i < dtbasicinfra.Rows.Count; i++)
                {
                    Listbinfra.Add(new ival_BasicInfraAvailability
                    {
                        Project_ID = Convert.ToInt32(dtbasicinfra.Rows[i]["Project_ID"]),
                        BasicInfra_type = Convert.ToString(dtbasicinfra.Rows[i]["BasicInfra_type"]),
                        ProjectInfra_ID = Convert.ToInt32(dtbasicinfra.Rows[i]["ProjectInfra_ID"]),


                    });
                }
                //test
                ViewBag.Listbasicinfra = JsonConvert.SerializeObject(Listbinfra);
            }



            return View(vm);
        }



        [HttpPost]
        public List<ival_States> GetStates()
        {
            List<ival_States> ListStates = new List<ival_States>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtCompBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetStateByID");
            for (int i = 0; i < dtCompBuildermaster.Rows.Count; i++)
            {

                ListStates.Add(new ival_States
                {
                    State_ID = Convert.ToInt32(dtCompBuildermaster.Rows[i]["State_ID"]),
                    State_Name = Convert.ToString(dtCompBuildermaster.Rows[i]["State_Name"])

                });


            }
            return ListStates;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> Getpincode()
        {
            List<ival_LookupCategoryValues> Listpincode = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtCompBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPincode");
            for (int i = 0; i < dtCompBuildermaster.Rows.Count; i++)
            {

                Listpincode.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtCompBuildermaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtCompBuildermaster.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtCompBuildermaster.Rows[i]["Lookup_Value"])

                });


            }
            return Listpincode;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetTypeofstructure()
        {
            List<ival_LookupCategoryValues> Listtype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dttype = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTypeOfstructure");
            for (int i = 0; i < dttype.Rows.Count; i++)
            {

                Listtype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dttype.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dttype.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dttype.Rows[i]["Lookup_Value"])

                });

            }
            return Listtype;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetQualityOfConstruction()
        {
            List<ival_LookupCategoryValues> Listquality = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtqlty = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_QualityOfConstruction");
            for (int i = 0; i < dtqlty.Rows.Count; i++)
            {

                Listquality.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtqlty.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtqlty.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtqlty.Rows[i]["Lookup_Value"])

                });

            }
            return Listquality;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetMaintenanceProperty()
        {
            List<ival_LookupCategoryValues> Listmainprop = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtmainprop = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_MaintenanceofProperty");
            for (int i = 0; i < dtmainprop.Rows.Count; i++)
            {

                Listmainprop.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtmainprop.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtmainprop.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtmainprop.Rows[i]["Lookup_Value"])

                });

            }
            return Listmainprop;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetOccupancyDetails()
        {
            List<ival_LookupCategoryValues> Listoccudetails = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtoccu = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_OccupancyDetails");
            for (int i = 0; i < dtoccu.Rows.Count; i++)
            {

                Listoccudetails.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtoccu.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtoccu.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtoccu.Rows[i]["Lookup_Value"])

                });

            }
            return Listoccudetails;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetClassoflocality()
        {
            List<ival_LookupCategoryValues> ListClassoflocality = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtListClassoflocality = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalityType");
            for (int i = 0; i < dtListClassoflocality.Rows.Count; i++)
            {

                ListClassoflocality.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtListClassoflocality.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtListClassoflocality.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtListClassoflocality.Rows[i]["Lookup_Value"])

                });

            }
            return ListClassoflocality;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetTypeOfLocality()
        {
            List<ival_LookupCategoryValues> ListFloors = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtFloors = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTypeOfLocality");
            for (int i = 0; i < dtFloors.Rows.Count; i++)
            {
                ListFloors.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtFloors.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtFloors.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtFloors.Rows[i]["Lookup_Value"])
                });
            }
            return ListFloors;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetProjectTypeMaster()
        {
            List<ival_LookupCategoryValues> Listprojtype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtprojecttypermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectType");
            for (int i = 0; i < dtprojecttypermaster.Rows.Count; i++)
            {

                Listprojtype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtprojecttypermaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtprojecttypermaster.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtprojecttypermaster.Rows[i]["Lookup_Value"])

                });


            }
            return Listprojtype;
        }

        [HttpPost]
        public List<ival_SocialDevelopmentForProject> GetSocialInfra()
        {
            List<ival_SocialDevelopmentForProject> Listsocialinfra = new List<ival_SocialDevelopmentForProject>();
            hInputPara = new Hashtable();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            sqlDataAccess = new SQLDataAccess();
            DataTable dtsocialinfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSocialDevBYReqID", hInputPara);
            for (int i = 0; i < dtsocialinfra.Rows.Count; i++)
            {

                Listsocialinfra.Add(new ival_SocialDevelopmentForProject
                {
                    Social_Dev_ID = Convert.ToInt32(dtsocialinfra.Rows[i]["Social_Dev_ID"]),
                    Social_Development_Type = Convert.ToString(dtsocialinfra.Rows[i]["Social_Development_Type"]),


                });
            }
            return Listsocialinfra;
        }

        [HttpPost]
        public List<ival_ProjectBuildingWingFloorDetails> GetFloorListbyreqID()
        {
            List<ival_ProjectBuildingWingFloorDetails> Listsocialinfra = new List<ival_ProjectBuildingWingFloorDetails>();
            hInputPara = new Hashtable();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            sqlDataAccess = new SQLDataAccess();
            DataTable dtsocialinfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorDetailsByReqID", hInputPara);
            for (int i = 0; i < dtsocialinfra.Rows.Count; i++)
            {

                Listsocialinfra.Add(new ival_ProjectBuildingWingFloorDetails
                {
                    BuildingFloor_ID = Convert.ToInt32(dtsocialinfra.Rows[i]["BuildingFloor_ID"]),
                    FloorType_ID = Convert.ToString(dtsocialinfra.Rows[i]["FloorType_ID"]),
                    Number_Of_Floors = Convert.ToString(dtsocialinfra.Rows[i]["Number_Of_Floors"]),

                });
            }
            return Listsocialinfra;
        }

        [HttpPost]
        public List<ival_BasicInfraAvailability> GetSocialBasicInfra()
        {
            List<ival_BasicInfraAvailability> Listbasicinfra = new List<ival_BasicInfraAvailability>();
            hInputPara = new Hashtable();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            sqlDataAccess = new SQLDataAccess();
            DataTable dtbasicinfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicInfraBYReqID", hInputPara);
            for (int i = 0; i < dtbasicinfra.Rows.Count; i++)
            {

                Listbasicinfra.Add(new ival_BasicInfraAvailability
                {
                    ProjectInfra_ID = Convert.ToInt32(dtbasicinfra.Rows[i]["ProjectInfra_ID"]),
                    BasicInfra_type = Convert.ToString(dtbasicinfra.Rows[i]["BasicInfra_type"]),
                });
            }
            return Listbasicinfra;
        }

        [HttpPost]
        public List<ival_LocalTransportForProject> Gettransport()
        {
            List<ival_LocalTransportForProject> Listlocalinfra = new List<ival_LocalTransportForProject>();
            hInputPara = new Hashtable();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            sqlDataAccess = new SQLDataAccess();
            DataTable dtlocal = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqID", hInputPara);
            for (int i = 0; i < dtlocal.Rows.Count; i++)
            {

                Listlocalinfra.Add(new ival_LocalTransportForProject
                {
                    LocalTransport_ID = Convert.ToString(dtlocal.Rows[i]["LocalTransport_ID"]),
                    ProjectLocalTransport_ID = Convert.ToInt32(dtlocal.Rows[i]["ProjectLocalTransport_ID"]),
                });
            }
            return Listlocalinfra;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetDepartment()
        {
            List<ival_LookupCategoryValues> ListReporttype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDepartment");
            for (int i = 0; i < dtGetReportType.Rows.Count; i++)
            {

                ListReporttype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtGetReportType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtGetReportType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtGetReportType.Rows[i]["Lookup_Value"])

                });

            }
            return ListReporttype;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetLoanType()
        {
            List<ival_LookupCategoryValues> ListReporttype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLoanType");
            for (int i = 0; i < dtGetReportType.Rows.Count; i++)
            {

                ListReporttype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtGetReportType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtGetReportType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtGetReportType.Rows[i]["Lookup_Value"])

                });

            }
            return ListReporttype;
        }

        [HttpPost]
        public List<ival_ClientMaster> GetClientList()
        {
            List<ival_ClientMaster> ListReporttype = new List<ival_ClientMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            //DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClient");
            DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientName");
            for (int i = 0; i < dtGetReportType.Rows.Count; i++)
            {

                ListReporttype.Add(new ival_ClientMaster
                {
                    //Client_ID = Convert.ToInt32(dtGetReportType.Rows[i]["Client_ID"]),
                    Client_Name = Convert.ToString(dtGetReportType.Rows[i]["Client_Name"]),
                    // Branch_Name = Convert.ToString(dtGetReportType.Rows[i]["Branch_Name"])

                });

            }
            return ListReporttype;
        }

        //new method added by punam for bind distinct values of bank name
        [HttpPost]
        public List<ival_ClientMaster> GetClientName()
        {
            List<ival_ClientMaster> ListReporttype = new List<ival_ClientMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientName");
            //DataTable dtGetReportType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientName");
            for (int i = 0; i < dtGetReportType.Rows.Count; i++)
            {

                ListReporttype.Add(new ival_ClientMaster
                {
                    //Client_ID = Convert.ToInt32(dtGetReportType.Rows[i]["Client_ID"]),
                    Client_Name = Convert.ToString(dtGetReportType.Rows[i]["Client_Name"]),
                    // Branch_Name = Convert.ToString(dtGetReportType.Rows[i]["Branch_Name"])

                });

            }
            return ListReporttype;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetSocialDevelopment()
        {
            List<ival_LookupCategoryValues> ListSocialDev = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtFloors = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSocialDevelopment");
            for (int i = 0; i < dtFloors.Rows.Count; i++)
            {
                ListSocialDev.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtFloors.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtFloors.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtFloors.Rows[i]["Lookup_Value"])
                });
            }
            return ListSocialDev;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetBasicInfra()
        {
            List<ival_LookupCategoryValues> ListBasicInfra = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicInfraType");
            for (int i = 0; i < dtBasicInfra.Rows.Count; i++)
            {

                ListBasicInfra.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtBasicInfra.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtBasicInfra.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtBasicInfra.Rows[i]["Lookup_Value"])

                });

            }
            return ListBasicInfra;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetConnectivity()
        {
            List<ival_LookupCategoryValues> ListConnectivity = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtConnectivity = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalTransportType");
            for (int i = 0; i < dtConnectivity.Rows.Count; i++)
            {

                ListConnectivity.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtConnectivity.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtConnectivity.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtConnectivity.Rows[i]["Lookup_Value"])

                });

            }
            return ListConnectivity;
        }


        [HttpPost]
        public ActionResult SaveRequestorBankDetails(string str)
        {
            try
            {
                try
                {
                    if (Session["Role"].ToString() == null)
                    {
                        return RedirectToAction("Index1", "Login");
                    }
                }
                catch (Exception)
                {
                    return RedirectToAction("Index1", "Login");
                }
                DataTable listDT = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                if (listDT != null && listDT.Rows.Count > 0)
                {
                    for (int i = 0; i < listDT.Rows.Count; i++)
                    {

                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        //NameOfBank,BranchName,Department,LoanType,CustomerName,CustomerApplicationId,CustomerEmailID,ContactNO1,ContactNO2,NameOfRequestor,DateRequest,ValuationRequestID,DoctHolderAndPropertyBank,TypeOwnerShip,ReportType,MethodValuation,ReportRequestType,RequestType
                        hInputPara.Add("@Request_Flag", "New_Request");
                        hInputPara.Add("@Requestor_Name", Convert.ToString(listDT.Rows[i]["NameOfRequestor"]));
                        hInputPara.Add("@Customer_Application_ID", Convert.ToString(listDT.Rows[i]["CustomerApplicationId"]));
                        hInputPara.Add("@Customer_Name", Convert.ToString(listDT.Rows[i]["CustomerName"]));
                        hInputPara.Add("@Customer_Name1", Convert.ToString(listDT.Rows[i]["CustomerName1"]));
                        hInputPara.Add("@Customer_Contact_No_1", Convert.ToString(listDT.Rows[i]["ContactNO1"]));
                        hInputPara.Add("@Customer_Contact_No_2", Convert.ToString(listDT.Rows[i]["ContactNO2"]));
                        hInputPara.Add("@Request_Date", CDateTime(listDT.Rows[i]["DateRequest"].ToString()));
                        hInputPara.Add("@Email_ID", Convert.ToString(listDT.Rows[i]["CustomerEmailID"]));
                        hInputPara.Add("@Valuation_Request_ID", Convert.ToString(listDT.Rows[i]["ValuationRequestID"]));
                        hInputPara.Add("@Document_Holder_Name", Convert.ToString(listDT.Rows[i]["DoctHolderAndPropertyBank"]));
                        hInputPara.Add("@Document_Holder_Name1", Convert.ToString(listDT.Rows[i]["DoctHolderAndPropertyBank1"]));
                        hInputPara.Add("@Type_Of_Ownership", Convert.ToString(listDT.Rows[i]["TypeOwnerShip"]));
                        hInputPara.Add("@ValuationType", Convert.ToString(listDT.Rows[i]["ValutionType"]));
                        hInputPara.Add("@ReportType", Convert.ToString(listDT.Rows[i]["ReportType"]));
                        hInputPara.Add("@Method_of_Valuation", Convert.ToString(listDT.Rows[i]["MethodValuation"]));
                        hInputPara.Add("@Department", Convert.ToString(listDT.Rows[i]["Department"]));
                        hInputPara.Add("@LoanType", Convert.ToString(listDT.Rows[i]["LoanType"]));

                        hInputPara.Add("@RequestType", Convert.ToString(listDT.Rows[i]["RequestType1"]));
                        hInputPara.Add("@ReportRequestType", Convert.ToString(listDT.Rows[i]["ReportRequestType"]));

                        hInputPara.Add("@Client_ID", Convert.ToString(listDT.Rows[i]["Client_ID"]));
                        hInputPara.Add("@UserName", Convert.ToString(Session["UserName"]));
                        hInputPara.Add("@Date_of_Inspection", CDateTime(listDT.Rows[i]["datepicker2"].ToString()));
                        hInputPara.Add("@Date_of_Valuation", CDateTime(listDT.Rows[i]["datepicker3"].ToString()));

                        hInputPara.Add("@Balanced_Lease_Period", Convert.ToString(listDT.Rows[i]["leasedperiod"]));
                        //hInputPara.Add("@NameOfBank", Convert.ToString(listDT.Rows[i]["NameOfBank"]));

                        //hInputPara.Add("@BranchName", Convert.ToString(listDT.Rows[i]["BranchName"]));

                        //hInputPara.Add("@AssignedToEmpID", Convert.ToString(list1.Rows[i]["ddlsiteeng"]));

                        var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertNewRequestorDetails", hInputPara);

                    }

                    //return Redirect("NewRequestBank");
                    //return Json(Url.Ac tion("Index", "Request"));
                    return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, Message = "Please Insert " }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



        [HttpPost]
        public ActionResult SaveSubsequentRequestorBankDetails(string str)
        {
            try
            {
                try
                {
                    if (Session["Role"].ToString() == null)
                    {
                        return RedirectToAction("Index1", "Login");
                    }
                }
                catch (Exception)
                {
                    return RedirectToAction("Index1", "Login");
                }
                DataTable listDT = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                if (listDT != null && listDT.Rows.Count > 0)
                {
                    for (int i = 0; i < listDT.Rows.Count; i++)
                    {

                        hInputPara = new Hashtable();
                        hInputPara1 = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        //NameOfBank,BranchName,Department,LoanType,CustomerName,CustomerApplicationId,CustomerEmailID,ContactNO1,ContactNO2,NameOfRequestor,DateRequest,ValuationRequestID,DoctHolderAndPropertyBank,TypeOwnerShip,ReportType,MethodValuation,ReportRequestType,RequestType
                        hInputPara.Add("@Request_Flag", "Subsequent_Request");
                        hInputPara.Add("@Requestor_Name", Convert.ToString(listDT.Rows[i]["NameOfRequestor"]));
                        hInputPara.Add("@Customer_Application_ID", Convert.ToString(listDT.Rows[i]["CustomerApplicationId"]));
                        hInputPara.Add("@Customer_Name", Convert.ToString(listDT.Rows[i]["CustomerName"]));
                        hInputPara.Add("@Customer_Name1", Convert.ToString(listDT.Rows[i]["CustomerName1"]));
                        hInputPara.Add("@Customer_Contact_No_1", Convert.ToString(listDT.Rows[i]["ContactNO1"]));
                        hInputPara.Add("@Customer_Contact_No_2", Convert.ToString(listDT.Rows[i]["ContactNO2"]));
                        hInputPara.Add("@Request_Date", CDateTime(listDT.Rows[i]["DateRequest"].ToString()));
                        hInputPara.Add("@Email_ID", Convert.ToString(listDT.Rows[i]["CustomerEmailID"]));
                        hInputPara.Add("@Valuation_Request_ID", Convert.ToString(listDT.Rows[i]["ValRequestID"]));
                        hInputPara.Add("@Document_Holder_Name", Convert.ToString(listDT.Rows[i]["DoctHolderAndPropertyBank"]));
                        hInputPara.Add("@Document_Holder_Name1", Convert.ToString(listDT.Rows[i]["DoctHolderAndPropertyBank1"]));
                        hInputPara.Add("@Type_Of_Ownership", Convert.ToString(listDT.Rows[i]["TypeOwnerShip"]));
                        hInputPara.Add("@ValuationType", Convert.ToString(listDT.Rows[i]["ValutionType"]));
                        hInputPara.Add("@ReportType", Convert.ToString(listDT.Rows[i]["ReportType"]));
                        hInputPara.Add("@Method_of_Valuation", Convert.ToString(listDT.Rows[i]["MethodValuation"]));
                        hInputPara.Add("@Department", Convert.ToString(listDT.Rows[i]["Department"]));
                        hInputPara.Add("@LoanType", Convert.ToString(listDT.Rows[i]["LoanType"]));

                        hInputPara.Add("@RequestType", Convert.ToString(listDT.Rows[i]["RequestType1"]));
                        hInputPara.Add("@ReportRequestType", Convert.ToString(listDT.Rows[i]["ReportRequestType"]));

                        hInputPara1.Add("@Client_Name", Convert.ToString(listDT.Rows[i]["Client_ID"]));

                        DataTable dtClientid = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientMasterByName", hInputPara1);
                        hInputPara1.Clear();
                        hInputPara.Add("@Client_ID", Convert.ToString(dtClientid.Rows[i]["Client_ID"]));

                        hInputPara.Add("@UserName", Convert.ToString(Session["UserName"]));



                        //hInputPara.Add("@NameOfBank", Convert.ToString(listDT.Rows[i]["NameOfBank"]));

                        //hInputPara.Add("@BranchName", Convert.ToString(listDT.Rows[i]["BranchName"]));

                        //hInputPara.Add("@AssignedToEmpID", Convert.ToString(list1.Rows[i]["ddlsiteeng"]));

                        var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertSubsequentRequestorDetails", hInputPara);

                    }

                    //return Redirect("NewRequestBank");
                    //return Json(Url.Ac tion("Index", "Request"));
                    return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, Message = "Please Insert " }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPost]
        public JsonResult GetBranchListByBankID(string Bank_ID)
        {
            List<ival_ClientMaster> unitList = new List<ival_ClientMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@BankName", Bank_ID);
            DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBranchListByBankNameNew", hInputPara);
            for (int i = 0; i < dtrptmaster.Rows.Count; i++)
            {

                unitList.Add(new ival_ClientMaster
                {
                    Client_ID = Convert.ToInt32(dtrptmaster.Rows[i]["Client_ID"]),
                    Branch_Name = Convert.ToString(dtrptmaster.Rows[i]["Branch_Name"]),

                });

            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonvMUnitTypes = jsonSerialiser.Serialize(unitList);
            return Json(jsonvMUnitTypes);
        }
        [HttpPost]
        public JsonResult GetBranchListByBankName(string Bank_ID)
        {


            List<ival_BranchMaster> unitList = new List<ival_BranchMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@BankName", Bank_ID);
            DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBranchListByBankName", hInputPara);
            for (int i = 0; i < dtrptmaster.Rows.Count; i++)
            {

                unitList.Add(new ival_BranchMaster
                {
                    Branch_ID = Convert.ToInt32(dtrptmaster.Rows[i]["Client_ID"]),
                    Branch_Name = Convert.ToString(dtrptmaster.Rows[i]["Branch_Name"]),

                });
                ViewBag.SelectBranchName = Convert.ToString(dtrptmaster.Rows[i]["Branch_Name"]);
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonvMUnitTypes = jsonSerialiser.Serialize(unitList);
            return Json(jsonvMUnitTypes);
        }
        //added by punam
        [HttpPost]
        public JsonResult GetBranchListByBankNameNew(string Bank_ID)
        {


            List<ival_BranchMaster> unitList = new List<ival_BranchMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@BankName", Bank_ID);
            DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBranchListByBankNameNew", hInputPara);
            for (int i = 0; i < dtrptmaster.Rows.Count; i++)
            {

                unitList.Add(new ival_BranchMaster
                {
                    Branch_ID = Convert.ToInt32(dtrptmaster.Rows[i]["Client_ID"]),
                    Branch_Name = Convert.ToString(dtrptmaster.Rows[i]["Branch_Name"]),

                });
                ViewBag.SelectBranchName = Convert.ToString(dtrptmaster.Rows[i]["Branch_Name"]);
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonvMUnitTypes = jsonSerialiser.Serialize(unitList);
            return Json(jsonvMUnitTypes);
        }


        [HttpPost]
        public List<ival_BranchMaster> GetBranchName(string Bank_ID)
        {
            List<ival_BranchMaster> Listbranch = new List<ival_BranchMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@BankName", Bank_ID);
            DataTable dtbranch = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBranchListByBankName", hInputPara);
            for (int i = 0; i < dtbranch.Rows.Count; i++)
            {

                Listbranch.Add(new ival_BranchMaster
                {

                    Branch_ID = Convert.ToInt32(dtbranch.Rows[i]["Client_ID"]),
                    Branch_Name = Convert.ToString(dtbranch.Rows[i]["Branch_Name"]),

                });

            }
            return Listbranch;
        }

        //added by punam
        [HttpPost]
        public List<ival_BranchMaster> GetBranchNameNew(string Bank_ID)
        {
            List<ival_BranchMaster> Listbranch = new List<ival_BranchMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@BankName", Bank_ID);
            DataTable dtbranch = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBranchListByBankNameNew", hInputPara);
            for (int i = 0; i < dtbranch.Rows.Count; i++)
            {

                Listbranch.Add(new ival_BranchMaster
                {

                    Branch_ID = Convert.ToInt32(dtbranch.Rows[i]["Client_ID"]),
                    Branch_Name = Convert.ToString(dtbranch.Rows[i]["Branch_Name"]),

                });

            }
            return Listbranch;
        }

        public ActionResult DownloadPDF()
        {
            return View();
        }

        //public JsonResult Download(string value)
        //{

        //    int documentId = Convert.ToInt32(value);
        //    string DocId = value;
        //    int Request_ID = Convert.ToInt32(Session["Request_ID"]);
        //    string fullName = "";
        //    string a = DocId.PadLeft(2, '0');
        //    var FilePath = Request_ID + "_" + a ;
        //    string partialName = FilePath;

        //    string path = ConfigurationManager.AppSettings["oldpath"];
        //    //string path = VirtualPathUtility.ToAbsolute("../UploadFiles/");
        //    DirectoryInfo hdDirectoryInWhichToSearch = new DirectoryInfo(path);
        //    FileInfo[] filesInDir = hdDirectoryInWhichToSearch.GetFiles("*" + partialName + "*.*");

        //    foreach (FileInfo foundFile in filesInDir)
        //    {
        //        fullName = foundFile.FullName;
        //        Console.WriteLine(fullName);
        //    }




        //    if (fullName != "")
        //    {
        //        string[] newValue = fullName.Split('\\');
        //        if (newValue != null)
        //        {


        //            string Name1 = newValue[newValue.Length - 1];
        //            string Name = newValue[newValue.Length - 2];
        //            fullName = Name1;
        //        }
        //    }

        //    var FilePath1 = Session["File"].ToString();

        //    var folderPath = Server.MapPath("~/UploadFiles");
        //    var fileCount = Directory.GetFiles(Server.MapPath("~/UploadFiles"));
        //    if (fileCount.Length > 1) // exactly only 1 file
        //    {
        //        string embed = "<object data=\"{0}\" type=\"application/pdf\"width=\"2000px\" height=\"1000px\">";
        //        embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
        //        embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
        //        embed += "</object>";
        //        TempData["Embed"] = string.Format(embed, VirtualPathUtility.ToAbsolute("~/UploadFiles/" + fullName));
        //        return Json("Exist", JsonRequestBehavior.AllowGet);
        //    }
        //    //else if (fileCount.Length > 1) // more than 1 file
        //    //{
        //    //    Process.Start(folderPath);
        //    //}
        //    else
        //    {
        //        Process.Start(folderPath);
        //    }

        //    return Json("", JsonRequestBehavior.AllowGet);
        //}

        public JsonResult Download()
        {
            var FilePath = Session["File"].ToString();
            var folderPath = Server.MapPath("~/UploadFiles");
            var fileCount = Directory.GetFiles(Server.MapPath("~/UploadFiles"));
            if (fileCount.Length > 1) // exactly only 1 file
            {
                string embed = "<object data=\"{0}\" type=\"application/pdf\"width=\"100%\" height=\"600px\">";
                embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
                embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                embed += "</object>";
                TempData["Embed"] = string.Format(embed, VirtualPathUtility.ToAbsolute("~/UploadFiles/" + FilePath));
                return Json("Exist", JsonRequestBehavior.AllowGet);
            }
            //else if (fileCount.Length > 1) // more than 1 file
            //{
            //    Process.Start(folderPath);
            //}
            else
            {
                Process.Start(folderPath);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }


        public ActionResult SubDownloadNewEdit(string value)
        {
            var FilePath = "";
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["SubRequest_ID"]);
            int documentId = Convert.ToInt32(value);
            hInputPara.Add("@RequestId", RequestID);
            hInputPara.Add("@DocumentId", documentId);
            DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadedFileName", hInputPara);
            if (dtRequests.Rows.Count > 0)
            {
                if (dtRequests.Rows[0]["Uploaded_Document"].ToString() != string.Empty && (dtRequests.Rows[0]["Uploaded_Document"].ToString()) != null)
                {
                    string fileName = Convert.ToString(dtRequests.Rows[0]["Uploaded_Document"]);
                    FilePath = fileName;
                }

            }
            else
            {
                var FileGet = Session["FileEdit"];
                FilePath = FileGet.ToString();
            }


            var folderPath = Server.MapPath("~/UploadFiles");
            var fileCount = Directory.GetFiles(Server.MapPath("~/UploadFiles"));
            if (fileCount.Length > 1) // exactly only 1 file
            {
                string embed = "<object data=\"{0}\" type=\"application/pdf\"width=\"100%\" height=\"600px\">";
                embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
                embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                embed += "</object>";

                // TempData["Embed"] = string.Format(embed,"D://Punam Gat//04092020BackUp_IValue//ivalue//ivalue_codefirst//I_Value.WebApp//IValue//UploadFiles//" + FilePath);
                TempData["Embed"] = string.Format(embed, VirtualPathUtility.ToAbsolute("~/UploadFiles/" + FilePath));

                return Json("Exist", JsonRequestBehavior.AllowGet);
            }

            else
            {
                Process.Start(folderPath);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }
        //changes by punam

        public ActionResult DownloadNewEdit(string value)
        {
            var FilePath = "";
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestID"]);

            //var NewFilePath = Session["FileEdit"];
            var NewFilePath = TempData["TempFileEdit"];
            int documentId = Convert.ToInt32(value);
            hInputPara.Add("@RequestId", RequestID);
            hInputPara.Add("@DocumentId", documentId);
            DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadedFileName", hInputPara);
            if (dtRequests.Rows.Count > 0)
            {
                if (dtRequests.Rows[0]["Uploaded_Document"].ToString() != string.Empty && (dtRequests.Rows[0]["Uploaded_Document"].ToString()) != null)
                {
                    string fileName = Convert.ToString(dtRequests.Rows[0]["Uploaded_Document"]);
                    if (NewFilePath != null)
                    {
                        FilePath = NewFilePath.ToString();
                    }
                    else
                    {
                        FilePath = fileName;
                    }

                }

            }
            else
            {
                var FileGet = Session["FileEdit"];
                FilePath = FileGet.ToString();
            }


            var folderPath = Server.MapPath("~/UploadFiles");
            var fileCount = Directory.GetFiles(Server.MapPath("~/UploadFiles"));
            if (fileCount.Length > 1) // exactly only 1 file
            {
                string embed = "<object data=\"{0}\" type=\"application/pdf\"width=\"100%\" height=\"600px\">";
                embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
                embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                embed += "</object>";

                // TempData["Embed"] = string.Format(embed,"D://Punam Gat//04092020BackUp_IValue//ivalue//ivalue_codefirst//I_Value.WebApp//IValue//UploadFiles//" + FilePath);
                TempData["Embed"] = string.Format(embed, VirtualPathUtility.ToAbsolute("~/UploadFiles/" + FilePath));

                return Json("Exist", JsonRequestBehavior.AllowGet);
            }

            else
            {
                Process.Start(folderPath);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }

        //changes by punam

        public ActionResult DownloadNewDynamicEdit(string value)
        {
            var FilePath = "";
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            //int RequestID = Convert.ToInt32(Session["RequestID"]);
            //var NewFilePath = Session["EditfileSave"];
            //int documentId = Convert.ToInt32(value);
            //hInputPara.Add("@RequestId", RequestID);
            //hInputPara.Add("@DocumentId", documentId);
            //DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadedFileName", hInputPara);
            //if (dtRequests.Rows.Count > 0)
            //{
            //    if (dtRequests.Rows[0]["Uploaded_Document"].ToString() != string.Empty && (dtRequests.Rows[0]["Uploaded_Document"].ToString()) != null)
            //    {
            //        string fileName = Convert.ToString(dtRequests.Rows[0]["Uploaded_Document"]);
            //        FilePath = fileName;
            //    }

            //}

            var FileGet = Session["FileEdit"];
            FilePath = FileGet.ToString();



            var folderPath = Server.MapPath("~/UploadFiles");
            var fileCount = Directory.GetFiles(Server.MapPath("~/UploadFiles"));
            if (fileCount.Length > 1) // exactly only 1 file
            {
                string embed = "<object data=\"{0}\" type=\"application/pdf\"width=\"100%\" height=\"600px\">";
                embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
                embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                embed += "</object>";

                // TempData["Embed"] = string.Format(embed,"D://Punam Gat//04092020BackUp_IValue//ivalue//ivalue_codefirst//I_Value.WebApp//IValue//UploadFiles//" + FilePath);
                TempData["Embed"] = string.Format(embed, VirtualPathUtility.ToAbsolute("~/UploadFiles/" + FilePath));

                return Json("Exist", JsonRequestBehavior.AllowGet);
            }

            else
            {
                Process.Start(folderPath);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult Show()
        {
            string path = Server.MapPath("~/images/admin.jpg");
            byte[] imageByteData = System.IO.File.ReadAllBytes(path);
            string imageBase64Data = Convert.ToBase64String(imageByteData);
            string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);
            ViewBag.ImageData = imageDataURL;
            return View();
            //int RequestID = Convert.ToInt32(Session["RequestID"]);
            //hInputPara = new Hashtable();
            //sqlDataAccess = new SQLDataAccess();
            //var imageData = "";

            //hInputPara.Add("@Request_ID", RequestID);
            //DataTable dtImages = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesByReqID", hInputPara);
            //for(int i = 0; dtImages.Rows.Count > 0; i++)
            //{
            //    imageData = dtImages.Rows[0]["Uploaded_Image"].ToString();

            //}
            //return File(imageData, "image/jpg");
        }

        public ActionResult GetImage()
        {
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            byte[] imageByteData = { };
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtImages = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesByReqID", hInputPara);
            if (dtImages.Rows.Count > 0)
            {
                for (int i = 0; i < dtImages.Rows.Count; i++)
                {
                    string imagename = dtImages.Rows[0]["Image_Path"].ToString();
                    string path = Server.MapPath("~/images/" + imagename);
                    imageByteData = System.IO.File.ReadAllBytes(path);
                }
            }
            return File(imageByteData, "image/png");

        }


        public ActionResult MyAction()
        {
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            byte[] imageByteData = { };
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtImages = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesByReqID", hInputPara);
            string imagename = dtImages.Rows[0]["Image_Path"].ToString();
            ViewBag.Images = Directory.EnumerateFiles(Server.MapPath("~/Images"))
                                      .Select(fn => "~/Images/" + Path.GetFileName(imagename));

            return View();
        }

        public ActionResult SingleEditUnitDetails()
        {
            // Session["RequestID"] = 45;
            //string ReqID = Session["RequestID"].ToString();

            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            //int RequestID = 60;
            Hashtable hInputPara123 = new Hashtable();

            // DataTable dtREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1RequestID");
            hInputPara123.Add("@Project_ID", Convert.ToString(RequestID));
            DataTable ValuationRequestID1 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRequest", hInputPara123);
            for (int i = 0; i < ValuationRequestID1.Rows.Count; i++)
            {

                Session["GetInspDate"] = ValuationRequestID1.Rows[0]["Date_of_Inspection"].ToString();
                Session["GetEmpId"] = ValuationRequestID1.Rows[0]["AssignedToEmpID"].ToString();
            }


            
            ViewModelRequest vm = new ViewModelRequest();
            vm.relationWithEmpList1 = GetRelationsWithEmp1();
            vm.relationWithEmpList2 = GetRelationsWithEmp2();

            ViewBag.GetInspDate = Session["GetInspDate"].ToString();
            ViewBag.GetEmpId = Session["GetEmpId"].ToString();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            ViewBag.UnitType = Session["TypeOfUnit"].ToString();
            ViewBag.PType = Session["Property_Type"].ToString();
            Session["TypeOfSelect"] = ViewBag.SelectType;
            //ViewBag.UnitType = "Flat";
            //ViewBag.PType = "Residential";
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtunitREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllunitsBYReqID", hInputPara);
            if (dtunitREQ.Rows.Count > 0)
            {
                ViewBag.Deviation_Description = dtunitREQ.Rows[0]["Deviation_Description"].ToString();
                ViewBag.Construction_as_per_plan = dtunitREQ.Rows[0]["Construction_as_per_plan"].ToString();
                ViewBag.Risk_of_Demolition = dtunitREQ.Rows[0]["Risk_of_Demolition"].ToString();
                ViewBag.Type_of_Flooring = dtunitREQ.Rows[0]["Type_of_Flooring"].ToString();
                ViewBag.Wall_Finish = dtunitREQ.Rows[0]["Wall_Finish"].ToString();
                ViewBag.Plumbing_Fitting = dtunitREQ.Rows[0]["Plumbing_Fitting"].ToString();
                ViewBag.Electrical_Fittings = dtunitREQ.Rows[0]["Electrical_Fittings"].ToString();
                ViewBag.Rough_Plaster = dtunitREQ.Rows[0]["Rough_Plaster"].ToString();
                ViewBag.quality = dtunitREQ.Rows[0]["quality_fixtures"].ToString();
                ViewBag.Project_ID = dtunitREQ.Rows[0]["Project_ID"].ToString();



                ViewBag.txtrealizable = dtunitREQ.Rows[0]["realizable_value"].ToString();
                ViewBag.ddldevv = dtunitREQ.Rows[0]["matchAt"].ToString();
                ViewBag.txt_nill = dtunitREQ.Rows[0]["riskof"].ToString();

                ViewBag.txtdev = dtunitREQ.Rows[0]["devdetails"].ToString();
                ViewBag.txtdevunit = dtunitREQ.Rows[0]["devunit"].ToString();






                ViewBag.Selectedtypeofdemarcations2 = dtunitREQ.Rows[0]["constr_As"].ToString();



                ViewBag.Tiling = dtunitREQ.Rows[0]["Tiling"].ToString();
                ViewBag.Cord_Longitude = dtunitREQ.Rows[0]["Cord_Longitude"].ToString();
                ViewBag.Cord_Latitude = dtunitREQ.Rows[0]["Cord_Latitude"].ToString();
                ViewBag.Road1 = dtunitREQ.Rows[0]["Road1"].ToString();
                ViewBag.Road2 = dtunitREQ.Rows[0]["Road2"].ToString();
                ViewBag.Road3 = dtunitREQ.Rows[0]["Road3"].ToString();
                ViewBag.Road4 = dtunitREQ.Rows[0]["Road4"].ToString();
                ViewBag.SeismicZone = dtunitREQ.Rows[0]["SeismicZone"].ToString();
                ViewBag.FloodZone = dtunitREQ.Rows[0]["FloodZone"].ToString();
                ViewBag.CycloneZone = dtunitREQ.Rows[0]["CycloneZone"].ToString();
                ViewBag.CRZ = dtunitREQ.Rows[0]["CRZ"].ToString();
                ViewBag.Remarks = dtunitREQ.Rows[0]["Remarks"].ToString();
                ViewBag.Plot_Demarcated_at_site = dtunitREQ.Rows[0]["Plot_Demarcated_at_site"].ToString();
                ViewBag.Selectedtypeofdemarcations = dtunitREQ.Rows[0]["Type_of_Demarcation"].ToString();

                ViewBag.LiftDetails = dtunitREQ.Rows[0]["Lift_Details"].ToString();

                ViewBag.Selectedtypeofdemarcations1 = dtunitREQ.Rows[0]["risk_demarcation"].ToString();

                ViewBag.Boundaries_Matching = dtunitREQ.Rows[0]["Boundaries_Matching"].ToString();
                ViewBag.SelectedRoadFinish = dtunitREQ.Rows[0]["Road_Finish"].ToString();
                ViewBag.AreBoundariesremarks = dtunitREQ.Rows[0]["AreBoundariesremarks"].ToString();

                ViewBag.Name = dtunitREQ.Rows[0]["Name"].ToString();
                ViewBag.Property_Identified_PName = dtunitREQ.Rows[0]["Property_Identified_PName"].ToString();
                ViewBag.P_Contact_Num = dtunitREQ.Rows[0]["P_Contact_Num"].ToString();
                ViewBag.Customer_Name_On_Board = dtunitREQ.Rows[0]["CustomerDisplayBoard"].ToString();
                ViewBag.Society_OR_Building_Name = dtunitREQ.Rows[0]["Society_OR_Building_Name"].ToString();

                ViewBag.Current_Value_of_Property = dtunitREQ.Rows[0]["Current_Value_of_Property"].ToString();
                ViewBag.Total_Value_of_Property = dtunitREQ.Rows[0]["Total_Value_of_Property"].ToString();
                ViewBag.Government_Value = dtunitREQ.Rows[0]["Government_Value"].ToString();
                ViewBag.Cost_of_Interior = dtunitREQ.Rows[0]["Cost_of_Interior"].ToString();
                ViewBag.MeasuredSaleableArea = dtunitREQ.Rows[0]["MeasuredSaleableArea"].ToString();
                ViewBag.ApprovedSaleableArea = dtunitREQ.Rows[0]["ApprovedSaleableArea"].ToString();
                ViewBag.distressValue = dtunitREQ.Rows[0]["distressValue"].ToString();
                ViewBag.StageOfConstruction = dtunitREQ.Rows[0]["StageOfConstruction"].ToString();
                ViewBag.IsInteriorworkcarriedout = dtunitREQ.Rows[0]["IsInteriorworkcarriedout"].ToString();
                ViewBag.ValuationResult = dtunitREQ.Rows[0]["ValuationResult"].ToString();
                ViewBag.DescStageofconstruction = dtunitREQ.Rows[0]["DescStageofconstruction"].ToString();
                ViewBag.No_Of_Lifts = dtunitREQ.Rows[0]["No_Of_Lifts"].ToString();
                ViewBag.SelectedModelLiftno = dtunitREQ.Rows[0]["IsLifts_Present"].ToString();
                ViewBag.SelectedModelviewunits = dtunitREQ.Rows[0]["ViewFromUnit"].ToString();
                ViewBag.MeasuredApprovedareamatchatsite = dtunitREQ.Rows[0]["MeasuredApprovedareamatchatsite"].ToString();
                ViewBag.DeviationPercentage = dtunitREQ.Rows[0]["DeviationPercentage"].ToString();
                ViewBag.Value_of_Plot = dtunitREQ.Rows[0]["Value_of_Plot"].ToString();
                ViewBag.Total_Land_Plot_Value = dtunitREQ.Rows[0]["Total_Land_Plot_Value"].ToString();
                ViewBag.Value_of_other_Plot = dtunitREQ.Rows[0]["Value_of_other_Plot"].ToString();
                ViewBag.Distress_Value_of_Plot = dtunitREQ.Rows[0]["Distress_Value_of_Plot"].ToString();
                ViewBag.OtherPlotType = dtunitREQ.Rows[0]["OtherPlotType"].ToString();
                ViewBag.Extra_Cost_Valuation = dtunitREQ.Rows[0]["Extra_Cost_Valuation"].ToString();

                ViewBag.Work_Completed_Perc = dtunitREQ.Rows[0]["Work_Completed_Perc"].ToString();
                ViewBag.StageOfConstructionBunglow = dtunitREQ.Rows[0]["StageOfConstructionBunglow"].ToString();
                ViewBag.StageOfConstructionPerct = dtunitREQ.Rows[0]["StageOfConstructionPerct"].ToString();
                ViewBag.DisbursementRecommendPerct = dtunitREQ.Rows[0]["DisbursementRecommendPerct"].ToString();
                ViewBag.IsInteriorworkcarriedout = dtunitREQ.Rows[0]["IsInteriorworkcarriedout"].ToString();
                ViewBag.DistressValuePerct = dtunitREQ.Rows[0]["DistressValuePerct"].ToString();
                ViewBag.Government_Rate = dtunitREQ.Rows[0]["Government_Rate"].ToString();
                ViewBag.DescStageofconstructionBunglow = dtunitREQ.Rows[0]["DescStageofconstructionBunglow"].ToString();
                ViewBag.Gross_Estimated_Cost_Construction = dtunitREQ.Rows[0]["Gross_Estimated_Cost_Construction"].ToString();
                ViewBag.Cost_Construction_PerSqFT = dtunitREQ.Rows[0]["Cost_Construction_PerSqFT"].ToString();
                ViewBag.Does_this_Matches_UnderConstruction = dtunitREQ.Rows[0]["Does_this_Matches_UnderConstruction"].ToString();
                ViewBag.Does_this_Cost_Remark = dtunitREQ.Rows[0]["Does_this_Cost_Remark"].ToString();

                ViewBag.Valueoftheplotbungalow = dtunitREQ.Rows[0]["Valueoftheplotbungalow"].ToString();
                ViewBag.Valueoftheotherplotbungalow = dtunitREQ.Rows[0]["Valueoftheotherplotbungalow"].ToString();
                ViewBag.Totalofthebungalow = dtunitREQ.Rows[0]["Totalofthebungalow"].ToString();
                ViewBag.TotalvaluepropertyBunglow = dtunitREQ.Rows[0]["TotalvaluepropertyBunglow"].ToString();
                ViewBag.DistressValueofPropertyBunglow = dtunitREQ.Rows[0]["DistressValueofPropertyBunglow"].ToString();
                ViewBag.TotalvaluepropertyBunglowUC = dtunitREQ.Rows[0]["TotalvaluepropertyBunglowUC"].ToString();
                ViewBag.Current_Value_of_Property_UC = dtunitREQ.Rows[0]["Current_Value_of_Property_UC"].ToString();
                ViewBag.ValuationResultR = dtunitREQ.Rows[0]["ValuationResultR"].ToString();
                ViewBag.ValuationResultUC = dtunitREQ.Rows[0]["ValuationResultUC"].ToString();
                ViewBag.Cost_of_InteriorReady = dtunitREQ.Rows[0]["Cost_of_InteriorReady"].ToString();
                ViewBag.StageOfConstructionUC = dtunitREQ.Rows[0]["StageOfConstructionUC"].ToString();
                ViewBag.DisbursementRecommendPerctUC = dtunitREQ.Rows[0]["DisbursementRecommendPerctUC"].ToString();
                ViewBag.ValuationResultplot = dtunitREQ.Rows[0]["ValuationResultplot"].ToString();
                ViewBag.TotalCostCons = dtunitREQ.Rows[0]["TotalCostCons"].ToString();
                ViewBag.Distressvalpercentplot = dtunitREQ.Rows[0]["Distressvalpercentplot"].ToString();

                //new
                ViewBag.MeasuredAreaFt = dtunitREQ.Rows[0]["MeasuredAreaFt"].ToString();
                ViewBag.ApprovedAreaF = dtunitREQ.Rows[0]["ApprovedAreaF"].ToString();
                ViewBag.AdoptedAValFt = dtunitREQ.Rows[0]["AdoptedAValFt"].ToString();
                ViewBag.AdoptedLandRFt = dtunitREQ.Rows[0]["AdoptedLandRFt"].ToString();

                ViewBag.MeasuredAreaMt = dtunitREQ.Rows[0]["MeasuredAreaMt"].ToString();
                ViewBag.ApprovedAreaMt = dtunitREQ.Rows[0]["ApprovedAreaMt"].ToString();
                ViewBag.AdoptedAValMt = dtunitREQ.Rows[0]["AdoptedAValMt"].ToString();
                ViewBag.AdoptedLandRMt = dtunitREQ.Rows[0]["AdoptedLandRMt"].ToString();

                ViewBag.MeasuredOtherPAFt = dtunitREQ.Rows[0]["MeasuredOtherPAFt"].ToString();
                ViewBag.ApproveOtherPAFt = dtunitREQ.Rows[0]["ApproveOtherPAFt"].ToString();
                ViewBag.AdoptedOtherPAValFt = dtunitREQ.Rows[0]["AdoptedOtherPAValFt"].ToString();
                ViewBag.AdoptedOtherPLandRFt = dtunitREQ.Rows[0]["AdoptedOtherPLandRFt"].ToString();

                ViewBag.MeasuredOtherPAMt = dtunitREQ.Rows[0]["MeasuredOtherPAMt"].ToString();
                ViewBag.ApproveOtherPAMt = dtunitREQ.Rows[0]["ApproveOtherPAMt"].ToString();
                ViewBag.AdoptedOtherPAValMt = dtunitREQ.Rows[0]["AdoptedOtherPAValMt"].ToString();
                ViewBag.AdoptedOtherPLandRMt = dtunitREQ.Rows[0]["AdoptedOtherPLandRMt"].ToString();

                ViewBag.Permissible = dtunitREQ.Rows[0]["Permissible"].ToString();
                ViewBag.LandRateRangeLoc = dtunitREQ.Rows[0]["LandRateRangeLoc"].ToString();
                ViewBag.PermissibleOtherplot = dtunitREQ.Rows[0]["PermissibleOtherplot"].ToString();

                ViewBag.GovtRateSF = dtunitREQ.Rows[0]["GovtRateSF"].ToString();
                ViewBag.GovtvalueS = dtunitREQ.Rows[0]["GovtvalueS"].ToString();
                ViewBag.MatchAt = dtunitREQ.Rows[0]["MatchAt"].ToString();
                ViewBag.nill = dtunitREQ.Rows[0]["RiskOf"].ToString();
                ViewBag.DevDeatils = dtunitREQ.Rows[0]["DevDetails"].ToString();
                ViewBag.DevUnit = dtunitREQ.Rows[0]["DevUnit"].ToString();


                ViewBag.ValuationResultplot = dtunitREQ.Rows[0]["ValuationResultplot"].ToString();
                ViewBag.TotalFlatwords = dtunitREQ.Rows[0]["TotalFlatwords"].ToString();
                ViewBag.TotalPlotwords = dtunitREQ.Rows[0]["TotalPlotwords"].ToString();
                ViewBag.TotalreadyBunglowWords = dtunitREQ.Rows[0]["TotalreadyBunglowWords"].ToString();
                ViewBag.TotalUCBunglowWords = dtunitREQ.Rows[0]["TotalUCBunglowWords"].ToString();
                ViewBag.txtrealpercent = dtunitREQ.Rows[0]["RealPercPlot"].ToString();
                ViewBag.txtrealizablevalue = dtunitREQ.Rows[0]["Realizableplot"].ToString();


                //vm.ival_UnitParameterMasters = GetUnitDetails(unitname);
                vm.ival_LookupCategoryFloorTypes = GetFloorType();
               // vm.ival_ProjectBuildingWingFloorDetails = GetFloorListbyreqIDNew();
                vm.ival_ProjectApprovalDetails = GetApprovalTypesnew1();
                vm.ival_ProjectBoundaries = GetBoundariesByReqID();
                vm.ival_ProjectSideMargin = GetSidearginsByReqID();
                vm.ival_LookupCategoryValuesviewfromunit = GetviewUnits();
                vm.ival_LookupCategoryValuesRiskofdemolition = Getdemolition();
                vm.ival_LookupCategoryValuesdemarcation = GetDemarcation();
                vm.ival_LookupCategoryValuesdemarcation1 = GetDemarcation1();
                vm.ival_LookupCategoryValuesRoadFinish = GetRoadFinish();
                vm.ival_LookupCategoryValuesPropZone = GetPropZone();
                vm.ival_Lookupflooring = Getfloring();
                vm.ival_Lookupwall = Getwall();
                vm.ival_Lookupplumbing = Getplumbing();
                vm.ival_Lookupelec = Getelect();
                vm.ival_Lookupother = Getother();//qualtlity
                vm.ival_Lookupexter = Getextr();
                //vm.ival_BrokerDetails = GetbrokerBYreqID();
                int RequestIDNew = Convert.ToInt32(Session["RequestID"]);
                hInputPara.Add("@Request_ID", RequestIDNew);
                DataTable dtbroker = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBrokerDetailsBYReqID", hInputPara);
                hInputPara.Clear();
                if (dtbroker.Rows.Count > 0)
                {


                    //ViewBag.Broker_IDs = Convert.ToInt32(dtbroker.Rows[i]["Broker_IDs"]);

                    ViewBag.BrokerId1 = Convert.ToInt32(dtbroker.Rows[0]["Broker_ID"]);

                    ViewBag.BrokerId2 = Convert.ToInt32(dtbroker.Rows[1]["Broker_ID"]);



                    ViewBag.Broker_Name1 = Convert.ToString(dtbroker.Rows[0]["Broker_Name"]);
                    ViewBag.Broker_Name2 = Convert.ToString(dtbroker.Rows[1]["Broker_Name"]);
                    ViewBag.Firm_Name1 = Convert.ToString(dtbroker.Rows[0]["Firm_Name"]);
                    ViewBag.Firm_Name2 = Convert.ToString(dtbroker.Rows[1]["Firm_Name"]);
                    ViewBag.Rate_Quoted_For_Flat1 = Convert.ToString(dtbroker.Rows[0]["Rate_Quoted_For_Flat"]);
                    ViewBag.Rate_Quoted_For_Flat2 = Convert.ToString(dtbroker.Rows[1]["Rate_Quoted_For_Flat"]);
                    ViewBag.Rate_Quoted_For_Land1 = Convert.ToString(dtbroker.Rows[0]["Rate_Quoted_For_Land"]);
                    ViewBag.Rate_Quoted_For_Land2 = Convert.ToString(dtbroker.Rows[1]["Rate_Quoted_For_Land"]);
                    ViewBag.Contact_Num1 = Convert.ToString(dtbroker.Rows[0]["Contact_Num"]);
                    ViewBag.Contact_Num2 = Convert.ToString(dtbroker.Rows[1]["Contact_Num"]);


                }

                int RequestIDCom = Convert.ToInt32(Session["RequestID"]);
                hInputPara.Add("@Request_ID", RequestID);
                DataTable dtcomp = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetComparabledetailssBYReqID", hInputPara);
                //DataTable dtbroker = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBrokerDetailsBYReqID", hInputPara);

                if (dtcomp.Rows.Count > 0)
                {

                    ViewBag.Property_ID1 = Convert.ToInt32(dtcomp.Rows[0]["Property_ID"]);
                    //ViewBag.Property_IDs = Convert.ToInt32(dtcomp.Rows[0]["Property_IDs"]);
                    ViewBag.Project_Name1 = Convert.ToString(dtcomp.Rows[0]["Project_Name"]);
                    ViewBag.Dist_Subject_Property1 = Convert.ToString(dtcomp.Rows[0]["Dist_Subject_Property"]);
                    ViewBag.Comparable_Unit_Details1 = Convert.ToString(dtcomp.Rows[0]["Comparable_Unit_Details"]);
                    ViewBag.Quality_And_Amenities1 = Convert.ToString(dtcomp.Rows[0]["Quality_And_Amenities"]);
                    ViewBag.Ongoing_Rate1 = Convert.ToString(dtcomp.Rows[0]["Ongoing_Rate"]);
                    ViewBag.Property_ID2 = Convert.ToInt32(dtcomp.Rows[1]["Property_ID"]);
                    //ViewBag.Property_IDs = Convert.ToInt32(dtcomp.Rows[0]["Property_IDs"]);
                    ViewBag.Project_Name2 = Convert.ToString(dtcomp.Rows[1]["Project_Name"]);
                    ViewBag.Dist_Subject_Property2 = Convert.ToString(dtcomp.Rows[1]["Dist_Subject_Property"]);
                    ViewBag.Comparable_Unit_Details2 = Convert.ToString(dtcomp.Rows[1]["Comparable_Unit_Details"]);
                    ViewBag.Quality_And_Amenities2 = Convert.ToString(dtcomp.Rows[1]["Quality_And_Amenities"]);
                    ViewBag.Ongoing_Rate2 = Convert.ToString(dtcomp.Rows[1]["Ongoing_Rate"]);

                    vm.SelectedRelationWithEmpID2 = Convert.ToString(dtcomp.Rows[0]["Comparable_Unit_Details"]);
                    vm.SelectedRelationWithEmpID = Convert.ToString(dtcomp.Rows[0]["Quality_And_Amenities"]);

                    vm.SelectedRelationWithEmpID3 = Convert.ToString(dtcomp.Rows[1]["Comparable_Unit_Details"]);
                    vm.SelectedRelationWithEmpID4 = Convert.ToString(dtcomp.Rows[1]["Quality_And_Amenities"]);
                }
                // vm.iva_Comparable_Properties = GetComprabledetreqID();
                vm.ival_LookupCategoryLiftTypes = GetLiftType();
                vm.ival_ProjectFloorWiseBreakUps = GetFloorwisebreakbyreqID();
                vm.ival_ProjectFloorWiseBreakUpstotal = GetFloorwisebreakbyreqIDtotal();
                vm.ival_ProjectImageName = GetProjectMasterImageName();

                hInputPara1 = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                byte[] imageByteData = { };
                hInputPara1.Add("@Request_ID", RequestID);
                DataTable dtImages = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesByReqID", hInputPara1);


                List<Image_Uploaded_List> listimg = new List<Image_Uploaded_List>();
                Image_Uploaded_List objeqi = new Image_Uploaded_List();
                if (dtImages.Rows.Count > 0)
                {
                    for (int i = 0; i < dtImages.Rows.Count; i++)
                    {


                        objeqi.Image_Path = dtImages.Rows[i]["Image_Path"].ToString();
                        listimg.Add(objeqi);

                    }
                    ViewBag.Images = listimg;
                }
                vm.UploadedImagesList = GetUploadedImageListEdit(RequestID.ToString());
                foreach (var data in vm.UploadedImagesList)
                {
                    if (data.Image_Type == "Internal View")
                    {
                        if (data.Seq_No == 1)
                        {
                            ViewBag.FilePathIn1 = data.FilePath;
                            ViewBag.ImagePathIn1 = data.Image_Path;
                            //TempData["ImagePathIn1"] = data.Image_Path;
                        }
                        if (data.Seq_No == 2)
                        {
                            ViewBag.FilePathIn2 = data.FilePath;
                            ViewBag.ImagePathIn2 = data.Image_Path;
                        }
                        if (data.Seq_No == 3)
                        {
                            ViewBag.FilePathIn3 = data.FilePath;
                            ViewBag.ImagePathIn3 = data.Image_Path;
                        }
                        if (data.Seq_No == 4)
                        {
                            ViewBag.FilePathIn4 = data.FilePath;
                            ViewBag.ImagePathIn4 = data.Image_Path;
                        }

                    }
                    if (data.Image_Type == "External View ")
                    {
                        if (data.Seq_No == 1)
                        {
                            ViewBag.FilePathEx1 = data.FilePath;
                            ViewBag.ImagePathEx1 = data.Image_Path;
                        }
                        if (data.Seq_No == 2)
                        {
                            ViewBag.FilePathEx2 = data.FilePath;
                            ViewBag.ImagePathEx2 = data.Image_Path;
                        }
                        if (data.Seq_No == 3)
                        {
                            ViewBag.FilePathEx3 = data.FilePath;
                            ViewBag.ImagePathEx3 = data.Image_Path;
                        }
                        if (data.Seq_No == 4)
                        {
                            ViewBag.FilePathEx4 = data.FilePath;
                            ViewBag.ImagePathEx4 = data.Image_Path;
                        }

                    }
                    if (data.Image_Type == "Approach Road")
                    {
                        if (data.Seq_No == 1)
                        {
                            ViewBag.FilePathAR1 = data.FilePath;
                            ViewBag.ImagePathAR1 = data.Image_Path;
                        }
                        if (data.Seq_No == 2)
                        {
                            ViewBag.FilePathAR2 = data.FilePath;
                            ViewBag.ImagePathAR2 = data.Image_Path;
                        }
                        if (data.Seq_No == 3)
                        {
                            ViewBag.FilePathAR3 = data.FilePath;
                            ViewBag.ImagePathAR3 = data.Image_Path;
                        }
                        if (data.Seq_No == 4)
                        {
                            ViewBag.FilePathAR4 = data.FilePath;
                            ViewBag.ImagePathAR4 = data.Image_Path;
                        }

                    }
                    if (data.Image_Type == "View from property ")
                    {
                        if (data.Seq_No == 1)
                        {
                            ViewBag.FilePathv1 = data.FilePath;
                            ViewBag.ImagePathv1 = data.Image_Path;
                        }
                        if (data.Seq_No == 2)
                        {
                            ViewBag.FilePathv2 = data.FilePath;
                            ViewBag.ImagePathv2 = data.Image_Path;
                        }
                        if (data.Seq_No == 3)
                        {
                            ViewBag.FilePathv3 = data.FilePath;
                            ViewBag.ImagePathv3 = data.Image_Path;
                        }
                        if (data.Seq_No == 4)
                        {
                            ViewBag.FilePathv4 = data.FilePath;
                            ViewBag.ImagePathv4 = data.Image_Path;
                        }

                    }
                    if (data.Image_Type == "IGR Photo")
                    {
                        if (data.Seq_No == 1)
                        {
                            ViewBag.FilePathIg = data.FilePath;
                            ViewBag.ImagePathIg = data.Image_Path;
                        }
                    }
                    if (data.Image_Type == "Location Map")
                    {
                        if (data.Seq_No == 1)
                        {
                            ViewBag.FilePathlm = data.FilePath;
                            ViewBag.ImagePathlm = data.Image_Path;
                        }
                    }
                }

                Hashtable hInputPara22 = new Hashtable();
                hInputPara22.Add("@Project_ID", ViewBag.Project_ID);
                DataTable ValuationRequestID2 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorwisebreakUpperFloor", hInputPara22);
                // Number_Of_Floors = Convert.ToString(dtsocialinfra.Rows[i]["Number_Of_Floors"]),
               // ViewBag.floorCount = Convert.ToInt32(ValuationRequestID2.Rows[0]["Number_Of_Floors"]);

                if (ValuationRequestID2.Rows.Count > 0)
                {
                    ViewBag.floorCount = Convert.ToInt32(ValuationRequestID2.Rows[0]["Number_Of_Floors"]);
                }
                else
                {
                    ViewBag.floorCount = 0;
                }
            }
            else
            {
                return RedirectToAction("SingleUnitdetails", "NewRequestorSingleUnit", new { area = "Project" });
            }
            return View(vm);


        }
        [HttpPost]
        public List<UploadedImageAndImageMaster> GetUploadedImageListEdit(string RequestID)
        {

            //List<ival_Document_Uploaded_List> listfiles = new List<ival_Document_Uploaded_List>();
            List<UploadedImageAndImageMaster> listfiles = new List<UploadedImageAndImageMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@RequestId", RequestID);

            DataTable dtdoc = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUploadedImagesForUnitEdit", hInputPara);
            for (int i = 0; i < dtdoc.Rows.Count; i++)
            {
                //ival_Document_Uploaded_List obj = new ival_Document_Uploaded_List();
                UploadedImageAndImageMaster obj = new UploadedImageAndImageMaster();
                //listfiles.Add(new ival_Document_Uploaded_List
                //{
                if ((dtdoc.Rows[i]["Image_ID"]) != DBNull.Value)
                {
                    obj.Image_ID = Convert.ToInt32(dtdoc.Rows[i]["Image_ID"]);
                }
                if ((dtdoc.Rows[i]["Seq_No"]) != DBNull.Value)
                {
                    string seqNo = dtdoc.Rows[i]["Seq_No"].ToString();
                    string Sequence = seqNo.PadLeft(2, '0');
                    obj.Seq_No = Convert.ToInt32(Sequence);
                }
                obj.Image_Type_ID = Convert.ToInt32(dtdoc.Rows[i]["Image_Type_ID"]);
                obj.Image_Type = Convert.ToString(dtdoc.Rows[i]["Image_Type"]);

                if ((dtdoc.Rows[i]["Image_Path"]) != DBNull.Value)
                {
                    string uploaddoc = Convert.ToString(dtdoc.Rows[i]["Image_Path"]);
                    string value = uploaddoc;
                    if (value != "")
                    {
                        //string[] newValue = value.Split('\\');
                        //if (newValue != null)
                        //{

                        string fileName = value;

                        obj.FilePath = fileName;
                        var filePath = Directory.GetFiles(Server.MapPath("~/images"));
                        foreach (string filename in filePath)
                        {
                            if (filename.Contains(fileName))
                            {
                                var Get = filename;
                                obj.Image_Path = Get;
                            }
                        }
                        // }
                    }

                }

                listfiles.Add(obj);
                //});

            }
            return listfiles;
        }
        [HttpPost]
        public JsonResult Getimages()
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<Image_Uploaded_List> imagelist = new List<Image_Uploaded_List>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            // int RequestID = 60;
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtunits = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesByReqID", hInputPara);
            for (int i = 0; i < dtunits.Rows.Count; i++)
            {
                imagelist.Add(new Image_Uploaded_List
                {
                    Image_ID = Convert.ToInt32(dtunits.Rows[i]["Image_ID"]),
                    Image_Path = Convert.ToString(dtunits.Rows[i]["Image_Path"]),

                });
            }
            //ViewBag.unitdetails = ProjectUnitLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(imagelist);
            return Json(imagelist);

        }

        [HttpPost]
        public JsonResult GetimagesIGR()
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<Image_Uploaded_List> imagelist = new List<Image_Uploaded_List>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            // int RequestID = 60;
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtunits = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesIGRByReqID", hInputPara);
            for (int i = 0; i < dtunits.Rows.Count; i++)
            {
                imagelist.Add(new Image_Uploaded_List
                {
                    Image_ID = Convert.ToInt32(dtunits.Rows[i]["Image_ID"]),
                    Image_Path = Convert.ToString(dtunits.Rows[i]["Image_Path"]),

                });
            }
            //ViewBag.unitdetails = ProjectUnitLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(imagelist);
            return Json(imagelist);

        }

        [HttpPost]
        public JsonResult GetimagesIGRLOC()
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<Image_Uploaded_List> imagelist = new List<Image_Uploaded_List>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            // int RequestID = 60;
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtunits = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesLocByReqID", hInputPara);
            for (int i = 0; i < dtunits.Rows.Count; i++)
            {
                imagelist.Add(new Image_Uploaded_List
                {
                    Image_ID = Convert.ToInt32(dtunits.Rows[i]["Image_ID"]),
                    Image_Path = Convert.ToString(dtunits.Rows[i]["Image_Path"]),

                });
            }
            //ViewBag.unitdetails = ProjectUnitLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(imagelist);
            return Json(imagelist);

        }
        //public List<EquipmentMasterModel> getEquipmentddList()
        //{
        //    string flag = "GetEquipmentDropdownList";
        //    DataTable dt = objBAL.getEquipmentDropdownList(flag);
        //    EquipmentMasterModel modelobj = new EquipmentMasterModel();
        //    List<EquipmentMasterModel> list = new List<EquipmentMasterModel>();
        //    if (dt.Rows.Count > 0 && dt != null)
        //    {
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            EquipmentMasterModel objeqi = new EquipmentMasterModel();
        //            objeqi.eq_name = dt.Rows[i]["eq_name"].ToString();
        //            list.Add(objeqi);
        //        }
        //    }
        //    return list.ToList();
        //}
        [HttpPost]
        public ActionResult UpdateAll(string str, string str1, string str2, string str3, string str4, string str5, string str6)
        {
            try
            {

                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                DataTable list2 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));
                DataTable list3 = (DataTable)JsonConvert.DeserializeObject(str2, (typeof(DataTable)));
                DataTable list4 = (DataTable)JsonConvert.DeserializeObject(str3, (typeof(DataTable)));
                DataTable list5 = (DataTable)JsonConvert.DeserializeObject(str4, (typeof(DataTable)));
                DataTable list6 = (DataTable)JsonConvert.DeserializeObject(str5, (typeof(DataTable)));
                DataTable list7 = (DataTable)JsonConvert.DeserializeObject(str6, (typeof(DataTable)));

                hInputPara1 = new Hashtable();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                //int RequestID = 45;
                int RequestID = Convert.ToInt32(Session["RequestID"]);
                hInputPara1.Add("@Request_ID", RequestID);
                DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprjIDByRequestID", hInputPara1);
                Session["Project_ID"] = dttop1project.Rows[0]["Project_ID"].ToString();
                Session["Unit_ID"] = dttop1project.Rows[0]["Unit_ID"].ToString();
                //for (int i = 0; i < list1.Rows.Count; i++)
                //{
                //    hInputPara = new Hashtable();
                //    sqlDataAccess = new SQLDataAccess();

                //    hInputPara.Add("@Project_ID", Convert.ToString(dttop1project.Rows[0]["Project_ID"]));
                //    hInputPara.Add("@Floor_Type", Convert.ToString(list1.Rows[i]["UnitParameters"]));
                //    hInputPara.Add("@Approved_Carpet_Area", Convert.ToString(list1.Rows[i]["carpetarea"]));
                //    hInputPara.Add("@Approved_Built_up_Area", Convert.ToString(list1.Rows[i]["apprbuiltuparea"]));
                //    hInputPara.Add("@Measured_Carpet_Area", Convert.ToString(list1.Rows[i]["measuredcarpet"]));
                //    hInputPara.Add("@Measured_Built_up_Area", Convert.ToString(list1.Rows[i]["meabuiltuparea"]));
                //    hInputPara.Add("@Adopted_Area", Convert.ToString(list1.Rows[i]["adoptedsqft"]));
                //   var res1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertFloorWiseBreakUp", hInputPara);
                //}



                for (int i1 = 0; i1 < list2.Rows.Count; i1++)
                {

                    //hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                    //hInputPara.Add("@Building_ID", 1);
                    //hInputPara.Add("@Wing_ID", 1);
                    ////hInputPara.Add("@PApproval_ID", Convert.ToInt32(list2.Rows[i1]["PApproval_ID"]));
                    //hInputPara.Add("@Approval_Type", Convert.ToString(list2.Rows[i1]["ApprovalType"]));
                    //// hInputPara.Add("@Approving_Authority", Convert.ToString(list2.Rows[i1]["Approving_Authority"]));
                    ////hInputPara.Add("@Date_of_Approval", Convert.ToString(list2.Rows[i1]["Approving_Date"]));
                    ////  hInputPara.Add("@Approval_Num", Convert.ToString(list2.Rows[i1]["Approving_Num"]));
                    //// var res2 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectApprovalDetails", hInputPara);
                    ////   hInputPara.Clear();
                }

                for (int i2 = 0; i2 < list3.Rows.Count; i2++)
                {

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    //hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                    //hInputPara.Add("@Building_ID", "");
                    //hInputPara.Add("@Wing_ID", "");
                    //hInputPara.Add("@Unit_ID", "");
                    hInputPara.Add("@Side_Margin_ID", list3.Rows[i2]["SidemarginId"].ToString());
                    hInputPara.Add("@Side_Margin_Description", list3.Rows[i2]["SidemarginsType"].ToString());
                    hInputPara.Add("@As_per_Approvals", list3.Rows[i2]["Asperapprovedplan"].ToString());
                    hInputPara.Add("@As_per_Local_Bye_Laws", list3.Rows[i2]["AsperLocalByLaws"].ToString());
                    hInputPara.Add("@As_Actuals", list3.Rows[i2]["AsActuals"].ToString());
                    hInputPara.Add("@Deviation_No", list3.Rows[i2]["Deviation"].ToString());
                    hInputPara.Add("@Percentage_Deviation", list3.Rows[i2]["perdevistion"].ToString());

                    var res3 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectSideMarginsnew1", hInputPara);
                    hInputPara.Clear();
                }


                for (int i3 = 0; i3 < list4.Rows.Count; i3++)
                {


                    hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                    hInputPara.Add("@Building_ID", 1);
                    hInputPara.Add("@Wing_ID", 1);
                    hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                    hInputPara.Add("@Boundry_Name", list4.Rows[i3]["BoundariesType"].ToString());
                    hInputPara.Add("@AsPer_Document", list4.Rows[i3]["asperdoc"].ToString());
                    hInputPara.Add("@AsPer_Site", list4.Rows[i3]["aspersite"].ToString());
                    string str155 = Session["TypeOfUnit"].ToString();
                    if (str155 == "Plot" || str155 == "Bungalow" || str155 == "Mall" || str155 == "Shopping Complex" || str155 == "Standalone Building")
                    {
                        hInputPara.Add("@Dimension", list4.Rows[i3]["Dimension"].ToString());
                    }
                    var res4 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectBoundariesNew", hInputPara);
                    hInputPara.Clear();
                }

                for (int i = 0; i < list6.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    //DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1Project");

                    hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                    hInputPara.Add("@Deviation_Description", Convert.ToString(list6.Rows[i]["txtdevdesc"]));
                    hInputPara.Add("@Risk_of_Demolition", Convert.ToString(list6.Rows[i]["ddlriskdemolition"]));
                    hInputPara.Add("@Type_of_Flooring", Convert.ToString(list6.Rows[i]["txttypefloor"]));
                    hInputPara.Add("@Wall_Finish", Convert.ToString(list6.Rows[i]["txtwallfinish"]));
                    hInputPara.Add("@Plumbing_Fitting", Convert.ToString(list6.Rows[i]["txtplumbfit"]));
                    hInputPara.Add("@Electrical_Fittings", Convert.ToString(list6.Rows[i]["txtelecfitt"]));
                    hInputPara.Add("@txt_quality", Convert.ToString(list6.Rows[i]["txt_quality"]));
                    hInputPara.Add("@Rough_Plaster", Convert.ToString(list6.Rows[i]["txtroughplaster"]));

                    hInputPara.Add("@Cord_Latitude", Convert.ToString(list6.Rows[i]["txtlatitude"]));
                    hInputPara.Add("@Cord_Longitude", Convert.ToString(list6.Rows[i]["txtlongitude"]));
                    hInputPara.Add("@Road1", Convert.ToString(list6.Rows[i]["txtroad1"]));
                    hInputPara.Add("@Road2", Convert.ToString(list6.Rows[i]["txtroad2"]));
                    hInputPara.Add("@Road3", Convert.ToString(list6.Rows[i]["txtroad3"]));
                    hInputPara.Add("@Road4", Convert.ToString(list6.Rows[i]["txtroad4"]));

                    hInputPara.Add("@ddldemarcation1", Convert.ToString(list6.Rows[i]["ddldemarcation1"]));
                    hInputPara.Add("@constructionsite", Convert.ToString(list6.Rows[i]["constructionsite"]));


                    hInputPara.Add("@Plot_Demarcated_at_site", Convert.ToString(list6.Rows[i]["plotdemosite"]));
                    hInputPara.Add("@Type_of_Demarcation", Convert.ToString(list6.Rows[i]["ddldemarcation"]));
                    hInputPara.Add("@Boundaries_Matching", Convert.ToString(list6.Rows[i]["boundmatc"]));
                    hInputPara.Add("@AreBoundariesremarks", Convert.ToString(list6.Rows[i]["areboundaryremarks"]));
                    hInputPara.Add("@Road_Finish", Convert.ToString(list6.Rows[i]["ddlroadfinish"]));
                    //hInputPara.Add("@Remarks", Convert.ToString(list6.Rows[i]["remarks"]));
                    hInputPara.Add("@SeismicZone", Convert.ToString(list6.Rows[i]["txtseismic"]));
                    hInputPara.Add("@FloodZone", Convert.ToString(list6.Rows[i]["txtflood"]));
                    hInputPara.Add("@CycloneZone", Convert.ToString(list6.Rows[i]["txtcyclone"]));
                    hInputPara.Add("@CRZ", Convert.ToString(list6.Rows[i]["txtCRZ"]));
                    hInputPara.Add("@No_Of_Lifts", Convert.ToString(list6.Rows[i]["txtnumberoflifts"]));//Added By Rupali
                    hInputPara.Add("@Lifts_details", Convert.ToString(list6.Rows[i]["txtliftdetails"]));//Added By Rupali
                    hInputPara.Add("@PType", Convert.ToString(list6.Rows[i]["PType"]));
                    hInputPara.Add("@UType", Convert.ToString(list6.Rows[i]["UnitType"]));//Added By Rupali
                    hInputPara.Add("@MeasuredApprovedareamatchatsite", Convert.ToString(list6.Rows[i]["ddlmeasuredareamatch"]));
                    hInputPara.Add("@DeviationPercentage", Convert.ToString(list6.Rows[i]["txtDeviation"]));
                    hInputPara.Add("@IsLifts_Present", Convert.ToString(list6.Rows[i]["ddlLifttype"]));
                    hInputPara.Add("@ViewFromUnit", Convert.ToString(list6.Rows[i]["SelectedViewFromUnitDdl"]));

                    var res5 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateRequestUnitMaster", hInputPara);
                    hInputPara.Clear();


                }


                for (int i1 = 0; i1 < list5.Rows.Count; i1++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();

                    hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());

                    hInputPara.Add("@UnitParameter_ID", list5.Rows[i1]["ParamID"].ToString());
                    hInputPara.Add("@UnitParameter_Value", list5.Rows[i1]["Value"].ToString());

                    var res6 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateUnitParametersValue", hInputPara);
                    hInputPara.Clear();
                }

                return Json(new { success = true, Message = "Unit Details Inserted Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult UpdateUnitPrice(string str, string str1, string str2, string str3, string str4, string str5, string str6, string radioValue, string str7, string str8, string str9)
        {
            try
            {
                hInputPara = new Hashtable();
                hInputPara1 = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                int RequestID = Convert.ToInt32(Session["RequestID"]);
                //  int RequestID = 45;
                hInputPara1.Add("@Request_ID", RequestID);
                DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprjIDByRequestID", hInputPara1);
                Session["Project_ID"] = dttop1project.Rows[0]["Project_ID"].ToString();
                Session["Unit_ID"] = dttop1project.Rows[0]["Unit_ID"].ToString();

                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                DataTable list2 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));
                DataTable list3 = (DataTable)JsonConvert.DeserializeObject(str2, (typeof(DataTable)));
                DataTable list4 = (DataTable)JsonConvert.DeserializeObject(str3, (typeof(DataTable)));
                DataTable list5 = (DataTable)JsonConvert.DeserializeObject(str4, (typeof(DataTable)));
                DataTable list8 = (DataTable)JsonConvert.DeserializeObject(str5, (typeof(DataTable)));
                DataTable list9 = (DataTable)JsonConvert.DeserializeObject(str6, (typeof(DataTable)));


                DataTable list10 = (DataTable)JsonConvert.DeserializeObject(str7, (typeof(DataTable)));
                DataTable list11 = (DataTable)JsonConvert.DeserializeObject(str8, (typeof(DataTable)));
                DataTable list12 = (DataTable)JsonConvert.DeserializeObject(str9, (typeof(DataTable)));
                hInputPara1.Clear();
                for (int i = 0; i < list2.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    //check record is already exist or not
                    var projectId = Convert.ToInt32(Session["Project_ID"]);
                    hInputPara.Add("@projectId", Convert.ToInt32(Session["Project_ID"]));
                    hInputPara.Add("@UnitId", Session["Unit_ID"]);
                    //hInputPara.Add("@projectName", Convert.ToString(list2.Rows[i]["txtname"]));
                    var result2 = sqlDataAccess.ExecuteStoreProcedure("usp_CheckUnitComparableExist", hInputPara);
                    hInputPara.Clear();
                    if (result2 == "Record already exist")
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        //DataTable dttopproject = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1Project");
                        hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                        hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                        //hInputPara.Add("@Project_ID",23);
                        //hInputPara.Add("@Unit_ID", 2052);
                        hInputPara.Add("@Property_ID", Convert.ToString(list2.Rows[i]["ProprtyId"].ToString()));
                        hInputPara.Add("@Project_Name", Convert.ToString(list2.Rows[i]["txtname"].ToString()));
                        hInputPara.Add("@Dist_Subject_Property", Convert.ToString(list2.Rows[i]["txtdist"].ToString()));
                        hInputPara.Add("@Comparable_Unit_Details", Convert.ToString(list2.Rows[i]["txtunitdetails"].ToString()));
                        hInputPara.Add("@Quality_And_Amenities", Convert.ToString(list2.Rows[i]["txtqualituamenities"].ToString()));
                        hInputPara.Add("@Ongoing_Rate", Convert.ToString(list2.Rows[i]["txtongoing"].ToString()));

                        var res = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateCompProp", hInputPara);
                        hInputPara.Clear();
                    }
                    //else
                    //{
                    //    hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                    //    hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                    //    //hInputPara.Add("@Project_ID", Project_ID);
                    //    //hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                    //    //hInputPara.Add("@Project_ID",23);
                    //    //hInputPara.Add("@Unit_ID", 2052);
                    //    hInputPara.Add("@Project_Name", Convert.ToString(list2.Rows[i]["txtname"].ToString()));
                    //    hInputPara.Add("@Dist_Subject_Property", Convert.ToString(list2.Rows[i]["txtdist"].ToString()));
                    //    hInputPara.Add("@Comparable_Unit_Details", Convert.ToString(list2.Rows[i]["txtunitdetails"].ToString()));
                    //    hInputPara.Add("@Quality_And_Amenities", Convert.ToString(list2.Rows[i]["txtqualituamenities"].ToString()));
                    //    hInputPara.Add("@Ongoing_Rate", Convert.ToString(list2.Rows[i]["txtongoing"].ToString()));

                    //    var res = sqlDataAccess.ExecuteStoreProcedure("usp_InsertCompProp", hInputPara);
                    //    hInputPara.Clear();
                    //}

                }
                //Broker details
                for (int i = 0; i < list1.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    //check record is already exist or not
                    hInputPara.Add("@projectId", Convert.ToInt32(Session["Project_ID"]));
                    hInputPara.Add("@UnitId", Session["Unit_ID"]);
                   // hInputPara.Add("@brokerName", Convert.ToString(list1.Rows[i]["txtbrkname"]));
                    var result2 = sqlDataAccess.ExecuteStoreProcedure("usp_CheckUnitBrokerDetails", hInputPara);
                    hInputPara.Clear();
                    if (result2 == "Record already exist")
                    {
                        if (Convert.ToString(list1.Rows[i]["txtbrkname"]) != "")
                        {
                            hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                            hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                            //hInputPara.Add("@Project_ID", 23);
                            //hInputPara.Add("@Unit_ID", 2052);
                            hInputPara.Add("@Broker_ID", Convert.ToString(list1.Rows[i]["txtbrkID"].ToString()));
                            hInputPara.Add("@Broker_Name", Convert.ToString(list1.Rows[i]["txtbrkname"].ToString()));
                            hInputPara.Add("@Firm_Name", Convert.ToString(list1.Rows[i]["txtfirm"].ToString()));
                            hInputPara.Add("@Contact_Num", Convert.ToString(list1.Rows[i]["txtctcno"].ToString()));
                            hInputPara.Add("@Rate_Quoted_For_Land", Convert.ToString(list1.Rows[i]["txtrateland"].ToString()));
                            hInputPara.Add("@Rate_Quoted_For_Flat", Convert.ToString(list1.Rows[i]["txtrateflat"].ToString()));

                            var res2 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateBrokerDetails", hInputPara);
                        }
                        hInputPara.Clear();
                    }
                    //else
                    //{
                    //    if (Convert.ToString(list1.Rows[i]["txtbrkname"]) != "")
                    //    {
                    //        hInputPara.Add("@Project_ID", Session["Project_ID"]);
                    //        hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                    //        //hInputPara.Add("@Project_ID", 23);
                    //        //hInputPara.Add("@Unit_ID", 2052);
                    //        hInputPara.Add("@Broker_Name", Convert.ToString(list1.Rows[i]["txtbrkname"].ToString()));
                    //        hInputPara.Add("@Firm_Name", Convert.ToString(list1.Rows[i]["txtfirm"].ToString()));
                    //        hInputPara.Add("@Contact_Num", Convert.ToString(list1.Rows[i]["txtctcno"].ToString()));
                    //        hInputPara.Add("@Rate_Quoted_For_Land", Convert.ToString(list1.Rows[i]["txtrateland"].ToString()));
                    //        hInputPara.Add("@Rate_Quoted_For_Flat", Convert.ToString(list1.Rows[i]["txtrateflat"].ToString()));

                    //        var res2 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertBrokerDetails", hInputPara);
                    //    }
                    //    hInputPara.Clear();

                    //}
                }

                for (int i1 = 0; i1 < list4.Rows.Count; i1++)
                {

                    var UnitParameter_IDVal = list4.Rows[i1]["UnitPriceParaValue_ID"];
                    var UnitParameter_Value = list4.Rows[i1]["UnitPriceParameter_Value"];

                    if (UnitParameter_IDVal.ToString() != "" && UnitParameter_Value.ToString() != "")
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());

                        hInputPara.Add("@UnitPriceParameter_Value", list4.Rows[i1]["UnitPriceParameter_Value"].ToString());

                        hInputPara.Add("@UnitPriceParaValue_ID", Convert.ToInt32(list4.Rows[i1]["UnitPriceParaValue_ID"].ToString()));
                        hInputPara.Add("@UnitPriceSQMt_Value", list4.Rows[i1]["UnitPriceParameterChange_Value"].ToString());
                        var r1 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateUnitPriceParametersValue", hInputPara);
                        hInputPara.Clear();
                    }

                }


                for (int i1 = 0; i1 < list11.Rows.Count; i1++)
                {

                    var unitPriceparamid = list11.Rows[i1]["unitPriceparamid"];
                    var UnitPriceParameter_Value = list11.Rows[i1]["UnitPriceParameter_Value"];

                    if (unitPriceparamid.ToString() != "" && UnitPriceParameter_Value.ToString() != "")
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();

                        hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());

                        hInputPara.Add("@UnitPriceParameter_Value", list11.Rows[i1]["UnitPriceParameter_Value"].ToString());

                        hInputPara.Add("@UnitPriceParaValue_ID", Convert.ToInt32(list11.Rows[i1]["unitPriceparamid"].ToString()));
                        hInputPara.Add("@UnitPriceSQMt_Value", list11.Rows[i1]["UnitPriceParameterChange_Value"].ToString());
                        var r1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertUnitPriceParametersValue", hInputPara);


                        hInputPara.Clear();
                    }


                }
                for (int i1 = 0; i1 < list12.Rows.Count; i1++)
                {

                    var unitPriceparamid = list12.Rows[i1]["unitPriceparamid"];
                    var UnitPriceParameter_Value = list12.Rows[i1]["UnitPriceParameter_Value"];

                    if (unitPriceparamid.ToString() != "" && UnitPriceParameter_Value.ToString() != "")
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();

                        hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());

                        hInputPara.Add("@UnitPriceParameter_Value", list12.Rows[i1]["UnitPriceParameter_Value"].ToString());

                        hInputPara.Add("@UnitPriceParaValue_ID", Convert.ToInt32(list12.Rows[i1]["unitPriceparamid"].ToString()));
                        hInputPara.Add("@UnitPriceSQMt_Value", list12.Rows[i1]["UnitPriceParameterChange_Value"].ToString());
                        var r1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertUnitPriceParametersValue", hInputPara);
                        hInputPara.Clear();
                    }

                }

                for (int i1 = 0; i1 < list9.Rows.Count; i1++)
                {

                    var UnitParameter_IDVal = list9.Rows[i1]["UnitPriceParaValue_ID"];
                    var UnitParameter_Value = list9.Rows[i1]["UnitPriceParameter_Value"];
                    //var unitPriceparamid= list9.Rows[i1]["unitPriceparamid"];
                    //if (unitPriceparamid == "")
                    //{
                    if (UnitParameter_IDVal.ToString() != "" && UnitParameter_Value.ToString() != "")
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        //hInputPara.Add("@Project_ID", 23);
                        //hInputPara.Add("@Unit_ID", 2052);
                        hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                        //  hInputPara.Add("@Name", list4.Rows[i1]["UnitPriceParameter_Name"].ToString());
                        // hInputPara.Add("@UnitPriceParameter_ID", list9.Rows[i1]["unitPriceparamid"].ToString());
                        hInputPara.Add("@UnitPriceParameter_Value", list9.Rows[i1]["UnitPriceParameter_Value"].ToString());

                        hInputPara.Add("@UnitPriceParaValue_ID", Convert.ToInt32(list9.Rows[i1]["UnitPriceParaValue_ID"].ToString()));
                        hInputPara.Add("@UnitPriceSQMt_Value", list9.Rows[i1]["UnitPriceParameterChange_Value"].ToString());
                        var r1 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateUnitPriceParametersValue1", hInputPara);
                        hInputPara.Clear();
                    }
                    //}
                    //else
                    //{
                    //    hInputPara = new Hashtable();
                    //    sqlDataAccess = new SQLDataAccess();

                    //    hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());

                    //    hInputPara.Add("@UnitPriceParameter_Value", list9.Rows[i1]["UnitPriceParameter_Value"].ToString());

                    //    hInputPara.Add("@UnitPriceParameter_ID", Convert.ToInt32(list9.Rows[i1]["unitPriceparamid"].ToString()));
                    //    hInputPara.Add("@UnitPriceSQMt_Value", list9.Rows[i1]["UnitPriceParameterChange_Value"].ToString());
                    //    var r1 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateUnitPriceParametersValue", hInputPara);
                    //    hInputPara.Clear();
                    //}
                }


                for (int i1 = 0; i1 < list3.Rows.Count; i1++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    //hInputPara.Add("@Project_ID", 23);
                    //hInputPara.Add("@Unit_ID", 2052);
                    hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                    hInputPara.Add("@Property_Identified_PName", Convert.ToString(list3.Rows[i1]["propidentity"]));
                    hInputPara.Add("@Name", Convert.ToString(list3.Rows[i1]["txtname"]));
                    hInputPara.Add("@P_Contact_Num", list3.Rows[i1]["txtcontno"].ToString());
                    hInputPara.Add("@Society_OR_Building_Name", Convert.ToString(list3.Rows[i1]["society"]));
                    hInputPara.Add("@CustomerDisplayBoard", Convert.ToString(list3.Rows[i1]["custdisboard"]));
                   
                    //hInputPara.Add("@StageOfConstruction", Convert.ToString(list3.Rows[i1]["ddlstage"].ToString()));
                    //hInputPara.Add("@IsInteriorworkcarriedout", list3.Rows[i1]["ddlinterior"].ToString());
                    //hInputPara.Add("@DescStageofconstruction", list3.Rows[i1]["descConstruction"].ToString());
                    var r1 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateretailPrices", hInputPara);
                    hInputPara.Clear();

                }

                for (int i = 0; i < list10.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    hInputPara1 = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();

                    hInputPara.Add("@Project_ID", Session["Project_ID"]);
                    hInputPara.Add("@Floor_Type", Convert.ToString(list10.Rows[i]["UnitParameters"]));
                    hInputPara.Add("@Approved_Carpet_Area", Convert.ToString(list10.Rows[i]["carpetarea"]));
                    hInputPara.Add("@Approved_Built_up_Area", Convert.ToString(list10.Rows[i]["apprbuiltuparea"]));
                    hInputPara.Add("@Measured_Carpet_Area", Convert.ToString(list10.Rows[i]["measuredcarpet"]));
                    hInputPara.Add("@Measured_Built_up_Area", Convert.ToString(list10.Rows[i]["meabuiltuparea"]));
                    hInputPara.Add("@Adopted_Area_for_Valuation", Convert.ToString(list10.Rows[i]["adoptedsqft"]));
                    hInputPara.Add("@Adopted_Cost_Construction", Convert.ToString(list10.Rows[i]["adoptedsqftcost"]));
                    // hInputPara.Add("@Total_Cost_Construction", Convert.ToString(list7.Rows[i]["totalcost"]));
                    var res1 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateFloorWiseBreakUp", hInputPara);
                    hInputPara1.Add("@FloorName", Convert.ToString(list10.Rows[i]["UnitParameters"]));
                    var res2 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertFloorTypes", hInputPara1);
                    hInputPara.Clear();
                    hInputPara1.Clear();
                    //Session.Abandon();          
                    //Session.Clear();
                    //Session["Request_ID"] = null;
                    //Session["Unit_ID"] = null;
                    //Session["Project_ID"] = null;
                }

                for (int i1 = 0; i1 < list5.Rows.Count; i1++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    //hInputPara.Add("@Project_ID", 23);
                    //hInputPara.Add("@Unit_ID", 2052);

                    //hInputPara = new Hashtable();
                    //sqlDataAccess = new SQLDataAccess();
                    //hInputPara.Add("@Project_ID", 23);
                    //hInputPara.Add("@Unit_ID", 2052);
                    double Amount1 = 0;
                    double txtvalueoftheplot = 0;
                    hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                    //hInputPara.Add("@Total_Value_of_Property", Convert.ToString(list5.Rows[i1]["txttotalvalue"]));
                    hInputPara.Add("@TotalvaluepropertyBunglow", Convert.ToString(list5.Rows[i1]["txttotalbanglowready"]));
                    hInputPara.Add("@TotalvaluepropertyBunglowUC", Convert.ToString(list5.Rows[i1]["txttotvalpropunder"]));
                    //hInputPara.Add("@Current_Value_of_Property", Convert.ToString(list5.Rows[i1]["currentValueOfPropertyUnderConstructionUnitId"]));
                    hInputPara.Add("@Current_Value_of_Property_UC", Convert.ToString(list5.Rows[i1]["txtcurrentvalundercons"]));
                    hInputPara.Add("@Value_of_Plot", Convert.ToString(list5.Rows[i1]["txtvalueoftheplot"]));
                    hInputPara.Add("@Valueoftheplotbungalow", Convert.ToString(list5.Rows[i1]["txtvalueofthebunglow"]));
                    hInputPara.Add("@Value_of_other_Plot", Convert.ToString(list5.Rows[i1]["txtvalueoftheotherplot"]));
                    hInputPara.Add("@Valueoftheotherplotbungalow", Convert.ToString(list5.Rows[i1]["txtvalueoftheotherbunglow"]));
                    hInputPara.Add("@Total_Land_Plot_Value", Convert.ToString(list5.Rows[i1]["txttotallandval"]));
                    hInputPara.Add("@Totalofthebungalow", Convert.ToString(list5.Rows[i1]["txtlandvaluation"]));
                    hInputPara.Add("@Distress_Value_of_Plot", Convert.ToString(list5.Rows[i1]["txtdistressval"]));
                    hInputPara.Add("@DistressValueofPropertyBunglow", Convert.ToString(list5.Rows[i1]["txtdistressvalprop"]));
                    //hInputPara.Add("@ValuationResult", Convert.ToString(list5.Rows[i1]["ddlvaluation"]));
                    hInputPara.Add("@ValuationResultplot", Convert.ToString(list5.Rows[i1]["ddlvaluationplot"]));
                    hInputPara.Add("@ValuationResultR", Convert.ToString(list5.Rows[i1]["txtvalaresult"]));
                    hInputPara.Add("@ValuationResultUC", Convert.ToString(list5.Rows[i1]["valresultundercons"]));
                    //hInputPara.Add("@Cost_of_Interior", Convert.ToString(list5.Rows[i1]["txtcostof"]));
                    //hInputPara.Add("@Government_Value", Convert.ToString(list5.Rows[i1]["txtgovtvalue"]));
                    //hInputPara.Add("@MeasuredSaleableArea", Convert.ToString(list5.Rows[i1]["txtMeasuredSaleableArea"])); 
                    //hInputPara.Add("@ApprovedSaleableArea", Convert.ToString(list5.Rows[i1]["txtApprovedSaleableArea"]));
                    //hInputPara.Add("@distressValue", Convert.ToString(list5.Rows[i1]["txtdistress"]));

                    //hInputPara.Add("@realizable_value", Convert.ToString(list5.Rows[i1]["txtrealizable"]));
                    //hInputPara.Add("@matchAt", Convert.ToString(list5.Rows[i1]["matchAt"]));
                    //hInputPara.Add("@riskof", Convert.ToString(list5.Rows[i1]["txtnill"]));

                    //hInputPara.Add("@devdetails", Convert.ToString(list5.Rows[i1]["txtdevunit"]));
                    //hInputPara.Add("@devunit", Convert.ToString(list5.Rows[i1]["txtdev"]));





                    hInputPara.Add("@OtherPlotType", radioValue);
                    hInputPara.Add("@Extra_Cost_Valuation", Convert.ToString(list5.Rows[i1]["txtextracost"]));
                    hInputPara.Add("@Work_Completed_Perc", Convert.ToString(list5.Rows[i1]["txtPerworkcompleted"]));
                    hInputPara.Add("@StageOfConstructionBunglow", Convert.ToString(list5.Rows[i1]["ddlstagecons"]));
                    hInputPara.Add("@StageOfConstructionPerct", Convert.ToString(list5.Rows[i1]["txtstgconsperc"]));
                    hInputPara.Add("@DisbursementRecommendPerct", Convert.ToString(list5.Rows[i1]["txtdisbrecom"]));
                    hInputPara.Add("@IsInteriorworkcarriedout", Convert.ToString(list5.Rows[i1]["isinteriorwork"]));
                    hInputPara.Add("@DistressValuePerct", Convert.ToString(list5.Rows[i1]["txtdistressvalperc"]));
                    hInputPara.Add("@Government_Rate", Convert.ToString(list5.Rows[i1]["txtgovtrate"]));
                    hInputPara.Add("@DescStageofconstructionBunglow", Convert.ToString(list5.Rows[i1]["txtdescundercons"]));
                    hInputPara.Add("@Gross_Estimated_Cost_Construction", Convert.ToString(list5.Rows[i1]["txtgrossestimate"]));
                    hInputPara.Add("@Cost_Construction_PerSqFT", Convert.ToString(list5.Rows[i1]["txtcostcons"]));
                    hInputPara.Add("@Does_this_Matches_UnderConstruction", Convert.ToString(list5.Rows[i1]["ddladoptedcostmatch"]));
                    hInputPara.Add("@Does_this_Cost_Remark", Convert.ToString(list5.Rows[i1]["txtremarkundercons"]));
                    hInputPara.Add("@Cost_of_InteriorReady", Convert.ToString(list5.Rows[i1]["txtcostinterior"]));
                    hInputPara.Add("@StageOfConstructionUC", Convert.ToString(list5.Rows[i1]["txtstgconspercUC"]));
                    hInputPara.Add("@DisbursementRecommendPerctUC", Convert.ToString(list5.Rows[i1]["txtdisbrecomUC"]));
                    //hInputPara.Add("@TotalFlatwords", Convert.ToString(list5.Rows[i1]["txttotalflatwords"]));
                    hInputPara.Add("@TotalPlotwords", Convert.ToString(list5.Rows[i1]["txttotalplotwords"]));
                    hInputPara.Add("@TotalreadyBunglowWords", Convert.ToString(list5.Rows[i1]["txttotalbungowreadywords"]));
                    hInputPara.Add("@TotalUCBunglowWords", Convert.ToString(list5.Rows[i1]["txttotalbungowUCwords"]));
                    hInputPara.Add("@TotalCostCons", Convert.ToString(list5.Rows[i1]["sum7"]));
                    hInputPara.Add("@Distressvalpercentplot", Convert.ToString(list5.Rows[i1]["txtdistressvalpercentplot"]));
                    
                    //new
                    hInputPara.Add("@MeasuredAreaFt", Convert.ToString(list5.Rows[i1]["txtMeasuredAreaFt"]));
                    hInputPara.Add("@ApprovedAreaF", Convert.ToString(list5.Rows[i1]["txtApprovedAreaFt"]));
                    hInputPara.Add("@AdoptedAValFt", Convert.ToString(list5.Rows[i1]["txtAdoptedAValFt"]));
                    hInputPara.Add("@AdoptedLandRFt", Convert.ToString(list5.Rows[i1]["txtAdoptedLandRFt"]));
                    hInputPara.Add("@MeasuredAreaMt", Convert.ToString(list5.Rows[i1]["txtMeasuredAreaMt"]));
                    hInputPara.Add("@ApprovedAreaMt", Convert.ToString(list5.Rows[i1]["txtApprovedAreaMt"]));
                    hInputPara.Add("@AdoptedAValMt", Convert.ToString(list5.Rows[i1]["txtAdoptedAValMt"]));
                    hInputPara.Add("@AdoptedLandRMt", Convert.ToString(list5.Rows[i1]["txtAdoptedLandRMt"]));

                    hInputPara.Add("@MeasuredOtherPAFt", Convert.ToString(list5.Rows[i1]["txtMeasuredOtherPAFt"]));
                    hInputPara.Add("@ApproveOtherPAFt", Convert.ToString(list5.Rows[i1]["txtApproveOtherPAFt"]));
                    hInputPara.Add("@AdoptedOtherPAValFt", Convert.ToString(list5.Rows[i1]["txtAdoptedOtherPAValFt"]));
                    hInputPara.Add("@AdoptedOtherPLandRFt", Convert.ToString(list5.Rows[i1]["txtAdoptedOtherPLandRFt"]));
                    hInputPara.Add("@MeasuredOtherPAMt", Convert.ToString(list5.Rows[i1]["txtMeasuredOtherPAMt"]));
                    hInputPara.Add("@ApproveOtherPAMt", Convert.ToString(list5.Rows[i1]["txtApproveOtherPAMt"]));
                    hInputPara.Add("@AdoptedOtherPAValMt", Convert.ToString(list5.Rows[i1]["txtAdoptedOtherPAValMt"]));
                    hInputPara.Add("@AdoptedOtherPLandRMt", Convert.ToString(list5.Rows[i1]["txtAdoptedOtherPLandRMt"]));

                    hInputPara.Add("@Permissible", Convert.ToString(list5.Rows[i1]["txtPermissible"]));
                    hInputPara.Add("@LandRateRangeLoc", Convert.ToString(list5.Rows[i1]["txtLandRateRangeLoc"]));

                    hInputPara.Add("@PermissibleOtherplot", Convert.ToString(list5.Rows[i1]["txtPermissibleOtherplot"]));
                    hInputPara.Add("@GovtRateSF", Convert.ToString(list5.Rows[i1]["txtgovtrateftnew"]));
                    hInputPara.Add("@GovtvalueS", Convert.ToString(list5.Rows[i1]["txtgovtratevalue"]));
                    if (Session["TypeOfUnit"].ToString() == "Bungalow")
                    {
                        hInputPara.Add("@RealPercPlot", Convert.ToString(list5.Rows[i1]["txtrealpercent1"]));
                        hInputPara.Add("@RealizablePlot", Convert.ToString(list5.Rows[i1]["txtrealizablevalue1"]));
                    }
                    if (Session["TypeOfUnit"].ToString() == "Plot")
                    {
                        hInputPara.Add("@RealPercPlot", Convert.ToString(list5.Rows[i1]["txtrealpercent"]));
                        hInputPara.Add("@RealizablePlot", Convert.ToString(list5.Rows[i1]["txtrealizablevalue"]));
                    }
                    if (Session["TypeOfUnit"].ToString() != "Plot" && Session["TypeOfUnit"].ToString() != "Bungalow")
                    {
                        hInputPara.Add("@RealPercPlot", Convert.ToString(list5.Rows[i1]["txtrealpercent"]));
                        hInputPara.Add("@RealizablePlot", Convert.ToString(list5.Rows[i1]["txtrealizablevalue"]));
                    }
                    var r1 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateretailunitTotalsSingleUnit", hInputPara);

                    hInputPara.Clear();


                    Hashtable hInputPara22 = new Hashtable();
                    hInputPara22.Add("@Project_ID", Session["Project_ID"].ToString());
                    hInputPara22.Add("@Remarks", Convert.ToString(list5.Rows[i1]["remarks"]));
                    var res2 = sqlDataAccess.ExecuteStoreProcedure("usp_updateRemarks", hInputPara22);

                    hInputPara22.Clear();



                    //hInputPara.Add("@Unit_ID", Session["Unit_ID"].ToString());
                    //if(list5.Rows[i1]["txttotalvalue"].ToString()!="")
                    //{
                    //    hInputPara.Add("@Total_Value_of_Property", Convert.ToDouble(list5.Rows[i1]["txttotalvalue"]));
                    //}
                    //if (list5.Rows[i1]["currentValueOfPropertyUnderConstructionUnitId"].ToString() != "")
                    //{
                    //    hInputPara.Add("@Current_Value_of_Property", Convert.ToDouble(list5.Rows[i1]["currentValueOfPropertyUnderConstructionUnitId"]));
                    //}
                    //hInputPara.Add("@Cost_of_Interior", Convert.ToString(list5.Rows[i1]["txtcostofinterior"].ToString()));
                    //if (list5.Rows[i1]["txtgovtvalue"].ToString() != "")
                    //{
                    //    hInputPara.Add("@Government_Value", Convert.ToDouble(list5.Rows[i1]["txtgovtvalue"]));
                    //}
                    //if (list5.Rows[i1]["txtMeasuredSaleableArea"].ToString() != "")
                    //{
                    //    hInputPara.Add("@MeasuredSaleableArea", Convert.ToDouble(list5.Rows[i1]["txtMeasuredSaleableArea"]));
                    //}
                    //if (list5.Rows[i1]["txtApprovedSaleableArea"].ToString() != "")
                    //{
                    //    hInputPara.Add("@ApprovedSaleableArea", Convert.ToDouble(list5.Rows[i1]["txtApprovedSaleableArea"]));
                    //}
                    //if (list5.Rows[i1]["txtdistress"].ToString() != "")
                    //{
                    //    hInputPara.Add("@distressValue", Convert.ToDouble(list5.Rows[i1]["txtdistress"]));
                    //}
                    //hInputPara.Add("@ValuationResult", Convert.ToString(list5.Rows[i1]["ddlvaluation"]));

                }
                //UploadImages
                for (int i = 0; i < list8.Rows.Count; i++)
                {
                    string ImageFile = "";
                    //get file name for adding prefix of request and documentId
                    string file = list8.Rows[i]["Imagepath"].ToString();
                    string imageTypeId = list8.Rows[i]["ImageTypeId"].ToString();
                    string seqNO = list8.Rows[i]["ImageSequNo"].ToString();
                    string imagetypeIDNew = seqNO + imageTypeId;
                    string a = imagetypeIDNew.PadLeft(2, '0');
                    if (file != "")
                    {
                        string[] newValue = file.Split('\\');
                        if (newValue != null)
                        {
                            string Name = newValue[newValue.Length - 1];
                            ImageFile = RequestID + "_" + a + "_" + Name;


                        }
                    }
                    if ((list8.Rows[i]["ImageTypeId"].ToString()) != String.Empty && (list8.Rows[i]["ImageTypeId"].ToString()) != null)
                    {
                        hInputPara.Add("@Request_ID", Convert.ToInt32(RequestID));
                        hInputPara.Add("@projectId", Convert.ToInt32(Session["Project_ID"]));
                        hInputPara.Add("@imageTypeId", Convert.ToInt32(list8.Rows[i]["ImageTypeId"]));
                        hInputPara.Add("@SeqId", Convert.ToInt32(list8.Rows[i]["ImageSequNo"]));
                        string result = sqlDataAccess.ExecuteStoreProcedure("usp_checkUploadImageUnitExist", hInputPara);
                        hInputPara.Clear();
                        if (result == "document already exist")
                        {
                            sqlDataAccess = new SQLDataAccess();

                            if (list8.Rows[i]["Imagepath"].ToString() != string.Empty && (list8.Rows[i]["Imagepath"].ToString()) != null)
                            {
                                hInputPara.Add("@RequestId", Convert.ToInt32(RequestID));
                                hInputPara.Add("@projectId", Convert.ToInt32(Session["Project_ID"]));
                                hInputPara.Add("@imageTypeId", Convert.ToInt32(list8.Rows[i]["ImageTypeId"]));
                                hInputPara.Add("@segId", Convert.ToInt32(list8.Rows[i]["ImageSequNo"]));
                                hInputPara.Add("@imagePath", ImageFile);

                                sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_UpdateUnitImageUploaded", hInputPara);
                                hInputPara.Clear();
                            }
                            else
                            {
                                hInputPara.Add("@RequestId", Convert.ToInt32(RequestID));
                                hInputPara.Add("@projectId", Convert.ToInt32(Session["Project_ID"]));
                                hInputPara.Add("@imageTypeId", Convert.ToInt32(list8.Rows[i]["ImageTypeId"]));
                                hInputPara.Add("@segId", Convert.ToInt32(list8.Rows[i]["ImageSequNo"]));
                                hInputPara.Add("@UnitID", Convert.ToInt32(Session["Unit_ID"]));
                                //hInputPara.Add("@imagePath", Convert.ToString(list8.Rows[i]["Imagepath"]));

                                sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_UpdateUnitBlankImagePathUploaded", hInputPara);
                                hInputPara.Clear();
                            }
                        }
                        else
                        {
                            hInputPara = new Hashtable();
                            sqlDataAccess = new SQLDataAccess();
                            if (list8.Rows[i]["Imagepath"].ToString() != string.Empty && list8.Rows[i]["Imagepath"].ToString() != null)
                            {
                                string ImagePath = "";
                                hInputPara.Add("@Project_ID", Session["Project_ID"]);
                                hInputPara.Add("@Unit_ID", Session["Unit_ID"]);
                                hInputPara.Add("@RequestId", Convert.ToInt32(RequestID));
                                hInputPara.Add("@ImageTypeId", Convert.ToInt32(list8.Rows[i]["ImageTypeId"]));
                                string ImageName = list8.Rows[i]["Imagepath"].ToString();
                                if (ImageName != "")
                                {
                                    string[] newValue = ImageName.Split('\\');
                                    if (newValue != null)
                                    {
                                        string ImageTypeId = Convert.ToString(list8.Rows[i]["ImageTypeId"]);
                                        string imagesqNO = list8.Rows[i]["ImageSequNo"].ToString();
                                        string ImageTypeIdNew = imagesqNO + ImageTypeId;
                                        string ImTypeId = ImageTypeIdNew.PadLeft(2, '0');
                                        string Name = newValue[newValue.Length - 1];
                                        ImagePath = RequestID + "_" + ImTypeId + "_" + Name;
                                    }
                                }
                                hInputPara.Add("@ImagePath", ImagePath);
                                hInputPara.Add("@SequenceNo", Convert.ToInt32(list8.Rows[i]["ImageSequNo"]));
                                var result1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertImageUploadImagesNew", hInputPara);
                                hInputPara.Clear();
                            }

                        }
                    }
                }

                return Json(new { success = true, Message = "Unit Details Inserted Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public ActionResult SubsequentUnitDetails()
        {
            int RequestID = Convert.ToInt32(Session["SubRequest_ID"]);
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            ViewBag.UnitType = Session["TypeOfUnit"].ToString();
            ViewBag.PType = Session["Property_Type"].ToString();
            //ViewBag.UnitType = "Flat";
            //ViewBag.PType = "Residential";
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dtunitREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllunitsBYReqID", hInputPara);
            ViewBag.Deviation_Description = dtunitREQ.Rows[0]["Deviation_Description"].ToString();
            ViewBag.Construction_as_per_plan = dtunitREQ.Rows[0]["Construction_as_per_plan"].ToString();
            ViewBag.Risk_of_Demolition = dtunitREQ.Rows[0]["Risk_of_Demolition"].ToString();
            ViewBag.Type_of_Flooring = dtunitREQ.Rows[0]["Type_of_Flooring"].ToString();
            ViewBag.Wall_Finish = dtunitREQ.Rows[0]["Wall_Finish"].ToString();
            ViewBag.Plumbing_Fitting = dtunitREQ.Rows[0]["Plumbing_Fitting"].ToString();
            ViewBag.Electrical_Fittings = dtunitREQ.Rows[0]["Electrical_Fittings"].ToString();
            ViewBag.Rough_Plaster = dtunitREQ.Rows[0]["Rough_Plaster"].ToString();
            ViewBag.Tiling = dtunitREQ.Rows[0]["Tiling"].ToString();
            ViewBag.Cord_Longitude = dtunitREQ.Rows[0]["Cord_Longitude"].ToString();
            ViewBag.Cord_Latitude = dtunitREQ.Rows[0]["Cord_Latitude"].ToString();
            ViewBag.Road1 = dtunitREQ.Rows[0]["Road1"].ToString();
            ViewBag.Road2 = dtunitREQ.Rows[0]["Road2"].ToString();
            ViewBag.Road3 = dtunitREQ.Rows[0]["Road3"].ToString();
            ViewBag.Road4 = dtunitREQ.Rows[0]["Road4"].ToString();
            ViewBag.SeismicZone = dtunitREQ.Rows[0]["SeismicZone"].ToString();
            ViewBag.FloodZone = dtunitREQ.Rows[0]["FloodZone"].ToString();
            ViewBag.CycloneZone = dtunitREQ.Rows[0]["CycloneZone"].ToString();
            ViewBag.CRZ = dtunitREQ.Rows[0]["CRZ"].ToString();
            ViewBag.Remarks = dtunitREQ.Rows[0]["Remarks"].ToString();
            ViewBag.Plot_Demarcated_at_site = dtunitREQ.Rows[0]["Plot_Demarcated_at_site"].ToString();
            ViewBag.Type_of_Demarcation = dtunitREQ.Rows[0]["Type_of_Demarcation"].ToString();

            ViewBag.Boundaries_Matching = dtunitREQ.Rows[0]["Boundaries_Matching"].ToString();
            ViewBag.Road_Finish = dtunitREQ.Rows[0]["Road_Finish"].ToString();
            ViewBag.AreBoundariesremarks = dtunitREQ.Rows[0]["AreBoundariesremarks"].ToString();

            ViewBag.Name = dtunitREQ.Rows[0]["Name"].ToString();
            ViewBag.Property_Identified_PName = dtunitREQ.Rows[0]["Property_Identified_PName"].ToString();
            ViewBag.P_Contact_Num = dtunitREQ.Rows[0]["P_Contact_Num"].ToString();
            ViewBag.Customer_Name_On_Board = dtunitREQ.Rows[0]["CustomerDisplayBoard"].ToString();
            ViewBag.Society_OR_Building_Name = dtunitREQ.Rows[0]["Society_OR_Building_Name"].ToString();

            ViewBag.Current_Value_of_Property = dtunitREQ.Rows[0]["Current_Value_of_Property"].ToString();
            ViewBag.Total_Value_of_Property = dtunitREQ.Rows[0]["Total_Value_of_Property"].ToString();
            ViewBag.Government_Value = dtunitREQ.Rows[0]["Government_Value"].ToString();
            ViewBag.Cost_of_Interior = dtunitREQ.Rows[0]["Cost_of_Interior"].ToString();
            ViewBag.MeasuredSaleableArea = dtunitREQ.Rows[0]["MeasuredSaleableArea"].ToString();
            ViewBag.ApprovedSaleableArea = dtunitREQ.Rows[0]["ApprovedSaleableArea"].ToString();
            ViewBag.distressValue = dtunitREQ.Rows[0]["distressValue"].ToString();
            ViewBag.StageOfConstruction = dtunitREQ.Rows[0]["StageOfConstruction"].ToString();
            ViewBag.IsInteriorworkcarriedout = dtunitREQ.Rows[0]["IsInteriorworkcarriedout"].ToString();
            ViewBag.ValuationResult = dtunitREQ.Rows[0]["ValuationResult"].ToString();
            ViewBag.DescStageofconstruction = dtunitREQ.Rows[0]["DescStageofconstruction"].ToString();
            ViewBag.No_Of_Lifts = dtunitREQ.Rows[0]["No_Of_Lifts"].ToString();
            ViewBag.IsLifts_Present = dtunitREQ.Rows[0]["IsLifts_Present"].ToString();
            ViewBag.ViewFromUnit = dtunitREQ.Rows[0]["ViewFromUnit"].ToString();
            ViewBag.MeasuredApprovedareamatchatsite = dtunitREQ.Rows[0]["MeasuredApprovedareamatchatsite"].ToString();
            ViewBag.DeviationPercentage = dtunitREQ.Rows[0]["DeviationPercentage"].ToString();
            ViewBag.Value_of_Plot = dtunitREQ.Rows[0]["Value_of_Plot"].ToString();
            ViewBag.Total_Land_Plot_Value = dtunitREQ.Rows[0]["Total_Land_Plot_Value"].ToString();
            ViewBag.Value_of_other_Plot = dtunitREQ.Rows[0]["Value_of_other_Plot"].ToString();
            ViewBag.Distress_Value_of_Plot = dtunitREQ.Rows[0]["Distress_Value_of_Plot"].ToString();
            ViewBag.OtherPlotType = dtunitREQ.Rows[0]["OtherPlotType"].ToString();
            ViewBag.Extra_Cost_Valuation = dtunitREQ.Rows[0]["Extra_Cost_Valuation"].ToString();

            ViewBag.Work_Completed_Perc = dtunitREQ.Rows[0]["Work_Completed_Perc"].ToString();
            ViewBag.StageOfConstructionBunglow = dtunitREQ.Rows[0]["StageOfConstructionBunglow"].ToString();
            ViewBag.StageOfConstructionPerct = dtunitREQ.Rows[0]["StageOfConstructionPerct"].ToString();
            ViewBag.DisbursementRecommendPerct = dtunitREQ.Rows[0]["DisbursementRecommendPerct"].ToString();
            ViewBag.IsInteriorworkcarriedout = dtunitREQ.Rows[0]["IsInteriorworkcarriedout"].ToString();
            ViewBag.DistressValuePerct = dtunitREQ.Rows[0]["DistressValuePerct"].ToString();
            ViewBag.Government_Rate = dtunitREQ.Rows[0]["Government_Rate"].ToString();
            ViewBag.DescStageofconstructionBunglow = dtunitREQ.Rows[0]["DescStageofconstructionBunglow"].ToString();
            ViewBag.Gross_Estimated_Cost_Construction = dtunitREQ.Rows[0]["Gross_Estimated_Cost_Construction"].ToString();
            ViewBag.Cost_Construction_PerSqFT = dtunitREQ.Rows[0]["Cost_Construction_PerSqFT"].ToString();
            ViewBag.Does_this_Matches_UnderConstruction = dtunitREQ.Rows[0]["Does_this_Matches_UnderConstruction"].ToString();
            ViewBag.Does_this_Cost_Remark = dtunitREQ.Rows[0]["Does_this_Cost_Remark"].ToString();

            ViewBag.Valueoftheplotbungalow = dtunitREQ.Rows[0]["Valueoftheplotbungalow"].ToString();
            ViewBag.Valueoftheotherplotbungalow = dtunitREQ.Rows[0]["Valueoftheotherplotbungalow"].ToString();
            ViewBag.Totalofthebungalow = dtunitREQ.Rows[0]["Totalofthebungalow"].ToString();
            ViewBag.TotalvaluepropertyBunglow = dtunitREQ.Rows[0]["TotalvaluepropertyBunglow"].ToString();
            ViewBag.DistressValueofPropertyBunglow = dtunitREQ.Rows[0]["DistressValueofPropertyBunglow"].ToString();
            ViewBag.TotalvaluepropertyBunglowUC = dtunitREQ.Rows[0]["TotalvaluepropertyBunglowUC"].ToString();
            ViewBag.Current_Value_of_Property_UC = dtunitREQ.Rows[0]["Current_Value_of_Property_UC"].ToString();
            ViewBag.ValuationResultR = dtunitREQ.Rows[0]["ValuationResultR"].ToString();
            ViewBag.ValuationResultUC = dtunitREQ.Rows[0]["ValuationResultUC"].ToString();
            ViewBag.Cost_of_InteriorReady = dtunitREQ.Rows[0]["Cost_of_InteriorReady"].ToString();
            ViewBag.StageOfConstructionUC = dtunitREQ.Rows[0]["StageOfConstructionUC"].ToString();
            ViewBag.DisbursementRecommendPerctUC = dtunitREQ.Rows[0]["DisbursementRecommendPerctUC"].ToString();
            ViewBag.ValuationResultplot = dtunitREQ.Rows[0]["ValuationResultplot"].ToString();

            ViewBag.MeasuredAreaFt = dtunitREQ.Rows[0]["MeasuredAreaFt"].ToString();
            ViewBag.ApprovedAreaF = dtunitREQ.Rows[0]["ApprovedAreaF"].ToString();
            ViewBag.AdoptedAValFt = dtunitREQ.Rows[0]["AdoptedAValFt"].ToString();
            ViewBag.AdoptedLandRFt = dtunitREQ.Rows[0]["AdoptedLandRFt"].ToString();

            ViewBag.MeasuredAreaMt = dtunitREQ.Rows[0]["MeasuredAreaMt"].ToString();
            ViewBag.ApprovedAreaMt = dtunitREQ.Rows[0]["ApprovedAreaMt"].ToString();
            ViewBag.AdoptedAValMt = dtunitREQ.Rows[0]["AdoptedAValMt"].ToString();
            ViewBag.AdoptedLandRMt = dtunitREQ.Rows[0]["AdoptedLandRMt"].ToString();

            ViewBag.MeasuredOtherPAFt = dtunitREQ.Rows[0]["MeasuredOtherPAFt"].ToString();
            ViewBag.ApproveOtherPAFt = dtunitREQ.Rows[0]["ApproveOtherPAFt"].ToString();
            ViewBag.AdoptedOtherPAValFt = dtunitREQ.Rows[0]["AdoptedOtherPAValFt"].ToString();
            ViewBag.AdoptedOtherPLandRFt = dtunitREQ.Rows[0]["AdoptedOtherPLandRFt"].ToString();

            ViewBag.MeasuredOtherPAMt = dtunitREQ.Rows[0]["MeasuredOtherPAMt"].ToString();
            ViewBag.ApproveOtherPAMt = dtunitREQ.Rows[0]["ApproveOtherPAMt"].ToString();
            ViewBag.AdoptedOtherPAValMt = dtunitREQ.Rows[0]["AdoptedOtherPAValMt"].ToString();
            ViewBag.AdoptedOtherPLandRMt = dtunitREQ.Rows[0]["AdoptedOtherPLandRMt"].ToString();

            ViewBag.Permissible = dtunitREQ.Rows[0]["Permissible"].ToString();
            ViewBag.LandRateRangeLoc = dtunitREQ.Rows[0]["LandRateRangeLoc"].ToString();
            ViewBag.PermissibleOtherplot = dtunitREQ.Rows[0]["PermissibleOtherplot"].ToString();
            ViewBag.GovtRateSF = dtunitREQ.Rows[0]["GovtRateSF"].ToString();
            ViewBag.GovtvalueS = dtunitREQ.Rows[0]["GovtvalueS"].ToString();


            ViewBag.MatchAt = dtunitREQ.Rows[0]["MatchAt"].ToString();
            ViewBag.nill = dtunitREQ.Rows[0]["RiskOf"].ToString();
            ViewBag.DevDeatils = dtunitREQ.Rows[0]["DevDetails"].ToString();
            ViewBag.DevUnit = dtunitREQ.Rows[0]["DevUnit"].ToString();
            ViewModelRequest vm = new ViewModelRequest();

            //vm.ival_UnitParameterMasters = GetUnitDetails(unitname);
            vm.ival_ProjectFloorWiseBreakUps = GetFloorwisebreakbyreqID();
            vm.ival_ProjectApprovalDetails = GetApprovalTypesnew();
            vm.ival_ProjectBoundaries = GetBoundariesByReqID();
            vm.ival_ProjectSideMargins = GetSidearginsByReqID();
            vm.ival_LookupCategoryValuesviewfromunit = GetviewUnits();
            vm.ival_LookupCategoryValuesRiskofdemolition = Getdemolition();
            vm.ival_LookupCategoryValuesdemarcation = GetDemarcation();
            vm.ival_LookupCategoryValuesdemarcation1 = GetDemarcation1();
            vm.ival_LookupCategoryValuesRoadFinish = GetRoadFinish();
            vm.ival_LookupCategoryValuesPropZone = GetPropZone();
            vm.ival_BrokerDetails = GetbrokerBYreqID();
            vm.iva_Comparable_Properties = GetComprabledetreqID();
            vm.ival_LookupCategoryLiftTypes = GetLiftType();




            vm.ival_LookupCategoryFloorTypes = GetFloorType();
            vm.ival_ApprovalType = GetApprovalType();
            vm.ival_LookupCategoryBoundaries = GetBoundaries();
            vm.ival_LookupCategorySideMargins = GetSideMargins();
            vm.ival_LookupCategoryValuesviewfromunit = GetviewUnits();
            vm.ival_LookupCategoryValuesRiskofdemolition = Getdemolition();
            vm.ival_LookupCategoryValuesdemarcation = GetDemarcation();
            vm.ival_LookupCategoryValuesRoadFinish = GetRoadFinish();

            vm.ival_ProjectApprovalDetails = GetApprovalTypesnew();
            vm.ival_ImageMasters = GetImageMaster();
            //hInputPara1 = new Hashtable();
            //sqlDataAccess = new SQLDataAccess();
            //byte[] imageByteData = { };
            //hInputPara1.Add("@Request_ID", RequestID);
            //DataTable dtImages = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetImagesByReqID", hInputPara1);
            //if (dtImages.Rows.Count > 0)
            //{
            //    for (int i = 0; i < dtImages.Rows.Count; i++)
            //    {

            //        string imagename = dtImages.Rows[i]["Image_Path"].ToString();
            //        string path = Server.MapPath("~/images/" + imagename);
            //        imageByteData = System.IO.File.ReadAllBytes(path);
            //        ViewBag.Images = Directory.EnumerateFiles(path)
            //                        .Select(fn => "~/Images/" + Path.GetFileName(imagename));

            //        //ViewBag.Images= Directory.EnumerateFiles((path));
            //    }
            //    // ViewBag.Images = imageByteData;
            //}


            return View(vm);

        }
        [HttpPost]
        public JsonResult TestImage()
        {

            var ImageFile = Session["ImageSave"];
            Session["ImageSaveNewNew"] = ImageFile;
            ViewBag.Image1 = ImageFile;
            TempData["ImageS"] = ImageFile;
            return Json("Ok");
        }
    }

}

