﻿using I_Value.WebApp.Models;
using IvalueDataAccess;
using IValuePublishProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace I_Value.WebApp.Controllers
{
    public class ValuationAndReportController : Controller
    {
        // GET: ValuationAndReport
        SQLDataAccess sqlDataAccess;
        Hashtable hInputPara;
        public ActionResult Index()
        {
            VMReport_TypeMaster vMReport_TypeMaster = new VMReport_TypeMaster();
            vMReport_TypeMaster.ival_ValuationReportList = GetValuationReportList();
            return View(vMReport_TypeMaster);
        }
        [HttpPost]
        public ActionResult AddValuationType(string ValuationTypeName)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Lookup_Value", ValuationTypeName);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertValuationType", hInputPara);
                if (Convert.ToString(result) == "1")
                {
                    return Json(new { success = true });

                }
                else if (Convert.ToString(result) == "Valuation Type already exist")
                {
                    return Json(new { success = false, message = "Valuation Type already exist" });
                }
                else
                {
                    return Json(new { success = false, message = "Error while Saving record!" });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult GetValuationList()
        {

            VMLookupCategoryValues VMLookupCategoryValues = new VMLookupCategoryValues();
            VMLookupCategoryValues.ival_ValuationTypeList = GetValuationTypeList();
            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string ival_ValuationTypeListfilter = serializer.Serialize(VMLookupCategoryValues.ival_ValuationTypeList);
            return Json(ival_ValuationTypeListfilter);
        }

        public IEnumerable<ival_LookupCategoryValues> GetValuationTypeList()
        {
            List<ival_LookupCategoryValues> listValuationType = new List<ival_LookupCategoryValues>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtValuationTypeList = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetValuationType");
            for (int i = 0; i < dtValuationTypeList.Rows.Count; i++)
            {

                listValuationType.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToUInt16(dtValuationTypeList.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtValuationTypeList.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtValuationTypeList.Rows[i]["Lookup_Value"])

                });

            }
            return listValuationType.AsEnumerable();
        }
        public IEnumerable<ZNIU_ival_Report_TypeMaster> GetValuationReportList()
        {
            List<ZNIU_ival_Report_TypeMaster> listValuationReport = new List<ZNIU_ival_Report_TypeMaster>();
            sqlDataAccess = new SQLDataAccess();
            try
            {
                DataTable dtValuationReportlist = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetValuationReportList");
                for (int i = 0; i < dtValuationReportlist.Rows.Count; i++)
                {

                    listValuationReport.Add(new ZNIU_ival_Report_TypeMaster
                    {
                        //Valuation_Type_ID = Convert.ToInt32(dtValuationReportlist.Rows[i]["Valuation_Type_ID"]),
                        Name = Convert.ToString(dtValuationReportlist.Rows[i]["Name"]),
                        Lookup_ID = Convert.ToInt32(dtValuationReportlist.Rows[i]["Lookup_ID"]),
                        Lookup_Type = Convert.ToString(dtValuationReportlist.Rows[i]["Lookup_Type"]),
                        Lookup_Value = Convert.ToString(dtValuationReportlist.Rows[i]["Lookup_Value"])

                    });

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return listValuationReport.AsEnumerable();
        }
        [HttpPost]
        public ActionResult EditReportType(int ID, string Name, string ToeditName)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Valuation_ID", ID);
                hInputPara.Add("@Name", Name);
                hInputPara.Add("@ToeditName", ToeditName);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateReportType", hInputPara);
                if (Convert.ToString(result) == "1")
                {
                    return Json(new { success = true, message = "Report Type Updated Successfully" });

                }
                else if (Convert.ToString(result) == "Report Type already exists")
                {
                    return Json(new { success = false, message = "Report Type already exists" });
                }
                else
                {
                    return Json(new { success = false, message = "Error while Updating record!" });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public ActionResult DeleteReportType(int ID, string Name)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Valuation_ID", ID);
                hInputPara.Add("@Name", Name);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteReportType", hInputPara);
                if (Convert.ToString(result) == "1")
                {
                    return Json(new { success = true, message = "Report Type Deleted Successfully" });

                }
                else
                {
                    return Json(new { success = false, message = "Error while deleting!" });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public ActionResult AddReportType(int ID, string Name)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Valuation_ID", ID);
                hInputPara.Add("@Name", Name);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertReportType", hInputPara);
                if (Convert.ToString(result) == "1")
                {
                    return Json(new { success = true, message = "Report Type Inserted Successfully" });

                }
                else if (Convert.ToString(result) == "Report Type already exist")
                {
                    return Json(new { success = false, message = "Report Type already exists" });
                }
                else
                {
                    return Json(new { success = false, message = "Error while Inserting!" });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}