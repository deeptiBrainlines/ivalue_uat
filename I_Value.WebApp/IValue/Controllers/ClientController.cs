﻿using I_Value.WebApp.Models;
using IvalueDataAccess;
using IValuePublishProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace I_Value.WebApp.Controllers
{
    public class ClientController : Controller
    {
        // GET: Client
        SQLDataAccess sqlDataAccess;
        Hashtable hInputPara;
        VMClientMaster vMClientMaster;
        ival_ClientMaster objClient;

        public ActionResult Index()
        {

            try
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                DataTable dtClient = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClient");

                List<ival_ClientMaster> listClient = new List<ival_ClientMaster>();

                if (dtClient.Rows.Count > 0)

                {
                    for (int i = 0; i < dtClient.Rows.Count; i++)
                    {

                        listClient.Add(new ival_ClientMaster
                        {
                            Client_ID = Convert.ToInt32(dtClient.Rows[i]["Client_ID"]),
                            Client_Name = Convert.ToString(dtClient.Rows[i]["Client_Name"]),
                            Branch_Name = Convert.ToString(dtClient.Rows[i]["Branch_Name"]),
                            Client_State = Convert.ToString(dtClient.Rows[i]["Client_State"]),

                        });


                    }
                }
                return View(listClient);

            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
        
        public ActionResult AddNewClient()
        {
            vMClientMaster = new VMClientMaster();
            vMClientMaster.ival_StateList = GetState();
            //vMClientMaster.pincodeList = GetPincode();
            vMClientMaster.ival_DistrictsList = GetDistricts();
            vMClientMaster.ival_LocalityList = GetLocality();
            vMClientMaster.ival_CitiesList = GetCity();
            vMClientMaster.ReportTypeList = GetReportType();
            vMClientMaster.ValuationTypeList = GetValuationType();
            return View(vMClientMaster);
        }

        [HttpPost]
        public ActionResult AddNewClient(String str)
        {
            string resultStr = string.Empty;
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

                try
                {
                for (int i = 0; i < list1.Rows.Count; i++)
                {

                    IEnumerable<ival_States> ival_StatesList = GetState();
                    var state = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMClientMaster.SelectedStateID).Select(row => row.State_Name.ToString());

                    IEnumerable<ival_Districts> ival_DistrictsList = GetDistricts();
                    var Dist = ival_DistrictsList.AsEnumerable().Where(row => row.District_ID == vMClientMaster.SelectedDistID).Select(row => row.District_Name.ToString());

                    IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                    var City = ival_LocalityList.AsEnumerable().Where(row => row.Loc_Id == vMClientMaster.SelectedLocID).Select(row => row.Loc_Name.ToString());

                    var pincode = ival_LocalityList.AsEnumerable().Where(row => row.Loc_Id == vMClientMaster.SelectedBranchPincode).Select(row => row.Pin_Code.ToString());

                    IEnumerable<ival_LookupCategoryValues> reportTypeList = GetReportType();
                    var ReportType = reportTypeList.AsEnumerable().Where(row => row.Lookup_ID == vMClientMaster.SelectedReportTypeID).Select(row => row.Lookup_Value.ToString());

                    IEnumerable<ival_LookupCategoryValues> valuationList = GetValuationType();
                    var ValuationType = valuationList.AsEnumerable().Where(row => row.Lookup_ID == vMClientMaster.SelectedValuationTypeID).Select(row => row.Lookup_Value.ToString());

                     
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();   
                    //              
                    hInputPara = new Hashtable();

                    hInputPara.Add("@Client_Name", Convert.ToString(list1.Rows[i]["Client_Name"]));
                    hInputPara.Add("@Branch_Name", Convert.ToString(list1.Rows[i]["Branch_Name"]));
                    hInputPara.Add("@Client_Address1", Convert.ToString(list1.Rows[i]["Client_Addr1"]));
                    hInputPara.Add("@Client_Address2", Convert.ToString(list1.Rows[i]["Client_Addr2"]));
                    hInputPara.Add("@Client_City", Convert.ToString(list1.Rows[i]["Client_Loc"]));
                   
                    hInputPara.Add("@Client_District", Convert.ToString(list1.Rows[i]["Client_Dist"]));
                    hInputPara.Add("@Client_State", Convert.ToString(list1.Rows[i]["Client_State"]));
                    hInputPara.Add("@Client_Pincode", Convert.ToString(list1.Rows[i]["Client_Pin"]));
                    hInputPara.Add("@Client_First_Name", Convert.ToString(list1.Rows[i]["Client_Fname"]));
                    hInputPara.Add("@Client_Contact_Num", Convert.ToString(list1.Rows[i]["Client_Phone"]));
                    hInputPara.Add("@Client_Designation", Convert.ToString(list1.Rows[i]["Client_Desn"]));
                    hInputPara.Add("@Client_Contact_Email", Convert.ToString(list1.Rows[i]["Client_Mail"]));
                    hInputPara.Add("@PAN_Num", Convert.ToString(list1.Rows[i]["Client_Pan"]));
                    hInputPara.Add("@GSTIN_Num", Convert.ToString(list1.Rows[i]["Client_Gstin"]));
                    hInputPara.Add("@Valuation_Type", Convert.ToString(list1.Rows[i]["val_Type"]));
                    hInputPara.Add("@Report_Type", Convert.ToString(list1.Rows[i]["Repo_Type"]));
                    hInputPara.Add("@Basic_Fees", Convert.ToString(list1.Rows[i]["Base_fee"]));
                    hInputPara.Add("@Additional_Fees", Convert.ToString(list1.Rows[i]["Add_fee"]));
                    hInputPara.Add("@OutofGeo_Fees", Convert.ToString(list1.Rows[i]["outofgeo"]));
                    hInputPara.Add("@Total_Fees", Convert.ToString(list1.Rows[i]["Total_fee"]));
                    hInputPara.Add("@Created_By", string.Empty);
                    hInputPara.Add("@Client_Locality", Convert.ToString(list1.Rows[i]["Client_City"]));

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertClientMaster", hInputPara);

                    if (Convert.ToInt32(result) == 1)
                    {
                        return RedirectToAction("Index", "Client");
                    }
                    else
                    {
                        return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                //e.InnerException.ToString();
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, Message = "SuccessFull" }, JsonRequestBehavior.AllowGet);
        }

        /*[HttpPost]
        public ActionResult AddNewClient(VMClientMaster vMClientMaster)
        {
          //  bool isSuccess = true;
            if (vMClientMaster != null)
            {
                try
                {

                    IEnumerable<ival_States> ival_StatesList = GetState();
                    var state = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMClientMaster.SelectedStateID).Select(row => row.State_Name.ToString());

                    IEnumerable<ival_Districts> ival_DistrictsList = GetDistricts();
                    var Dist = ival_DistrictsList.AsEnumerable().Where(row => row.District_ID == vMClientMaster.SelectedDistID).Select(row => row.District_Name.ToString());

                    IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                    var City = ival_LocalityList.AsEnumerable().Where(row => row.Loc_Id == vMClientMaster.SelectedLocID).Select(row => row.Loc_Name.ToString());

                    IEnumerable<ival_Locality> pincodeList = GetPincode();
                    var pincode = pincodeList.AsEnumerable().Where(row => row.Loc_Id == vMClientMaster.SelectedBranchPincode).Select(row => row.Pin_Code.ToString());

                    IEnumerable<ival_LookupCategoryValues> reportTypeList = GetReportType();
                    var ReportType = reportTypeList.AsEnumerable().Where(row => row.Lookup_ID == vMClientMaster.SelectedReportTypeID).Select(row => row.Lookup_Value.ToString());

                    IEnumerable<ival_LookupCategoryValues> valuationList = GetValuationType();
                    var ValuationType = valuationList.AsEnumerable().Where(row => row.Lookup_ID == vMClientMaster.SelectedValuationTypeID).Select(row => row.Lookup_Value.ToString());


                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    //              
                    hInputPara = new Hashtable();

                    hInputPara.Add("@Client_Name", vMClientMaster.ival_ClientMaster.Client_Name.ToString());
                    hInputPara.Add("@Branch_Name", vMClientMaster.ival_ClientMaster.Branch_Name.ToString());
                    hInputPara.Add("@Client_Address1", vMClientMaster.ival_ClientMaster.Client_Address1.ToString());
                    hInputPara.Add("@Client_Address2", vMClientMaster.ival_ClientMaster.Client_Address2.ToString());
                    hInputPara.Add("@Client_City", vMClientMaster.ival_ClientMaster.Client_City.ToString());
                    hInputPara.Add("@Client_District", vMClientMaster.ival_ClientMaster.Client_District.ToString());
                    hInputPara.Add("@Client_State", state.FirstOrDefault());
                    hInputPara.Add("@Client_Pincode", Convert.ToInt32(pincode.FirstOrDefault()));
                    hInputPara.Add("@Client_First_Name", vMClientMaster.ival_ClientMaster.Client_First_Name.ToString());
                    hInputPara.Add("@Client_Contact_Num", vMClientMaster.ival_ClientMaster.Client_Contact_Num.ToString());
                    hInputPara.Add("@Client_Designation", vMClientMaster.ival_ClientMaster.Client_Designation.ToString());
                    hInputPara.Add("@Client_Contact_Email", vMClientMaster.ival_ClientMaster.Client_Contact_Email.ToString());
                    hInputPara.Add("@PAN_Num", vMClientMaster.ival_ClientMaster.PAN_Num.ToString());
                    hInputPara.Add("@GSTIN_Num", vMClientMaster.ival_ClientMaster.GSTIN_Num.ToString());
                    hInputPara.Add("@Valuation_Type", ValuationType.FirstOrDefault());
                    hInputPara.Add("@Report_Type", ReportType.FirstOrDefault());
                    hInputPara.Add("@Basic_Fees", vMClientMaster.ival_ClientMaster.Basic_Fees.ToString());
                    hInputPara.Add("@Additional_Fees", vMClientMaster.ival_ClientMaster.Additional_Fees.ToString());
                    hInputPara.Add("@OutofGeo_Fees", vMClientMaster.ival_ClientMaster.OutofGeo_Fees.ToString());
                    hInputPara.Add("@Total_Fees", vMClientMaster.ival_ClientMaster.Total_Fees.ToString());
                    hInputPara.Add("@Created_By", string.Empty);

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertClientMaster", hInputPara);

                    if (Convert.ToInt32(result) == 1)
                    {
                        //Json(new { result = isSuccess, responseText = "Add Successfull!" });
                        return RedirectToAction("Index", "Client");
                    }
                    else
                    {
                        // Json(new { result = isSuccess, responseText = "Error in Database Record not Save!" });
                        return Json("Error in Database !", JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception e)
                {
                    //e.InnerException.ToString();
                    string error = e.Message.ToString();
                    return Json(e.Message, JsonRequestBehavior.AllowGet);
                }
            }
            return Json( "Please Fill Details", JsonRequestBehavior.AllowGet);

        }*/
        public ActionResult EditClient(int?ID)
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //return View();
            }
            vMClientMaster = new VMClientMaster();
            objClient = new ival_ClientMaster();
            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Client_ID", ID);
                DataTable dtClient = sqlDataAccess.GetDatatableExecuteStoreProcedure("[usp_GetClientMasterByID]", hInputPara);

                if (dtClient.Rows.Count > 0 && dtClient != null)
                {

                    object valuedtClient = Convert.ToInt32(dtClient.Rows[0]["Client_ID"]);
                    if (dtClient.Columns.Contains("Client_ID") && valuedtClient != null && valuedtClient.ToString()!="")
                        objClient.Client_ID = Convert.ToInt32(valuedtClient);
                    else
                        objClient.Client_ID = 0;

                    object valueClient_Name = dtClient.Rows[0]["Client_Name"];
                    if (dtClient.Columns.Contains("Client_Name") && valueClient_Name != DBNull.Value && valueClient_Name.ToString() != "")
                        objClient.Client_Name = dtClient.Rows[0]["Client_Name"].ToString();
                    else
                        objClient.Client_Name = string.Empty;

                    object valueBranch_Name = dtClient.Rows[0]["Branch_Name"];
                    if (dtClient.Columns.Contains("Branch_Name") && valueBranch_Name != DBNull.Value && valueBranch_Name.ToString() != "")
                        objClient.Branch_Name = dtClient.Rows[0]["Branch_Name"].ToString();
                    else
                        objClient.Branch_Name = string.Empty;

                    object valueClient_First_Name = dtClient.Rows[0]["Client_First_Name"];
                    if (dtClient.Columns.Contains("Client_First_Name") && valueClient_First_Name != DBNull.Value && valueClient_First_Name.ToString() != "")
                        objClient.Client_First_Name = dtClient.Rows[0]["Client_First_Name"].ToString();
                    else
                        objClient.Client_First_Name = string.Empty;

                    object valueClient_Contact_Num = dtClient.Rows[0]["Client_Contact_Num"];
                    if (dtClient.Columns.Contains("Client_Contact_Num") && valueClient_Contact_Num != DBNull.Value && valueClient_Contact_Num.ToString() != "")
                        objClient.Client_Contact_Num = dtClient.Rows[0]["Client_Contact_Num"].ToString();
                    else
                        objClient.Client_Contact_Num = string.Empty;

                    object valueClient_Contact_Email = dtClient.Rows[0]["Client_Contact_Email"];
                    if (dtClient.Columns.Contains("Client_Contact_Email") && valueClient_Contact_Email != DBNull.Value && valueClient_Contact_Email.ToString() != "")
                        objClient.Client_Contact_Email = dtClient.Rows[0]["Client_Contact_Email"].ToString();
                    else
                        objClient.Client_Contact_Email = string.Empty;

                    object valueClient_Designation = dtClient.Rows[0]["Client_Designation"];
                    if (dtClient.Columns.Contains("Client_Designation") && valueClient_Designation != DBNull.Value && valueClient_Designation.ToString() != "")
                        objClient.Client_Designation = dtClient.Rows[0]["Client_Designation"].ToString();
                    else
                        objClient.Client_Designation = string.Empty;

                    object valueClient_Address1 = dtClient.Rows[0]["Client_Address1"];
                    if (dtClient.Columns.Contains("Client_Address1") && valueClient_Address1 != DBNull.Value && valueClient_Address1.ToString() != "")
                        objClient.Client_Address1 = dtClient.Rows[0]["Client_Address1"].ToString();
                    else
                        objClient.Client_Address1 = string.Empty;

                    object valueClient_Address2 = dtClient.Rows[0]["Client_Address2"];
                    if (dtClient.Columns.Contains("Client_Address2") && valueClient_Address2 != DBNull.Value && valueClient_Address2.ToString() != "")
                        objClient.Client_Address2 = dtClient.Rows[0]["Client_Address2"].ToString();
                    else
                        objClient.Client_Address2 = string.Empty;

                    object valueClient_City = dtClient.Rows[0]["Client_City"];
                    if (dtClient.Columns.Contains("Client_City") && valueClient_City != DBNull.Value && valueClient_City.ToString() != "")
                        objClient.Client_City = dtClient.Rows[0]["Client_City"].ToString();
                    else
                        objClient.Client_City = string.Empty;

                    object valueClient_Locality = dtClient.Rows[0]["Client_Locality"];
                    if (dtClient.Columns.Contains("Client_Locality") && valueClient_Locality != DBNull.Value && valueClient_Locality.ToString() != "")
                        objClient.Client_Locality = dtClient.Rows[0]["Client_Locality"].ToString();
                    else
                        objClient.Client_Locality = string.Empty;

                    object valueClient_District = dtClient.Rows[0]["Client_District"];
                    if (dtClient.Columns.Contains("Client_District") && valueClient_District != DBNull.Value && valueClient_District.ToString() != "")
                        objClient.Client_District = dtClient.Rows[0]["Client_District"].ToString();
                    else
                        objClient.Client_District = string.Empty;

                    object valueClient_State = dtClient.Rows[0]["Client_State"];
                    if (dtClient.Columns.Contains("Client_State") && valueClient_State != DBNull.Value && valueClient_State.ToString() != "")
                        objClient.Client_State = dtClient.Rows[0]["Client_State"].ToString();
                    else
                        objClient.Client_State = string.Empty;

                    object valueClient_Pincode = dtClient.Rows[0]["Client_Pincode"];
                    if (dtClient.Columns.Contains("Client_Pincode") && valueClient_Pincode != DBNull.Value && valueClient_Pincode.ToString() != "")
                        objClient.Client_Pincode = Convert.ToInt32(dtClient.Rows[0]["Client_Pincode"].ToString());
                    else
                        objClient.Client_Pincode = 0;

                    object valuePAN_Num = dtClient.Rows[0]["PAN_Num"];
                    if (dtClient.Columns.Contains("PAN_Num") && valuePAN_Num != DBNull.Value && valuePAN_Num.ToString() != "")
                        objClient.PAN_Num = Convert.ToString(dtClient.Rows[0]["PAN_Num"].ToString());
                    else
                        objClient.PAN_Num = string.Empty;

                    object valueGSTIN_Num = dtClient.Rows[0]["GSTIN_Num"];
                    if (dtClient.Columns.Contains("GSTIN_Num") && valueGSTIN_Num != DBNull.Value && valueGSTIN_Num.ToString() != "")
                        objClient.GSTIN_Num = Convert.ToString(dtClient.Rows[0]["GSTIN_Num"].ToString());
                    else
                        objClient.GSTIN_Num = string.Empty;

                    object valueBasic_Fees = dtClient.Rows[0]["Basic_Fees"];
                    if (dtClient.Columns.Contains("Basic_Fees") && valueBasic_Fees != DBNull.Value && valueBasic_Fees.ToString() != "")
                        objClient.Basic_Fees = dtClient.Rows[0]["Basic_Fees"].ToString();
                    else
                        objClient.Basic_Fees = string.Empty;

                    object valueAdditional_Fees = dtClient.Rows[0]["Additional_Fees"];
                    if (dtClient.Columns.Contains("Additional_Fees") && valueAdditional_Fees != DBNull.Value && valueAdditional_Fees.ToString() != "")
                        objClient.Additional_Fees = dtClient.Rows[0]["Additional_Fees"].ToString();
                    else
                        objClient.Additional_Fees = string.Empty;

                    object valueOutofGeo_Fees = dtClient.Rows[0]["OutofGeo_Fees"];
                    if (dtClient.Columns.Contains("OutofGeo_Fees") && valueOutofGeo_Fees != DBNull.Value && valueOutofGeo_Fees.ToString() != "")
                        objClient.OutofGeo_Fees = dtClient.Rows[0]["OutofGeo_Fees"].ToString();
                    else
                        objClient.OutofGeo_Fees = string.Empty;

                    object valueTotal_Fees = dtClient.Rows[0]["Total_Fees"];
                    if (dtClient.Columns.Contains("Total_Fees") && valueTotal_Fees != DBNull.Value && valueTotal_Fees.ToString() != "")
                        objClient.Total_Fees = dtClient.Rows[0]["Total_Fees"].ToString();
                    else
                        objClient.Total_Fees = string.Empty;


                    //vMClientMaster.pincodeList = GetPincode();
                    vMClientMaster.ival_StateList = GetState();
                    vMClientMaster.ival_DistrictsList = GetDistricts();
                    vMClientMaster.ival_LocalityList = GetLocality();
                    vMClientMaster.ReportTypeList = GetReportType();
                    vMClientMaster.ValuationTypeList = GetValuationType();
                    vMClientMaster.ival_CitiesList = GetCity();

                    //vMClientMaster.SelectedBranchPincode = Convert.ToInt32(dtClient.Rows[0]["Client_Pincode"]);
                    

                    IEnumerable <ival_States> ival_StatesList = GetState();
                    if (ival_StatesList.ToList().Count > 0 && ival_StatesList != null)
                    {
                        var state = ival_StatesList.Where(s => s.State_Name == dtClient.Rows[0]["Client_State"].ToString()).Select(s => s.State_ID);
                        vMClientMaster.SelectedStateID = state.FirstOrDefault();
                    }
                    IEnumerable<ival_Districts> ival_DistrictsList = GetDistricts();
                    if (ival_DistrictsList.ToList().Count > 0 && ival_DistrictsList != null)
                    {
                        var Dist = ival_DistrictsList.Where(s => s.District_Name == dtClient.Rows[0]["Client_District"].ToString()).Select(s => s.District_ID);
                        vMClientMaster.SelectedDistID = Dist.FirstOrDefault();
                    }
                    IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                    if (ival_LocalityList.ToList().Count > 0 && ival_LocalityList != null)
                    {
                        var City = ival_LocalityList.Where(s => s.Loc_Name == dtClient.Rows[0]["Client_City"].ToString()).Select(s => s.Loc_Id);
                        vMClientMaster.SelectedLocID = City.FirstOrDefault();
                    }
                    IEnumerable<ival_Cities> ival_CitiesList = GetCity();
                    if (ival_CitiesList.ToList().Count > 0 && ival_CitiesList != null)
                    {
                        var Locality = ival_CitiesList.Where(s => s.City_Name == dtClient.Rows[0]["Client_Locality"].ToString()).Select(s => s.Loc_Id);
                        vMClientMaster.SelectedCityID = Locality.FirstOrDefault();
                    }

                    vMClientMaster.SelectedLocID = Convert.ToInt32(dtClient.Rows[0]["Client_City"]);
                    vMClientMaster.Client_Pincode = Convert.ToInt32(dtClient.Rows[0]["Client_City"]);

                    /* IEnumerable<ival_Locality> pincodeList = GetPincode();
                     if (pincodeList.ToList().Count > 0 && pincodeList != null)
                     {
                         var pincode = pincodeList.Where(s => s.Pin_Code == dtClient.Rows[0]["Client_Pincode"].ToInt32()).Select(s => s.Loc_Id);
                         vMClientMaster.SelectedBranchPincode = pincode.FirstOrDefault();
                     }*/

                    IEnumerable<ival_LookupCategoryValues> reportList = GetReportType();
                    if (reportList.ToList().Count > 0 && reportList != null)
                    {
                        var reportType = reportList.Where(s => s.Lookup_Value == dtClient.Rows[0]["Report_Type"].ToString()).Select(s => s.Lookup_ID);
                        vMClientMaster.SelectedReportTypeID = Convert.ToInt32(reportType.FirstOrDefault());
                    }
                    IEnumerable<ival_LookupCategoryValues> valuatonList = GetValuationType();
                    if (valuatonList.ToList().Count > 0 && valuatonList != null)
                    {
                        var valuationType = valuatonList.Where(s => s.Lookup_Value == dtClient.Rows[0]["Valuation_Type"].ToString()).Select(s => s.Lookup_ID);
                        vMClientMaster.SelectedValuationTypeID = Convert.ToInt32(valuationType.FirstOrDefault());
                    }

                    objClient.Client_ID = Convert.ToInt32(valuedtClient);

                    vMClientMaster.Client_ID = objClient.Client_ID;
                    vMClientMaster.ival_ClientMaster = objClient;

                }
            }
            catch (Exception ex)
            {
                //ex.InnerException.ToString();
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

            return View(vMClientMaster);
        }

        [HttpPost]
        public ActionResult UpdateClient(String str)
        {
            string resultStr = string.Empty;
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

            try
            {
                for (int i = 0; i < list1.Rows.Count; i++)
                {

                    IEnumerable<ival_States> ival_StatesList = GetState();
                    var state = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMClientMaster.SelectedStateID).Select(row => row.State_Name.ToString());

                    IEnumerable<ival_Districts> ival_DistrictsList = GetDistricts();
                    var Dist = ival_DistrictsList.AsEnumerable().Where(row => row.District_ID == vMClientMaster.SelectedDistID).Select(row => row.District_Name.ToString());

                    IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                    var City = ival_LocalityList.AsEnumerable().Where(row => row.Loc_Id == vMClientMaster.SelectedLocID).Select(row => row.Loc_Name.ToString());
                    var pincode = ival_LocalityList.AsEnumerable().Where(row => row.Loc_Id == vMClientMaster.SelectedBranchPincode).Select(row => row.Pin_Code.ToString());

                    IEnumerable<ival_LookupCategoryValues> reportTypeList = GetReportType();
                    var ReportType = reportTypeList.AsEnumerable().Where(row => row.Lookup_ID == vMClientMaster.SelectedReportTypeID).Select(row => row.Lookup_Value.ToString());

                    IEnumerable<ival_LookupCategoryValues> valuationList = GetValuationType();
                    var ValuationType = valuationList.AsEnumerable().Where(row => row.Lookup_ID == vMClientMaster.SelectedValuationTypeID).Select(row => row.Lookup_Value.ToString());


                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    //              
                    hInputPara = new Hashtable();
                    hInputPara.Add("@Client_ID", Convert.ToInt32(list1.Rows[i]["Client_Id"]));
                    hInputPara.Add("@Client_Name", Convert.ToString(list1.Rows[i]["Client_Name"]));
                    hInputPara.Add("@Branch_Name", Convert.ToString(list1.Rows[i]["Branch_Name"]));
                    hInputPara.Add("@Client_Address1", Convert.ToString(list1.Rows[i]["Client_Addr1"]));
                    hInputPara.Add("@Client_Address2", Convert.ToString(list1.Rows[i]["Client_Addr2"]));
                    hInputPara.Add("@Client_City", Convert.ToString(list1.Rows[i]["Client_Loc"]));
                    hInputPara.Add("@Client_Locality", Convert.ToString(list1.Rows[i]["Client_City"]));
                    hInputPara.Add("@Client_District", Convert.ToString(list1.Rows[i]["Client_Dist"]));
                    hInputPara.Add("@Client_State", Convert.ToString(list1.Rows[i]["Client_State"]));
                    hInputPara.Add("@Client_Pincode", Convert.ToString(list1.Rows[i]["Client_Pin"]));
                    hInputPara.Add("@Client_First_Name", Convert.ToString(list1.Rows[i]["Client_Fname"]));
                    hInputPara.Add("@Client_Contact_Num", Convert.ToString(list1.Rows[i]["Client_Phone"]));
                    hInputPara.Add("@Client_Designation", Convert.ToString(list1.Rows[i]["Client_Desn"]));
                    hInputPara.Add("@Client_Contact_Email", Convert.ToString(list1.Rows[i]["Client_Mail"]));
                    hInputPara.Add("@PAN_Num", Convert.ToString(list1.Rows[i]["Client_Pan"]));
                    hInputPara.Add("@GSTIN_Num", Convert.ToString(list1.Rows[i]["Client_Gstin"]));
                    hInputPara.Add("@Valuation_Type", Convert.ToString(list1.Rows[i]["val_Type"]));
                    hInputPara.Add("@Report_Type", Convert.ToString(list1.Rows[i]["Repo_Type"]));
                    hInputPara.Add("@Basic_Fees", Convert.ToString(list1.Rows[i]["Base_fee"]));
                    hInputPara.Add("@Additional_Fees", Convert.ToString(list1.Rows[i]["Add_fee"]));
                    hInputPara.Add("@OutofGeo_Fees", Convert.ToString(list1.Rows[i]["outofgeo"]));
                    hInputPara.Add("@Total_Fees", Convert.ToString(list1.Rows[i]["Total_fee"]));
                    hInputPara.Add("@Updated_By", string.Empty);

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateClientMasterByClientID", hInputPara);

                    if (Convert.ToInt32(result) == 1)
                    {
                        return RedirectToAction("Index", "Client");
                    }
                    else
                    {
                        return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                //e.InnerException.ToString();
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, Message = "SuccessFull" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewClient(int? ID)
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //return View();
            }
            vMClientMaster = new VMClientMaster();
            objClient = new ival_ClientMaster();
            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Client_ID", ID);
                DataTable dtClient = sqlDataAccess.GetDatatableExecuteStoreProcedure("[usp_GetClientMasterByID]", hInputPara);

                if (dtClient.Rows.Count > 0 && dtClient != null)
                {

                    object valuedtClient = Convert.ToInt32(dtClient.Rows[0]["Client_ID"]);
                    if (dtClient.Columns.Contains("Client_ID") && valuedtClient != null && valuedtClient.ToString() != "")
                        objClient.Client_ID = Convert.ToInt32(valuedtClient);
                    else
                        objClient.Client_ID = 0;

                    object valueClient_Name = dtClient.Rows[0]["Client_Name"];
                    if (dtClient.Columns.Contains("Client_Name") && valueClient_Name != DBNull.Value && valueClient_Name.ToString() != "")
                        objClient.Client_Name = dtClient.Rows[0]["Client_Name"].ToString();
                    else
                        objClient.Client_Name = string.Empty;

                    object valueBranch_Name = dtClient.Rows[0]["Branch_Name"];
                    if (dtClient.Columns.Contains("Branch_Name") && valueBranch_Name != DBNull.Value && valueBranch_Name.ToString() != "")
                        objClient.Branch_Name = dtClient.Rows[0]["Branch_Name"].ToString();
                    else
                        objClient.Branch_Name = string.Empty;

                    object valueClient_First_Name = dtClient.Rows[0]["Client_First_Name"];
                    if (dtClient.Columns.Contains("Client_First_Name") && valueClient_First_Name != DBNull.Value && valueClient_First_Name.ToString() != "")
                        objClient.Client_First_Name = dtClient.Rows[0]["Client_First_Name"].ToString();
                    else
                        objClient.Client_First_Name = string.Empty;

                    object valueClient_Contact_Num = dtClient.Rows[0]["Client_Contact_Num"];
                    if (dtClient.Columns.Contains("Client_Contact_Num") && valueClient_Contact_Num != DBNull.Value && valueClient_Contact_Num.ToString() != "")
                        objClient.Client_Contact_Num = dtClient.Rows[0]["Client_Contact_Num"].ToString();
                    else
                        objClient.Client_Contact_Num = string.Empty;

                    object valueClient_Contact_Email = dtClient.Rows[0]["Client_Contact_Email"];
                    if (dtClient.Columns.Contains("Client_Contact_Email") && valueClient_Contact_Email != DBNull.Value && valueClient_Contact_Email.ToString() != "")
                        objClient.Client_Contact_Email = dtClient.Rows[0]["Client_Contact_Email"].ToString();
                    else
                        objClient.Client_Contact_Email = string.Empty;

                    object valueClient_Designation = dtClient.Rows[0]["Client_Designation"];
                    if (dtClient.Columns.Contains("Client_Designation") && valueClient_Designation != DBNull.Value && valueClient_Designation.ToString() != "")
                        objClient.Client_Designation = dtClient.Rows[0]["Client_Designation"].ToString();
                    else
                        objClient.Client_Designation = string.Empty;

                    object valueClient_Address1 = dtClient.Rows[0]["Client_Address1"];
                    if (dtClient.Columns.Contains("Client_Address1") && valueClient_Address1 != DBNull.Value && valueClient_Address1.ToString() != "")
                        objClient.Client_Address1 = dtClient.Rows[0]["Client_Address1"].ToString();
                    else
                        objClient.Client_Address1 = string.Empty;

                    object valueClient_Address2 = dtClient.Rows[0]["Client_Address2"];
                    if (dtClient.Columns.Contains("Client_Address2") && valueClient_Address2 != DBNull.Value && valueClient_Address2.ToString() != "")
                        objClient.Client_Address2 = dtClient.Rows[0]["Client_Address2"].ToString();
                    else
                        objClient.Client_Address2 = string.Empty;

                    object valueClient_City = dtClient.Rows[0]["Client_City"];
                    if (dtClient.Columns.Contains("Client_City") && valueClient_City != DBNull.Value && valueClient_City.ToString() != "")
                        objClient.Client_City = dtClient.Rows[0]["Client_City"].ToString();
                    else
                        objClient.Client_City = string.Empty;

                    object valueClient_Locality = dtClient.Rows[0]["Client_Locality"];
                    if (dtClient.Columns.Contains("Client_Locality") && valueClient_Locality != DBNull.Value && valueClient_Locality.ToString() != "")
                        objClient.Client_Locality = dtClient.Rows[0]["Client_Locality"].ToString();
                    else
                        objClient.Client_Locality = string.Empty;

                    object valueClient_District = dtClient.Rows[0]["Client_District"];
                    if (dtClient.Columns.Contains("Client_District") && valueClient_District != DBNull.Value && valueClient_District.ToString() != "")
                        objClient.Client_District = dtClient.Rows[0]["Client_District"].ToString();
                    else
                        objClient.Client_District = string.Empty;

                    object valueClient_State = dtClient.Rows[0]["Client_State"];
                    if (dtClient.Columns.Contains("Client_State") && valueClient_State != DBNull.Value && valueClient_State.ToString() != "")
                        objClient.Client_State = dtClient.Rows[0]["Client_State"].ToString();
                    else
                        objClient.Client_State = string.Empty;

                    object valueClient_Pincode = dtClient.Rows[0]["Client_Pincode"];
                    if (dtClient.Columns.Contains("Client_Pincode") && valueClient_Pincode != DBNull.Value && valueClient_Pincode.ToString() != "")
                        objClient.Client_Pincode = Convert.ToInt32(dtClient.Rows[0]["Client_Pincode"].ToString());
                    else
                        objClient.Client_Pincode = 0;

                    object valuePAN_Num = dtClient.Rows[0]["PAN_Num"];
                    if (dtClient.Columns.Contains("PAN_Num") && valuePAN_Num != DBNull.Value && valuePAN_Num.ToString() != "")
                        objClient.PAN_Num = Convert.ToString(dtClient.Rows[0]["PAN_Num"].ToString());
                    else
                        objClient.PAN_Num = string.Empty;

                    object valueGSTIN_Num = dtClient.Rows[0]["GSTIN_Num"];
                    if (dtClient.Columns.Contains("GSTIN_Num") && valueGSTIN_Num != DBNull.Value && valueGSTIN_Num.ToString() != "")
                        objClient.GSTIN_Num = Convert.ToString(dtClient.Rows[0]["GSTIN_Num"].ToString());
                    else
                        objClient.GSTIN_Num = string.Empty;

                    object valueBasic_Fees = dtClient.Rows[0]["Basic_Fees"];
                    if (dtClient.Columns.Contains("Basic_Fees") && valueBasic_Fees != DBNull.Value && valueBasic_Fees.ToString() != "")
                        objClient.Basic_Fees = dtClient.Rows[0]["Basic_Fees"].ToString();
                    else
                        objClient.Basic_Fees = string.Empty;

                    object valueAdditional_Fees = dtClient.Rows[0]["Additional_Fees"];
                    if (dtClient.Columns.Contains("Additional_Fees") && valueAdditional_Fees != DBNull.Value && valueAdditional_Fees.ToString() != "")
                        objClient.Additional_Fees = dtClient.Rows[0]["Additional_Fees"].ToString();
                    else
                        objClient.Additional_Fees = string.Empty;

                    object valueOutofGeo_Fees = dtClient.Rows[0]["OutofGeo_Fees"];
                    if (dtClient.Columns.Contains("OutofGeo_Fees") && valueOutofGeo_Fees != DBNull.Value && valueOutofGeo_Fees.ToString() != "")
                        objClient.OutofGeo_Fees = dtClient.Rows[0]["OutofGeo_Fees"].ToString();
                    else
                        objClient.OutofGeo_Fees = string.Empty;

                    object valueTotal_Fees = dtClient.Rows[0]["Total_Fees"];
                    if (dtClient.Columns.Contains("Total_Fees") && valueTotal_Fees != DBNull.Value && valueTotal_Fees.ToString() != "")
                        objClient.Total_Fees = dtClient.Rows[0]["Total_Fees"].ToString();
                    else
                        objClient.Total_Fees = string.Empty;


                   // vMClientMaster.pincodeList = GetPincode();
                    vMClientMaster.ival_StateList = GetState();
                    vMClientMaster.ival_DistrictsList = GetDistricts();
                    vMClientMaster.ival_LocalityList = GetLocality();
                    vMClientMaster.ReportTypeList = GetReportType();
                    vMClientMaster.ValuationTypeList = GetValuationType();
                    vMClientMaster.ival_CitiesList = GetCity();
                    //vMClientMaster.SelectedBranchPincode = Convert.ToInt32(dtClient.Rows[0]["Client_Pincode"]);


                    IEnumerable<ival_States> ival_StatesList = GetState();
                    if (ival_StatesList.ToList().Count > 0 && ival_StatesList != null)
                    {
                        var state = ival_StatesList.Where(s => s.State_Name == dtClient.Rows[0]["Client_State"].ToString()).Select(s => s.State_ID);
                        vMClientMaster.SelectedStateID = state.FirstOrDefault();
                    }
                    IEnumerable<ival_Districts> ival_DistrictsList = GetDistricts();
                    if (ival_DistrictsList.ToList().Count > 0 && ival_DistrictsList != null)
                    {
                        var Dist = ival_DistrictsList.Where(s => s.District_Name == dtClient.Rows[0]["Client_District"].ToString()).Select(s => s.District_ID);
                        vMClientMaster.SelectedDistID = Dist.FirstOrDefault();
                    }
                    IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                    if (ival_LocalityList.ToList().Count > 0 && ival_LocalityList != null)
                    {
                        var City = ival_LocalityList.Where(s => s.Loc_Name == dtClient.Rows[0]["Client_City"].ToString()).Select(s => s.Loc_Id);
                        vMClientMaster.SelectedLocID = City.FirstOrDefault();
                    }
                    IEnumerable<ival_Cities> ival_CitiesList = GetCity();
                    if (ival_CitiesList.ToList().Count > 0 && ival_CitiesList != null)
                    {
                        var Locality = ival_CitiesList.Where(s => s.City_Name == dtClient.Rows[0]["Client_Locality"].ToString()).Select(s => s.City_ID);
                        vMClientMaster.SelectedCityID = Locality.FirstOrDefault();
                    }

                    vMClientMaster.Client_Pincode = Convert.ToInt32(dtClient.Rows[0]["Client_City"]);
                    /* IEnumerable<ival_Locality> pincodeList = GetPincode();
                     if (pincodeList.ToList().Count > 0 && pincodeList != null)
                     {
                         var pincode = pincodeList.Where(s => s.Pin_Code == dtClient.Rows[0]["Client_Pincode"].ToInt32()).Select(s => s.Loc_Id);
                         vMClientMaster.SelectedBranchPincode = pincode.FirstOrDefault();
                     }*/

                    IEnumerable<ival_LookupCategoryValues> reportList = GetReportType();
                    if (reportList.ToList().Count > 0 && reportList != null)
                    {
                        var reportType = reportList.Where(s => s.Lookup_Value == dtClient.Rows[0]["Report_Type"].ToString()).Select(s => s.Lookup_ID);
                        vMClientMaster.SelectedReportTypeID = Convert.ToInt32(reportType.FirstOrDefault());
                    }
                    IEnumerable<ival_LookupCategoryValues> valuatonList = GetValuationType();
                    if (valuatonList.ToList().Count > 0 && valuatonList != null)
                    {
                        var valuationType = valuatonList.Where(s => s.Lookup_Value == dtClient.Rows[0]["Valuation_Type"].ToString()).Select(s => s.Lookup_ID);
                        vMClientMaster.SelectedValuationTypeID = Convert.ToInt32(valuationType.FirstOrDefault());
                    }

                    vMClientMaster.ival_ClientMaster = objClient;

                }
            }
            catch (Exception ex)
            {
                //ex.InnerException.ToString();
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

            return View(vMClientMaster);
        }


        /*[HttpPost]
        public ActionResult EditClient(VMClientMaster vMClientMaster)
        {
           // bool isSuccess = true;
            if (vMClientMaster.ival_ClientMaster.Branch_Name != null)
            {
                try
                {

                    IEnumerable<ival_States> ival_StatesList = GetState();
                    var state = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMClientMaster.SelectedStateID).Select(row => row.State_Name.ToString());

                    IEnumerable<ival_Districts> ival_DistrictsList = GetDistricts();
                    var Dist = ival_DistrictsList.AsEnumerable().Where(row => row.District_ID == vMClientMaster.SelectedDistID).Select(row => row.District_Name.ToString());

                    IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                    var City = ival_LocalityList.AsEnumerable().Where(row => row.Loc_Id == vMClientMaster.SelectedLocID).Select(row => row.Loc_Name.ToString());

                    IEnumerable<ival_Locality> pincodeList = GetPincode();
                    var pincode = pincodeList.AsEnumerable().Where(row => row.Loc_Id == vMClientMaster.SelectedBranchPincode).Select(row => row.Pin_Code.ToString());

                    IEnumerable<ival_LookupCategoryValues> reportTypeList = GetReportType();
                    var ReportType = reportTypeList.AsEnumerable().Where(row => row.Lookup_ID == vMClientMaster.SelectedReportTypeID).Select(row => row.Lookup_Value.ToString());

                    IEnumerable<ival_LookupCategoryValues> valuationList = GetValuationType();
                    var ValuationType = valuationList.AsEnumerable().Where(row => row.Lookup_ID == vMClientMaster.SelectedValuationTypeID).Select(row => row.Lookup_Value.ToString());


                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();

                    hInputPara = new Hashtable();
                    hInputPara.Add("@Client_ID", Convert.ToInt32(vMClientMaster.ival_ClientMaster.Client_ID));
                    hInputPara.Add("@Client_Name", vMClientMaster.ival_ClientMaster.Client_Name.ToString());
                    hInputPara.Add("@Branch_Name", vMClientMaster.ival_ClientMaster.Branch_Name.ToString());
                    hInputPara.Add("@Client_Address1", vMClientMaster.ival_ClientMaster.Client_Address1.ToString());
                    hInputPara.Add("@Client_Address2", vMClientMaster.ival_ClientMaster.Client_Address2.ToString());
                    hInputPara.Add("@Client_City", vMClientMaster.ival_ClientMaster.Client_City.ToString());
                    hInputPara.Add("@Client_District", vMClientMaster.ival_ClientMaster.Client_District.ToString());
                    hInputPara.Add("@Client_State", state.FirstOrDefault());
                    hInputPara.Add("@Client_Pincode", Convert.ToInt32(pincode.FirstOrDefault()));
                    hInputPara.Add("@Client_First_Name", vMClientMaster.ival_ClientMaster.Client_First_Name.ToString());
                    hInputPara.Add("@Client_Contact_Num", vMClientMaster.ival_ClientMaster.Client_Contact_Num.ToString());
                    hInputPara.Add("@Client_Designation", vMClientMaster.ival_ClientMaster.Client_Designation.ToString());
                    hInputPara.Add("@Client_Contact_Email", vMClientMaster.ival_ClientMaster.Client_Contact_Email.ToString());
                    hInputPara.Add("@PAN_Num", vMClientMaster.ival_ClientMaster.PAN_Num.ToString());
                    hInputPara.Add("@GSTIN_Num", vMClientMaster.ival_ClientMaster.GSTIN_Num.ToString());
                    hInputPara.Add("@Valuation_Type", ReportType.FirstOrDefault());
                    hInputPara.Add("@Report_Type", ValuationType.FirstOrDefault());
                    hInputPara.Add("@Basic_Fees", vMClientMaster.ival_ClientMaster.Basic_Fees.ToString());
                    hInputPara.Add("@Additional_Fees", vMClientMaster.ival_ClientMaster.Additional_Fees.ToString());
                    hInputPara.Add("@OutofGeo_Fees", vMClientMaster.ival_ClientMaster.OutofGeo_Fees.ToString());
                    hInputPara.Add("@Total_Fees", vMClientMaster.ival_ClientMaster.Total_Fees.ToString());
                    hInputPara.Add("@Updated_By", string.Empty);

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateClientMasterByClientID", hInputPara);

                    if (Convert.ToInt32(result) == 1)
                    {
                        return RedirectToAction("Index", "Client");
                    }
                    else
                    {
                        
                        return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception e)
                {
                    //e.InnerException.ToString();
                    string error = e.Message.ToString();
                    return Json(error, JsonRequestBehavior.AllowGet);
                }
            }
            return Json("Error !", JsonRequestBehavior.AllowGet);
        }*/

        public ActionResult Delete(int? ID)
        {
            try
            {
                sqlDataAccess = new SQLDataAccess();
                hInputPara = new Hashtable();
                hInputPara.Add("@Client_ID", ID);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteClientMasterID", hInputPara);
                result.ToString();

                return RedirectToAction("Index", "Client");
            }
            catch (Exception ex)
            {

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public IEnumerable<ival_States> GetState()
        {
            List<ival_States> listState = new List<ival_States>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetStateByID");
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                listState.Add(new ival_States
                {
                    State_ID = Convert.ToInt32(dtState.Rows[i]["State_ID"]),
                    State_Name = Convert.ToString(dtState.Rows[i]["State_Name"])

                });


            }
            return listState.AsEnumerable();
        }

        [HttpPost]
        public IEnumerable<ival_Districts> GetDistricts()
        {
            List<ival_Districts> lists = new List<ival_Districts>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            try
            {
                DataTable dtDistict = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDistrictByID");
                for (int i = 0; i < dtDistict.Rows.Count; i++)
                {
                    lists.Add(new ival_Districts
                    {
                        District_ID = Convert.ToInt32(dtDistict.Rows[i]["District_ID"]),
                        District_Name = Convert.ToString(dtDistict.Rows[i]["District_Name"])
                    });
                }
            }
            catch (Exception)
            {

            }
            return lists.AsEnumerable();
        }

        [HttpPost]
        public IEnumerable<ival_Cities> GetCity()
        {
            List<ival_Cities> listState = new List<ival_Cities>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtCity = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCityByID");
            for (int i = 0; i < dtCity.Rows.Count; i++)
            {

                listState.Add(new ival_Cities
                {
                    Loc_Id = Convert.ToInt32(dtCity.Rows[i]["City_ID"]),
                    City_Name = Convert.ToString(dtCity.Rows[i]["City"])

                });


            }
            return listState.AsEnumerable();
        }

        public IEnumerable<ival_Locality> GetLocality()
        {
            List<ival_Locality> listLoc = new List<ival_Locality>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            DataTable dtLocality = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalityByID");
            for (int i = 0; i < dtLocality.Rows.Count; i++)
            {

                listLoc.Add(new ival_Locality
                {
                    Loc_Id = Convert.ToInt32(dtLocality.Rows[i]["Loc_Id"]),
                    Loc_Name = Convert.ToString(dtLocality.Rows[i]["Loc_Name"]),
                    Pin_Code = Convert.ToInt32(dtLocality.Rows[i]["Pin_Code"])

                });
            }
            return listLoc.AsEnumerable();
        }

        /*[HttpPost]
        public IEnumerable<ival_Locality> GetPincode()
        {
            List<ival_Locality> listLocPin = new List<ival_Locality>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            DataTable dtLocPin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalityByID");
            for (int i = 0; i < dtLocPin.Rows.Count; i++)
            {

                listLocPin.Add(new ival_Locality
                {
                    Loc_Id = Convert.ToInt32(dtLocPin.Rows[i]["Loc_Id"]),
                    //Loc_Name = Convert.ToString(listLocPin.Rows[i]["Loc_Name"]),
                    Pin_Code = Convert.ToInt32(dtLocPin.Rows[i]["Pin_Code"])

                });
            }
            return listLocPin.AsEnumerable();
        }*/

        [HttpPost]
        public JsonResult GetPinByOnselect(int str)
        {
            hInputPara = new Hashtable();
            List<ival_Locality> PinList = new List<ival_Locality>();
            // DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Loc_Id", str);

            DataTable dtLocPin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPinByLocName", hInputPara);
            for (int i = 0; i < dtLocPin.Rows.Count; i++)
            {
                PinList.Add(new ival_Locality
                {
                    Loc_Id = Convert.ToInt32(dtLocPin.Rows[i]["Loc_Id"]),
                    Pin_Code = Convert.ToInt32(dtLocPin.Rows[i]["Pin_Code"])

                });
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectpinMaster = jsonSerialiser.Serialize(PinList);
            return Json(jsonProjectpinMaster);
        }

        /*[HttpPost]
        public IEnumerable<ival_LookupCategoryValues> GetPincode()
        {
            List<ival_LookupCategoryValues> listPincode = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtPincode = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPincode");
            for (int i = 0; i < dtPincode.Rows.Count; i++)
            {

                listPincode.Add(new ival_LookupCategoryValues
                {
                    Lookup_Value = Convert.ToString(dtPincode.Rows[i]["Lookup_Value"]),
                    Lookup_ID = Convert.ToInt32(dtPincode.Rows[i]["Lookup_ID"])

                });


            }
            return listPincode.AsEnumerable();
        }*/

        [HttpPost]
        public IEnumerable<ival_LookupCategoryValues> GetReportType()
        {
            List<ival_LookupCategoryValues> listReportType = new List<ival_LookupCategoryValues>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtReport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetReportType");
            for (int i = 0; i < dtReport.Rows.Count; i++)
            {

                listReportType.Add(new ival_LookupCategoryValues
                {
                    Lookup_Value = Convert.ToString(dtReport.Rows[i]["Lookup_Value"]),
                    Lookup_ID = Convert.ToInt32(dtReport.Rows[i]["Lookup_ID"])

                });


            }
            return listReportType.AsEnumerable();
        }
        [HttpPost]
        public IEnumerable<ival_LookupCategoryValues> GetValuationType()
        {
            List<ival_LookupCategoryValues> listValuationType = new List<ival_LookupCategoryValues>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtValue = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetValuationType");
            for (int i = 0; i < dtValue.Rows.Count; i++)
            {

                listValuationType.Add(new ival_LookupCategoryValues
                {
                    Lookup_Value = Convert.ToString(dtValue.Rows[i]["Lookup_Value"]),
                    Lookup_ID = Convert.ToInt32(dtValue.Rows[i]["Lookup_ID"])

                });

            }
            return listValuationType.AsEnumerable();
        }
    }
}