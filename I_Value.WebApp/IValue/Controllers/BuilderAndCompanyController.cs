﻿using I_Value.WebApp.Models;
using IvalueDataAccess;
using IValuePublishProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;

namespace I_Value.WebApp.Controllers
{
    public class BuilderAndCompanyController : Controller
    {
        VMBuilderGroupMaster vMBuilderGroupMaster;
        ival_BuilderGroupMaster ival_BuilderGroupMaster;
        ival_BuilderCompanyMaster ival_BuilderCompanyMaster;
        ival_BuilderCompanyMasterNew ival_BuilderCompanyMasterNew;
        SQLDataAccess sqlDataAccess;
        Hashtable hInputPara;
        // GET: BuilderAndCompany
        public ActionResult Index()
        {
           
            try
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                DataTable dtBuilderGroup = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuilderList");

                List<VMBuilderGroupMaster> vMBuilderGroupMasterList = new List<VMBuilderGroupMaster>();

                if (dtBuilderGroup.Rows.Count > 0 && dtBuilderGroup!=null)

                {
                    for (int i = 0; i < dtBuilderGroup.Rows.Count; i++)
                    {

                        vMBuilderGroupMasterList.Add(new VMBuilderGroupMaster
                        {
                            Builder_Group_Master_IDl = Convert.ToInt32(dtBuilderGroup.Rows[i]["Builder_Group_Master_ID"]),
                           // Builder_Company_Master_IDl = Convert.ToInt32(dtBuilderGroup.Rows[i]["Builder_Company_Master_ID"]),
                            BuilderGroupNamel = Convert.ToString(dtBuilderGroup.Rows[i]["BuilderGroupName"]),
                            Builder_Cityl = Convert.ToString(dtBuilderGroup.Rows[i]["Builder_City"]),
                            Builder_Statel = Convert.ToString(dtBuilderGroup.Rows[i]["Builder_State"])
                        });


                    }
                }
                return View(vMBuilderGroupMasterList);

            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("Login", "Account");
            }
            
        }
        public ActionResult CompanyIndex()
        {

            try
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                DataTable dtBuilderGroup = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCompanyBuilderListDetails");

                List<VMBuilderGroupMaster> vMBuilderGroupMasterList = new List<VMBuilderGroupMaster>();

                if (dtBuilderGroup.Rows.Count > 0 && dtBuilderGroup != null)

                {
                    for (int i = 0; i < dtBuilderGroup.Rows.Count; i++)
                    {

                        vMBuilderGroupMasterList.Add(new VMBuilderGroupMaster
                        {
                            Builder_Company_Master_IDl = Convert.ToInt32(dtBuilderGroup.Rows[i]["Builder_Company_Master_ID"]),
                             Builder_Company_Name = Convert.ToString(dtBuilderGroup.Rows[i]["Builder_Company_Name"]),
                            BuilderGroupNamel = Convert.ToString(dtBuilderGroup.Rows[i]["BuilderGroupName"]),
                            Builder_Cityl = Convert.ToString(dtBuilderGroup.Rows[i]["Builder_C_City"]),
                            Builder_Statel = Convert.ToString(dtBuilderGroup.Rows[i]["Builder_C_State"])
                        });


                    }
                }
                return View(vMBuilderGroupMasterList);

            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("Login", "Account");
            }
            return View();
        }
        public ActionResult AddBuilder()
        {
            vMBuilderGroupMaster = new VMBuilderGroupMaster();
            vMBuilderGroupMaster.ival_StatesList = GetState();
            vMBuilderGroupMaster.ival_LocalityList = GetLocality();
            vMBuilderGroupMaster.ival_CitiesList = GetCity();
            vMBuilderGroupMaster.ival_DistrictsList = GetDistrict();
            vMBuilderGroupMaster.BuilderGroupList = GetBuilderList();
            return View(vMBuilderGroupMaster);
        }
        [HttpPost]
        public ActionResult AddBuilder(string str)
        {

            try
            {

                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

                for (int i=0;i<list1.Rows.Count;i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();


                    hInputPara.Add("@BuilderGroupName", Convert.ToString(list1.Rows[i]["BuilderGroupName"]));
                    hInputPara.Add("@Builder_Address1", Convert.ToString(list1.Rows[i]["officeAdd1"]));
                    hInputPara.Add("@Builder_Address2", Convert.ToString(list1.Rows[i]["officeAdd2"]));
                    hInputPara.Add("@Builder_Locality", Convert.ToString(list1.Rows[i]["ddlLocality"]));
                    hInputPara.Add("@Nearby_Landmark", Convert.ToString(list1.Rows[i]["nearByLandmark"]));
                    hInputPara.Add("@Builder_District", Convert.ToString(list1.Rows[i]["ddlDistrict"]));
                    hInputPara.Add("@Builder_City", Convert.ToString(list1.Rows[i]["ddlCity"]));
                    hInputPara.Add("@Builder_State", Convert.ToString(list1.Rows[i]["ddlstate"]));
                    hInputPara.Add("@Builder_Pincode", Convert.ToString(list1.Rows[i]["ddlPincode"]));
                    
                    hInputPara.Add("@Director_Name1", Convert.ToString(list1.Rows[i]["DirectorName1"]));
                    hInputPara.Add("@Director_Phone1", Convert.ToString(list1.Rows[i]["DirectorPhone1"]));
                    hInputPara.Add("@Director_Mobile1", Convert.ToString(list1.Rows[i]["DirectorMobile1"]));
                    hInputPara.Add("@Director_Email1", Convert.ToString(list1.Rows[i]["DirectorEmail1"]));
                    hInputPara.Add("@Director_Name2", Convert.ToString(list1.Rows[i]["DirectorName2"]));
                    hInputPara.Add("@Director_Phone2", Convert.ToString(list1.Rows[i]["DirectorPhone2"]));
                    hInputPara.Add("@Director_Mobile2", Convert.ToString(list1.Rows[i]["DirectorMobile2"]));
                    hInputPara.Add("@Director_Email2", Convert.ToString(list1.Rows[i]["DirectorEmail2"]));
                    hInputPara.Add("@Director_Name3", Convert.ToString(list1.Rows[i]["DirectorName3"]));
                   
                    hInputPara.Add("@Director_Phone3", Convert.ToString(list1.Rows[i]["DirectorPhone3"]));
                    hInputPara.Add("@Director_Mobile3", Convert.ToString(list1.Rows[i]["DirectorMobile3"]));
                   
                   
                    hInputPara.Add("@Director_Email3", Convert.ToString(list1.Rows[i]["DirectorEmail3"]));
                    hInputPara.Add("@Builder_Street", Convert.ToString(list1.Rows[i]["street"]));
                    hInputPara.Add("@Builder_Grade", Convert.ToString(list1.Rows[i]["BuilderGrade"]));

                    string result=sqlDataAccess.ExecuteStoreProcedure("usp_InsertBuilderGroup", hInputPara);
                    return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception e)
            {
                e.InnerException.ToString();
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
            return Json("");
        }
        //[HttpPost]
        //public ActionResult AddBuilder(VMBuilderGroupMaster vMBuilderGroupMaster)
        //{

        //    try
        //    {

        //        //IEnumerable<ival_States> ival_StatesList = GetState();
        //        //var builderState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMBuilderGroupMaster.SelectedBuilderStateId).Select(row => row.State_Name.ToString());

        //        //IEnumerable<ival_LookupCategoryValues> pincodeList = GetPincode();
        //        //var builderPincode = pincodeList.AsEnumerable().Where(row => row.Lookup_ID == vMBuilderGroupMaster.SelectedBuilderPincodeID).Select(row => row.Lookup_Value.ToString());


        //        //hInputPara = new Hashtable();
        //        //sqlDataAccess = new SQLDataAccess();

        //        //hInputPara = new Hashtable();

        //        //hInputPara.Add("@BuilderGroupName", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.BuilderGroupName));
        //        //hInputPara.Add("@Builder_Address1", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Builder_Address1));
        //        //hInputPara.Add("@Builder_Address2", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Builder_Address2));
        //        //hInputPara.Add("@Builder_Locality", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Builder_Locality));
        //        //hInputPara.Add("@Nearby_Landmark", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Nearby_Landmark));
        //        //hInputPara.Add("@Builder_District", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Builder_District));
        //        //hInputPara.Add("@Builder_City", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Builder_City));
        //        //hInputPara.Add("@Builder_State", builderState.FirstOrDefault());
        //        //hInputPara.Add("@Builder_Pincode ", builderPincode.FirstOrDefault());
        //        //hInputPara.Add("@Director_Name1 ", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Name1));
        //        //hInputPara.Add("@Director_Phone1 ", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Phone1));
        //        //hInputPara.Add("@Director_Mobile1 ", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Mobile1));
        //        //hInputPara.Add("@Director_Email1", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Email1));
        //        //hInputPara.Add("@Director_Name2", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Name2));
        //        //hInputPara.Add("@Director_Phone2 ", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Phone2));
        //        //hInputPara.Add("@Director_Mobile2", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Mobile2));
        //        //hInputPara.Add("@Director_Email2 ", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Email2));
        //        //hInputPara.Add("@Director_Name3", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Name3));
        //        //hInputPara.Add("@Director_Phone3", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Mobile3));
        //        //hInputPara.Add("@Director_Mobile3", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Email3));
        //        //hInputPara.Add("@Director_Email3", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Email3));
        //        //hInputPara.Add("@Builder_Grade", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Builder_Grade));
        //        //hInputPara.Add("@Builder_Street", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Builder_Street));
        //        //hInputPara.Add("@Created_By", string.Empty);
        //        //var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertBuilderGroup", hInputPara);

        //        //if (Convert.ToInt32(result) == 1)
        //        //{
        //        //    return RedirectToAction("Index", "BuilderAndCompany");
        //        //}
        //        //else
        //        //{
        //        //   return Json( "Error in Database Record not Save!", JsonRequestBehavior.AllowGet );
        //        //}
        //    }
        //    catch (Exception e)
        //    {
        //        //e.InnerException.ToString();
        //        return Json( e.Message, JsonRequestBehavior.AllowGet );
        //    }
        //    return Json("");
        //}

        //[HttpGet]
        //public ActionResult EditBuilder(int? ID)
        //{
        //    if (string.IsNullOrEmpty(ID.ToString()))
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    //    return view();
        //    }
        //    vMBuilderGroupMaster = new VMBuilderGroupMaster();
        //    ival_BuilderGroupMaster = new ival_BuilderGroupMaster();
        //    ival_BuilderCompanyMaster = new ival_BuilderCompanyMaster();
            
        //    sqlDataAccess = new SQLDataAccess();
        //    hInputPara = new Hashtable();
        //    try
        //    {
        //        hInputPara.Add("@Builder_Group_Master_ID", ID);
        //        DataTable dtBuilderGroup = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuilderGroupIDDetailsByID", hInputPara);

        //        if (dtBuilderGroup.Rows.Count > 0 && dtBuilderGroup!=null)
        //        {
        //                object valueBuilder_Group_Master_ID = Convert.ToInt32(dtBuilderGroup.Rows[0]["Builder_Group_Master_ID"]);
        //                if (dtBuilderGroup.Columns.Contains("Builder_Group_Master_ID") && valueBuilder_Group_Master_ID != null && valueBuilder_Group_Master_ID.ToString()!="")
        //                    ival_BuilderGroupMaster.Builder_Group_Master_ID = Convert.ToInt32(valueBuilder_Group_Master_ID);
        //                else
        //                    ival_BuilderGroupMaster.Builder_Group_Master_ID = 0;

        //                object valueBuilderGroupName = dtBuilderGroup.Rows[0]["BuilderGroupName"];
        //                if (dtBuilderGroup.Columns.Contains("BuilderGroupName") && valueBuilderGroupName != null && valueBuilderGroupName.ToString() != "")
        //                ival_BuilderGroupMaster.BuilderGroupName = Convert.ToString(dtBuilderGroup.Rows[0]["BuilderGroupName"]);
        //                else
        //                    ival_BuilderGroupMaster.BuilderGroupName = string.Empty;

        //                object valueBuilder_Address1 = dtBuilderGroup.Rows[0]["Builder_Address1"];
        //                if (dtBuilderGroup.Columns.Contains("Builder_Address1") && valueBuilder_Address1 != null && valueBuilder_Address1.ToString() != "")
        //                ival_BuilderGroupMaster.Builder_Address1 = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_Address1"]);
        //                else
        //                    ival_BuilderGroupMaster.Builder_Address1 = string.Empty;

        //                object valueBuilder_Address2 = dtBuilderGroup.Rows[0]["Builder_Address2"];
        //                if(dtBuilderGroup.Columns.Contains("Builder_Address2") && valueBuilder_Address2!= null && valueBuilder_Address2.ToString() != "")
        //                ival_BuilderGroupMaster.Builder_Address2 = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_Address2"]);
        //                   else
        //                     ival_BuilderGroupMaster.Builder_Address2 = string.Empty;

        //                object valueBuilder_Locality = dtBuilderGroup.Rows[0]["Builder_Locality"];
        //                if (dtBuilderGroup.Columns.Contains("Builder_Locality") && valueBuilder_Locality != null && valueBuilder_Locality.ToString() != "")
        //                ival_BuilderGroupMaster.Builder_Locality = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_Locality"]);
        //                else
        //                    ival_BuilderGroupMaster.Builder_Locality = string.Empty;

        //                object valueBuilder_State = dtBuilderGroup.Rows[0]["Builder_State"];
        //                if (dtBuilderGroup.Columns.Contains("Builder_State") && valueBuilder_State != null && valueBuilder_State.ToString() != "")
        //                ival_BuilderGroupMaster.Builder_State = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_State"]);
        //                else
        //                    ival_BuilderGroupMaster.Builder_State = string.Empty;

        //            object valueBuilder_District = dtBuilderGroup.Rows[0]["Builder_District"];
        //            if (dtBuilderGroup.Columns.Contains("Builder_District") && valueBuilder_District != null && valueBuilder_District.ToString() != "")
        //                ival_BuilderGroupMaster.Builder_District = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_District"]);
        //            else
        //                ival_BuilderGroupMaster.Builder_District = string.Empty;

        //            object valueBuilder_City = dtBuilderGroup.Rows[0]["Builder_City"];
        //            if (dtBuilderGroup.Columns.Contains("Builder_City") && valueBuilder_City != null && valueBuilder_City.ToString() != "")
        //                ival_BuilderGroupMaster.Builder_City = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_City"]);
        //            else
        //                ival_BuilderGroupMaster.Builder_City = string.Empty;

        //            object valueBuilder_Pincode = dtBuilderGroup.Rows[0]["Builder_Pincode"];
        //            if (dtBuilderGroup.Columns.Contains("Builder_Pincode") && valueBuilder_Pincode != null && valueBuilder_Pincode.ToString() != "")
        //                ival_BuilderGroupMaster.Builder_Pincode = Convert.ToInt32(dtBuilderGroup.Rows[0]["Builder_Pincode"]);
        //            else
        //                ival_BuilderGroupMaster.Builder_Pincode = 0;

        //            object valueNearby_Landmark = dtBuilderGroup.Rows[0]["Nearby_Landmark"];
        //            if (dtBuilderGroup.Columns.Contains("Nearby_Landmark") && valueNearby_Landmark != null && valueNearby_Landmark.ToString() != "")
        //                ival_BuilderGroupMaster.Nearby_Landmark = Convert.ToString(dtBuilderGroup.Rows[0]["Nearby_Landmark"]);
        //            else
        //                ival_BuilderGroupMaster.Nearby_Landmark = string.Empty;

        //            object valueDirector_Name1 =dtBuilderGroup.Rows[0]["Director_Name1"];
        //            if (dtBuilderGroup.Columns.Contains("Director_Name1") && valueDirector_Name1 != null && valueDirector_Name1.ToString() != "")
        //                ival_BuilderGroupMaster.Director_Name1 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Name1"]);
        //            else
        //                ival_BuilderGroupMaster.Director_Name1 = string.Empty;

        //            object valueDirector_Name2 = dtBuilderGroup.Rows[0]["Director_Name2"];
        //            if (dtBuilderGroup.Columns.Contains("Director_Name2") && valueDirector_Name2 != null && valueDirector_Name2.ToString() != "")
        //                ival_BuilderGroupMaster.Director_Name2 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Name2"]);
        //            else
        //                ival_BuilderGroupMaster.Director_Name2 = string.Empty;

        //            object valueDirector_Name3 = dtBuilderGroup.Rows[0]["Director_Name3"];
        //            if (dtBuilderGroup.Columns.Contains("Director_Name3") && valueDirector_Name3 != null && valueDirector_Name3.ToString() != "")
        //                ival_BuilderGroupMaster.Director_Name3 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Name3"]);
        //            else
        //                ival_BuilderGroupMaster.Director_Name3 = string.Empty;

        //            object valueDirector_Phone1 = dtBuilderGroup.Rows[0]["Director_Phone1"];
        //            if (dtBuilderGroup.Columns.Contains("Director_Phone1") && valueDirector_Phone1 != null && valueDirector_Phone1.ToString() != "")
        //                ival_BuilderGroupMaster.Director_Phone1 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Phone1"]);
        //            else
        //                ival_BuilderGroupMaster.Director_Phone1 = string.Empty;

        //            object valueDirector_Phone2 = dtBuilderGroup.Rows[0]["Director_Phone2"];
        //            if (dtBuilderGroup.Columns.Contains("Director_Phone2") && valueDirector_Phone2 != null && valueDirector_Phone2.ToString() != "")
        //                ival_BuilderGroupMaster.Director_Phone2 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Phone2"]);
        //            else
        //                ival_BuilderGroupMaster.Director_Phone2 = string.Empty;

        //            object valueDirector_Phone3 = dtBuilderGroup.Rows[0]["Director_Phone3"];
        //            if (dtBuilderGroup.Columns.Contains("Director_Phone3") && valueDirector_Phone3 != null && valueDirector_Phone3.ToString() != "")
        //                ival_BuilderGroupMaster.Director_Phone3 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Phone3"]);
        //            else
        //                ival_BuilderGroupMaster.Director_Phone3 = string.Empty;

        //            object valueDirector_Mobile1 = dtBuilderGroup.Rows[0]["Director_Mobile1"];
        //            if (dtBuilderGroup.Columns.Contains("Director_Mobile1") && valueDirector_Mobile1 != null && valueDirector_Mobile1.ToString() != "")
        //                ival_BuilderGroupMaster.Director_Mobile1 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Mobile1"]);
        //            else
        //                ival_BuilderGroupMaster.Director_Mobile1 = string.Empty;

        //            object valueDirector_Mobile2 = dtBuilderGroup.Rows[0]["Director_Mobile2"];
        //            if (dtBuilderGroup.Columns.Contains("Director_Mobile2") && valueDirector_Mobile2 != null && valueDirector_Mobile2.ToString() != "")
        //                ival_BuilderGroupMaster.Director_Mobile2 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Mobile2"]);
        //            else
        //                ival_BuilderGroupMaster.Director_Mobile2 = string.Empty;

        //            object valueDirector_Mobile3 = dtBuilderGroup.Rows[0]["Director_Mobile3"];
        //            if (dtBuilderGroup.Columns.Contains("Director_Mobile3") && valueDirector_Mobile3 != null && valueDirector_Mobile3.ToString() != "")
        //                ival_BuilderGroupMaster.Director_Mobile3 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Mobile3"]);
        //            else
        //                ival_BuilderGroupMaster.Director_Mobile3 = string.Empty;

        //            object valueDirector_Email1 = dtBuilderGroup.Rows[0]["Director_Email1"];
        //            if (dtBuilderGroup.Columns.Contains("Director_Email1") && valueDirector_Email1 != null && valueDirector_Email1.ToString() != "")
        //                ival_BuilderGroupMaster.Director_Email1 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Email1"]);
        //            else
        //                ival_BuilderGroupMaster.Director_Email1 = string.Empty;

        //            object valueDirector_Email2 =dtBuilderGroup.Rows[0]["Director_Email2"];
        //            if (dtBuilderGroup.Columns.Contains("Director_Email2") && valueDirector_Email2 != null && valueDirector_Email2.ToString() != "")
        //                ival_BuilderGroupMaster.Director_Email2 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Email2"]);
        //            else
        //                ival_BuilderGroupMaster.Director_Email2 = string.Empty;

        //            object valueDirector_Email3 = dtBuilderGroup.Rows[0]["Director_Email3"];
        //            if (dtBuilderGroup.Columns.Contains("Director_Email3") && valueDirector_Email3 != null && valueDirector_Email3.ToString() != "")
        //                ival_BuilderGroupMaster.Director_Email3 = Convert.ToString(dtBuilderGroup.Rows[0]["BuilderGroupName"]);
        //            else
        //                ival_BuilderGroupMaster.Director_Email3 = string.Empty;

        //            object valueBuilder_Grade = dtBuilderGroup.Rows[0]["Builder_Grade"];
        //            if (dtBuilderGroup.Columns.Contains("Builder_Grade") && valueBuilder_Grade != null && valueBuilder_Grade.ToString() != "")
        //                ival_BuilderGroupMaster.Builder_Grade = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_Grade"]);
        //            else
        //                ival_BuilderGroupMaster.Builder_Grade = string.Empty;

        //            vMBuilderGroupMaster.ival_BuilderGroupMaster = ival_BuilderGroupMaster;

        //            object valueStreet = dtBuilderGroup.Rows[0]["Builder_Street"];
        //            if (dtBuilderGroup.Columns.Contains("Builder_Street") && valueStreet != null && valueStreet.ToString() != "")
        //                ival_BuilderGroupMaster.Builder_Street = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_Street"]);
        //            else
        //                ival_BuilderGroupMaster.Builder_Street = string.Empty;

        //            vMBuilderGroupMaster.ival_BuilderGroupMaster = ival_BuilderGroupMaster;


        //            vMBuilderGroupMaster.ival_StatesList = GetState();
        //            vMBuilderGroupMaster.PincodeList = GetPincode();
        //            vMBuilderGroupMaster.BuilderGroupList = GetBuilderList();

        //            IEnumerable<ival_States> ival_StatesList = GetState();
        //            if (ival_StatesList.ToList().Count > 0 && ival_StatesList != null)
        //            {
        //                var BuilderState = ival_StatesList.Where(s => s.State_Name == dtBuilderGroup.Rows[0]["Builder_State"].ToString()).Select(s => s.State_ID);
                      
        //                vMBuilderGroupMaster.SelectedBuilderStateId = Convert.ToInt32(BuilderState.FirstOrDefault());
                       
        //            }

        //            IEnumerable<ival_LookupCategoryValues> pincodelist = GetPincode();
        //            if (pincodelist.ToList().Count > 0 && pincodelist != null)
        //            {
        //                var builderpostalpincode = pincodelist.Where(s => s.Lookup_Value == dtBuilderGroup.Rows[0]["Builder_Pincode"].ToString()).Select(s => s.Lookup_ID);

        //                vMBuilderGroupMaster.SelectedBuilderPincodeID = Convert.ToInt32(builderpostalpincode.FirstOrDefault());

        //            }

        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        //    ex.InnerException.ToString();
        //        Json( ex.Message , JsonRequestBehavior.AllowGet);
        //    }
        //    return View(vMBuilderGroupMaster);
        //}
         [HttpGet]
        public ActionResult EditBuilder(int? ID)
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //    return view();
            }
            vMBuilderGroupMaster = new VMBuilderGroupMaster();
            ival_BuilderGroupMaster = new ival_BuilderGroupMaster();
            ival_BuilderCompanyMaster = new ival_BuilderCompanyMaster();
            
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Builder_Group_Master_ID", ID);
                DataTable dtBuilderGroup = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuilderGroupIDDetailsByID", hInputPara);

                if (dtBuilderGroup.Rows.Count > 0 && dtBuilderGroup!=null)
                {
                        object valueBuilder_Group_Master_ID = Convert.ToInt32(dtBuilderGroup.Rows[0]["Builder_Group_Master_ID"]);
                        if (dtBuilderGroup.Columns.Contains("Builder_Group_Master_ID") && valueBuilder_Group_Master_ID != null && valueBuilder_Group_Master_ID.ToString()!="")
                            ival_BuilderGroupMaster.Builder_Group_Master_ID = Convert.ToInt32(valueBuilder_Group_Master_ID);
                        else
                            ival_BuilderGroupMaster.Builder_Group_Master_ID = 0;

                        object valueBuilderGroupName = dtBuilderGroup.Rows[0]["BuilderGroupName"];
                        if (dtBuilderGroup.Columns.Contains("BuilderGroupName") && valueBuilderGroupName != null && valueBuilderGroupName.ToString() != "")
                        ival_BuilderGroupMaster.BuilderGroupName = Convert.ToString(dtBuilderGroup.Rows[0]["BuilderGroupName"]);
                        else
                            ival_BuilderGroupMaster.BuilderGroupName = string.Empty;

                        object valueBuilder_Address1 = dtBuilderGroup.Rows[0]["Builder_Address1"];
                        if (dtBuilderGroup.Columns.Contains("Builder_Address1") && valueBuilder_Address1 != null && valueBuilder_Address1.ToString() != "")
                        ival_BuilderGroupMaster.Builder_Address1 = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_Address1"]);
                        else
                            ival_BuilderGroupMaster.Builder_Address1 = string.Empty;

                        object valueBuilder_Address2 = dtBuilderGroup.Rows[0]["Builder_Address2"];
                        if(dtBuilderGroup.Columns.Contains("Builder_Address2") && valueBuilder_Address2!= null && valueBuilder_Address2.ToString() != "")
                        ival_BuilderGroupMaster.Builder_Address2 = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_Address2"]);
                           else
                             ival_BuilderGroupMaster.Builder_Address2 = string.Empty;

                    object valueBuilder_Locality = dtBuilderGroup.Rows[0]["Builder_Locality"];
                    if (dtBuilderGroup.Columns.Contains("Builder_Locality") && valueBuilder_Locality != null && valueBuilder_Locality.ToString() != "")
                        ival_BuilderGroupMaster.Builder_Locality = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_Locality"]);
                    else
                        ival_BuilderGroupMaster.Builder_Locality = string.Empty;

                    object valueBuilder_State = dtBuilderGroup.Rows[0]["Builder_State"];
                        if (dtBuilderGroup.Columns.Contains("Builder_State") && valueBuilder_State != null && valueBuilder_State.ToString() != "")
                        ival_BuilderGroupMaster.Builder_State = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_State"]);
                        else
                            ival_BuilderGroupMaster.Builder_State = string.Empty;

                    object valueBuilder_District = dtBuilderGroup.Rows[0]["Builder_District"];
                    if (dtBuilderGroup.Columns.Contains("Builder_District") && valueBuilder_District != null && valueBuilder_District.ToString() != "")
                        ival_BuilderGroupMaster.Builder_District = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_District"]);
                    else
                        ival_BuilderGroupMaster.Builder_District = string.Empty;

                    object valueBuilder_City = dtBuilderGroup.Rows[0]["Builder_City"];
                    if (dtBuilderGroup.Columns.Contains("Builder_City") && valueBuilder_City != null && valueBuilder_City.ToString() != "")
                        ival_BuilderGroupMaster.Builder_City = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_City"]);
                    else
                        ival_BuilderGroupMaster.Builder_City = string.Empty;

                    object valueBuilder_Pincode = dtBuilderGroup.Rows[0]["Builder_Pincode"];
                    if (dtBuilderGroup.Columns.Contains("Builder_Pincode") && valueBuilder_Pincode != null && valueBuilder_Pincode.ToString() != "")
                        ival_BuilderGroupMaster.Builder_Pincode = Convert.ToInt32(dtBuilderGroup.Rows[0]["Builder_Pincode"]);
                    else
                        ival_BuilderGroupMaster.Builder_Pincode = 0;

                    object valueNearby_Landmark = dtBuilderGroup.Rows[0]["Nearby_Landmark"];
                    if (dtBuilderGroup.Columns.Contains("Nearby_Landmark") && valueNearby_Landmark != null && valueNearby_Landmark.ToString() != "")
                        ival_BuilderGroupMaster.Nearby_Landmark = Convert.ToString(dtBuilderGroup.Rows[0]["Nearby_Landmark"]);
                    else
                        ival_BuilderGroupMaster.Nearby_Landmark = string.Empty;

                    object valueDirector_Name1 =dtBuilderGroup.Rows[0]["Director_Name1"];
                    if (dtBuilderGroup.Columns.Contains("Director_Name1") && valueDirector_Name1 != null && valueDirector_Name1.ToString() != "")
                        ival_BuilderGroupMaster.Director_Name1 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Name1"]);
                    else
                        ival_BuilderGroupMaster.Director_Name1 = string.Empty;

                    object valueDirector_Name2 = dtBuilderGroup.Rows[0]["Director_Name2"];
                    if (dtBuilderGroup.Columns.Contains("Director_Name2") && valueDirector_Name2 != null && valueDirector_Name2.ToString() != "")
                        ival_BuilderGroupMaster.Director_Name2 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Name2"]);
                    else
                        ival_BuilderGroupMaster.Director_Name2 = string.Empty;

                    object valueDirector_Name3 = dtBuilderGroup.Rows[0]["Director_Name3"];
                    if (dtBuilderGroup.Columns.Contains("Director_Name3") && valueDirector_Name3 != null && valueDirector_Name3.ToString() != "")
                        ival_BuilderGroupMaster.Director_Name3 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Name3"]);
                    else
                        ival_BuilderGroupMaster.Director_Name3 = string.Empty;

                    object valueDirector_Phone1 = dtBuilderGroup.Rows[0]["Director_Phone1"];
                    if (dtBuilderGroup.Columns.Contains("Director_Phone1") && valueDirector_Phone1 != null && valueDirector_Phone1.ToString() != "")
                        ival_BuilderGroupMaster.Director_Phone1 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Phone1"]);
                    else
                        ival_BuilderGroupMaster.Director_Phone1 = string.Empty;

                    object valueDirector_Phone2 = dtBuilderGroup.Rows[0]["Director_Phone2"];
                    if (dtBuilderGroup.Columns.Contains("Director_Phone2") && valueDirector_Phone2 != null && valueDirector_Phone2.ToString() != "")
                        ival_BuilderGroupMaster.Director_Phone2 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Phone2"]);
                    else
                        ival_BuilderGroupMaster.Director_Phone2 = string.Empty;

                    object valueDirector_Phone3 = dtBuilderGroup.Rows[0]["Director_Phone3"];
                    if (dtBuilderGroup.Columns.Contains("Director_Phone3") && valueDirector_Phone3 != null && valueDirector_Phone3.ToString() != "")
                        ival_BuilderGroupMaster.Director_Phone3 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Phone3"]);
                    else
                        ival_BuilderGroupMaster.Director_Phone3 = string.Empty;

                    object valueDirector_Mobile1 = dtBuilderGroup.Rows[0]["Director_Mobile1"];
                    if (dtBuilderGroup.Columns.Contains("Director_Mobile1") && valueDirector_Mobile1 != null && valueDirector_Mobile1.ToString() != "")
                        ival_BuilderGroupMaster.Director_Mobile1 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Mobile1"]);
                    else
                        ival_BuilderGroupMaster.Director_Mobile1 = string.Empty;

                    object valueDirector_Mobile2 = dtBuilderGroup.Rows[0]["Director_Mobile2"];
                    if (dtBuilderGroup.Columns.Contains("Director_Mobile2") && valueDirector_Mobile2 != null && valueDirector_Mobile2.ToString() != "")
                        ival_BuilderGroupMaster.Director_Mobile2 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Mobile2"]);
                    else
                        ival_BuilderGroupMaster.Director_Mobile2 = string.Empty;

                    object valueDirector_Mobile3 = dtBuilderGroup.Rows[0]["Director_Mobile3"];
                    if (dtBuilderGroup.Columns.Contains("Director_Mobile3") && valueDirector_Mobile3 != null && valueDirector_Mobile3.ToString() != "")
                        ival_BuilderGroupMaster.Director_Mobile3 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Mobile3"]);
                    else
                        ival_BuilderGroupMaster.Director_Mobile3 = string.Empty;

                    object valueDirector_Email1 = dtBuilderGroup.Rows[0]["Director_Email1"];
                    if (dtBuilderGroup.Columns.Contains("Director_Email1") && valueDirector_Email1 != null && valueDirector_Email1.ToString() != "")
                        ival_BuilderGroupMaster.Director_Email1 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Email1"]);
                    else
                        ival_BuilderGroupMaster.Director_Email1 = string.Empty;

                    object valueDirector_Email2 =dtBuilderGroup.Rows[0]["Director_Email2"];
                    if (dtBuilderGroup.Columns.Contains("Director_Email2") && valueDirector_Email2 != null && valueDirector_Email2.ToString() != "")
                        ival_BuilderGroupMaster.Director_Email2 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Email2"]);
                    else
                        ival_BuilderGroupMaster.Director_Email2 = string.Empty;

                    object valueDirector_Email3 = dtBuilderGroup.Rows[0]["Director_Email3"];
                    if (dtBuilderGroup.Columns.Contains("Director_Email3") && valueDirector_Email3 != null && valueDirector_Email3.ToString() != "")
                        ival_BuilderGroupMaster.Director_Email3 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Email3"]);
                    else
                        ival_BuilderGroupMaster.Director_Email3 = string.Empty;

                    object valueBuilder_Grade = dtBuilderGroup.Rows[0]["Builder_Grade"];
                    if (dtBuilderGroup.Columns.Contains("Builder_Grade") && valueBuilder_Grade != null && valueBuilder_Grade.ToString() != "")
                        ival_BuilderGroupMaster.Builder_Grade = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_Grade"]);
                    else
                        ival_BuilderGroupMaster.Builder_Grade = string.Empty;

                    vMBuilderGroupMaster.ival_BuilderGroupMaster = ival_BuilderGroupMaster;

                    object valueStreet = dtBuilderGroup.Rows[0]["Builder_Street"];
                    if (dtBuilderGroup.Columns.Contains("Builder_Street") && valueStreet != null && valueStreet.ToString() != "")
                        ival_BuilderGroupMaster.Builder_Street = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_Street"]);
                    else
                        ival_BuilderGroupMaster.Builder_Street = string.Empty;

                    vMBuilderGroupMaster.ival_BuilderGroupMaster = ival_BuilderGroupMaster;


                    vMBuilderGroupMaster.ival_StatesList = GetState();
                    //vMBuilderGroupMaster.PincodeList = GetPincode();
                    vMBuilderGroupMaster.BuilderGroupList = GetBuilderList();
                   vMBuilderGroupMaster.ival_DistrictsList = GetDistrict();
                    vMBuilderGroupMaster.ival_LocalityList = GetLocality();
                    vMBuilderGroupMaster.ival_CitiesList = GetCity();
                    // IEnumerable<ival_Districts> ival_DistList = GetDistByStateId();
                    IEnumerable<ival_States> ival_StatesList = GetState();
                    if (ival_StatesList.ToList().Count > 0 && ival_StatesList != null)
                    {
                        var BuilderState = ival_StatesList.Where(s => s.State_Name == dtBuilderGroup.Rows[0]["Builder_State"].ToString()).Select(s => s.State_ID);
                      
                        vMBuilderGroupMaster.SelectedBuilderStateId = Convert.ToInt32(BuilderState.FirstOrDefault());
                       
                    }

                    /*IEnumerable<ival_LookupCategoryValues> pincodelist = GetPincode();
                    if (pincodelist.ToList().Count > 0 && pincodelist != null)
                    {
                       var builderpostalpincode = pincodelist.Where(s => s.Lookup_Value == dtBuilderGroup.Rows[0]["Builder_Pincode"].ToString()).Select(s => s.Lookup_ID);

                        vMBuilderGroupMaster.SelectedBuilderPincodeID = Convert.ToInt32(builderpostalpincode.FirstOrDefault());

                    }*/
                    IEnumerable<ival_Cities> ival_CitiesList = GetCity();
                    if (ival_CitiesList.ToList().Count > 0 && ival_CitiesList != null)
                    {
                        var buildercity = ival_CitiesList.Where(s => s.City_Name == dtBuilderGroup.Rows[0]["Builder_City"].ToString()).Select(s => s.City_ID);

                        vMBuilderGroupMaster.selectedCityID = Convert.ToInt32(buildercity.FirstOrDefault());

                    }
                    IEnumerable<ival_Districts> ival_DistrictsList = GetDistrict();
                    if (ival_DistrictsList.ToList().Count > 0 && ival_DistrictsList != null)
                    {
                        var BuilderDistrict = ival_DistrictsList.Where(s => s.District_Name == dtBuilderGroup.Rows[0]["Builder_District"].ToString()).Select(s => s.District_ID);

                        vMBuilderGroupMaster.selectedDistID = Convert.ToInt32(BuilderDistrict.FirstOrDefault());

                    }
                    IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                    if (ival_LocalityList.ToList().Count > 0 && ival_LocalityList != null)
                    {
                        var BuilderCity = ival_LocalityList.Where(s => s.Loc_Name == dtBuilderGroup.Rows[0]["Builder_Locality"].ToString()).Select(s => s.Loc_Id);

                        vMBuilderGroupMaster.selectedLocID = Convert.ToInt32(BuilderCity.FirstOrDefault());

                        vMBuilderGroupMaster.SelectedBuilderPincodeID = Convert.ToInt32(BuilderCity.FirstOrDefault());
                    }
                    
                }

                vMBuilderGroupMaster.Builder_Group_Master_ID = ival_BuilderGroupMaster.Builder_Group_Master_ID;
            }
            catch (Exception ex)
            {
                //    ex.InnerException.ToString();
                Json( ex.Message , JsonRequestBehavior.AllowGet);
            }
            return View(vMBuilderGroupMaster);
        }

        [HttpPost]
        public ActionResult UpdateBuilder(string str)
        {

            try
            {

                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

                for (int i = 0; i < list1.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();

                    hInputPara.Add("@Builder_Group_Master_ID", Convert.ToString(list1.Rows[i]["Builder_Master_ID"]));
                    hInputPara.Add("@BuilderGroupName", Convert.ToString(list1.Rows[i]["BuilderGroupName"]));
                    hInputPara.Add("@Builder_Address1", Convert.ToString(list1.Rows[i]["officeAdd1"]));
                    hInputPara.Add("@Builder_Address2", Convert.ToString(list1.Rows[i]["officeAdd2"]));
                    hInputPara.Add("@Builder_Locality", Convert.ToString(list1.Rows[i]["ddlLocality"]));
                    hInputPara.Add("@Nearby_Landmark", Convert.ToString(list1.Rows[i]["nearByLandmark"]));
                    hInputPara.Add("@Builder_District", Convert.ToString(list1.Rows[i]["ddlDistrict"]));
                    hInputPara.Add("@Builder_City", Convert.ToString(list1.Rows[i]["ddlCity"]));
                    hInputPara.Add("@Builder_State", Convert.ToString(list1.Rows[i]["ddlstate"]));
                    hInputPara.Add("@Builder_Pincode", Convert.ToString(list1.Rows[i]["ddlPincode"]));

                    hInputPara.Add("@Director_Name1", Convert.ToString(list1.Rows[i]["DirectorName1"]));
                    hInputPara.Add("@Director_Phone1", Convert.ToString(list1.Rows[i]["DirectorPhone1"]));
                    hInputPara.Add("@Director_Mobile1", Convert.ToString(list1.Rows[i]["DirectorMobile1"]));
                    hInputPara.Add("@Director_Email1", Convert.ToString(list1.Rows[i]["DirectorEmail1"]));
                    hInputPara.Add("@Director_Name2", Convert.ToString(list1.Rows[i]["DirectorName2"]));
                    hInputPara.Add("@Director_Phone2", Convert.ToString(list1.Rows[i]["DirectorPhone2"]));
                    hInputPara.Add("@Director_Mobile2", Convert.ToString(list1.Rows[i]["DirectorMobile2"]));
                    hInputPara.Add("@Director_Email2", Convert.ToString(list1.Rows[i]["DirectorEmail2"]));
                    hInputPara.Add("@Director_Name3", Convert.ToString(list1.Rows[i]["DirectorName3"]));

                    hInputPara.Add("@Director_Phone3", Convert.ToString(list1.Rows[i]["DirectorPhone3"]));
                    hInputPara.Add("@Director_Mobile3", Convert.ToString(list1.Rows[i]["DirectorMobile3"]));


                    hInputPara.Add("@Director_Email3", Convert.ToString(list1.Rows[i]["DirectorEmail3"]));
                    hInputPara.Add("@Builder_Street", Convert.ToString(list1.Rows[i]["street"]));
                    hInputPara.Add("@Builder_Grade", Convert.ToString(list1.Rows[i]["BuilderGrade"]));
                    hInputPara.Add("@Updated_By", string.Empty);

                    string result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateBuilderGroupMasterDetailsByID", hInputPara);
                    return Json(new { success = true, Message = "Update Successfully" }, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception e)
            {
                e.InnerException.ToString();
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
            return Json("");
        }

        /* [HttpPost]
         public ActionResult EditBuilder(VMBuilderGroupMaster vMBuilderGroupMaster)
         {

             try
             {

                 IEnumerable<ival_States> ival_StatesList = GetState();
                 var builderState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMBuilderGroupMaster.SelectedBuilderStateId).Select(row => row.State_Name.ToString());

                 //IEnumerable<ival_Cities> ival_PincodeList = GetCity();


                 IEnumerable<ival_Districts> ival_Districtlist = GetDistrict();
                 var builderDistrict = ival_Districtlist.AsEnumerable().Where(row => row.District_ID == vMBuilderGroupMaster.selectedDistID).Select(row => row.District_Name.ToString());

                 IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                 var builderCity = ival_LocalityList.AsEnumerable().Where(row => row.Loc_Id == vMBuilderGroupMaster.selectedCityID).Select(row => row.Loc_Name.ToString());
                 var builderPincode = ival_LocalityList.AsEnumerable().Where(row => row.Loc_Id == vMBuilderGroupMaster.SelectedBuilderPincodeID).Select(row => row.Pin_Code.ToString());

                 hInputPara = new Hashtable();
                 sqlDataAccess = new SQLDataAccess();

                 hInputPara = new Hashtable();
                 hInputPara.Add("@Builder_Group_Master_ID", Convert.ToInt32(vMBuilderGroupMaster.ival_BuilderGroupMaster.Builder_Group_Master_ID));
                 hInputPara.Add("@BuilderGroupName", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.BuilderGroupName));
                 hInputPara.Add("@Builder_Address1", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Builder_Address1));
                 hInputPara.Add("@Builder_Address2", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Builder_Address2));
                // hInputPara.Add("@Builder_Locality", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Builder_Locality));
                 hInputPara.Add("@Nearby_Landmark", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Nearby_Landmark));
                 hInputPara.Add("@Builder_District", builderDistrict.FirstOrDefault());
                 hInputPara.Add("@Builder_City",builderCity.FirstOrDefault());
                 hInputPara.Add("@Builder_State", builderState.FirstOrDefault());
                 hInputPara.Add("@Builder_Pincode ", builderPincode.FirstOrDefault());
                 hInputPara.Add("@Director_Name1 ", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Name1));
                 hInputPara.Add("@Director_Phone1 ", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Phone1));
                 hInputPara.Add("@Director_Mobile1 ", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Mobile1));
                 hInputPara.Add("@Director_Email1", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Email1));
                 hInputPara.Add("@Director_Name2", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Name2));
                 hInputPara.Add("@Director_Phone2 ", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Phone2));
                 hInputPara.Add("@Director_Mobile2", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Mobile2));
                 hInputPara.Add("@Director_Email2 ", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Email2));
                 hInputPara.Add("@Director_Name3", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Name3));
                 hInputPara.Add("@Director_Phone3", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Phone3));
                 hInputPara.Add("@Director_Mobile3", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Mobile3));
                 hInputPara.Add("@Director_Email3", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Director_Email3));
                 hInputPara.Add("@Builder_Street", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Builder_Street));
                 hInputPara.Add("@Updated_By", string.Empty);
                 hInputPara.Add("@Builder_Grade", Convert.ToString(vMBuilderGroupMaster.ival_BuilderGroupMaster.Builder_Grade));
                 var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateBuilderGroupMasterDetailsByID", hInputPara);

                 if (Convert.ToInt32(result) == 1)
                 {
                     return RedirectToAction("Index", "BuilderAndCompany");
                 }
                 else
                 {
                     return Json("Error in Database !", JsonRequestBehavior.AllowGet);
                 }
             }
             catch (Exception e)
             {
                 //e.InnerException.ToString();
                 return Json(e.Message, JsonRequestBehavior.AllowGet);
             }

         }*/
        public ActionResult ViewCompany(int? ID)
        {
            if (ID != null)
            {
                try
                {
                    vMBuilderGroupMaster = new VMBuilderGroupMaster();
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Builder_Group_Master_ID", ID);
                    DataTable dtBuilderGroupCompany = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCompanyListByBuilderGroupID", hInputPara);

                    List<ival_BuilderCompanyMaster> ival_BuilderCompanyMastersList = new List<ival_BuilderCompanyMaster>();

                    if (dtBuilderGroupCompany.Rows.Count > 0 && dtBuilderGroupCompany!=null)

                    {
                        for (int i = 0; i < dtBuilderGroupCompany.Rows.Count; i++)
                        {

                            ival_BuilderCompanyMastersList.Add(new ival_BuilderCompanyMaster
                            {
                                Builder_Company_Master_ID= Convert.ToInt32(dtBuilderGroupCompany.Rows[i]["Builder_Company_Master_ID"]),
                                Builder_Group_Master_ID = Convert.ToInt32(dtBuilderGroupCompany.Rows[i]["Builder_Group_Master_ID"]),
                                Builder_Company_Name = Convert.ToString(dtBuilderGroupCompany.Rows[i]["Builder_Company_Name"]),
                                Builder_C_City = Convert.ToString(dtBuilderGroupCompany.Rows[i]["Builder_C_City"]),
                                Builder_C_State = Convert.ToString(dtBuilderGroupCompany.Rows[i]["Builder_C_State"])
                            });


                        }
                    }

                    return View(ival_BuilderCompanyMastersList);

                }
                catch (Exception ex)
                {
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("Error in Link", JsonRequestBehavior.AllowGet);
            }
           
        }
        //changes by punam
        public JsonResult ViewCompanyDetailsByGroup(int? Id)
        {
            if (Id != null)
            {
                try
                {
                    vMBuilderGroupMaster = new VMBuilderGroupMaster();
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Builder_Group_Master_ID", Id);
                    DataTable dtBuilderGroupCompany = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCompanyListByBuilderGroupIDNew", hInputPara);

                    List<ival_BuilderCompanyMasterNew> ival_BuilderCompanyMastersList = new List<ival_BuilderCompanyMasterNew>();
                    string builderGroupName = "";
                    if (dtBuilderGroupCompany.Rows.Count > 0 && dtBuilderGroupCompany != null)

                    {
                        for (int i = 0; i < dtBuilderGroupCompany.Rows.Count; i++)
                        {

                            ival_BuilderCompanyMastersList.Add(new ival_BuilderCompanyMasterNew
                            {
                                Builder_Company_Master_ID = Convert.ToInt32(dtBuilderGroupCompany.Rows[i]["Builder_Company_Master_ID"]),
                                Builder_Group_Master_ID = Convert.ToInt32(dtBuilderGroupCompany.Rows[i]["Builder_Group_Master_ID"]),
                                Builder_Company_Name = Convert.ToString(dtBuilderGroupCompany.Rows[i]["Builder_Company_Name"]),
                                Builder_C_City = Convert.ToString(dtBuilderGroupCompany.Rows[i]["Builder_C_City"]),
                                Builder_C_State = Convert.ToString(dtBuilderGroupCompany.Rows[i]["Builder_C_State"]),

                            });

                            builderGroupName = Convert.ToString(dtBuilderGroupCompany.Rows[i]["BuilderGroupName"]);
                            ViewBag.BuilderGroupName = builderGroupName;

                        }
                    }
                    //ViewBag.List = ival_BuilderCompanyMastersList;
                    //vMBuilderGroupMaster.ival_BuilderCompanyMasterNewList = ival_BuilderCompanyMastersList;
                    // return View(ival_BuilderCompanyMastersList);
                    //return RedirectToAction("Index");
                    //return Json(new { success = true, Message = "Successfully" }, JsonRequestBehavior.AllowGet);

                    var jsonSerialiser = new JavaScriptSerializer();
                    var jsonBuilderCompany = jsonSerialiser.Serialize(ival_BuilderCompanyMastersList);
                    return Json(jsonBuilderCompany);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("Error in Link", JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult ViewBuilderCompany(int? ID)
        {
            vMBuilderGroupMaster = new VMBuilderGroupMaster();
            ival_BuilderCompanyMasterNew = new ival_BuilderCompanyMasterNew();
            vMBuilderGroupMaster.ival_StatesList = GetState();
            vMBuilderGroupMaster.PincodeList = GetPincode();
            vMBuilderGroupMaster.BuilderGroupList = GetBuilderList();
            vMBuilderGroupMaster.ival_natureOfcompanyList = GetNatureOfCompanyList();

            vMBuilderGroupMaster.ival_DistrictsList = GetDistrict();
            vMBuilderGroupMaster.ival_LocalityList = GetLocality();
            vMBuilderGroupMaster.ival_CitiesList = GetCity();
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //    return view();
            }



            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Builder_Company_Master_ID", ID);
                DataTable dtBuilderCompany = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuilderCompanyMasterNewDetails", hInputPara);
                if (dtBuilderCompany.Rows.Count > 0 && dtBuilderCompany != null)
                {


                    object valueBuilder_Company_Master_ID = dtBuilderCompany.Rows[0]["Builder_Company_Master_ID"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Company_Master_ID") && valueBuilder_Company_Master_ID != null && valueBuilder_Company_Master_ID.ToString() != "")
                        vMBuilderGroupMaster.Builder_Company_Master_IDl = Convert.ToInt32(dtBuilderCompany.Rows[0]["Builder_Company_Master_ID"]);
                    else
                        vMBuilderGroupMaster.Builder_Company_Master_IDl = 0;

                    object valueBuilder_Group_Master_ID = dtBuilderCompany.Rows[0]["Builder_Group_Master_ID"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Group_Master_ID") && valueBuilder_Group_Master_ID != null && valueBuilder_Group_Master_ID.ToString() != "")
                    {
                        ival_BuilderCompanyMasterNew.Builder_Group_Master_ID = Convert.ToInt32(dtBuilderCompany.Rows[0]["Builder_Group_Master_ID"]);
                        vMBuilderGroupMaster.SelectedBuilderGroupMasterID = Convert.ToInt32(dtBuilderCompany.Rows[0]["Builder_Group_Master_ID"]);
                    }
                    else
                    {
                        ival_BuilderCompanyMasterNew.Builder_Group_Master_ID = 0;
                        vMBuilderGroupMaster.SelectedBuilderGroupMasterID = 0; ;
                    }
                    object valueBuilder_NatureOfCom = dtBuilderCompany.Rows[0]["Nature_Of_Company"];
                    if (dtBuilderCompany.Columns.Contains("Nature_Of_Company") && valueBuilder_NatureOfCom != null && valueBuilder_NatureOfCom.ToString() != "")
                    {
                        //ival_BuilderCompanyMasterNew.Builder_Group_Master_ID = Convert.ToInt32(dtBuilderCompany.Rows[0]["Builder_Group_Master_ID"]);
                        vMBuilderGroupMaster.SelectedBuilderNatureOfCom = Convert.ToString(dtBuilderCompany.Rows[0]["Nature_Of_Company"]);
                    }
                    else
                    {
                        //ival_BuilderCompanyMasterNew.Builder_Group_Master_ID = 0;
                        vMBuilderGroupMaster.SelectedBuilderNatureOfCom = string.Empty; ;
                    }

                    object valueBuilder_Company_Name = dtBuilderCompany.Rows[0]["Builder_Company_Name"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Company_Name") && valueBuilder_Company_Name != null && valueBuilder_Company_Name.ToString() != "")
                        //ival_BuilderCompanyMasterNew.Builder_Company_Name = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Company_Name"]);

                        ViewBag.companyName = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Company_Name"]);
                    else
                        ViewBag.companyName = string.Empty;
                    object valueBuilder_officeadd1 = dtBuilderCompany.Rows[0]["Builder_Office_Address1"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Office_Address1") && valueBuilder_officeadd1 != null && valueBuilder_officeadd1.ToString() != "")

                        //ival_BuilderCompanyMaster.Builder_Company_Name = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Company_Name"]);
                        ViewBag.officeadd1 = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Office_Address1"]);
                    // vMBuilderGroupMaster.ival_BuilderCompanyMasterNew.Builder_Office_Address1 = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Office_Address1"]);                    

                    else
                        ViewBag.officeadd1 = string.Empty;

                    object valueBuilder_officeadd2 = dtBuilderCompany.Rows[0]["Builder_Office_Address2"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Office_Address2") && valueBuilder_officeadd2 != null && valueBuilder_officeadd2.ToString() != "")

                        ViewBag.officeadd2 = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Office_Address2"]);
                    else
                        ViewBag.officeadd2 = string.Empty;

                    object valueBuilder_street = dtBuilderCompany.Rows[0]["Builder_C_Street"];
                    if (dtBuilderCompany.Columns.Contains("Builder_C_Street") && valueBuilder_street != null && valueBuilder_street.ToString() != "")

                        ViewBag.street = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_C_Street"]);
                    else
                        ViewBag.street = string.Empty;

                    /*object valueBuilder_locality = dtBuilderCompany.Rows[0]["Builder_C_Locality"];
                    if (dtBuilderCompany.Columns.Contains("Builder_C_Locality") && valueBuilder_locality != null && valueBuilder_locality.ToString() != "")

                        ViewBag.locality = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_C_Locality"]);
                    else
                        ViewBag.locality = string.Empty;*/

                    object valueBuilder_NearbyLandMark = dtBuilderCompany.Rows[0]["Builder_Nearby_LandMark"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Nearby_LandMark") && valueBuilder_NearbyLandMark != null && valueBuilder_NearbyLandMark.ToString() != "")

                        ViewBag.nearbyLand = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Nearby_LandMark"]);
                    else
                        ViewBag.nearbyLand = string.Empty;

                    object valueBuilder_directorName1 = dtBuilderCompany.Rows[0]["Director_Name1"];
                    if (dtBuilderCompany.Columns.Contains("Director_Name1") && valueBuilder_directorName1 != null && valueBuilder_directorName1.ToString() != "")

                        ViewBag.directorName1 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_Name1"]);
                    else
                        ViewBag.directorName1 = string.Empty;
                    object valueBuilder_directorName2 = dtBuilderCompany.Rows[0]["Director_Name2"];
                    if (dtBuilderCompany.Columns.Contains("Director_Name2") && valueBuilder_directorName2 != null && valueBuilder_directorName2.ToString() != "")

                        ViewBag.directorName2 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_Name2"]);
                    else
                        ViewBag.directorName2 = string.Empty;
                    object valueBuilder_directorName3 = dtBuilderCompany.Rows[0]["Director_Name3"];
                    if (dtBuilderCompany.Columns.Contains("Director_Name3") && valueBuilder_directorName3 != null && valueBuilder_directorName3.ToString() != "")

                        ViewBag.directorName3 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_Name3"]);
                    else
                        ViewBag.directorName3 = string.Empty;

                    object valueBuilder_directorphone1 = dtBuilderCompany.Rows[0]["Director_Phone1"];
                    if (dtBuilderCompany.Columns.Contains("Director_Phone1") && valueBuilder_directorphone1 != null && valueBuilder_directorphone1.ToString() != "")

                        ViewBag.directorphone1 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_Phone1"]);
                    else
                        ViewBag.directorphone1 = string.Empty;


                    object valueBuilder_directorphone2 = dtBuilderCompany.Rows[0]["Director_Phone2"];
                    if (dtBuilderCompany.Columns.Contains("Director_Phone2") && valueBuilder_directorphone2 != null && valueBuilder_directorphone2.ToString() != "")

                        ViewBag.directorphone2 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_Phone2"]);
                    else
                        ViewBag.directorphone2 = string.Empty;
                    object valueBuilder_directorphone3 = dtBuilderCompany.Rows[0]["Director_Phone3"];
                    if (dtBuilderCompany.Columns.Contains("Director_Phone3") && valueBuilder_directorphone3 != null && valueBuilder_directorphone3.ToString() != "")

                        ViewBag.directorphone3 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_Phone3"]);
                    else
                        ViewBag.directorphone3 = string.Empty;

                    object valueBuilder_directormobile1 = dtBuilderCompany.Rows[0]["Director_Mobile1"];
                    if (dtBuilderCompany.Columns.Contains("Director_Mobile1") && valueBuilder_directormobile1 != null && valueBuilder_directormobile1.ToString() != "")

                        ViewBag.directorMobile1 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_Mobile1"]);
                    else
                        ViewBag.directorMobile1 = string.Empty;

                    object valueBuilder_directormobile2 = dtBuilderCompany.Rows[0]["Director_Mobile2"];
                    if (dtBuilderCompany.Columns.Contains("Director_Mobile2") && valueBuilder_directormobile2 != null && valueBuilder_directormobile2.ToString() != "")

                        ViewBag.directorMobile2 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_Mobile2"]);
                    else
                        ViewBag.directorMobile2 = string.Empty;


                    object valueBuilder_directormobile3 = dtBuilderCompany.Rows[0]["Director_Mobile3"];
                    if (dtBuilderCompany.Columns.Contains("Director_Mobile3") && valueBuilder_directormobile3 != null && valueBuilder_directormobile3.ToString() != "")

                        ViewBag.directorMobile3 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_Mobile3"]);
                    else
                        ViewBag.directorMobile3 = string.Empty;

                    object valueBuilder_directoremail1 = dtBuilderCompany.Rows[0]["Director_EmailId1"];
                    if (dtBuilderCompany.Columns.Contains("Director_EmailId1") && valueBuilder_directoremail1 != null && valueBuilder_directoremail1.ToString() != "")

                        ViewBag.directorEmail1 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_EmailId1"]);
                    else
                        ViewBag.directorEmail1 = string.Empty;

                    object valueBuilder_directoremail2 = dtBuilderCompany.Rows[0]["Director_EmailId2"];
                    if (dtBuilderCompany.Columns.Contains("Director_EmailId2") && valueBuilder_directoremail2 != null && valueBuilder_directoremail2.ToString() != "")

                        ViewBag.directorEmail2 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_EmailId2"]);
                    else
                        ViewBag.directorEmail2 = string.Empty;

                    object valueBuilder_directoremail3 = dtBuilderCompany.Rows[0]["Director_EmailId3"];
                    if (dtBuilderCompany.Columns.Contains("Director_EmailId3") && valueBuilder_directoremail3 != null && valueBuilder_directoremail3.ToString() != "")

                        ViewBag.directorEmail3 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_EmailId3"]);
                    else
                        ViewBag.directorEmail3 = string.Empty;

                    object valueBuilder_BuilderGrade = dtBuilderCompany.Rows[0]["Builder_Grade"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Grade") && valueBuilder_BuilderGrade != null && valueBuilder_BuilderGrade.ToString() != "")

                        ViewBag.BuilderGrade = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Grade"]);
                    else
                        ViewBag.BuilderGrade = string.Empty;

                    object valueBuilder_State = dtBuilderCompany.Rows[0]["Builder_C_State"];
                    if (dtBuilderCompany.Columns.Contains("Builder_C_State") && valueBuilder_State != null && valueBuilder_State.ToString() != "")
                        ival_BuilderCompanyMasterNew.Builder_C_State = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_C_State"]);
                    else
                        ival_BuilderCompanyMasterNew.Builder_C_State = string.Empty;

                    object valueBuilder_District = dtBuilderCompany.Rows[0]["Builder_C_District"];
                    if (dtBuilderCompany.Columns.Contains("Builder_C_District") && valueBuilder_District != null && valueBuilder_District.ToString() != "")
                        ival_BuilderCompanyMasterNew.Builder_C_District = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_C_District"]);
                    else
                        ival_BuilderCompanyMasterNew.Builder_C_District = string.Empty;

                    object valueBuilder_City = dtBuilderCompany.Rows[0]["Builder_C_City"];
                    if (dtBuilderCompany.Columns.Contains("Builder_C_City") && valueBuilder_City != null && valueBuilder_City.ToString() != "")
                        ival_BuilderCompanyMasterNew.Builder_C_City = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_C_City"]);
                    else
                        ival_BuilderCompanyMasterNew.Builder_C_City = string.Empty;

                    object valueBuilder_Loc = dtBuilderCompany.Rows[0]["Builder_C_Locality"];
                    if (dtBuilderCompany.Columns.Contains("Builder_C_Locality") && valueBuilder_Loc != null && valueBuilder_Loc.ToString() != "")
                        ival_BuilderCompanyMasterNew.Builder_C_Locality = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_C_Locality"]);
                    else
                        ival_BuilderCompanyMasterNew.Builder_C_Locality = string.Empty;

                    object valueBuilder_Pincode = dtBuilderCompany.Rows[0]["Builder_C_Pincode"];
                    if (dtBuilderCompany.Columns.Contains("Builder_C_Pincode") && valueBuilder_Pincode != null && valueBuilder_Pincode.ToString() != "")
                        ival_BuilderCompanyMasterNew.Builder_C_Pincode = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_C_Pincode"]);
                    else
                        ival_BuilderCompanyMasterNew.Builder_C_Pincode = string.Empty;

                    IEnumerable<ival_States> ival_StatesList = GetState();
                    if (ival_StatesList.ToList().Count > 0 && ival_StatesList != null)
                    {
                        var BuilderState = ival_StatesList.Where(s => s.State_Name == dtBuilderCompany.Rows[0]["Builder_C_State"].ToString()).Select(s => s.State_ID);

                        vMBuilderGroupMaster.SelectedBuilderStateId = Convert.ToInt32(BuilderState.FirstOrDefault());

                    }

                    //IEnumerable<ival_LookupCategoryValues> pincodelist = GetPincode();
                    //if (pincodelist.ToList().Count > 0 && pincodelist != null)
                    //{
                    //    var builderpostalpincode = pincodelist.Where(s => s.Lookup_Value == dtBuilderGroup.Rows[0]["Builder_Pincode"].ToString()).Select(s => s.Lookup_ID);

                    //    vMBuilderGroupMaster.SelectedBuilderPincodeID = Convert.ToInt32(builderpostalpincode.FirstOrDefault());

                    //}
                   /* IEnumerable<ival_Cities> ival_pincodelist = GetCity();
                    if (ival_pincodelist.ToList().Count > 0 && ival_pincodelist != null)
                    {
                        var builderpostalpincode = ival_pincodelist.Where(s => s.Pin_Code == dtBuilderCompany.Rows[0]["Builder_C_Pincode"].ToString()).Select(s => s.Loc_Id);

                        vMBuilderGroupMaster.SelectedBuilderPincodeID = Convert.ToInt32(builderpostalpincode.FirstOrDefault());

                    }*/
                    IEnumerable<ival_Districts> ival_DistrictsList = GetDistrict();
                    if (ival_DistrictsList.ToList().Count > 0 && ival_DistrictsList != null)
                    {
                        var BuilderDistrict = ival_DistrictsList.Where(s => s.District_Name == dtBuilderCompany.Rows[0]["Builder_C_District"].ToString()).Select(s => s.District_ID);

                        vMBuilderGroupMaster.selectedDistID = Convert.ToInt32(BuilderDistrict.FirstOrDefault());

                    }
                    IEnumerable<ival_Cities> ival_CitiesList = GetCity();
                    if (ival_CitiesList.ToList().Count > 0 && ival_CitiesList != null)
                    {
                        var BuilderCity = ival_CitiesList.Where(s => s.City_Name == dtBuilderCompany.Rows[0]["Builder_C_City"].ToString()).Select(s => s.City_ID);

                        vMBuilderGroupMaster.selectedCityID = Convert.ToInt32(BuilderCity.FirstOrDefault());

                    }

                    IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                    if (ival_LocalityList.ToList().Count > 0 && ival_LocalityList != null)
                    {
                        var BuilderLoc = ival_LocalityList.Where(s => s.Loc_Name == dtBuilderCompany.Rows[0]["Builder_C_Locality"].ToString()).Select(s => s.Loc_Id);

                        vMBuilderGroupMaster.selectedLocID = Convert.ToInt32(BuilderLoc.FirstOrDefault());
                        vMBuilderGroupMaster.SelectedBuilderPincodeID = Convert.ToInt32(BuilderLoc.FirstOrDefault());
                    }

                    /*IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                    if (ival_LocalityList.ToList().Count > 0 && ival_LocalityList != null)
                    {
                        var BuilderCity = ival_LocalityList.Where(s => s.Loc_Name == dtBuilderCompany.Rows[0]["Builder_C_City"].ToString()).Select(s => s.Loc_Id);

                        vMBuilderGroupMaster.selectedCityID = Convert.ToInt32(BuilderCity.FirstOrDefault());

                    }*/





                }


            }
            catch (Exception e)
            {

            }

            return View(vMBuilderGroupMaster);
        }
       // changes by punam
        public ActionResult ViewDetails(int? ID)
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //    return view();
            }
            vMBuilderGroupMaster = new VMBuilderGroupMaster();
            ival_BuilderGroupMaster = new ival_BuilderGroupMaster();
            ival_BuilderCompanyMaster = new ival_BuilderCompanyMaster();

            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Builder_Group_Master_ID", ID);
                DataTable dtBuilderGroup = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuilderGroupIDDetailsByID", hInputPara);

                if (dtBuilderGroup.Rows.Count > 0 && dtBuilderGroup != null)
                {
                    object valueBuilder_Group_Master_ID = Convert.ToInt32(dtBuilderGroup.Rows[0]["Builder_Group_Master_ID"]);
                    if (dtBuilderGroup.Columns.Contains("Builder_Group_Master_ID") && valueBuilder_Group_Master_ID != null && valueBuilder_Group_Master_ID.ToString() != "")
                        ival_BuilderGroupMaster.Builder_Group_Master_ID = Convert.ToInt32(valueBuilder_Group_Master_ID);
                    else
                        ival_BuilderGroupMaster.Builder_Group_Master_ID = 0;

                    object valueBuilderGroupName = dtBuilderGroup.Rows[0]["BuilderGroupName"];
                    if (dtBuilderGroup.Columns.Contains("BuilderGroupName") && valueBuilderGroupName != null && valueBuilderGroupName.ToString() != "")
                        ival_BuilderGroupMaster.BuilderGroupName = Convert.ToString(dtBuilderGroup.Rows[0]["BuilderGroupName"]);
                    else
                        ival_BuilderGroupMaster.BuilderGroupName = string.Empty;

                    object valueBuilder_Address1 = dtBuilderGroup.Rows[0]["Builder_Address1"];
                    if (dtBuilderGroup.Columns.Contains("Builder_Address1") && valueBuilder_Address1 != null && valueBuilder_Address1.ToString() != "")
                        ival_BuilderGroupMaster.Builder_Address1 = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_Address1"]);
                    else
                        ival_BuilderGroupMaster.Builder_Address1 = string.Empty;

                    object valueBuilder_Address2 = dtBuilderGroup.Rows[0]["Builder_Address2"];
                    if (dtBuilderGroup.Columns.Contains("Builder_Address2") && valueBuilder_Address2 != null && valueBuilder_Address2.ToString() != "")
                        ival_BuilderGroupMaster.Builder_Address2 = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_Address2"]);
                    else
                        ival_BuilderGroupMaster.Builder_Address2 = string.Empty;

                    object valueBuilder_Locality = dtBuilderGroup.Rows[0]["Builder_Locality"];
                    if (dtBuilderGroup.Columns.Contains("Builder_Locality") && valueBuilder_Locality != null && valueBuilder_Locality.ToString() != "")
                        ival_BuilderGroupMaster.Builder_Locality = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_Locality"]);
                    else
                        ival_BuilderGroupMaster.Builder_Locality = string.Empty;

                    object valueBuilder_State = dtBuilderGroup.Rows[0]["Builder_State"];
                    if (dtBuilderGroup.Columns.Contains("Builder_State") && valueBuilder_State != null && valueBuilder_State.ToString() != "")
                        ival_BuilderGroupMaster.Builder_State = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_State"]);
                    else
                        ival_BuilderGroupMaster.Builder_State = string.Empty;

                    object valueBuilder_District = dtBuilderGroup.Rows[0]["Builder_District"];
                    if (dtBuilderGroup.Columns.Contains("Builder_District") && valueBuilder_District != null && valueBuilder_District.ToString() != "")
                        ival_BuilderGroupMaster.Builder_District = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_District"]);
                    else
                        ival_BuilderGroupMaster.Builder_District = string.Empty;

                    object valueBuilder_City = dtBuilderGroup.Rows[0]["Builder_City"];
                    if (dtBuilderGroup.Columns.Contains("Builder_City") && valueBuilder_City != null && valueBuilder_City.ToString() != "")
                        ival_BuilderGroupMaster.Builder_City = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_City"]);
                    else
                        ival_BuilderGroupMaster.Builder_City = string.Empty;

                    object valueBuilder_Pincode = dtBuilderGroup.Rows[0]["Builder_Pincode"];
                    if (dtBuilderGroup.Columns.Contains("Builder_Pincode") && valueBuilder_Pincode != null && valueBuilder_Pincode.ToString() != "")
                        ival_BuilderGroupMaster.Builder_Pincode = Convert.ToInt32(dtBuilderGroup.Rows[0]["Builder_Pincode"]);
                    else
                        ival_BuilderGroupMaster.Builder_Pincode = 0;

                    object valueNearby_Landmark = dtBuilderGroup.Rows[0]["Nearby_Landmark"];
                    if (dtBuilderGroup.Columns.Contains("Nearby_Landmark") && valueNearby_Landmark != null && valueNearby_Landmark.ToString() != "")
                        ival_BuilderGroupMaster.Nearby_Landmark = Convert.ToString(dtBuilderGroup.Rows[0]["Nearby_Landmark"]);
                    else
                        ival_BuilderGroupMaster.Nearby_Landmark = string.Empty;

                    object valueDirector_Name1 = dtBuilderGroup.Rows[0]["Director_Name1"];
                    if (dtBuilderGroup.Columns.Contains("Director_Name1") && valueDirector_Name1 != null && valueDirector_Name1.ToString() != "")
                        ival_BuilderGroupMaster.Director_Name1 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Name1"]);
                    else
                        ival_BuilderGroupMaster.Director_Name1 = string.Empty;

                    object valueDirector_Name2 = dtBuilderGroup.Rows[0]["Director_Name2"];
                    if (dtBuilderGroup.Columns.Contains("Director_Name2") && valueDirector_Name2 != null && valueDirector_Name2.ToString() != "")
                        ival_BuilderGroupMaster.Director_Name2 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Name2"]);
                    else
                        ival_BuilderGroupMaster.Director_Name2 = string.Empty;

                    object valueDirector_Name3 = dtBuilderGroup.Rows[0]["Director_Name3"];
                    if (dtBuilderGroup.Columns.Contains("Director_Name3") && valueDirector_Name3 != null && valueDirector_Name3.ToString() != "")
                        ival_BuilderGroupMaster.Director_Name3 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Name3"]);
                    else
                        ival_BuilderGroupMaster.Director_Name3 = string.Empty;

                    object valueDirector_Phone1 = dtBuilderGroup.Rows[0]["Director_Phone1"];
                    if (dtBuilderGroup.Columns.Contains("Director_Phone1") && valueDirector_Phone1 != null && valueDirector_Phone1.ToString() != "")
                        ival_BuilderGroupMaster.Director_Phone1 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Phone1"]);
                    else
                        ival_BuilderGroupMaster.Director_Phone1 = string.Empty;

                    object valueDirector_Phone2 = dtBuilderGroup.Rows[0]["Director_Phone2"];
                    if (dtBuilderGroup.Columns.Contains("Director_Phone2") && valueDirector_Phone2 != null && valueDirector_Phone2.ToString() != "")
                        ival_BuilderGroupMaster.Director_Phone2 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Phone2"]);
                    else
                        ival_BuilderGroupMaster.Director_Phone2 = string.Empty;

                    object valueDirector_Phone3 = dtBuilderGroup.Rows[0]["Director_Phone3"];
                    if (dtBuilderGroup.Columns.Contains("Director_Phone3") && valueDirector_Phone3 != null && valueDirector_Phone3.ToString() != "")
                        ival_BuilderGroupMaster.Director_Phone3 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Phone3"]);
                    else
                        ival_BuilderGroupMaster.Director_Phone3 = string.Empty;

                    object valueDirector_Mobile1 = dtBuilderGroup.Rows[0]["Director_Mobile1"];
                    if (dtBuilderGroup.Columns.Contains("Director_Mobile1") && valueDirector_Mobile1 != null && valueDirector_Mobile1.ToString() != "")
                        ival_BuilderGroupMaster.Director_Mobile1 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Mobile1"]);
                    else
                        ival_BuilderGroupMaster.Director_Mobile1 = string.Empty;

                    object valueDirector_Mobile2 = dtBuilderGroup.Rows[0]["Director_Mobile2"];
                    if (dtBuilderGroup.Columns.Contains("Director_Mobile2") && valueDirector_Mobile2 != null && valueDirector_Mobile2.ToString() != "")
                        ival_BuilderGroupMaster.Director_Mobile2 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Mobile2"]);
                    else
                        ival_BuilderGroupMaster.Director_Mobile2 = string.Empty;

                    object valueDirector_Mobile3 = dtBuilderGroup.Rows[0]["Director_Mobile3"];
                    if (dtBuilderGroup.Columns.Contains("Director_Mobile3") && valueDirector_Mobile3 != null && valueDirector_Mobile3.ToString() != "")
                        ival_BuilderGroupMaster.Director_Mobile3 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Mobile3"]);
                    else
                        ival_BuilderGroupMaster.Director_Mobile3 = string.Empty;

                    object valueDirector_Email1 = dtBuilderGroup.Rows[0]["Director_Email1"];
                    if (dtBuilderGroup.Columns.Contains("Director_Email1") && valueDirector_Email1 != null && valueDirector_Email1.ToString() != "")
                        ival_BuilderGroupMaster.Director_Email1 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Email1"]);
                    else
                        ival_BuilderGroupMaster.Director_Email1 = string.Empty;

                    object valueDirector_Email2 = dtBuilderGroup.Rows[0]["Director_Email2"];
                    if (dtBuilderGroup.Columns.Contains("Director_Email2") && valueDirector_Email2 != null && valueDirector_Email2.ToString() != "")
                        ival_BuilderGroupMaster.Director_Email2 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Email2"]);
                    else
                        ival_BuilderGroupMaster.Director_Email2 = string.Empty;

                    object valueDirector_Email3 = dtBuilderGroup.Rows[0]["Director_Email3"];
                    if (dtBuilderGroup.Columns.Contains("Director_Email3") && valueDirector_Email3 != null && valueDirector_Email3.ToString() != "")
                        ival_BuilderGroupMaster.Director_Email3 = Convert.ToString(dtBuilderGroup.Rows[0]["Director_Email3"]);
                    else
                        ival_BuilderGroupMaster.Director_Email3 = string.Empty;

                    object valueBuilder_Grade = dtBuilderGroup.Rows[0]["Builder_Grade"];
                    if (dtBuilderGroup.Columns.Contains("Builder_Grade") && valueBuilder_Grade != null && valueBuilder_Grade.ToString() != "")
                        ival_BuilderGroupMaster.Builder_Grade = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_Grade"]);
                    else
                        ival_BuilderGroupMaster.Builder_Grade = string.Empty;

                    vMBuilderGroupMaster.ival_BuilderGroupMaster = ival_BuilderGroupMaster;

                    object valueStreet = dtBuilderGroup.Rows[0]["Builder_Street"];
                    if (dtBuilderGroup.Columns.Contains("Builder_Street") && valueStreet != null && valueStreet.ToString() != "")
                        ival_BuilderGroupMaster.Builder_Street = Convert.ToString(dtBuilderGroup.Rows[0]["Builder_Street"]);
                    else
                        ival_BuilderGroupMaster.Builder_Street = string.Empty;

                    vMBuilderGroupMaster.ival_BuilderGroupMaster = ival_BuilderGroupMaster;


                    vMBuilderGroupMaster.ival_StatesList = GetState();
                    vMBuilderGroupMaster.PincodeList = GetPincode();
                    vMBuilderGroupMaster.BuilderGroupList = GetBuilderList();
                    vMBuilderGroupMaster.ival_DistrictsList = GetDistrict();
                    vMBuilderGroupMaster.ival_LocalityList = GetLocality();
                    vMBuilderGroupMaster.ival_CitiesList = GetCity();
                    // IEnumerable<ival_Districts> ival_DistList = GetDistByStateId();
                    IEnumerable<ival_States> ival_StatesList = GetState();
                    if (ival_StatesList.ToList().Count > 0 && ival_StatesList != null)
                    {
                        var BuilderState = ival_StatesList.Where(s => s.State_Name == dtBuilderGroup.Rows[0]["Builder_State"].ToString()).Select(s => s.State_ID);

                        vMBuilderGroupMaster.SelectedBuilderStateId = Convert.ToInt32(BuilderState.FirstOrDefault());

                    }

                    /*IEnumerable<ival_LookupCategoryValues> pincodelist = GetPincode();
                    if (pincodelist.ToList().Count > 0 && pincodelist != null)
                    {
                       var builderpostalpincode = pincodelist.Where(s => s.Lookup_Value == dtBuilderGroup.Rows[0]["Builder_Pincode"].ToString()).Select(s => s.Lookup_ID);

                        vMBuilderGroupMaster.SelectedBuilderPincodeID = Convert.ToInt32(builderpostalpincode.FirstOrDefault());

                    }*/
                    IEnumerable<ival_Cities> ival_CitiesList = GetCity();
                    if (ival_CitiesList.ToList().Count > 0 && ival_CitiesList != null)
                    {
                        var buildercity = ival_CitiesList.Where(s => s.City_Name == dtBuilderGroup.Rows[0]["Builder_City"].ToString()).Select(s => s.City_ID);

                        vMBuilderGroupMaster.selectedCityID = Convert.ToInt32(buildercity.FirstOrDefault());

                    }
                    IEnumerable<ival_Districts> ival_DistrictsList = GetDistrict();
                    if (ival_DistrictsList.ToList().Count > 0 && ival_DistrictsList != null)
                    {
                        var BuilderDistrict = ival_DistrictsList.Where(s => s.District_Name == dtBuilderGroup.Rows[0]["Builder_District"].ToString()).Select(s => s.District_ID);

                        vMBuilderGroupMaster.selectedDistID = Convert.ToInt32(BuilderDistrict.FirstOrDefault());

                    }
                    IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                    if (ival_LocalityList.ToList().Count > 0 && ival_LocalityList != null)
                    {
                        var BuilderCity = ival_LocalityList.Where(s => s.Loc_Name == dtBuilderGroup.Rows[0]["Builder_Locality"].ToString()).Select(s => s.Loc_Id);

                        vMBuilderGroupMaster.selectedLocID = Convert.ToInt32(BuilderCity.FirstOrDefault());

                        vMBuilderGroupMaster.SelectedBuilderPincodeID = Convert.ToInt32(BuilderCity.FirstOrDefault());
                    }

                }
            }
            catch (Exception ex)
            {
                //    ex.InnerException.ToString();
                Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
            return View(vMBuilderGroupMaster);
        }
        public ActionResult CompanyMaster()
        {
            vMBuilderGroupMaster = new VMBuilderGroupMaster();
            vMBuilderGroupMaster.ival_StatesList = GetState();
            vMBuilderGroupMaster.PincodeList = GetPincode();
            vMBuilderGroupMaster.BuilderGroupList = GetBuilderList();
            return View(vMBuilderGroupMaster);
        }

        public ActionResult ViewProject(int ? ID)
        {

            try
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@Builder_Company_ID", ID);
                DataTable dtProjects = sqlDataAccess.GetDatatableExecuteStoreProcedure("[usp_GetProjectsbyCompanyID]", hInputPara);

                List<ival_ProjectMaster> listProjects = new List<ival_ProjectMaster>();

                if (dtProjects.Rows.Count > 0 && dtProjects != null)

                {
                    for (int i = 0; i < dtProjects.Rows.Count; i++)
                    {

                        listProjects.Add(new ival_ProjectMaster
                        {
                            Project_ID = Convert.ToInt32(dtProjects.Rows[i]["Project_ID"]),
                            Project_Name = Convert.ToString(dtProjects.Rows[i]["Project_Name"]),
                            Legal_City = Convert.ToString(dtProjects.Rows[i]["Legal_City"]),
                            Legal_State = Convert.ToString(dtProjects.Rows[i]["Legal_State"]),


                        });


                    }
                }
                return View(listProjects);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            } 
        }
        //added by punam
        public ActionResult CompanyMasterNew()
        {
            vMBuilderGroupMaster = new VMBuilderGroupMaster();
            vMBuilderGroupMaster.ival_StatesList = GetState();
            vMBuilderGroupMaster.PincodeList = GetPincode();
            vMBuilderGroupMaster.BuilderGroupList = GetBuilderList();
             vMBuilderGroupMaster.ival_natureOfcompanyList = GetNatureOfCompanyList();
            return View(vMBuilderGroupMaster);
        }

        [HttpPost]
        public ActionResult AddCompanyMasterNew(string str)
        {
            try
            {

                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

                for (int i = 0; i < list1.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();


                    hInputPara.Add("@Builder_Group_Master_ID", Convert.ToString(list1.Rows[i]["BuilderGroupid"]));
                    hInputPara.Add("@Builder_Company_Name", Convert.ToString(list1.Rows[i]["BuilderCompany"]));
                    hInputPara.Add("@Builder_Office_Address1", Convert.ToString(list1.Rows[i]["officeAdd1"]));
                    hInputPara.Add("@Builder_Office_Address2", Convert.ToString(list1.Rows[i]["officeAdd2"]));
                    /*hInputPara.Add("@Builder_Locality", Convert.ToString(list1.Rows[i]["locality"]));*/
                    hInputPara.Add("@Builder_Nearby_LandMark", Convert.ToString(list1.Rows[i]["nearByLandmark"]));
                    hInputPara.Add("@Builder_C_District", Convert.ToString(list1.Rows[i]["ddlDistrict"]));
                    hInputPara.Add("@Builder_C_City", Convert.ToString(list1.Rows[i]["ddlCity"]));
                    hInputPara.Add("@Builder_C_State", Convert.ToString(list1.Rows[i]["ddlstate"]));
                    hInputPara.Add("@Builder_C_Loc", Convert.ToString(list1.Rows[i]["ddlLoc"]));
                    hInputPara.Add("@Builder_C_Pincode", Convert.ToString(list1.Rows[i]["ddlPincode"]));
                    hInputPara.Add("@Nature_Of_Company", Convert.ToString(list1.Rows[i]["ddlNatureofCompany"]));
                    hInputPara.Add("@Director_Name1", Convert.ToString(list1.Rows[i]["DirectorName1"]));
                    hInputPara.Add("@Director_Phone1", Convert.ToString(list1.Rows[i]["DirectorPhone1"]));
                    hInputPara.Add("@Director_Mobile1", Convert.ToString(list1.Rows[i]["DirectorMobile1"]));
                    hInputPara.Add("@Director_EmailId1", Convert.ToString(list1.Rows[i]["DirectorEmail1"]));
                    hInputPara.Add("@Director_Name2", Convert.ToString(list1.Rows[i]["DirectorName2"]));
                    hInputPara.Add("@Director_Phone2", Convert.ToString(list1.Rows[i]["DirectorPhone2"]));
                    hInputPara.Add("@Director_Mobile2", Convert.ToString(list1.Rows[i]["DirectorMobile2"]));
                    hInputPara.Add("@Director_EmailId2", Convert.ToString(list1.Rows[i]["DirectorEmail2"]));
                    hInputPara.Add("@Director_Name3", Convert.ToString(list1.Rows[i]["DirectorName3"]));

                    hInputPara.Add("@Director_Phone3", Convert.ToString(list1.Rows[i]["DirectorPhone3"]));
                    hInputPara.Add("@Director_Mobile3", Convert.ToString(list1.Rows[i]["DirectorMobile3"]));


                    hInputPara.Add("@Director_EmailId3", Convert.ToString(list1.Rows[i]["DirectorEmail3"]));
                    hInputPara.Add("@Builder_C_Street", Convert.ToString(list1.Rows[i]["street"]));
                    hInputPara.Add("@Builder_Grade", Convert.ToString(list1.Rows[i]["BuilderGrade"]));

                    string result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertBuilderCompanyMasterNew", hInputPara);

                    return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception e)
            {
                e.InnerException.ToString();
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
            return Json("");
        }

        /*[HttpPost]
        public ActionResult CompanyMasterNew(VMBuilderGroupMaster vm)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            hInputPara = new Hashtable();

            IEnumerable<ival_States> ival_StatesList = GetState();
            var builderState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vm.SelectedBuilderStateId).Select(row => row.State_Name.ToString());

            IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
            var builderPincode = ival_LocalityList.AsEnumerable().Where(row => row.Loc_Id == vm.SelectedBuilderPincodeID).Select(row => row.Pin_Code.ToString());
            var builderCity = ival_LocalityList.AsEnumerable().Where(row => row.Loc_Id == vm.selectedCityID).Select(row => row.Loc_Name.ToString());

            IEnumerable<ival_Districts> ival_Districtlist = GetDistrict();
            var builderDistrict = ival_Districtlist.AsEnumerable().Where(row => row.District_ID == vm.selectedDistID).Select(row => row.District_Name.ToString());

            //IEnumerable<ival_Cities> ival_Citylist = GetLocality();




            hInputPara.Add("@Builder_Group_Master_ID", vm.SelectedBuilderGroupMasterID);
            hInputPara.Add("@Builder_Company_Name", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Builder_Company_Name));
            hInputPara.Add("@Builder_Office_Address1", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Builder_Office_Address1));
            hInputPara.Add("@Builder_Office_Address2", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Builder_Office_Address2));
            hInputPara.Add("@Builder_C_Street", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Builder_C_Street));
            hInputPara.Add("@Builder_C_Locality", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Builder_C_Locality));
            hInputPara.Add("@Builder_C_City", builderCity.FirstOrDefault());
            hInputPara.Add("@Builder_C_District", builderDistrict.FirstOrDefault());
            hInputPara.Add("@Builder_C_State", builderState.FirstOrDefault());
            hInputPara.Add("@Builder_C_Pincode", builderPincode.FirstOrDefault());
            hInputPara.Add("@Builder_Nearby_LandMark", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Builder_Nearby_LandMark));
            hInputPara.Add("@Nature_Of_Company", vm.SelectedBuilderNatureOfCom);
            hInputPara.Add("@Director_Name1", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_Name1));
            hInputPara.Add("@Director_Phone1", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_Phone1));
            hInputPara.Add("@Director_Mobile1", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_Mobile1));
            hInputPara.Add("@Director_EmailId1", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_EmailId1));
            hInputPara.Add("@Director_Name2", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_Name2));
            hInputPara.Add("@Director_Phone2", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_Phone2));
            hInputPara.Add("@Director_Mobile2", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_Mobile2));
            hInputPara.Add("@Director_EmailId2", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_EmailId2));
            hInputPara.Add("@Director_Name3", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_Name3));
            hInputPara.Add("@Director_Phone3", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_Phone3));
            hInputPara.Add("@Director_Mobile3", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_Mobile3));
            hInputPara.Add("@Director_EmailId3", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_EmailId3));
            hInputPara.Add("@Builder_Grade", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Builder_Grade));

            var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertBuilderCompanyMasterNew", hInputPara);
            if (Convert.ToInt32(result) == 1)
            {
                 return RedirectToAction("CompanyIndex", "BuilderAndCompany");
               // return RedirectToAction("ViewCompany", new RouteValueDictionary(new { controller = "BuilderAndCompany", action = "ViewCompany", Id = vMBuilderGroupMaster.SelectedBuilderGroupMasterID }));
            }
            else
            {
                return Json("Error in Database !", JsonRequestBehavior.AllowGet);
            }

        }*/
        public ActionResult AddCompanyMaster(VMBuilderGroupMaster vMBuilderGroupMaster)
        {

            try
            {

                IEnumerable<ival_States> ival_StatesList = GetState();
                var companyState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMBuilderGroupMaster.SelectedBuilderCompanyStateID).Select(row => row.State_Name.ToString());
                var postalState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMBuilderGroupMaster.SelectedBuilderPostalStateID).Select(row => row.State_Name.ToString());

                IEnumerable<ival_LookupCategoryValues> pincodeList = GetPincode();
                var companyPincode = pincodeList.AsEnumerable().Where(row => row.Lookup_ID == vMBuilderGroupMaster.SelectedBuilderCompanyPincodeID).Select(row => row.Lookup_Value.ToString());
                var postalPincode = pincodeList.AsEnumerable().Where(row => row.Lookup_ID == vMBuilderGroupMaster.SelectedBuilderPostalPincodeID).Select(row => row.Lookup_Value.ToString());


                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                hInputPara = new Hashtable();

                hInputPara.Add("@Builder_Group_Master_ID", Convert.ToString(vMBuilderGroupMaster.SelectedBuilderGroupMasterID));
                hInputPara.Add("@Builder_Company_Name", Convert.ToString(vMBuilderGroupMaster.ival_BuilderCompanyMaster.Builder_Company_Name));
                hInputPara.Add("@Builder_Legal_Address1", Convert.ToString(vMBuilderGroupMaster.ival_BuilderCompanyMaster.Builder_Legal_Address1));
                hInputPara.Add("@Builder_Legal_Address2", Convert.ToString(vMBuilderGroupMaster.ival_BuilderCompanyMaster.Builder_Legal_Address2));
                hInputPara.Add("@Builder_C_City", Convert.ToString(vMBuilderGroupMaster.ival_BuilderCompanyMaster.Builder_C_City));
                hInputPara.Add("@Builder_C_District", Convert.ToString(vMBuilderGroupMaster.ival_BuilderCompanyMaster.Builder_C_District));
                hInputPara.Add("@Builder_C_State", companyState.FirstOrDefault());
                hInputPara.Add("@Builder_C_Pincode", companyPincode.FirstOrDefault());
                hInputPara.Add("@Builder_Postal_Address1", Convert.ToString(vMBuilderGroupMaster.ival_BuilderCompanyMaster.Builder_Postal_Address1));
                hInputPara.Add("@Builder_Postal_Address2", Convert.ToString(vMBuilderGroupMaster.ival_BuilderCompanyMaster.Builder_Postal_Address2));
                hInputPara.Add("@Builder_Postal_City", Convert.ToString(vMBuilderGroupMaster.ival_BuilderCompanyMaster.Builder_Postal_City));
                hInputPara.Add("@Builder_Postal_State", postalState.FirstOrDefault());
                hInputPara.Add("@Builder_Postal_Pincode", postalPincode.FirstOrDefault());
                hInputPara.Add("@Created_By", string.Empty);
                var result = sqlDataAccess.ExecuteStoreProcedure("[usp_InsertBuilderCompanyMaster]", hInputPara);

                if (Convert.ToInt32(result) == 1)
                {
                   // return RedirectToAction("Index", "BuilderAndCompany");
                    return RedirectToAction("ViewCompany", new RouteValueDictionary(new { controller = "BuilderAndCompany", action = "ViewCompany", Id = vMBuilderGroupMaster.SelectedBuilderGroupMasterID }));
                }
                else
                {
                    return Json("Error in Database !", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                //e.InnerException.ToString();
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult GetBuilderComapniesForGroup(int?id)
        {
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult EditCompanyMaster(int?ID)
        {

            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //    return view();
            }
            vMBuilderGroupMaster = new VMBuilderGroupMaster();
            ival_BuilderCompanyMaster = new ival_BuilderCompanyMaster();

            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Builder_Company_Master_ID", ID);
                DataTable dtBuilderCompany = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuilderCompanyMasterDetailsByID", hInputPara);

                if (dtBuilderCompany.Rows.Count > 0 && dtBuilderCompany!=null)
                {
                    

                    object valueBuilder_Company_Master_ID = dtBuilderCompany.Rows[0]["Builder_Company_Master_ID"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Company_Master_ID") && valueBuilder_Company_Master_ID != null && valueBuilder_Company_Master_ID.ToString()!="")
                        ival_BuilderCompanyMaster.Builder_Company_Master_ID = Convert.ToInt32(dtBuilderCompany.Rows[0]["Builder_Company_Master_ID"]);
                    else
                        ival_BuilderCompanyMaster.Builder_Company_Master_ID = 0;

                    object valueBuilder_Group_Master_ID = dtBuilderCompany.Rows[0]["Builder_Group_Master_ID"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Group_Master_ID") && valueBuilder_Group_Master_ID != null && valueBuilder_Group_Master_ID.ToString() != "")
                    {
                        ival_BuilderCompanyMaster.Builder_Group_Master_ID = Convert.ToInt32(dtBuilderCompany.Rows[0]["Builder_Group_Master_ID"]);
                        vMBuilderGroupMaster.SelectedBuilderGroupMasterID = Convert.ToInt32(dtBuilderCompany.Rows[0]["Builder_Group_Master_ID"]);
                    }
                    else
                    {
                        ival_BuilderCompanyMaster.Builder_Group_Master_ID = 0;
                        vMBuilderGroupMaster.SelectedBuilderGroupMasterID = 0; ;
                    }

                    object valueBuilder_Company_Name = dtBuilderCompany.Rows[0]["Builder_Company_Name"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Company_Name") && valueBuilder_Company_Name != null && valueBuilder_Company_Name.ToString() != "")
                        ival_BuilderCompanyMaster.Builder_Company_Name = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Company_Name"]);
                    else
                        ival_BuilderCompanyMaster.Builder_Company_Name = string.Empty;

                    object valueBuilder_Legal_Address1 = dtBuilderCompany.Rows[0]["Builder_Legal_Address1"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Legal_Address1") && valueBuilder_Legal_Address1 != null && valueBuilder_Legal_Address1.ToString() != "")
                        ival_BuilderCompanyMaster.Builder_Legal_Address1 = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Legal_Address1"]);
                    else
                        ival_BuilderCompanyMaster.Builder_Legal_Address1 = string.Empty;

                    object valueBuilder_Legal_Address2 = dtBuilderCompany.Rows[0]["Builder_Legal_Address2"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Legal_Address2") && valueBuilder_Legal_Address2 != null && valueBuilder_Legal_Address2.ToString() != "")
                        ival_BuilderCompanyMaster.Builder_Legal_Address2 = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Legal_Address2"]);
                    else
                        ival_BuilderCompanyMaster.Builder_Legal_Address2 = string.Empty;

                    object valueBuilder_C_City = dtBuilderCompany.Rows[0]["Builder_C_City"];
                    if (dtBuilderCompany.Columns.Contains("Builder_C_City") && valueBuilder_C_City != null && valueBuilder_C_City.ToString() != "")
                        ival_BuilderCompanyMaster.Builder_C_City = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_C_City"]);
                    else
                        ival_BuilderCompanyMaster.Builder_C_City = string.Empty;

                    object valueBuilder_C_District = dtBuilderCompany.Rows[0]["Builder_C_District"];
                    if (dtBuilderCompany.Columns.Contains("Builder_C_District") && valueBuilder_C_District != null && valueBuilder_C_District.ToString() != "")
                        ival_BuilderCompanyMaster.Builder_C_District = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_C_District"]);
                    else
                        ival_BuilderCompanyMaster.Builder_C_District = string.Empty;

                    object valueBuilder_C_State = dtBuilderCompany.Rows[0]["Builder_C_State"];
                    if (dtBuilderCompany.Columns.Contains("Builder_C_State") && valueBuilder_C_State != null && valueBuilder_C_State.ToString() != "")
                        ival_BuilderCompanyMaster.Builder_C_State = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_C_State"]);
                    else
                        ival_BuilderCompanyMaster.Builder_C_State = string.Empty;

                    object valueBuilder_C_Pincode = dtBuilderCompany.Rows[0]["Builder_C_Pincode"];
                    if (dtBuilderCompany.Columns.Contains("Builder_C_Pincode") && valueBuilder_C_Pincode != null && valueBuilder_C_Pincode.ToString() != "")
                        ival_BuilderCompanyMaster.Builder_C_Pincode = Convert.ToInt32(dtBuilderCompany.Rows[0]["Builder_C_Pincode"]);
                    else
                        ival_BuilderCompanyMaster.Builder_C_Pincode = 0;

                    object valueBuilder_Postal_Address1 = dtBuilderCompany.Rows[0]["Builder_Postal_Address1"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Postal_Address1") && valueBuilder_Postal_Address1 != null && valueBuilder_Postal_Address1.ToString() != "")
                        ival_BuilderCompanyMaster.Builder_Postal_Address1 = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Postal_Address1"]);
                    else
                        ival_BuilderCompanyMaster.Builder_Postal_Address1 = string.Empty;

                    object valueBuilder_Postal_Address2 = dtBuilderCompany.Rows[0]["Builder_Postal_Address2"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Postal_Address2") && valueBuilder_Postal_Address2 != null && valueBuilder_Postal_Address2.ToString() != "")
                        ival_BuilderCompanyMaster.Builder_Postal_Address2 = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Postal_Address2"]);
                    else
                        ival_BuilderCompanyMaster.Builder_Postal_Address2 = string.Empty;

                    object valueBuilder_Postal_City = dtBuilderCompany.Rows[0]["Builder_Postal_City"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Postal_City") && valueBuilder_Postal_City != null && valueBuilder_Postal_City.ToString() != "")
                        ival_BuilderCompanyMaster.Builder_Postal_City = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Postal_City"]);
                    else
                        ival_BuilderCompanyMaster.Builder_Postal_City = string.Empty;

                    object valueBuilder_Postal_District = dtBuilderCompany.Rows[0]["Builder_Postal_District"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Postal_District") && valueBuilder_Postal_District != null && valueBuilder_Postal_District.ToString() != "")
                        ival_BuilderCompanyMaster.Builder_Postal_District = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Postal_District"]);
                    else
                        ival_BuilderCompanyMaster.Builder_Postal_District = string.Empty;

                    object valueBuilder_Postal_State = dtBuilderCompany.Rows[0]["Builder_Postal_State"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Postal_State") && valueBuilder_Postal_State != null && valueBuilder_Postal_State.ToString() != "")
                        ival_BuilderCompanyMaster.Builder_Postal_State = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Postal_State"]);
                    else
                        ival_BuilderCompanyMaster.Builder_Postal_State = string.Empty;

                    object valueBuilder_Postal_Pincode = dtBuilderCompany.Rows[0]["Builder_Postal_Pincode"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Postal_Pincode") && valueBuilder_Postal_Pincode.ToString() != "" && valueBuilder_Postal_Pincode.ToString() != "")
                        ival_BuilderCompanyMaster.Builder_Postal_Pincode = Convert.ToInt32(dtBuilderCompany.Rows[0]["Builder_Postal_Pincode"]);
                    else
                        ival_BuilderCompanyMaster.Builder_Postal_Pincode = 0;

                    vMBuilderGroupMaster.ival_BuilderCompanyMaster = ival_BuilderCompanyMaster;

                    vMBuilderGroupMaster.ival_StatesList = GetState();
                    vMBuilderGroupMaster.PincodeList = GetPincode();
                    vMBuilderGroupMaster.BuilderGroupList = GetBuilderList();

                    IEnumerable<ival_States> ival_StatesList = GetState();
                    if (ival_StatesList.ToList().Count > 0 && ival_StatesList != null)
                    {
                        var BuilderCState = ival_StatesList.Where(s => s.State_Name == dtBuilderCompany.Rows[0]["Builder_C_State"].ToString()).Select(s => s.State_ID);
                        var BuilderPostalState = ival_StatesList.Where(s => s.State_Name == dtBuilderCompany.Rows[0]["Builder_Postal_State"].ToString()).Select(s => s.State_ID);

                        vMBuilderGroupMaster.SelectedBuilderCompanyStateID = Convert.ToInt32(BuilderCState.FirstOrDefault());
                        vMBuilderGroupMaster.SelectedBuilderPostalStateID = Convert.ToInt32(BuilderPostalState.FirstOrDefault());
                    }

                    IEnumerable<ival_LookupCategoryValues> pincodeList = GetPincode();
                    if (pincodeList.ToList().Count > 0 && pincodeList != null)
                    {
                       
                        var BuilderCPincode = pincodeList.Where(s => s.Lookup_Value == dtBuilderCompany.Rows[0]["Builder_C_Pincode"].ToString()).Select(s => s.Lookup_ID);
                        var BuilderState = pincodeList.Where(s => s.Lookup_Value == dtBuilderCompany.Rows[0]["Builder_Postal_Pincode"].ToString()).Select(s => s.Lookup_ID);

                        vMBuilderGroupMaster.SelectedBuilderCompanyPincodeID = Convert.ToInt32(BuilderCPincode.FirstOrDefault());
                        vMBuilderGroupMaster.SelectedBuilderPostalPincodeID = Convert.ToInt32(BuilderState.FirstOrDefault());
                    }
                    vMBuilderGroupMaster.ival_BuilderCompanyMaster = ival_BuilderCompanyMaster;


                }


            }
            catch (Exception ex)
            {
                //    ex.InnerException.ToString();
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
            return View(vMBuilderGroupMaster);

        }
        [HttpPost]
        public ActionResult EditCompanyMaster(VMBuilderGroupMaster vMBuilderGroupMaster)
        {
            try
            {

                IEnumerable<ival_States> ival_StatesList = GetState();
                
                var companyState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMBuilderGroupMaster.SelectedBuilderCompanyStateID).Select(row => row.State_Name.ToString());
                var postalState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMBuilderGroupMaster.SelectedBuilderPostalStateID).Select(row => row.State_Name.ToString());

                IEnumerable<ival_LookupCategoryValues> pincodeList = GetPincode();
                var companyPincode = pincodeList.AsEnumerable().Where(row => row.Lookup_ID == vMBuilderGroupMaster.SelectedBuilderCompanyPincodeID).Select(row => row.Lookup_Value.ToString());
                var postalPincode = pincodeList.AsEnumerable().Where(row => row.Lookup_ID == vMBuilderGroupMaster.SelectedBuilderPostalPincodeID).Select(row => row.Lookup_Value.ToString());


                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                hInputPara = new Hashtable();
                hInputPara.Add("@Builder_Company_Master_ID", Convert.ToString(vMBuilderGroupMaster.ival_BuilderCompanyMaster.Builder_Company_Master_ID));
                hInputPara.Add("@Builder_Group_Master_ID", Convert.ToString(vMBuilderGroupMaster.SelectedBuilderGroupMasterID));
                hInputPara.Add("@Builder_Company_Name", Convert.ToString(vMBuilderGroupMaster.ival_BuilderCompanyMaster.Builder_Company_Name));
                hInputPara.Add("@Builder_Legal_Address1", Convert.ToString(vMBuilderGroupMaster.ival_BuilderCompanyMaster.Builder_Legal_Address1));
                hInputPara.Add("@Builder_Legal_Address2", Convert.ToString(vMBuilderGroupMaster.ival_BuilderCompanyMaster.Builder_Legal_Address2));
                hInputPara.Add("@Builder_C_City", Convert.ToString(vMBuilderGroupMaster.ival_BuilderCompanyMaster.Builder_C_City));
                hInputPara.Add("@Builder_C_District", Convert.ToString(vMBuilderGroupMaster.ival_BuilderCompanyMaster.Builder_C_District));
                hInputPara.Add("@Builder_C_State", companyState.FirstOrDefault());
                hInputPara.Add("@Builder_C_Pincode", companyPincode.FirstOrDefault());
                hInputPara.Add("@Builder_Postal_Address1", Convert.ToString(vMBuilderGroupMaster.ival_BuilderCompanyMaster.Builder_Postal_Address1));
                hInputPara.Add("@Builder_Postal_Address2", Convert.ToString(vMBuilderGroupMaster.ival_BuilderCompanyMaster.Builder_Postal_Address2));
                hInputPara.Add("@Builder_Postal_City", Convert.ToString(vMBuilderGroupMaster.ival_BuilderCompanyMaster.Builder_Postal_City));
                hInputPara.Add("@Builder_Postal_State", postalState.FirstOrDefault());
                hInputPara.Add("@Builder_Postal_Pincode", postalPincode.FirstOrDefault());
                hInputPara.Add("@Updated_By", string.Empty);

                var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateBuilderCompanyMasterDetailsByID", hInputPara);

                if (Convert.ToInt32(result) == 1)
                {
                    //return RedirectToAction("ViewCompany", "BuilderAndCompany", vMBuilderGroupMaster.SelectedBuilderGroupMasterID);
                    return RedirectToAction("ViewCompany", new RouteValueDictionary( new { controller = "BuilderAndCompany", action = "ViewCompany", Id = vMBuilderGroupMaster.SelectedBuilderGroupMasterID }));
                }
                else
                {
                    return Json("Error in Database !", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                //e.InnerException.ToString();
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public ActionResult EditCompanyMasterNew(int? ID)
        {

            vMBuilderGroupMaster = new VMBuilderGroupMaster();
            ival_BuilderCompanyMasterNew = new ival_BuilderCompanyMasterNew();
            vMBuilderGroupMaster.ival_StatesList = GetState();
            vMBuilderGroupMaster.PincodeList = GetPincode();
            vMBuilderGroupMaster.BuilderGroupList = GetBuilderList();
           vMBuilderGroupMaster.ival_natureOfcompanyList=GetNatureOfCompanyList();
          
            vMBuilderGroupMaster.ival_DistrictsList = GetDistrict();
            vMBuilderGroupMaster.ival_LocalityList = GetLocality();
            vMBuilderGroupMaster.ival_CitiesList = GetCity();
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //    return view();
            }
           
          

            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Builder_Company_Master_ID", ID);
                DataTable dtBuilderCompany = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuilderCompanyMasterNewDetails", hInputPara);
                if (dtBuilderCompany.Rows.Count > 0 && dtBuilderCompany != null)
                {


                    object valueBuilder_Company_Master_ID = dtBuilderCompany.Rows[0]["Builder_Company_Master_ID"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Company_Master_ID") && valueBuilder_Company_Master_ID != null && valueBuilder_Company_Master_ID.ToString() != "")
                        vMBuilderGroupMaster.Builder_Company_Master_IDl = Convert.ToInt32(dtBuilderCompany.Rows[0]["Builder_Company_Master_ID"]);
                    else
                        vMBuilderGroupMaster.Builder_Company_Master_IDl = 0;

                    object valueBuilder_Group_Master_ID = dtBuilderCompany.Rows[0]["Builder_Group_Master_ID"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Group_Master_ID") && valueBuilder_Group_Master_ID != null && valueBuilder_Group_Master_ID.ToString() != "")
                    {
                        ival_BuilderCompanyMasterNew.Builder_Group_Master_ID = Convert.ToInt32(dtBuilderCompany.Rows[0]["Builder_Group_Master_ID"]);
                        vMBuilderGroupMaster.SelectedBuilderGroupMasterID = Convert.ToInt32(dtBuilderCompany.Rows[0]["Builder_Group_Master_ID"]);
                    }
                    else
                    {
                        ival_BuilderCompanyMasterNew.Builder_Group_Master_ID = 0;
                        vMBuilderGroupMaster.SelectedBuilderGroupMasterID = 0; ;
                    }
                    object valueBuilder_NatureOfCom = dtBuilderCompany.Rows[0]["Nature_Of_Company"];
                    if (dtBuilderCompany.Columns.Contains("Nature_Of_Company") && valueBuilder_NatureOfCom != null && valueBuilder_NatureOfCom.ToString() != "")
                    {
                        //ival_BuilderCompanyMasterNew.Builder_Group_Master_ID = Convert.ToInt32(dtBuilderCompany.Rows[0]["Builder_Group_Master_ID"]);
                        vMBuilderGroupMaster.SelectedBuilderNatureOfCom = Convert.ToString(dtBuilderCompany.Rows[0]["Nature_Of_Company"]);
                    }
                    else
                    {
                        //ival_BuilderCompanyMasterNew.Builder_Group_Master_ID = 0;
                        vMBuilderGroupMaster.SelectedBuilderNatureOfCom = string.Empty; ;
                    }

                    object valueBuilder_Company_Name = dtBuilderCompany.Rows[0]["Builder_Company_Name"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Company_Name") && valueBuilder_Company_Name != null && valueBuilder_Company_Name.ToString() != "")
                     //ival_BuilderCompanyMasterNew.Builder_Company_Name = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Company_Name"]);
                      
                    ViewBag.companyName = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Company_Name"]);
                    else
                        ViewBag.companyName = string.Empty;
                    object valueBuilder_officeadd1 = dtBuilderCompany.Rows[0]["Builder_Office_Address1"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Office_Address1") && valueBuilder_officeadd1 != null && valueBuilder_officeadd1.ToString() != "")
                    
                        //ival_BuilderCompanyMaster.Builder_Company_Name = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Company_Name"]);
                        ViewBag.officeadd1 = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Office_Address1"]);
                       // vMBuilderGroupMaster.ival_BuilderCompanyMasterNew.Builder_Office_Address1 = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Office_Address1"]);                    

                    else
                        ViewBag.officeadd1 = string.Empty;

                    object valueBuilder_officeadd2 = dtBuilderCompany.Rows[0]["Builder_Office_Address2"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Office_Address2") && valueBuilder_officeadd2 != null && valueBuilder_officeadd2.ToString() != "")
                        
                        ViewBag.officeadd2 = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Office_Address2"]);
                    else
                        ViewBag.officeadd2 = string.Empty;

                    object valueBuilder_street = dtBuilderCompany.Rows[0]["Builder_C_Street"];
                    if (dtBuilderCompany.Columns.Contains("Builder_C_Street") && valueBuilder_street != null && valueBuilder_street.ToString() != "")
                        
                        ViewBag.street = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_C_Street"]);
                    else
                        ViewBag.street = string.Empty;

                    /*object valueBuilder_locality = dtBuilderCompany.Rows[0]["Builder_C_Locality"];
                    if (dtBuilderCompany.Columns.Contains("Builder_C_Locality") && valueBuilder_locality != null && valueBuilder_locality.ToString() != "")

                        ViewBag.locality = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_C_Locality"]);
                    else
                        ViewBag.locality = string.Empty;*/

                    object valueBuilder_NearbyLandMark= dtBuilderCompany.Rows[0]["Builder_Nearby_LandMark"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Nearby_LandMark") && valueBuilder_NearbyLandMark != null && valueBuilder_NearbyLandMark.ToString() != "")

                        ViewBag.nearbyLand = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Nearby_LandMark"]);
                    else
                        ViewBag.nearbyLand = string.Empty;

                    object valueBuilder_directorName1 = dtBuilderCompany.Rows[0]["Director_Name1"];
                    if (dtBuilderCompany.Columns.Contains("Director_Name1") && valueBuilder_directorName1 != null && valueBuilder_directorName1.ToString() != "")

                        ViewBag.directorName1 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_Name1"]);
                    else
                        ViewBag.directorName1 = string.Empty;
                    object valueBuilder_directorName2 = dtBuilderCompany.Rows[0]["Director_Name2"];
                    if (dtBuilderCompany.Columns.Contains("Director_Name2") && valueBuilder_directorName2 != null && valueBuilder_directorName2.ToString() != "")

                        ViewBag.directorName2 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_Name2"]);
                    else
                        ViewBag.directorName2 = string.Empty;
                    object valueBuilder_directorName3 = dtBuilderCompany.Rows[0]["Director_Name3"];
                    if (dtBuilderCompany.Columns.Contains("Director_Name3") && valueBuilder_directorName3 != null && valueBuilder_directorName3.ToString() != "")

                        ViewBag.directorName3 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_Name3"]);
                    else
                        ViewBag.directorName3 = string.Empty;

                    object valueBuilder_directorphone1 = dtBuilderCompany.Rows[0]["Director_Phone1"];
                    if (dtBuilderCompany.Columns.Contains("Director_Phone1") && valueBuilder_directorphone1 != null && valueBuilder_directorphone1.ToString() != "")

                        ViewBag.directorphone1 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_Phone1"]);
                    else
                        ViewBag.directorphone1 = string.Empty;

                  
                    object valueBuilder_directorphone2 = dtBuilderCompany.Rows[0]["Director_Phone2"];
                    if (dtBuilderCompany.Columns.Contains("Director_Phone2") && valueBuilder_directorphone2 != null && valueBuilder_directorphone2.ToString() != "")

                        ViewBag.directorphone2 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_Phone2"]);
                    else
                        ViewBag.directorphone2 = string.Empty;
                    object valueBuilder_directorphone3 = dtBuilderCompany.Rows[0]["Director_Phone3"];
                    if (dtBuilderCompany.Columns.Contains("Director_Phone3") && valueBuilder_directorphone3 != null && valueBuilder_directorphone3.ToString() != "")

                        ViewBag.directorphone3 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_Phone3"]);
                    else
                        ViewBag.directorphone3 = string.Empty;

                    object valueBuilder_directormobile1 = dtBuilderCompany.Rows[0]["Director_Mobile1"];
                    if (dtBuilderCompany.Columns.Contains("Director_Mobile1") && valueBuilder_directormobile1 != null && valueBuilder_directormobile1.ToString() != "")

                        ViewBag.directorMobile1 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_Mobile1"]);
                    else
                        ViewBag.directorMobile1 = string.Empty;

                    object valueBuilder_directormobile2 = dtBuilderCompany.Rows[0]["Director_Mobile2"];
                    if (dtBuilderCompany.Columns.Contains("Director_Mobile2") && valueBuilder_directormobile2 != null && valueBuilder_directormobile2.ToString() != "")

                        ViewBag.directorMobile2 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_Mobile2"]);
                    else
                        ViewBag.directorMobile2 = string.Empty;


                    object valueBuilder_directormobile3 = dtBuilderCompany.Rows[0]["Director_Mobile3"];
                    if (dtBuilderCompany.Columns.Contains("Director_Mobile3") && valueBuilder_directormobile3 != null && valueBuilder_directormobile3.ToString() != "")

                        ViewBag.directorMobile3 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_Mobile3"]);
                    else
                        ViewBag.directorMobile3 = string.Empty;

                    object valueBuilder_directoremail1 = dtBuilderCompany.Rows[0]["Director_EmailId1"];
                    if (dtBuilderCompany.Columns.Contains("Director_EmailId1") && valueBuilder_directoremail1 != null && valueBuilder_directoremail1.ToString() != "")

                        ViewBag.directorEmail1 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_EmailId1"]);
                    else
                        ViewBag.directorEmail1 = string.Empty;

                    object valueBuilder_directoremail2 = dtBuilderCompany.Rows[0]["Director_EmailId2"];
                    if (dtBuilderCompany.Columns.Contains("Director_EmailId2") && valueBuilder_directoremail2 != null && valueBuilder_directoremail2.ToString() != "")

                        ViewBag.directorEmail2 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_EmailId2"]);
                    else
                        ViewBag.directorEmail2 = string.Empty;

                    object valueBuilder_directoremail3 = dtBuilderCompany.Rows[0]["Director_EmailId3"];
                    if (dtBuilderCompany.Columns.Contains("Director_EmailId3") && valueBuilder_directoremail3 != null && valueBuilder_directoremail3.ToString() != "")

                        ViewBag.directorEmail3 = Convert.ToString(dtBuilderCompany.Rows[0]["Director_EmailId3"]);
                    else
                        ViewBag.directorEmail3 = string.Empty;

                    object valueBuilder_BuilderGrade = dtBuilderCompany.Rows[0]["Builder_Grade"];
                    if (dtBuilderCompany.Columns.Contains("Builder_Grade") && valueBuilder_BuilderGrade != null && valueBuilder_BuilderGrade.ToString() != "")

                        ViewBag.BuilderGrade = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_Grade"]);
                    else
                        ViewBag.BuilderGrade = string.Empty;

                    object valueBuilder_State = dtBuilderCompany.Rows[0]["Builder_C_State"];
                    if (dtBuilderCompany.Columns.Contains("Builder_C_State") && valueBuilder_State != null && valueBuilder_State.ToString() != "")
                        ival_BuilderCompanyMasterNew.Builder_C_State = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_C_State"]);
                    else
                        ival_BuilderCompanyMasterNew.Builder_C_State = string.Empty;

                    object valueBuilder_District = dtBuilderCompany.Rows[0]["Builder_C_District"];
                    if (dtBuilderCompany.Columns.Contains("Builder_C_District") && valueBuilder_District != null && valueBuilder_District.ToString() != "")
                        ival_BuilderCompanyMasterNew.Builder_C_District = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_C_District"]);
                    else
                        ival_BuilderCompanyMasterNew.Builder_C_District = string.Empty;

                    object valueBuilder_City = dtBuilderCompany.Rows[0]["Builder_C_City"];
                    if (dtBuilderCompany.Columns.Contains("Builder_C_City") && valueBuilder_City != null && valueBuilder_City.ToString() != "")
                        ival_BuilderCompanyMasterNew.Builder_C_City = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_C_City"]);
                    else
                        ival_BuilderCompanyMasterNew.Builder_C_City = string.Empty;

                    object valueBuilder_Loc = dtBuilderCompany.Rows[0]["Builder_C_Locality"];
                    if (dtBuilderCompany.Columns.Contains("Builder_C_Locality") && valueBuilder_Loc != null && valueBuilder_Loc.ToString() != "")
                        ival_BuilderCompanyMasterNew.Builder_C_Locality = Convert.ToString(dtBuilderCompany.Rows[0]["Builder_C_Locality"]);
                    else
                        ival_BuilderCompanyMasterNew.Builder_C_Locality = string.Empty;

                    object valueBuilder_Pincode = dtBuilderCompany.Rows[0]["Builder_C_Pincode"];
                    if (dtBuilderCompany.Columns.Contains("Builder_C_Pincode") && valueBuilder_Pincode != null && valueBuilder_Pincode.ToString() != "")
                        ival_BuilderCompanyMasterNew.Builder_C_Pincode =Convert.ToString(dtBuilderCompany.Rows[0]["Builder_C_Pincode"]);
                    else
                        ival_BuilderCompanyMasterNew.Builder_C_Pincode = string.Empty;

                    IEnumerable<ival_States> ival_StatesList = GetState();
                    if (ival_StatesList.ToList().Count > 0 && ival_StatesList != null)
                    {
                        var BuilderState = ival_StatesList.Where(s => s.State_Name == dtBuilderCompany.Rows[0]["Builder_C_State"].ToString()).Select(s => s.State_ID);

                        vMBuilderGroupMaster.SelectedBuilderStateId = Convert.ToInt32(BuilderState.FirstOrDefault());

                    }

                   
                    /*IEnumerable<ival_Cities> ival_pincodelist = GetLocality();
                    if (ival_pincodelist.ToList().Count > 0 && ival_pincodelist != null)
                    {
                        var builderpostalpincode = ival_pincodelist.Where(s => s.Pin_Code == dtBuilderCompany.Rows[0]["Builder_C_Pincode"].ToString()).Select(s => s.Loc_Id);

                        vMBuilderGroupMaster.SelectedBuilderPincodeID = Convert.ToInt32(builderpostalpincode.FirstOrDefault());

                    }*/
                    IEnumerable<ival_Districts> ival_DistrictsList = GetDistrict();
                    if (ival_DistrictsList.ToList().Count > 0 && ival_DistrictsList != null)
                    {
                        var BuilderDistrict = ival_DistrictsList.Where(s => s.District_Name == dtBuilderCompany.Rows[0]["Builder_C_District"].ToString()).Select(s => s.District_ID);

                        vMBuilderGroupMaster.selectedDistID = Convert.ToInt32(BuilderDistrict.FirstOrDefault());

                    }
                    IEnumerable<ival_Cities> ival_CitiesList = GetCity();
                    if (ival_CitiesList.ToList().Count > 0 && ival_CitiesList != null)
                    {
                        var BuilderCity = ival_CitiesList.Where(s => s.City_Name == dtBuilderCompany.Rows[0]["Builder_C_City"].ToString()).Select(s => s.City_ID);

                        vMBuilderGroupMaster.selectedCityID = Convert.ToInt32(BuilderCity.FirstOrDefault());
                        
                    }

                    IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                    if (ival_LocalityList.ToList().Count > 0 && ival_LocalityList != null)
                    {
                        var BuilderLoc = ival_LocalityList.Where(s => s.Loc_Name == dtBuilderCompany.Rows[0]["Builder_C_Locality"].ToString()).Select(s => s.Loc_Id);

                        vMBuilderGroupMaster.selectedLocID = Convert.ToInt32(BuilderLoc.FirstOrDefault());
                        vMBuilderGroupMaster.SelectedBuilderPincodeID = Convert.ToInt32(BuilderLoc.FirstOrDefault());
                    }

                }

            }
            catch (Exception e)
            {

            }

                return View(vMBuilderGroupMaster);



        }
        [HttpPost]
        public ActionResult UpdateCompanyMasterNew(string str)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            hInputPara = new Hashtable();

            
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

            for (int i = 0; i < list1.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                Hashtable hInputPara1 = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                //hInputPara1.Add("@builderGroupName", Convert.ToString(list1.Rows[i]["groupId"]));
                //hInputPara1.Add("@Builder_Company_Master_ID", Convert.ToString(list1.Rows[i]["companyId"]));

                //var result1 = sqlDataAccess.ExecuteStoreProcedure("usp_GetBuilderGroupNamebygroupId", hInputPara1);
                hInputPara.Add("@Builder_Company_Master_ID", Convert.ToInt32(list1.Rows[i]["Company_Master_Id"]));
                hInputPara.Add("@Builder_Group_Master_ID", Convert.ToInt32(list1.Rows[i]["BuilderGroupId"]));
                hInputPara.Add("@Builder_Company_Name", Convert.ToString(list1.Rows[i]["BuilderCompany"]));
                hInputPara.Add("@Builder_Office_Address1", Convert.ToString(list1.Rows[i]["officeAdd1"]));
                hInputPara.Add("@Builder_Office_Address2", Convert.ToString(list1.Rows[i]["officeAdd2"]));
                hInputPara.Add("@Builder_C_Street", Convert.ToString(list1.Rows[i]["street"]));
                hInputPara.Add("@Builder_C_Locality", Convert.ToString(list1.Rows[i]["ddlLoc"]));
                hInputPara.Add("@Builder_C_City", Convert.ToString(list1.Rows[i]["ddlCity"]));
                hInputPara.Add("@Builder_C_District", Convert.ToString(list1.Rows[i]["ddlDistrict"]));
                hInputPara.Add("@Builder_C_State", Convert.ToString(list1.Rows[i]["ddlstate"]));
                hInputPara.Add("@Builder_C_Pincode", Convert.ToString(list1.Rows[i]["ddlPincode"]));
                hInputPara.Add("@Builder_Nearby_LandMark", Convert.ToString(list1.Rows[i]["nearByLandmark"]));
                hInputPara.Add("@Nature_Of_Company", Convert.ToString(list1.Rows[i]["ddlNatureofCompany"]));
                hInputPara.Add("@Director_Name1", Convert.ToString(list1.Rows[i]["DirectorName1"]));
                hInputPara.Add("@Director_Phone1", Convert.ToString(list1.Rows[i]["DirectorPhone1"]));
                hInputPara.Add("@Director_Mobile1", Convert.ToString(list1.Rows[i]["DirectorMobile1"]));
                hInputPara.Add("@Director_EmailId1", Convert.ToString(list1.Rows[i]["DirectorEmail1"]));
                hInputPara.Add("@Director_Name2", Convert.ToString(list1.Rows[i]["DirectorName2"]));
                hInputPara.Add("@Director_Phone2", Convert.ToString(list1.Rows[i]["DirectorPhone2"]));
                hInputPara.Add("@Director_Mobile2", Convert.ToString(list1.Rows[i]["DirectorMobile2"]));
                hInputPara.Add("@Director_EmailId2", Convert.ToString(list1.Rows[i]["DirectorEmail2"]));
                hInputPara.Add("@Director_Name3", Convert.ToString(list1.Rows[i]["DirectorName3"]));
                hInputPara.Add("@Director_Phone3", Convert.ToString(list1.Rows[i]["DirectorPhone3"]));
                hInputPara.Add("@Director_Mobile3", Convert.ToString(list1.Rows[i]["DirectorMobile3"]));
                hInputPara.Add("@Director_EmailId3", Convert.ToString(list1.Rows[i]["DirectorEmail3"]));
                hInputPara.Add("@Builder_Grade", Convert.ToString(list1.Rows[i]["BuilderGrade"]));
                hInputPara.Add("@Updated_By", string.Empty);

                var result3 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateBuilderCompanyMasterDetailsByIDNew", hInputPara);

                return Json(new { success = true, Message = "Update Successfully" }, JsonRequestBehavior.AllowGet);

            }


            //hInputPara.Add("@Builder_Group_Master_ID", vm.SelectedBuilderGroupMasterID);
            //hInputPara.Add("@Builder_Company_Name", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Builder_Company_Name));
            //hInputPara.Add("@Builder_Office_Address1", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Builder_Office_Address1));
            //hInputPara.Add("@Builder_Office_Address2", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Builder_Office_Address2));
            //hInputPara.Add("@Builder_C_Street", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Builder_C_Street));
            //hInputPara.Add("@Builder_C_Locality", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Builder_C_Locality));
            //hInputPara.Add("@Builder_C_City", builderCity.FirstOrDefault());
            //hInputPara.Add("@Builder_C_District", builderDistrict.FirstOrDefault());
            //hInputPara.Add("@Builder_C_State", builderState.FirstOrDefault());
            //hInputPara.Add("@Builder_C_Pincode", builderPincode.FirstOrDefault());
            //hInputPara.Add("@Builder_Nearby_LandMark", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Builder_Nearby_LandMark));
            //hInputPara.Add("@Nature_Of_Company", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Nature_Of_Company));
            //hInputPara.Add("@Director_Name1", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_Name1));
            //hInputPara.Add("@Director_Phone1", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_Phone1));
            //hInputPara.Add("@Director_Mobile1", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_Mobile1));
            //hInputPara.Add("@Director_EmailId1", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_EmailId1));
            //hInputPara.Add("@Director_Name2", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_Name2));
            //hInputPara.Add("@Director_Phone2", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_Phone2));
            //hInputPara.Add("@Director_Mobile2", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_Mobile2));
            //hInputPara.Add("@Director_EmailId2", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_EmailId2));
            //hInputPara.Add("@Director_Name3", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_Name3));
            //hInputPara.Add("@Director_Phone3", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_Phone3));
            //hInputPara.Add("@Director_Mobile3", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_Mobile3));
            //hInputPara.Add("@Director_EmailId3", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Director_EmailId3));
            //hInputPara.Add("@Builder_Grade", Convert.ToString(vm.ival_BuilderCompanyMasterNew.Builder_Grade));

            // var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateBuilderCompanyMasterDetailsByIDNew", hInputPara);
            //if (Convert.ToInt32(result) == 1)
            //{
            //    return RedirectToAction("Index", "BuilderAndCompany");
            //    // return RedirectToAction("ViewCompany", new RouteValueDictionary(new { controller = "BuilderAndCompany", action = "ViewCompany", Id = vMBuilderGroupMaster.SelectedBuilderGroupMasterID }));
            //}
            //else
            //{
            //    return Json("Error in Database !", JsonRequestBehavior.AllowGet);
            // }

            return Json("");


        }


        [HttpGet]
        public ActionResult DeleteBuilderGroup(int? ID)
        {
            try
            {
                sqlDataAccess = new SQLDataAccess();
                hInputPara = new Hashtable();
                hInputPara.Add("@Builder_Group_Master_ID", ID);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteBuilderGroupByMasterID", hInputPara);
                result.ToString();
                return RedirectToAction("Index", "BuilderAndCompany");
            }
            catch(Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteCompany(int? ID)
        {
            try
            {
                sqlDataAccess = new SQLDataAccess();
                hInputPara = new Hashtable();
                hInputPara.Add("@Builder_Company_Master_ID", ID);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteBuilderCompanyMasterByID", hInputPara);
                result.ToString();
                return RedirectToAction("Index", "BuilderAndCompany");
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult DeleteBuilderCompany(int? ID)
        {
            try
            {
                sqlDataAccess = new SQLDataAccess();
                hInputPara = new Hashtable();
                hInputPara.Add("@Builder_Company_Master_ID", ID);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteBuilderCompanyMaster", hInputPara);
                result.ToString();
                return RedirectToAction("CompanyIndex", "BuilderAndCompany");
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public IEnumerable<ival_States> GetState()
        {
            List<ival_States> listState = new List<ival_States>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetStateByID");
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                listState.Add(new ival_States
                {
                    State_ID = Convert.ToInt32(dtState.Rows[i]["State_ID"]),
                    State_Name = Convert.ToString(dtState.Rows[i]["State_Name"])

                });


            }
            return listState.AsEnumerable();
        }

        [HttpPost]
        public IEnumerable<ival_Districts> GetDistrict()
        {
            List<ival_Districts> lists = new List<ival_Districts>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            try
            {
                DataTable dtDistict = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDistrictByID");
                for (int i = 0; i < dtDistict.Rows.Count; i++)
                {
                    lists.Add(new ival_Districts
                    {
                        District_ID = Convert.ToInt32(dtDistict.Rows[i]["District_ID"]),
                        District_Name = Convert.ToString(dtDistict.Rows[i]["District_Name"])
                    });
                }
            }
            catch (Exception)
            {

            }
            return lists.AsEnumerable();
        }

        public IEnumerable<ival_Locality> GetLocality()
        {
            List<ival_Locality> listLoc = new List<ival_Locality>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            DataTable dtLocality = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalityByID");
            for (int i = 0; i < dtLocality.Rows.Count; i++)
            {

                listLoc.Add(new ival_Locality
                {
                    Loc_Id = Convert.ToInt32(dtLocality.Rows[i]["Loc_Id"]),
                    Loc_Name = Convert.ToString(dtLocality.Rows[i]["Loc_Name"]),
                    Pin_Code = Convert.ToInt32(dtLocality.Rows[i]["Pin_Code"])

                });
            }
            return listLoc.AsEnumerable();
        }

        [HttpPost]
        public IEnumerable<ival_Cities> GetCity()
        {
            List<ival_Cities> listState = new List<ival_Cities>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtCity = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCityByID");
            for (int i = 0; i < dtCity.Rows.Count; i++)
            {

                listState.Add(new ival_Cities
                {
                    City_ID = Convert.ToInt32(dtCity.Rows[i]["City_ID"]),
                    City_Name = Convert.ToString(dtCity.Rows[i]["City"])

                });


            }
            return listState.AsEnumerable();
        }


        /* [HttpPost]
         public IEnumerable<ival_Locality> GetPincode()
         {
             List<ival_Locality> listLocPin = new List<ival_Locality>();
             SQLDataAccess sqlDataAccess = new SQLDataAccess();
             DataTable dtLocPin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalityByID");
             for (int i = 0; i < dtLocPin.Rows.Count; i++)
             {

                 listLocPin.Add(new ival_Locality
                 {
                     Loc_Id = Convert.ToInt32(dtLocPin.Rows[i]["Loc_Id"]),
                     //Loc_Name = Convert.ToString(listLocPin.Rows[i]["Loc_Name"]),
                     Pin_Code = Convert.ToInt32(dtLocPin.Rows[i]["Pin_Code"])

                 });
             }
             return listLocPin.AsEnumerable();
         }*/

        [HttpPost]
        public JsonResult GetPinByOnselect(int str)
        {
            hInputPara = new Hashtable();
            List<ival_Locality> PinList = new List<ival_Locality>();
            // DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Loc_Id", str);

            DataTable dtLocPin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPinByLocName", hInputPara);
            for (int i = 0; i < dtLocPin.Rows.Count; i++)
            {
                PinList.Add(new ival_Locality
                {
                    Pin_Code = Convert.ToInt32(dtLocPin.Rows[i]["Pin_Code"])

                });
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectpinMaster = jsonSerialiser.Serialize(PinList);
            return Json(jsonProjectpinMaster);
        }
        /* public JsonResult GetpinbyCityId(int str)
         {
             List<ival_Cities> ival_pin = new List<ival_Cities>();

             hInputPara = new Hashtable();
             sqlDataAccess = new SQLDataAccess();
             hInputPara.Add("@City_ID", str);
             DataTable dtpin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPinbycity", hInputPara);
             for (int i = 0; i < dtpin.Rows.Count; i++)
             {
                 ival_pin.Add(new ival_Cities
                 {
                     Loc_Id = Convert.ToInt32(dtpin.Rows[i]["City_ID"]),
                     City_Name = Convert.ToString(dtpin.Rows[i]["City_Name"]),
                     Pin_Code = Convert.ToString(dtpin.Rows[i]["Pin_Code"]),

                 });
             }

             var jsonSerialiser = new JavaScriptSerializer();
             var jsonProjectpinMaster = jsonSerialiser.Serialize(ival_pin);
             return Json(jsonProjectpinMaster);
         }*/

        

        public IEnumerable<ival_LookupCategoryValues> GetPincode()
        {
            List<ival_LookupCategoryValues> listPincode = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtPincode = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPincode");
            for (int i = 0; i < dtPincode.Rows.Count; i++)
            {

                listPincode.Add(new ival_LookupCategoryValues
                {
                    Lookup_Value = Convert.ToString(dtPincode.Rows[i]["Lookup_Value"]),
                    Lookup_ID = Convert.ToInt32(dtPincode.Rows[i]["Lookup_ID"])

                });


            }
            return listPincode.AsEnumerable();
        }
        public IEnumerable<ival_BuilderGroupMaster> GetBuilderList()
        {
            List<ival_BuilderGroupMaster> builderList = new List<ival_BuilderGroupMaster>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuilder");
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                builderList.Add(new ival_BuilderGroupMaster
                {
                    Builder_Group_Master_ID = Convert.ToInt32(dtState.Rows[i]["Builder_Group_Master_ID"]),
                    BuilderGroupName = Convert.ToString(dtState.Rows[i]["BuilderGroupName"])
 
                });


            }
            return builderList.AsEnumerable();
        }
        public IEnumerable<ival_natureOfCompany> GetNatureOfCompanyList()
        {
            List<ival_natureOfCompany> builderList = new List<ival_natureOfCompany>();

            ival_natureOfCompany ival_Nature = new ival_natureOfCompany();
            ival_natureOfCompany ival_Nature1 = new ival_natureOfCompany();
            ival_natureOfCompany ival_Nature2 = new ival_natureOfCompany();
            ival_natureOfCompany ival_Nature3 = new ival_natureOfCompany();
            ival_natureOfCompany ival_Nature4 = new ival_natureOfCompany();
            ival_natureOfCompany ival_Nature5 = new ival_natureOfCompany();
            ival_Nature.NatureName = "Public Limited";
            ival_Nature.NatureId = 1;
            builderList.Add(ival_Nature);
            ival_Nature1.NatureName = "Private Limited";
            ival_Nature1.NatureId = 2;
            builderList.Add(ival_Nature1);
            ival_Nature2.NatureName = "Partnership";
            ival_Nature2.NatureId = 3;
            builderList.Add(ival_Nature2);
            ival_Nature3.NatureName = "LLP";
            ival_Nature3.NatureId = 4;
            builderList.Add(ival_Nature3);
            ival_Nature4.NatureName = "Proprietor";
            ival_Nature4.NatureId = 5;
            builderList.Add(ival_Nature4);
            return builderList.AsEnumerable();
        }


        public JsonResult GetDistByStateId(int State_Id)
        {
            List<ival_Districts> ival_Districts = new List<ival_Districts>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@State_ID", State_Id);
            DataTable dtdist = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDisticts", hInputPara);
            for (int i = 0; i < dtdist.Rows.Count; i++)
            {
                ival_Districts.Add(new ival_Districts
                {
                    District_ID = Convert.ToInt32(dtdist.Rows[i]["District_ID"]),
                    District_Name = Convert.ToString(dtdist.Rows[i]["District_Name"]),

                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectdistMaster = jsonSerialiser.Serialize(ival_Districts);
            return Json(jsonProjectdistMaster);
        }
        public JsonResult GetcitybydistId(int District_ID)
        {
            List<ival_Cities> ival_city = new List<ival_Cities>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@District_ID", District_ID);
            DataTable dtcity = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCitys", hInputPara);
            for (int i = 0; i < dtcity.Rows.Count; i++)
            {
                ival_city.Add(new ival_Cities
                {
                    Loc_Id = Convert.ToInt32(dtcity.Rows[i]["City_ID"]),
                    City_Name = Convert.ToString(dtcity.Rows[i]["City_Name"]),

                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectcityMaster = jsonSerialiser.Serialize(ival_city);
            return Json(jsonProjectcityMaster);
        }


    }
}