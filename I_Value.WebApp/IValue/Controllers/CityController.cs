﻿using I_Value.WebApp.Models;
using IvalueDataAccess;
using IValuePublishProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace I_Value.WebApp.Controllers
{
    public class CityController : Controller
    {
        SQLDataAccess sqlDataAccess;
        Hashtable hInputPara;
        IvalueDataModel Datamodel = new IvalueDataModel();
        ViewModel viewModel;
        // GET: City
        public ActionResult Index()
        {
            VMCityMaster vMCityMaster = new VMCityMaster();
            SQLDataAccess sqlDataAccess;

            vMCityMaster.ival_DistrictsList = GetDistricts();
            vMCityMaster.ival_CityMaster = GetCity();
            return View(vMCityMaster);
        }
        [HttpPost]
        public ActionResult AddCity(string str)
        {
            string resultStr = string.Empty;
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
          
            for (int i = 0; i < list1.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                hInputPara.Add("@City", Convert.ToString(list1.Rows[i]["txt_city"]));
                hInputPara.Add("@Created_By", "Test");
                hInputPara.Add("@District_ID", Convert.ToInt32(list1.Rows[i]["txt_districtid"]));

                string Datastr= sqlDataAccess.ExecuteStoreProcedure("usp_InsertCity", hInputPara);
                if(Datastr== "City Already Exists")
                {
                    resultStr = Datastr;
                }
                else if (Datastr == "1")
                {
                    resultStr = "City Details Added Successfully";
                }
                else
                {
                    resultStr = "Error while inserting";
                }
                hInputPara.Clear();
            }

            return Json(new { success = true, Message = resultStr }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public IEnumerable<ival_Districts> GetDistricts()
        {
            List<ival_Districts> lists = new List<ival_Districts>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            try
            {
                DataTable dtDistict = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDistrictByID");
                for (int i = 0; i < dtDistict.Rows.Count; i++)
                {
                    lists.Add(new ival_Districts
                    {
                        District_ID = Convert.ToInt32(dtDistict.Rows[i]["District_ID"]),
                        District_Name = Convert.ToString(dtDistict.Rows[i]["District_Name"])
                    });
                }
            }
            catch (Exception)
            {

            }
            return lists.AsEnumerable();
        }

        [HttpPost]
        public IEnumerable<ival_CityMaster> GetCity()
        {
            List<ival_CityMaster> lists = new List<ival_CityMaster>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            DataTable dtCity = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCityAndDist");
            for (int i = 0; i < dtCity.Rows.Count; i++)
            {
                lists.Add(new ival_CityMaster
                {
                    City_Id = Convert.ToInt32(dtCity.Rows[i]["City_Id"]),
                    District_Name = dtCity.Rows[i]["District_Name"].ToString(),
                    City = dtCity.Rows[i]["City"].ToString()
                });
            }
            return lists.AsEnumerable();
        }
        
        [HttpGet]
        public ActionResult EditCity(int ID)
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VMCityMaster vMCityMaster = new VMCityMaster();
            vMCityMaster.ival_DistrictsList = GetDistricts();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            try 
            { 
           
                hInputPara.Add("City_Id", ID);
                
                DataTable dtCity = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCityByCityId", hInputPara);
                if (dtCity != null && dtCity.Rows.Count > 0)
                {
                    vMCityMaster.City_Id = ID;
                    vMCityMaster.District_ID = Convert.ToInt32(dtCity.Rows[0]["District_ID"]);
                    vMCityMaster.City = dtCity.Rows[0]["City"].ToString();
                }
        }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
    }
            return View(vMCityMaster);
        }

        [HttpPost]
        public ActionResult EditCity(string str)
        {
            string resultStr = string.Empty;
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

            for (int i = 0; i < list1.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                hInputPara.Add("@City_Id", Convert.ToString(list1.Rows[i]["txt_Id"]));
                hInputPara.Add("@City", Convert.ToString(list1.Rows[i]["txt_city"]));
                hInputPara.Add("@Modified_By", "Test");
                hInputPara.Add("@District_ID", Convert.ToInt32(list1.Rows[i]["txt_districtid"]));

                string Datastr = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateCityByCityId", hInputPara);
                if (Datastr == "City Already Exists")
                {
                    resultStr = Datastr;
                }
                else if (Datastr == "1")
                {
                    resultStr = "City Details Updated Successfully";
                }
                else
                {
                    resultStr = "Error while inserting";
                }
                hInputPara.Clear();
            }

            return Json(new { success = true, Message = resultStr }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int ID)
        {
            try
            {
                SQLDataAccess sqlDataAccess = new SQLDataAccess();
                Hashtable hInputPara = new Hashtable();
                hInputPara.Add("@City_Id", ID);
                var result = sqlDataAccess.ExecuteStoreProcedure("[usp_DeleteCityById]", hInputPara);
                result.ToString();
                if (Convert.ToInt32(result) == 1)
                {
                    
                    return RedirectToAction("Index", "City");
                }
                else
                {
                    return Json("Error in Database Record not Delete!", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ViewDetails(int ID)
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VMCityMaster vMCityMaster = new VMCityMaster();
            vMCityMaster.ival_DistrictsList = GetDistricts();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            try
            {

                hInputPara.Add("City_Id", ID);

                DataTable dtCity = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCityByCityId", hInputPara);
                if (dtCity != null && dtCity.Rows.Count > 0)
                {
                    vMCityMaster.City_Id = ID;
                    vMCityMaster.District_ID = Convert.ToInt32(dtCity.Rows[0]["District_ID"]);
                    vMCityMaster.City = dtCity.Rows[0]["City"].ToString();
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("Index", "Branch");
            }
            return View(vMCityMaster);
        }
    }
}