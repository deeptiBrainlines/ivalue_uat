﻿using I_Value.WebApp.Models;
using IvalueDataAccess;
using IValuePublishProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace I_Value.WebApp.Controllers
{

    public class Ival_EmployeeController : Controller
    {

        SQLDataAccess sqlDataAccess;
        Hashtable hInputPara;
        IvalueDataModel Datamodel = new IvalueDataModel();
        VMEmployeeMaster vMEmployeeMaster;
        ival_EmployeeMaster objEmp;

        // GET: Ival_Employee
        public ActionResult Index()
        {
            try
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                IEnumerable<ival_BranchMaster> ival_BranchMastersList = GetBranch();
                IEnumerable<ival_RoleMaster> ival_RoleMasters = GetRoles();
                DataTable dtEmployee = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetEmployees");

                List<VMEmployeeMaster> listEmp = new List<VMEmployeeMaster>();

                if (dtEmployee.Rows.Count > 0 && dtEmployee != null)

                {
                    for (int i = 0; i < dtEmployee.Rows.Count; i++)
                    {
                        var branchName = ival_BranchMastersList.Where(row => row.Branch_ID == Convert.ToInt32(dtEmployee.Rows[i]["Branch_ID"])).Select(p => p.Branch_Name.ToString());
                        var roleName = ival_RoleMasters.Where(row => row.Role_ID == Convert.ToInt32(dtEmployee.Rows[i]["Role_ID"])).Select(p => p.Role_Name.ToString());
                        listEmp.Add(new VMEmployeeMaster
                        {
                            current_status = Convert.ToString(dtEmployee.Rows[i]["current_status"]),
                            Employee_ID = Convert.ToInt16(dtEmployee.Rows[i]["Employee_ID"]),
                            Company_Email_ID = Convert.ToString(dtEmployee.Rows[i]["Company_Email_ID"]),
                            First_Name = Convert.ToString(dtEmployee.Rows[i]["First_Name"]),

                            Branch_Name = branchName.FirstOrDefault(),
                            Role_Name = Convert.ToString(dtEmployee.Rows[i]["Role_Name"])

                        }); ;

                    }
                }
                return View(listEmp);
            }

            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult AddEmployee()
        {
            vMEmployeeMaster = new VMEmployeeMaster();
            vMEmployeeMaster.ival_BranchMastersList = GetBranch();
            vMEmployeeMaster.ival_RoleMastersList = GetRoles();
            vMEmployeeMaster.ival_LocalityList = GetLocality();
            vMEmployeeMaster.ival_StatesList = GetState();
            vMEmployeeMaster.ival_CitiesList = GetCity();
            vMEmployeeMaster.ival_DistrictsList = GetDistricts();
            //vMEmployeeMaster.pincodeList = GetPincode();
            vMEmployeeMaster.relationWithEmpList = GetRelationsWithEmp();
            vMEmployeeMaster.banklist = Getbank();
            vMEmployeeMaster.bloodGrpList = GetBloodGrp();
            vMEmployeeMaster.currentstatus = GetCurrent();
            return View(vMEmployeeMaster);
        }
        [HttpGet]
        public ActionResult Edit(int ID)
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //return View();
            }
            vMEmployeeMaster = new VMEmployeeMaster();
            objEmp = new ival_EmployeeMaster();
            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Employee_ID", ID);
                DataTable dtEmp = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetEmployeesByEmpId", hInputPara);

                if (dtEmp.Rows.Count > 0 && dtEmp != null)

                {
                    vMEmployeeMaster.relationWithEmpList = GetRelationsWithEmp();
                    vMEmployeeMaster.banklist = Getbank();
                    object valueEmployee_ID = Convert.ToInt32(dtEmp.Rows[0]["Employee_ID"]);
                    if (dtEmp.Columns.Contains("Employee_ID") && valueEmployee_ID != null)
                        objEmp.Employee_ID = Convert.ToInt32(valueEmployee_ID);
                    else
                        objEmp.Employee_ID = 0;

                    object valueFirst_Name = dtEmp.Rows[0]["First_Name"];
                    if (dtEmp.Columns.Contains("First_Name") && valueFirst_Name != null && valueFirst_Name.ToString() != "")
                        objEmp.First_Name = dtEmp.Rows[0]["First_Name"].ToString();
                    else
                        objEmp.First_Name = string.Empty;


                    object valueMiddle_Name = dtEmp.Rows[0]["Middle_Name"];
                    if (dtEmp.Columns.Contains("Middle_Name") && valueMiddle_Name != null && valueMiddle_Name.ToString() != "")
                        objEmp.Middle_Name = dtEmp.Rows[0]["Middle_Name"].ToString();
                    else
                        objEmp.Middle_Name = string.Empty;

                    object valueLast_Name = dtEmp.Rows[0]["Last_Name"];
                    if (dtEmp.Columns.Contains("Last_Name") && valueLast_Name != null && valueLast_Name.ToString() != "")
                        objEmp.Last_Name = dtEmp.Rows[0]["Last_Name"].ToString();
                    else
                        objEmp.Last_Name = string.Empty;

                    object valueCompany_Email_ID = dtEmp.Rows[0]["Company_Email_ID"];
                    if (dtEmp.Columns.Contains("Company_Email_ID") && valueCompany_Email_ID != null && valueCompany_Email_ID.ToString() != "")
                        objEmp.Company_Email_ID = dtEmp.Rows[0]["Company_Email_ID"].ToString();
                    else
                        objEmp.Company_Email_ID = string.Empty;

                    object valueBranch_ID = dtEmp.Rows[0]["Branch_ID"];
                    if (dtEmp.Columns.Contains("Branch_ID") && valueBranch_ID != null)
                        objEmp.Branch_ID = Convert.ToInt32(dtEmp.Rows[0]["Branch_ID"].ToString());
                    else
                        objEmp.Branch_ID = 0;

                    object valueRole_ID = dtEmp.Rows[0]["Role_ID"];
                    if (dtEmp.Columns.Contains("Role_ID") && valueRole_ID != null)
                        objEmp.Role_ID = Convert.ToInt32(dtEmp.Rows[0]["Role_ID"].ToString());
                    else
                        objEmp.Role_ID = 0;


                    object valueContact_Number = dtEmp.Rows[0]["Contact_Number"];
                    if (dtEmp.Columns.Contains("Contact_Number") && valueContact_Number != null && valueContact_Number.ToString() != "")
                        vMEmployeeMaster.Contact_Number = Convert.ToString(dtEmp.Rows[0]["Contact_Number"].ToString());
                    else
                        vMEmployeeMaster.Contact_Number = string.Empty;

                    object valuePersonal_Email_ID = dtEmp.Rows[0]["Personal_Email_ID"];
                    if (dtEmp.Columns.Contains("Personal_Email_ID") && valuePersonal_Email_ID != null && valuePersonal_Email_ID.ToString() != "")
                        objEmp.Personal_Email_ID = dtEmp.Rows[0]["Personal_Email_ID"].ToString();
                    else
                        objEmp.Personal_Email_ID = string.Empty;

                    object valueCurrent_Address_1 = dtEmp.Rows[0]["Current_Address_1"];
                    if (dtEmp.Columns.Contains("Current_Address_1") && valueCurrent_Address_1 != null && valueCurrent_Address_1.ToString() != "")
                        objEmp.Current_Address_1 = dtEmp.Rows[0]["Current_Address_1"].ToString();
                    else
                        objEmp.Current_Address_1 = string.Empty;

                    object valueCurrent_Address_2 = dtEmp.Rows[0]["Current_Address_2"];
                    if (dtEmp.Columns.Contains("Current_Address_2") && valueCurrent_Address_2 != null && valueCurrent_Address_2.ToString() != "")
                        objEmp.Current_Address_2 = dtEmp.Rows[0]["Current_Address_2"].ToString();
                    else
                        objEmp.Current_Address_2 = string.Empty;

                    object valueCurrent_City1 = dtEmp.Rows[0]["Current_City1"];
                    if (dtEmp.Columns.Contains("Current_City1") && valueCurrent_City1 != null && valueCurrent_City1.ToString() != "")
                        objEmp.Current_City1 = dtEmp.Rows[0]["Current_City1"].ToString();
                    else
                        objEmp.Current_City1 = string.Empty;

                    object valueCurrent_State = dtEmp.Rows[0]["Current_State"];
                    if (dtEmp.Columns.Contains("Current_State") && valueCurrent_State != null && valueCurrent_State.ToString() != "")
                        objEmp.Current_State = dtEmp.Rows[0]["Current_State"].ToString();
                    else
                        objEmp.Current_State = string.Empty;

                    object valueCurrent_District = dtEmp.Rows[0]["Current_District"];
                    if (dtEmp.Columns.Contains("Current_District") && valueCurrent_District != null && valueCurrent_District.ToString() != "")
                        objEmp.Current_District = dtEmp.Rows[0]["Current_District"].ToString();
                    else
                        objEmp.Current_District = string.Empty;


                    object valueCurrent_City = dtEmp.Rows[0]["Current_City"];
                    if (dtEmp.Columns.Contains("Current_City") && valueCurrent_City != null && valueCurrent_City.ToString() != "")
                        objEmp.Current_City = dtEmp.Rows[0]["Current_City"].ToString();
                    else
                        objEmp.Current_City = string.Empty;

                    object valueCurrent_Pincode = dtEmp.Rows[0]["Current_Pincode"];
                    if (dtEmp.Columns.Contains("Current_Pincode") && valueCurrent_Pincode != null)
                        objEmp.Current_Pincode = Convert.ToInt32(dtEmp.Rows[0]["Current_Pincode"].ToString());
                    else
                        objEmp.Current_Pincode = 0;

                    object valuePerm_Address_1 = dtEmp.Rows[0]["Perm_Address_1"];
                    if (dtEmp.Columns.Contains("Perm_Address_1") && valuePerm_Address_1 != null && valuePerm_Address_1.ToString() != "")
                        objEmp.Perm_Address_1 = dtEmp.Rows[0]["Perm_Address_1"].ToString();
                    else
                        objEmp.Perm_Address_1 = string.Empty;

                    object valuePerm_Address_2 = dtEmp.Rows[0]["Perm_Address_2"];
                    if (dtEmp.Columns.Contains("Perm_Address_2") && valuePerm_Address_2 != null && valuePerm_Address_2.ToString() != "")
                        objEmp.Perm_Address_2 = dtEmp.Rows[0]["Perm_Address_2"].ToString();
                    else
                        objEmp.Perm_Address_2 = string.Empty;

                    object valuePerm_City1 = dtEmp.Rows[0]["Perm_City1"];
                    if (dtEmp.Columns.Contains("Perm_City1") && valuePerm_City1 != null && valuePerm_City1.ToString() != "")
                        objEmp.Perm_City1 = dtEmp.Rows[0]["Perm_City1"].ToString();
                    else
                        objEmp.Perm_City1 = string.Empty;

                    object valuePerm_State = dtEmp.Rows[0]["Perm_State"];
                    if (dtEmp.Columns.Contains("Perm_State") && valuePerm_State != null && valuePerm_State.ToString() != "")
                        objEmp.Perm_State = dtEmp.Rows[0]["Perm_State"].ToString();
                    else
                        objEmp.Perm_State = string.Empty;

                    object valuePerm_District = dtEmp.Rows[0]["Perm_District"];
                    if (dtEmp.Columns.Contains("Perm_District") && valuePerm_District != null && valuePerm_District.ToString() != "")
                        objEmp.Perm_District = dtEmp.Rows[0]["Perm_District"].ToString();
                    else
                        objEmp.Perm_District = string.Empty;

                    object valuePerm_City = dtEmp.Rows[0]["Perm_City"];
                    if (dtEmp.Columns.Contains("Perm_City") && valuePerm_City != null && valuePerm_City.ToString() != "")
                        objEmp.Perm_City = dtEmp.Rows[0]["Perm_City"].ToString();
                    else
                        objEmp.Perm_City = string.Empty;

                    object valuePerm_Pincode = dtEmp.Rows[0]["Perm_Pincode"];
                    if (dtEmp.Columns.Contains("Perm_Pincode") && valuePerm_Pincode != null)
                        objEmp.Perm_Pincode = Convert.ToInt32(dtEmp.Rows[0]["Perm_Pincode"].ToString());
                    else
                        objEmp.Perm_Pincode = 0;

                    object valueEmergency_Contact_Name = dtEmp.Rows[0]["Emergency_Contact_Name"];
                    if (dtEmp.Columns.Contains("Emergency_Contact_Name") && valueEmergency_Contact_Name != null && valueEmergency_Contact_Name.ToString() != "")
                        objEmp.Emergency_Contact_Name = dtEmp.Rows[0]["Emergency_Contact_Name"].ToString();
                    else
                        objEmp.Emergency_Contact_Name = string.Empty;

                    object valueEmergency_Contact_No = dtEmp.Rows[0]["Emergency_Contact_No"];
                    if (dtEmp.Columns.Contains("Emergency_Contact_No") && valueEmergency_Contact_No != null && valueEmergency_Contact_No.ToString() != "")
                        objEmp.Emergency_Contact_No = dtEmp.Rows[0]["Emergency_Contact_No"].ToString();
                    else
                        objEmp.Emergency_Contact_No = string.Empty;

                    object valueRelationship_with_Employee = dtEmp.Rows[0]["Relationship_with_Employee"];
                    if (dtEmp.Columns.Contains("Relationship_with_Employee") && valueRelationship_with_Employee != null && valueRelationship_with_Employee.ToString() != "")
                        objEmp.Relationship_with_Employee = dtEmp.Rows[0]["Relationship_with_Employee"].ToString();
                    else
                        objEmp.Relationship_with_Employee = string.Empty;

                    object valueBlood_Group = dtEmp.Rows[0]["Blood_Group"];
                    if (dtEmp.Columns.Contains("Blood_Group") && valueBlood_Group != null && valueBlood_Group.ToString() != "")
                    {
                        objEmp.Blood_Group = dtEmp.Rows[0]["Blood_Group"].ToString();
                        vMEmployeeMaster.SelectedBloodGrpID = dtEmp.Rows[0]["Blood_Group"].ToString();
                    }
                    else
                        objEmp.Blood_Group = string.Empty;

                    object valueAadhar_No = dtEmp.Rows[0]["Aadhar_No"];
                    if (dtEmp.Columns.Contains("Aadhar_No") && valueAadhar_No != null && valueAadhar_No.ToString() != "")
                        objEmp.Aadhar_No = dtEmp.Rows[0]["Aadhar_No"].ToString();
                    else
                        objEmp.Aadhar_No = string.Empty;

                    object valuePan_No = dtEmp.Rows[0]["Pan_No"];
                    if (dtEmp.Columns.Contains("Pan_No") && valuePan_No != null && valuePan_No.ToString() != "")
                        objEmp.Pan_No = dtEmp.Rows[0]["Pan_No"].ToString();
                    else
                        objEmp.Pan_No = string.Empty;

                    object valueBank_Name = dtEmp.Rows[0]["Bank_Name"];
                    if (dtEmp.Columns.Contains("Bank_Name") && valueBank_Name != null && valueBank_Name.ToString() != "")
                        objEmp.Bank_Name = dtEmp.Rows[0]["Bank_Name"].ToString();
                    else
                        objEmp.Bank_Name = string.Empty;

                    object valueBank_Address1 = dtEmp.Rows[0]["Bank_Address1"];
                    if (dtEmp.Columns.Contains("Bank_Address1") && valueBank_Address1 != null && valueBank_Address1.ToString() != "")
                        objEmp.Bank_Address1 = dtEmp.Rows[0]["Bank_Address1"].ToString();
                    else
                        objEmp.Bank_Address1 = string.Empty;

                    object valueBank_Address2 = dtEmp.Rows[0]["Bank_Address2"];
                    if (dtEmp.Columns.Contains("Bank_Address2") && valueBank_Address2 != null && valueBank_Address2.ToString() != "")
                        objEmp.Bank_Address2 = dtEmp.Rows[0]["Bank_Address2"].ToString();
                    else
                        objEmp.Bank_Address2 = string.Empty;

                    object valueBank_State = dtEmp.Rows[0]["Bank_State"];
                    if (dtEmp.Columns.Contains("Bank_State") && valueBank_State != null && valueBank_State.ToString() != "")
                        objEmp.Bank_State = dtEmp.Rows[0]["Bank_State"].ToString();
                    else
                        objEmp.Bank_State = string.Empty;

                    object valueBank_District = dtEmp.Rows[0]["Bank_District"];
                    if (dtEmp.Columns.Contains("Bank_District") && valueBank_District != null && valueBank_District.ToString() != "")
                        objEmp.Bank_District = dtEmp.Rows[0]["Bank_District"].ToString();
                    else
                        objEmp.Bank_District = string.Empty;

                    object valueBank_City = dtEmp.Rows[0]["Bank_City"];
                    if (dtEmp.Columns.Contains("Bank_City") && valueBank_City != null && valueBank_City.ToString() != "")
                        objEmp.Bank_City = dtEmp.Rows[0]["Bank_City"].ToString();
                    else
                        objEmp.Bank_City = string.Empty;

                    object valueBank_Locality = dtEmp.Rows[0]["Bank_Locality"];
                    if (dtEmp.Columns.Contains("Bank_Locality") && valueBank_Locality != null && valueBank_Locality.ToString() != "")
                        objEmp.Bank_Locality = dtEmp.Rows[0]["Bank_Locality"].ToString();
                    else
                        objEmp.Bank_Locality = string.Empty;

                    object valueBank_Pincode = dtEmp.Rows[0]["Bank_Pincode"];
                    if (dtEmp.Columns.Contains("Bank_Pincode") && valueBank_Pincode != null && valueBank_Pincode.ToString() != "")
                        objEmp.Bank_Pincode = dtEmp.Rows[0]["Bank_Pincode"].ToString();
                    else
                        objEmp.Bank_Pincode = string.Empty;


                    object valueBank_IFSC_Code = dtEmp.Rows[0]["Bank_IFSC_Code"];
                    if (dtEmp.Columns.Contains("Bank_IFSC_Code") && valueBank_IFSC_Code != null && valueBank_IFSC_Code.ToString() != "")
                        objEmp.Bank_IFSC_Code = dtEmp.Rows[0]["Bank_IFSC_Code"].ToString();
                    else
                        objEmp.Bank_IFSC_Code = string.Empty;

                    object valueRole_ID1 = dtEmp.Rows[0]["Role_ID"];
                    if (dtEmp.Columns.Contains("Role_ID") && valueRole_ID1 != null)
                        vMEmployeeMaster.SelectedRoleId = Convert.ToInt32(dtEmp.Rows[0]["Role_ID"].ToString());
                    else
                        vMEmployeeMaster.SelectedRoleId = 0;

                    object valueCurrent_Pincode1 = dtEmp.Rows[0]["Current_Pincode"];
                    if (dtEmp.Columns.Contains("Current_Pincode") && valueCurrent_Pincode1 != null)
                        vMEmployeeMaster.SelectedCurrentPincodeID = Convert.ToInt32(dtEmp.Rows[0]["Current_Pincode"].ToString());
                    else
                        vMEmployeeMaster.SelectedCurrentPincodeID = 0;

                    object valuePerm_Pincode1 = dtEmp.Rows[0]["Perm_Pincode"];
                    if (dtEmp.Columns.Contains("Perm_Pincode") && valuePerm_Pincode1 != null)
                        vMEmployeeMaster.SelectedPermanentPincodeID = Convert.ToInt32(dtEmp.Rows[0]["Perm_Pincode"].ToString());
                    else
                        vMEmployeeMaster.SelectedPermanentPincodeID = 0;

                    /* object valueBank_Pincode1 = dtEmp.Rows[0]["Bank_Pincode"];
                     if (dtEmp.Columns.Contains("Bank_Pincode") && valueBank_Pincode1 != null)
                         vMEmployeeMaster.SelectedBankPincodeID = Convert.ToInt32(dtEmp.Rows[0]["Bank_Pincode"].ToString());
                     else
                         vMEmployeeMaster.SelectedBankPincodeID = current_status0;*/
                    ViewBag.Aadhar_No = dtEmp.Rows[0]["Aadhar_No"].ToString();
                    ViewBag.Pan_No = dtEmp.Rows[0]["Pan_No"].ToString();
                    ViewBag.Bank_IFSC_Code = dtEmp.Rows[0]["Bank_IFSC_Code"].ToString();
                    ViewBag.Contact_Number = dtEmp.Rows[0]["Contact_Number"].ToString();
                    ViewBag.Emergency_Contact_No = dtEmp.Rows[0]["Emergency_Contact_No"];
                    vMEmployeeMaster.ival_RoleMastersList = GetRoles();
                    vMEmployeeMaster.ival_BranchMastersList = GetBranch();
                    vMEmployeeMaster.ival_LocalityList = GetLocality();
                    vMEmployeeMaster.ival_DistrictsList = GetDistricts();
                    //vMEmployeeMaster.pincodeList = GetPincode();
                    vMEmployeeMaster.ival_CitiesList = GetCity();
                    vMEmployeeMaster.ival_StatesList = GetState();
                    vMEmployeeMaster.bloodGrpList = GetBloodGrp();
                    vMEmployeeMaster.banklist = Getbank();
                    vMEmployeeMaster.relationWithEmpList = GetRelationsWithEmp();
                    vMEmployeeMaster.currentstatus = GetCurrent();
                    /* IEnumerable<ival_States> ival_StatesList = GetState();
                     if (ival_StatesList.ToList().Count > 0 && ival_StatesList != null)
                     {
                         var currentState = ival_StatesList.Where(s => s.State_Name == dtEmp.Rows[0]["Current_State"].ToString()).Select(s => s.State_ID);
                         var permenantState = ival_StatesList.Where(s => s.State_Name == dtEmp.Rows[0]["Perm_State"].ToString()).Select(s => s.State_ID);
                         var bankState = ival_StatesList.Where(s => s.State_Name == dtEmp.Rows[0]["Bank_State"].ToString()).Select(s => s.State_ID);

                         vMEmployeeMaster.SelectedRelationWithEmpID = objEmp.Relationship_with_Employee;
                         vMEmployeeMaster.SelectedCurrentStateId = Convert.ToInt32(currentState.FirstOrDefault());
                         vMEmployeeMaster.SelectedPermenantStateId = Convert.ToInt32(permenantState.FirstOrDefault());
                         vMEmployeeMaster.SelectedBankStateId = Convert.ToInt32(bankState.FirstOrDefault());
                     }
                     IEnumerable<ival_Districts> ival_DistrictsList = GetDistricts();
                     if (ival_DistrictsList.ToList().Count > 0 && ival_DistrictsList != null)
                     {
                         var currentDist = ival_DistrictsList.Where(s => s.District_Name == dtEmp.Rows[0]["Current_District"].ToString()).Select(s => s.District_ID);
                         var permenantDist = ival_DistrictsList.Where(s => s.District_Name == dtEmp.Rows[0]["Perm_District"].ToString()).Select(s => s.District_ID);
                         var bankDist = ival_DistrictsList.Where(s => s.District_Name == dtEmp.Rows[0]["Bank_District"].ToString()).Select(s => s.District_ID);

                         vMEmployeeMaster.SelectedCurrentDistId = Convert.ToInt32(currentDist.FirstOrDefault());
                         vMEmployeeMaster.SelectedPermenantDistId = Convert.ToInt32(permenantDist.FirstOrDefault());
                         vMEmployeeMaster.SelectedBankDistId = Convert.ToInt32(bankDist.FirstOrDefault());
                     }

                     IEnumerable<ival_Locality> pincodeList = GetPincode();
                     if (pincodeList.ToList().Count > 0 && pincodeList != null)
                     {
                         var currentPincode = pincodeList.Where(s => s.Loc_Name == dtEmp.Rows[0]["Current_Pincode"].ToString()).Select(s => s.Loc_Id);
                         var permenantPincode = pincodeList.Where(s => s.Loc_Name == dtEmp.Rows[0]["Current_Pincode"].ToString()).Select(s => s.Loc_Id);
                         var bankPincode = pincodeList.Where(s => s.Loc_Name == dtEmp.Rows[0]["Current_Pincode"].ToString()).Select(s => s.Loc_Id);

                         vMEmployeeMaster.SelectedCurrentPincodeID = Convert.ToInt32(currentPincode.FirstOrDefault());
                         vMEmployeeMaster.SelectedPermanentPincodeID = Convert.ToInt32(permenantPincode.FirstOrDefault());
                         vMEmployeeMaster.SelectedBankPincodeID = Convert.ToInt32(bankPincode.FirstOrDefault());
                     }

                     IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                     if (ival_LocalityList.ToList().Count > 0 && ival_LocalityList != null)
                     {
                         var currentLocality = ival_LocalityList.Where(s => s.Loc_Name == dtEmp.Rows[0]["Current_City"].ToString()).Select(s => s.Loc_Id);
                         var permenantLocality = ival_LocalityList.Where(s => s.Loc_Name == dtEmp.Rows[0]["Perm_City"].ToString()).Select(s => s.Loc_Id);
                         var bankLocality = ival_LocalityList.Where(s => s.Loc_Name == dtEmp.Rows[0]["Bank_City"].ToString()).Select(s => s.Loc_Id);

                         vMEmployeeMaster.SelectedCurrentCityID = Convert.ToInt32(currentLocality.FirstOrDefault());
                         vMEmployeeMaster.SelectedPermenantCityID = Convert.ToInt32(permenantLocality.FirstOrDefault());
                         vMEmployeeMaster.SelectedBankCityID = Convert.ToInt32(bankLocality.FirstOrDefault());
                     }*/

                    vMEmployeeMaster.Selectedstatus = dtEmp.Rows[0]["current_status"].ToString();
                    vMEmployeeMaster.SelectedBranchId = Convert.ToInt32(dtEmp.Rows[0]["Branch_ID"]);
                    vMEmployeeMaster.ival_EmployeeMaster = objEmp;
                    vMEmployeeMaster.Loc_Id = Convert.ToInt32(dtEmp.Rows[0]["Current_City1"]);
                    vMEmployeeMaster.PermLoc_Id = Convert.ToInt32(dtEmp.Rows[0]["Perm_City1"]);
                    vMEmployeeMaster.SelectedCurrentStateId = Convert.ToInt32(dtEmp.Rows[0]["Current_State"]);
                    vMEmployeeMaster.SelectedPermenantStateId = Convert.ToInt32(dtEmp.Rows[0]["Perm_State"]);
                    vMEmployeeMaster.SelectedBankStateId = Convert.ToInt32(dtEmp.Rows[0]["Bank_State"]);
                    vMEmployeeMaster.SelectedCurrentDistId = Convert.ToInt32(dtEmp.Rows[0]["Current_District"]);
                    vMEmployeeMaster.SelectedPermenantDistId = Convert.ToInt32(dtEmp.Rows[0]["Perm_District"]);
                    vMEmployeeMaster.SelectedBankDistId = Convert.ToInt32(dtEmp.Rows[0]["Bank_District"]);
                    vMEmployeeMaster.SelectedCurrentCityID = Convert.ToInt32(dtEmp.Rows[0]["Current_City"]);
                    vMEmployeeMaster.SelectedPermenantCityID = Convert.ToInt32(dtEmp.Rows[0]["Perm_City"]);
                    vMEmployeeMaster.SelectedBankCityID = Convert.ToInt32(dtEmp.Rows[0]["Bank_City"]);
                    vMEmployeeMaster.SelectedCurrentPincodeID = Convert.ToInt32(dtEmp.Rows[0]["Current_City"]);
                    vMEmployeeMaster.SelectedPermanentPincodeID = Convert.ToInt32(dtEmp.Rows[0]["Perm_City"]);
                    vMEmployeeMaster.SelectedBankPincodeID = Convert.ToInt32(dtEmp.Rows[0]["Bank_City"]);
                    vMEmployeeMaster.SelectedRelationWithEmpID = Convert.ToString(dtEmp.Rows[0]["Relationship_with_Employee"]);
                    vMEmployeeMaster.Bank_Name = Convert.ToString(dtEmp.Rows[0]["Bank_Name"]);
                    vMEmployeeMaster.SelectedBloodGrpID = Convert.ToString(dtEmp.Rows[0]["Blood_Group"]);
                    vMEmployeeMaster.BankLoc_Id = Convert.ToInt32(dtEmp.Rows[0]["Bank_Locality"]);
                    vMEmployeeMaster.Employee_ID = objEmp.Employee_ID;
                }
            }
            catch (Exception ex)
            {
                //ex.InnerException.ToString();
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

            return View(vMEmployeeMaster);
        }

        [HttpPost]
        public ActionResult AddEmployee(String str)
        {
            string resultStr = string.Empty;
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            
            try
            {
                IEnumerable<ival_States> ival_StatesList = GetState();
                var currentState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMEmployeeMaster.SelectedCurrentStateId).Select(row => row.State_Name.ToString());
                var permenantState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMEmployeeMaster.SelectedPermenantStateId).Select(row => row.State_Name.ToString());
                var bankState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMEmployeeMaster.SelectedBankStateId).Select(row => row.State_Name.ToString());

                IEnumerable<ival_Locality> pincodeList = GetLocality();
                var currentPincode = pincodeList.AsEnumerable().Where(row => row.Loc_Id == vMEmployeeMaster.SelectedCurrentPincodeID).Select(row => row.Loc_Name.ToString());
                var permenantPincode = pincodeList.AsEnumerable().Where(row => row.Loc_Id == vMEmployeeMaster.SelectedPermanentPincodeID).Select(row => row.Loc_Name.ToString());
                var bankPincode = pincodeList.AsEnumerable().Where(row => row.Loc_Id == vMEmployeeMaster.SelectedBankPincodeID).Select(row => row.Loc_Name.ToString());


                for (int i = 0; i < list1.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();

                    hInputPara.Add("@Role_ID", Convert.ToInt32(list1.Rows[i]["Role_Id"]));

                    hInputPara.Add("@Branch_ID", Convert.ToInt32(list1.Rows[i]["Branch_Id"]));

                   

                    hInputPara.Add("@First_Name", Convert.ToString(list1.Rows[i]["First_name"]));

                    hInputPara.Add("@Middle_Name", Convert.ToString(list1.Rows[i]["Middle_name"]));

                    hInputPara.Add("@Last_Name", Convert.ToString(list1.Rows[i]["Last_name"]));

                    hInputPara.Add("@Contact_Number", Convert.ToString(list1.Rows[i]["Contact_Number"]));

                    hInputPara.Add("@Company_Email_ID", Convert.ToString(list1.Rows[i]["Company_Mail"]));

                    hInputPara.Add("@Personal_Email_ID", Convert.ToString(list1.Rows[i]["Personal_Mail"]));

                    hInputPara.Add("@Current_Address_1", Convert.ToString(list1.Rows[i]["Current_Address"]));

                    hInputPara.Add("@Current_Address_2", Convert.ToString(list1.Rows[i]["Current_Address_2"]));

                    hInputPara.Add("@Current_State", Convert.ToString(list1.Rows[i]["Current_State"]));

                    hInputPara.Add("@Current_District", Convert.ToString(list1.Rows[i]["Current_District"]));
                    
                    hInputPara.Add("@Current_City", Convert.ToString(list1.Rows[i]["Current_City"]));

                    hInputPara.Add("@Current_Pincode", Convert.ToString(list1.Rows[i]["Current_PinCode"]));

                    hInputPara.Add("@Perm_Address_1", Convert.ToString(list1.Rows[i]["Perm_Address"]));

                    hInputPara.Add("@Perm_Address_2", Convert.ToString(list1.Rows[i]["Perm_Address_2"]));

                    hInputPara.Add("@Perm_State", Convert.ToString(list1.Rows[i]["Perm_State"]));

                    hInputPara.Add("@Perm_District", Convert.ToString(list1.Rows[i]["Perm_District"]));

                    hInputPara.Add("@Perm_City", Convert.ToString(list1.Rows[i]["Perm_City"]));

                    hInputPara.Add("@Perm_Pincode", Convert.ToString(list1.Rows[i]["Perm_PinCode"]));

                    hInputPara.Add("@Emergency_Contact_Name", Convert.ToString(list1.Rows[i]["Emergency_Name"]));

                    hInputPara.Add("@Emergency_Contact_No", Convert.ToString(list1.Rows[i]["Emergency_Phone"]));

                    hInputPara.Add("@Relationship_with_Employee", Convert.ToString(list1.Rows[i]["Relationship_With_Emp"]));

                    hInputPara.Add("@Blood_Group", Convert.ToString(list1.Rows[i]["Blood_Group"]));

                    hInputPara.Add("@Aadhar_No", Convert.ToString(list1.Rows[i]["Aadhar_Id"]));

                    hInputPara.Add("@Pan_No", Convert.ToString(list1.Rows[i]["PAN_Id"]));

                    hInputPara.Add("@Bank_Name", Convert.ToString(list1.Rows[i]["Bank_Name"]));

                    hInputPara.Add("@Bank_Address1", Convert.ToString(list1.Rows[i]["Bank_Address"]));

                    hInputPara.Add("@Bank_State", Convert.ToString(list1.Rows[i]["Bank_State"]));

                    hInputPara.Add("@Bank_City", Convert.ToString(list1.Rows[i]["Bank_City"]));

                    hInputPara.Add("@Bank_Locality", Convert.ToInt32(list1.Rows[i]["BankCity"]));

                    hInputPara.Add("@Bank_Pincode", Convert.ToString(list1.Rows[i]["Bank_PinCode"]));

                    hInputPara.Add("@Bank_IFSC_Code", Convert.ToString(list1.Rows[i]["Bank_IFSC"]));

                    hInputPara.Add("@Created_By", string.Empty);

                    hInputPara.Add("@Current_City1", Convert.ToString(list1.Rows[i]["Current_City1"]));

                    hInputPara.Add("@Perm_City1", Convert.ToString(list1.Rows[i]["Perm_City1"]));

                    hInputPara.Add("@Bank_District", Convert.ToString(list1.Rows[i]["Bank_District"]));

                    //hInputPara.Add("@Current", Convert.ToInt32(list1.Rows[i]["Current"]));

                    hInputPara.Add("@Current", Convert.ToString(list1.Rows[i]["EmpCurrent"]));

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertEmployees", hInputPara);

                    if (Convert.ToInt32(result) == 1)
                    {
                        return RedirectToAction("Index", "Ival_Employee");
                    }
                    else
                    {
                        return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                //e.InnerException.ToString();
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, Message = "Employee Details Added SuccessFull" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateEmployee(String str)
        {
            string resultStr = string.Empty;
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

            try
            {
                IEnumerable<ival_States> ival_StatesList = GetState();
                var currentState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMEmployeeMaster.SelectedCurrentStateId).Select(row => row.State_Name.ToString());
                var permenantState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMEmployeeMaster.SelectedPermenantStateId).Select(row => row.State_Name.ToString());
                var bankState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMEmployeeMaster.SelectedBankStateId).Select(row => row.State_Name.ToString());

                IEnumerable<ival_Locality> pincodeList = GetLocality();
                var currentPincode = pincodeList.AsEnumerable().Where(row => row.Loc_Id == vMEmployeeMaster.SelectedCurrentPincodeID).Select(row => row.Loc_Name.ToString());
                var permenantPincode = pincodeList.AsEnumerable().Where(row => row.Loc_Id == vMEmployeeMaster.SelectedPermanentPincodeID).Select(row => row.Loc_Name.ToString());
                var bankPincode = pincodeList.AsEnumerable().Where(row => row.Loc_Id == vMEmployeeMaster.SelectedBankPincodeID).Select(row => row.Loc_Name.ToString());


                for (int i = 0; i < list1.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();

                    hInputPara.Add("@Employee_ID", Convert.ToInt32(list1.Rows[i]["Emp_ID"]));

                    hInputPara.Add("@Role_ID", Convert.ToInt32(list1.Rows[i]["Role_ID"]));

                    hInputPara.Add("@Branch_ID", Convert.ToInt32(list1.Rows[i]["Branch_Id"]));

                    hInputPara.Add("@First_Name", Convert.ToString(list1.Rows[i]["First_name"]));

                    hInputPara.Add("@Middle_Name", Convert.ToString(list1.Rows[i]["Middle_name"]));

                    hInputPara.Add("@Last_Name", Convert.ToString(list1.Rows[i]["Last_name"]));

                    hInputPara.Add("@Contact_Number", Convert.ToString(list1.Rows[i]["Contact_Number"]));

                    hInputPara.Add("@Company_Email_ID", Convert.ToString(list1.Rows[i]["Company_Mail"]));

                    hInputPara.Add("@Personal_Email_ID", Convert.ToString(list1.Rows[i]["Personal_Mail"]));

                    hInputPara.Add("@Current_Address_1", Convert.ToString(list1.Rows[i]["Current_Address"]));

                    hInputPara.Add("@Current_Address_2", Convert.ToString(list1.Rows[i]["Current_Address_2"]));

                    hInputPara.Add("@Current_State", Convert.ToString(list1.Rows[i]["Current_State"]));

                    hInputPara.Add("@Current_District", Convert.ToString(list1.Rows[i]["Current_District"]));

                    hInputPara.Add("@Current_City", Convert.ToString(list1.Rows[i]["Current_City"]));

                    hInputPara.Add("@Current_Pincode", Convert.ToString(list1.Rows[i]["Current_PinCode"]));

                    hInputPara.Add("@Perm_Address_1", Convert.ToString(list1.Rows[i]["Perm_Address"]));

                    hInputPara.Add("@Perm_Address_2", Convert.ToString(list1.Rows[i]["Perm_Address_2"]));

                    hInputPara.Add("@Perm_State", Convert.ToString(list1.Rows[i]["Perm_State"]));

                    hInputPara.Add("@Perm_District", Convert.ToString(list1.Rows[i]["Perm_District"]));

                    hInputPara.Add("@Perm_City", Convert.ToString(list1.Rows[i]["Perm_City"]));

                    hInputPara.Add("@Perm_Pincode", Convert.ToString(list1.Rows[i]["Perm_PinCode"]));

                    hInputPara.Add("@Emergency_Contact_Name", Convert.ToString(list1.Rows[i]["Emergency_Name"]));

                    hInputPara.Add("@Emergency_Contact_No", Convert.ToString(list1.Rows[i]["Emergency_Phone"]));

                    hInputPara.Add("@Relationship_with_Employee", Convert.ToString(list1.Rows[i]["Relationship_With_Emp"]));

                    hInputPara.Add("@Blood_Group", Convert.ToString(list1.Rows[i]["Blood_Group"]));

                    hInputPara.Add("@Aadhar_No", Convert.ToString(list1.Rows[i]["Aadhar_Id"]));

                    hInputPara.Add("@Pan_No", Convert.ToString(list1.Rows[i]["PAN_Id"]));

                    hInputPara.Add("@Bank_Name", Convert.ToString(list1.Rows[i]["Bank_Name"]));

                    hInputPara.Add("@Bank_Address1", Convert.ToString(list1.Rows[i]["Bank_Address"]));

                    hInputPara.Add("@Bank_State", Convert.ToString(list1.Rows[i]["Bank_State"]));

                    hInputPara.Add("@Bank_City", Convert.ToString(list1.Rows[i]["Bank_City"]));

                    hInputPara.Add("@Bank_Locality", Convert.ToString(list1.Rows[i]["BankCity"]));

                    hInputPara.Add("@Bank_Pincode", Convert.ToString(list1.Rows[i]["Bank_PinCode"]));

                    hInputPara.Add("@Bank_IFSC_Code", Convert.ToString(list1.Rows[i]["Bank_IFSC"]));

                    hInputPara.Add("@Updated_By", string.Empty);

                    hInputPara.Add("@Current_City1", Convert.ToString(list1.Rows[i]["Current_City1"]));

                    hInputPara.Add("@Perm_City1", Convert.ToString(list1.Rows[i]["Perm_City1"]));

                    hInputPara.Add("@Bank_District", Convert.ToString(list1.Rows[i]["Bank_District"]));

                    hInputPara.Add("@Current", Convert.ToString(list1.Rows[i]["EmpCurrent"]));

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateEmployeeDetailsByEmpID", hInputPara);

                    if (Convert.ToInt32(result) == 1)
                    {
                        return RedirectToAction("Index", "Ival_Employee");
                    }
                    else
                    {
                        return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                //e.InnerException.ToString();
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, Message = "SuccessFull" }, JsonRequestBehavior.AllowGet);
        }

        /* [HttpPost]
         public ActionResult EditEmployee(VMEmployeeMaster vMEmployeeMaster)
         {

             try
             {
                 IEnumerable<ival_States> ival_StatesList = GetState();

                 var currentState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMEmployeeMaster.SelectedCurrentStateId).Select(row => row.State_Name.ToString());
                 var permenantState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMEmployeeMaster.SelectedPermenantStateId).Select(row => row.State_Name.ToString());
                 var bankState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMEmployeeMaster.SelectedBankStateId).Select(row => row.State_Name.ToString());

                 *//*IEnumerable<ival_Locality> pincodeList = GetPincode();
                 var currentPincode = pincodeList.AsEnumerable().Where(row => row.Lookup_ID == vMEmployeeMaster.SelectedCurrentPincodeID).Select(row => row.Lookup_Value.ToString());
                 var permenantPincode = pincodeList.AsEnumerable().Where(row => row.Lookup_ID == vMEmployeeMaster.SelectedPermanentPincodeID).Select(row => row.Lookup_Value.ToString());
                 var bankPincode = pincodeList.AsEnumerable().Where(row => row.Lookup_ID == vMEmployeeMaster.SelectedBankPincodeID).Select(row => row.Lookup_Value.ToString());
 *//*
                 hInputPara = new Hashtable();
                 sqlDataAccess = new SQLDataAccess();

                 hInputPara = new Hashtable();

                 hInputPara.Add("@Employee_ID", Convert.ToInt32(vMEmployeeMaster.ival_EmployeeMaster.Employee_ID));

                 hInputPara.Add("@Role_ID", Convert.ToInt32(vMEmployeeMaster.SelectedRoleId.ToString()));

                 hInputPara.Add("@Branch_ID", Convert.ToInt32(vMEmployeeMaster.SelectedBranchId.ToString()));

                 hInputPara.Add("@First_Name", vMEmployeeMaster.ival_EmployeeMaster.First_Name.ToString());

                 hInputPara.Add("@Middle_Name", vMEmployeeMaster.ival_EmployeeMaster.Middle_Name.ToString());

                 hInputPara.Add("@Last_Name", vMEmployeeMaster.ival_EmployeeMaster.Last_Name.ToString());

                 hInputPara.Add("@Contact_Number", vMEmployeeMaster.Contact_Number.ToString());

                 hInputPara.Add("@Company_Email_ID", vMEmployeeMaster.ival_EmployeeMaster.Company_Email_ID.ToString());

                 hInputPara.Add("@Personal_Email_ID", vMEmployeeMaster.ival_EmployeeMaster.Personal_Email_ID.ToString());

                 hInputPara.Add("@Current_Address_1", vMEmployeeMaster.ival_EmployeeMaster.Current_Address_1.ToString());

                 hInputPara.Add("@Current_Address_2", vMEmployeeMaster.ival_EmployeeMaster.Current_Address_2.ToString());

                 *//*hInputPara.Add("@Current_State", vMEmployeeMaster.ival_EmployeeMaster.Current_State.ToString());*//*

                 hInputPara.Add("@Current_State", Convert.ToInt32(vMEmployeeMaster.SelectedBankStateId.ToString()));

                 hInputPara.Add("@Current_District", Convert.ToInt32(vMEmployeeMaster.SelectedBankDistId.ToString())); 

                 hInputPara.Add("@Current_City", Convert.ToInt32(vMEmployeeMaster.SelectedBankCityID.ToString()));

                 *//* hInputPara.Add("@Current_State", Convert.ToString(currentState.FirstOrDefault()));

                  hInputPara.Add("@Current_Pincode", Convert.ToInt32(currentPincode.FirstOrDefault()));*//*



                 hInputPara.Add("@Current_Pincode", Convert.ToInt32(vMEmployeeMaster.SelectedBankPincodeID.ToString()));

                 hInputPara.Add("@Perm_Address_1", vMEmployeeMaster.ival_EmployeeMaster.Perm_Address_1.ToString());

                 hInputPara.Add("@Perm_Address_2", vMEmployeeMaster.ival_EmployeeMaster.Perm_Address_2.ToString());

                 hInputPara.Add("@Perm_City", Convert.ToInt32(vMEmployeeMaster.SelectedPermenantCityID.ToString()));

                 hInputPara.Add("@Perm_District", Convert.ToInt32(vMEmployeeMaster.SelectedPermenantDistId.ToString()));

                 //hInputPara.Add("@Perm_State", Convert.ToString(permenantState.ToString()));

                 // hInputPara.Add("@Perm_Pincode", Convert.ToInt32(permenantPincode.FirstOrDefault()));
                 hInputPara.Add("@Perm_State", Convert.ToInt32(vMEmployeeMaster.SelectedPermenantStateId.ToString()));

                 hInputPara.Add("@Perm_Pincode", Convert.ToInt32(vMEmployeeMaster.SelectedPermanentPincodeID.ToString()));

                 hInputPara.Add("@Emergency_Contact_Name", vMEmployeeMaster.ival_EmployeeMaster.Emergency_Contact_Name.ToString());

                 hInputPara.Add("@Emergency_Contact_No", vMEmployeeMaster.ival_EmployeeMaster.Emergency_Contact_No.ToString());

                 hInputPara.Add("@Relationship_with_Employee", vMEmployeeMaster.SelectedRelationWithEmpID.ToString());

                 hInputPara.Add("@Blood_Group", vMEmployeeMaster.SelectedBloodGrpID.ToString());

                 hInputPara.Add("@Aadhar_No", vMEmployeeMaster.ival_EmployeeMaster.Aadhar_No.ToString());

                 hInputPara.Add("@Pan_No", vMEmployeeMaster.ival_EmployeeMaster.Pan_No.ToString());

                 hInputPara.Add("@Bank_Name", vMEmployeeMaster.ival_EmployeeMaster.Bank_Name.ToString());

                 hInputPara.Add("@Bank_Address1", vMEmployeeMaster.ival_EmployeeMaster.Bank_Address1.ToString());

                 hInputPara.Add("@Bank_City", Convert.ToInt32(vMEmployeeMaster.SelectedBankCityID.ToString()));

                 hInputPara.Add("@Bank_State", Convert.ToInt32(vMEmployeeMaster.SelectedBankStateId.ToString()));

                 // hInputPara.Add("@Bank_Pincode", Convert.ToString(bankPincode.FirstOrDefault()));

                 hInputPara.Add("@Bank_Pincode", vMEmployeeMaster.SelectedBankPincodeID.ToString());

                 hInputPara.Add("@Bank_IFSC_Code", vMEmployeeMaster.ival_EmployeeMaster.Bank_IFSC_Code.ToString());

                 hInputPara.Add("@Updated_By", string.Empty);

                 hInputPara.Add("@Current_City1", Convert.ToInt32(vMEmployeeMaster.Loc_Id.ToString()));

                 hInputPara.Add("@Perm_City1", Convert.ToInt32(vMEmployeeMaster.PermLoc_Id.ToString()));

                 hInputPara.Add("@Bank_District", Convert.ToInt32(vMEmployeeMaster.SelectedBankDistId.ToString()));

                 var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateEmployeeDetailsByEmpID", hInputPara);

                 if (Convert.ToInt32(result) == 1)
                 {
                     return RedirectToAction("Index", "Ival_Employee");
                 }
                 else
                 {
                     return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                 }
             }
             catch (Exception e)
             {
                 //e.InnerException.ToString();
                 return Json(e.Message, JsonRequestBehavior.AllowGet);
             }

         }*/



        [HttpGet]
        public ActionResult ViewEmployeeDetails(int ID)
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //return View();
            }
            vMEmployeeMaster = new VMEmployeeMaster();
            objEmp = new ival_EmployeeMaster();
            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Employee_ID", ID);
                DataTable dtEmp = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetEmployeesByEmpId", hInputPara);

                if (dtEmp.Rows.Count > 0 && dtEmp != null)

                {
                    object valueEmployee_ID = Convert.ToInt32(dtEmp.Rows[0]["Employee_ID"]);
                    if (dtEmp.Columns.Contains("Employee_ID") && valueEmployee_ID != null)
                        objEmp.Employee_ID = Convert.ToInt32(valueEmployee_ID);
                    else
                        objEmp.Employee_ID = 0;

                    object valueFirst_Name = dtEmp.Rows[0]["First_Name"];
                    if (dtEmp.Columns.Contains("First_Name") && valueFirst_Name != null && valueFirst_Name.ToString() != "")
                        objEmp.First_Name = dtEmp.Rows[0]["First_Name"].ToString();
                    else
                        objEmp.First_Name = string.Empty;


                    object valueMiddle_Name = dtEmp.Rows[0]["Middle_Name"];
                    if (dtEmp.Columns.Contains("Middle_Name") && valueMiddle_Name != null && valueMiddle_Name.ToString() != "")
                        objEmp.Middle_Name = dtEmp.Rows[0]["Middle_Name"].ToString();
                    else
                        objEmp.Middle_Name = string.Empty;

                    object valueLast_Name = dtEmp.Rows[0]["Last_Name"];
                    if (dtEmp.Columns.Contains("Last_Name") && valueLast_Name != null && valueLast_Name.ToString() != "")
                        objEmp.Last_Name = dtEmp.Rows[0]["Last_Name"].ToString();
                    else
                        objEmp.Last_Name = string.Empty;

                    object valueCompany_Email_ID = dtEmp.Rows[0]["Company_Email_ID"];
                    if (dtEmp.Columns.Contains("Company_Email_ID") && valueCompany_Email_ID != null && valueCompany_Email_ID.ToString() != "")
                        objEmp.Company_Email_ID = dtEmp.Rows[0]["Company_Email_ID"].ToString();
                    else
                        objEmp.Company_Email_ID = string.Empty;

                    object valueBranch_ID = dtEmp.Rows[0]["Branch_ID"];
                    if (dtEmp.Columns.Contains("Branch_ID") && valueBranch_ID != null)
                        objEmp.Branch_ID = Convert.ToInt32(dtEmp.Rows[0]["Branch_ID"].ToString());
                    else
                        objEmp.Branch_ID = 0;

                    object valueRole_ID = dtEmp.Rows[0]["Role_ID"];
                    if (dtEmp.Columns.Contains("Role_ID") && valueRole_ID != null)
                        objEmp.Role_ID = Convert.ToInt32(dtEmp.Rows[0]["Role_ID"].ToString());
                    else
                        objEmp.Role_ID = 0;


                    object valueContact_Number = dtEmp.Rows[0]["Contact_Number"];
                    if (dtEmp.Columns.Contains("Contact_Number") && valueContact_Number != null && valueContact_Number.ToString() != "")
                        vMEmployeeMaster.Contact_Number = Convert.ToString(dtEmp.Rows[0]["Contact_Number"].ToString());
                   
                    else
                        vMEmployeeMaster.Contact_Number = string.Empty;

                    object valuePersonal_Email_ID = dtEmp.Rows[0]["Personal_Email_ID"];
                    if (dtEmp.Columns.Contains("Personal_Email_ID") && valuePersonal_Email_ID != null && valuePersonal_Email_ID.ToString() != "")
                        objEmp.Personal_Email_ID = dtEmp.Rows[0]["Personal_Email_ID"].ToString();
                    else
                        objEmp.Personal_Email_ID = string.Empty;

                    object valueCurrent_Address_1 = dtEmp.Rows[0]["Current_Address_1"];
                    if (dtEmp.Columns.Contains("Current_Address_1") && valueCurrent_Address_1 != null && valueCurrent_Address_1.ToString() != "")
                        objEmp.Current_Address_1 = dtEmp.Rows[0]["Current_Address_1"].ToString();
                    else
                        objEmp.Current_Address_1 = string.Empty;

                    object valueCurrent_Address_2 = dtEmp.Rows[0]["Current_Address_2"];
                    if (dtEmp.Columns.Contains("Current_Address_2") && valueCurrent_Address_2 != null && valueCurrent_Address_2.ToString() != "")
                        objEmp.Current_Address_2 = dtEmp.Rows[0]["Current_Address_2"].ToString();
                    else
                        objEmp.Current_Address_2 = string.Empty;

                    object valueCurrent_City1 = dtEmp.Rows[0]["Current_City1"];
                    if (dtEmp.Columns.Contains("Current_City1") && valueCurrent_City1 != null && valueCurrent_City1.ToString() != "")
                        objEmp.Current_City1 = dtEmp.Rows[0]["Current_City1"].ToString();
                    else
                        objEmp.Current_City1 = string.Empty;

                    object valueCurrent_State = dtEmp.Rows[0]["Current_State"];
                    if (dtEmp.Columns.Contains("Current_State") && valueCurrent_State != null && valueCurrent_State.ToString() != "")
                        objEmp.Current_State = dtEmp.Rows[0]["Current_State"].ToString();
                    else
                        objEmp.Current_State = string.Empty;

                    object valueCurrent_District = dtEmp.Rows[0]["Current_District"];
                    if (dtEmp.Columns.Contains("Current_District") && valueCurrent_District != null && valueCurrent_District.ToString() != "")
                        objEmp.Current_District = dtEmp.Rows[0]["Current_District"].ToString();
                    else
                        objEmp.Current_District = string.Empty;


                    object valueCurrent_City = dtEmp.Rows[0]["Current_City"];
                    if (dtEmp.Columns.Contains("Current_City") && valueCurrent_City != null && valueCurrent_City.ToString() != "")
                        objEmp.Current_City = dtEmp.Rows[0]["Current_City"].ToString();
                    else
                        objEmp.Current_City = string.Empty;

                    object valueCurrent_Pincode = dtEmp.Rows[0]["Current_Pincode"];
                    if (dtEmp.Columns.Contains("Current_Pincode") && valueCurrent_Pincode != null)
                        objEmp.Current_Pincode = Convert.ToInt32(dtEmp.Rows[0]["Current_Pincode"].ToString());
                    else
                        objEmp.Current_Pincode = 0;

                    object valuePerm_Address_1 = dtEmp.Rows[0]["Perm_Address_1"];
                    if (dtEmp.Columns.Contains("Perm_Address_1") && valuePerm_Address_1 != null && valuePerm_Address_1.ToString() != "")
                        objEmp.Perm_Address_1 = dtEmp.Rows[0]["Perm_Address_1"].ToString();
                    else
                        objEmp.Perm_Address_1 = string.Empty;

                    object valuePerm_Address_2 = dtEmp.Rows[0]["Perm_Address_2"];
                    if (dtEmp.Columns.Contains("Perm_Address_2") && valuePerm_Address_2 != null && valuePerm_Address_2.ToString() != "")
                        objEmp.Perm_Address_2 = dtEmp.Rows[0]["Perm_Address_2"].ToString();
                    else
                        objEmp.Perm_Address_2 = string.Empty;

                    object valuePerm_City1 = dtEmp.Rows[0]["Perm_City1"];
                    if (dtEmp.Columns.Contains("Perm_City1") && valuePerm_City1 != null && valuePerm_City1.ToString() != "")
                        objEmp.Perm_City1 = dtEmp.Rows[0]["Perm_City1"].ToString();
                    else
                        objEmp.Perm_City1 = string.Empty;

                    object valuePerm_State = dtEmp.Rows[0]["Perm_State"];
                    if (dtEmp.Columns.Contains("Perm_State") && valuePerm_State != null && valuePerm_State.ToString() != "")
                        objEmp.Perm_State = dtEmp.Rows[0]["Perm_State"].ToString();
                    else
                        objEmp.Perm_State = string.Empty;

                    object valuePerm_District = dtEmp.Rows[0]["Perm_District"];
                    if (dtEmp.Columns.Contains("Perm_District") && valuePerm_District != null && valuePerm_District.ToString() != "")
                        objEmp.Perm_District = dtEmp.Rows[0]["Perm_District"].ToString();
                    else
                        objEmp.Perm_District = string.Empty;

                    object valuePerm_City = dtEmp.Rows[0]["Perm_City"];
                    if (dtEmp.Columns.Contains("Perm_City") && valuePerm_City != null && valuePerm_City.ToString() != "")
                        objEmp.Perm_City = dtEmp.Rows[0]["Perm_City"].ToString();
                    else
                        objEmp.Perm_City = string.Empty;

                    object valuePerm_Pincode = dtEmp.Rows[0]["Perm_Pincode"];
                    if (dtEmp.Columns.Contains("Perm_Pincode") && valuePerm_Pincode != null)
                        objEmp.Perm_Pincode = Convert.ToInt32(dtEmp.Rows[0]["Perm_Pincode"].ToString());
                    else
                        objEmp.Perm_Pincode = 0;

                    object valueEmergency_Contact_Name = dtEmp.Rows[0]["Emergency_Contact_Name"];
                    if (dtEmp.Columns.Contains("Emergency_Contact_Name") && valueEmergency_Contact_Name != null && valueEmergency_Contact_Name.ToString() != "")
                        objEmp.Emergency_Contact_Name = dtEmp.Rows[0]["Emergency_Contact_Name"].ToString();
                    else
                        objEmp.Emergency_Contact_Name = string.Empty;

                    object valueEmergency_Contact_No = dtEmp.Rows[0]["Emergency_Contact_No"];
                    if (dtEmp.Columns.Contains("Emergency_Contact_No") && valueEmergency_Contact_No != null && valueEmergency_Contact_No.ToString() != "")
                        objEmp.Emergency_Contact_No = dtEmp.Rows[0]["Emergency_Contact_No"].ToString();
                    else
                        objEmp.Emergency_Contact_No = string.Empty;

                    object valueRelationship_with_Employee = dtEmp.Rows[0]["Relationship_with_Employee"];
                    if (dtEmp.Columns.Contains("Relationship_with_Employee") && valueRelationship_with_Employee != null && valueRelationship_with_Employee.ToString() != "")
                        objEmp.Relationship_with_Employee = dtEmp.Rows[0]["Relationship_with_Employee"].ToString();
                    else
                        objEmp.Relationship_with_Employee = string.Empty;

                    object valueBlood_Group = dtEmp.Rows[0]["Blood_Group"];
                    if (dtEmp.Columns.Contains("Blood_Group") && valueBlood_Group != null && valueBlood_Group.ToString() != "")
                    {
                        objEmp.Blood_Group = dtEmp.Rows[0]["Blood_Group"].ToString();
                        vMEmployeeMaster.SelectedBloodGrpID = dtEmp.Rows[0]["Blood_Group"].ToString();
                    }
                    else
                        objEmp.Blood_Group = string.Empty;

                    object valueAadhar_No = dtEmp.Rows[0]["Aadhar_No"];
                    if (dtEmp.Columns.Contains("Aadhar_No") && valueAadhar_No != null && valueAadhar_No.ToString() != "")
                        objEmp.Aadhar_No = dtEmp.Rows[0]["Aadhar_No"].ToString();
                    else
                        objEmp.Aadhar_No = string.Empty;

                    object valuePan_No = dtEmp.Rows[0]["Pan_No"];
                    if (dtEmp.Columns.Contains("Pan_No") && valuePan_No != null && valuePan_No.ToString() != "")
                        objEmp.Pan_No = dtEmp.Rows[0]["Pan_No"].ToString();
                    else
                        objEmp.Pan_No = string.Empty;

                    object valueBank_Name = dtEmp.Rows[0]["Bank_Name"];
                    if (dtEmp.Columns.Contains("Bank_Name") && valueBank_Name != null && valueBank_Name.ToString() != "")
                        objEmp.Bank_Name = dtEmp.Rows[0]["Bank_Name"].ToString();
                    else
                        objEmp.Bank_Name = string.Empty;

                    object valueBank_Address1 = dtEmp.Rows[0]["Bank_Address1"];
                    if (dtEmp.Columns.Contains("Bank_Address1") && valueBank_Address1 != null && valueBank_Address1.ToString() != "")
                        objEmp.Bank_Address1 = dtEmp.Rows[0]["Bank_Address1"].ToString();
                    else
                        objEmp.Bank_Address1 = string.Empty;

                    object valueBank_Address2 = dtEmp.Rows[0]["Bank_Address2"];
                    if (dtEmp.Columns.Contains("Bank_Address2") && valueBank_Address2 != null && valueBank_Address2.ToString() != "")
                        objEmp.Bank_Address2 = dtEmp.Rows[0]["Bank_Address2"].ToString();
                    else
                        objEmp.Bank_Address2 = string.Empty;

                    object valueBank_State = dtEmp.Rows[0]["Bank_State"];
                    if (dtEmp.Columns.Contains("Bank_State") && valueBank_State != null && valueBank_State.ToString() != "")
                        objEmp.Bank_State = dtEmp.Rows[0]["Bank_State"].ToString();
                    else
                        objEmp.Bank_State = string.Empty;

                    object valueBank_District = dtEmp.Rows[0]["Bank_District"];
                    if (dtEmp.Columns.Contains("Bank_District") && valueBank_District != null && valueBank_District.ToString() != "")
                        objEmp.Bank_District = dtEmp.Rows[0]["Bank_District"].ToString();
                    else
                        objEmp.Bank_District = string.Empty;

                    object valueBank_City = dtEmp.Rows[0]["Bank_City"];
                    if (dtEmp.Columns.Contains("Bank_City") && valueBank_City != null && valueBank_City.ToString() != "")
                        objEmp.Bank_City = dtEmp.Rows[0]["Bank_City"].ToString();
                    else
                        objEmp.Bank_City = string.Empty;

                    object valueBank_Locality = dtEmp.Rows[0]["Bank_Locality"];
                    if (dtEmp.Columns.Contains("Bank_Locality") && valueBank_Locality != null && valueBank_Locality.ToString() != "")
                        objEmp.Bank_Locality = dtEmp.Rows[0]["Bank_Locality"].ToString();
                    else
                        objEmp.Bank_Locality = string.Empty;

                    object valueBank_Pincode = dtEmp.Rows[0]["Bank_Pincode"];
                    if (dtEmp.Columns.Contains("Bank_Pincode") && valueBank_Pincode != null && valueBank_Pincode.ToString() != "")
                        objEmp.Bank_Pincode = dtEmp.Rows[0]["Bank_Pincode"].ToString();
                    else
                        objEmp.Bank_Pincode = string.Empty;


                    object valueBank_IFSC_Code = dtEmp.Rows[0]["Bank_IFSC_Code"];
                    if (dtEmp.Columns.Contains("Bank_IFSC_Code") && valueBank_IFSC_Code != null && valueBank_IFSC_Code.ToString() != "")
                        objEmp.Bank_IFSC_Code = dtEmp.Rows[0]["Bank_IFSC_Code"].ToString();
                    else
                        objEmp.Bank_IFSC_Code = string.Empty;

                    object valueRole_ID1 = dtEmp.Rows[0]["Role_ID"];
                    if (dtEmp.Columns.Contains("Role_ID") && valueRole_ID1 != null)
                        vMEmployeeMaster.SelectedRoleId = Convert.ToInt32(dtEmp.Rows[0]["Role_ID"].ToString());
                    else
                        vMEmployeeMaster.SelectedRoleId = 0;

                    object valueCurrent_Pincode1 = dtEmp.Rows[0]["Current_Pincode"];
                    if (dtEmp.Columns.Contains("Current_Pincode") && valueCurrent_Pincode1 != null)
                        vMEmployeeMaster.SelectedCurrentPincodeID = Convert.ToInt32(dtEmp.Rows[0]["Current_Pincode"].ToString());
                    else
                        vMEmployeeMaster.SelectedCurrentPincodeID = 0;

                    object valuePerm_Pincode1 = dtEmp.Rows[0]["Perm_Pincode"];
                    if (dtEmp.Columns.Contains("Perm_Pincode") && valuePerm_Pincode1 != null)
                        vMEmployeeMaster.SelectedPermanentPincodeID = Convert.ToInt32(dtEmp.Rows[0]["Perm_Pincode"].ToString());
                    else
                        vMEmployeeMaster.SelectedPermanentPincodeID = 0;

                    /* object valueBank_Pincode1 = dtEmp.Rows[0]["Bank_Pincode"];
                     if (dtEmp.Columns.Contains("Bank_Pincode") && valueBank_Pincode1 != null)
                         vMEmployeeMaster.SelectedBankPincodeID = Convert.ToInt32(dtEmp.Rows[0]["Bank_Pincode"].ToString());
                     else
                         vMEmployeeMaster.SelectedBankPincodeID = 0;*/
               
                    vMEmployeeMaster.ival_RoleMastersList = GetRoles();
                    vMEmployeeMaster.ival_BranchMastersList = GetBranch();
                    vMEmployeeMaster.ival_LocalityList = GetLocality();
                    vMEmployeeMaster.ival_DistrictsList = GetDistricts();
                    //vMEmployeeMaster.pincodeList = GetPincode();
                    vMEmployeeMaster.ival_CitiesList = GetCity();
                    vMEmployeeMaster.ival_StatesList = GetState();
                    vMEmployeeMaster.bloodGrpList = GetBloodGrp();
                    vMEmployeeMaster.relationWithEmpList = GetRelationsWithEmp();
                    /* IEnumerable<ival_States> ival_StatesList = GetState();
                     if (ival_StatesList.ToList().Count > 0 && ival_StatesList != null)
                     {
                         var currentState = ival_StatesList.Where(s => s.State_Name == dtEmp.Rows[0]["Current_State"].ToString()).Select(s => s.State_ID);
                         var permenantState = ival_StatesList.Where(s => s.State_Name == dtEmp.Rows[0]["Perm_State"].ToString()).Select(s => s.State_ID);
                         var bankState = ival_StatesList.Where(s => s.State_Name == dtEmp.Rows[0]["Bank_State"].ToString()).Select(s => s.State_ID);

                         vMEmployeeMaster.SelectedRelationWithEmpID = objEmp.Relationship_with_Employee;
                         vMEmployeeMaster.SelectedCurrentStateId = Convert.ToInt32(currentState.FirstOrDefault());
                         vMEmployeeMaster.SelectedPermenantStateId = Convert.ToInt32(permenantState.FirstOrDefault());
                         vMEmployeeMaster.SelectedBankStateId = Convert.ToInt32(bankState.FirstOrDefault());
                     }
                     IEnumerable<ival_Districts> ival_DistrictsList = GetDistricts();
                     if (ival_DistrictsList.ToList().Count > 0 && ival_DistrictsList != null)
                     {
                         var currentDist = ival_DistrictsList.Where(s => s.District_Name == dtEmp.Rows[0]["Current_District"].ToString()).Select(s => s.District_ID);
                         var permenantDist = ival_DistrictsList.Where(s => s.District_Name == dtEmp.Rows[0]["Perm_District"].ToString()).Select(s => s.District_ID);
                         var bankDist = ival_DistrictsList.Where(s => s.District_Name == dtEmp.Rows[0]["Bank_District"].ToString()).Select(s => s.District_ID);

                         vMEmployeeMaster.SelectedCurrentDistId = Convert.ToInt32(currentDist.FirstOrDefault());
                         vMEmployeeMaster.SelectedPermenantDistId = Convert.ToInt32(permenantDist.FirstOrDefault());
                         vMEmployeeMaster.SelectedBankDistId = Convert.ToInt32(bankDist.FirstOrDefault());
                     }

                     IEnumerable<ival_Locality> pincodeList = GetPincode();
                     if (pincodeList.ToList().Count > 0 && pincodeList != null)
                     {
                         var currentPincode = pincodeList.Where(s => s.Loc_Name == dtEmp.Rows[0]["Current_Pincode"].ToString()).Select(s => s.Loc_Id);
                         var permenantPincode = pincodeList.Where(s => s.Loc_Name == dtEmp.Rows[0]["Current_Pincode"].ToString()).Select(s => s.Loc_Id);
                         var bankPincode = pincodeList.Where(s => s.Loc_Name == dtEmp.Rows[0]["Current_Pincode"].ToString()).Select(s => s.Loc_Id);

                         vMEmployeeMaster.SelectedCurrentPincodeID = Convert.ToInt32(currentPincode.FirstOrDefault());
                         vMEmployeeMaster.SelectedPermanentPincodeID = Convert.ToInt32(permenantPincode.FirstOrDefault());
                         vMEmployeeMaster.SelectedBankPincodeID = Convert.ToInt32(bankPincode.FirstOrDefault());
                     }

                     IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                     if (ival_LocalityList.ToList().Count > 0 && ival_LocalityList != null)
                     {
                         var currentLocality = ival_LocalityList.Where(s => s.Loc_Name == dtEmp.Rows[0]["Current_City"].ToString()).Select(s => s.Loc_Id);
                         var permenantLocality = ival_LocalityList.Where(s => s.Loc_Name == dtEmp.Rows[0]["Perm_City"].ToString()).Select(s => s.Loc_Id);
                         var bankLocality = ival_LocalityList.Where(s => s.Loc_Name == dtEmp.Rows[0]["Bank_City"].ToString()).Select(s => s.Loc_Id);

                         vMEmployeeMaster.SelectedCurrentCityID = Convert.ToInt32(currentLocality.FirstOrDefault());
                         vMEmployeeMaster.SelectedPermenantCityID = Convert.ToInt32(permenantLocality.FirstOrDefault());
                         vMEmployeeMaster.SelectedBankCityID = Convert.ToInt32(bankLocality.FirstOrDefault());
                     }*/

                    vMEmployeeMaster.SelectedBranchId = Convert.ToInt32(dtEmp.Rows[0]["Branch_ID"]);
                    vMEmployeeMaster.ival_EmployeeMaster = objEmp;
                    vMEmployeeMaster.Loc_Id = Convert.ToInt32(dtEmp.Rows[0]["Current_City1"]);
                    vMEmployeeMaster.PermLoc_Id = Convert.ToInt32(dtEmp.Rows[0]["Perm_City1"]);
                    vMEmployeeMaster.SelectedCurrentStateId = Convert.ToInt32(dtEmp.Rows[0]["Current_State"]);
                    vMEmployeeMaster.SelectedPermenantStateId = Convert.ToInt32(dtEmp.Rows[0]["Perm_State"]);
                    vMEmployeeMaster.SelectedBankStateId = Convert.ToInt32(dtEmp.Rows[0]["Bank_State"]);
                    vMEmployeeMaster.SelectedCurrentDistId = Convert.ToInt32(dtEmp.Rows[0]["Current_District"]);
                    vMEmployeeMaster.SelectedPermenantDistId = Convert.ToInt32(dtEmp.Rows[0]["Perm_District"]);
                    vMEmployeeMaster.SelectedBankDistId = Convert.ToInt32(dtEmp.Rows[0]["Bank_District"]);
                    vMEmployeeMaster.SelectedCurrentCityID = Convert.ToInt32(dtEmp.Rows[0]["Current_City"]);
                    vMEmployeeMaster.SelectedPermenantCityID = Convert.ToInt32(dtEmp.Rows[0]["Perm_City"]);
                    vMEmployeeMaster.SelectedBankCityID = Convert.ToInt32(dtEmp.Rows[0]["Bank_City"]);
                    vMEmployeeMaster.SelectedCurrentPincodeID = Convert.ToInt32(dtEmp.Rows[0]["Current_City"]);
                    vMEmployeeMaster.SelectedPermanentPincodeID = Convert.ToInt32(dtEmp.Rows[0]["Perm_City"]);
                    vMEmployeeMaster.SelectedBankPincodeID = Convert.ToInt32(dtEmp.Rows[0]["Bank_City"]);
                    vMEmployeeMaster.BankLoc_Id = Convert.ToInt32(dtEmp.Rows[0]["Bank_Locality"]);
                    vMEmployeeMaster.SelectedRelationWithEmpID = Convert.ToString(dtEmp.Rows[0]["Relationship_with_Employee"]);
                    vMEmployeeMaster.SelectedBloodGrpID = Convert.ToString(dtEmp.Rows[0]["Blood_Group"]);
                    vMEmployeeMaster.Employee_ID = objEmp.Employee_ID;
                }
            }
            catch (Exception ex)
            {
                //ex.InnerException.ToString();
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

            return View(vMEmployeeMaster);
        }

        [HttpPost]
        public ActionResult CheckMail(string str)
        {
             string resultStr = string.Empty;
                sqlDataAccess = new SQLDataAccess();
                hInputPara = new Hashtable();
                hInputPara.Add("@Email_ID", str);
            DataTable result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_CheckMailInLogin", hInputPara);

            if (result.Rows.Count > 0 && result != null)
            {
                //resultStr = result;
                resultStr = "Email Already Exists";
            }

            //hInputPara.Clear();

            if (result.Rows.Count > 0 && result != null)
            {
                return Json(new { success = true, Message = resultStr }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false}, JsonRequestBehavior.AllowGet);
            }
    }

        [HttpGet]
        public ActionResult Delete(int ID)
        {
            try
            {
                sqlDataAccess = new SQLDataAccess();
                hInputPara = new Hashtable();
                hInputPara.Add("@Employee_ID", ID);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteEmployeesByEmpID", hInputPara);
                result.ToString();
                if (Convert.ToInt32(result) == 1)
                {
                    return RedirectToAction("Index", "Ival_Employee");
                }
                else
                {
                    return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public IEnumerable<ival_RoleMaster> GetRoles()
        {
            List<ival_RoleMaster> ListRoles = new List<ival_RoleMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtRoles = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRoleByID");
            for (int i = 0; i < dtRoles.Rows.Count; i++)
            {

                ListRoles.Add(new ival_RoleMaster
                {
                    Role_ID = Convert.ToInt32(dtRoles.Rows[i]["Role_ID"]),
                    Role_Name = Convert.ToString(dtRoles.Rows[i]["Role_Name"])

                });


            }
            return ListRoles.AsEnumerable();
        }

        [HttpPost]
        public IEnumerable<ival_BranchMaster> GetBranch()
        {
            List<ival_BranchMaster> listBranches = new List<ival_BranchMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBranch = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBranchByIDN");
            for (int i = 0; i < dtBranch.Rows.Count; i++)
            {

                listBranches.Add(new ival_BranchMaster
                {
                    Branch_ID = Convert.ToInt32(dtBranch.Rows[i]["Branch_ID"]),
                    Branch_Name = Convert.ToString(dtBranch.Rows[i]["Branch_Name"])

                });


            }
            return listBranches.AsEnumerable();
        }
        [HttpPost]
        public IEnumerable<ival_States> GetState()
        {
            List<ival_States> listState = new List<ival_States>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetStateByID");
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                listState.Add(new ival_States
                {
                    State_ID = Convert.ToInt32(dtState.Rows[i]["State_ID"]),
                    State_Name = Convert.ToString(dtState.Rows[i]["State_Name"])

                });


            }
            return listState.AsEnumerable();
        }

        [HttpPost]
        public IEnumerable<ival_Cities> GetCity()
        {
            List<ival_Cities> listState = new List<ival_Cities>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtCity = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCityByID");
            for (int i = 0; i < dtCity.Rows.Count; i++)
            {

                listState.Add(new ival_Cities
                {
                    Loc_Id = Convert.ToInt32(dtCity.Rows[i]["City_ID"]),
                    City_Name = Convert.ToString(dtCity.Rows[i]["City"])

                });


            }
            return listState.AsEnumerable();
        }


        [HttpPost]
        public IEnumerable<ival_Districts> GetDistricts()
        {
            List<ival_Districts> lists = new List<ival_Districts>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            try
            {
                DataTable dtDistict = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDistrictByID");
                for (int i = 0; i < dtDistict.Rows.Count; i++)
                {
                    lists.Add(new ival_Districts
                    {
                        District_ID = Convert.ToInt32(dtDistict.Rows[i]["District_ID"]),
                        District_Name = Convert.ToString(dtDistict.Rows[i]["District_Name"])
                    });
                }
            }
            catch (Exception)
            {

            }
            return lists.AsEnumerable();
        }


        [HttpPost]
        /*public IEnumerable<ival_Districts> GetDistrictByOnselect1( int ID)
        {
            hInputPara = new Hashtable();
            List<ival_Districts> lists = new List<ival_Districts>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@State_ID", ID);

            
            try
            {
                DataTable dtDistict = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDisticts", hInputPara);
                for (int i = 0; i < dtDistict.Rows.Count; i++)
                {
                    lists.Add(new ival_Districts
                    {
                        District_ID = Convert.ToInt32(dtDistict.Rows[i]["District_ID"]),
                        District_Name = Convert.ToString(dtDistict.Rows[i]["District_Name"])
                    });
                }
            }
            catch (Exception)
            {

            }
            return lists.AsEnumerable();
        }*/


        
        public IEnumerable<ival_Locality> GetLocality()
        {
            List<ival_Locality> listLoc = new List<ival_Locality>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            DataTable dtLocality = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalityByID");
            for (int i = 0; i < dtLocality.Rows.Count; i++)
            {

                listLoc.Add(new ival_Locality
                {
                    Loc_Id = Convert.ToInt32(dtLocality.Rows[i]["Loc_Id"]),
                    Loc_Name = Convert.ToString(dtLocality.Rows[i]["Loc_Name"]),
                    Pin_Code = Convert.ToInt32(dtLocality.Rows[i]["Pin_Code"])

                });
            }
            return listLoc.AsEnumerable();
        }

       /* [HttpPost]
        public IEnumerable<ival_Locality> GetPincode()
        {
            List<ival_Locality> listLocPin = new List<ival_Locality>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            DataTable dtLocPin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalityByID");
            for (int i = 0; i < dtLocPin.Rows.Count; i++)
            {

                listLocPin.Add(new ival_Locality
                {
                    Loc_Id = Convert.ToInt32(dtLocPin.Rows[i]["Loc_Id"]),
                    //Loc_Name = Convert.ToString(listLocPin.Rows[i]["Loc_Name"]),
                    Pin_Code = Convert.ToInt32(dtLocPin.Rows[i]["Pin_Code"])

                });
            }
            return listLocPin.AsEnumerable();
        }*/

        [HttpPost]
        public JsonResult GetDistrictByOnselect(int ID)
        {
            hInputPara = new Hashtable();
            List<ival_EmployeeMaster> DistList = new List<ival_EmployeeMaster>();
            // DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@State_ID", ID);

            DataTable dtLocName = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDisticts", hInputPara);
            for (int i = 0; i < dtLocName.Rows.Count; i++)
            {
                DistList.Add(new ival_EmployeeMaster
                {
                    District_ID = Convert.ToInt32(dtLocName.Rows[i]["District_ID"]),
                    District_Name = dtLocName.Rows[i]["District_Name"].ToString()

                });
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectpinMaster = jsonSerialiser.Serialize(DistList);
            return Json(jsonProjectpinMaster);
        }

        [HttpPost]
        public JsonResult GetPinByOnselect(int str)
        {
            hInputPara = new Hashtable();
            List<ival_Locality> PinList = new List<ival_Locality>();
            // DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Loc_Id", str);

            DataTable dtLocPin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPinByLocName", hInputPara);
            for (int i = 0; i < dtLocPin.Rows.Count; i++)
            {
                PinList.Add(new ival_Locality
                {
                    Pin_Code = Convert.ToInt32(dtLocPin.Rows[i]["Pin_Code"])

                });
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectpinMaster = jsonSerialiser.Serialize(PinList);
            return Json(jsonProjectpinMaster);
        }

        [HttpPost]
        public JsonResult GetLocalityByOnselect(int ID)
        {
            hInputPara = new Hashtable();
            List<ival_SubLocalityMaster> LocList = new List<ival_SubLocalityMaster>();
            // DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Dist_Id", ID);

            DataTable dtLocName = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalityByDistId", hInputPara);
            for (int i = 0; i < dtLocName.Rows.Count; i++)
            {
                LocList.Add(new ival_SubLocalityMaster
                {
                    Loc_Id = Convert.ToInt32(dtLocName.Rows[i]["Loc_Id"]),
                    Loc_Name = dtLocName.Rows[i]["Loc_Name"].ToString()

                });
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectpinMaster = jsonSerialiser.Serialize(LocList);
            return Json(jsonProjectpinMaster);
        }

        /*
                [HttpPost]
                public IEnumerable<ival_LookupCategoryValues> GetPincode()
                {
                    List<ival_LookupCategoryValues> listPincode = new List<ival_LookupCategoryValues>();
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    DataTable dtPincode = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPincode");
                    for (int i = 0; i < dtPincode.Rows.Count; i++)
                    {

                        listPincode.Add(new ival_LookupCategoryValues
                        {
                            Lookup_Value = Convert.ToString(dtPincode.Rows[i]["Lookup_Value"]),
                            Lookup_ID = Convert.ToInt32(dtPincode.Rows[i]["Lookup_ID"])

                        });
                    }
                    return listPincode.AsEnumerable();
                }
                [HttpPost]
        */
        public IEnumerable<SelectListItem> GetBloodGrp()
        {
            var _bloodGrp = new List<SelectListItem>();
            string[] bldGrpArray = { "O+", "O-", "A+", "A-", "B+", "B-", "AB+", " AB-" };
            for (int i = 0; i < bldGrpArray.Length; i++)
            {

                _bloodGrp.Add(new SelectListItem()
                {
                    Text = Convert.ToString(bldGrpArray[i]),
                    Value = Convert.ToString(bldGrpArray[i])

                });
            }
            return _bloodGrp;
        }
        //[HttpPost]
        //public IEnumerable<SelectListItem> GetRelationsWithEmp()
        //{
        //    var _relationGrp = new List<SelectListItem>();
        //    string[] relatonArray = { "Spouse", "Friend", "Sibling", "Child", "Other" };
        //    for (int i = 0; i < relatonArray.Length; i++)
        //    {

        //        _relationGrp.Add(new SelectListItem()
        //        {
        //            Text = Convert.ToString(relatonArray[i]),
        //            Value = Convert.ToString(relatonArray[i])

        //        });
        //    }
        //    return _relationGrp;
        //}

        [HttpPost]
        public List<ival_LookupCategoryValues> GetRelationsWithEmp()
        {
            List<ival_LookupCategoryValues> ListFloorType1 = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtFloorType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Relation");
            for (int i = 0; i < dtFloorType.Rows.Count; i++)
            {

                ListFloorType1.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtFloorType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtFloorType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtFloorType.Rows[i]["Lookup_Value"])

                });

            }
            return ListFloorType1;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> Getbank()
        {
            List<ival_LookupCategoryValues> ListFloorType1 = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtFloorType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_bank");
            for (int i = 0; i < dtFloorType.Rows.Count; i++)
            {

                ListFloorType1.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtFloorType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtFloorType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtFloorType.Rows[i]["Lookup_Value"])

                });

            }
            return ListFloorType1;
        }


        [HttpPost]
        public List<ival_LookupCategoryValues> GetCurrent()
        {
            List<ival_LookupCategoryValues> ListFloorType1 = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtFloorType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCurrent");
            for (int i = 0; i < dtFloorType.Rows.Count; i++)
            {

                ListFloorType1.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtFloorType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtFloorType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtFloorType.Rows[i]["Lookup_Value"])

                });

            }
            return ListFloorType1;
        }

        [NonAction]
        public SelectList ToSelectList(DataTable table, string valueField, string textField)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            foreach (DataRow row in table.Rows)
            {
                list.Add(new SelectListItem()
                {
                    Text = row[textField].ToString(),
                    Value = row[valueField].ToString()
                });
            }

            return new SelectList(list, "Value", "Text");
        }
    }
}