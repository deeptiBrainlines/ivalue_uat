﻿using I_Value.WebApp.Models;
using IvalueDataAccess;
using IValuePublishProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace IValuePublishProject.Controllers
{
    public class ClientMasterNewController : Controller
    {
        // GET: ClientMasterNew
        SQLDataAccess sqlDataAccess;
        Hashtable hInputPara;
        VMClientMaster vMClientMaster;
        ival_ClientMaster objClient;

        public ActionResult Index()
        {
            try
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                DataTable dtClient = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientNew");

                List<ival_ClientMaster> listClient = new List<ival_ClientMaster>();

                if (dtClient.Rows.Count > 0)

                {
                    for (int i = 0; i < dtClient.Rows.Count; i++)
                    {

                        listClient.Add(new ival_ClientMaster
                        {
                            Client_ID = Convert.ToInt32(dtClient.Rows[i]["client_id"]),
                            Client_Name = Convert.ToString(dtClient.Rows[i]["Client_Name"]),
                            Client_State = Convert.ToString(dtClient.Rows[i]["State"]),

                        });
                    }
                }
                return View(listClient);

            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
           // return View();
        

        public ActionResult AddClientNew()
        {
            vMClientMaster = new VMClientMaster();
            vMClientMaster.ival_StateList = GetState();
            
            return View(vMClientMaster);
        }

        [HttpPost]
        public ActionResult AddClientNew(String str)
        {
            string resultStr = string.Empty;
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

            try
            {
                for (int i = 0; i < list1.Rows.Count; i++)
                {

                    IEnumerable<ival_States> ival_StatesList = GetState();
                    var state = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMClientMaster.SelectedStateID).Select(row => row.State_Name.ToString());

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    //              
                    hInputPara = new Hashtable();

                    hInputPara.Add("@Client_Name", Convert.ToString(list1.Rows[i]["Client_Name"]));
                    hInputPara.Add("@State_Name", Convert.ToString(list1.Rows[i]["State_Name"]));
                    hInputPara.Add("@Client_PAN", Convert.ToString(list1.Rows[i]["Client_PAN"]));
                    hInputPara.Add("@Client_GST", Convert.ToString(list1.Rows[i]["Client_GST"]));

                    hInputPara.Add("@Created_By", string.Empty);

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertClientMasterNew", hInputPara);
                    if (result == "Client Already Exists")
                    {
                        resultStr = result;
                    }
                    else if (result == "1")
                    {
                        resultStr = "Client Details Added Successfully";
                    }
                    else
                    {
                        resultStr = "Error while inserting";
                    }
                    hInputPara.Clear();

                }
                return Json(new { success = true, Message = resultStr }, JsonRequestBehavior.AllowGet);
           
                    /*if (Convert.ToInt32(result) == 1)
                    {
                        return RedirectToAction("Index", "ClientMasterNew");
                    }
                    else
                    {
                        return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                    }*/
              
            }
            catch (Exception e)
            {
                //e.InnerException.ToString();
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
            //return Json(new { success = true, Message = "SuccessFull" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditClientNew(int? ID)
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //return View();
            }
            vMClientMaster = new VMClientMaster();
            objClient = new ival_ClientMaster();
            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Client_ID", ID);
                DataTable dtClient = sqlDataAccess.GetDatatableExecuteStoreProcedure("[usp_GetClientMasterByIDNew]", hInputPara);

                if (dtClient.Rows.Count > 0 && dtClient != null)
                {

                    object valuedtClient = Convert.ToInt32(dtClient.Rows[0]["client_id"]);
                    if (dtClient.Columns.Contains("client_id") && valuedtClient != null && valuedtClient.ToString() != "")
                        objClient.Client_ID = Convert.ToInt32(valuedtClient);
                    else
                        objClient.Client_ID = 0;

                    object valueClient_Name = dtClient.Rows[0]["client_name"];
                    if (dtClient.Columns.Contains("client_name") && valueClient_Name != DBNull.Value && valueClient_Name.ToString() != "")
                        objClient.Client_Name = dtClient.Rows[0]["client_name"].ToString();
                    else
                        objClient.Client_Name = string.Empty;

                    object valueState_Name = dtClient.Rows[0]["state"];
                    if (dtClient.Columns.Contains("state") && valueState_Name != DBNull.Value && valueState_Name.ToString() != "")
                        objClient.Client_State = dtClient.Rows[0]["state"].ToString();
                    else
                        objClient.Client_State = string.Empty;

                    
                    object valuePAN_Num = dtClient.Rows[0]["pan_no"];
                    if (dtClient.Columns.Contains("pan_no") && valuePAN_Num != DBNull.Value && valuePAN_Num.ToString() != "")
                        objClient.PAN_Num = Convert.ToString(dtClient.Rows[0]["pan_no"].ToString());
                    else
                        objClient.PAN_Num = string.Empty;

                    object valueGSTIN_Num = dtClient.Rows[0]["gstin_no"];
                    if (dtClient.Columns.Contains("gstin_no") && valueGSTIN_Num != DBNull.Value && valueGSTIN_Num.ToString() != "")
                        objClient.GSTIN_Num = Convert.ToString(dtClient.Rows[0]["gstin_no"].ToString());
                    else
                        objClient.GSTIN_Num = string.Empty;

                   
                    vMClientMaster.ival_StateList = GetState();
                    
                    IEnumerable<ival_States> ival_StatesList = GetState();
                    if (ival_StatesList.ToList().Count > 0 && ival_StatesList != null)
                    {
                        var state = ival_StatesList.Where(s => s.State_Name == dtClient.Rows[0]["state"].ToString()).Select(s => s.State_ID);
                        vMClientMaster.SelectedStateID = state.FirstOrDefault();
                    }

                    //objClient.Client_ID = Convert.ToInt32(valuedtClient);

                    vMClientMaster.Client_ID = objClient.Client_ID;
                    vMClientMaster.ival_ClientMaster = objClient;

                }
            }
            catch (Exception ex)
            {
                //ex.InnerException.ToString();
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

            return View(vMClientMaster);
        }

        [HttpPost]
        public ActionResult UpdateClientNew(String str)
        {
            string resultStr = string.Empty;
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

            try
            {
                for (int i = 0; i < list1.Rows.Count; i++)
                {

                    IEnumerable<ival_States> ival_StatesList = GetState();
                    var state = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMClientMaster.SelectedStateID).Select(row => row.State_Name.ToString());

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    //              
                    hInputPara = new Hashtable();

                    hInputPara.Add("@Client_ID", Convert.ToInt32(list1.Rows[i]["Client_ID"]));
                    hInputPara.Add("@Client_Name", Convert.ToString(list1.Rows[i]["Client_Name"]));
                    hInputPara.Add("@State_Name", Convert.ToString(list1.Rows[i]["State_Name"]));
                    hInputPara.Add("@Client_PAN", Convert.ToString(list1.Rows[i]["Client_PAN"]));
                    hInputPara.Add("@Client_GST", Convert.ToString(list1.Rows[i]["Client_GST"]));
                   // hInputPara.Add("@Updated_By", string.Empty);

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateClientMasterByClientIDNew", hInputPara);

                    if (Convert.ToInt32(result) == 1)
                    {
                        return RedirectToAction("Index", "ClientMasterNew");
                    }
                    else
                    {
                        return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                //e.InnerException.ToString();
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, Message = "SuccessFull" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewClientNew(int? ID)
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //return View();
            }
            vMClientMaster = new VMClientMaster();
            objClient = new ival_ClientMaster();
            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Client_ID", ID);
                DataTable dtClient = sqlDataAccess.GetDatatableExecuteStoreProcedure("[usp_GetClientMasterByIDNew]", hInputPara);

                if (dtClient.Rows.Count > 0 && dtClient != null)
                {

                    object valuedtClient = Convert.ToInt32(dtClient.Rows[0]["client_id"]);
                    if (dtClient.Columns.Contains("client_id") && valuedtClient != null && valuedtClient.ToString() != "")
                        objClient.Client_ID = Convert.ToInt32(valuedtClient);
                    else
                        objClient.Client_ID = 0;

                    object valueClient_Name = dtClient.Rows[0]["client_name"];
                    if (dtClient.Columns.Contains("client_name") && valueClient_Name != DBNull.Value && valueClient_Name.ToString() != "")
                        objClient.Client_Name = dtClient.Rows[0]["client_name"].ToString();
                    else
                        objClient.Client_Name = string.Empty;

                    object valueState_Name = dtClient.Rows[0]["state"];
                    if (dtClient.Columns.Contains("state") && valueState_Name != DBNull.Value && valueState_Name.ToString() != "")
                        objClient.Client_State = dtClient.Rows[0]["state"].ToString();
                    else
                        objClient.Client_State = string.Empty;


                    object valuePAN_Num = dtClient.Rows[0]["pan_no"];
                    if (dtClient.Columns.Contains("pan_no") && valuePAN_Num != DBNull.Value && valuePAN_Num.ToString() != "")
                        objClient.PAN_Num = Convert.ToString(dtClient.Rows[0]["pan_no"].ToString());
                    else
                        objClient.PAN_Num = string.Empty;

                    object valueGSTIN_Num = dtClient.Rows[0]["gstin_no"];
                    if (dtClient.Columns.Contains("gstin_no") && valueGSTIN_Num != DBNull.Value && valueGSTIN_Num.ToString() != "")
                        objClient.GSTIN_Num = Convert.ToString(dtClient.Rows[0]["gstin_no"].ToString());
                    else
                        objClient.GSTIN_Num = string.Empty;


                    vMClientMaster.ival_StateList = GetState();

                    IEnumerable<ival_States> ival_StatesList = GetState();
                    if (ival_StatesList.ToList().Count > 0 && ival_StatesList != null)
                    {
                        var state = ival_StatesList.Where(s => s.State_Name == dtClient.Rows[0]["state"].ToString()).Select(s => s.State_ID);
                        vMClientMaster.SelectedStateID = state.FirstOrDefault();
                    }

                    //objClient.Client_ID = Convert.ToInt32(valuedtClient);

                    vMClientMaster.Client_ID = objClient.Client_ID;
                    vMClientMaster.ival_ClientMaster = objClient;

                }
            }
            catch (Exception ex)
            {
                //ex.InnerException.ToString();
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

            return View(vMClientMaster);
        }

        [HttpPost]
        public IEnumerable<ival_States> GetState()
        {
            List<ival_States> listState = new List<ival_States>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetStateByID");
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                listState.Add(new ival_States
                {
                    State_ID = Convert.ToInt32(dtState.Rows[i]["State_ID"]),
                    State_Name = Convert.ToString(dtState.Rows[i]["State_Name"])

                });


            }
            return listState.AsEnumerable();
        }

        public ActionResult Delete(int? ID)
        {
            try
            {
                sqlDataAccess = new SQLDataAccess();
                hInputPara = new Hashtable();
                hInputPara.Add("@Client_ID", ID);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteClientMasterIDNew", hInputPara);
                result.ToString();

                return RedirectToAction("Index", "ClientMasterNew");
            }
            catch (Exception ex)
            {

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

    }
}