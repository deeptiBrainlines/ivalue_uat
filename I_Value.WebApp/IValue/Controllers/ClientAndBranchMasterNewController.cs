﻿using I_Value.WebApp.Models;
using IvalueDataAccess;
using IValuePublishProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace IValuePublishProject.Controllers
{
    public class ClientAndBranchMasterNewController : Controller
    {
        // GET: ClientAndBranchMasterNew
        SQLDataAccess sqlDataAccess;
        Hashtable hInputPara;
        VMClientAndBranchMasterNew vMClientAndBranchMasterNew;
        ival_ClientAndBranchMasterNew objBranch = new ival_ClientAndBranchMasterNew();
        ival_DepartmentList ival_DepartmentList1 = new ival_DepartmentList();
      
        public ActionResult Index()
        {
            try
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                DataTable dtClient = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientAndBranch");

                List<ival_ClientAndBranchMasterNew> listClient = new List<ival_ClientAndBranchMasterNew>();

                if (dtClient.Rows.Count > 0)

                {
                    for (int i = 0; i < dtClient.Rows.Count; i++)
                    {

                        listClient.Add(new ival_ClientAndBranchMasterNew
                        {
                            Branch_ID = Convert.ToInt32(dtClient.Rows[i]["branch_id"]),
                            Client_ID = Convert.ToInt32(dtClient.Rows[i]["client_id"]),
                            Client_Name = Convert.ToString(dtClient.Rows[i]["client_name"]),
                            Branch_Name = Convert.ToString(dtClient.Rows[i]["branch_name"]),
                            Branch_State = Convert.ToString(dtClient.Rows[i]["state"]),
                            
                        });
                    }
                }
                return View(listClient);

            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
            //return View();
        }

        public ActionResult AddBranchNew()
        {
            vMClientAndBranchMasterNew = new VMClientAndBranchMasterNew();
            vMClientAndBranchMasterNew.ival_StatesList = GetState();
            vMClientAndBranchMasterNew.ival_DistrictsList = GetDistricts();
            vMClientAndBranchMasterNew.ival_CitiesList = GetCity();
            vMClientAndBranchMasterNew.ival_LocalityList = GetLocality();
            vMClientAndBranchMasterNew.ival_ClientList = GetClient();
            vMClientAndBranchMasterNew.ival_DepartmentList = GetDepartment();
            vMClientAndBranchMasterNew.ival_Des =GetDes();
            return View(vMClientAndBranchMasterNew);
        }

        [HttpPost]
        public ActionResult AddBranchNew(String str, String DeptList)
        {
            string resultStr = string.Empty;
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            DataTable DeptList1 = (DataTable)JsonConvert.DeserializeObject(DeptList, (typeof(DataTable)));
            try
            {
                for (int i = 0; i < list1.Rows.Count; i++)
                {

                    
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                   

                    IEnumerable<ival_States> ival_StatesList = GetState();
                    var state = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMClientAndBranchMasterNew.SelectedStateID).Select(row => row.State_Name.ToString());

                    IEnumerable<ival_Districts> ival_DistrictsList = GetDistricts();
                    var Dist = ival_DistrictsList.AsEnumerable().Where(row => row.District_ID == vMClientAndBranchMasterNew.SelectedDistID).Select(row => row.District_Name.ToString());

                    IEnumerable<ival_Cities> ival_CitiesList = GetCity();
                    var City = ival_CitiesList.AsEnumerable().Where(row => row.Loc_Id == vMClientAndBranchMasterNew.SelectedCityID).Select(row => row.City_Name.ToString());

                    IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                    var Loc = ival_LocalityList.AsEnumerable().Where(row => row.Loc_Id == vMClientAndBranchMasterNew.SelectedLocID).Select(row => row.Loc_Name.ToString());

                    var pincode = ival_LocalityList.AsEnumerable().Where(row => row.Loc_Id == vMClientAndBranchMasterNew.SelectedBranchPincode).Select(row => row.Pin_Code.ToString());

                    IEnumerable<ival_ClientMaster> ival_ClientList = GetClient();
                    var Client = ival_ClientList.AsEnumerable().Where(row => row.Client_ID == vMClientAndBranchMasterNew.SelectedClientID).Select(row => row.Client_Name.ToString());

                    /*IEnumerable<ival_LookupCategoryValues> ival_DepartmentList = GetDepartment();
                    var Dept = ival_DepartmentList.AsEnumerable().Where(row => row.Lookup_ID == vMClientAndBranchMasterNew.SelectedDeptID).Select(row => row.Lookup_Value.ToString());*/

                    hInputPara.Add("@Client_Id", Convert.ToInt32(list1.Rows[i]["Client_ID"]));
                    hInputPara.Add("@Branch_Name", Convert.ToString(list1.Rows[i]["Branch_Name"]));
                    hInputPara.Add("@Br_Address1", Convert.ToString(list1.Rows[i]["Br_Addr1"]));
                    hInputPara.Add("@Br_Address2", Convert.ToString(list1.Rows[i]["Br_Addr2"]));
                    hInputPara.Add("@Br_State", Convert.ToString(list1.Rows[i]["Br_State"]));
                    hInputPara.Add("@Br_District", Convert.ToString(list1.Rows[i]["Br_Dist"]));
                    hInputPara.Add("@Br_City", Convert.ToString(list1.Rows[i]["Br_City"]));
                    hInputPara.Add("@Br_Locality", Convert.ToString(list1.Rows[i]["Br_Loc"]));
                    hInputPara.Add("@Br_Pincode", Convert.ToString(list1.Rows[i]["Br_Pin"]));
                    hInputPara.Add("@Basic_Fees", Convert.ToString(list1.Rows[i]["Basic_Fees"]));
                    hInputPara.Add("@Additional_Fees", Convert.ToString(list1.Rows[i]["Add_Fees"]));
                    hInputPara.Add("@OutofGeo_Fees", Convert.ToString(list1.Rows[i]["Outof_Fees"]));
                    hInputPara.Add("@Total_Fees", Convert.ToString(list1.Rows[i]["Total_Fees"]));
                    hInputPara.Add("@Created_By", string.Empty);

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertClientAndBranch", hInputPara);

                  
                    Hashtable hInputPara1 = new Hashtable();
                    for (int j = 0; j < DeptList1.Rows.Count; j++)
                        {

                        hInputPara1.Add("@Client_Id", result);
                       // hInputPara1.Add("@Client_Id", Convert.ToInt32(DeptList1.Rows[j]["Client_ID"]));
                        hInputPara1.Add("@Client_Dept", Convert.ToString(DeptList1.Rows[j]["Client_Dept"]));
                        hInputPara1.Add("@Client_Name", Convert.ToString(DeptList1.Rows[j]["Client_Name"]));
                        hInputPara1.Add("@Client_No", Convert.ToString(DeptList1.Rows[j]["Client_No"]));
                        hInputPara1.Add("@Client_Desgn", Convert.ToString(DeptList1.Rows[j]["Client_Desgn"]));
                        hInputPara1.Add("@Client_Email", Convert.ToString(DeptList1.Rows[j]["Client_Email"]));
                        hInputPara1.Add("@Dept_Id", Convert.ToString(DeptList1.Rows[j]["Dept_Id"]));
                        hInputPara1.Add("@Created_By", string.Empty);
                        //insert clientDept parameter
                        var result1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertDepartmentNew", hInputPara1);
                        if(Convert.ToInt32(result1) == 1)
                        {
                            hInputPara1.Clear();
                        }
                    }
                       
                }
            }
            catch (Exception e)
            {
                //e.InnerException.ToString();
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, Message = "SuccessFull" }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult EditBranchNew(int? ID)
       
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //return View();
            }
            vMClientAndBranchMasterNew = new VMClientAndBranchMasterNew();
            objBranch = new ival_ClientAndBranchMasterNew();
            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Client_ID", ID);
                DataTable dtBranch = sqlDataAccess.GetDatatableExecuteStoreProcedure("[usp_GetClientAndBranchMasterByID]", hInputPara);

                if (dtBranch.Rows.Count > 0 && dtBranch != null)
                {
                    /*object valuedtbranchid = Convert.ToInt32(dtBranch.Rows[0]["branch_id"]);
                    if (dtBranch.Columns.Contains("branch_id") && valuedtbranchid != null && valuedtbranchid.ToString() != "")
                        objBranch.Branch_Id = Convert.ToInt32(valuedtbranchid);
                    else
                        objBranch.Branch_Id = 0;*/

                    object valuedtClient = Convert.ToInt32(dtBranch.Rows[0]["client_id"]);
                    if (dtBranch.Columns.Contains("client_id") && valuedtClient != null && valuedtClient.ToString() != "")
                        objBranch.Client_ID = Convert.ToInt32(valuedtClient);
                    else
                        objBranch.Client_ID = 0;


                    object valueBranch_Name = dtBranch.Rows[0]["branch_name"];
                    if (dtBranch.Columns.Contains("branch_name") && valueBranch_Name != DBNull.Value && valueBranch_Name.ToString() != "")
                        objBranch.Branch_Name = dtBranch.Rows[0]["branch_name"].ToString();
                    else
                        objBranch.Branch_Name = string.Empty;

                   
                    object valueClient_Address1 = dtBranch.Rows[0]["address1"];
                    if (dtBranch.Columns.Contains("address1") && valueClient_Address1 != DBNull.Value && valueClient_Address1.ToString() != "")
                        objBranch.Address1 = dtBranch.Rows[0]["address1"].ToString();
                    else
                        objBranch.Address1 = string.Empty;

                    object valueClient_Address2 = dtBranch.Rows[0]["address2"];
                    if (dtBranch.Columns.Contains("address2") && valueClient_Address2 != DBNull.Value && valueClient_Address2.ToString() != "")
                        objBranch.Address2 = dtBranch.Rows[0]["address2"].ToString();
                    else
                        objBranch.Address2 = string.Empty;

                    object valueClient_City = dtBranch.Rows[0]["city"];
                    if (dtBranch.Columns.Contains("city") && valueClient_City != DBNull.Value && valueClient_City.ToString() != "")
                        objBranch.Branch_City = dtBranch.Rows[0]["city"].ToString();
                    else
                        objBranch.Branch_City = string.Empty;

                    object valueClient_Locality = dtBranch.Rows[0]["locality"];
                    if (dtBranch.Columns.Contains("locality") && valueClient_Locality != DBNull.Value && valueClient_Locality.ToString() != "")
                        objBranch.Branch_Locality = dtBranch.Rows[0]["locality"].ToString();
                    else
                        objBranch.Branch_Locality = string.Empty;

                    object valueClient_District = dtBranch.Rows[0]["district"];
                    if (dtBranch.Columns.Contains("district") && valueClient_District != DBNull.Value && valueClient_District.ToString() != "")
                        objBranch.Branch_District = dtBranch.Rows[0]["district"].ToString();
                    else
                        objBranch.Branch_District = string.Empty;

                    object valueClient_State = dtBranch.Rows[0]["state"];
                    if (dtBranch.Columns.Contains("state") && valueClient_State != DBNull.Value && valueClient_State.ToString() != "")
                        objBranch.Branch_State = dtBranch.Rows[0]["state"].ToString();
                    else
                        objBranch.Branch_State = string.Empty;

                    object valueClient_Pincode = dtBranch.Rows[0]["pin_code"];
                    if (dtBranch.Columns.Contains("pin_code") && valueClient_Pincode != DBNull.Value && valueClient_Pincode.ToString() != "")
                        objBranch.Branch_Pincode = Convert.ToInt32(dtBranch.Rows[0]["pin_code"].ToString());
                    else
                        objBranch.Branch_Pincode = 0;

                    object valueBasic_Fees = dtBranch.Rows[0]["basic_fees"];
                    if (dtBranch.Columns.Contains("basic_fees") && valueBasic_Fees != DBNull.Value && valueBasic_Fees.ToString() != "")
                        objBranch.Basic_Fees = dtBranch.Rows[0]["basic_fees"].ToString();
                    else
                        objBranch.Basic_Fees = string.Empty;

                    object valueAdditional_Fees = dtBranch.Rows[0]["additional_fees"];
                    if (dtBranch.Columns.Contains("additional_fees") && valueAdditional_Fees != DBNull.Value && valueAdditional_Fees.ToString() != "")
                        objBranch.Additional_Fees = dtBranch.Rows[0]["additional_fees"].ToString();
                    else
                        objBranch.Additional_Fees = string.Empty;

                    object valueOutofGeo_Fees = dtBranch.Rows[0]["out_of_Geo_fees"];
                    if (dtBranch.Columns.Contains("out_of_Geo_fees") && valueOutofGeo_Fees != DBNull.Value && valueOutofGeo_Fees.ToString() != "")
                        objBranch.OutofGeo_Fees = dtBranch.Rows[0]["out_of_Geo_fees"].ToString();
                    else
                        objBranch.OutofGeo_Fees = string.Empty;

                    object valueTotal_Fees = dtBranch.Rows[0]["total_fees"];
                    if (dtBranch.Columns.Contains("total_fees") && valueTotal_Fees != DBNull.Value && valueTotal_Fees.ToString() != "")
                        objBranch.Total_Fees = dtBranch.Rows[0]["total_fees"].ToString();
                    else
                        objBranch.Total_Fees = string.Empty;

                    vMClientAndBranchMasterNew.ival_StatesList = GetState();
                    vMClientAndBranchMasterNew.ival_DistrictsList = GetDistricts();
                    vMClientAndBranchMasterNew.ival_LocalityList = GetLocality();
                    vMClientAndBranchMasterNew.ival_ClientList = GetClient();
                    vMClientAndBranchMasterNew.ival_DepartmentList = GetDepartment();
                    vMClientAndBranchMasterNew.ival_CitiesList = GetCity();


                    IEnumerable<ival_States> ival_StatesList = GetState();
                    if (ival_StatesList.ToList().Count > 0 && ival_StatesList != null)
                    {
                        var state = ival_StatesList.Where(s => s.State_Name == dtBranch.Rows[0]["state"].ToString()).Select(s => s.State_ID);
                        vMClientAndBranchMasterNew.SelectedStateID = state.FirstOrDefault();
                    }
                    IEnumerable<ival_Districts> ival_DistrictsList = GetDistricts();
                    if (ival_DistrictsList.ToList().Count > 0 && ival_DistrictsList != null)
                    {
                        var Dist = ival_DistrictsList.Where(s => s.District_Name == dtBranch.Rows[0]["district"].ToString()).Select(s => s.District_ID);
                        vMClientAndBranchMasterNew.SelectedDistID = Dist.FirstOrDefault();
                    }
                    IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                    if (ival_LocalityList.ToList().Count > 0 && ival_LocalityList != null)
                    {
                        var City = ival_LocalityList.Where(s => s.Loc_Name == dtBranch.Rows[0]["locality"].ToString()).Select(s => s.Loc_Id);
                        vMClientAndBranchMasterNew.SelectedLocID = City.FirstOrDefault();

                    }
                    IEnumerable<ival_Cities> ival_CitiesList = GetCity();
                    if (ival_CitiesList.ToList().Count > 0 && ival_CitiesList != null)
                    {
                        var Locality = ival_CitiesList.Where(s => s.City_Name == dtBranch.Rows[0]["city"].ToString()).Select(s => s.Loc_Id);
                        vMClientAndBranchMasterNew.SelectedCityID = Locality.FirstOrDefault();
                    }
                    IEnumerable<ival_ClientMaster> ival_ClientList = GetClient();
                    if (ival_ClientList.ToList().Count > 0 && ival_ClientList != null)
                    {
                        var City = ival_ClientList.Where(s => s.Client_Name == dtBranch.Rows[0]["client_id"].ToString()).Select(s => s.Client_ID);
                        vMClientAndBranchMasterNew.SelectedClientID = City.FirstOrDefault();
                    }
                    /*IEnumerable<ival_LookupCategoryValues> ival_DepartmentList = GetDepartment();
                    if (ival_DepartmentList.ToList().Count > 0 && ival_DepartmentList != null)
                    {
                        var Locality = ival_DepartmentList.Where(s => s.Lookup_Value == dtBranch.Rows[0]["Client_Locality"].ToString()).Select(s => s.Lookup_ID);
                        vMClientAndBranchMasterNew.SelectedCityID = Locality.FirstOrDefault();
                    }*/
                    
                    vMClientAndBranchMasterNew.SelectedClientID = Convert.ToInt32(dtBranch.Rows[0]["client_id"]);
                    vMClientAndBranchMasterNew.SelectedLocID = Convert.ToInt32(dtBranch.Rows[0]["locality"]);
                    vMClientAndBranchMasterNew.SelectedBranchPincode = Convert.ToInt32(dtBranch.Rows[0]["locality"]);
                    
                    vMClientAndBranchMasterNew.Client_ID = objBranch.Client_ID;
                    vMClientAndBranchMasterNew.Branch_Name= objBranch.Branch_Name;
                    vMClientAndBranchMasterNew.Address1 = objBranch.Address1;
                    vMClientAndBranchMasterNew.Address2 = objBranch.Address2;
                    vMClientAndBranchMasterNew.Basic_Fees = objBranch.Basic_Fees;
                    vMClientAndBranchMasterNew.Additional_Fees = objBranch.Additional_Fees;
                    vMClientAndBranchMasterNew.OutofGeo_Fees = objBranch.OutofGeo_Fees;
                    vMClientAndBranchMasterNew.Total_Fees = objBranch.Total_Fees;

                    vMClientAndBranchMasterNew.ival_DepartmentList1 = GetDept();
                    //vMClientAndBranchMasterNew.Dept_ID = vMClientAndBranchMasterNew.ival_DepartmentList1[0].Dept_ID;
                    vMClientAndBranchMasterNew.Dept_ID = ival_DepartmentList1.Dept_ID;
                }
            }
            catch (Exception ex)
            {
                //ex.InnerException.ToString();
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

            return View(vMClientAndBranchMasterNew);
        }


        [HttpPost]
        public List<ival_DepartmentList> GetDept()
        {
            List<ival_DepartmentList> ListDept = new List<ival_DepartmentList>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            //int RequestID = Convert.ToInt32(Session["client_id"]);
            hInputPara.Add("@Client_Id", vMClientAndBranchMasterNew.Client_ID);
            DataTable dtDept = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDepartmentNew", hInputPara);

            for (int i = 0; i < dtDept.Rows.Count; i++)
            {

                ListDept.Add(new ival_DepartmentList
                {
                    SrNo = i+1,
                    Dept_ID = Convert.ToInt32(dtDept.Rows[i]["department_id"]),
                    Dept_Id = Convert.ToInt32(dtDept.Rows[i]["Dept_Id"]),
                    Client_ID = Convert.ToInt32(dtDept.Rows[i]["client_id"]),
                    Dept_Name = Convert.ToString(dtDept.Rows[i]["Department_Name"]),
                    Client_Name = Convert.ToString(dtDept.Rows[i]["contact_name"]),
                    Contact_Num = Convert.ToString(dtDept.Rows[i]["contact_number"]),
                    Client_Desgn1 = Convert.ToString(dtDept.Rows[i]["Designation"]),
                    Client_Mail = Convert.ToString(dtDept.Rows[i]["email_id"]),

                });

            }
            return ListDept;
        }

        public ActionResult ViewBranchNew(int? ID)
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //return View();
            }
            vMClientAndBranchMasterNew = new VMClientAndBranchMasterNew();
            objBranch = new ival_ClientAndBranchMasterNew();
            var items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            try
            {
                hInputPara.Add("@Client_ID", ID);
                DataTable dtBranch = sqlDataAccess.GetDatatableExecuteStoreProcedure("[usp_GetClientAndBranchMasterByID]", hInputPara);

                if (dtBranch.Rows.Count > 0 && dtBranch != null)
                {
                    /*object valuedtbranchid = Convert.ToInt32(dtBranch.Rows[0]["branch_id"]);
                    if (dtBranch.Columns.Contains("branch_id") && valuedtbranchid != null && valuedtbranchid.ToString() != "")
                        objBranch.Branch_Id = Convert.ToInt32(valuedtbranchid);
                    else
                        objBranch.Branch_Id = 0;*/

                    object valuedtClient = Convert.ToInt32(dtBranch.Rows[0]["client_id"]);
                    if (dtBranch.Columns.Contains("client_id") && valuedtClient != null && valuedtClient.ToString() != "")
                        objBranch.Client_ID = Convert.ToInt32(valuedtClient);
                    else
                        objBranch.Client_ID = 0;


                    object valueBranch_Name = dtBranch.Rows[0]["branch_name"];
                    if (dtBranch.Columns.Contains("branch_name") && valueBranch_Name != DBNull.Value && valueBranch_Name.ToString() != "")
                        objBranch.Branch_Name = dtBranch.Rows[0]["branch_name"].ToString();
                    else
                        objBranch.Branch_Name = string.Empty;


                    object valueClient_Address1 = dtBranch.Rows[0]["address1"];
                    if (dtBranch.Columns.Contains("address1") && valueClient_Address1 != DBNull.Value && valueClient_Address1.ToString() != "")
                        objBranch.Address1 = dtBranch.Rows[0]["address1"].ToString();
                    else
                        objBranch.Address1 = string.Empty;

                    object valueClient_Address2 = dtBranch.Rows[0]["address2"];
                    if (dtBranch.Columns.Contains("address2") && valueClient_Address2 != DBNull.Value && valueClient_Address2.ToString() != "")
                        objBranch.Address2 = dtBranch.Rows[0]["address2"].ToString();
                    else
                        objBranch.Address2 = string.Empty;

                    object valueClient_City = dtBranch.Rows[0]["city"];
                    if (dtBranch.Columns.Contains("city") && valueClient_City != DBNull.Value && valueClient_City.ToString() != "")
                        objBranch.Branch_City = dtBranch.Rows[0]["city"].ToString();
                    else
                        objBranch.Branch_City = string.Empty;

                    object valueClient_Locality = dtBranch.Rows[0]["locality"];
                    if (dtBranch.Columns.Contains("locality") && valueClient_Locality != DBNull.Value && valueClient_Locality.ToString() != "")
                        objBranch.Branch_Locality = dtBranch.Rows[0]["locality"].ToString();
                    else
                        objBranch.Branch_Locality = string.Empty;

                    object valueClient_District = dtBranch.Rows[0]["district"];
                    if (dtBranch.Columns.Contains("district") && valueClient_District != DBNull.Value && valueClient_District.ToString() != "")
                        objBranch.Branch_District = dtBranch.Rows[0]["district"].ToString();
                    else
                        objBranch.Branch_District = string.Empty;

                    object valueClient_State = dtBranch.Rows[0]["state"];
                    if (dtBranch.Columns.Contains("state") && valueClient_State != DBNull.Value && valueClient_State.ToString() != "")
                        objBranch.Branch_State = dtBranch.Rows[0]["state"].ToString();
                    else
                        objBranch.Branch_State = string.Empty;

                    object valueClient_Pincode = dtBranch.Rows[0]["pin_code"];
                    if (dtBranch.Columns.Contains("pin_code") && valueClient_Pincode != DBNull.Value && valueClient_Pincode.ToString() != "")
                        objBranch.Branch_Pincode = Convert.ToInt32(dtBranch.Rows[0]["pin_code"].ToString());
                    else
                        objBranch.Branch_Pincode = 0;

                    object valueBasic_Fees = dtBranch.Rows[0]["basic_fees"];
                    if (dtBranch.Columns.Contains("basic_fees") && valueBasic_Fees != DBNull.Value && valueBasic_Fees.ToString() != "")
                        objBranch.Basic_Fees = dtBranch.Rows[0]["basic_fees"].ToString();
                    else
                        objBranch.Basic_Fees = string.Empty;

                    object valueAdditional_Fees = dtBranch.Rows[0]["additional_fees"];
                    if (dtBranch.Columns.Contains("additional_fees") && valueAdditional_Fees != DBNull.Value && valueAdditional_Fees.ToString() != "")
                        objBranch.Additional_Fees = dtBranch.Rows[0]["additional_fees"].ToString();
                    else
                        objBranch.Additional_Fees = string.Empty;

                    object valueOutofGeo_Fees = dtBranch.Rows[0]["out_of_Geo_fees"];
                    if (dtBranch.Columns.Contains("out_of_Geo_fees") && valueOutofGeo_Fees != DBNull.Value && valueOutofGeo_Fees.ToString() != "")
                        objBranch.OutofGeo_Fees = dtBranch.Rows[0]["out_of_Geo_fees"].ToString();
                    else
                        objBranch.OutofGeo_Fees = string.Empty;

                    object valueTotal_Fees = dtBranch.Rows[0]["total_fees"];
                    if (dtBranch.Columns.Contains("total_fees") && valueTotal_Fees != DBNull.Value && valueTotal_Fees.ToString() != "")
                        objBranch.Total_Fees = dtBranch.Rows[0]["total_fees"].ToString();
                    else
                        objBranch.Total_Fees = string.Empty;

                    vMClientAndBranchMasterNew.ival_StatesList = GetState();
                    vMClientAndBranchMasterNew.ival_DistrictsList = GetDistricts();
                    vMClientAndBranchMasterNew.ival_LocalityList = GetLocality();
                    vMClientAndBranchMasterNew.ival_ClientList = GetClient();
                    vMClientAndBranchMasterNew.ival_DepartmentList = GetDepartment();
                    vMClientAndBranchMasterNew.ival_CitiesList = GetCity();


                    IEnumerable<ival_States> ival_StatesList = GetState();
                    if (ival_StatesList.ToList().Count > 0 && ival_StatesList != null)
                    {
                        var state = ival_StatesList.Where(s => s.State_Name == dtBranch.Rows[0]["state"].ToString()).Select(s => s.State_ID);
                        vMClientAndBranchMasterNew.SelectedStateID = state.FirstOrDefault();
                    }
                    IEnumerable<ival_Districts> ival_DistrictsList = GetDistricts();
                    if (ival_DistrictsList.ToList().Count > 0 && ival_DistrictsList != null)
                    {
                        var Dist = ival_DistrictsList.Where(s => s.District_Name == dtBranch.Rows[0]["district"].ToString()).Select(s => s.District_ID);
                        vMClientAndBranchMasterNew.SelectedDistID = Dist.FirstOrDefault();
                    }
                    IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                    if (ival_LocalityList.ToList().Count > 0 && ival_LocalityList != null)
                    {
                        var City = ival_LocalityList.Where(s => s.Loc_Name == dtBranch.Rows[0]["locality"].ToString()).Select(s => s.Loc_Id);
                        vMClientAndBranchMasterNew.SelectedLocID = City.FirstOrDefault();

                    }
                    IEnumerable<ival_Cities> ival_CitiesList = GetCity();
                    if (ival_CitiesList.ToList().Count > 0 && ival_CitiesList != null)
                    {
                        var Locality = ival_CitiesList.Where(s => s.City_Name == dtBranch.Rows[0]["city"].ToString()).Select(s => s.Loc_Id);
                        vMClientAndBranchMasterNew.SelectedCityID = Locality.FirstOrDefault();
                    }
                    IEnumerable<ival_ClientMaster> ival_ClientList = GetClient();
                    if (ival_ClientList.ToList().Count > 0 && ival_ClientList != null)
                    {
                        var City = ival_ClientList.Where(s => s.Client_Name == dtBranch.Rows[0]["client_id"].ToString()).Select(s => s.Client_ID);
                        vMClientAndBranchMasterNew.SelectedClientID = City.FirstOrDefault();
                    }
                    /*IEnumerable<ival_LookupCategoryValues> ival_DepartmentList = GetDepartment();
                    if (ival_DepartmentList.ToList().Count > 0 && ival_DepartmentList != null)
                    {
                        var Locality = ival_DepartmentList.Where(s => s.Lookup_Value == dtBranch.Rows[0]["Client_Locality"].ToString()).Select(s => s.Lookup_ID);
                        vMClientAndBranchMasterNew.SelectedCityID = Locality.FirstOrDefault();
                    }*/

                    vMClientAndBranchMasterNew.SelectedClientID = Convert.ToInt32(dtBranch.Rows[0]["client_id"]);
                    vMClientAndBranchMasterNew.SelectedLocID = Convert.ToInt32(dtBranch.Rows[0]["locality"]);
                    vMClientAndBranchMasterNew.SelectedBranchPincode = Convert.ToInt32(dtBranch.Rows[0]["locality"]);

                    vMClientAndBranchMasterNew.Client_ID = objBranch.Client_ID;
                    vMClientAndBranchMasterNew.Branch_Name = objBranch.Branch_Name;
                    vMClientAndBranchMasterNew.Address1 = objBranch.Address1;
                    vMClientAndBranchMasterNew.Address2 = objBranch.Address2;
                    vMClientAndBranchMasterNew.Basic_Fees = objBranch.Basic_Fees;
                    vMClientAndBranchMasterNew.Additional_Fees = objBranch.Additional_Fees;
                    vMClientAndBranchMasterNew.OutofGeo_Fees = objBranch.OutofGeo_Fees;
                    vMClientAndBranchMasterNew.Total_Fees = objBranch.Total_Fees;

                    vMClientAndBranchMasterNew.ival_DepartmentList1 = GetDept();
                }
            }
            catch (Exception ex)
            {
                //ex.InnerException.ToString();
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

            return View(vMClientAndBranchMasterNew);
        }

        [HttpPost]
        public ActionResult UpdateBranchNew(String str, String DeptList)
        {
            string resultStr = string.Empty;
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            DataTable DeptList1 = (DataTable)JsonConvert.DeserializeObject(DeptList, (typeof(DataTable)));
            try
            {
                for (int i = 0; i < list1.Rows.Count; i++)
                {


                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();


                    IEnumerable<ival_States> ival_StatesList = GetState();
                    var state = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMClientAndBranchMasterNew.SelectedStateID).Select(row => row.State_Name.ToString());

                    IEnumerable<ival_Districts> ival_DistrictsList = GetDistricts();
                    var Dist = ival_DistrictsList.AsEnumerable().Where(row => row.District_ID == vMClientAndBranchMasterNew.SelectedDistID).Select(row => row.District_Name.ToString());

                    IEnumerable<ival_Cities> ival_CitiesList = GetCity();
                    var City = ival_CitiesList.AsEnumerable().Where(row => row.Loc_Id == vMClientAndBranchMasterNew.SelectedCityID).Select(row => row.City_Name.ToString());

                    IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                    var Loc = ival_LocalityList.AsEnumerable().Where(row => row.Loc_Id == vMClientAndBranchMasterNew.SelectedLocID).Select(row => row.Loc_Name.ToString());

                    var pincode = ival_LocalityList.AsEnumerable().Where(row => row.Loc_Id == vMClientAndBranchMasterNew.SelectedBranchPincode).Select(row => row.Pin_Code.ToString());

                    IEnumerable<ival_ClientMaster> ival_ClientList = GetClient();
                    var Client = ival_ClientList.AsEnumerable().Where(row => row.Client_ID == vMClientAndBranchMasterNew.SelectedClientID).Select(row => row.Client_Name.ToString());

                    /*IEnumerable<ival_LookupCategoryValues> ival_DepartmentList = GetDepartment();
                    var Dept = ival_DepartmentList.AsEnumerable().Where(row => row.Lookup_ID == vMClientAndBranchMasterNew.SelectedDeptID).Select(row => row.Lookup_Value.ToString());*/

                    hInputPara.Add("@Client_Id", Convert.ToInt32(list1.Rows[i]["Client_ID"]));
                    hInputPara.Add("@Branch_Name", Convert.ToString(list1.Rows[i]["Branch_Name"]));
                    hInputPara.Add("@Br_Address1", Convert.ToString(list1.Rows[i]["Br_Addr1"]));
                    hInputPara.Add("@Br_Address2", Convert.ToString(list1.Rows[i]["Br_Addr2"]));
                    hInputPara.Add("@Br_State", Convert.ToString(list1.Rows[i]["Br_State"]));
                    hInputPara.Add("@Br_District", Convert.ToString(list1.Rows[i]["Br_Dist"]));
                    hInputPara.Add("@Br_City", Convert.ToString(list1.Rows[i]["Br_City"]));
                    hInputPara.Add("@Br_Locality", Convert.ToString(list1.Rows[i]["Br_Loc"]));
                    hInputPara.Add("@Br_Pincode", Convert.ToString(list1.Rows[i]["Br_Pin"]));
                    hInputPara.Add("@Basic_Fees", Convert.ToString(list1.Rows[i]["Basic_Fees"]));
                    hInputPara.Add("@Additional_Fees", Convert.ToString(list1.Rows[i]["Add_Fees"]));
                    hInputPara.Add("@OutofGeo_Fees", Convert.ToString(list1.Rows[i]["Outof_Fees"]));
                    hInputPara.Add("@Total_Fees", Convert.ToString(list1.Rows[i]["Total_Fees"]));
                    hInputPara.Add("@Created_By", string.Empty);

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateClientAndBranch", hInputPara);


                    var result2 = DeleteDept(Convert.ToInt32(list1.Rows[i]["Client_ID"]));

                    // insert client branch master
                    //after successfull insert get client id 

                    // var Client_ID= Convert.ToInt32(result.ToString());
                    Hashtable hInputPara1 = new Hashtable();
                    for (int j = 0; j < DeptList1.Rows.Count; j++)
                    {

                        hInputPara1.Add("@Client_Id", result);
                        // hInputPara1.Add("@Client_Id", Convert.ToInt32(DeptList1.Rows[j]["Client_ID"]));
                        hInputPara1.Add("@Client_Dept", Convert.ToString(DeptList1.Rows[j]["Client_Dept"]));
                        hInputPara1.Add("@Client_Name", Convert.ToString(DeptList1.Rows[j]["Client_Name"]));
                        hInputPara1.Add("@Client_No", Convert.ToString(DeptList1.Rows[j]["Client_No"]));
                        hInputPara1.Add("@Client_Desgn", Convert.ToString(DeptList1.Rows[j]["Client_Desgn"]));
                        hInputPara1.Add("@Client_Email", Convert.ToString(DeptList1.Rows[j]["Client_Email"]));
                        hInputPara1.Add("@Dept_Id", Convert.ToString(DeptList1.Rows[j]["Dept_Id"]));
                        hInputPara1.Add("@Created_By", string.Empty);
                        //insert clientDept parameter
                        var result1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertDepartmentNew", hInputPara1);
                        if (Convert.ToInt32(result1) == 1)
                        {
                            hInputPara1.Clear();
                        }
                    }


                    if (Convert.ToInt32(result) == 1)
                    {
                        return RedirectToAction("Index", "ClientAndBranchMasterNew");
                    }
                    
                }
            }
            catch (Exception e)
            {
                //e.InnerException.ToString();
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, Message = "SuccessFull" }, JsonRequestBehavior.AllowGet);
        }


        /*[HttpPost]
        public ActionResult AddDepartment(String str)
        {
            string resultStr = string.Empty;
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

            try
            {
                for (int i = 0; i < list1.Rows.Count; i++)
                {

                    IEnumerable<ival_LookupCategoryValues> ival_DepartmentList = GetDepartment();
                    var Dept = ival_DepartmentList.AsEnumerable().Where(row => row.Lookup_ID == vMClientAndBranchMasterNew.SelectedDeptID).Select(row => row.Lookup_Value.ToString());

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    //              
                    hInputPara = new Hashtable();

                    hInputPara.Add("@Client_Id", Convert.ToInt32(list1.Rows[i]["Client_Id"]));
                    hInputPara.Add("@Client_Dept", Convert.ToString(list1.Rows[i]["Client_Dept"]));
                    hInputPara.Add("@Client_Name", Convert.ToString(list1.Rows[i]["Client_Name"]));
                    hInputPara.Add("@Client_No", Convert.ToString(list1.Rows[i]["Client_No"]));
                    hInputPara.Add("@Client_Desgn", Convert.ToString(list1.Rows[i]["Client_Desgn"]));
                    hInputPara.Add("@Client_Email", Convert.ToString(list1.Rows[i]["Client_Email"]));
                    hInputPara.Add("@Created_By", string.Empty);

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertDepartmentNew", hInputPara);

                    if (Convert.ToInt32(result) == 1)
                    {
                        return RedirectToAction("Index", "ClientAndBranchMasterNew");
                    }
                    else
                    {
                        return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                //e.InnerException.ToString();
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, Message = "SuccessFull" }, JsonRequestBehavior.AllowGet);
        }*/




        /*public ActionResult EditDept(int ID)
        {
            try
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@Client_Id", ID);
                DataTable dtDept = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDepartmentNew");

                List<ival_ClientAndBranchMasterNew> listDept = new List<ival_ClientAndBranchMasterNew>();

                if (dtDept.Rows.Count > 0)

                {
                    for (int i = 0; i < dtDept.Rows.Count; i++)
                    {

                        listDept.Add(new ival_ClientAndBranchMasterNew
                        {
                            Dept_ID = Convert.ToInt32(dtDept.Rows[i]["department_id"]),
                            Dept_Name = Convert.ToString(dtDept.Rows[i]["Department_Name"]),
                            Client_Name = Convert.ToString(dtDept.Rows[i]["contact_name"]),
                            Contact_Num = Convert.ToString(dtDept.Rows[i]["contact_number"]),
                            Client_Desgn1 = Convert.ToString(dtDept.Rows[i]["Designation"]),
                            Client_Mail = Convert.ToString(dtDept.Rows[i]["email_id"]),

                        });
                    }
                }
                return View(listDept);

            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }*/


        [HttpPost]
        public IEnumerable<ival_States> GetState()
        {
            List<ival_States> listState = new List<ival_States>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetStateByID");
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                listState.Add(new ival_States
                {
                    State_ID = Convert.ToInt32(dtState.Rows[i]["State_ID"]),
                    State_Name = Convert.ToString(dtState.Rows[i]["State_Name"])

                });

            }
            return listState.AsEnumerable();
        }

        [HttpPost]
        public IEnumerable<ival_Cities> GetCity()
        {
            List<ival_Cities> listState = new List<ival_Cities>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtCity = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCityByID");
            for (int i = 0; i < dtCity.Rows.Count; i++)
            {

                listState.Add(new ival_Cities
                {
                    Loc_Id = Convert.ToInt32(dtCity.Rows[i]["City_ID"]),
                    City_Name = Convert.ToString(dtCity.Rows[i]["City"])

                });


            }
            return listState.AsEnumerable();
        }


        [HttpPost]
        public IEnumerable<ival_Districts> GetDistricts()
        {
            List<ival_Districts> lists = new List<ival_Districts>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            try
            {
                DataTable dtDistict = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDistrictByID");
                for (int i = 0; i < dtDistict.Rows.Count; i++)
                {
                    lists.Add(new ival_Districts
                    {
                        District_ID = Convert.ToInt32(dtDistict.Rows[i]["District_ID"]),
                        District_Name = Convert.ToString(dtDistict.Rows[i]["District_Name"])
                    });
                }
            }
            catch (Exception)
            {

            }
            return lists.AsEnumerable();
        }

        public IEnumerable<ival_Locality> GetLocality()
        {
            List<ival_Locality> listLoc = new List<ival_Locality>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            DataTable dtLocality = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalityByID");
            for (int i = 0; i < dtLocality.Rows.Count; i++)
            {

                listLoc.Add(new ival_Locality
                {
                    Loc_Id = Convert.ToInt32(dtLocality.Rows[i]["Loc_Id"]),
                    Loc_Name = Convert.ToString(dtLocality.Rows[i]["Loc_Name"]),
                    Pin_Code = Convert.ToInt32(dtLocality.Rows[i]["Pin_Code"])

                });
            }
            return listLoc.AsEnumerable();
        }

        [HttpPost]
        public IEnumerable<ival_ClientMaster> GetClient()
        {
            List<ival_ClientMaster> listClient = new List<ival_ClientMaster>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtClient = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClientByID");
            for (int i = 0; i < dtClient.Rows.Count; i++)
            {

                listClient.Add(new ival_ClientMaster
                {
                    Client_ID = Convert.ToInt32(dtClient.Rows[i]["client_id"]),
                    Client_Name = Convert.ToString(dtClient.Rows[i]["client_name"])

                });


            }
            return listClient.AsEnumerable();
        }

        [HttpPost]
        public IEnumerable<ival_LookupCategoryValues> GetDepartment()
        {
            List<ival_LookupCategoryValues> listDept = new List<ival_LookupCategoryValues>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtDept = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDept");
            for (int i = 0; i < dtDept.Rows.Count; i++)
            {

                listDept.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtDept.Rows[i]["Lookup_ID"]),
                    Lookup_Value = Convert.ToString(dtDept.Rows[i]["Lookup_Value"])

                });


            }
            return listDept.AsEnumerable();
        }
        [HttpPost]
        public IEnumerable<ival_LookupCategoryValues> GetDes()
        {
            List<ival_LookupCategoryValues> listDept = new List<ival_LookupCategoryValues>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtDept = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDesignation");
            for (int i = 0; i < dtDept.Rows.Count; i++)
            {

                listDept.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtDept.Rows[i]["Lookup_ID"]),
                    Lookup_Value = Convert.ToString(dtDept.Rows[i]["Lookup_Value"])

                });


            }
            return listDept.AsEnumerable();
        }

        [HttpGet]
        public ActionResult Delete(int ID)
        {
            try
            {
                sqlDataAccess = new SQLDataAccess();
                hInputPara = new Hashtable();
                hInputPara.Add("@Branch_ID", ID);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteClientAndBranchByID", hInputPara);
                result.ToString();
                if (Convert.ToInt32(result) == 1)
                {
                    return RedirectToAction("Index", "ClientAndBranchMasterNew");
                }
                else
                {
                    return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteDept(int ID)
        {
            try
            {
                sqlDataAccess = new SQLDataAccess();
                hInputPara = new Hashtable();
                //hInputPara.Add("@Dept_ID", ID);
                //hInputPara.Add("@Branch_Id", ID);
                hInputPara.Add("@Client_ID", ID);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteDepartment", hInputPara);
                result.ToString();
                if (Convert.ToInt32(result) == 1)
                {
                    return RedirectToAction("Index", "ClientAndBranchMasterNew");
                }
                else
                {
                    return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

    }
}