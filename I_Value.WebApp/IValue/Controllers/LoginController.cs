﻿using I_Value.WebApp.Models;
using IvalueDataAccess;
using IValuePublishProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace I_Value.WebApp.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        SQLDataAccess sqlDataAccess;
        Hashtable hInputPara;
        public ActionResult Index1()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetLogindetails(string str)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            try
            {

                hInputPara.Add("@UserName", Convert.ToString(list1.Rows[0]["usernameID"]));
                hInputPara.Add("@Password", Convert.ToString(list1.Rows[0]["passwordID"]));
                Session["UserName"] = Convert.ToString(list1.Rows[0]["usernameID"]);
                DataTable get = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLogindetailsN", hInputPara);
                if (get.Rows.Count > 0 && get != null)

                {
                    hInputPara.Clear();

                    hInputPara.Add("@role", Convert.ToString(get.Rows[0]["role"]));
                    Session["Name"] = get.Rows[0]["fname"];


                    DataTable GetMenuList = sqlDataAccess.GetDatatableExecuteStoreProcedure("GetMenuList", hInputPara);

                    List<Menu> listEmp = new List<Menu>();
                    Session["Role"] = GetMenuList.Rows[0]["role_id"];
                    
                    if (GetMenuList.Rows.Count > 0 && GetMenuList != null)

                    {
                        for (int i = 0; i < GetMenuList.Rows.Count; i++)
                        {
                            listEmp.Add(new Menu
                            {
                                MainMenuId = Convert.ToInt16(GetMenuList.Rows[i]["menu_id"]),
                                MainMenuName = Convert.ToString(GetMenuList.Rows[i]["menu_name"]),
                                ControllerName = Convert.ToString(GetMenuList.Rows[i]["controller"]),
                                ActionName = Convert.ToString(GetMenuList.Rows[i]["action"]),
                                SubMenuName = Convert.ToString(GetMenuList.Rows[i]["sub_menu"])

                            }); ;

                        }
                        Session["MenuMaster"] = listEmp;
                        return Json(new { success = true, Message = "Login Successfully" }, JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        return Json(new { success = false, Message = "User does not exits exists" }, JsonRequestBehavior.AllowGet);


                    }

                }
                else
                {
                    return Json(new { success = false, Message = "User does not exits exists" }, JsonRequestBehavior.AllowGet);
                  

                }
                //Session["MenuMaster"] = listEmp;
                //if (Convert.ToString(result) == "1")
                //{
                //    return Json(new { success = true,Message="Login Successfully"},JsonRequestBehavior.AllowGet);
                //}
                //else 
                //{
                //    return Json(new { success = false, Message = "User does not exits exists" },JsonRequestBehavior.AllowGet);
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
        public ActionResult redirecttoaction()
        {
            return RedirectToAction("Index", "NewRequestor");
        }


        //[HttpPost]
        //public ActionResult GetLogindetails(string UserName, string Password)
        //{
        //    hInputPara = new Hashtable();
        //    sqlDataAccess = new SQLDataAccess();
        //    try
        //    {

        //        hInputPara.Add("@UserName", UserName);
        //        hInputPara.Add("@Password", Password);
        //        Session["UserName"] = UserName;
        //        var result = sqlDataAccess.ExecuteStoreProcedure("usp_GetLogindetails", hInputPara);
        //        if (Convert.ToString(result) == "1")
        //        {
        //            return Json(new { success = true, Message = "Login Successfully" }, JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //        {
        //            return Json(new { success = false, Message = "User does not exits exists" }, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}