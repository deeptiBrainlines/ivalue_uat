﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using I_Value.WebApp.Models;
using IvalueDataAccess;
using IValuePublishProject.Models;
using Newtonsoft.Json;

namespace I_Value.WebApp.Controllers
{
    public class RequestController : Controller
    {
        SQLDataAccess sqlDataAccess;
        Hashtable hInputPara;
        Hashtable hInputPara1;
        IvalueDataModel Datamodel = new IvalueDataModel();
        // GET: Request
        public ActionResult Index()
        {
            try
            {
                ViewModelRequest vmreq = new ViewModelRequest();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                DataTable dtRequests = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectRequestIndex");

                List<VMRequestIndex> Listrequests = new List<VMRequestIndex>();

                if (dtRequests.Rows.Count > 0)

                {
                    for (int i = 0; i < dtRequests.Rows.Count; i++)
                    {

                        Listrequests.Add(new VMRequestIndex
                        {
                            Request_ID = Convert.ToInt32(dtRequests.Rows[i]["Request_ID"]),
                            Requesttype = Convert.ToString(dtRequests.Rows[i]["Request_Type"]),
                            Project_ID = Convert.ToInt32(dtRequests.Rows[i]["Project_ID"]),
                            Project_Name = Convert.ToString(dtRequests.Rows[i]["Project_Name"]),
                           // Customer_Name = Convert.ToString(dtRequests.Rows[i]["Customer_Name"]),
                            
                            Legal_City = Convert.ToString(dtRequests.Rows[i]["City"]),
                            Legal_State = Convert.ToString(dtRequests.Rows[i]["State"]),
                           // Status = Convert.ToString(dtRequests.Rows[i]["Status"]),
                        });
                    }
                }
                vmreq.VMRequestIndex = Listrequests;

                return View(vmreq);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("Login", "Account");
            }

        }
        public ActionResult AddNewRequest()
        {
            ViewModelRequest vmreq = new ViewModelRequest();
            vmreq.ival_ClientMasters = GetClients();
            vmreq.ival_BranchMasters = GetBranches();
            vmreq.ival_States = GetStates();
            vmreq.ival_LookupCategoryValuespincode = Getpincode();
            vmreq.ival_LookupCategoryProjectType = GetProjectTypeMaster();
            vmreq.ival_LookupCategoryValtype = GetValuationTypeMaster();
            vmreq.ival_LookupCategoryValuesAssign = GetEmailMaster();
            vmreq.ival_RetailStates = GetStates();
            vmreq.ival_LookupCategoryValuesretailpincode = Getpincode();
            vmreq.ival_LookupCategoryValuesretailvaluetype = GetValuationTypeMaster();
            vmreq.ival_LookupCategoryValuesretailreporttype = GetReportType();
            return View(vmreq);
        }
        [HttpGet]
        public ActionResult NewRequestBank()
        {
            ViewModelRequest vm = new ViewModelRequest();
            vm.ival_BuilderGroupMasters = GetBuilderMaster();
            //vm.ival_UnitParameterMasters = ViewBag.unitdetails();
            // vm.ival_UnitParameterMasters = getUnitDetailsold();
            //vm.ival_LookupCategoryBoundaries = GetBoundaries();
            //vm.ival_LookupCategorySideMargins = GetSideMargins();
            vm.ival_LookupCategoryFloorTypes = GetFloorType();
            vm.ival_LookupCategoryValuesviewfromunit = GetviewUnits();
            vm.ival_LookupCategoryValuestypeofstructure = GetTypeofstructure();
            vm.ival_LookupCategoryValuesQualityConstruction = GetQualityOfConstruction();
            vm.ival_LookupCategoryValuesMaintenanceoProperty = GetMaintenanceProperty();
            vm.ival_LookupCategoryValuesOccupancyDetails = GetOccupancyDetails();
            return View(vm);

        }

        [HttpGet]
        public ActionResult NewRequestBankNew()
        {
            ViewModelRequest vm = new ViewModelRequest();
            vm.ival_BuilderGroupMasters = GetBuilderMaster();
            //vm.ival_UnitParameterMasters = ViewBag.unitdetails();
            // vm.ival_UnitParameterMasters = getUnitDetailsold();
            vm.ival_LookupCategoryBoundaries = GetBoundaries();
            vm.ival_LookupCategorySideMargins = GetSideMargins();
            vm.ival_LookupCategoryValuesviewfromunit = GetviewUnits();
            return View(vm);

        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetSideMargins()
        {
            List<ival_LookupCategoryValues> ListSideMargins = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtSideMargins = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSideMargin");
            for (int i = 0; i < dtSideMargins.Rows.Count; i++)
            {

                ListSideMargins.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtSideMargins.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtSideMargins.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtSideMargins.Rows[i]["Lookup_Value"])

                });

            }
            return ListSideMargins;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetviewUnits()
        {
            List<ival_LookupCategoryValues> ListView = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtunitview = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetViewUnit");
            for (int i = 0; i < dtunitview.Rows.Count; i++)
            {

                ListView.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtunitview.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtunitview.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtunitview.Rows[i]["Lookup_Value"])

                });

            }
            return ListView;
        }





        [HttpPost]
        public List<ival_LookupCategoryValues> GetBoundaries()
        {
            List<ival_LookupCategoryValues> ListBoundaries = new List<ival_LookupCategoryValues>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBoundaries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBoundryType");
            for (int i = 0; i < dtBoundaries.Rows.Count; i++)
            {

                ListBoundaries.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtBoundaries.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Value"])

                });

            }
            return ListBoundaries;
        }


        //[HttpPost]
        //public JsonResult NewRequestBanknew(string UnitType)
        //{
        //    ViewModelRequest vm = new ViewModelRequest();
        //    vm.ival_UnitParameterMasters = getUnitDetailsold(UnitType);

        //    //List<ival_UnitParameterMaster> Listunits = new List<ival_UnitParameterMaster>();
        //    //hInputPara = new Hashtable();
        //    //sqlDataAccess = new SQLDataAccess();
        //    //hInputPara.Add("@UnitType", UnitType);
        //    //DataTable dtBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitdetailsbyUnitID", hInputPara);
        //    //for (int i = 0; i < dtBuildermaster.Rows.Count; i++)
        //    //{

        //    //    Listunits.Add(new ival_UnitParameterMaster
        //    //    {
        //    //        UnitParamterID = Convert.ToInt32(dtBuildermaster.Rows[i]["UnitParamterID"]),
        //    //        Name = Convert.ToString(dtBuildermaster.Rows[i]["Name"])

        //    //    });

        //    //}
        //    //ViewBag.Units= Listunits;
        //    return Json(vm);

        //}
        ////[HttpPost]
        //public ActionResult NewRequestBank(ViewModelRequest model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        ConfigureViewModel(model);
        //        return View(model);
        //    }
        //    // save and redirect
        //    return RedirectToAction("Index");
        //}
        public ActionResult RetailsProperty()
        {
            return View();
        }



        public List<ival_BuilderGroupMaster> GetBuilderMaster()
        {
            List<ival_BuilderGroupMaster> ListBuilders = new List<ival_BuilderGroupMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuliderGroup");
            for (int i = 0; i < dtBuildermaster.Rows.Count; i++)
            {

                ListBuilders.Add(new ival_BuilderGroupMaster
                {
                    Builder_Group_Master_ID = Convert.ToInt32(dtBuildermaster.Rows[i]["Builder_Group_Master_ID"]),
                    BuilderGroupName = Convert.ToString(dtBuildermaster.Rows[i]["BuilderGroupName"])

                });


            }
            return ListBuilders;
        }

        [HttpPost]
        public JsonResult GetCompanyBuilderMaster(int Builder_Group_Master_ID)
        {
            List<ival_BuilderCompanyMaster> ListCompanyBuilders = new List<ival_BuilderCompanyMaster>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Builder_Group_Master_ID", Builder_Group_Master_ID);
            DataTable dtcompany = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuliderCompanyByGpID", hInputPara);
            for (int i = 0; i < dtcompany.Rows.Count; i++)
            {

                ListCompanyBuilders.Add(new ival_BuilderCompanyMaster
                {
                    Builder_Company_Master_ID = Convert.ToInt32(dtcompany.Rows[i]["Builder_Company_Master_ID"]),
                    Builder_Company_Name = Convert.ToString(dtcompany.Rows[i]["Builder_Company_Name"]),
                    Builder_Group_Master_ID = Convert.ToInt32(dtcompany.Rows[i]["Builder_Group_Master_ID"])
                });


            }


            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(ListCompanyBuilders);
            return Json(jsonProjectBuildingMaster);
        }

        [HttpPost]
        public JsonResult Getprojectsbycompid(int Builder_Company_Master_ID)
        {
            List<ival_ProjectMaster> Listprjbycomp = new List<ival_ProjectMaster>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Builder_Company_ID", Builder_Company_Master_ID);
            DataTable dtprjbycompany = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjetsCompanyID", hInputPara);
            for (int i = 0; i < dtprjbycompany.Rows.Count; i++)
            {

                Listprjbycomp.Add(new ival_ProjectMaster
                {
                    Project_ID = Convert.ToInt32(dtprjbycompany.Rows[i]["Project_ID"]),
                    Project_Name = Convert.ToString(dtprjbycompany.Rows[i]["Project_Name"]),
                    Builder_Company_ID = Convert.ToInt32(dtprjbycompany.Rows[i]["Builder_Company_ID"])
                });


            }


            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjects = jsonSerialiser.Serialize(Listprjbycomp);
            return Json(jsonProjects);
        }


        [HttpPost]
        public JsonResult GetBuildingListbyprojectID(int Project_ID)
        {
            List<ival_ProjectBuildingMaster> ival_ProjectBuildingMastersList = new List<ival_ProjectBuildingMaster>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Project_ID", Project_ID);
            DataTable dtBuilding = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectBuildingMasterList", hInputPara);
            for (int i = 0; i < dtBuilding.Rows.Count; i++)
            {
                ival_ProjectBuildingMastersList.Add(new ival_ProjectBuildingMaster
                {
                    Building_ID = Convert.ToInt32(dtBuilding.Rows[i]["Building_ID"]),
                    Building_Name = Convert.ToString(dtBuilding.Rows[i]["Building_Name"]),
                    No_of_Wings = Convert.ToString(dtBuilding.Rows[i]["Building_Name"]),
                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(ival_ProjectBuildingMastersList);
            return Json(jsonProjectBuildingMaster);
        }


        [HttpPost]
        public JsonResult GetWingListbybuildingID(int Building_ID)
        {
            List<ival_ProjBldgWingMaster> ival_ProjBldgWingMastersList = new List<ival_ProjBldgWingMaster>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Building_ID", Building_ID);
            DataTable dtBuilding = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBldgWingMasterList", hInputPara);
            for (int i = 0; i < dtBuilding.Rows.Count; i++)
            {
                ival_ProjBldgWingMastersList.Add(new ival_ProjBldgWingMaster
                {
                    Wing_ID = Convert.ToInt32(dtBuilding.Rows[i]["Wing_ID"]),
                    Wing_Name = Convert.ToString(dtBuilding.Rows[i]["Wing_Name"]),
                    No_Of_Wings = Convert.ToString(dtBuilding.Rows[i]["No_Of_Wings"])
                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(ival_ProjBldgWingMastersList);
            return Json(jsonProjectBuildingMaster);
        }
        [HttpPost]
        public JsonResult Getpropertybywingid(int Wing_ID)
        {
            List<ival_LookupCategoryValues> ival_Proplist = new List<ival_LookupCategoryValues>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Wing_ID", Wing_ID);
            DataTable dtprptype = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectTypebywing", hInputPara);
            for (int i = 0; i < dtprptype.Rows.Count; i++)
            {
                ival_Proplist.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtprptype.Rows[i]["Lookup_ID"]),
                    Lookup_Value = Convert.ToString(dtprptype.Rows[i]["Lookup_Value"]),
                    Lookup_Type = Convert.ToString(dtprptype.Rows[i]["Lookup_Type"])
                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(ival_Proplist);
            return Json(jsonProjectBuildingMaster);
        }

        [HttpPost]
        public JsonResult Getunitbypropid(int Lookup_ID)
        {
            List<ival_ProjBldgWingUnitMaster> ival_unitlist = new List<ival_ProjBldgWingUnitMaster>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Lookup_ID", Lookup_ID);
            DataTable dtunittype = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetunitBYpropID", hInputPara);
            for (int i = 0; i < dtunittype.Rows.Count; i++)
            {
                ival_unitlist.Add(new ival_ProjBldgWingUnitMaster
                {
                    Unit_ID = Convert.ToInt32(dtunittype.Rows[i]["Unit_ID"]),
                    UType = Convert.ToString(dtunittype.Rows[i]["UType"]),
                    PTypeName = Convert.ToString(dtunittype.Rows[i]["PTypeName"])
                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(ival_unitlist);
            return Json(jsonProjectBuildingMaster);
        }
        //[HttpPost]
        //public List<ival_BuilderCompanyMaster> GetCompanyBuilderMaster()
        //{
        //    List<ival_BuilderCompanyMaster> ListCompanyBuilders = new List<ival_BuilderCompanyMaster>();
        //    hInputPara = new Hashtable();
        //    sqlDataAccess = new SQLDataAccess();
        //    DataTable dtCompBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuliderCompanyName");
        //    for (int i = 0; i < dtCompBuildermaster.Rows.Count; i++)
        //    {

        //        ListCompanyBuilders.Add(new ival_BuilderCompanyMaster
        //        {
        //            Builder_Company_Master_ID = Convert.ToInt32(dtCompBuildermaster.Rows[i]["Builder_Company_Master_ID"]),
        //            Builder_Company_Name = Convert.ToString(dtCompBuildermaster.Rows[i]["Builder_Company_Name"]),
        //            Builder_Group_Master_ID = Convert.ToInt32(dtCompBuildermaster.Rows[i]["Builder_Group_Master_ID"])
        //        });


        //    }
        //    return ListCompanyBuilders;
        //}


        //[HttpGet]
        //public JsonResult GetCompanyBuilderMaster(int ID)
        //{
        //    var data = GetCompanyBuilderMaster()
        //       .Where(l => l.Builder_Group_Master_ID == ID)
        //       .Select(l => new { Value = l.Builder_Company_Master_ID, Text = l.Builder_Company_Name });
        //    return Json(data, JsonRequestBehavior.AllowGet);
        //}

        //private void ConfigureViewModel(ViewModelRequest model)
        //{
        //    List<ival_BuilderGroupMaster> cities = GetBuilderMaster();
        //    model.ival_BuilderGroupMasters = GetBuilderMaster();
        //    model.BuilderMasterslist = new SelectList(cities, "Builder_Group_Master_ID", "BuilderGroupName");

        //    if (model.SelectedModelGroupMaster.HasValue)
        //    {
        //        IEnumerable<ival_BuilderCompanyMaster> localities = GetCompanyBuilderMaster().Where(l => l.Builder_Company_Master_ID == model.SelectedModelGroupMaster.Value);
        //        model.CompanyList = new SelectList(localities, "Builder_Company_Master_ID", "Builder_Company_Name");
        //    }
        //    else
        //    {
        //        model.CompanyList = new SelectList(Enumerable.Empty<SelectListItem>());
        //    }
        //    if (model.SelectedModelCompanyGpMaster.HasValue)
        //    {
        //        IEnumerable<ival_BuilderCompanyMaster> subLocalities = GetCompanyBuilderMaster().Where(l => l.Builder_Company_Master_ID == model.SelectedModelCompanyGpMaster.Value);
        //    }
        //    //else
        //    //{
        //    //    model.SubLocalityList = new SelectList(Enumerable.Empty<SelectListItem>());
        //    //}
        //}

        [HttpPost]
        public List<ival_ClientMaster> GetClients()
        {
            List<ival_ClientMaster> ListClients = new List<ival_ClientMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtClient = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetClient");
            for (int i = 0; i < dtClient.Rows.Count; i++)
            {
                ListClients.Add(new ival_ClientMaster
                {
                    Client_ID = Convert.ToInt32(dtClient.Rows[i]["Client_ID"]),
                    Client_Name = Convert.ToString(dtClient.Rows[i]["Client_Name"])

                });
            }
            return ListClients;
        }
        [HttpPost]
        public List<ival_BranchMaster> GetBranches()
        {
            List<ival_BranchMaster> ListBranches = new List<ival_BranchMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBranch = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBranch");
            for (int i = 0; i < dtBranch.Rows.Count; i++)
            {
                ListBranches.Add(new ival_BranchMaster
                {
                    Branch_ID = Convert.ToInt32(dtBranch.Rows[i]["Branch_ID"]),
                    Branch_Name = Convert.ToString(dtBranch.Rows[i]["Branch_Name"])

                });
            }
            return ListBranches;
        }


        [HttpPost]
        public List<ival_States> GetStates()
        {
            List<ival_States> ListStates = new List<ival_States>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtCompBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetStateByID");
            for (int i = 0; i < dtCompBuildermaster.Rows.Count; i++)
            {

                ListStates.Add(new ival_States
                {
                    State_ID = Convert.ToInt32(dtCompBuildermaster.Rows[i]["State_ID"]),
                    State_Name = Convert.ToString(dtCompBuildermaster.Rows[i]["State_Name"])

                });


            }
            return ListStates;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> Getpincode()
        {
            List<ival_LookupCategoryValues> Listpincode = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtCompBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPincode");
            for (int i = 0; i < dtCompBuildermaster.Rows.Count; i++)
            {

                Listpincode.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtCompBuildermaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtCompBuildermaster.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtCompBuildermaster.Rows[i]["Lookup_Value"])

                });


            }
            return Listpincode;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetProjectTypeMaster()
        {
            List<ival_LookupCategoryValues> Listprojtype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtprojecttypermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectType");
            for (int i = 0; i < dtprojecttypermaster.Rows.Count; i++)
            {

                Listprojtype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtprojecttypermaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtprojecttypermaster.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtprojecttypermaster.Rows[i]["Lookup_Value"])

                });


            }
            return Listprojtype;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetValuationTypeMaster()
        {
            List<ival_LookupCategoryValues> Listvaltype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtvalmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetValuationType");
            for (int i = 0; i < dtvalmaster.Rows.Count; i++)
            {

                Listvaltype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtvalmaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtvalmaster.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtvalmaster.Rows[i]["Lookup_Value"])

                });


            }
            return Listvaltype;
        }

        [HttpPost]
        public List<ival_EmployeeMaster> GetEmailMaster()
        {
            List<ival_EmployeeMaster> ListEmp = new List<ival_EmployeeMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtemailmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCompEmail");
            for (int i = 0; i < dtemailmaster.Rows.Count; i++)
            {

                ListEmp.Add(new ival_EmployeeMaster
                {
                    Employee_ID = Convert.ToInt32(dtemailmaster.Rows[i]["Employee_ID"]),
                    Company_Email_ID = Convert.ToString(dtemailmaster.Rows[i]["Company_Email_ID"]),


                });


            }
            return ListEmp;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetReportType()
        {
            List<ival_LookupCategoryValues> Listrpttype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetReportType");
            for (int i = 0; i < dtrptmaster.Rows.Count; i++)
            {

                Listrpttype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtrptmaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtrptmaster.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtrptmaster.Rows[i]["Lookup_Value"])

                });


            }
            return Listrpttype;
        }

        //to save requests by client
        [HttpPost]
        public ActionResult SaveRequests(string str, string str1)
        {
            try
            {
                DataTable list = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));

                for (int i = 0; i < list1.Rows.Count; i++)
                {
                    string isRetail = Convert.ToString(list1.Rows[i]["isinstitute"]);
                    bool inst;
                    if (isRetail == "Bank/NBFC")
                    {
                        inst = false;
                    }
                    else
                    {
                        inst = true;
                    }
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@IsRetail", inst);
                    hInputPara.Add("@Client_ID", Convert.ToString(list1.Rows[i]["Bank_Name"]));
                    hInputPara.Add("@DateOfRequest", Convert.ToString(list1.Rows[i]["DateOfRequest"]));
                    hInputPara.Add("@Request_Code", Convert.ToString(list1.Rows[i]["ValuationRequestId"]));
                    hInputPara.Add("@Customer_ID", Convert.ToString(list1.Rows[i]["CustomerId"]));
                    hInputPara.Add("@Customer_Name", Convert.ToString(list1.Rows[i]["CustomerName"]));
                    hInputPara.Add("@Customer_Contact_No", Convert.ToString(list1.Rows[i]["CustomerContactNo"]));
                    hInputPara.Add("@Address1", Convert.ToString(list1.Rows[i]["Address1"]));
                    hInputPara.Add("@Address2", Convert.ToString(list1.Rows[i]["Address2"]));
                    hInputPara.Add("@City", Convert.ToString(list1.Rows[i]["City"]));
                    hInputPara.Add("@District", Convert.ToString(list1.Rows[i]["District"]));
                    hInputPara.Add("@State", Convert.ToString(list1.Rows[i]["State"]));
                    hInputPara.Add("@PinCode", Convert.ToString(list1.Rows[i]["Pincode"]));
                    hInputPara.Add("@AssignedToEmpID", Convert.ToString(list1.Rows[i]["AssignTo"]));
                    hInputPara.Add("@PropertyType", Convert.ToString(list1.Rows[i]["PropertyType"]));
                    hInputPara.Add("@ValuationType", Convert.ToString(list1.Rows[i]["ValuationType"]));
                    sqlDataAccess.ExecuteStoreProcedure("usp_InsertequestDetails", hInputPara);

                }

                //return Redirect("NewRequestBank");
                //return Json(Url.Action("Index", "Request"));
                return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public ActionResult SaveRequestsRetail(string str3)
        {
            try
            {
                DataTable list = (DataTable)JsonConvert.DeserializeObject(str3, (typeof(DataTable)));


                for (int i = 0; i < list.Rows.Count; i++)
                {
                    string isRetail = Convert.ToString(list.Rows[i]["isinstitute"]);
                    bool inst;
                    if (isRetail == "Bank/NBFC")
                    {
                        inst = false;
                    }
                    else
                    {
                        inst = true;
                    }
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@IsInstitute", inst);
                    hInputPara.Add("@Client_Name", Convert.ToString(list.Rows[i]["RequestorName"]));
                    hInputPara.Add("@Client_Address1", Convert.ToString(list.Rows[i]["Address1"]));
                    hInputPara.Add("@Client_Address2", Convert.ToString(list.Rows[i]["Address2"]));
                    hInputPara.Add("@Client_City", Convert.ToString(list.Rows[i]["City"]));
                    hInputPara.Add("@Client_State", Convert.ToString(list.Rows[i]["State"]));
                    hInputPara.Add("@Client_Pincode", Convert.ToString(list.Rows[i]["Pincode"]));
                    hInputPara.Add("@Client_Contact_Email", Convert.ToString(list.Rows[i]["EmailId"]));
                    hInputPara.Add("@Client_Contact_Num", Convert.ToString(list.Rows[i]["ContactNumber1"]));
                    hInputPara.Add("@ValuationType", Convert.ToString(list.Rows[i]["ValuationType"]));
                    hInputPara.Add("@ReportType", Convert.ToString(list.Rows[i]["ReportType"]));
                    sqlDataAccess.ExecuteStoreProcedure("usp_InsertretailDetails", hInputPara);


                }

                return RedirectToAction("RetailsProperty", "Request");
                // return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Dynamic units list

        public List<ival_UnitParameterMaster> getUnitDetailsold(string UnitType)
        {
            List<ival_UnitParameterMaster> Listunits = new List<ival_UnitParameterMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@UnitType", UnitType);
            DataTable dtBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitdetailsbyUnitID", hInputPara);
            for (int i = 0; i < dtBuildermaster.Rows.Count; i++)
            {

                Listunits.Add(new ival_UnitParameterMaster
                {
                    UnitParamterID = Convert.ToInt32(dtBuildermaster.Rows[i]["UnitParamterID"]),
                    Name = Convert.ToString(dtBuildermaster.Rows[i]["Name"])

                });


            }
            return Listunits;
        }

        [HttpPost]
        public JsonResult GetUnitDetaiGetUnitDetailsls(string UnitType, int projectID)
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_UnitParametersValue> ProjectUnitLists = new List<ival_UnitParametersValue>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@UnitType", UnitType);
            hInputPara.Add("@projectID", projectID);
            DataTable dtUNITS = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitdetailsbyUnitID", hInputPara);
            for (int i = 0; i < dtUNITS.Rows.Count; i++)
            {


                ProjectUnitLists.Add(new ival_UnitParametersValue
                {
                    Unit_ID = Convert.ToInt32(dtUNITS.Rows[i]["Unit_ID"]),
                    UnitParameter_ID = Convert.ToInt16(dtUNITS.Rows[i]["UnitParameter_ID"]),
                    UnitParameter_Value = Convert.ToString(dtUNITS.Rows[i]["UnitParameter_Value"]),
                    UnitParaValue_ID = Convert.ToInt16(dtUNITS.Rows[i]["UnitParaValue_ID"]),
                    Name = Convert.ToString(dtUNITS.Rows[i]["Name"]),
                    DeviationDescription = Convert.ToString(dtUNITS.Rows[i]["Deviation_Description"]),
                    Latitude = Convert.ToString(dtUNITS.Rows[i]["Cord_Latitude"]),
                    Longitude = Convert.ToString(dtUNITS.Rows[i]["Cord_Longitude"]),
                });

            }
            ViewBag.unitdetails = ProjectUnitLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(ProjectUnitLists);
            return Json(jsonProjectBuildingMaster);


        }

        [HttpPost]
        public JsonResult GetSideMarginDetails(string UnitType)
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_ProjectSideMargin> ProjectsideUnitLists = new List<ival_ProjectSideMargin>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@UnitType", "Flat");
            DataTable dtsides = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSideMarginbyUtype", hInputPara);
            for (int i = 0; i < dtsides.Rows.Count; i++)
            {
                ProjectsideUnitLists.Add(new ival_ProjectSideMargin
                {
                    Unit_ID = Convert.ToInt32(dtsides.Rows[i]["Unit_ID"]),
                    Side_Margin_ID = Convert.ToInt16(dtsides.Rows[i]["Side_Margin_ID"]),
                    Side_Margin_Description = Convert.ToString(dtsides.Rows[i]["Side_Margin_Description"]),
                    As_per_Approved = Convert.ToString(dtsides.Rows[i]["As_per_Approved"]),
                    As_per_Measurement = Convert.ToString(dtsides.Rows[i]["As_per_Measurement"]),
                    Percentage_Deviation = Convert.ToString(dtsides.Rows[i]["Percentage_Deviation"]),
                });

            }
            ViewBag.unitdetails = ProjectsideUnitLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(ProjectsideUnitLists);
            return Json(jsonProjectBuildingMaster);
        }

        [HttpPost]
        public JsonResult GetBoundriesDetails(string UnitType)
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_ProjectBoundaries> ProjectboundryLists = new List<ival_ProjectBoundaries>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@UnitType", "Flat");
            DataTable dtsides = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBoundariesbyUtype", hInputPara);
            for (int i = 0; i < dtsides.Rows.Count; i++)
            {
                ProjectboundryLists.Add(new ival_ProjectBoundaries
                {
                    Unit_ID = Convert.ToInt32(dtsides.Rows[i]["Unit_ID"]),
                    Boundry_ID = Convert.ToInt16(dtsides.Rows[i]["Boundry_ID"]),
                    Boundry_Name = Convert.ToString(dtsides.Rows[i]["Boundry_Name"]),
                    AsPer_Document = Convert.ToString(dtsides.Rows[i]["AsPer_Document"]),
                    AsPer_Site = Convert.ToString(dtsides.Rows[i]["AsPer_Site"]),

                });
            }
            ViewBag.unitdetails = ProjectboundryLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(ProjectboundryLists);
            return Json(jsonProjectBuildingMaster);
        }


        [HttpPost]
        public List<ival_LookupCategoryValues> GetFloorType()
        {
            List<ival_LookupCategoryValues> ListFloorType = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtFloorType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorType");
            for (int i = 0; i < dtFloorType.Rows.Count; i++)
            {

                ListFloorType.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtFloorType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtFloorType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtFloorType.Rows[i]["Lookup_Value"])

                });

            }
            return ListFloorType;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetTypeofstructure()
        {
            List<ival_LookupCategoryValues> Listtype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dttype = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTypeOfstructure");
            for (int i = 0; i < dttype.Rows.Count; i++)
            {

                Listtype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dttype.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dttype.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dttype.Rows[i]["Lookup_Value"])

                });

            }
            return Listtype;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetQualityOfConstruction()
        {
            List<ival_LookupCategoryValues> Listquality = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtqlty = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_QualityOfConstruction");
            for (int i = 0; i < dtqlty.Rows.Count; i++)
            {

                Listquality.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtqlty.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtqlty.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtqlty.Rows[i]["Lookup_Value"])

                });

            }
            return Listquality;
        }


        [HttpPost]
        public List<ival_LookupCategoryValues> GetMaintenanceProperty()
        {
            List<ival_LookupCategoryValues> Listmainprop = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtmainprop = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_MaintenanceofProperty");
            for (int i = 0; i < dtmainprop.Rows.Count; i++)
            {

                Listmainprop.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtmainprop.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtmainprop.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtmainprop.Rows[i]["Lookup_Value"])

                });

            }
            return Listmainprop;
        }


        [HttpPost]
        public List<ival_LookupCategoryValues> GetOccupancyDetails()
        {
            List<ival_LookupCategoryValues> Listoccudetails = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtoccu = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_OccupancyDetails");
            for (int i = 0; i < dtoccu.Rows.Count; i++)
            {

                Listoccudetails.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtoccu.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtoccu.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtoccu.Rows[i]["Lookup_Value"])

                });

            }
            return Listoccudetails;
        }

        [HttpPost]
        public ActionResult UpdateAllRequest(string str1, string str2, string str3, string str4, string str5)
        {
            try
            {
                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));
                DataTable list2 = (DataTable)JsonConvert.DeserializeObject(str2, (typeof(DataTable)));
                DataTable list3 = (DataTable)JsonConvert.DeserializeObject(str3, (typeof(DataTable)));
                DataTable list4 = (DataTable)JsonConvert.DeserializeObject(str4, (typeof(DataTable)));
                DataTable list5 = (DataTable)JsonConvert.DeserializeObject(str5, (typeof(DataTable)));


                for (int i = 0; i < list5.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    hInputPara1 = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara1.Add("@PropertyType", list5.Rows[0]["PropertyType"].ToString());

                    DataTable dtreqID = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRequestID", hInputPara1);

                    hInputPara.Add("@Request_ID", dtreqID.Rows[0]["Request_ID"].ToString());
                    hInputPara.Add("@Project_ID", list5.Rows[i]["projectID"].ToString());
                    hInputPara.Add("@Building_ID", list5.Rows[i]["Building_ID"].ToString());
                    hInputPara.Add("@Wing_ID", list5.Rows[i]["Wing_ID"].ToString());
                    hInputPara.Add("@Unit_ID", list5.Rows[i]["Unit_ID"].ToString());
                    hInputPara.Add("@Type_of_Structure", list5.Rows[i]["ddltypeofstructure"].ToString());
                    hInputPara.Add("@Current_Age_of_Structure", list5.Rows[i]["txtcurrentage"].ToString());
                    hInputPara.Add("@Expected_Balance_Life", list5.Rows[i]["txtExpected_Balance_Life"].ToString());
                    hInputPara.Add("@Quality_of_Construction", list5.Rows[i]["ddlqltycons"].ToString());
                    hInputPara.Add("@Mainte_of_Property", list5.Rows[i]["ddlmainprop"].ToString());
                    hInputPara.Add("@Occupancy_Details", list5.Rows[i]["ddloccupancydetails"].ToString());
                    hInputPara.Add("@Tenant_Name", list5.Rows[i]["txtTenant_Name"].ToString());
                    hInputPara.Add("@Lease_Period", list5.Rows[i]["txtLease_Period"].ToString());
                    hInputPara.Add("@Lease_Rental", list5.Rows[i]["txtLeaseRental"].ToString());

                    sqlDataAccess.ExecuteStoreProcedure("usp_Update_RequestByreqID", hInputPara);
                }
                var ProjectId = Convert.ToInt32(list5.Rows[0]["projectID"].ToString());
                ViewModelRequest vmappr = new ViewModelRequest();
                vmappr.ival_UnitParametersValues = GetUNITPARAMDetails(ProjectId);
                vmappr.ival_ProjectSideMargin = GetSideMarginsDetails(ProjectId);
                vmappr.ival_ProjectBoundaries = GetBoundryDetails(ProjectId);

                foreach (ival_UnitParametersValue ival_unitparams in vmappr.ival_UnitParametersValues)
                {

                    for (int i1 = 0; i1 < list1.Rows.Count; i1++)
                    {
                        if (Convert.ToString(ival_unitparams.UnitParameter_ID) == list1.Rows[i1]["UnitParameter_ID"].ToString())
                        {
                            hInputPara = new Hashtable();
                            sqlDataAccess = new SQLDataAccess();
                            hInputPara.Add("@UnitParameter_ID", ival_unitparams.UnitParameter_ID);
                            hInputPara.Add("@UnitParameter_Value", list1.Rows[i1]["UnitParameter_Value"].ToString());

                            sqlDataAccess.ExecuteStoreProcedure("usp_UpdateUnitparameters", hInputPara);
                            //break;
                        }
                    }
                    //  break;
                }

                foreach (ival_ProjectSideMargin ival_Projsidemargins in vmappr.ival_ProjectSideMargin)
                {

                    for (int i2 = 0; i2 < list2.Rows.Count; i2++)
                    {
                        //if (ival_Projsidemargins.Side_Margin_Description == list2.Rows[i2]["SidemarginsType"].ToString())
                        //{
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();


                        hInputPara.Add("@Side_Margin_ID", ival_Projsidemargins.Side_Margin_ID);
                        //hInputPara.Add("@Side_Margin_Description", list2.Rows[i2]["SidemarginsType"].ToString());
                        hInputPara.Add("@As_per_Approved", list2.Rows[i2]["Asperapprovedplan"].ToString());
                        hInputPara.Add("@As_per_Measurement", list2.Rows[i2]["Aspermeasurement"].ToString());
                        hInputPara.Add("@Percentage_Deviation", list2.Rows[i2]["perdevistion"].ToString());
                        //hInputPara.Add("@Deviation_Description", list2.Rows[i2]["perdevistion"].ToString());

                        sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectSideMargins", hInputPara);
                        //break;
                        //}
                    }
                    //  break;
                }

                foreach (ival_ProjectBoundaries ival_ProjBoundaries in vmappr.ival_ProjectBoundaries)
                {

                    for (int i3 = 0; i3 < list3.Rows.Count; i3++)
                    {
                        if (ival_ProjBoundaries.Boundry_Name == list3.Rows[i3]["BoundariesType"].ToString())
                        {
                            hInputPara = new Hashtable();
                            sqlDataAccess = new SQLDataAccess();
                            sqlDataAccess = new SQLDataAccess();
                            hInputPara.Add("@Boundry_ID", ival_ProjBoundaries.Boundry_ID);
                            //  hInputPara.Add("@Boundry_Name", list3.Rows[i3]["BoundariesType"].ToString());
                            hInputPara.Add("@AsPer_Document", list3.Rows[i3]["asperdoc"].ToString());
                            hInputPara.Add("@AsPer_Site", list3.Rows[i3]["aspersite"].ToString());
                            sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectBoundaries", hInputPara);

                        }
                    }
                }

                return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ival_UnitParametersValue> GetUNITPARAMDetails(int Project_ID)
        {
            List<ival_UnitParametersValue> ProjectboundryLists = new List<ival_UnitParametersValue>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Project_ID", Project_ID);
            DataTable dtUNITPARA = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitparametersByProjectID", hInputPara);
            for (int i = 0; i < dtUNITPARA.Rows.Count; i++)
            {
                ProjectboundryLists.Add(new ival_UnitParametersValue
                {

                    UnitParameter_ID = Convert.ToInt32(dtUNITPARA.Rows[i]["UnitParameter_ID"]),
                    Unit_ID = Convert.ToInt32(dtUNITPARA.Rows[i]["Unit_ID"]),
                    UnitParaValue_ID = Convert.ToInt32(dtUNITPARA.Rows[i]["UnitParaValue_ID"]),
                    UnitParameter_Value = Convert.ToString(dtUNITPARA.Rows[i]["UnitParameter_Value"]),


                });
            }
            return ProjectboundryLists;
        }




        public List<ival_ProjectSideMargin> GetSideMarginsDetails(int Project_ID)
        {
            List<ival_ProjectSideMargin> ProjectsideLists = new List<ival_ProjectSideMargin>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Project_ID", Project_ID);
            DataTable dtsides = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSideMarginsByProjectID", hInputPara);
            for (int i = 0; i < dtsides.Rows.Count; i++)
            {
                ProjectsideLists.Add(new ival_ProjectSideMargin
                {
                    Side_Margin_ID = Convert.ToInt32(dtsides.Rows[i]["Side_Margin_ID"]),
                    As_per_Approved = Convert.ToString(dtsides.Rows[i]["As_per_Approved"]),
                    As_per_Measurement = Convert.ToString(dtsides.Rows[i]["As_per_Measurement"]),
                    Percentage_Deviation = Convert.ToString(dtsides.Rows[i]["Percentage_Deviation"]),
                    Deviation_Description = Convert.ToString(dtsides.Rows[i]["Deviation_Description"]),
                    Side_Margin_Description = Convert.ToString(dtsides.Rows[i]["Side_Margin_Description"]),

                });
            }
            return ProjectsideLists;
        }


        public List<ival_ProjectBoundaries> GetBoundryDetails(int Project_ID)
        {
            List<ival_ProjectBoundaries> ProjectboundryLists = new List<ival_ProjectBoundaries>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Project_ID", Project_ID);
            DataTable dtboundry = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBoundariesByProjectID", hInputPara);
            for (int i = 0; i < dtboundry.Rows.Count; i++)
            {
                ProjectboundryLists.Add(new ival_ProjectBoundaries
                {
                    Boundry_ID = Convert.ToInt32(dtboundry.Rows[i]["Boundry_ID"]),
                    Boundry_Name = Convert.ToString(dtboundry.Rows[i]["Boundry_Name"]),
                    AsPer_Document = Convert.ToString(dtboundry.Rows[i]["AsPer_Document"]),


                });
            }
            return ProjectboundryLists;
        }
        [HttpPost]
        public JsonResult GetUnitPriceDetails(string UnitType, int projectID)
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_UnitPriceParametersValue> ProjectUnitLists = new List<ival_UnitPriceParametersValue>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@UnitType", UnitType);
            hInputPara.Add("@projectID", projectID);
            DataTable dtUNITS = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitPricedetailsbyUnitID", hInputPara);
            for (int i = 0; i < dtUNITS.Rows.Count; i++)
            {


                ProjectUnitLists.Add(new ival_UnitPriceParametersValue
                {
                    Unit_ID = Convert.ToInt32(dtUNITS.Rows[i]["Unit_ID"]),
                    UnitPriceParameter_Value = Convert.ToString(dtUNITS.Rows[i]["UnitPriceParameter_Value"]),
                    UnitPriceParameter_ID = Convert.ToInt32(dtUNITS.Rows[i]["UnitPriceParameter_ID"]),
                    UnitPriceParaValue_ID = Convert.ToInt32(dtUNITS.Rows[i]["UnitPriceParaValue_ID"]),
                    Name = Convert.ToString(dtUNITS.Rows[i]["Name"]),
                    UnitCost = Convert.ToString(dtUNITS.Rows[i]["UnitCost"]),
                    Total_Cost_of_Property = Convert.ToString(dtUNITS.Rows[i]["Total_Cost_of_Property"]),
                    Stage_of_Construction = Convert.ToString(dtUNITS.Rows[i]["Stage_of_Construction"]),
                    Stage_Description = Convert.ToString(dtUNITS.Rows[i]["Stage_Description"]),
                    Percent_Completed = Convert.ToString(dtUNITS.Rows[i]["Percent_Completed"]),
                    Valuation_of_unit = Convert.ToString(dtUNITS.Rows[i]["Valuation_of_unit"]),
                    Current_Value = Convert.ToString(dtUNITS.Rows[i]["Current_Value"]),
                    Government_Rate = Convert.ToString(dtUNITS.Rows[i]["Government_Rate"]),
                    Government_Value = Convert.ToString(dtUNITS.Rows[i]["Government_Value"]),

                });

            }
            ViewBag.unitdetails = ProjectUnitLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(ProjectUnitLists);
            return Json(jsonProjectBuildingMaster);


        }

        [HttpPost]
        public JsonResult AddProfile(string files)
        {
            var profile = Request.Files;

            string imgname = string.Empty;
            string ImageName = string.Empty;

            //Following code will check that image is there 
            //If it will Upload or else it will use Default Image

            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var logo = System.Web.HttpContext.Current.Request.Files["file"];
                if (logo.ContentLength > 0)
                {
                    var profileName = Path.GetFileName(logo.FileName);
                    var ext = Path.GetExtension(logo.FileName);

                    ImageName = profileName;
                    var comPath = Server.MapPath("/images/") + ImageName;

                    logo.SaveAs(comPath);
                    files = comPath;
                }

            }
            else
                files = Server.MapPath("/images/") + "profile.jpg";

            int response = 1;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void Upload()
        {
            if (Request.Files.Count != 0)
            {

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];

                    var fileName = Path.GetFileName(file.FileName);

                    // Find its length and convert it to byte array
                    int ContentLength = file.ContentLength;

                    // Create Byte Array
                    byte[] bytImg = new byte[ContentLength];

                    // Read Uploaded file in Byte Array
                    file.InputStream.Read(bytImg, 0, ContentLength);

                    var path = Path.Combine(Server.MapPath("~/images/"), fileName);
                    file.SaveAs(path);

                }

            }

        }

        [HttpPost]
        public ActionResult UpdatePricesRequest(string str1, string str2)
        {
            try
            {
                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));
                DataTable list2 = (DataTable)JsonConvert.DeserializeObject(str2, (typeof(DataTable)));
                var ProjectId = Convert.ToInt32(list2.Rows[0]["projectID"].ToString());
                var Unit_ID = Convert.ToString(list2.Rows[0]["Unit_ID"].ToString());

                //  DataTable dtgetunittype = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitPriceparametersByProjectID", hInputPara);

                for (int i = 0; i < list2.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();

                    sqlDataAccess = new SQLDataAccess();

                    hInputPara.Add("@Unit_ID", list2.Rows[i]["Unit_ID"].ToString());
                    hInputPara.Add("@UnitCost", list2.Rows[i]["unitCost"].ToString());
                    hInputPara.Add("@Total_Cost_of_Property", list2.Rows[i]["CostValueTotal"].ToString());
                    hInputPara.Add("@Stage_of_Construction", list2.Rows[i]["StageOfConstruction"].ToString());
                    hInputPara.Add("@Stage_Description", list2.Rows[i]["DescriptionofStageOfConstructionId"].ToString());
                    hInputPara.Add("@Percent_Completed", list2.Rows[i]["workCompleted"].ToString());
                    hInputPara.Add("@Valuation_of_unit", list2.Rows[i]["valuationOfUnit"].ToString());
                    hInputPara.Add("@Current_Value", list2.Rows[i]["currentValueOfPropertyUnderConstructionUnit"].ToString());
                    hInputPara.Add("@Government_Rate", list2.Rows[i]["governmentRateSqMt"].ToString());
                    hInputPara.Add("@Government_Value", list2.Rows[i]["governmentValueSqMt"].ToString());

                    sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectunitdetails", hInputPara);
                }


                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                hInputPara1 = new Hashtable();
                hInputPara1.Add("@Unit_ID", Unit_ID);

                DataTable dtgetunittype = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitTypeRequest", hInputPara1);

                var UType = dtgetunittype.Rows[0]["UType"].ToString();
                ViewModelRequest vmappr = new ViewModelRequest();
                vmappr.ival_UnitPriceParametersValues = GetPriceparamDetails(ProjectId, UType);


                foreach (ival_UnitPriceParametersValue ival_unitparams in vmappr.ival_UnitPriceParametersValues)
                {

                    for (int i1 = 0; i1 < list1.Rows.Count; i1++)
                    {
                        if (Convert.ToString(ival_unitparams.UnitPriceParameter_ID) == list1.Rows[i1]["UnitPriceParameter_ID"].ToString())
                        {
                            hInputPara = new Hashtable();
                            sqlDataAccess = new SQLDataAccess();
                            hInputPara.Add("@UnitPriceParameter_ID", ival_unitparams.UnitPriceParameter_ID);
                            hInputPara.Add("@UnitPriceParameter_Value", list1.Rows[i1]["UnitPriceParameter_Value"].ToString());
                            sqlDataAccess.ExecuteStoreProcedure("usp_UpdateUnitPriceparameters", hInputPara);
                            //break;
                        }
                    }
                    //  break;
                }




                return Json(new { success = true, Message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ival_UnitPriceParametersValue> GetPriceparamDetails(int Project_ID, string UType)
        {
            List<ival_UnitPriceParametersValue> ProjectboundryLists = new List<ival_UnitPriceParametersValue>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Project_ID", Project_ID);
            hInputPara.Add("@UType", UType);
            DataTable dtPricedata = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitPriceparametersByProjectID", hInputPara);
            for (int i = 0; i < dtPricedata.Rows.Count; i++)
            {
                ProjectboundryLists.Add(new ival_UnitPriceParametersValue
                {

                    UnitPriceParameter_ID = Convert.ToInt32(dtPricedata.Rows[i]["UnitPriceParameter_ID"]),
                    Unit_ID = Convert.ToInt32(dtPricedata.Rows[i]["Unit_ID"]),
                    UnitPriceParaValue_ID = Convert.ToInt32(dtPricedata.Rows[i]["UnitPriceParaValue_ID"]),
                    UnitPriceParameter_Value = Convert.ToString(dtPricedata.Rows[i]["UnitPriceParameter_Value"]),


                });
            }
            return ProjectboundryLists;
        }

    }
}