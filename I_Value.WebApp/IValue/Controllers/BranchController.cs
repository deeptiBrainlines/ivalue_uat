﻿using I_Value.WebApp.Models;
using IValuePublishProject.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using IvalueDataAccess;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Globalization;

namespace I_Value.WebApp.Controllers
{
    public class BranchController : Controller
    {
        SQLDataAccess sqlDataAccess;
        Hashtable hInputPara;
        VMBranchMaster vMBranchMaster;
        IvalueDataModel Datamodel = new IvalueDataModel();
        // GET: Branch
        public ActionResult Index()
        {
            try
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                DataTable dtBranch = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBranch");

                List<ival_BranchMaster> listbranch = new List<ival_BranchMaster>();

                if (dtBranch.Rows.Count > 0&& dtBranch!=null)

                {
                    for (int i = 0; i < dtBranch.Rows.Count; i++)
                    {

                        listbranch.Add(new ival_BranchMaster
                        {
                            Branch_ID= Convert.ToInt32(dtBranch.Rows[i]["Branch_ID"]),
                            Branch_Code= Convert.ToString(dtBranch.Rows[i]["Branch_Code"]),
                            Branch_Name = Convert.ToString(dtBranch.Rows[i]["Branch_Name"]),
                            Branch_State = Convert.ToString(dtBranch.Rows[i]["Branch_State"]),
                            Lessor_First_Name = Convert.ToString(dtBranch.Rows[i]["First_Name"])
                           
                        });


                    }
                }
                return View(listbranch);

            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
            //return View();
        }
        
        public ActionResult AddNewBranch()
        {
            VMBranchMaster vMBranchMaster = new VMBranchMaster();
            vMBranchMaster.ival_StatesList = GetState();
            vMBranchMaster.ival_CitiesList = GetCity();
            //vMBranchMaster.pincodeList = GetPincode();
            vMBranchMaster.ival_DistrictsList = GetDistricts();
            vMBranchMaster.ival_LocalityList = GetLocality();

            vMBranchMaster.AccountTypeList = GetAccountType();
            vMBranchMaster.branchHeadsList = GetBranchHeadList();
            return View(vMBranchMaster);
        }

        [HttpPost]
        public ActionResult AddNewBranch(String str)
        {
            string resultStr = string.Empty;
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

            try
            {
                IEnumerable<ival_States> ival_StatesList = GetState();
                var branchState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMBranchMaster.SelectedBranchStateID).Select(row => row.State_Name.ToString());

                IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                var branchPincode = ival_LocalityList.AsEnumerable().Where(row => row.Loc_Id == vMBranchMaster.SelectedBranchPincode).Select(row => row.Loc_Name.ToString());

                for (int i = 0; i < list1.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();

                    /*if (Convert.ToBoolean(list1.Rows[i]["Is_Leased"]))
                    {*/
                    /* var lessorState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMBranchMaster.SelectedLessorStateID).Select(row => row.State_Name.ToString());
                     var landlordState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMBranchMaster.SelectedLandlordStateID).Select(row => row.State_Name.ToString());
                     vMBranchMaster.Lessor_State = lessorState.FirstOrDefault();
                     vMBranchMaster.LandLord_State = landlordState.FirstOrDefault();

                     var lessorPincode = pincodeList.AsEnumerable().Where(row => row.Loc_Id == vMBranchMaster.SelectedLessorPincode).Select(row => row.Loc_Name.ToString());
                     var landlordPincode = pincodeList.AsEnumerable().Where(row => row.Loc_Id == vMBranchMaster.SelectedLandlordPincode).Select(row => row.Loc_Name.ToString());*/
                    //if (Convert.ToBoolean(list1.Rows[i]["Is_Leased"]))
                    if (Convert.ToBoolean(list1.Rows[i]["Is_Leased"]))
                    {
                       /* if (Convert.ToString(list1.Rows[i]["Lessor_Fname"]) != "")
                    {*/

                        hInputPara.Add("@Is_Leased", 1);
                        hInputPara.Add("@Lessor_First_Name", Convert.ToString(list1.Rows[i]["Lessor_Fname"]));
                        hInputPara.Add("@Lessor_Middle_Name", Convert.ToString(list1.Rows[i]["Lessor_Mname"]));
                        hInputPara.Add("@Lessor_Last_Name", Convert.ToString(list1.Rows[i]["Lessor_Lname"]));
                        hInputPara.Add("@Lessor_Address1", Convert.ToString(list1.Rows[i]["Lessor_Address1"]));
                        hInputPara.Add("@Lessor_Address2", Convert.ToString(list1.Rows[i]["Lessor_Address2"]));
                        hInputPara.Add("@Lessor_City", Convert.ToString(list1.Rows[i]["Lessor_City1"]));
                        hInputPara.Add("@Lessor_Locality", Convert.ToString(list1.Rows[i]["Lessor_City"]));
                        hInputPara.Add("@Lessor_District", Convert.ToString(list1.Rows[i]["Lessor_Dist"]));
                        hInputPara.Add("@Lessor_State", Convert.ToString(list1.Rows[i]["Lessor_State"]));
                        hInputPara.Add("@Lessor_Pincode", Convert.ToInt32(list1.Rows[i]["Lessor_Pin"]));
                        hInputPara.Add("@Lessor_Contact_Num", Convert.ToString(list1.Rows[i]["Lessor_Contact"]));
                        hInputPara.Add("@Lessor_Email_ID", Convert.ToString(list1.Rows[i]["Lessor_Email"]));

                        /*var dateString = list1.Rows[i]["Lease_Start_Date"].ToString();
                        var format = "dd/MM/yyyy";
                        if (dateString != "")
                        {
                            var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                            hInputPara.Add("@Lease_Start_Date", dateTime);
                        }
                        var dateString1 = list1.Rows[i]["Lease_End_Date"].ToString();
                        var format1 = "dd/MM/yyyy";
                        if (dateString1 != "")
                        {
                            var dateTime = DateTime.ParseExact(dateString1, format1, CultureInfo.InvariantCulture);
                            hInputPara.Add("@Lease_End_Date", dateTime);
                        }*/

                        hInputPara.Add("@Lease_Start_Date", CDateTime(list1.Rows[i]["Lease_Start_Date"].ToString()));
                       // hInputPara.Add("@Lease_Start_Date", Convert.ToString(list1.Rows[i]["Lease_Start_Date"]));
                        hInputPara.Add("@Lease_End_Date", CDateTime(list1.Rows[i]["Lease_End_Date"].ToString()));
                       //hInputPara.Add("@Lease_End_Date", Convert.ToString(list1.Rows[i]["Lease_End_Date"]));
                        hInputPara.Add("@Monthly_Rent", Convert.ToString(list1.Rows[i]["Monthly_Rent"]));
                        hInputPara.Add("@Maintaince_Charges", Convert.ToString(list1.Rows[i]["Maintaince_Charges"]));
                        hInputPara.Add("@Other_Charges", Convert.ToString(list1.Rows[i]["Other_Charges"]));

                        hInputPara.Add("@LandLord_Bank", Convert.ToString(list1.Rows[i]["LL_Bank"]));
                        hInputPara.Add("@LandLord_Branch", Convert.ToString(list1.Rows[i]["LL_Branch"]));
                        hInputPara.Add("@LandLord_Address1", Convert.ToString(list1.Rows[i]["LL_Address1"]));
                        hInputPara.Add("@LandLord_Address2", Convert.ToString(list1.Rows[i]["LL_Address2"]));
                        hInputPara.Add("@LandLord_City", Convert.ToString(list1.Rows[i]["LL_City1"]));
                        hInputPara.Add("@LandLord_Locality", Convert.ToString(list1.Rows[i]["LL_City"]));
                        hInputPara.Add("@LandLord_District", Convert.ToString(list1.Rows[i]["LL_Dist"]));
                        hInputPara.Add("@LandLord_State", Convert.ToString(list1.Rows[i]["LL_State"]));
                        hInputPara.Add("@LandLord_Pincode", Convert.ToInt32(list1.Rows[i]["LL_Pin"]));
                        hInputPara.Add("@Account_Type", Convert.ToString(list1.Rows[i]["Acc_Type"]));
                        hInputPara.Add("@Account_Number", Convert.ToString(list1.Rows[i]["Acc_No"]));
                        hInputPara.Add("@IFSC_Num", Convert.ToString(list1.Rows[i]["IFSC_Num"]));
                        hInputPara.Add("@PAN_Num", Convert.ToString(list1.Rows[i]["PAN_Num"]));
                        hInputPara.Add("@GSTIN_Num", Convert.ToString(list1.Rows[i]["GSTIN_Num"]));

                        hInputPara.Add("@Branch_Code", Convert.ToString(list1.Rows[i]["Br_Code"]));
                        hInputPara.Add("@Branch_Name", Convert.ToString(list1.Rows[i]["Br_Name"]));
                        hInputPara.Add("@Branch_Address1", Convert.ToString(list1.Rows[i]["Br_Addr1"]));
                        hInputPara.Add("@Branch_Address2", Convert.ToString(list1.Rows[i]["Br_Addr2"]));
                        hInputPara.Add("@Branch_City", Convert.ToString(list1.Rows[i]["Br_City"]));
                        hInputPara.Add("@Branch_District", Convert.ToString(list1.Rows[i]["Br_Dist"]));
                        hInputPara.Add("@Branch_State", Convert.ToString(list1.Rows[i]["Br_State"]));
                        hInputPara.Add("@Branch_Pincode", Convert.ToInt32(list1.Rows[i]["Br_Pin"]));

                        hInputPara.Add("@Branch_Loc", Convert.ToString(list1.Rows[i]["Branch_Loc"]));
                    }
                    else
                    {
                        hInputPara.Add("@Branch_Code", Convert.ToString(list1.Rows[i]["Br_Code"]));
                        hInputPara.Add("@Branch_Name", Convert.ToString(list1.Rows[i]["Br_Name"]));
                        hInputPara.Add("@Branch_Address1", Convert.ToString(list1.Rows[i]["Br_Addr1"]));
                        hInputPara.Add("@Branch_Address2", Convert.ToString(list1.Rows[i]["Br_Addr2"]));
                        hInputPara.Add("@Branch_City", Convert.ToString(list1.Rows[i]["Br_City"]));
                        hInputPara.Add("@Branch_District", Convert.ToString(list1.Rows[i]["Br_Dist"]));
                        hInputPara.Add("@Branch_State", Convert.ToString(list1.Rows[i]["Br_State"]));
                        hInputPara.Add("@Branch_Pincode", Convert.ToInt32(list1.Rows[i]["Br_Pin"]));

                        hInputPara.Add("@Branch_Loc", Convert.ToString(list1.Rows[i]["Branch_Loc"]));
                        hInputPara.Add("@Is_Leased", 0);
                    }
                    hInputPara.Add("@BranchHeadID", Convert.ToInt32(list1.Rows[i]["Br_Head"]));
                    hInputPara.Add("@Created_By", string.Empty);

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertBranchNew", hInputPara);

                    if (Convert.ToInt32(result) == 1)
                    {
                        return RedirectToAction("Index", "Branch");
                    }
                    else
                    {
                        return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                    }
                }
               
            }
            catch (Exception e)
            {
                //e.InnerException.ToString();
                return Json(new { e.Message, JsonRequestBehavior.AllowGet });
            }

            return Json(new { success = true, Message = "Inserted SuccessFull" }, JsonRequestBehavior.AllowGet);
        }

        public DateTime CDateTime(string SEDateTime)
        {



            int seconds = DateTime.Now.Second;
            int min = DateTime.Now.Minute;
            int hr = DateTime.Now.Hour;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-CA", true);
            string st = SEDateTime;

            string[] sdatesplit = st.Split('/');


            string startdate = sdatesplit[2] + "-" + sdatesplit[1] + "-" + sdatesplit[0] + ' ' + hr + ':' + min + ':' + seconds;
            DateTime dt = DateTime.Parse(startdate, provider, System.Globalization.DateTimeStyles.NoCurrentDateDefault);
            return dt;
        }

        /* [HttpPost]
         public ActionResult AddNewBranch(VMBranchMaster vMBranchMaster)
         {
             try
             {
                 IEnumerable<ival_States> ival_StatesList = GetState();
                 IEnumerable<ival_Locality> pincodeList = GetPincode();
                 IEnumerable<ival_Cities> ival_CitiesList = GetCity();
                 IEnumerable<ival_Districts> ival_DistrictsList = GetDistricts();
                 IEnumerable<ival_Locality> ival_LocalityList = GetLocality();


                 hInputPara = new Hashtable();
                 sqlDataAccess = new SQLDataAccess();

                 hInputPara = new Hashtable();
                 var branchState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMBranchMaster.SelectedBranchStateID).Select(row => row.State_Name.ToString());

                 vMBranchMaster.Branch_State = branchState.FirstOrDefault();


                 var branchPincode = pincodeList.AsEnumerable().Where(row => row.Loc_Id == vMBranchMaster.SelectedBranchPincode).Select(row => row.Pin_Code.ToString());


                 if (Convert.ToBoolean( vMBranchMaster.Is_Leased ))
                 {
                     var lessortState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMBranchMaster.SelectedLessorStateID).Select(row => row.State_Name.ToString());
                     var bankState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMBranchMaster.SelectedLandlordStateID).Select(row => row.State_Name.ToString());
                     vMBranchMaster.Lessor_State = lessortState.FirstOrDefault();
                     vMBranchMaster.LandLord_State = bankState.FirstOrDefault();


                     var lessorPincode = pincodeList.AsEnumerable().Where(row => row.Loc_Id == vMBranchMaster.SelectedLessorPincode).Select(row => row.Pin_Code.ToString());
                     var landlordPincode = pincodeList.AsEnumerable().Where(row => row.Loc_Id == vMBranchMaster.SelectedLandlordPincode).Select(row => row.Pin_Code.ToString());

                     hInputPara.Add("@Is_Leased", 1);
                     hInputPara.Add("@Lessor_First_Name", Convert.ToString(vMBranchMaster.Lessor_First_Name));
                     hInputPara.Add("@Lessor_Middle_Name", Convert.ToString(vMBranchMaster.Lessor_Middle_Name));
                     hInputPara.Add("@Lessor_Last_Name", Convert.ToString(vMBranchMaster.Lessor_Last_Name));
                     hInputPara.Add("@Lessor_Address1", Convert.ToString(vMBranchMaster.Lessor_Address1));
                     hInputPara.Add("@Lessor_Address2", Convert.ToString(vMBranchMaster.Lessor_Address2));
                     hInputPara.Add("@Lessor_City", Convert.ToString(vMBranchMaster.Lessor_City));
                     hInputPara.Add("@Lessor_District", Convert.ToString(vMBranchMaster.Lessor_District));
                     hInputPara.Add("@Lessor_State", Convert.ToString(vMBranchMaster.Lessor_State));
                     hInputPara.Add("@Lessor_Pincode", Convert.ToInt32(lessorPincode.FirstOrDefault()));
                     hInputPara.Add("@Lessor_Contact_Num", Convert.ToString(vMBranchMaster.Lessor_Contact_Num));
                     hInputPara.Add("@Lessor_Email_ID", Convert.ToString(vMBranchMaster.Lessor_Email_ID));
                     hInputPara.Add("@Lease_Start_Date", vMBranchMaster.Lease_Start_Date);
                     hInputPara.Add("@Lease_End_Date", vMBranchMaster.Lease_End_Date);
                     hInputPara.Add("@Monthly_Rent", Convert.ToString(vMBranchMaster.Monthly_Rent));
                     hInputPara.Add("@Maintaince_Charges", Convert.ToString(vMBranchMaster.Maintaince_Charges));
                     hInputPara.Add("@Other_Charges", Convert.ToString(vMBranchMaster.Other_Charges));
                     hInputPara.Add("@LandLord_Bank", Convert.ToString(vMBranchMaster.LandLord_Bank));
                     hInputPara.Add("@LandLord_Branch", Convert.ToString(vMBranchMaster.LandLord_Branch));
                     hInputPara.Add("@LandLord_Address1", Convert.ToString(vMBranchMaster.LandLord_Address1));
                     hInputPara.Add("@LandLord_Address2", Convert.ToString(vMBranchMaster.LandLord_Address2));
                     hInputPara.Add("@LandLord_City", Convert.ToString(vMBranchMaster.LandLord_City));
                     hInputPara.Add("@LandLord_District", Convert.ToString(vMBranchMaster.LandLord_District));
                     hInputPara.Add("@LandLord_State", Convert.ToString(vMBranchMaster.LandLord_State));
                     hInputPara.Add("@LandLord_Pincode", Convert.ToInt32(landlordPincode.FirstOrDefault()));
                     hInputPara.Add("@Account_Type", Convert.ToString(vMBranchMaster.SelectedAccountType));
                     hInputPara.Add("@Account_Number", Convert.ToString(vMBranchMaster.Account_Number));
                     hInputPara.Add("@IFSC_Num", Convert.ToString(vMBranchMaster.IFSC_Num));
                     hInputPara.Add("@PAN_Num", Convert.ToString(vMBranchMaster.PAN_Num));
                     hInputPara.Add("@GSTIN_Num", Convert.ToString(vMBranchMaster.GSTIN_Num));

                     hInputPara.Add("@Branch_Code", vMBranchMaster.Branch_Code);
                     hInputPara.Add("@Branch_Name", Convert.ToString(vMBranchMaster.Branch_Name));
                     hInputPara.Add("@Branch_Address1", Convert.ToString(vMBranchMaster.Branch_Address1));
                     hInputPara.Add("@Branch_Address2", Convert.ToString(vMBranchMaster.Branch_Address2));
                     hInputPara.Add("@Branch_City", Convert.ToString(vMBranchMaster.Branch_City));
                     hInputPara.Add("@Branch_District", Convert.ToString(vMBranchMaster.Branch_District));
                     hInputPara.Add("@Branch_State", Convert.ToString(vMBranchMaster.Branch_State));
                     hInputPara.Add("@Branch_Pincode", Convert.ToInt32(branchPincode.FirstOrDefault()));
                 }
                 else
                 {


                     hInputPara.Add("@Branch_Code", vMBranchMaster.Branch_Code);
                     hInputPara.Add("@Branch_Name", Convert.ToString(vMBranchMaster.Branch_Name));
                     hInputPara.Add("@Branch_Address1", Convert.ToString(vMBranchMaster.Branch_Address1));
                     hInputPara.Add("@Branch_Address2", Convert.ToString(vMBranchMaster.Branch_Address2));
                     hInputPara.Add("@Branch_City", Convert.ToString(vMBranchMaster.Branch_City));
                     hInputPara.Add("@Branch_District", Convert.ToString(vMBranchMaster.Branch_District));
                     hInputPara.Add("@Branch_State", Convert.ToString(vMBranchMaster.Branch_State));
                     hInputPara.Add("@Branch_Pincode", Convert.ToInt32(branchPincode.FirstOrDefault()));

                 }
                 hInputPara.Add("@BranchHeadID", vMBranchMaster.SelectedBranchHead);
                 hInputPara.Add("@Created_By", string.Empty);
                 var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertBranch", hInputPara);

                 if (Convert.ToInt32(result) == 1)
                 {
                     return RedirectToAction("Index", "Branch");
                 }
                 else
                 {
                     return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                 }
             }
             catch (Exception e)
             {
                 //e.InnerException.ToString();
                 return Json(new { e.Message, JsonRequestBehavior.AllowGet });
             }

         }*/
        public ActionResult EditBranch(int ID)
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //return View();
            }
            VMBranchMaster vMBranchMaster = new VMBranchMaster(); 
            ival_BranchMaster objBranch = new ival_BranchMaster();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            try
            {
                hInputPara.Add("@Branch_ID", ID);
                DataTable dtBranch = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBranchByBranchId", hInputPara);
                if (dtBranch != null && dtBranch.Rows.Count > 0)
                {

                    object valueBranch_ID = Convert.ToInt32(dtBranch.Rows[0]["Branch_ID"]);
                    if (dtBranch.Columns.Contains("Branch_ID") && valueBranch_ID != DBNull.Value && valueBranch_ID.ToString() != "")
                    {
                        objBranch.Branch_ID = Convert.ToInt32(dtBranch.Rows[0]["Branch_ID"]);
                        vMBranchMaster.Branch_ID = Convert.ToInt32(dtBranch.Rows[0]["Branch_ID"]);
                    }
                    else
                        objBranch.Branch_ID = 0;


                    object valueBranch_Code = dtBranch.Rows[0]["Branch_Code"];
                    if (dtBranch.Columns.Contains("Branch_Code") && valueBranch_Code != null && valueBranch_ID.ToString()!="")
                        objBranch.Branch_Code = dtBranch.Rows[0]["Branch_Code"].ToString();
                    else
                        objBranch.Branch_Code = string.Empty;

                    object valueBranchHeadID = dtBranch.Rows[0]["BranchHeadID"];
                    if (dtBranch.Columns.Contains("BranchHeadID") && valueBranchHeadID != null && valueBranchHeadID.ToString() != "")
                        vMBranchMaster.SelectedBranchHead = Convert.ToInt32( dtBranch.Rows[0]["BranchHeadID"].ToString());
                    else
                        vMBranchMaster.SelectedBranchHead = 0;

                    object valueBranch_Name =dtBranch.Rows[0]["Branch_Name"];
                    if (dtBranch.Columns.Contains("Branch_Name") && valueBranch_Name != null && valueBranch_Name.ToString()!="")
                        objBranch.Branch_Name = dtBranch.Rows[0]["Branch_Name"].ToString();
                    else
                        objBranch.Branch_Name = string.Empty;

                    object valueBranch_Address1 = dtBranch.Rows[0]["Branch_Address1"];
                    if (dtBranch.Columns.Contains("Branch_Address1") && valueBranch_Address1 != null && valueBranch_Address1.ToString()!="")
                        objBranch.Branch_Address1 = dtBranch.Rows[0]["Branch_Address1"].ToString();
                    else
                        objBranch.Branch_Address1 = string.Empty;

                    object valueBranch_Address2 = dtBranch.Rows[0]["Branch_Address2"];
                    if (dtBranch.Columns.Contains("Branch_Address2") && valueBranch_Address2 != null && valueBranch_Address2.ToString()!="")
                        objBranch.Branch_Address2 = dtBranch.Rows[0]["Branch_Address2"].ToString();
                    else
                        objBranch.Branch_Address2 = string.Empty;

                    object valueBranch_City = dtBranch.Rows[0]["Branch_City"];
                    if (dtBranch.Columns.Contains("Branch_City") && valueBranch_City != null && valueBranch_City.ToString()!="")
                        objBranch.Branch_City = dtBranch.Rows[0]["Branch_City"].ToString();
                    else
                        objBranch.Branch_City = string.Empty;

                    object valueBranch_District = dtBranch.Rows[0]["Branch_District"];
                    if (dtBranch.Columns.Contains("Branch_District") && valueBranch_District != null && valueBranch_District.ToString()!="")
                        objBranch.Branch_District = dtBranch.Rows[0]["Branch_District"].ToString();
                    else
                        objBranch.Branch_District = string.Empty;

                    object valueBranch_State = dtBranch.Rows[0]["Branch_State"];
                    if (dtBranch.Columns.Contains("Branch_State") && valueBranch_State != null && valueBranch_State.ToString()!="")
                        objBranch.Branch_State = dtBranch.Rows[0]["Branch_State"].ToString();
                    else
                        objBranch.Branch_State = string.Empty;

                    object valueBranch_Pincode = dtBranch.Rows[0]["Branch_Pincode"];
                    if (dtBranch.Columns.Contains("Branch_Pincode") && valueBranch_Pincode !=null && valueBranch_Pincode.ToString() != "")
                        vMBranchMaster.SelectedBranchPincode = Convert.ToInt32(dtBranch.Rows[0]["Branch_Pincode"].ToString());
                    else
                        vMBranchMaster.SelectedBranchPincode = 0;

                    object valueBranch_Loc = dtBranch.Rows[0]["Branch_Loc"];
                    if (dtBranch.Columns.Contains("Branch_Loc") && valueBranch_Loc != null && valueBranch_Loc.ToString() != "")
                        vMBranchMaster.Branch_Loc = dtBranch.Rows[0]["Branch_Loc"].ToString();
                    else
                        vMBranchMaster.Branch_Loc = string.Empty;

                    object valueIs_Leased = Convert.ToString(dtBranch.Rows[0]["Is_Leased"]);
                    if (valueIs_Leased.ToString() != "")
                    {
                        //objBranch.Is_Leased = true;
                        if (Convert.ToBoolean(dtBranch.Rows[0]["Is_Leased"].ToString()))
                        {
                            objBranch.Is_Leased = true;
                        }
                    }
                    else
                    {
                        objBranch.Is_Leased = false;
                    }
                    if ( Convert.ToBoolean(objBranch.Is_Leased) && valueIs_Leased != DBNull.Value)
                    {
                        object valueLessor_First_Name = dtBranch.Rows[0]["Lessor_First_Name"];
                        if (dtBranch.Columns.Contains("Lessor_First_Name") && valueLessor_First_Name != null && valueLessor_First_Name.ToString()!="")
                            objBranch.Lessor_First_Name = dtBranch.Rows[0]["Lessor_First_Name"].ToString();
                        else
                            objBranch.Lessor_First_Name = string.Empty;

                        object valueLessor_Middle_Name = dtBranch.Rows[0]["Lessor_Middle_Name"];
                        if (dtBranch.Columns.Contains("Lessor_Middle_Name") && valueLessor_Middle_Name != null && valueLessor_Middle_Name.ToString()!="")
                            objBranch.Lessor_Middle_Name = dtBranch.Rows[0]["Lessor_Middle_Name"].ToString();
                        else
                            objBranch.Lessor_Middle_Name = string.Empty;

                        object valueLessor_Last_Name =dtBranch.Rows[0]["Lessor_Last_Name"];
                        if (dtBranch.Columns.Contains("Lessor_Last_Name") && valueLessor_Last_Name !=  null && valueLessor_Last_Name.ToString()!="")
                            objBranch.Lessor_Last_Name = dtBranch.Rows[0]["Lessor_Last_Name"].ToString();
                        else
                            objBranch.Lessor_Last_Name = string.Empty;

                        object valueLessor_Address1 = dtBranch.Rows[0]["Lessor_Address1"];
                        if (dtBranch.Columns.Contains("Lessor_Address1") && valueLessor_Address1 != null && valueLessor_Address1.ToString()!="")
                            objBranch.Lessor_Address1 = dtBranch.Rows[0]["Lessor_Address1"].ToString();
                        else
                            objBranch.Lessor_Address1 = string.Empty;

                        object valueLessor_Address2 = dtBranch.Rows[0]["Lessor_Address2"];
                        if (dtBranch.Columns.Contains("Lessor_Address2") && valueLessor_Address2 != null && valueLessor_Address2.ToString()!="")
                            objBranch.Lessor_Address2 = dtBranch.Rows[0]["Lessor_Address2"].ToString();
                        else
                            objBranch.Lessor_Address2 = string.Empty;

                        object valueLessor_Loc = dtBranch.Rows[0]["Lessor_Locality"];
                        if (dtBranch.Columns.Contains("Lessor_Locality") && valueLessor_Loc != null && valueLessor_Loc.ToString() != "")
                            objBranch.Lessor_Loc = dtBranch.Rows[0]["Lessor_Locality"].ToString();
                        else
                            objBranch.Lessor_Loc = string.Empty;

                        object valueLessor_District = dtBranch.Rows[0]["Lessor_District"];
                        if (dtBranch.Columns.Contains("Lessor_District") && valueLessor_District != null && valueLessor_District.ToString() != "")
                            objBranch.Lessor_District = dtBranch.Rows[0]["Lessor_District"].ToString();
                        else
                            objBranch.Lessor_District = string.Empty;

                        object valueLessor_City = dtBranch.Rows[0]["Lessor_City"];
                        if (dtBranch.Columns.Contains("Lessor_City") && valueLessor_City != null && valueLessor_City.ToString() != "")
                            objBranch.Lessor_City = dtBranch.Rows[0]["Lessor_City"].ToString();
                        else
                            objBranch.Lessor_City = string.Empty;

                        object valueLessor_State = Convert.ToString(dtBranch.Rows[0]["Lessor_State"]);
                        if (dtBranch.Columns.Contains("Lessor_State") && valueLessor_State != null && valueLessor_State.ToString() != "")
                            objBranch.Lessor_State = dtBranch.Rows[0]["Lessor_State"].ToString();
                        else
                            objBranch.Lessor_State = string.Empty;

                        object valueLessor_Pincode = Convert.ToInt32(dtBranch.Rows[0]["Lessor_Pincode"]);
                        if (dtBranch.Columns.Contains("Lessor_Pincode") && valueLessor_Pincode != null && valueLessor_Pincode.ToString() != "")
                            vMBranchMaster.SelectedLessorPincode = Convert.ToInt32(dtBranch.Rows[0]["Lessor_Pincode"].ToString());
                        else
                            vMBranchMaster.SelectedLessorPincode = 0;

                        object valueLessor_Contact_Num = dtBranch.Rows[0]["Lessor_Contact_Num"];
                        if (dtBranch.Columns.Contains("Lessor_Contact_Num") && valueLessor_Contact_Num != null)
                            objBranch.Lessor_Contact_Num = dtBranch.Rows[0]["Lessor_Contact_Num"].ToString();
                        else
                            objBranch.Lessor_Contact_Num = string.Empty;

                        object valueLessor_Email_ID = Convert.ToString(dtBranch.Rows[0]["Lessor_Email_ID"]);
                        if (dtBranch.Columns.Contains("Lessor_Email_ID") && valueLessor_Email_ID != null && valueLessor_Email_ID.ToString()!="")
                            objBranch.Lessor_Email_ID = dtBranch.Rows[0]["Lessor_Email_ID"].ToString();
                        else
                            objBranch.Lessor_Email_ID = string.Empty;

                        /*object valueLease_Start_Date = Convert.ToString(dtBranch.Rows[0]["Lease_Start_Date"]);
                        if (dtBranch.Columns.Contains("Lease_Start_Date") && valueLease_Start_Date != null && valueLease_Start_Date.ToString() != "")
                            objBranch.Lease_Start_Date = dtBranch.Rows[0]["Lease_Start_Date"].ToString();
                        else
                            objBranch.Lease_Start_Date = string.Empty;

                        object valueLease_End_Date = Convert.ToString(dtBranch.Rows[0]["Lease_End_Date"]);
                        if (dtBranch.Columns.Contains("Lease_End_Date") && valueLease_End_Date != null && valueLease_End_Date.ToString() != "")
                            objBranch.Lease_End_Date = dtBranch.Rows[0]["Lease_End_Date"].ToString();
                        else
                            objBranch.Lease_End_Date = string.Empty;*/

                        object valueLease_Start_Date = dtBranch.Rows[0]["Lease_Start_Date"];
                        if (dtBranch.Columns.Contains("Lease_Start_Date") && valueLease_Start_Date != null && valueLease_Start_Date.ToString() != "")
                        {
                            DateTime Lease_Start_Date = Convert.ToDateTime(dtBranch.Rows[0]["Lease_Start_Date"]);
                            objBranch.Lease_Start_Date = Lease_Start_Date.Date;
                        }

                        else
                            objBranch.Lease_Start_Date = null;

                        object valueLease_End_Date = dtBranch.Rows[0]["Lease_End_Date"];
                        if (dtBranch.Columns.Contains("Lease_End_Date") && valueLease_End_Date != null && valueLease_End_Date.ToString() != "")
                        {
                            DateTime Lease_End_Date = Convert.ToDateTime(dtBranch.Rows[0]["Lease_End_Date"]);
                            objBranch.Lease_End_Date = Lease_End_Date.Date;
                        }
                        else
                            objBranch.Lease_End_Date = null;
                        //{
                        //    DateTime dt = Convert.ToDateTime(dtBranch.Rows[0]["Lease_Start_Date"]);
                        //    string date = dt.ToString("dd.MM.yyy");
                        //    objBranch.Lease_Start_Date = Convert.ToDateTime(date);

                        //}
                        //else
                        //{
                        //    objBranch.Lease_Start_Date = Convert.ToDateTime(string.Empty);
                        //}

                        //object valueLease_End_Date = Convert.ToDateTime(dtBranch.Rows[0]["Lease_End_Date"]);
                        //if (dtBranch.Columns.Contains("Lease_End_Date") && valueLease_End_Date != DBNull.Value)
                        //{
                        //    DateTime dt = Convert.ToDateTime(dtBranch.Rows[0]["Lease_End_Date"]);
                        //    string date= dt.ToString("dd.MM.yyy");
                        //    objBranch.Lease_End_Date = Convert.ToDateTime(date);
                        //}
                        //else
                        //    objBranch.Lease_End_Date = Convert.ToDateTime(string.Empty);

                        object valueMonthly_Rent = dtBranch.Rows[0]["Monthly_Rent"];
                        if (dtBranch.Columns.Contains("Monthly_Rent") && valueMonthly_Rent != null && valueMonthly_Rent.ToString() != "")
                            objBranch.Monthly_Rent = dtBranch.Rows[0]["Monthly_Rent"].ToString();
                        else
                            objBranch.Monthly_Rent = string.Empty;

                        object valueMaintaince_Charges = dtBranch.Rows[0]["Maintaince_Charges"];
                        if (dtBranch.Columns.Contains("Maintaince_Charges") && valueMaintaince_Charges != null && valueMaintaince_Charges.ToString() != "")
                            objBranch.Maintaince_Charges = dtBranch.Rows[0]["Maintaince_Charges"].ToString();
                        else
                            objBranch.Maintaince_Charges = string.Empty;

                        object valueOther_Charges = dtBranch.Rows[0]["Other_Charges"];
                        if (dtBranch.Columns.Contains("Other_Charges") && valueOther_Charges != null && valueOther_Charges.ToString() != "")
                            objBranch.Other_Charges = dtBranch.Rows[0]["Other_Charges"].ToString();
                        else
                            objBranch.Other_Charges = string.Empty;

                        object valueLandLord_Bank = Convert.ToString(dtBranch.Rows[0]["LandLord_Bank"]);
                        if (dtBranch.Columns.Contains("LandLord_Bank") && valueLandLord_Bank != DBNull.Value && valueLandLord_Bank.ToString() != "")
                            objBranch.LandLord_Bank = dtBranch.Rows[0]["LandLord_Bank"].ToString();
                        else
                            objBranch.LandLord_Bank = string.Empty;

                        object valueLandLord_Branch = Convert.ToString(dtBranch.Rows[0]["LandLord_Branch"]);
                        if (dtBranch.Columns.Contains("LandLord_Branch") && valueLandLord_Branch != DBNull.Value && valueLandLord_Branch.ToString() != "")
                            objBranch.LandLord_Branch = dtBranch.Rows[0]["LandLord_Branch"].ToString();
                        else
                            objBranch.LandLord_Branch = string.Empty;

                        object valueLandLord_Address1 = Convert.ToString(dtBranch.Rows[0]["LandLord_Address1"]);
                        if (dtBranch.Columns.Contains("LandLord_Address1") && valueLandLord_Address1 != DBNull.Value && valueLandLord_Address1.ToString() != "")
                            objBranch.LandLord_Address1 = dtBranch.Rows[0]["LandLord_Address1"].ToString();
                        else
                            objBranch.LandLord_Address1 = string.Empty;

                        object valueLandLord_Address2 = Convert.ToString(dtBranch.Rows[0]["LandLord_Address2"]);
                        if (dtBranch.Columns.Contains("LandLord_Address2") && valueLandLord_Address2 != DBNull.Value && valueLandLord_Address2.ToString() != "")
                            objBranch.LandLord_Address2 = dtBranch.Rows[0]["LandLord_Address2"].ToString();
                        else
                            objBranch.LandLord_Address2 = string.Empty;

                        object valueLandLord_City = Convert.ToString(dtBranch.Rows[0]["LandLord_City"]);
                        if (dtBranch.Columns.Contains("LandLord_City") && valueLandLord_City != DBNull.Value && valueLandLord_City.ToString() != "")
                            objBranch.LandLord_City = dtBranch.Rows[0]["LandLord_City"].ToString();
                        else
                            objBranch.LandLord_City = string.Empty;

                        object valueLandLord_Loc = Convert.ToString(dtBranch.Rows[0]["LandLord_Locality"]);
                        if (dtBranch.Columns.Contains("LandLord_Locality") && valueLandLord_Loc != DBNull.Value && valueLandLord_Loc.ToString() != "")
                            objBranch.LandLord_Loc = dtBranch.Rows[0]["LandLord_Locality"].ToString();
                        else
                            objBranch.LandLord_Loc = string.Empty;

                        object valueLandLord_District = Convert.ToString(dtBranch.Rows[0]["LandLord_District"]);
                        if (dtBranch.Columns.Contains("LandLord_District") && valueLandLord_District != DBNull.Value && valueLandLord_District.ToString() != "")
                            objBranch.LandLord_District = dtBranch.Rows[0]["LandLord_District"].ToString();
                        else
                            objBranch.LandLord_District = string.Empty;

                        object valueLandLord_State = Convert.ToString(dtBranch.Rows[0]["LandLord_State"]);
                        if (dtBranch.Columns.Contains("LandLord_State") && valueLandLord_State != DBNull.Value && valueLandLord_State.ToString() != "")
                            objBranch.LandLord_State = dtBranch.Rows[0]["LandLord_State"].ToString();
                        else
                            objBranch.LandLord_State = string.Empty;

                        object valueLandLord_Pincode = Convert.ToString(dtBranch.Rows[0]["LandLord_Pincode"]);
                        if (dtBranch.Columns.Contains("LandLord_Pincode") && valueLandLord_Pincode != DBNull.Value && valueLandLord_Pincode.ToString() != "")
                            vMBranchMaster.SelectedLandlordPincode = Convert.ToInt32(dtBranch.Rows[0]["LandLord_Pincode"].ToString());
                        else
                            vMBranchMaster.SelectedLandlordPincode = 0;

                        object valueAccount_Type = Convert.ToString(dtBranch.Rows[0]["Account_Type"]);
                        if (dtBranch.Columns.Contains("Account_Type") && valueAccount_Type != DBNull.Value && valueAccount_Type.ToString() != "")
                            vMBranchMaster.SelectedAccountType = dtBranch.Rows[0]["Account_Type"].ToString();
                        else
                            vMBranchMaster.SelectedAccountType = string.Empty;

                        object valueAccount_Number = Convert.ToString(dtBranch.Rows[0]["Account_Number"]);
                        if (dtBranch.Columns.Contains("Account_Number") && valueAccount_Number != DBNull.Value && valueAccount_Number.ToString() != "")
                            objBranch.Account_Number = dtBranch.Rows[0]["Account_Number"].ToString();
                        else
                            objBranch.Account_Number = string.Empty;

                        object valueIFSC_Num = Convert.ToString(dtBranch.Rows[0]["IFSC_Num"]);
                        if (dtBranch.Columns.Contains("IFSC_Num") && valueIFSC_Num != DBNull.Value && valueIFSC_Num.ToString() != "")
                            objBranch.IFSC_Num = dtBranch.Rows[0]["IFSC_Num"].ToString();
                        else
                            objBranch.IFSC_Num = string.Empty;

                        object valuePAN_Num = Convert.ToString(dtBranch.Rows[0]["PAN_Num"]);
                        if (dtBranch.Columns.Contains("PAN_Num") && valuePAN_Num != DBNull.Value && valuePAN_Num.ToString() != "")
                            objBranch.PAN_Num = dtBranch.Rows[0]["PAN_Num"].ToString();
                        else
                            objBranch.PAN_Num = string.Empty;

                        object valueGSTIN_Num = Convert.ToString(dtBranch.Rows[0]["GSTIN_Num"]);
                        if (dtBranch.Columns.Contains("GSTIN_Num") && valueGSTIN_Num != DBNull.Value && valueGSTIN_Num.ToString() != "")
                            objBranch.GSTIN_Num = dtBranch.Rows[0]["GSTIN_Num"].ToString();
                        else
                            objBranch.GSTIN_Num = string.Empty;

                        //vMBranchMaster.pincodeList = GetPincode();
                        vMBranchMaster.ival_CitiesList = GetCity();
                        vMBranchMaster.ival_LocalityList = GetLocality();
                        vMBranchMaster.ival_DistrictsList = GetDistricts();
                        vMBranchMaster.ival_StatesList = GetState();
                        vMBranchMaster.AccountTypeList = GetAccountType();
                        vMBranchMaster.branchHeadsList = GetBranchHeadList();

                        IEnumerable<ival_States> ival_StateList = GetState();
                        var lessorState1 = ival_StateList.AsEnumerable().Where(row => row.State_Name == dtBranch.Rows[0]["Lessor_State"].ToString()).Select(row => row.State_ID);
                        var landlordState1 = ival_StateList.AsEnumerable().Where(row => row.State_Name == dtBranch.Rows[0]["LandLord_State"].ToString()).Select(row => row.State_ID);
                        vMBranchMaster.SelectedLessorStateID = Convert.ToInt32(lessorState1.FirstOrDefault());
                        vMBranchMaster.SelectedLandlordStateID = Convert.ToInt32(landlordState1.FirstOrDefault());

                        IEnumerable<ival_Districts> ival_DistrictList = GetDistricts();
                        var lessorDist1 = ival_DistrictList.AsEnumerable().Where(row => row.District_Name == dtBranch.Rows[0]["Lessor_District"].ToString()).Select(row => row.District_ID);
                        var landlordDist1 = ival_DistrictList.AsEnumerable().Where(row => row.District_Name == dtBranch.Rows[0]["LandLord_District"].ToString()).Select(row => row.District_ID);
                        vMBranchMaster.SelectedLessorDistID = Convert.ToInt32(lessorDist1.FirstOrDefault());
                        vMBranchMaster.SelectedLandlordDistID = Convert.ToInt32(landlordDist1.FirstOrDefault());

                        IEnumerable<ival_Locality> ival_LocalityLists = GetLocality();
                        var lessorLoc1 = ival_LocalityLists.AsEnumerable().Where(row => row.Loc_Name == dtBranch.Rows[0]["Lessor_City"].ToString()).Select(row => row.Loc_Id);
                        var landlordLoc1 = ival_LocalityLists.AsEnumerable().Where(row => row.Loc_Name == dtBranch.Rows[0]["LandLord_City"].ToString()).Select(row => row.Loc_Id);
                        vMBranchMaster.SelectedLessorLocID = Convert.ToInt32(lessorLoc1.FirstOrDefault());
                        vMBranchMaster.SelectedLandlordLocID = Convert.ToInt32(landlordLoc1.FirstOrDefault());

                        vMBranchMaster.SelectedLessorStateID = Convert.ToInt32(dtBranch.Rows[0]["Lessor_State"]);
                        vMBranchMaster.SelectedLessorDistID = Convert.ToInt32(dtBranch.Rows[0]["Lessor_District"]);
                        vMBranchMaster.SelectedLessorLocID = Convert.ToInt32(dtBranch.Rows[0]["Lessor_Locality"]);
                        vMBranchMaster.SelectedLessorCityID = Convert.ToInt32(dtBranch.Rows[0]["Lessor_City"]);
                        vMBranchMaster.SelectedLessorPincode = Convert.ToInt32(dtBranch.Rows[0]["Lessor_City"]);
                        vMBranchMaster.SelectedLandlordStateID = Convert.ToInt32(dtBranch.Rows[0]["LandLord_State"]);
                        vMBranchMaster.SelectedLandlordDistID = Convert.ToInt32(dtBranch.Rows[0]["LandLord_District"]);
                        vMBranchMaster.SelectedLandlordLocID = Convert.ToInt32(dtBranch.Rows[0]["LandLord_Locality"]);
                        vMBranchMaster.SelectedLandlordCityID = Convert.ToInt32(dtBranch.Rows[0]["LandLord_City"]);
                        vMBranchMaster.SelectedLandlordPincode = Convert.ToInt32(dtBranch.Rows[0]["LandLord_City"]);
                       // vMBranchMaster.Landlord_Branch = dtBranch.Rows[0]["LandLord_Branch"].ToString();

                    }
                    
                   // vMBranchMaster.pincodeList = GetPincode();
                    vMBranchMaster.ival_CitiesList = GetCity();
                    vMBranchMaster.ival_LocalityList = GetLocality();
                    vMBranchMaster.ival_DistrictsList = GetDistricts();
                    vMBranchMaster.ival_StatesList = GetState();
                    vMBranchMaster.AccountTypeList = GetAccountType();
                    vMBranchMaster.branchHeadsList = GetBranchHeadList();

                    IEnumerable<ival_States> ival_StatesList = GetState();
                    var branchState = ival_StatesList.AsEnumerable().Where(row => row.State_Name == dtBranch.Rows[0]["Branch_State"].ToString()).Select(row => row.State_ID);
                    vMBranchMaster.SelectedBranchStateID = Convert.ToInt32(branchState.FirstOrDefault());

                    IEnumerable<ival_Districts> ival_DistrictsList = GetDistricts();
                    var branchDist = ival_DistrictsList.AsEnumerable().Where(row => row.District_Name == dtBranch.Rows[0]["Branch_District"].ToString()).Select(row => row.District_ID);
                    vMBranchMaster.SelectedBranchDistID = Convert.ToInt32(branchDist.FirstOrDefault());

                    IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                    var branchLoc = ival_LocalityList.AsEnumerable().Where(row => row.Loc_Name == dtBranch.Rows[0]["Branch_City"].ToString()).Select(row => row.Loc_Id);
                    vMBranchMaster.SelectedBranchLocID = Convert.ToInt32(branchLoc.FirstOrDefault());

                    IEnumerable<ival_Cities> ival_CitiesList = GetCity();
                    var branchLoc1 = ival_CitiesList.AsEnumerable().Where(row => row.City_Name == dtBranch.Rows[0]["Branch_Loc"].ToString()).Select(row => row.Loc_Id);
                    vMBranchMaster.SelectedBranchLoc = Convert.ToInt32(branchLoc1.FirstOrDefault());
                    /*IEnumerable<ival_Locality> pincodeList = GetPincode();
                    var branchPincode = pincodeList.AsEnumerable().Where(row => row.Pin_Code == dtBranch.Rows[0]["Branch_Pincode"].ToString()).Select(row => row.Loc_Id);
                    var lessorPincode = pincodeList.AsEnumerable().Where(row => row.Pin_Code == dtBranch.Rows[0]["Lessor_Pincode"].ToString()).Select(row => row.Loc_Id);
                    var landlordPincode = pincodeList.AsEnumerable().Where(row => row.Pin_Code == dtBranch.Rows[0]["LandLord_Pincode"].ToString()).Select(row => row.Loc_Id);
                    vMBranchMaster.SelectedBranchPincode = Convert.ToString(branchPincode.FirstOrDefault());
                    vMBranchMaster.SelectedLessorPincode = Convert.ToInt32(lessorPincode.FirstOrDefault());
                    vMBranchMaster.SelectedLandlordPincode = Convert.ToInt32(landlordPincode.FirstOrDefault());*/

                    vMBranchMaster.SelectedBranchDistID = Convert.ToInt32(dtBranch.Rows[0]["Branch_District"]);
                    vMBranchMaster.SelectedBranchLocID = Convert.ToInt32(dtBranch.Rows[0]["Branch_City"]);
                    vMBranchMaster.SelectedBranchPincode = Convert.ToInt32(dtBranch.Rows[0]["Branch_City"]);
                    vMBranchMaster.SelectedBranchLoc = Convert.ToInt32(dtBranch.Rows[0]["Branch_Loc"]);
                    /* vMBranchMaster.SelectedLessorStateID = Convert.ToInt32(dtBranch.Rows[0]["Lessor_State"]);
                     vMBranchMaster.SelectedLessorDistID = Convert.ToInt32(dtBranch.Rows[0]["Lessor_District"]);
                     vMBranchMaster.SelectedLessorLocID = Convert.ToInt32(dtBranch.Rows[0]["Lessor_City"]);
                     vMBranchMaster.SelectedLessorPincode = Convert.ToInt32(dtBranch.Rows[0]["Lessor_Pincode"]);
                     vMBranchMaster.SelectedLandlordStateID = Convert.ToInt32(dtBranch.Rows[0]["LandLord_State"]);
                     vMBranchMaster.SelectedLandlordDistID = Convert.ToInt32(dtBranch.Rows[0]["LandLord_District"]);
                     vMBranchMaster.SelectedLandlordLocID = Convert.ToInt32(dtBranch.Rows[0]["LandLord_City"]);
                     vMBranchMaster.SelectedLandlordPincode = Convert.ToInt32(dtBranch.Rows[0]["LandLord_Pincode"]);*/
                    vMBranchMaster.ival_BranchMaster = objBranch;
                }
            
                else
                {
                    return Json( "Error in Database !", JsonRequestBehavior.AllowGet );
                }
            }
            catch(Exception ex)
            {
                return Json(  ex.Message, JsonRequestBehavior.AllowGet );
                //return RedirectToAction("Index", "Branch");
            }
            return View(vMBranchMaster);
        }

        [HttpPost]
        public ActionResult UpdateBranch(String str)
        {
            string resultStr = string.Empty;
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

            try
            {
                IEnumerable<ival_States> ival_StatesList = GetState();
                var branchState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMBranchMaster.SelectedBranchStateID).Select(row => row.State_Name.ToString());

                IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                var branchPincode = ival_LocalityList.AsEnumerable().Where(row => row.Loc_Id == vMBranchMaster.SelectedBranchPincode).Select(row => row.Loc_Name.ToString());

                for (int i = 0; i < list1.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();


                    /*if (Convert.ToBoolean(list1.Rows[i]["Is_Leased"]))
                    {*/
                    /* var lessorState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMBranchMaster.SelectedLessorStateID).Select(row => row.State_Name.ToString());
                     var landlordState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMBranchMaster.SelectedLandlordStateID).Select(row => row.State_Name.ToString());
                     vMBranchMaster.Lessor_State = lessorState.FirstOrDefault();
                     vMBranchMaster.LandLord_State = landlordState.FirstOrDefault();

                     var lessorPincode = pincodeList.AsEnumerable().Where(row => row.Loc_Id == vMBranchMaster.SelectedLessorPincode).Select(row => row.Loc_Name.ToString());
                     var landlordPincode = pincodeList.AsEnumerable().Where(row => row.Loc_Id == vMBranchMaster.SelectedLandlordPincode).Select(row => row.Loc_Name.ToString());*/

                    //if (Convert.ToString(list1.Rows[i]["Lessor_Fname"]) != "")
                    if (Convert.ToBoolean(list1.Rows[i]["Is_Leased"]))
                    {

                        hInputPara.Add("@Is_Leased", 1);
                        hInputPara.Add("@Branch_ID", Convert.ToInt32(list1.Rows[i]["txt_BranchID"]));
                        hInputPara.Add("@Lessor_First_Name", Convert.ToString(list1.Rows[i]["Lessor_Fname"]));
                        hInputPara.Add("@Lessor_Middle_Name", Convert.ToString(list1.Rows[i]["Lessor_Mname"]));
                        hInputPara.Add("@Lessor_Last_Name", Convert.ToString(list1.Rows[i]["Lessor_Lname"]));
                        hInputPara.Add("@Lessor_Address1", Convert.ToString(list1.Rows[i]["Lessor_Address1"]));
                        hInputPara.Add("@Lessor_Address2", Convert.ToString(list1.Rows[i]["Lessor_Address2"]));
                        hInputPara.Add("@Lessor_City", Convert.ToString(list1.Rows[i]["Lessor_City1"]));
                        hInputPara.Add("@Lessor_Locality", Convert.ToString(list1.Rows[i]["Lessor_City"]));
                        hInputPara.Add("@Lessor_District", Convert.ToString(list1.Rows[i]["Lessor_Dist"]));
                        hInputPara.Add("@Lessor_State", Convert.ToString(list1.Rows[i]["Lessor_State"]));
                        hInputPara.Add("@Lessor_Pincode", Convert.ToInt32(list1.Rows[i]["Lessor_Pin"]));
                        hInputPara.Add("@Lessor_Contact_Num", Convert.ToString(list1.Rows[i]["Lessor_Contact"]));
                        hInputPara.Add("@Lessor_Email_ID", Convert.ToString(list1.Rows[i]["Lessor_Email"]));

                        hInputPara.Add("@Lease_Start_Date", CDateTime(list1.Rows[i]["Lease_Start_Date"].ToString()));
                        //hInputPara.Add("@Lease_Start_Date", Convert.ToString(list1.Rows[i]["Lease_Start_Date"]));
                        hInputPara.Add("@Lease_End_Date", CDateTime(list1.Rows[i]["Lease_End_Date"].ToString()));
                        //hInputPara.Add("@Lease_Start_Date", CDateTime(list1.Rows[i]["Lease_Start_Date"].ToString()));
                        //hInputPara.Add("@Lease_End_Date", Convert.ToString(list1.Rows[i]["Lease_End_Date"]));
                        //hInputPara.Add("@Lease_End_Date", CDateTime(list1.Rows[i]["Lease_End_Date"].ToString()));
                        hInputPara.Add("@Monthly_Rent", Convert.ToString(list1.Rows[i]["Monthly_Rent"]));
                        hInputPara.Add("@Maintaince_Charges", Convert.ToString(list1.Rows[i]["Maintaince_Charges"]));
                        hInputPara.Add("@Other_Charges", Convert.ToString(list1.Rows[i]["Other_Charges"]));

                        hInputPara.Add("@LandLord_Bank", Convert.ToString(list1.Rows[i]["LL_Bank"]));
                        hInputPara.Add("@LandLord_Branch", Convert.ToString(list1.Rows[i]["LL_Branch"]));
                        hInputPara.Add("@LandLord_Address1", Convert.ToString(list1.Rows[i]["LL_Address1"]));
                        hInputPara.Add("@LandLord_Address2", Convert.ToString(list1.Rows[i]["LL_Address2"]));
                        hInputPara.Add("@LandLord_City", Convert.ToString(list1.Rows[i]["LL_City1"]));
                        hInputPara.Add("@LandLord_Locality", Convert.ToString(list1.Rows[i]["LL_City"]));
                        hInputPara.Add("@LandLord_District", Convert.ToString(list1.Rows[i]["LL_Dist"]));
                        hInputPara.Add("@LandLord_State", Convert.ToString(list1.Rows[i]["LL_State"]));
                        hInputPara.Add("@LandLord_Pincode", Convert.ToInt32(list1.Rows[i]["LL_Pin"]));
                        hInputPara.Add("@Account_Type", Convert.ToString(list1.Rows[i]["Acc_Type"]));
                        hInputPara.Add("@Account_Number", Convert.ToString(list1.Rows[i]["Acc_No"]));
                        hInputPara.Add("@IFSC_Num", Convert.ToString(list1.Rows[i]["IFSC_Num"]));
                        hInputPara.Add("@PAN_Num", Convert.ToString(list1.Rows[i]["PAN_Num"]));
                        hInputPara.Add("@GSTIN_Num", Convert.ToString(list1.Rows[i]["GSTIN_Num"]));

                        hInputPara.Add("@Branch_Code", Convert.ToString(list1.Rows[i]["Br_Code"]));
                        hInputPara.Add("@Branch_Name", Convert.ToString(list1.Rows[i]["Br_Name"]));
                        hInputPara.Add("@Branch_Address1", Convert.ToString(list1.Rows[i]["Br_Addr1"]));
                        hInputPara.Add("@Branch_Address2", Convert.ToString(list1.Rows[i]["Br_Addr2"]));
                        hInputPara.Add("@Branch_City", Convert.ToString(list1.Rows[i]["Br_City"]));
                        hInputPara.Add("@Branch_District", Convert.ToString(list1.Rows[i]["Br_Dist"]));
                        hInputPara.Add("@Branch_State", Convert.ToString(list1.Rows[i]["Br_State"]));
                        hInputPara.Add("@Branch_Pincode", Convert.ToInt32(list1.Rows[i]["Br_Pin"]));

                        hInputPara.Add("@Branch_Loc", Convert.ToString(list1.Rows[i]["Branch_Loc"]));

                    }
                    else
                    {
                        hInputPara.Add("@Branch_ID", Convert.ToInt32(list1.Rows[i]["txt_BranchID"]));
                        hInputPara.Add("@Branch_Code", Convert.ToString(list1.Rows[i]["Br_Code"]));
                        hInputPara.Add("@Branch_Name", Convert.ToString(list1.Rows[i]["Br_Name"]));
                        hInputPara.Add("@Branch_Address1", Convert.ToString(list1.Rows[i]["Br_Addr1"]));
                        hInputPara.Add("@Branch_Address2", Convert.ToString(list1.Rows[i]["Br_Addr2"]));
                        hInputPara.Add("@Branch_City", Convert.ToString(list1.Rows[i]["Br_City"]));
                        hInputPara.Add("@Branch_District", Convert.ToString(list1.Rows[i]["Br_Dist"]));
                        hInputPara.Add("@Branch_State", Convert.ToString(list1.Rows[i]["Br_State"]));
                        hInputPara.Add("@Branch_Pincode", Convert.ToInt32(list1.Rows[i]["Br_Pin"]));

                        hInputPara.Add("@Branch_Loc", Convert.ToString(list1.Rows[i]["Branch_Loc"]));
                        hInputPara.Add("@Is_Leased", 0);
                    }
                    hInputPara.Add("@BranchHeadID", Convert.ToInt32(list1.Rows[i]["Br_Head"]));
                    hInputPara.Add("@Modified_By", string.Empty);

                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateBranchDetailsByID", hInputPara);

                    if (Convert.ToInt32(result) == 1)
                    {
                        return RedirectToAction("Index", "Branch");
                    }
                    else
                    {
                        return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                    }
                }
                
            }
            catch (Exception e)
            {
                //e.InnerException.ToString();
                return Json(new { e.Message, JsonRequestBehavior.AllowGet });
            }

            return Json(new { success = true, Message = "Updated SuccessFull" }, JsonRequestBehavior.AllowGet);
        }

        /*[HttpPost]
        public ActionResult EditBranch(VMBranchMaster vMBranchMaster )
        {
            try
            {
                IEnumerable<ival_States> ival_StatesList = GetState();
                var branchState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMBranchMaster.SelectedBranchStateID).Select(row => row.State_Name.ToString());
                var lessortState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMBranchMaster.SelectedLessorStateID).Select(row => row.State_Name.ToString());
                var bankState = ival_StatesList.AsEnumerable().Where(row => row.State_ID == vMBranchMaster.SelectedLandlordStateID).Select(row => row.State_Name.ToString());

                vMBranchMaster.ival_BranchMaster.Branch_State = branchState.FirstOrDefault();
                vMBranchMaster.ival_BranchMaster.Lessor_State = lessortState.FirstOrDefault();
                vMBranchMaster.ival_BranchMaster.LandLord_State = bankState.FirstOrDefault();


                IEnumerable<ival_Locality> pincodeList = GetPincode();
                var branchPincode = pincodeList.AsEnumerable().Where(row => row.Loc_Id == vMBranchMaster.SelectedBranchPincode).Select(row => row.Pin_Code.ToString());
                var lessorPincode = pincodeList.AsEnumerable().Where(row => row.Loc_Id == vMBranchMaster.SelectedLessorPincode).Select(row => row.Pin_Code.ToString());
                var landlordPincode = pincodeList.AsEnumerable().Where(row => row.Loc_Id == vMBranchMaster.SelectedLandlordPincode).Select(row => row.Pin_Code.ToString());


                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                hInputPara = new Hashtable();
               
                if (Convert.ToBoolean( vMBranchMaster.SelectedIsSelected))
                {
                    hInputPara.Add("@Is_Leased", 1);
                    hInputPara.Add("@Branch_ID", vMBranchMaster.ival_BranchMaster.Branch_ID);
                    hInputPara.Add("@Lessor_First_Name", Convert.ToString(vMBranchMaster.ival_BranchMaster.Lessor_First_Name));
                    hInputPara.Add("@Lessor_Middle_Name", Convert.ToString(vMBranchMaster.ival_BranchMaster.Lessor_Middle_Name));
                    hInputPara.Add("@Lessor_Last_Name", Convert.ToString(vMBranchMaster.ival_BranchMaster.Lessor_Last_Name));
                    hInputPara.Add("@Lessor_Address1", Convert.ToString(vMBranchMaster.ival_BranchMaster.Lessor_Address1));
                    hInputPara.Add("@Lessor_Address2", Convert.ToString(vMBranchMaster.ival_BranchMaster.Lessor_Address2));
                    hInputPara.Add("@Lessor_City", Convert.ToString(vMBranchMaster.ival_BranchMaster.Lessor_City));
                    hInputPara.Add("@Lessor_District", Convert.ToString(vMBranchMaster.ival_BranchMaster.Lessor_District));
                    hInputPara.Add("@Lessor_State", Convert.ToString(vMBranchMaster.ival_BranchMaster.Lessor_State));
                    hInputPara.Add("@Lessor_Pincode", Convert.ToInt32(lessorPincode.FirstOrDefault()));
                    hInputPara.Add("@Lessor_Contact_Num", Convert.ToString(vMBranchMaster.ival_BranchMaster.Lessor_Contact_Num));
                    hInputPara.Add("@Lessor_Email_ID", Convert.ToString(vMBranchMaster.ival_BranchMaster.Lessor_Email_ID));
                    hInputPara.Add("@Lease_Start_Date", Convert.ToDateTime(vMBranchMaster.ival_BranchMaster.Lease_Start_Date.ToString()));
                    hInputPara.Add("@Lease_End_Date", Convert.ToDateTime(vMBranchMaster.ival_BranchMaster.Lease_End_Date.ToString()));
                    hInputPara.Add("@Monthly_Rent", Convert.ToString(vMBranchMaster.ival_BranchMaster.Monthly_Rent));
                    hInputPara.Add("@Maintaince_Charges", Convert.ToString(vMBranchMaster.ival_BranchMaster.Maintaince_Charges));
                    hInputPara.Add("@Other_Charges", Convert.ToString(vMBranchMaster.ival_BranchMaster.Other_Charges));
                    hInputPara.Add("@LandLord_Bank", Convert.ToString(vMBranchMaster.ival_BranchMaster.LandLord_Bank));
                    hInputPara.Add("@LandLord_Branch", Convert.ToString(vMBranchMaster.ival_BranchMaster.LandLord_Branch));
                    hInputPara.Add("@LandLord_Address1", Convert.ToString(vMBranchMaster.ival_BranchMaster.LandLord_Address1));
                    hInputPara.Add("@LandLord_Address2", Convert.ToString(vMBranchMaster.ival_BranchMaster.LandLord_Address2));
                    hInputPara.Add("@LandLord_City", Convert.ToString(vMBranchMaster.ival_BranchMaster.LandLord_City));
                    hInputPara.Add("@LandLord_District", Convert.ToString(vMBranchMaster.ival_BranchMaster.LandLord_District));
                    hInputPara.Add("@LandLord_State", Convert.ToString(vMBranchMaster.ival_BranchMaster.LandLord_State));
                    hInputPara.Add("@LandLord_Pincode", Convert.ToInt32(landlordPincode.FirstOrDefault()));
                    hInputPara.Add("@Account_Type", Convert.ToString(vMBranchMaster.SelectedAccountType));
                    hInputPara.Add("@Account_Number", Convert.ToString(vMBranchMaster.ival_BranchMaster.Account_Number));
                    hInputPara.Add("@IFSC_Num", Convert.ToString(vMBranchMaster.ival_BranchMaster.IFSC_Num));
                    hInputPara.Add("@PAN_Num", Convert.ToString(vMBranchMaster.ival_BranchMaster.PAN_Num));
                    hInputPara.Add("@GSTIN_Num", Convert.ToString(vMBranchMaster.ival_BranchMaster.GSTIN_Num));
                    hInputPara.Add("@Branch_Code", vMBranchMaster.ival_BranchMaster.Branch_Code);
                    hInputPara.Add("@Branch_Name", Convert.ToString(vMBranchMaster.ival_BranchMaster.Branch_Name));
                    hInputPara.Add("@Branch_Address1", Convert.ToString(vMBranchMaster.ival_BranchMaster.Branch_Address1));
                    hInputPara.Add("@Branch_Address2", Convert.ToString(vMBranchMaster.ival_BranchMaster.Branch_Address2));
                    hInputPara.Add("@Branch_City", Convert.ToString(vMBranchMaster.ival_BranchMaster.Branch_City));
                    hInputPara.Add("@Branch_District", Convert.ToString(vMBranchMaster.ival_BranchMaster.Branch_District));
                    hInputPara.Add("@Branch_State", Convert.ToString(vMBranchMaster.ival_BranchMaster.Branch_State));
                    hInputPara.Add("@Branch_Pincode", Convert.ToInt32(branchPincode.FirstOrDefault()));
                }
                else
                {
                    hInputPara.Add("@Branch_ID", vMBranchMaster.ival_BranchMaster.Branch_ID);
                    hInputPara.Add("@Branch_Code", vMBranchMaster.ival_BranchMaster.Branch_Code);
                    hInputPara.Add("@Branch_Name", Convert.ToString(vMBranchMaster.ival_BranchMaster.Branch_Name));
                    hInputPara.Add("@Branch_Address1", Convert.ToString(vMBranchMaster.ival_BranchMaster.Branch_Address1));
                    hInputPara.Add("@Branch_Address2", Convert.ToString(vMBranchMaster.ival_BranchMaster.Branch_Address2));
                    hInputPara.Add("@Branch_City", Convert.ToString(vMBranchMaster.ival_BranchMaster.Branch_City));
                    hInputPara.Add("@Branch_District", Convert.ToString(vMBranchMaster.ival_BranchMaster.Branch_District));
                    hInputPara.Add("@Branch_State", Convert.ToString(vMBranchMaster.ival_BranchMaster.Branch_State));
                    hInputPara.Add("@Branch_Pincode", Convert.ToInt32(branchPincode.FirstOrDefault()));

                }
                hInputPara.Add("@BranchHeadID", vMBranchMaster.SelectedBranchHead);
                hInputPara.Add("@Modified_By", string.Empty) ;
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateBranchDetailsByID", hInputPara);

                if (Convert.ToInt32(result) == 1)
                {
                    return RedirectToAction("Index", "Branch");
                }
                else
                {
                    return Json( "Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                //e.InnerException.ToString();
                return Json( e.Message, JsonRequestBehavior.AllowGet );
            }
            
        }*/

        public ActionResult ViewBranchDetails(int ID)
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //return View();
            }
            VMBranchMaster vMBranchMaster = new VMBranchMaster();
            ival_BranchMaster objBranch = new ival_BranchMaster();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            try
            {
                hInputPara.Add("@Branch_ID", ID);
                DataTable dtBranch = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBranchByBranchId", hInputPara);
                if (dtBranch != null && dtBranch.Rows.Count > 0)
                {

                    object valueBranch_ID = Convert.ToInt32(dtBranch.Rows[0]["Branch_ID"]);
                    if (dtBranch.Columns.Contains("Branch_ID") && valueBranch_ID != DBNull.Value && valueBranch_ID.ToString() != "")
                        objBranch.Branch_ID = Convert.ToInt32(dtBranch.Rows[0]["Branch_ID"]);
                    else
                        objBranch.Branch_ID = 0;


                    object valueBranch_Code = dtBranch.Rows[0]["Branch_Code"];
                    if (dtBranch.Columns.Contains("Branch_Code") && valueBranch_Code != null && valueBranch_ID.ToString() != "")
                        objBranch.Branch_Code = dtBranch.Rows[0]["Branch_Code"].ToString();
                    else
                        objBranch.Branch_Code = string.Empty;

                    object valueBranchHeadID = dtBranch.Rows[0]["BranchHeadID"];
                    if (dtBranch.Columns.Contains("BranchHeadID") && valueBranchHeadID != null && valueBranchHeadID.ToString() != "")
                        vMBranchMaster.SelectedBranchHead = Convert.ToInt32(dtBranch.Rows[0]["BranchHeadID"].ToString());
                    else
                        vMBranchMaster.SelectedBranchHead = 0;

                    object valueBranch_Name = dtBranch.Rows[0]["Branch_Name"];
                    if (dtBranch.Columns.Contains("Branch_Name") && valueBranch_Name != null && valueBranch_Name.ToString() != "")
                        objBranch.Branch_Name = dtBranch.Rows[0]["Branch_Name"].ToString();
                    else
                        objBranch.Branch_Name = string.Empty;

                    object valueBranch_Address1 = dtBranch.Rows[0]["Branch_Address1"];
                    if (dtBranch.Columns.Contains("Branch_Address1") && valueBranch_Address1 != null && valueBranch_Address1.ToString() != "")
                        objBranch.Branch_Address1 = dtBranch.Rows[0]["Branch_Address1"].ToString();
                    else
                        objBranch.Branch_Address1 = string.Empty;

                    object valueBranch_Address2 = dtBranch.Rows[0]["Branch_Address2"];
                    if (dtBranch.Columns.Contains("Branch_Address2") && valueBranch_Address2 != null && valueBranch_Address2.ToString() != "")
                        objBranch.Branch_Address2 = dtBranch.Rows[0]["Branch_Address2"].ToString();
                    else
                        objBranch.Branch_Address2 = string.Empty;

                    object valueBranch_City = dtBranch.Rows[0]["Branch_City"];
                    if (dtBranch.Columns.Contains("Branch_City") && valueBranch_City != null && valueBranch_City.ToString() != "")
                        objBranch.Branch_City = dtBranch.Rows[0]["Branch_City"].ToString();
                    else
                        objBranch.Branch_City = string.Empty;

                    object valueBranch_District = dtBranch.Rows[0]["Branch_District"];
                    if (dtBranch.Columns.Contains("Branch_District") && valueBranch_District != null && valueBranch_District.ToString() != "")
                        objBranch.Branch_District = dtBranch.Rows[0]["Branch_District"].ToString();
                    else
                        objBranch.Branch_District = string.Empty;

                    object valueBranch_State = dtBranch.Rows[0]["Branch_State"];
                    if (dtBranch.Columns.Contains("Branch_State") && valueBranch_State != null && valueBranch_State.ToString() != "")
                        objBranch.Branch_State = dtBranch.Rows[0]["Branch_State"].ToString();
                    else
                        objBranch.Branch_State = string.Empty;

                    object valueBranch_Pincode = dtBranch.Rows[0]["Branch_Pincode"];
                    if (dtBranch.Columns.Contains("Branch_Pincode") && valueBranch_Pincode != null && valueBranch_Pincode.ToString() != "")
                        vMBranchMaster.SelectedBranchPincode = Convert.ToInt32(dtBranch.Rows[0]["Branch_Pincode"].ToString());
                    else
                        vMBranchMaster.SelectedBranchPincode = 0;

                    object valueBranch_Loc = dtBranch.Rows[0]["Branch_Loc"];
                    if (dtBranch.Columns.Contains("Branch_Loc") && valueBranch_Loc != null && valueBranch_Loc.ToString() != "")
                        vMBranchMaster.Branch_Loc = dtBranch.Rows[0]["Branch_Loc"].ToString();
                    else
                        vMBranchMaster.Branch_Loc = string.Empty;

                    object valueIs_Leased = Convert.ToString(dtBranch.Rows[0]["Is_Leased"]);
                    if (valueIs_Leased.ToString() != "")
                    {
                        if (Convert.ToBoolean(dtBranch.Rows[0]["Is_Leased"].ToString()))
                        {
                            objBranch.Is_Leased = true;
                        }
                    }
                    else
                    {
                        objBranch.Is_Leased = false;
                    }
                    if (Convert.ToBoolean(objBranch.Is_Leased) && valueIs_Leased != DBNull.Value)
                    {
                        object valueLessor_First_Name = dtBranch.Rows[0]["Lessor_First_Name"];
                        if (dtBranch.Columns.Contains("Lessor_First_Name") && valueLessor_First_Name != null && valueLessor_First_Name.ToString() != "")
                            objBranch.Lessor_First_Name = dtBranch.Rows[0]["Lessor_First_Name"].ToString();
                        else
                            objBranch.Lessor_First_Name = string.Empty;

                        object valueLessor_Middle_Name = dtBranch.Rows[0]["Lessor_Middle_Name"];
                        if (dtBranch.Columns.Contains("Lessor_Middle_Name") && valueLessor_Middle_Name != null && valueLessor_Middle_Name.ToString() != "")
                            objBranch.Lessor_Middle_Name = dtBranch.Rows[0]["Lessor_Middle_Name"].ToString();
                        else
                            objBranch.Lessor_Middle_Name = string.Empty;

                        object valueLessor_Last_Name = dtBranch.Rows[0]["Lessor_Last_Name"];
                        if (dtBranch.Columns.Contains("Lessor_Last_Name") && valueLessor_Last_Name != null && valueLessor_Last_Name.ToString() != "")
                            objBranch.Lessor_Last_Name = dtBranch.Rows[0]["Lessor_Last_Name"].ToString();
                        else
                            objBranch.Lessor_Last_Name = string.Empty;

                        object valueLessor_Address1 = dtBranch.Rows[0]["Lessor_Address1"];
                        if (dtBranch.Columns.Contains("Lessor_Address1") && valueLessor_Address1 != null && valueLessor_Address1.ToString() != "")
                            objBranch.Lessor_Address1 = dtBranch.Rows[0]["Lessor_Address1"].ToString();
                        else
                            objBranch.Lessor_Address1 = string.Empty;

                        object valueLessor_Address2 = dtBranch.Rows[0]["Lessor_Address2"];
                        if (dtBranch.Columns.Contains("Lessor_Address2") && valueLessor_Address2 != null && valueLessor_Address2.ToString() != "")
                            objBranch.Lessor_Address2 = dtBranch.Rows[0]["Lessor_Address2"].ToString();
                        else
                            objBranch.Lessor_Address2 = string.Empty;

                        object valueLessor_City = dtBranch.Rows[0]["Lessor_City"];
                        if (dtBranch.Columns.Contains("Lessor_City") && valueLessor_City != null && valueLessor_City.ToString() != "")
                            objBranch.Lessor_City = dtBranch.Rows[0]["Lessor_City"].ToString();
                        else
                            objBranch.Lessor_City = string.Empty;

                        object valueLessor_Loc = dtBranch.Rows[0]["Lessor_Locality"];
                        if (dtBranch.Columns.Contains("Lessor_Locality") && valueLessor_Loc != null && valueLessor_Loc.ToString() != "")
                            objBranch.Lessor_Loc = dtBranch.Rows[0]["Lessor_Locality"].ToString();
                        else
                            objBranch.Lessor_Loc = string.Empty;

                        object valueLessor_District = dtBranch.Rows[0]["Lessor_District"];
                        if (dtBranch.Columns.Contains("Lessor_District") && valueLessor_District != null && valueLessor_District.ToString() != "")
                            objBranch.Lessor_District = dtBranch.Rows[0]["Lessor_District"].ToString();
                        else
                            objBranch.Lessor_District = string.Empty;

                        object valueLessor_State = Convert.ToString(dtBranch.Rows[0]["Lessor_State"]);
                        if (dtBranch.Columns.Contains("Lessor_State") && valueLessor_State != null && valueLessor_State.ToString() != "")
                            objBranch.Lessor_State = dtBranch.Rows[0]["Lessor_State"].ToString();
                        else
                            objBranch.Lessor_State = string.Empty;

                        object valueLessor_Pincode = Convert.ToInt32(dtBranch.Rows[0]["Lessor_Pincode"]);
                        if (dtBranch.Columns.Contains("Lessor_Pincode") && valueLessor_Pincode != null && valueLessor_Pincode.ToString() != "")
                            vMBranchMaster.SelectedLessorPincode = Convert.ToInt32(dtBranch.Rows[0]["Lessor_Pincode"].ToString());
                        else
                            vMBranchMaster.SelectedLessorPincode = 0;

                        object valueLessor_Contact_Num = dtBranch.Rows[0]["Lessor_Contact_Num"];
                        if (dtBranch.Columns.Contains("Lessor_Contact_Num") && valueLessor_Contact_Num != null)
                            objBranch.Lessor_Contact_Num = dtBranch.Rows[0]["Lessor_Contact_Num"].ToString();
                        else
                            objBranch.Lessor_Contact_Num = string.Empty;

                        object valueLessor_Email_ID = Convert.ToString(dtBranch.Rows[0]["Lessor_Email_ID"]);
                        if (dtBranch.Columns.Contains("Lessor_Email_ID") && valueLessor_Email_ID != null && valueLessor_Email_ID.ToString() != "")
                            objBranch.Lessor_Email_ID = dtBranch.Rows[0]["Lessor_Email_ID"].ToString();
                        else
                            objBranch.Lessor_Email_ID = string.Empty;

                        /*object valueLease_Start_Date = Convert.ToString(dtBranch.Rows[0]["Lease_Start_Date"]);
                        if (dtBranch.Columns.Contains("Lease_Start_Date") && valueLease_Start_Date != null && valueLease_Start_Date.ToString() != "")
                            objBranch.Lease_Start_Date = dtBranch.Rows[0]["Lease_Start_Date"].ToString();
                        else
                            objBranch.Lease_Start_Date = string.Empty;

                        object valueLease_End_Date = Convert.ToString(dtBranch.Rows[0]["Lease_End_Date"]);
                        if (dtBranch.Columns.Contains("Lease_End_Date") && valueLease_End_Date != null && valueLease_End_Date.ToString() != "")
                            objBranch.Lease_End_Date = dtBranch.Rows[0]["Lease_End_Date"].ToString();
                        else
                            objBranch.Lease_End_Date = string.Empty;*/

                        object valueLease_Start_Date = dtBranch.Rows[0]["Lease_Start_Date"];
                        if (dtBranch.Columns.Contains("Lease_Start_Date") && valueLease_Start_Date != null && valueLease_Start_Date.ToString() != "")
                        {
                            DateTime Lease_Start_Date = Convert.ToDateTime(dtBranch.Rows[0]["Lease_Start_Date"]);
                            objBranch.Lease_Start_Date = Lease_Start_Date.Date;
                        }

                        else
                            objBranch.Lease_Start_Date = null;

                        object valueLease_End_Date = dtBranch.Rows[0]["Lease_End_Date"];
                        if (dtBranch.Columns.Contains("Lease_End_Date") && valueLease_End_Date != null && valueLease_End_Date.ToString() != "")
                        {
                            DateTime Lease_End_Date = Convert.ToDateTime(dtBranch.Rows[0]["Lease_End_Date"]);
                            objBranch.Lease_End_Date = Lease_End_Date.Date;
                        }
                        else
                            objBranch.Lease_End_Date = null;
                        //{
                        //    DateTime dt = Convert.ToDateTime(dtBranch.Rows[0]["Lease_Start_Date"]);
                        //    string date = dt.ToString("dd.MM.yyy");
                        //    objBranch.Lease_Start_Date = Convert.ToDateTime(date);

                        //}
                        //else
                        //{
                        //    objBranch.Lease_Start_Date = Convert.ToDateTime(string.Empty);
                        //}

                        //object valueLease_End_Date = Convert.ToDateTime(dtBranch.Rows[0]["Lease_End_Date"]);
                        //if (dtBranch.Columns.Contains("Lease_End_Date") && valueLease_End_Date != DBNull.Value)
                        //{
                        //    DateTime dt = Convert.ToDateTime(dtBranch.Rows[0]["Lease_End_Date"]);
                        //    string date= dt.ToString("dd.MM.yyy");
                        //    objBranch.Lease_End_Date = Convert.ToDateTime(date);
                        //}
                        //else
                        //    objBranch.Lease_End_Date = Convert.ToDateTime(string.Empty);

                        object valueMonthly_Rent = dtBranch.Rows[0]["Monthly_Rent"];
                        if (dtBranch.Columns.Contains("Monthly_Rent") && valueMonthly_Rent != null && valueMonthly_Rent.ToString() != "")
                            objBranch.Monthly_Rent = dtBranch.Rows[0]["Monthly_Rent"].ToString();
                        else
                            objBranch.Monthly_Rent = string.Empty;

                        object valueMaintaince_Charges = dtBranch.Rows[0]["Maintaince_Charges"];
                        if (dtBranch.Columns.Contains("Maintaince_Charges") && valueMaintaince_Charges != null && valueMaintaince_Charges.ToString() != "")
                            objBranch.Maintaince_Charges = dtBranch.Rows[0]["Maintaince_Charges"].ToString();
                        else
                            objBranch.Maintaince_Charges = string.Empty;

                        object valueOther_Charges = dtBranch.Rows[0]["Other_Charges"];
                        if (dtBranch.Columns.Contains("Other_Charges") && valueOther_Charges != null && valueOther_Charges.ToString() != "")
                            objBranch.Other_Charges = dtBranch.Rows[0]["Other_Charges"].ToString();
                        else
                            objBranch.Other_Charges = string.Empty;

                        object valueLandLord_Bank = Convert.ToString(dtBranch.Rows[0]["LandLord_Bank"]);
                        if (dtBranch.Columns.Contains("LandLord_Bank") && valueLandLord_Bank != DBNull.Value && valueLandLord_Bank.ToString() != "")
                            objBranch.LandLord_Bank = dtBranch.Rows[0]["LandLord_Bank"].ToString();
                        else
                            objBranch.LandLord_Bank = string.Empty;

                        object valueLandLord_Branch = Convert.ToString(dtBranch.Rows[0]["LandLord_Branch"]);
                        if (dtBranch.Columns.Contains("LandLord_Branch") && valueLandLord_Branch != DBNull.Value && valueLandLord_Branch.ToString() != "")
                            objBranch.LandLord_Branch = dtBranch.Rows[0]["LandLord_Branch"].ToString();
                        else
                            objBranch.LandLord_Branch = string.Empty;

                        object valueLandLord_Address1 = Convert.ToString(dtBranch.Rows[0]["LandLord_Address1"]);
                        if (dtBranch.Columns.Contains("LandLord_Address1") && valueLandLord_Address1 != DBNull.Value && valueLandLord_Address1.ToString() != "")
                            objBranch.LandLord_Address1 = dtBranch.Rows[0]["LandLord_Address1"].ToString();
                        else
                            objBranch.LandLord_Address1 = string.Empty;

                        object valueLandLord_Address2 = Convert.ToString(dtBranch.Rows[0]["LandLord_Address2"]);
                        if (dtBranch.Columns.Contains("LandLord_Address2") && valueLandLord_Address2 != DBNull.Value && valueLandLord_Address2.ToString() != "")
                            objBranch.LandLord_Address2 = dtBranch.Rows[0]["LandLord_Address2"].ToString();
                        else
                            objBranch.LandLord_Address2 = string.Empty;

                        object valueLandLord_City = Convert.ToString(dtBranch.Rows[0]["LandLord_City"]);
                        if (dtBranch.Columns.Contains("LandLord_City") && valueLandLord_City != DBNull.Value && valueLandLord_City.ToString() != "")
                            objBranch.LandLord_City = dtBranch.Rows[0]["LandLord_City"].ToString();
                        else
                            objBranch.LandLord_City = string.Empty;

                        object valueLandLord_Loc = Convert.ToString(dtBranch.Rows[0]["LandLord_Locality"]);
                        if (dtBranch.Columns.Contains("LandLord_Locality") && valueLandLord_Loc != DBNull.Value && valueLandLord_Loc.ToString() != "")
                            objBranch.LandLord_Loc = dtBranch.Rows[0]["LandLord_Locality"].ToString();
                        else
                            objBranch.LandLord_Loc = string.Empty;

                        object valueLandLord_District = Convert.ToString(dtBranch.Rows[0]["LandLord_District"]);
                        if (dtBranch.Columns.Contains("LandLord_District") && valueLandLord_District != DBNull.Value && valueLandLord_District.ToString() != "")
                            objBranch.LandLord_District = dtBranch.Rows[0]["LandLord_District"].ToString();
                        else
                            objBranch.LandLord_District = string.Empty;

                        object valueLandLord_State = Convert.ToString(dtBranch.Rows[0]["LandLord_State"]);
                        if (dtBranch.Columns.Contains("LandLord_State") && valueLandLord_State != DBNull.Value && valueLandLord_State.ToString() != "")
                            objBranch.LandLord_State = dtBranch.Rows[0]["LandLord_State"].ToString();
                        else
                            objBranch.LandLord_State = string.Empty;

                        object valueLandLord_Pincode = Convert.ToString(dtBranch.Rows[0]["LandLord_Pincode"]);
                        if (dtBranch.Columns.Contains("LandLord_Pincode") && valueLandLord_Pincode != DBNull.Value && valueLandLord_Pincode.ToString() != "")
                            vMBranchMaster.SelectedLandlordPincode = Convert.ToInt32(dtBranch.Rows[0]["LandLord_Pincode"].ToString());
                        else
                            vMBranchMaster.SelectedLandlordPincode = 0;

                        object valueAccount_Type = Convert.ToString(dtBranch.Rows[0]["Account_Type"]);
                        if (dtBranch.Columns.Contains("Account_Type") && valueAccount_Type != DBNull.Value && valueAccount_Type.ToString() != "")
                            vMBranchMaster.SelectedAccountType = dtBranch.Rows[0]["Account_Type"].ToString();
                        else
                            vMBranchMaster.SelectedAccountType = string.Empty;

                        object valueAccount_Number = Convert.ToString(dtBranch.Rows[0]["Account_Number"]);
                        if (dtBranch.Columns.Contains("Account_Number") && valueAccount_Number != DBNull.Value && valueAccount_Number.ToString() != "")
                            objBranch.Account_Number = dtBranch.Rows[0]["Account_Number"].ToString();
                        else
                            objBranch.Account_Number = string.Empty;

                        object valueIFSC_Num = Convert.ToString(dtBranch.Rows[0]["IFSC_Num"]);
                        if (dtBranch.Columns.Contains("IFSC_Num") && valueIFSC_Num != DBNull.Value && valueIFSC_Num.ToString() != "")
                            objBranch.IFSC_Num = dtBranch.Rows[0]["IFSC_Num"].ToString();
                        else
                            objBranch.IFSC_Num = string.Empty;

                        object valuePAN_Num = Convert.ToString(dtBranch.Rows[0]["PAN_Num"]);
                        if (dtBranch.Columns.Contains("PAN_Num") && valuePAN_Num != DBNull.Value && valuePAN_Num.ToString() != "")
                            objBranch.PAN_Num = dtBranch.Rows[0]["PAN_Num"].ToString();
                        else
                            objBranch.PAN_Num = string.Empty;

                        object valueGSTIN_Num = Convert.ToString(dtBranch.Rows[0]["GSTIN_Num"]);
                        if (dtBranch.Columns.Contains("GSTIN_Num") && valueGSTIN_Num != DBNull.Value && valueGSTIN_Num.ToString() != "")
                            objBranch.GSTIN_Num = dtBranch.Rows[0]["GSTIN_Num"].ToString();
                        else
                            objBranch.GSTIN_Num = string.Empty;


                        //vMBranchMaster.pincodeList = GetPincode();
                        vMBranchMaster.ival_CitiesList = GetCity();
                        vMBranchMaster.ival_LocalityList = GetLocality();
                        vMBranchMaster.ival_DistrictsList = GetDistricts();
                        vMBranchMaster.ival_StatesList = GetState();
                        vMBranchMaster.AccountTypeList = GetAccountType();
                        vMBranchMaster.branchHeadsList = GetBranchHeadList();

                        IEnumerable<ival_States> ival_StateList = GetState();
                        var lessorState1 = ival_StateList.AsEnumerable().Where(row => row.State_Name == dtBranch.Rows[0]["Lessor_State"].ToString()).Select(row => row.State_ID);
                        var landlordState1 = ival_StateList.AsEnumerable().Where(row => row.State_Name == dtBranch.Rows[0]["LandLord_State"].ToString()).Select(row => row.State_ID);
                        vMBranchMaster.SelectedLessorStateID = Convert.ToInt32(lessorState1.FirstOrDefault());
                        vMBranchMaster.SelectedLandlordStateID = Convert.ToInt32(landlordState1.FirstOrDefault());

                        IEnumerable<ival_Districts> ival_DistrictList = GetDistricts();
                        var lessorDist1 = ival_DistrictList.AsEnumerable().Where(row => row.District_Name == dtBranch.Rows[0]["Lessor_District"].ToString()).Select(row => row.District_ID);
                        var landlordDist1 = ival_DistrictList.AsEnumerable().Where(row => row.District_Name == dtBranch.Rows[0]["LandLord_District"].ToString()).Select(row => row.District_ID);
                        vMBranchMaster.SelectedLessorDistID = Convert.ToInt32(lessorDist1.FirstOrDefault());
                        vMBranchMaster.SelectedLandlordDistID = Convert.ToInt32(landlordDist1.FirstOrDefault());

                        IEnumerable<ival_Locality> ival_LocalityLists = GetLocality();
                        var lessorLoc1 = ival_LocalityLists.AsEnumerable().Where(row => row.Loc_Name == dtBranch.Rows[0]["Lessor_City"].ToString()).Select(row => row.Loc_Id);
                        var landlordLoc1 = ival_LocalityLists.AsEnumerable().Where(row => row.Loc_Name == dtBranch.Rows[0]["LandLord_City"].ToString()).Select(row => row.Loc_Id);
                        vMBranchMaster.SelectedLessorLocID = Convert.ToInt32(lessorLoc1.FirstOrDefault());
                        vMBranchMaster.SelectedLandlordLocID = Convert.ToInt32(landlordLoc1.FirstOrDefault());

                        vMBranchMaster.SelectedLessorStateID = Convert.ToInt32(dtBranch.Rows[0]["Lessor_State"]);
                        vMBranchMaster.SelectedLessorDistID = Convert.ToInt32(dtBranch.Rows[0]["Lessor_District"]);
                        vMBranchMaster.SelectedLessorLocID = Convert.ToInt32(dtBranch.Rows[0]["Lessor_Locality"]);
                        vMBranchMaster.SelectedLessorCityID = Convert.ToInt32(dtBranch.Rows[0]["Lessor_City"]);
                        vMBranchMaster.SelectedLessorPincode = Convert.ToInt32(dtBranch.Rows[0]["Lessor_City"]);
                        vMBranchMaster.SelectedLandlordStateID = Convert.ToInt32(dtBranch.Rows[0]["LandLord_State"]);
                        vMBranchMaster.SelectedLandlordDistID = Convert.ToInt32(dtBranch.Rows[0]["LandLord_District"]);
                        vMBranchMaster.SelectedLandlordLocID = Convert.ToInt32(dtBranch.Rows[0]["LandLord_Locality"]);
                        vMBranchMaster.SelectedLandlordCityID = Convert.ToInt32(dtBranch.Rows[0]["LandLord_City"]);
                        vMBranchMaster.SelectedLandlordPincode = Convert.ToInt32(dtBranch.Rows[0]["LandLord_City"]);
                    }

                    //vMBranchMaster.pincodeList = GetPincode();
                    vMBranchMaster.ival_CitiesList = GetCity();
                    vMBranchMaster.ival_LocalityList = GetLocality();
                    vMBranchMaster.ival_DistrictsList = GetDistricts();
                    vMBranchMaster.ival_StatesList = GetState();
                    vMBranchMaster.AccountTypeList = GetAccountType();
                    vMBranchMaster.branchHeadsList = GetBranchHeadList();

                    IEnumerable<ival_States> ival_StatesList = GetState();
                    var branchState = ival_StatesList.AsEnumerable().Where(row => row.State_Name == dtBranch.Rows[0]["Branch_State"].ToString()).Select(row => row.State_ID);
                    vMBranchMaster.SelectedBranchStateID = Convert.ToInt32(branchState.FirstOrDefault());

                    IEnumerable<ival_Districts> ival_DistrictsList = GetDistricts();
                    var branchDist = ival_DistrictsList.AsEnumerable().Where(row => row.District_Name == dtBranch.Rows[0]["Branch_District"].ToString()).Select(row => row.District_ID);
                    vMBranchMaster.SelectedBranchDistID = Convert.ToInt32(branchDist.FirstOrDefault());

                    IEnumerable<ival_Locality> ival_LocalityList = GetLocality();
                    var branchLoc = ival_LocalityList.AsEnumerable().Where(row => row.Loc_Name == dtBranch.Rows[0]["Branch_City"].ToString()).Select(row => row.Loc_Id);
                    vMBranchMaster.SelectedBranchLocID = Convert.ToInt32(branchLoc.FirstOrDefault());
                    
                    IEnumerable<ival_Cities> ival_CitiesList = GetCity();
                    var branchLoc1 = ival_CitiesList.AsEnumerable().Where(row => row.City_Name == dtBranch.Rows[0]["Branch_Loc"].ToString()).Select(row => row.Loc_Id);
                    vMBranchMaster.SelectedBranchLoc = Convert.ToInt32(branchLoc1.FirstOrDefault());
                    /*IEnumerable<ival_Locality> pincodeList = GetPincode();
                    var branchPincode = pincodeList.AsEnumerable().Where(row => row.Pin_Code == dtBranch.Rows[0]["Branch_Pincode"].ToString()).Select(row => row.Loc_Id);
                    var lessorPincode = pincodeList.AsEnumerable().Where(row => row.Pin_Code == dtBranch.Rows[0]["Lessor_Pincode"].ToString()).Select(row => row.Loc_Id);
                    var landlordPincode = pincodeList.AsEnumerable().Where(row => row.Pin_Code == dtBranch.Rows[0]["LandLord_Pincode"].ToString()).Select(row => row.Loc_Id);
                    vMBranchMaster.SelectedBranchPincode = Convert.ToString(branchPincode.FirstOrDefault());
                    vMBranchMaster.SelectedLessorPincode = Convert.ToInt32(lessorPincode.FirstOrDefault());
                    vMBranchMaster.SelectedLandlordPincode = Convert.ToInt32(landlordPincode.FirstOrDefault());*/

                    //vMBranchMaster.SelectedBranchStateID = Convert.ToString(dtBranch.Rows[0]["Branch_State"]);
                    vMBranchMaster.SelectedBranchDistID = Convert.ToInt32(dtBranch.Rows[0]["Branch_District"]);
                    vMBranchMaster.SelectedBranchLocID = Convert.ToInt32(dtBranch.Rows[0]["Branch_City"]);
                    vMBranchMaster.SelectedBranchPincode = Convert.ToInt32(dtBranch.Rows[0]["Branch_City"]);
                    vMBranchMaster.SelectedBranchLoc = Convert.ToInt32(dtBranch.Rows[0]["Branch_Loc"]);

                    vMBranchMaster.ival_BranchMaster = objBranch;
                }


                else
                {
                    return Json("Error in Database !", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("Index", "Branch");
            }
            return View(vMBranchMaster);
        }

        public ActionResult Delete(int ID)
        {
            try
            {
                sqlDataAccess = new SQLDataAccess();
                hInputPara = new Hashtable();
                hInputPara.Add("@Branch_ID", ID);
                var result = sqlDataAccess.ExecuteStoreProcedure("[usp_DeleteBranchByBranchID]", hInputPara);
                result.ToString();
                if (Convert.ToInt32(result) == 1)
                {
                    return RedirectToAction("Index", "Branch");
                }
                else
                {
                    return Json("Error in Database Record not Delete!", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //ex.InnerException.ToString();
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
            
        }

        [HttpPost]
        public IEnumerable<ival_Cities> GetCity()
        {
            List<ival_Cities> listState = new List<ival_Cities>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtCity = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCityByID");
            for (int i = 0; i < dtCity.Rows.Count; i++)
            {

                listState.Add(new ival_Cities
                {
                    Loc_Id = Convert.ToInt32(dtCity.Rows[i]["City_ID"]),
                    City_Name = Convert.ToString(dtCity.Rows[i]["City"])

                });


            }
            return listState.AsEnumerable();
        }

        [HttpPost]
        public IEnumerable<ival_States> GetState()
        {
            List<ival_States> listState = new List<ival_States>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetStateByID");
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                listState.Add(new ival_States
                {
                    State_ID = Convert.ToInt32(dtState.Rows[i]["State_ID"]),
                    State_Name = Convert.ToString(dtState.Rows[i]["State_Name"])

                });


            }
            return listState.AsEnumerable();
        }

        [HttpPost]
        public IEnumerable<ival_Districts> GetDistricts()
        {
            List<ival_Districts> lists = new List<ival_Districts>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            try
            {
                DataTable dtDistict = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDistrictByID");
                for (int i = 0; i < dtDistict.Rows.Count; i++)
                {
                    lists.Add(new ival_Districts
                    {
                        District_ID = Convert.ToInt32(dtDistict.Rows[i]["District_ID"]),
                        District_Name = Convert.ToString(dtDistict.Rows[i]["District_Name"])
                    });
                }
            }
            catch (Exception)
            {

            }
            return lists.AsEnumerable();
        }

        public IEnumerable<ival_Locality> GetLocality()
        {
            List<ival_Locality> listLoc = new List<ival_Locality>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            DataTable dtLocality = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalityByID");
            for (int i = 0; i < dtLocality.Rows.Count; i++)
            {

                listLoc.Add(new ival_Locality
                {
                    Loc_Id = Convert.ToInt32(dtLocality.Rows[i]["Loc_Id"]),
                    Loc_Name = Convert.ToString(dtLocality.Rows[i]["Loc_Name"]),
                    Pin_Code = Convert.ToInt32(dtLocality.Rows[i]["Pin_Code"])

                });
            }
            return listLoc.AsEnumerable();
        }

       /* [HttpPost]
        public IEnumerable<ival_Locality> GetPincode()
        {
            List<ival_Locality> listLocPin = new List<ival_Locality>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            DataTable dtLocPin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalityByID");
            for (int i = 0; i < dtLocPin.Rows.Count; i++)
            {

                listLocPin.Add(new ival_Locality
                {
                    Loc_Id = Convert.ToInt32(dtLocPin.Rows[i]["Loc_Id"]),
                    //Loc_Name = Convert.ToString(listLocPin.Rows[i]["Loc_Name"]),
                    Pin_Code = Convert.ToInt32(dtLocPin.Rows[i]["Pin_Code"])

                });
            }
            return listLocPin.AsEnumerable();
        }*/

        [HttpPost]
        public JsonResult GetPinByOnselect(int str)
        {
            hInputPara = new Hashtable();
            List<ival_Locality> PinList = new List<ival_Locality>();
            // DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Loc_Id", str);

            DataTable dtLocPin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPinByLocName", hInputPara);
            for (int i = 0; i < dtLocPin.Rows.Count; i++)
            {
                PinList.Add(new ival_Locality
                {
                    Loc_Id = Convert.ToInt32(dtLocPin.Rows[i]["Loc_Id"]),
                    Pin_Code = Convert.ToInt32(dtLocPin.Rows[i]["Pin_Code"])

                });
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectpinMaster = jsonSerialiser.Serialize(PinList);
            return Json(jsonProjectpinMaster);
        }

        [HttpPost]
        public IEnumerable<SelectListItem> GetAccountType()
        {
            var _AccountType = new List<SelectListItem>();
            string[] bldGrpArray = { "Savings", "Current" };
            for (int i = 0; i < bldGrpArray.Length; i++)
            {

                _AccountType.Add(new SelectListItem()
                {
                    Text = Convert.ToString(bldGrpArray[i]),
                    Value = Convert.ToString(bldGrpArray[i])

                });
            }
            return _AccountType;
        }
        [HttpPost]
        public IEnumerable<VMBranchHead> GetBranchHeadList()
        {
            List<VMBranchHead> vMBranchHeadsList = new List<VMBranchHead>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtbranchHead = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_BranchHeadList");
            if (dtbranchHead.Rows.Count > 0 && dtbranchHead != null)
            {


                for (int i = 0; i < dtbranchHead.Rows.Count; i++)
                {

                    vMBranchHeadsList.Add(new VMBranchHead
                    {
                        First_Name = Convert.ToString(dtbranchHead.Rows[i]["First_Name"]),
                        Employee_ID = Convert.ToInt32(dtbranchHead.Rows[i]["Employee_ID"]),
                        Role_ID = Convert.ToInt32(dtbranchHead.Rows[i]["Employee_ID"]),
                        Role_Name = Convert.ToString(dtbranchHead.Rows[i]["Role_Name"])
                    });
                }
            }
            return vMBranchHeadsList.AsEnumerable();
        }
    }
}