﻿using I_Value.WebApp.Models;
using IvalueDataAccess;
using IValuePublishProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IValuePublishProject.Controllers
{
    public class PropertyUnitTypeMasterController : Controller
    {
        // GET: PropertyUnitTypeMaster
        SQLDataAccess sqlDataAccess;
        Hashtable hInputPara;
        IvalueDataModel Datamodel = new IvalueDataModel();
        // GET: SubLocality
        public ActionResult Index()
        {
            VMPropertyUnitTypeMaster vMPropertyUnitTypeMaster = new VMPropertyUnitTypeMaster();
            SQLDataAccess sqlDataAccess;

            vMPropertyUnitTypeMaster.ival_PropertyTypeList = GetProperty();
            vMPropertyUnitTypeMaster.ival_SelectTypeList = Getselect();
            vMPropertyUnitTypeMaster.ival_UnitTypeList = Getunit();
            vMPropertyUnitTypeMaster.ival_PropertyUnitTypeMaster = GetPropertyUnitType();
            return View(vMPropertyUnitTypeMaster);
        }

        [HttpPost]
        public ActionResult AddPropertyType(string str)
        {
            string resultStr = string.Empty;
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

            for (int i = 0; i < list1.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                //hInputPara.Add("@property_id", Convert.ToInt32(list1.Rows[i]["txt_Locid"]));
                hInputPara.Add("@property_Type", Convert.ToString(list1.Rows[i]["txtpropertyselect1"]));
                hInputPara.Add("@select_Type", Convert.ToString(list1.Rows[i]["txtSelectType1"]));
                hInputPara.Add("@unit_Type", Convert.ToString(list1.Rows[i]["txtUnitType1"]));
                // hInputPara.Add("@Created_By", "Sms");

                string Datastr = sqlDataAccess.ExecuteStoreProcedure("usp_InsertPropertyDetails", hInputPara);
                if (Datastr == "Already Exists")
                {
                    resultStr = Datastr;
                }
                else if (Datastr == "1")
                {
                    resultStr = "Details Added Successfully";
                }
                else
                {
                    resultStr = "Error while inserting";
                }
                hInputPara.Clear();

            }
            return Json(new { success = true, Message = resultStr }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public IEnumerable<ival_PropertyType> GetProperty()
        {
            List<ival_PropertyType> lists = new List<ival_PropertyType>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            try
            {
                DataTable dtDistict = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPropertyUnitByProjectType");
                for (int i = 0; i < dtDistict.Rows.Count; i++)
                {
                    lists.Add(new ival_PropertyType
                    {
                        Lookup_ID = Convert.ToInt32(dtDistict.Rows[i]["Lookup_ID"]),
                        Lookup_Value = Convert.ToString(dtDistict.Rows[i]["Lookup_Value"])
                    });
                }
            }
            catch (Exception)
            {

            }
            return lists.AsEnumerable();
        }

        [HttpPost]
        public IEnumerable<ival_SelectType> Getselect()
        {
            List<ival_SelectType> lists = new List<ival_SelectType>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            try
            {
                DataTable dtDistict = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPropertyUnitByTypeOfUnit");
                for (int i = 0; i < dtDistict.Rows.Count; i++)
                {
                    lists.Add(new ival_SelectType
                    {
                        Lookup_ID = Convert.ToInt32(dtDistict.Rows[i]["Lookup_ID"]),
                        Lookup_Value = Convert.ToString(dtDistict.Rows[i]["Lookup_Value"])
                    });
                }
            }
            catch (Exception)
            {

            }
            return lists.AsEnumerable();
        }

        [HttpPost]
        public IEnumerable<ival_UnitType> Getunit()
        {
            List<ival_UnitType> listLoc = new List<ival_UnitType>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            DataTable dtLocality = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPropertyUnitByUType");
            for (int i = 0; i < dtLocality.Rows.Count; i++)
            {

                listLoc.Add(new ival_UnitType
                {
                    Lookup_ID = Convert.ToInt32(dtLocality.Rows[i]["Lookup_ID"]),
                    Lookup_Value = Convert.ToString(dtLocality.Rows[i]["Lookup_Value"]),
                    //Pin_Code = Convert.ToInt32(dtLocality.Rows[i]["Pin_Code"])

                });
            }
            return listLoc.AsEnumerable();
        }



        [HttpPost]
        public IEnumerable<ival_PropertyUnitTypeMaster> GetPropertyUnitType()
        {
            List<ival_PropertyUnitTypeMaster> lists = new List<ival_PropertyUnitTypeMaster>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            DataTable dtPropertyUnit = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Getival_Property_Details");
            for (int i = 0; i < dtPropertyUnit.Rows.Count; i++)
            {

                lists.Add(new ival_PropertyUnitTypeMaster
                {
                    property_id = Convert.ToInt32(dtPropertyUnit.Rows[i]["property_id"]),
                    property_Type = dtPropertyUnit.Rows[i]["property_Type"].ToString(),
                    select_Type = dtPropertyUnit.Rows[i]["select_Type"].ToString(),
                    unit_Type = dtPropertyUnit.Rows[i]["unit_Type"].ToString()
                });
            }
            return lists.AsEnumerable();
        }

        //[HttpPost]
        //public JsonResult GetPinByOnselect(int str)
        //{
        //    hInputPara = new Hashtable();
        //    List<ival_Locality> PinList = new List<ival_Locality>();
        //    // DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
        //    SQLDataAccess sqlDataAccess = new SQLDataAccess();
        //    hInputPara.Add("@Loc_Id", str);

        //    DataTable dtLocPin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPinByLocName", hInputPara);
        //    for (int i = 0; i < dtLocPin.Rows.Count; i++)
        //    {
        //        PinList.Add(new ival_Locality
        //        {
        //            Pin_Code = Convert.ToInt32(dtLocPin.Rows[i]["Pin_Code"])

        //        });
        //    }
        //    var jsonSerialiser = new JavaScriptSerializer();
        //    var jsonProjectpinMaster = jsonSerialiser.Serialize(PinList);
        //    return Json(jsonProjectpinMaster);
        //}

        [HttpPost]
        public JsonResult GetLookupvalueSelect(int ID)
        {
            hInputPara = new Hashtable();
            List<ival_PropertyUnitTypeMaster> LocList = new List<ival_PropertyUnitTypeMaster>();
            // DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Lookup_ID", ID);

            DataTable dtLocName = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPropertyTypeUnit", hInputPara);
            for (int i = 0; i < dtLocName.Rows.Count; i++)
            {
                LocList.Add(new ival_PropertyUnitTypeMaster
                {
                    //Lookup_ID = Convert.ToInt32(dtLocName.Rows[i]["Lookup_ID"]),
                    Lookup_Value = dtLocName.Rows[i]["Lookup_Value"].ToString()

                });
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectpinMaster = jsonSerialiser.Serialize(LocList);
            return Json(jsonProjectpinMaster);
        }

        [HttpPost]
        public JsonResult GetLookupvalueSelect2(int ID)
        {
            hInputPara = new Hashtable();
            List<ival_PropertyUnitTypeMaster> LocList = new List<ival_PropertyUnitTypeMaster>();
            // DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Lookup_ID", ID);

            DataTable dtLocName = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPropertyTypeUnit", hInputPara);
            for (int i = 0; i < dtLocName.Rows.Count; i++)
            {
                LocList.Add(new ival_PropertyUnitTypeMaster
                {
                    // Lookup_ID = Convert.ToInt32(dtLocName.Rows[i]["Lookup_ID"]),
                    Lookup_Value = dtLocName.Rows[i]["Lookup_Value"].ToString()

                });
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectpinMaster = jsonSerialiser.Serialize(LocList);
            return Json(jsonProjectpinMaster);
        }




        [HttpPost]
        public JsonResult GetLookupvalueSelect1(int ID)
        {
            hInputPara = new Hashtable();
            List<ival_PropertyUnitTypeMaster> LocList = new List<ival_PropertyUnitTypeMaster>();
            // DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Lookup_ID", ID);

            DataTable dtLocName = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPropertyTypeUnit", hInputPara);
            for (int i = 0; i < dtLocName.Rows.Count; i++)
            {
                LocList.Add(new ival_PropertyUnitTypeMaster
                {
                    //Lookup_ID = Convert.ToInt32(dtLocName.Rows[i]["Lookup_ID"]),
                    Lookup_Value = dtLocName.Rows[i]["Lookup_Value"].ToString()

                });
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectpinMaster = jsonSerialiser.Serialize(LocList);
            return Json(jsonProjectpinMaster);
        }

        [HttpGet]
        public ActionResult EditPropertyUnitType(int ID)
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //return View();
            }
            VMPropertyUnitTypeMaster vMPropertyUnitTypeMaster = new VMPropertyUnitTypeMaster();

            vMPropertyUnitTypeMaster.ival_PropertyTypeList = GetProperty();
            vMPropertyUnitTypeMaster.ival_SelectTypeList = Getselect();
            vMPropertyUnitTypeMaster.ival_UnitTypeList = Getunit();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            try
            {
                hInputPara.Add("property_id", ID);

                DataTable dtProperty = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetEditPropertyUnitTypeById", hInputPara);
                if (dtProperty != null && dtProperty.Rows.Count > 0)
                {
                    vMPropertyUnitTypeMaster.property_id = ID;
                    vMPropertyUnitTypeMaster.property_Type = dtProperty.Rows[0]["property_Type"].ToString();
                    vMPropertyUnitTypeMaster.select_Type = dtProperty.Rows[0]["select_Type"].ToString();
                    vMPropertyUnitTypeMaster.unit_Type = dtProperty.Rows[0]["unit_Type"].ToString();
                   


                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
            return View(vMPropertyUnitTypeMaster);
        }

        [HttpPost]
        public ActionResult UpdateProperty(string str)
        {
            string resultStr = string.Empty;
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

            for (int i = 0; i < list1.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                hInputPara.Add("@property_Type", Convert.ToString(list1.Rows[i]["txtpropertyselect"]));
                hInputPara.Add("@select_Type", Convert.ToString(list1.Rows[i]["txtSelectType"]));
                hInputPara.Add("@unit_Type", Convert.ToString(list1.Rows[i]["txtUnitType"]));

                //hInputPara.Add("@Pin_Code", Convert.ToInt32(list1.Rows[i]["txt_Pin"]));
                //hInputPara.Add("@Modified_By", "Test");

                string Datastr = sqlDataAccess.ExecuteStoreProcedure("usp_UpdatePropertyUnitTypeById", hInputPara);
                if (Datastr == "Already Exists")
                {
                    resultStr = Datastr;
                }
                else if (Datastr == "1")
                {
                    resultStr = "Details Updated Successfully";
                }
                else
                {
                    resultStr = "Error while inserting";
                }
                hInputPara.Clear();

            }
            return Json(new { success = true, Message = resultStr }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult ViewProperty(int ID)
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //return View();
            }
            VMPropertyUnitTypeMaster vMPropertyUnitTypeMaster = new VMPropertyUnitTypeMaster();

            vMPropertyUnitTypeMaster.ival_PropertyTypeList = GetProperty();
            vMPropertyUnitTypeMaster.ival_SelectTypeList = Getselect();
            vMPropertyUnitTypeMaster.ival_UnitTypeList = Getunit();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            try
            {
                hInputPara.Add("property_id", ID);
                DataTable dtProperty = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetEditPropertyUnitTypeById", hInputPara);
                if (dtProperty != null && dtProperty.Rows.Count > 0)
                {
                    vMPropertyUnitTypeMaster.property_id = ID;
                    vMPropertyUnitTypeMaster.property_Type = Convert.ToString(dtProperty.Rows[0]["property_Type"]);



                    vMPropertyUnitTypeMaster.select_Type = Convert.ToString(dtProperty.Rows[0]["select_Type"]);
                    vMPropertyUnitTypeMaster.unit_Type = Convert.ToString(dtProperty.Rows[0]["unit_Type"]);

                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
            return View(vMPropertyUnitTypeMaster);
        }

        [HttpPost]
        public ActionResult DeleteProperty(int ID)
        {
            try
            {
                SQLDataAccess sqlDataAccess = new SQLDataAccess();
                Hashtable hInputPara = new Hashtable();
                hInputPara.Add("@property_id", ID);
                var result = sqlDataAccess.ExecuteStoreProcedure("[usp_DeletePropertyUnitTypeById]", hInputPara);
                result.ToString();
                if (Convert.ToInt32(result) == 1)
                {

                    return RedirectToAction("Index", "PropertyUnitTypeMaster");
                }
                else
                {
                    return Json("Error in Database Record not Delete!", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

        }

    }
}