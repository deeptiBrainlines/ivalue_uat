﻿using I_Value.WebApp.Models;
using IvalueDataAccess;
using IValuePublishProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IValuePublishProject.Controllers
{
    public class SubLocalityController : Controller
    {
        SQLDataAccess sqlDataAccess;
        Hashtable hInputPara;
        IvalueDataModel Datamodel = new IvalueDataModel();
        // GET: SubLocality
        public ActionResult Index()
        {
            VMSubLocalityMaster vMSubLocalityMaster = new VMSubLocalityMaster();
            SQLDataAccess sqlDataAccess;

            vMSubLocalityMaster.ival_DistrictsList = GetDistricts();
            vMSubLocalityMaster.ival_CityList = GetCity();
            //vMSubLocalityMaster.ival_LocPincodeList = GetPincode();
            vMSubLocalityMaster.ival_SubLocalityMaster = GetSubLocality();
            return View(vMSubLocalityMaster);
        }

        [HttpPost]
        public ActionResult AddSubLocality(string str)
        {
            string resultStr = string.Empty;
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

            for (int i = 0; i < list1.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                hInputPara.Add("@Loc_Id", Convert.ToInt32(list1.Rows[i]["txt_Locid"]));
                hInputPara.Add("@SubLocality", Convert.ToString(list1.Rows[i]["txt_SubLoc"]));
                hInputPara.Add("@Pin_Code", Convert.ToInt32(list1.Rows[i]["txt_Pin"]));
                hInputPara.Add("@Created_By", "Sms");

                string Datastr = sqlDataAccess.ExecuteStoreProcedure("usp_InsertSubLocality", hInputPara);
                if (Datastr == "SubLocality Already Exists")
                {
                    resultStr = Datastr;
                }
                else if (Datastr == "1")
                {
                    resultStr = "SubLocality Details Added Successfully";
                }
                else
                {
                    resultStr = "Error while inserting";
                }
                hInputPara.Clear();

            }
            return Json(new { success = true, Message = resultStr }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public IEnumerable<ival_Districts> GetDistricts()
        {
            List<ival_Districts> lists = new List<ival_Districts>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            try
            {
                DataTable dtDistict = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDistrictByID");
                for (int i = 0; i < dtDistict.Rows.Count; i++)
                {
                    lists.Add(new ival_Districts
                    {
                        District_ID = Convert.ToInt32(dtDistict.Rows[i]["District_ID"]),
                        District_Name = Convert.ToString(dtDistict.Rows[i]["District_Name"])
                    });
                }
            }
            catch (Exception)
            {

            }
            return lists.AsEnumerable();
        }

        [HttpPost]
        public IEnumerable<ival_Districts> GetCity()
        {
            List<ival_Districts> lists = new List<ival_Districts>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            try
            {
                DataTable dtDistict = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCityN");
                for (int i = 0; i < dtDistict.Rows.Count; i++)
                {
                    lists.Add(new ival_Districts
                    {
                        City_ID = Convert.ToInt32(dtDistict.Rows[i]["City_Id"]),
                        City_Name = Convert.ToString(dtDistict.Rows[i]["City"])
                    });
                }
            }
            catch (Exception)
            {

            }
            return lists.AsEnumerable();
        }

        [HttpPost]
        public IEnumerable<ival_Locality> GetLocality()
        {
            List<ival_Locality> listLoc = new List<ival_Locality>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            DataTable dtLocality = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalityByID");
            for (int i = 0; i < dtLocality.Rows.Count; i++)
            {

                listLoc.Add(new ival_Locality
                {
                    Loc_Id = Convert.ToInt32(dtLocality.Rows[i]["Loc_Id"]),
                    Loc_Name = Convert.ToString(dtLocality.Rows[i]["Loc_Name"]),
                    Pin_Code = Convert.ToInt32(dtLocality.Rows[i]["Pin_Code"])

                });
            }
            return listLoc.AsEnumerable();
        }

        [HttpPost]
        public IEnumerable<ival_Locality> GetPincode()
        {
            List<ival_Locality> listLocPin = new List<ival_Locality>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            DataTable dtLocPin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalityByID");
            for (int i = 0; i < dtLocPin.Rows.Count; i++)
            {

                listLocPin.Add(new ival_Locality
                {
                    Loc_Id = Convert.ToInt32(dtLocPin.Rows[i]["Loc_Id"]),
                    //Loc_Name = Convert.ToString(listLocPin.Rows[i]["Loc_Name"]),
                    Pin_Code = Convert.ToInt32(dtLocPin.Rows[i]["Pin_Code"])

                });
            }
            return listLocPin.AsEnumerable();
        }

        [HttpPost]
        public IEnumerable<ival_SubLocalityMaster> GetSubLocality()
        {
            List<ival_SubLocalityMaster> lists = new List<ival_SubLocalityMaster>();
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            DataTable dtSubLocality = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocPinAndSubLoc");
            for (int i = 0; i < dtSubLocality.Rows.Count; i++)
            {

                lists.Add(new ival_SubLocalityMaster
                {
                    SubLocality_Id = Convert.ToInt32(dtSubLocality.Rows[i]["SubLocality_Id"]),
                    Loc_Name = dtSubLocality.Rows[i]["Loc_Name"].ToString(),
                    District_Name = dtSubLocality.Rows[i]["District_Name"].ToString(),
                    Pin_Code = Convert.ToInt32(dtSubLocality.Rows[i]["Pin_Code"]),
                    Sub_Locality = dtSubLocality.Rows[i]["Sub_Locality"].ToString()
                });
            }
            return lists.AsEnumerable();
        }

        [HttpPost]
        public JsonResult GetPinByOnselect(int str)
        {
            hInputPara = new Hashtable();
            List<ival_Locality> PinList = new List<ival_Locality>();
            // DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Loc_Id", str);

            DataTable dtLocPin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPinByLocName", hInputPara);
            for (int i = 0; i < dtLocPin.Rows.Count; i++)
            {
                PinList.Add(new ival_Locality
                {
                    Pin_Code = Convert.ToInt32(dtLocPin.Rows[i]["Pin_Code"])

                });
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectpinMaster = jsonSerialiser.Serialize(PinList);
            return Json(jsonProjectpinMaster);
        }

        [HttpPost]
        public JsonResult GetLocalityByOnselect(int ID)
        {
            hInputPara = new Hashtable();
            List<ival_SubLocalityMaster> LocList = new List<ival_SubLocalityMaster>();
            // DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@District_ID", ID);

            DataTable dtLocName = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Getloc", hInputPara);
            for (int i = 0; i < dtLocName.Rows.Count; i++)
            {
                LocList.Add(new ival_SubLocalityMaster
                {
                    Loc_Id = Convert.ToInt32(dtLocName.Rows[i]["Loc_Id"]),
                    Loc_Name = dtLocName.Rows[i]["Loc_Name"].ToString()

                });
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectpinMaster = jsonSerialiser.Serialize(LocList);
            return Json(jsonProjectpinMaster);
        }


        [HttpPost]
        public JsonResult GetCitySelect1(int ID)
        {
            hInputPara = new Hashtable();
            List<ival_SubLocalityMaster> LocList = new List<ival_SubLocalityMaster>();
            // DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            SQLDataAccess sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Dist_Id", ID);

            DataTable dtLocName = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCityId", hInputPara);
            for (int i = 0; i < dtLocName.Rows.Count; i++)
            {
                LocList.Add(new ival_SubLocalityMaster
                {
                    City_Id = Convert.ToInt32(dtLocName.Rows[i]["City_Id"]),
                    City_Name = dtLocName.Rows[i]["City"].ToString()

                });
            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectpinMaster = jsonSerialiser.Serialize(LocList);
            return Json(jsonProjectpinMaster);
        }

        [HttpGet]
        public ActionResult EditSubLocality(int ID)
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //return View();
            }
            VMSubLocalityMaster vMSubLocalityMaster = new VMSubLocalityMaster();
            vMSubLocalityMaster.ival_LocalityList = GetLocality();
            vMSubLocalityMaster.ival_DistrictsList = GetDistricts();

            vMSubLocalityMaster.ival_CityList = GetCity();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            try
            {
                hInputPara.Add("SubLocality_Id", ID);

                DataTable dtSubLocality = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetEditSubLocalityById", hInputPara);
                if (dtSubLocality != null && dtSubLocality.Rows.Count > 0)
                {
                    vMSubLocalityMaster.SubLocality_Id = ID;
                    vMSubLocalityMaster.District_ID = Convert.ToInt32(dtSubLocality.Rows[0]["Dist_ID"]);
                    vMSubLocalityMaster.City_Id = Convert.ToInt32(dtSubLocality.Rows[0]["City_Id"]);
                    vMSubLocalityMaster.City_Name = Convert.ToString(dtSubLocality.Rows[0]["City"]);

                    ViewBag.City_Name = vMSubLocalityMaster.City_Name;
                    vMSubLocalityMaster.District_Name = dtSubLocality.Rows[0]["District_Name"].ToString();
                    vMSubLocalityMaster.Loc_Id = Convert.ToInt32(dtSubLocality.Rows[0]["Locality_Id"]);
                    vMSubLocalityMaster.Pin_Code = Convert.ToInt32(dtSubLocality.Rows[0]["Pin_Code"]);
                    vMSubLocalityMaster.Sub_Locality = dtSubLocality.Rows[0]["Sub_Locality"].ToString();

                    /*object valueLoc_Id = Convert.ToInt32(dtSubLocality.Rows[0]["Locality_Id"]);
                    if (dtSubLocality.Columns.Contains("Locality_Id") && valueLoc_Id != DBNull.Value && valueLoc_Id.ToString() != "")
                        objSubLocality.Loc_Id = Convert.ToInt32(dtSubLocality.Rows[0]["Locality_Id"]);
                    else
                        objSubLocality.Loc_Id = 0;

                    object valuePin = dtSubLocality.Rows[0]["Pin_Code"];
                    if (dtSubLocality.Columns.Contains("Pin_Code") && valuePin != null && valuePin.ToString() != "")
                        objSubLocality.Pin_Code = Convert.ToInt32(dtSubLocality.Rows[0]["Pin_Code"]);
                    else
                        objSubLocality.Pin_Code = 0;

                    object valueSubLocality = dtSubLocality.Rows[0]["Sub_Locality"];
                    if (dtSubLocality.Columns.Contains("Sub_Locality") && valueSubLocality != null && valueSubLocality.ToString() != "")
                        objSubLocality.Sub_Locality = dtSubLocality.Rows[0]["Sub_Locality"].ToString();
                    else
                        objSubLocality.Sub_Locality = string.Empty;*/
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
            return View(vMSubLocalityMaster);
        }

        [HttpPost]
        public ActionResult UpdateSubLocality(string str)
        {
            string resultStr = string.Empty;
            DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));

            for (int i = 0; i < list1.Rows.Count; i++)
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                hInputPara.Add("@SubLocality_Id", Convert.ToInt32(list1.Rows[i]["txt_SubLocId"]));
                hInputPara.Add("@District_ID", Convert.ToInt32(list1.Rows[i]["txt_Distid"]));
                hInputPara.Add("@Loc_Id", Convert.ToInt32(list1.Rows[i]["txt_Locid"]));
                hInputPara.Add("@SubLocality", Convert.ToString(list1.Rows[i]["txt_SubLoc"]));
                hInputPara.Add("@Pin_Code", Convert.ToInt32(list1.Rows[i]["txt_Pin"]));
                hInputPara.Add("@Modified_By", "Test");

                string Datastr = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateSubLocalityById", hInputPara);
                if (Datastr == "SubLocality Already Exists")
                {
                    resultStr = Datastr;
                }
                else if (Datastr == "1")
                {
                    resultStr = "SubLocality Details Updated Successfully";
                }
                else
                {
                    resultStr = "Error while inserting";
                }
                hInputPara.Clear();

            }
            return Json(new { success = true, Message = resultStr }, JsonRequestBehavior.AllowGet);
            
        }

        [HttpGet]
        public ActionResult ViewSubLocalityDetails(int ID)
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //return View();
            }
            VMSubLocalityMaster vMSubLocalityMaster = new VMSubLocalityMaster();
            vMSubLocalityMaster.ival_LocalityList = GetLocality();
            vMSubLocalityMaster.ival_DistrictsList = GetDistricts();
            vMSubLocalityMaster.ival_CityList = GetCity();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            try
            {
                hInputPara.Add("SubLocality_Id", ID);

                DataTable dtSubLocality = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetEditSubLocalityById", hInputPara);
                if (dtSubLocality != null && dtSubLocality.Rows.Count > 0)
                {
                    vMSubLocalityMaster.SubLocality_Id = ID;
                    vMSubLocalityMaster.District_ID = Convert.ToInt32(dtSubLocality.Rows[0]["Dist_Id"]);



                    vMSubLocalityMaster.City_Name = Convert.ToString(dtSubLocality.Rows[0]["City"]);
                    ViewBag.City_Name = vMSubLocalityMaster.City_Name;
                    vMSubLocalityMaster.District_Name = Convert.ToString(dtSubLocality.Rows[0]["District_Name"]);
                    vMSubLocalityMaster.Loc_Id = Convert.ToInt32(dtSubLocality.Rows[0]["Locality_Id"]);
                    vMSubLocalityMaster.Pin_Code = Convert.ToInt32(dtSubLocality.Rows[0]["Pin_Code"]);
                    vMSubLocalityMaster.Sub_Locality = dtSubLocality.Rows[0]["Sub_Locality"].ToString();
}
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
            return View(vMSubLocalityMaster);
        }

        [HttpPost]
        public ActionResult DeleteSubLocality(int ID)
        {
            try
            {
                SQLDataAccess sqlDataAccess = new SQLDataAccess();
                Hashtable hInputPara = new Hashtable();
                hInputPara.Add("@SubLocality_Id", ID);
                var result = sqlDataAccess.ExecuteStoreProcedure("[usp_DeleteSubLocalityByID]", hInputPara);
                result.ToString();
                if (Convert.ToInt32(result) == 1)
                {

                    return RedirectToAction("Index", "SubLocality");
                }
                else
                {
                    return Json("Error in Database Record not Delete!", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

        }

    }
}