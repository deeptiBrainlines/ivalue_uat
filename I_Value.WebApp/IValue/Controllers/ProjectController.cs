﻿using I_Value.WebApp.Models;
using IvalueDataAccess;
using IValuePublishProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
namespace I_Value.WebApp.Controllers
{
    public class ProjectController : Controller
    {
        private static readonly ILog Log = LogManager.GetLogger("mylog");
        //  ILog log = LogManager.GetLogger("mylog");
        SQLDataAccess sqlDataAccess;
        Hashtable hInputPara;
        Hashtable hInputPara1;
        IvalueDataModel Datamodel = new IvalueDataModel();
        //IvalueDataModelNew ivalueDataModelNew = new IvalueDataModelNew();
        ViewModel viewModel;
        Int32 projectId = 0;
        Int32 buildingId = 0;
        Int32 wingId = 0;
        // GET: Project
        public ActionResult Index()
        {

            try
            {
                //  log.Info("This is my first log message");
                viewModel = new ViewModel();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                DataTable dtProjects = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjects");

                List<ival_ProjectMaster> listProjects = new List<ival_ProjectMaster>();

                if (dtProjects.Rows.Count > 0)

                {
                    for (int i = 0; i < dtProjects.Rows.Count; i++)
                    {

                        listProjects.Add(new ival_ProjectMaster
                        {
                            Project_ID = Convert.ToInt32(dtProjects.Rows[i]["Project_ID"]),
                            Project_Name = Convert.ToString(dtProjects.Rows[i]["Project_Name"]),
                            Legal_City = Convert.ToString(dtProjects.Rows[i]["Legal_City"]),
                            Legal_State = Convert.ToString(dtProjects.Rows[i]["Legal_State"]),

                        });


                    }
                }

                viewModel.ival_ProjectMastersList = listProjects;
                return View(viewModel);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("Login", "Account");
            }
        }
        public ActionResult AddProject()
        {

            ViewModel vm = new ViewModel();
            vm.ival_BuilderGroupMasters = GetBuilderMaster();
            vm.ival_BuilderCompanyMasters = GetCompanyBuilderMaster();
            vm.ival_LookupCategoryValuespincode = Getpincode();
            vm.ival_LookupCategoryValuesProjecttype = GetProjectTypeMaster();
            vm.ival_States = GetStates();
            vm.ival_LookupCategoryValuesunittype = GetChkboxunits();
            //vm.ival_LookupCategoryValuestypeofownership = GetTypeofOwnership();
            vm.ival_LookupCategoryValuesbasicInfra = GetBasicInfra();
            vm.ival_CommonAreaI = GetCommonAreas();
            vm.ival_CommonAreaI2 = GetCommonAreas2();
            vm.ival_CommonAreaI2 = GetCommonAreas3();
            vm.ival_LookupCategoryValuesClassOfLocality = GetClassoflocality();
            vm.ival_LookupCategoryValuesSocialInfra = GetSocialInfra();
            vm.ival_LookupCategoryValuesconnectivity = GetConnectivity();
            vm.ival_States_postal = GetStates();
            vm.ival_LookupCategoryValuespincode_postal = Getpincode();
            vm.ival_ApprovalType = GetApprovalType();
            vm.ival_LookupCategoryBoundaries = GetBoundaries();
            vm.ival_LookupCategorySideMargins = GetSideMargins();
            Session["Apprtype"] = vm.ival_ApprovalType;
            return View(vm);
        }

        [HttpGet]
        public ActionResult Edit(int? ID)
        {
            Session["parojectid"] = ID;
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }
            else
            {
                ViewModel VMprojectmaster = new ViewModel();
                ival_ProjectMaster objproject = new ival_ProjectMaster();
                ival_BasicInfraAvailability objinfra = new ival_BasicInfraAvailability();
                ival_LocalTransportForProject objlocal = new ival_LocalTransportForProject();
                ival_ProjectUnitDetails objunit = new ival_ProjectUnitDetails();

                var items = new List<SelectListItem>();
                sqlDataAccess = new SQLDataAccess();
                hInputPara = new Hashtable();
                try
                {
                    hInputPara.Add("@Project_ID", ID);
                    DataTable dtprojects = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectProjectByID", hInputPara);
                    DataTable dtprojectsApproval = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectApproval", hInputPara);
                    DataTable dtprojectsSidemargins = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectSideMargin", hInputPara);
                    DataTable dtprojectsBoundaries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectBoundaries", hInputPara);
                    DataTable dtbasicinfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectBasicInfraAvailability", hInputPara);
                    DataTable dtlocaltransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalTransport", hInputPara);
                    DataTable dtprojectscheckunits = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetcheckedUnitType", hInputPara);



                    if (dtprojects.Rows.Count > 0)
                    {
                        ViewBag.SelectedModelGroupmaster = Convert.ToInt32(dtprojects.Rows[0]["Builder_Group_ID"]);
                        ViewBag.SelectedModelCompanyMaster = Convert.ToInt32(dtprojects.Rows[0]["Builder_Company_ID"]);
                        ViewBag.SelectedModellookupprojecttype = Convert.ToString(dtprojects.Rows[0]["P_TypeID"]);
                        objproject.Project_Name = Convert.ToString(dtprojects.Rows[0]["Project_Name"]);
                        objproject.Legal_Address1 = Convert.ToString(dtprojects.Rows[0]["Legal_Address1"]);
                        objproject.Legal_Address2 = Convert.ToString(dtprojects.Rows[0]["Legal_Address2"]);
                        objproject.Legal_Locality = Convert.ToString(dtprojects.Rows[0]["Legal_Locality"]);
                        objproject.Legal_Street = Convert.ToString(dtprojects.Rows[0]["Legal_Street"]);
                        objproject.Legal_City = Convert.ToString(dtprojects.Rows[0]["Legal_City"]);
                        objproject.Legal_District = Convert.ToString(dtprojects.Rows[0]["Legal_District"]);
                        ViewBag.SelectedModelstates = Convert.ToInt32(dtprojects.Rows[0]["Legal_State"].ToString());
                        ViewBag.SelectedModelpincode = Convert.ToInt32(dtprojects.Rows[0]["Legal_Pincode"]);
                        objproject.Postal_Address1 = Convert.ToString(dtprojects.Rows[0]["Postal_Address1"]);
                        objproject.Postal_Address2 = Convert.ToString(dtprojects.Rows[0]["Postal_Address2"]);
                        objproject.Postal_Street = Convert.ToString(dtprojects.Rows[0]["Postal_Street"]);
                        objproject.Postal_Locality = Convert.ToString(dtprojects.Rows[0]["Postal_Locality"]);
                        objproject.Postal_City = Convert.ToString(dtprojects.Rows[0]["Postal_City"]);
                        objproject.Postal_District = Convert.ToString(dtprojects.Rows[0]["Postal_District"]);
                        ViewBag.SelectedModelstates_postal = Convert.ToInt32(dtprojects.Rows[0]["Postal_State"].ToString());
                        ViewBag.Postal_Pincode = Convert.ToString(dtprojects.Rows[0]["Postal_Pincode"]);
                        objproject.Postal_NearbyLandmark = Convert.ToString(dtprojects.Rows[0]["Postal_NearbyLandmark"]);
                        objproject.RERA_Approval_Num = Convert.ToString(dtprojects.Rows[0]["RERA_Approval_Num"]);
                        objproject.Total_Land_Area = Convert.ToString(dtprojects.Rows[0]["Total_Land_Area"]);
                        objproject.Total_Permissible_FSI = Convert.ToString(dtprojects.Rows[0]["Total_Permissible_FSI"]);
                        objproject.Approved_Builtup_Area = Convert.ToString(dtprojects.Rows[0]["Approved_Builtup_Area"]);
                        objproject.Approved_No_of_Buildings = Convert.ToString(dtprojects.Rows[0]["Approved_No_of_Buildings"]);
                        objproject.Approved_No_of_plotsorflats = Convert.ToString(dtprojects.Rows[0]["Approved_No_of_plotsorflats"]);
                        objproject.Approved_No_of_Wings = Convert.ToString(dtprojects.Rows[0]["Approved_No_of_Wings"]);
                        objproject.Approved_No_of_Floors = Convert.ToString(dtprojects.Rows[0]["Approved_No_of_Floors"]);
                        objproject.Approved_No_of_Units = Convert.ToString(dtprojects.Rows[0]["Approved_No_of_Units"]);
                        objproject.Specifications = Convert.ToString(dtprojects.Rows[0]["Specifications"]);
                        objproject.Amenities = Convert.ToString(dtprojects.Rows[0]["Amenities"]);
                        objproject.Cord_Latitude = Convert.ToString(dtprojects.Rows[0]["Cord_Latitude"]);
                        objproject.Cord_Longitude = Convert.ToString(dtprojects.Rows[0]["Cord_Longitude"]);
                        objproject.AdjoiningRoad_Width1 = Convert.ToString(dtprojects.Rows[0]["AdjoiningRoad_Width1"]);
                        objproject.AdjoiningRoad_Width2 = Convert.ToString(dtprojects.Rows[0]["AdjoiningRoad_Width2"]);
                        objproject.AdjoiningRoad_Width3 = Convert.ToString(dtprojects.Rows[0]["AdjoiningRoad_Width3"]);
                        objproject.AdjoiningRoad_Width4 = Convert.ToString(dtprojects.Rows[0]["AdjoiningRoad_Width4"]);
                        ViewBag.SelectedModelsocialInfra = Convert.ToString(dtprojects.Rows[0]["Social_Infrastructure"]);
                        ViewBag.SelectedModelclassoflocality = Convert.ToString(dtprojects.Rows[0]["Class_of_Locality"]);
                        objproject.DistFrom_City_Center = Convert.ToString(dtprojects.Rows[0]["DistFrom_City_Center"]);
                        objproject.Locality_Remarks = Convert.ToString(dtprojects.Rows[0]["Locality_Remarks"]);
                        objproject.Deviation_Description = Convert.ToString(dtprojects.Rows[0]["Deviation_Description"]);
                        VMprojectmaster.ival_BuilderGroupMasters = GetBuilderMaster();
                        VMprojectmaster.ival_BuilderCompanyMasters = GetCompanyBuilderMaster();
                        VMprojectmaster.ival_LookupCategoryValuespincode = Getpincode();
                        VMprojectmaster.ival_LookupCategoryValuesProjecttype = GetProjectTypeMaster();
                        VMprojectmaster.ival_States = GetStates();

                        VMprojectmaster.ival_LookupCategoryValuesunittype = GetChkboxunits();
                        VMprojectmaster.ival_LookupCategoryValuesClassOfLocality = GetClassoflocality();
                        VMprojectmaster.ival_LookupCategoryValuesSocialInfra = GetSocialInfra();
                        VMprojectmaster.ival_LookupCategoryValuesconnectivity = GetConnectivity();
                        VMprojectmaster.ival_States_postal = GetStates();
                        VMprojectmaster.ival_LookupCategoryValuespincode_postal = Getpincode();
                        VMprojectmaster.ival_ApprovalType = GetApprovalType();
                        VMprojectmaster.ival_LookupCategoryBoundaries = GetBoundaries();
                        VMprojectmaster.ival_LookupCategorySideMargins = GetSideMargins();
                        VMprojectmaster.ival_LookupCategoryValuesbasicInfra = GetBasicInfra();
                        VMprojectmaster.ival_LookupCategoryValuesClassOfLocality = GetClassoflocality();


                        IEnumerable<ival_BuilderGroupMaster> ival_BuilderGroupMasters = GetBuilderMaster();
                        IEnumerable<ival_BuilderCompanyMaster> ival_BuilderCompanyMasters = GetCompanyBuilderMaster();
                        IEnumerable<ival_States> ival_StatesList = GetStates();
                        IEnumerable<ival_LookupCategoryValues> legalpincode = Getpincode();
                        IEnumerable<ival_LookupCategoryValues> GetProjectType = GetProjectTypeMaster();
                        IEnumerable<ival_LookupCategoryValues> Getsocialinfras = GetSocialInfra();
                        IEnumerable<ival_LookupCategoryValues> Getclassofloc = GetClassoflocality();



                        if (Getsocialinfras.ToList().Count > 0 && Getsocialinfras != null)
                        {
                            var socialinfra = Getsocialinfras.AsEnumerable().Where(row => row.Lookup_Value == dtprojects.Rows[0]["Social_Infrastructure"].ToString()).Select(row => row.Lookup_ID);
                            VMprojectmaster.SelectedModelsocialInfra = Convert.ToInt32(socialinfra.FirstOrDefault());

                        }


                        if (Getclassofloc.ToList().Count > 0 && Getclassofloc != null)
                        {
                            var classloc = Getclassofloc.AsEnumerable().Where(row => row.Lookup_Value == dtprojects.Rows[0]["Class_of_Locality"].ToString()).Select(row => row.Lookup_ID);
                            VMprojectmaster.SelectedModelclassoflocality = Convert.ToInt32(classloc.FirstOrDefault());

                        }

                        if (legalpincode.ToList().Count > 0 && legalpincode != null)
                        {
                            var legalpin = legalpincode.AsEnumerable().Where(row => row.Lookup_Value == dtprojects.Rows[0]["Legal_Pincode"].ToString()).Select(row => row.Lookup_ID);
                            VMprojectmaster.SelectedModelpincode = Convert.ToInt32(legalpin.FirstOrDefault());

                        }


                        if (ival_BuilderGroupMasters.ToList().Count > 0 && ival_BuilderGroupMasters != null)
                        {
                            var BuilderGroupMasters = ival_BuilderGroupMasters.Where(b => b.BuilderGroupName == dtprojects.Rows[0]["Builder_Group_ID"].ToString()).Select(b => b.Builder_Group_Master_ID);
                            VMprojectmaster.SelectedModelGroupmaster = Convert.ToInt32(BuilderGroupMasters.FirstOrDefault());
                        }

                        if (GetProjectType.ToList().Count > 0 && GetProjectType != null)
                        {
                            var GetProjectTypes = GetProjectType.Where(b => b.Lookup_Value == dtprojects.Rows[0]["P_TypeID"].ToString()).Select(b => b.Lookup_ID);
                            VMprojectmaster.SelectedModellookupprojecttype = Convert.ToInt32(GetProjectTypes.FirstOrDefault());
                        }



                        if (ival_BuilderCompanyMasters.ToList().Count > 0 && ival_BuilderCompanyMasters != null)
                        {
                            var BuilderCompanyMasters = ival_BuilderCompanyMasters.AsEnumerable().Where(b => b.Builder_Company_Name == dtprojects.Rows[0]["Builder_Company_ID"].ToString()).Select(b => b.Builder_Company_Master_ID);
                            VMprojectmaster.SelectedModelCompanyMaster = Convert.ToInt32(BuilderCompanyMasters.FirstOrDefault());

                        }



                        if (ival_StatesList.ToList().Count > 0 && ival_StatesList != null)
                        {
                            var legalState = ival_StatesList.AsEnumerable().Where(s => s.State_Name == dtprojects.Rows[0]["Legal_State"].ToString()).Select(s => s.State_ID);
                            var PostalState = ival_StatesList.AsEnumerable().Where(s => s.State_Name == dtprojects.Rows[0]["Postal_State"].ToString()).Select(s => s.State_ID);

                            VMprojectmaster.SelectedModelstates = Convert.ToInt32(legalState.FirstOrDefault());
                            VMprojectmaster.SelectedModelstates_postal = Convert.ToInt32(PostalState.FirstOrDefault());
                        }
                        VMprojectmaster.ival_ProjectMasters = objproject;

                    }



                    if (dtprojectsApproval.Rows.Count > 0 && dtprojectsApproval != null)
                    {
                        List<ival_ProjectApprovalDetails> Listapproval = new List<ival_ProjectApprovalDetails>();
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        // DataTable dtCompBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetStateByID");
                        for (int i = 0; i < dtprojectsApproval.Rows.Count; i++)
                        {

                            Listapproval.Add(new ival_ProjectApprovalDetails
                            {
                                Project_ID = Convert.ToInt32(dtprojectsApproval.Rows[i]["Project_ID"]),
                                Approval_Type = Convert.ToString(dtprojectsApproval.Rows[i]["Approval_Type"]),
                                Approving_Authority = Convert.ToString(dtprojectsApproval.Rows[i]["Approving_Authority"]),
                                Date_of_Approval = Convert.ToDateTime(dtprojectsApproval.Rows[i]["Date_of_Approval"]),
                                Approval_Num = Convert.ToString(dtprojectsApproval.Rows[i]["Approval_Num"]),
                            });


                        }
                        ViewBag.ival_PrjAppDet_Dy_List = Listapproval;

                    }

                    if (dtprojectsSidemargins.Rows.Count > 0 && dtprojectsSidemargins != null)
                    {
                        List<ival_ProjectSideMargin> ListSidemargins = new List<ival_ProjectSideMargin>();
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        // DataTable dtCompBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetStateByID");
                        for (int i = 0; i < dtprojectsSidemargins.Rows.Count; i++)
                        {

                            ListSidemargins.Add(new ival_ProjectSideMargin
                            {
                                Project_ID = Convert.ToInt32(dtprojectsSidemargins.Rows[i]["Project_ID"]),
                                Side_Margin_Description = Convert.ToString(dtprojectsSidemargins.Rows[i]["Side_Margin_Description"]),
                                As_per_Approved = Convert.ToString(dtprojectsSidemargins.Rows[i]["As_per_Approved"]),
                                As_per_Measurement = Convert.ToString(dtprojectsSidemargins.Rows[i]["As_per_Measurement"]),
                                Percentage_Deviation = Convert.ToString(dtprojectsSidemargins.Rows[i]["Percentage_Deviation"]),
                                //Deviation_Description = Convert.ToString(dtprojectsSidemargins.Rows[i]["Deviation_Description"]),
                            });


                        }
                        ViewBag.ival_Sidemargins = ListSidemargins;

                    }

                    if (dtprojectsBoundaries.Rows.Count > 0 && dtprojectsBoundaries != null)
                    {
                        List<ival_ProjectBoundaries> ListBondaries = new List<ival_ProjectBoundaries>();
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        // DataTable dtCompBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetStateByID");
                        for (int i = 0; i < dtprojectsBoundaries.Rows.Count; i++)
                        {

                            ListBondaries.Add(new ival_ProjectBoundaries
                            {
                                Project_ID = Convert.ToInt32(dtprojectsBoundaries.Rows[i]["Project_ID"]),
                                Boundry_Name = Convert.ToString(dtprojectsBoundaries.Rows[i]["Boundry_Name"]),
                                AsPer_Document = Convert.ToString(dtprojectsBoundaries.Rows[i]["AsPer_Document"]),
                                AsPer_Site = Convert.ToString(dtprojectsBoundaries.Rows[i]["AsPer_Site"]),
                            });
                        }
                        ViewBag.ival_Boundaries = ListBondaries;

                    }



                    if (dtprojectscheckunits.Rows.Count > 0 && dtprojectscheckunits != null)
                    {
                        List<ival_ProjectUnitDetails> Listcheckunits = new List<ival_ProjectUnitDetails>();
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        for (int i = 0; i < dtprojectscheckunits.Rows.Count; i++)
                        {
                            Listcheckunits.Add(new ival_ProjectUnitDetails
                            {
                                Project_ID = Convert.ToInt32(dtprojectscheckunits.Rows[i]["Project_ID"]),
                                UType_ID = Convert.ToString(dtprojectscheckunits.Rows[i]["UType_ID"]),

                            });
                        }
                        //test
                        ViewBag.ival_Prjchkunits = JsonConvert.SerializeObject(Listcheckunits);
                    }
                    //if (dtprojectscheckunits.Rows.Count > 0)
                    //{
                    //    var SelectedModellookupunittype="";
                    //    for (int i = 0; i < dtprojectscheckunits.Rows.Count; i++)
                    //    {
                    //        SelectedModellookupunittype = dtprojectscheckunits.Rows[i]["UType_ID"].ToString();
                    //    }
                    //    ViewBag.SelectedModellookupunittype= SelectedModellookupunittype;
                    //    //VMprojectmaster.selectedUnitType = ViewBag.SelectedModellookupunittype;
                    //}

                    if (dtbasicinfra.Rows.Count > 0 && dtbasicinfra != null)
                    {
                        List<ival_BasicInfraAvailability> Listcheckbsics = new List<ival_BasicInfraAvailability>();
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        for (int i = 0; i < dtbasicinfra.Rows.Count; i++)
                        {
                            Listcheckbsics.Add(new ival_BasicInfraAvailability
                            {
                                Project_ID = Convert.ToInt32(dtbasicinfra.Rows[i]["Project_ID"]),
                                BasicInfra_type = Convert.ToString(dtbasicinfra.Rows[i]["BasicInfra_type"]),

                            });
                        }
                        ViewBag.ival_Prjchkbasic = JsonConvert.SerializeObject(Listcheckbsics);
                    }

                    //if (dtbasicinfra.Rows.Count > 0)
                    //{
                    //    for (int i = 0; i < dtbasicinfra.Rows.Count; i++)
                    //    {
                    //        ViewBag.SelectedModelbasicinfra = dtbasicinfra.Rows[i]["BasicInfra_type"].ToString();
                    //    }
                    //    //VMprojectmaster.selectedUnitType = ViewBag.SelectedModellookupunittype;
                    //}

                    if (dtlocaltransport.Rows.Count > 0 && dtlocaltransport != null)
                    {
                        List<ival_LocalTransportForProject> Listchecklocal = new List<ival_LocalTransportForProject>();
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        for (int i = 0; i < dtlocaltransport.Rows.Count; i++)
                        {
                            Listchecklocal.Add(new ival_LocalTransportForProject
                            {
                                Project_ID = Convert.ToInt32(dtlocaltransport.Rows[i]["Project_ID"]),
                                LocalTransport_ID = Convert.ToString(dtlocaltransport.Rows[i]["LocalTransport_ID"]),

                            });
                        }
                        ViewBag.ival_Prjchklocal = JsonConvert.SerializeObject(Listchecklocal);
                    }

                    //if (dtlocaltransport.Rows.Count > 0)
                    //{
                    //    for (int i = 0; i < dtprojectscheckunits.Rows.Count; i++)
                    //    {
                    //        ViewBag.SelectedModelconnectivity = dtlocaltransport.Rows[i]["LocalTransport_ID"].ToString();
                    //    }//VMprojectmaster.selectedUnitType = ViewBag.SelectedModellookupunittype;
                    //}

                }
                catch (Exception e)
                {
                    e.InnerException.ToString();

                }
                return View(VMprojectmaster);
            }

        }

        //changes by punam EditPropertyDetails
        public ActionResult EditPropertydetailsViewNew()
        {
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            ViewModel vm1 = new ViewModel();
            // vm1.ival_States = GetStates();

            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            DataTable dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRetailPropertyByReqID", hInputPara);
            DataTable dtprojectid = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprjIDByRequestID", hInputPara);
            DataTable dtprojectID = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprjIDByRequestIDEdit", hInputPara);
            //changes by punam
            DataTable dtsocial = new DataTable();
            DataTable dtbasicinfra = new DataTable();
            DataTable dtlocaltransport = new DataTable();
            if (dtprojectID.Rows.Count > 0)
            {
                hInputPara1.Add("@Project_ID", dtprojectID.Rows[0]["Project_ID"].ToString());
                dtsocial = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectSocialDev", hInputPara1);
                dtbasicinfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectBasicInfraAvailability", hInputPara1);
                dtlocaltransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalTransport", hInputPara1);

            }

            if (dt.Rows.Count > 0 && dt.Columns.Count > 0)
            {
                if (DBNull.Value.Equals(dt.Rows[0]["RequestID"]))
                {
                    ViewBag.RequestID = Convert.ToInt32(!DBNull.Value.Equals(dt.Rows[0]["RequestID"]));
                }
                else
                {
                    ViewBag.RequestID = Convert.ToInt32(dt.Rows[0]["RequestID"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Builder_Group_ID"]))
                {
                    ViewBag.Builder_Group_ID = Convert.ToInt32(!DBNull.Value.Equals(dt.Rows[0]["Builder_Group_ID"]));

                }
                else
                {
                    ViewBag.Builder_Group_ID = Convert.ToInt32(dt.Rows[0]["Builder_Group_ID"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Builder_Company_ID"]))
                {
                    ViewBag.Builder_Company_ID = Convert.ToInt32(!DBNull.Value.Equals(dt.Rows[0]["Builder_Company_ID"]));

                }
                else
                {
                    ViewBag.Builder_Company_ID = Convert.ToInt32(dt.Rows[0]["Builder_Company_ID"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Is_Active"]))
                {
                    ViewBag.Is_Active = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Is_Active"]));

                }
                else
                {
                    ViewBag.Is_Active = Convert.ToString(dt.Rows[0]["Is_Active"]);


                }
                if (DBNull.Value.Equals(dt.Rows[0]["Ward_No"]))
                {
                    ViewBag.Ward_Number = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Ward_No"]));

                }
                else
                {
                    ViewBag.Ward_Number = Convert.ToString(dt.Rows[0]["Ward_No"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["Municipal_No"]))
                {
                    ViewBag.Municipal_No = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Municipal_No"]));

                }
                else
                {
                    ViewBag.Municipal_No = Convert.ToString(dt.Rows[0]["Municipal_No"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["Is_Retail_Individual"]))
                {
                    ViewBag.Is_Retail_Individual = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Is_Retail_Individual"]));

                }
                else
                {
                    ViewBag.Is_Retail_Individual = Convert.ToString(dt.Rows[0]["Is_Retail_Individual"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Property_Type"]))
                {
                    ViewBag.Property_Type = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Property_Type"]));

                }
                else
                {
                    ViewBag.Property_Type = Convert.ToString(dt.Rows[0]["Property_Type"]);
                    Session["Property_Type"] = ViewBag.Property_Type;
                }
                if (DBNull.Value.Equals(dt.Rows[0]["TypeOfSelect"]))
                {
                    ViewBag.TypeOfSelect = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["TypeOfSelect"]));

                }
                else
                {
                    ViewBag.TypeOfSelect = Convert.ToString(dt.Rows[0]["TypeOfSelect"]);
                    Session["TypeOfSelect"] = ViewBag.TypeOfSelect;

                }
                if (DBNull.Value.Equals(dt.Rows[0]["TypeOfUnit"]))
                {
                    ViewBag.TypeOfUnit = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["TypeOfUnit"]));
                }
                else
                {
                    ViewBag.TypeOfUnit = Convert.ToString(dt.Rows[0]["TypeOfUnit"]);
                    Session["TypeOfUnit"] = ViewBag.TypeOfUnit;
                }

                if (DBNull.Value.Equals(dt.Rows[0]["StatusOfProperty"]))
                {
                    ViewBag.StatusOfProperty = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["StatusOfProperty"]));

                }
                else
                {
                    ViewBag.StatusOfProperty = Convert.ToString(dt.Rows[0]["StatusOfProperty"]);
                    Session["StatusOfProperty"] = ViewBag.StatusOfProperty;
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Plot_No"]))
                {
                    ViewBag.Plot_Number = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Plot_No"]));

                }
                else
                {
                    ViewBag.Plot_Number = Convert.ToString(dt.Rows[0]["Plot_No"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Plot_Number"]))
                {
                    ViewBag.PlotBunglowNumber = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Plot_Number"]));

                }
                else
                {
                    ViewBag.PlotBunglowNumber = Convert.ToString(dt.Rows[0]["Plot_Number"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["noofwings"]))
                {
                    ViewBag.txtwingsno = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["noofwings"]));

                }
                else
                {
                    ViewBag.txtwingsno = Convert.ToString(dt.Rows[0]["noofwings"]);

                }


                if (DBNull.Value.Equals(dt.Rows[0]["Bunglow_Number"]))
                {
                    ViewBag.BunglowNumber = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Bunglow_Number"]));

                }
                else
                {
                    ViewBag.BunglowNumber = Convert.ToString(dt.Rows[0]["Bunglow_Number"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["UnitNumber"]))
                {
                    ViewBag.UnitNumber = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["UnitNumber"]));
                }
                else
                {
                    ViewBag.UnitNumber = Convert.ToString(dt.Rows[0]["UnitNumber"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Project_Name"]))
                {
                    ViewBag.Project_Name = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Project_Name"]));

                }
                else
                {
                    ViewBag.Project_Name = Convert.ToString(dt.Rows[0]["Project_Name"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Building_Name_RI"]))
                {
                    ViewBag.Building_Name_RI = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Building_Name_RI"]));

                }
                else
                {
                    ViewBag.Building_Name_RI = Convert.ToString(dt.Rows[0]["Building_Name_RI"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Wing_Name_RI"]))
                {
                    ViewBag.Wing_Name_RI = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Wing_Name_RI"]));
                }
                else
                {
                    ViewBag.Wing_Name_RI = Convert.ToString(dt.Rows[0]["Wing_Name_RI"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Survey_Number"]))
                {
                    ViewBag.Survey_Number = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Survey_Number"]));

                }
                else
                {
                    ViewBag.Survey_Number = Convert.ToString(dt.Rows[0]["Survey_Number"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Street"]))
                {
                    ViewBag.Street = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Street"]));
                }
                else
                {
                    ViewBag.Street = Convert.ToString(dt.Rows[0]["Street"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Locality"]))
                {

                    ViewBag.Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Locality"]));
                }
                else
                {

                    ViewBag.Locality = Convert.ToString(dt.Rows[0]["Locality"]);
                }


                if (DBNull.Value.Equals(dt.Rows[0]["City"]))
                {
                    ViewBag.City = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["City"]));
                }
                else
                {
                    ViewBag.City = Convert.ToString(dt.Rows[0]["City"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["District"]))
                {
                    ViewBag.District = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["District"]));
                }
                else
                {
                    ViewBag.District = Convert.ToString(dt.Rows[0]["District"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["State"]))
                {
                    //IEnumerable<ival_States> ival_States = GetStates();
                    //if (ival_States.ToList().Count > 0 && ival_States != null)
                    //{
                    //    var BuilderState = ival_States.Where(s => s.State_Name == dt.Rows[0]["State"].ToString()).Select(s => s.State_ID);

                    //    ViewBag.SelectedModelstates = Convert.ToInt32(BuilderState.FirstOrDefault());

                    //}

                    ViewBag.SelectedModelstates = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["State"]));
                }
                else
                {
                    ViewBag.SelectedModelstates = Convert.ToString(dt.Rows[0]["State"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["PinCode"]))
                {
                    ViewBag.SelectedModelpincode = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["PinCode"]));
                }
                else
                {
                    ViewBag.SelectedModelpincode = Convert.ToString(dt.Rows[0]["PinCode"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["NearBy_Landmark"]))
                {
                    ViewBag.NearBy_Landmark = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["NearBy_Landmark"]));

                }
                else
                {
                    ViewBag.NearBy_Landmark = Convert.ToString(dt.Rows[0]["NearBy_Landmark"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Nearest_RailwayStation"]))
                {
                    ViewBag.Nearest_RailwayStation = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Nearest_RailwayStation"]));

                }
                else
                {
                    ViewBag.Nearest_RailwayStation = Convert.ToString(dt.Rows[0]["Nearest_RailwayStation"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Nearest_BusStop"]))
                {
                    ViewBag.Nearest_BusStop = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Nearest_BusStop"]));
                }
                else
                {
                    ViewBag.Nearest_BusStop = Convert.ToString(dt.Rows[0]["Nearest_BusStop"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Nearest_Market"]))
                {
                    ViewBag.Nearest_Market = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Nearest_Market"]));
                }
                else
                {
                    ViewBag.Nearest_Market = Convert.ToString(dt.Rows[0]["Nearest_Market"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["Nearest_Hospital"]))
                {
                    ViewBag.Nearest_Hospital = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Nearest_Hospital"]));
                }
                else
                {
                    ViewBag.Nearest_Hospital = Convert.ToString(dt.Rows[0]["Nearest_Hospital"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Address1"]))
                {
                    ViewBag.Legal_Address1 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Address1"]));
                }
                else
                {
                    ViewBag.Legal_Address1 = Convert.ToString(dt.Rows[0]["Legal_Address1"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Address2"]))
                {
                    ViewBag.Legal_Address2 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Address2"]));
                }
                else
                {
                    ViewBag.Legal_Address2 = Convert.ToString(dt.Rows[0]["Legal_Address2"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Locality"]))
                {
                    ViewBag.Legal_Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Locality"]));
                }
                else
                {
                    ViewBag.Legal_Locality = Convert.ToString(dt.Rows[0]["Legal_Locality"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Street"]))
                {
                    ViewBag.Legal_Street = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Street"]));
                }
                else
                {
                    ViewBag.Legal_Street = Convert.ToString(dt.Rows[0]["Legal_Street"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_City"]))
                {
                    ViewBag.Legal_City = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_City"]));
                }
                else
                {
                    ViewBag.Legal_City = Convert.ToString(dt.Rows[0]["Legal_City"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_District"]))
                {
                    ViewBag.Legal_District = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_District"]));
                }
                else
                {
                    ViewBag.Legal_District = Convert.ToString(dt.Rows[0]["Legal_District"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_State"]))
                {
                    ViewBag.Legal_State = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_State"]));
                }
                else
                {
                    ViewBag.Legal_State = Convert.ToString(dt.Rows[0]["Legal_State"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Pincode"]))
                {
                    ViewBag.Legal_Pincode = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Pincode"]));
                }
                else
                {
                    ViewBag.Legal_Pincode = Convert.ToString(dt.Rows[0]["Legal_Pincode"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["RERA_Approval_Num"]))
                {
                    ViewBag.RERA_Approval_Num = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["RERA_Approval_Num"]));
                }
                else
                {
                    ViewBag.RERA_Approval_Num = Convert.ToString(dt.Rows[0]["RERA_Approval_Num"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approval_Flag"]))
                {
                    ViewBag.Approval_Flag = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approval_Flag"]));

                }
                else
                {
                    ViewBag.Approval_Flag = Convert.ToString(dt.Rows[0]["Approval_Flag"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Total_Land_Area"]))
                {
                    ViewBag.Total_Land_Area = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Total_Land_Area"]));

                }
                else
                {
                    ViewBag.Total_Land_Area = Convert.ToString(dt.Rows[0]["Total_Land_Area"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Total_Permissible_FSI"]))
                {
                    ViewBag.Total_Permissible_FSI = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Total_Permissible_FSI"]));

                }
                else
                {
                    ViewBag.Total_Permissible_FSI = Convert.ToString(dt.Rows[0]["Total_Permissible_FSI"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_Builtup_Area"]))
                {
                    ViewBag.Approved_Builtup_Area = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_Builtup_Area"]));

                }
                else
                {
                    ViewBag.Approved_Builtup_Area = Convert.ToString(dt.Rows[0]["Approved_Builtup_Area"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["Postal_Address1"]))
                {

                    ViewBag.Postal_Address1 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Address1"]));
                }
                else
                {
                    ViewBag.Postal_Address1 = Convert.ToString(dt.Rows[0]["Postal_Address1"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Buildings"]))
                {
                    ViewBag.Approved_No_of_Buildings = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Buildings"]));
                }
                else
                {
                    ViewBag.Approved_No_of_Buildings = Convert.ToString(dt.Rows[0]["Approved_No_of_Buildings"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_plotsorflats"]))
                {
                    ViewBag.Approved_No_of_plotsorflats = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_plotsorflats"]));
                }
                else
                {
                    ViewBag.Approved_No_of_plotsorflats = Convert.ToString(dt.Rows[0]["Approved_No_of_plotsorflats"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Wings"]))
                {
                    ViewBag.Approved_No_of_Wings = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Wings"]));
                }
                else
                {
                    ViewBag.Approved_No_of_Wings = Convert.ToString(dt.Rows[0]["Approved_No_of_Wings"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Floors"]))
                {

                    ViewBag.Approved_No_of_Floors = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Floors"]));
                }
                else
                {

                    ViewBag.Approved_No_of_Floors = Convert.ToString(dt.Rows[0]["Approved_No_of_Floors"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["Distance_from_Airport"]))
                {

                    ViewBag.Distance_from_Airport = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Distance_from_Airport"]));
                }
                else
                {

                    ViewBag.Distance_from_Airport = Convert.ToString(dt.Rows[0]["Distance_from_Airport"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["txtdistanceland"]))
                {

                    ViewBag.txtdistanceland = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["txtdistanceland"]));
                }
                else
                {

                    ViewBag.txtdistanceland = Convert.ToString(dt.Rows[0]["txtdistanceland"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["txtdistancerailway"]))
                {

                    ViewBag.txtdistancerailway = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["txtdistancerailway"]));
                }
                else
                {

                    ViewBag.txtdistancerailway = Convert.ToString(dt.Rows[0]["txtdistancerailway"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["txtdistancebus"]))
                {

                    ViewBag.txtdistancebus = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["txtdistancebus"]));
                }
                else
                {

                    ViewBag.txtdistancebus = Convert.ToString(dt.Rows[0]["txtdistancebus"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["txtdistancehospital"]))
                {

                    ViewBag.txtdistancehospital = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["txtdistancehospital"]));
                }
                else
                {

                    ViewBag.txtdistancehospital = Convert.ToString(dt.Rows[0]["txtdistancehospital"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["txtdistancemarket"]))
                {

                    ViewBag.txtdistancemarket = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["txtdistancemarket"]));
                }
                else
                {

                    ViewBag.txtdistancemarket = Convert.ToString(dt.Rows[0]["txtdistancemarket"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["txtdistanceairport"]))
                {

                    ViewBag.txtdistanceairport = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["txtdistanceairport"]));
                }
                else
                {

                    ViewBag.txtdistanceairport = Convert.ToString(dt.Rows[0]["txtdistanceairport"]);
                }



                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Units"]))
                {
                    ViewBag.Approved_No_of_Units = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Units"]));
                }
                else
                {
                    ViewBag.Approved_No_of_Units = Convert.ToString(dt.Rows[0]["Approved_No_of_Units"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Actual_No_Floors_G"]))
                {
                    ViewBag.Actual_No_Floors_G = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Actual_No_Floors_G"]));
                }
                else
                {
                    ViewBag.Actual_No_Floors_G = Convert.ToString(dt.Rows[0]["Actual_No_Floors_G"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Floor_Of_Unit"]))
                {
                    ViewBag.Floor_Of_Unit = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Floor_Of_Unit"]));
                }
                else
                {
                    ViewBag.Floor_Of_Unit = Convert.ToString(dt.Rows[0]["Floor_Of_Unit"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Property_Description"]))
                {
                    ViewBag.Property_Description = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Property_Description"]));
                }
                else
                {
                    ViewBag.Property_Description = Convert.ToString(dt.Rows[0]["Property_Description"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Type_Of_Structure"]))
                {
                    ViewBag.SelectedModelRetailTypeOfStructure = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Type_Of_Structure"]));
                }
                else
                {
                    ViewBag.SelectedModelRetailTypeOfStructure = Convert.ToString(dt.Rows[0]["Type_Of_Structure"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_Usage_Of_Property"]))
                {
                    ViewBag.SelectedApprovedusage = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_Usage_Of_Property"]));
                }
                else
                {
                    ViewBag.SelectedApprovedusage = Convert.ToString(dt.Rows[0]["Approved_Usage_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Actual_Usage_Of_Property"]))
                {
                    ViewBag.SelectedActualusage = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Actual_Usage_Of_Property"]));
                }
                else
                {
                    ViewBag.SelectedActualusage = Convert.ToString(dt.Rows[0]["Actual_Usage_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Current_Age_Of_Property"]))
                {
                    ViewBag.Current_Age_Of_Property = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Current_Age_Of_Property"]));
                }
                else
                {
                    ViewBag.Current_Age_Of_Property = Convert.ToString(dt.Rows[0]["Current_Age_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Residual_Age_Of_Property"]))
                {
                    ViewBag.Residual_Age_Of_Property = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Residual_Age_Of_Property"]));
                }
                else
                {
                    ViewBag.Residual_Age_Of_Property = Convert.ToString(dt.Rows[0]["Residual_Age_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Common_Areas"]))
                {
                    ViewBag.Common_Areas = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Common_Areas"]));
                }
                else
                {
                    ViewBag.Common_Areas = Convert.ToString(dt.Rows[0]["Common_Areas"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Facilities"]))
                {
                    ViewBag.Facilities = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Facilities"]));
                }
                else
                {
                    ViewBag.Facilities = Convert.ToString(dt.Rows[0]["Facilities"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Anyother_Observation"]))
                {
                    ViewBag.Anyother_Observation = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Anyother_Observation"]));
                }
                else
                {
                    ViewBag.Anyother_Observation = Convert.ToString(dt.Rows[0]["Anyother_Observation"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Roofing_Terracing"]))
                {
                    ViewBag.Roofing_Terracing = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Roofing_Terracing"]));
                }
                else
                {
                    ViewBag.Roofing_Terracing = Convert.ToString(dt.Rows[0]["Roofing_Terracing"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Quality_Of_Fixture"]))
                {
                    ViewBag.Quality_Of_Fixture = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Quality_Of_Fixture"]));
                }
                else
                {
                    ViewBag.Quality_Of_Fixture = Convert.ToString(dt.Rows[0]["Quality_Of_Fixture"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Quality_Of_Construction"]))
                {
                    ViewBag.SelectedModelRetailQualityOfConstruction = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Quality_Of_Construction"]));
                }
                else
                {
                    ViewBag.SelectedModelRetailQualityOfConstruction = Convert.ToString(dt.Rows[0]["Quality_Of_Construction"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Maintenance_Of_Property"]))
                {

                    ViewBag.SelectedModelRetailMaintenanceOfProp = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Maintenance_Of_Property"]));
                }
                else
                {

                    ViewBag.SelectedModelRetailMaintenanceOfProp = Convert.ToString(dt.Rows[0]["Maintenance_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Marketability_Of_Property"]))
                {
                    ViewBag.Marketability_Of_Property = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Marketability_Of_Property"]));
                }
                else
                {
                    ViewBag.Marketability_Of_Property = Convert.ToString(dt.Rows[0]["Marketability_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Occupancy_Details"]))
                {
                    ViewBag.SelectedModelRetailOccupancyDetails = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Occupancy_Details"]));
                }
                else
                {

                    ViewBag.SelectedModelRetailOccupancyDetails = Convert.ToString(dt.Rows[0]["Occupancy_Details"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Renting_Potential"]))
                {
                    ViewBag.Renting_Potential = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Renting_Potential"]));
                }
                else
                {
                    ViewBag.Renting_Potential = Convert.ToString(dt.Rows[0]["Renting_Potential"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Expected_Monthly_Rental"]))
                {
                    ViewBag.Expected_Monthly_Rental = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Expected_Monthly_Rental"]));
                }
                else
                {
                    ViewBag.Expected_Monthly_Rental = Convert.ToString(dt.Rows[0]["Expected_Monthly_Rental"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Name_Of_Occupant"]))
                {
                    ViewBag.Name_Of_Occupant = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Name_Of_Occupant"]));
                }
                else
                {
                    ViewBag.Name_Of_Occupant = Convert.ToString(dt.Rows[0]["Name_Of_Occupant"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Occupied_Since"]))
                {
                    ViewBag.Occupied_Since = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Occupied_Since"]));
                }
                else
                {
                    ViewBag.Occupied_Since = Convert.ToString(dt.Rows[0]["Occupied_Since"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Monthly_Rental"]))
                {
                    ViewBag.Monthly_Rental = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Monthly_Rental"]));
                }
                else
                {
                    ViewBag.Monthly_Rental = Convert.ToString(dt.Rows[0]["Monthly_Rental"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Balanced_Leased_Period"]))
                {
                    ViewBag.Balanced_Leased_Period = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Balanced_Leased_Period"]));
                }
                else
                {
                    ViewBag.Balanced_Leased_Period = Convert.ToString(dt.Rows[0]["Balanced_Leased_Period"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Specifications"]))
                {
                    ViewBag.Specifications = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Specifications"]));
                }
                else
                {
                    ViewBag.Specifications = Convert.ToString(dt.Rows[0]["Specifications"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Amenities"]))
                {
                    ViewBag.Amenities = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Amenities"]));
                }
                else
                {
                    ViewBag.Amenities = Convert.ToString(dt.Rows[0]["Amenities"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Deviation_Description"]))
                {
                    ViewBag.Deviation_Description = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Deviation_Description"]));

                }
                else
                {
                    ViewBag.Deviation_Description = Convert.ToString(dt.Rows[0]["Deviation_Description"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Cord_Latitude"]))
                {
                    ViewBag.Cord_Latitude = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Cord_Latitude"]));
                }
                else
                {
                    ViewBag.Cord_Latitude = Convert.ToString(dt.Rows[0]["Cord_Latitude"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Cord_Longitude"]))
                {
                    ViewBag.Cord_Longitude = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Cord_Longitude"]));
                }
                else
                {
                    ViewBag.Cord_Longitude = Convert.ToString(dt.Rows[0]["Cord_Longitude"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width1"]))
                {
                    ViewBag.AdjoiningRoad_Width1 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width1"]));
                }
                else
                {
                    ViewBag.AdjoiningRoad_Width1 = Convert.ToString(dt.Rows[0]["AdjoiningRoad_Width1"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width2"]))
                {
                    ViewBag.AdjoiningRoad_Width2 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width2"]));
                }
                else
                {
                    ViewBag.AdjoiningRoad_Width2 = Convert.ToString(dt.Rows[0]["AdjoiningRoad_Width2"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width3"]))
                {
                    ViewBag.AdjoiningRoad_Width3 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width3"]));
                }
                else
                {
                    ViewBag.AdjoiningRoad_Width3 = Convert.ToString(dt.Rows[0]["AdjoiningRoad_Width3"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width4"]))
                {
                    ViewBag.AdjoiningRoad_Width4 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width4"]));
                }
                else
                {
                    ViewBag.AdjoiningRoad_Width4 = Convert.ToString(dt.Rows[0]["AdjoiningRoad_Width4"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Social_Development"]))
                {
                    ViewBag.Social_Development = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Social_Development"]));
                }
                else
                {
                    ViewBag.Social_Development = Convert.ToString(dt.Rows[0]["Social_Development"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Social_Infrastructure"]))
                {
                    ViewBag.Social_Infrastructure = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Social_Infrastructure"]));
                }
                else
                {
                    ViewBag.Social_Infrastructure = Convert.ToString(dt.Rows[0]["Social_Infrastructure"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Type_of_Locality"]))
                {
                    ViewBag.SelectedModellookuptypeOfLocality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Type_of_Locality"]));
                }
                else
                {
                    ViewBag.SelectedModellookuptypeOfLocality = Convert.ToString(dt.Rows[0]["Type_of_Locality"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Class_of_Locality"]))
                {

                    ViewBag.SelectedModellookupclassOfLocality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Class_of_Locality"]));
                }
                else
                {

                    ViewBag.SelectedModellookupclassOfLocality = Convert.ToString(dt.Rows[0]["Class_of_Locality"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["RERA_Registration_No"]))
                {
                    ViewBag.RERA_Registration_No = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["RERA_Registration_No"]));
                }
                else
                {
                    ViewBag.RERA_Registration_No = Convert.ToString(dt.Rows[0]["RERA_Registration_No"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["DistFrom_City_Center"]))
                {
                    ViewBag.DistFrom_City_Center = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["DistFrom_City_Center"]));

                }
                else
                {
                    ViewBag.DistFrom_City_Center = Convert.ToString(dt.Rows[0]["DistFrom_City_Center"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Locality_Remarks"]))
                {
                    ViewBag.Locality_Remarks = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Locality_Remarks"]));

                }
                else
                {
                    ViewBag.Locality_Remarks = Convert.ToString(dt.Rows[0]["Locality_Remarks"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["Village_Name"]))
                {
                    ViewBag.Village_Name = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Village_Name"]));

                }
                else
                {
                    ViewBag.Village_Name = Convert.ToString(dt.Rows[0]["Village_Name"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["Sub_Locality"]))
                {
                    ViewBag.Sub_Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Sub_Locality"]));

                }
                else
                {
                    ViewBag.Sub_Locality = Convert.ToString(dt.Rows[0]["Sub_Locality"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["type_ownership"]))
                {
                    ViewBag.ownership = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["type_ownership"]));

                }
                else
                {
                    ViewBag.ownership = Convert.ToString(dt.Rows[0]["type_ownership"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["leased_year"]))
                {
                    ViewBag.lease = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["leased_year"]));

                }
                else
                {
                    ViewBag.lease = Convert.ToString(dt.Rows[0]["leased_year"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Unit_on_floor"]))
                {
                    ViewBag.FloorUnit = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Unit_on_floor"]));

                }
                else
                {
                    ViewBag.FloorUnit = Convert.ToString(dt.Rows[0]["Unit_on_floor"]);

                }

                //ViewBag.Postal_Address2 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Address2"]));
                //ViewBag.Postal_Street = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Street"]));
                //ViewBag.Postal_Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Locality"]));
                //ViewBag.Postal_City = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_City"]));
                //ViewBag.Postal_District = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_District"]));
                //ViewBag.Postal_State = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_State"]));
                //ViewBag.Postal_Pincode = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Pincode"]));
                //ViewBag.Postal_NearbyLandmark = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_NearbyLandmark"]));






            }
            else
            {

            }
            ViewModel vm = new ViewModel();
            vm.ival_StatesList = GetState();
            vm.PincodeList = GetPincode();

            if (dt.Rows.Count > 0)
            {
                vm.ival_DistrictsList = GetDistrictN(dt.Rows[0]["State"].ToString());
                vm.ival_CitiesList = GetCityN(dt.Rows[0]["District"].ToString());
                vm.ival_pincodelist = GetLocW(dt.Rows[0]["Locality"].ToString());
                vm.ival_ProjectBuildingWingFloorDetails = GetFloorListbyreqID();

                vm.ival_LocalityList = GetLocN(dt.Rows[0]["District"].ToString());
                vm.ival_subLocalityList = GetSubLocN(dt.Rows[0]["Locality"].ToString());
            }
            else
            {
                vm.ival_ProjectBuildingWingFloorDetails = GetFloorListbyreqID();

                vm.ival_StatesList = GetState();
                vm.PincodeList = GetPincode();

                vm.ival_DistrictsList = GetDistrict();
                vm.ival_CitiesList = GetCity();
                vm.ival_pincodelist = GetLoc();
                vm.ival_LocalityList = GetLoc();
                vm.ival_subLocalityList = GetSubLoc();
            }
            vm.ival_States = GetStates();
            vm.ival_LookupCategoryValuespincode = Getpincode();
            vm.ival_LookupCategoryValuesTypeOfStrycture = GetTypeOfStructure();
            vm.ival_LookupCategoryValuesQualityOfConstruction = GetQualityOfConstruction();
            vm.ival_LookupCategoryValuesMaintenanceOfproperty = GetMaintenanceOfProperty();
            vm.ival_LookupCategoryValuesOccupancyDetails = GetOccupancyDetails();
            vm.ival_LookupCategoryValuesClassOfLocality = GetClassoflocality();
            vm.ival_LookupCategoryValuesTypeOfLocality = GetTypeOfLocality();
            vm.ival_LookupCategoryValuesProjecttype = GetProjectTypeMaster();
            vm.ival_LookupCategoryValuesrentingPotential = GetRentingPotential();
            vm.ival_LookupCategoryValuesSocialDev = GetSocialDevelopment();
            try
            {
                IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesrentingPotential = GetRentingPotential();
                if (ival_LookupCategoryValuesrentingPotential.ToList().Count > 0 && ival_LookupCategoryValuesrentingPotential != null)
                {
                    var RentingPotential = ival_LookupCategoryValuesrentingPotential.Where(s => s.Lookup_Value == dt.Rows[0]["Renting_Potential"].ToString()).Select(s => s.Lookup_ID);

                    vm.SelectedModelRetailRentingPotential = Convert.ToInt32(RentingPotential.FirstOrDefault());

                }

                //get Id For Binding State,District,City,pincode
                IEnumerable<ival_States> ival_States = GetStates();
                if (ival_States.ToList().Count > 0 && ival_States != null)
                {
                    var BuilderState = ival_States.Where(s => s.State_Name == dt.Rows[0]["State"].ToString()).Select(s => s.State_ID);

                    vm.selectedStateID = Convert.ToInt32(BuilderState.FirstOrDefault());

                }

                IEnumerable<ival_Cities> ival_LocalityList = GetLoc();
                if (ival_LocalityList.ToList().Count > 0 && ival_LocalityList != null)
                {
                    var BuilderDistrict1 = ival_LocalityList.Where(s => s.Loc_Name == dt.Rows[0]["Locality"].ToString()).Select(s => s.Loc_ID);

                    vm.selectedLocID = Convert.ToInt32(BuilderDistrict1.FirstOrDefault());

                }
                IEnumerable<ival_Cities> ival_SubLocalityList = GetSubLoc();
                if (ival_SubLocalityList.ToList().Count > 0 && ival_SubLocalityList != null)
                {
                    var BuilderDistrict = ival_SubLocalityList.Where(s => s.SubLoc_Name == dt.Rows[0]["Sub_Locality"].ToString()).Select(s => s.SubLoc_ID);

                    vm.selectedSLocID = Convert.ToInt32(BuilderDistrict.FirstOrDefault());

                }

                IEnumerable<ival_Cities> ival_pincodelist = GetLoc();
                if (ival_pincodelist.ToList().Count > 0 && ival_pincodelist != null)
                {
                    var builderpostalpincode = ival_pincodelist.Where(s => s.Pin_Code == dt.Rows[0]["PinCode"].ToString()).Select(s => s.Pin_Code);

                    vm.selectedpinID = Convert.ToInt32(builderpostalpincode.FirstOrDefault());

                }
                IEnumerable<ival_Districts> ival_DistrictsList = GetDistrict();
                if (ival_DistrictsList.ToList().Count > 0 && ival_DistrictsList != null)
                {
                    var BuilderDistrict = ival_DistrictsList.Where(s => s.District_Name == dt.Rows[0]["District"].ToString()).Select(s => s.District_ID);

                    vm.selectedDistID = Convert.ToInt32(BuilderDistrict.FirstOrDefault());

                }
                IEnumerable<ival_Cities> ival_CitiesList = GetCity();
                if (ival_CitiesList.ToList().Count > 0 && ival_CitiesList != null)
                {
                    var BuilderCity = ival_CitiesList.Where(s => s.City_Name == dt.Rows[0]["City"].ToString()).Select(s => s.City_ID);

                    vm.selectedCityID = Convert.ToInt32(BuilderCity.FirstOrDefault());

                }

            }
            catch (Exception)
            {

            }

            if (dtprojectID.Rows.Count > 0)
            {
                int projectId = Convert.ToInt32(dtprojectID.Rows[0]["Project_ID"]);
                vm.ival_ProjectUnitDetails = GetProjectUnitDetails(projectId);
                vm.ival_LookupCategoryValuesTransaportEdit = GetProjectLocalTransportDetails(projectId);
                vm.ival_LookupCategoryValuesBasicInfraEdit = GetProjectBasicInfraDetails(projectId);  //GetCoomaDetails
                vm.ival_commonarea = GetCoomaDetails(projectId);
                vm.ival_commonarea2 = GetCoomaDetails2(projectId);
                vm.ival_commonarea3 = GetCoomaDetails3(projectId);
                vm.ival_ProjectBuildingWingFloorDetails = GetProjectFloorWingDetails(projectId);
                foreach (var a in vm.ival_ProjectBuildingWingFloorDetails)
                {
                    if (a.FloorType_ID == "Podium")
                    {
                        ViewBag.pocheck = a.Project_ID;
                        ViewBag.podium = a.Number_Of_Floors;
                    }
                    if (a.FloorType_ID == "Upper Habitable Floors")
                    {
                        ViewBag.upcheck = a.Project_ID;
                        ViewBag.upper = a.Number_Of_Floors;
                    }
                    if (a.FloorType_ID == "Basement")
                    {
                        ViewBag.BaseCheck = a.Project_ID;
                        ViewBag.basement = a.Number_Of_Floors;
                    }
                    if (a.FloorType_ID == "Ground")
                    {
                        ViewBag.GroCheck = a.Project_ID;
                        ViewBag.Ground = a.Number_Of_Floors;
                    }
                    if (a.FloorType_ID == "Stilt")
                    {
                        ViewBag.stiltCheck = a.Project_ID;
                        ViewBag.Stilt = a.Number_Of_Floors;
                    }
                    if (a.FloorType_ID == "second floor")
                    {
                        ViewBag.secCheck = a.Project_ID;
                        ViewBag.secondfloor = a.Number_Of_Floors;
                    }

                }
            }



            vm.ival_LookupCategoryValuesconnectivity = GetConnectivity();

            vm.ival_LookupCategoryValuesbasicInfra = GetBasicInfra();

            vm.ival_CommonAreaI = GetCommonAreas();

            vm.ival_CommonAreaI2 = GetCommonAreas2();
            vm.ival_CommonAreaI3 = GetCommonAreas3();
            vm.ival_LookupCategoryValuesFloorType = GetFloorsType();

            if (dtsocial.Rows.Count > 0 && dtsocial != null)
            {
                List<ival_SocialDevelopmentForProject> Listsocial = new List<ival_SocialDevelopmentForProject>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                for (int i = 0; i < dtsocial.Rows.Count; i++)
                {
                    Listsocial.Add(new ival_SocialDevelopmentForProject
                    {
                        Project_ID = Convert.ToInt32(dtsocial.Rows[i]["Project_ID"]),
                        Social_Dev_ID = Convert.ToInt32(dtsocial.Rows[i]["Social_Dev_ID"]),
                        Social_Development_Type = Convert.ToString(dtsocial.Rows[i]["Social_Development_Type"]),


                    });
                }
                //test
                ViewBag.Listsocial = JsonConvert.SerializeObject(Listsocial);
            }
            if (dtlocaltransport.Rows.Count > 0 && dtlocaltransport != null)
            {
                List<ival_LocalTransportForProject> Listlocal = new List<ival_LocalTransportForProject>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                for (int i = 0; i < dtlocaltransport.Rows.Count; i++)
                {
                    Listlocal.Add(new ival_LocalTransportForProject
                    {
                        Project_ID = Convert.ToInt32(dtlocaltransport.Rows[i]["Project_ID"]),
                        ProjectLocalTransport_ID = Convert.ToInt32(dtlocaltransport.Rows[i]["ProjectLocalTransport_ID"]),
                        LocalTransport_ID = Convert.ToString(dtlocaltransport.Rows[i]["LocalTransport_ID"]),


                    });
                }
                //test
                ViewBag.Listlocaltransport = JsonConvert.SerializeObject(Listlocal);
            }

            if (dtbasicinfra.Rows.Count > 0 && dtbasicinfra != null)
            {
                List<ival_BasicInfraAvailability> Listbinfra = new List<ival_BasicInfraAvailability>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                for (int i = 0; i < dtbasicinfra.Rows.Count; i++)
                {
                    Listbinfra.Add(new ival_BasicInfraAvailability
                    {
                        Project_ID = Convert.ToInt32(dtbasicinfra.Rows[i]["Project_ID"]),
                        BasicInfra_type = Convert.ToString(dtbasicinfra.Rows[i]["BasicInfra_type"]),
                        ProjectInfra_ID = Convert.ToInt32(dtbasicinfra.Rows[i]["ProjectInfra_ID"]),


                    });
                }
                //test
                ViewBag.Listbasicinfra = JsonConvert.SerializeObject(Listbinfra);
            }



            return View(vm);
        }



        [HttpPost]
        public List<ival_LookupCategoryValues> GetTypeofOwnership()
        {
            List<ival_LookupCategoryValues> Listtypeofowner = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dttypeofowner = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTypeOfOwnership");
            for (int i = 0; i < dttypeofowner.Rows.Count; i++)
            {

                Listtypeofowner.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dttypeofowner.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dttypeofowner.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dttypeofowner.Rows[i]["Lookup_Value"])

                });

            }
            return Listtypeofowner;
        }
        //UpdatePropertyDetailsNew method added by punam 
        [HttpPost]
        public ActionResult UpdatePropertyDetailsNew(string str, string str4, string stringBasicInfra, string stringSocialDev, string stringtblCommonArea1, string stringtblCommonArea2, string stringtblCommonArea3, string stringfloor, string stringupper, string str3, string strground, string str5, string str6, string UncheckedValue, string podium, string UpperHabitableFloors)
        {
            try
            {
                hInputPara = new Hashtable();
                hInputPara1 = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                DataTable list4 = (DataTable)JsonConvert.DeserializeObject(str4, (typeof(DataTable)));
                DataTable listBasic = (DataTable)JsonConvert.DeserializeObject(stringBasicInfra, (typeof(DataTable)));
                DataTable listSocialDEv = (DataTable)JsonConvert.DeserializeObject(stringSocialDev, (typeof(DataTable)));
                DataTable listCommonArea1 = (DataTable)JsonConvert.DeserializeObject(stringtblCommonArea1, (typeof(DataTable)));
                DataTable listCommonArea2 = (DataTable)JsonConvert.DeserializeObject(stringtblCommonArea2, (typeof(DataTable)));
                DataTable listCommonArea3 = (DataTable)JsonConvert.DeserializeObject(stringtblCommonArea3, (typeof(DataTable)));
                DataTable listfloor = (DataTable)JsonConvert.DeserializeObject(stringfloor, (typeof(DataTable)));
                DataTable listupper = (DataTable)JsonConvert.DeserializeObject(stringupper, (typeof(DataTable)));
                DataTable listBasement = (DataTable)JsonConvert.DeserializeObject(str3, (typeof(DataTable)));
                DataTable listG = (DataTable)JsonConvert.DeserializeObject(strground, (typeof(DataTable)));
                DataTable listS = (DataTable)JsonConvert.DeserializeObject(str5, (typeof(DataTable)));
                DataTable listsecond = (DataTable)JsonConvert.DeserializeObject(str6, (typeof(DataTable)));
                DataTable listUncheck = (DataTable)JsonConvert.DeserializeObject(UncheckedValue, (typeof(DataTable)));
                int RequestID = Convert.ToInt32(Session["RequestID"]);
                hInputPara1.Add("@Request_ID", RequestID);
                DataTable dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRetailPropertyByReqID", hInputPara1);
                DataTable dtprojectid = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprjIDByRequestID", hInputPara1);
                DataTable dtprojectID = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprjIDByRequestIDEdit", hInputPara1);
                for (int i = 0; i < list1.Rows.Count; i++)
                {

                    hInputPara.Add("@RequestID", RequestID);

                    hInputPara.Add("@PlotBunglowNumber", "");
                    hInputPara.Add("@BunglowNumber", Convert.ToString(list1.Rows[i]["BunglowNumber"]));
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@Survey_Number", Convert.ToString(list1.Rows[i]["SurveyNumberSingle"]));
                    }
                    else
                    {
                        hInputPara.Add("@Survey_Number", Convert.ToString(list1.Rows[i]["SurveyNumber"]));
                    }

                    hInputPara.Add("@UnitNumber", Convert.ToString(list1.Rows[i]["UnitNumber"]));
                    hInputPara.Add("@Project_Name", Convert.ToString(list1.Rows[i]["ProjectName"]));
                    hInputPara.Add("@Building_Name_RI", Convert.ToString(list1.Rows[i]["BuildingName"]));
                    hInputPara.Add("@Wing_Name_RI", Convert.ToString(list1.Rows[i]["WingName"]));
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@Village_Name", Convert.ToString(list1.Rows[i]["VillageNameSingle"]));
                    }
                    else
                    {
                        hInputPara.Add("@Village_Name", Convert.ToString(list1.Rows[i]["VillageName"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@Street", Convert.ToString(list1.Rows[i]["StreetSingle"]));
                    }
                    else
                    {
                        hInputPara.Add("@Street", Convert.ToString(list1.Rows[i]["Street"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@Locality", Convert.ToString(list1.Rows[i]["LocalitySingle"]));
                    }
                    else
                    {
                        hInputPara.Add("@Locality", Convert.ToString(list1.Rows[i]["Locality"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@Sub_Locality", Convert.ToString(list1.Rows[i]["SubLocalitySingleP"]));
                    }
                    else
                    {
                        hInputPara.Add("@Sub_Locality", Convert.ToString(list1.Rows[i]["SubLocality"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@City", Convert.ToString(list1.Rows[i]["CitySingle"]));
                    }
                    else
                    {
                        hInputPara.Add("@City", Convert.ToString(list1.Rows[i]["City"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@District", Convert.ToString(list1.Rows[i]["DistrictSingle"]));
                    }
                    else
                    {
                        hInputPara.Add("@District", Convert.ToString(list1.Rows[i]["District"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@State", Convert.ToString(list1.Rows[i]["StateSingle"]));
                    }
                    else
                    {
                        hInputPara.Add("@State", Convert.ToString(list1.Rows[i]["State"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@PinCode", Convert.ToString(list1.Rows[i]["PincodeSingle"]));
                    }
                    else
                    {
                        hInputPara.Add("@PinCode", Convert.ToString(list1.Rows[i]["Pincode"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@NearBy_Landmark", list1.Rows[i]["NearByLandmarkPlot"].ToString());
                    }
                    else
                    {
                        hInputPara.Add("@NearBy_Landmark", list1.Rows[i]["NearByLandmark"].ToString());
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Nearest_RailwayStation", list1.Rows[i]["NearRailwayStationPlot"].ToString());
                    }
                    else
                    {
                        hInputPara.Add("@Nearest_RailwayStation", list1.Rows[i]["NearRailwayStation"].ToString());
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Nearest_BusStop", list1.Rows[i]["NearBusStopPlot"].ToString());
                    }
                    else
                    {
                        hInputPara.Add("@Nearest_BusStop", list1.Rows[i]["NearBusStop"].ToString());
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Nearest_Hospital", list1.Rows[i]["NearHospitalPlot"].ToString());
                    }
                    else
                    {
                        hInputPara.Add("@Nearest_Hospital", list1.Rows[i]["NearHospital"].ToString());
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Nearest_Market", list1.Rows[i]["NearMarketPlot"].ToString());
                    }
                    else
                    {
                        hInputPara.Add("@Nearest_Market", list1.Rows[i]["NearMarket"].ToString());
                    }
                    hInputPara.Add("@RERA_Approval_Num", Convert.ToString(list1.Rows[i]["RegistrationNumber"]));
                    hInputPara.Add("@Approval_Flag", Convert.ToString(list1.Rows[i]["ApprovalFlag"]));
                    hInputPara.Add("@Actual_No_Floors_G", Convert.ToString(list1.Rows[i]["ActualNumberOfFloars"]));
                    hInputPara.Add("@Approved_No_of_Floors", Convert.ToString(list1.Rows[i]["txtapprovednooffloors"]));
                    hInputPara.Add("@Distance_from_Airport", Convert.ToString(list1.Rows[i]["txtdisAirport"]));
                    hInputPara.Add("@txtdistanceland", Convert.ToString(list1.Rows[i]["txtdistanceland"]));
                    hInputPara.Add("@txtdistancerailway", Convert.ToString(list1.Rows[i]["txtdistancerailway"]));
                    hInputPara.Add("@txtdistancebus", Convert.ToString(list1.Rows[i]["txtdistancebus"]));
                    hInputPara.Add("@txtdistancehospital", Convert.ToString(list1.Rows[i]["txtdistancehospital"]));
                    hInputPara.Add("@txtdistancemarket", Convert.ToString(list1.Rows[i]["txtdistancemarket"]));
                    hInputPara.Add("@txtdistanceairport", Convert.ToString(list1.Rows[i]["txtdistanceairport"]));
                   

                    hInputPara.Add("@Floor_Of_Unit", Convert.ToString(list1.Rows[i]["FloorOfUnits"]));
                    hInputPara.Add("@Property_Description", Convert.ToString(list1.Rows[i]["PropertyDescription"]));
                    hInputPara.Add("@Type_Of_Structure", Convert.ToString(list1.Rows[i]["TypeOfStructure"]));

                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Approved_Usage_Of_Property", list1.Rows[i]["ApprovedUsageOfPropertyPlot"].ToString());
                    }
                    else
                    {
                        hInputPara.Add("@Approved_Usage_Of_Property", list1.Rows[i]["ApprovedUsageOfProperty"].ToString());
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Actual_Usage_Of_Property", list1.Rows[i]["ActualUsageOfPropertyPlot"].ToString());
                    }
                    else
                    {
                        hInputPara.Add("@Actual_Usage_Of_Property", list1.Rows[i]["ActualUsageOfProperty"].ToString());
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Current_Age_Of_Property", Convert.ToString(list1.Rows[i]["CurrentAgePropertyPlot"]));
                    }
                    else
                    {
                        hInputPara.Add("@Current_Age_Of_Property", Convert.ToString(list1.Rows[i]["CurrentAgeProperty"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Residual_Age_Of_Property", Convert.ToString(list1.Rows[i]["ResidualAgePropertyPlot"]));
                    }
                    else
                    {
                        hInputPara.Add("@Residual_Age_Of_Property", Convert.ToString(list1.Rows[i]["ResidualAgeProperty"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Common_Areas", Convert.ToString(list1.Rows[i]["CommonAreaPlot"]));
                    }
                    else
                    {
                        hInputPara.Add("@Common_Areas", Convert.ToString(list1.Rows[i]["CommonArea"]));
                    }
                    hInputPara.Add("@Facilities", Convert.ToString(list1.Rows[i]["Facilities"]));
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Anyother_Observation", Convert.ToString(list1.Rows[i]["AnyOtherObservationPlot"]));
                    }
                    else
                    {
                        hInputPara.Add("@Anyother_Observation", Convert.ToString(list1.Rows[i]["AnyOtherObservation"]));
                    }
                    hInputPara.Add("@Roofing_Terracing", Convert.ToString(list1.Rows[i]["RoofingandTerrecing"]));
                    hInputPara.Add("@Quality_Of_Fixture", Convert.ToString(list1.Rows[i]["QualityOfFixture"]));
                    hInputPara.Add("@Quality_Of_Construction", Convert.ToString(list1.Rows[i]["QualityOfConstruction"]));
                    hInputPara.Add("@Maintenance_Of_Property", Convert.ToString(list1.Rows[i]["MaintenceOfProperty"]));
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@Marketability_Of_Property", Convert.ToString(list1.Rows[i]["MarketabilityOfPropertyPlot"]));
                    }
                    else
                    {
                        hInputPara.Add("@Marketability_Of_Property", Convert.ToString(list1.Rows[i]["MarketabilityOfProperty"]));
                    }
                    hInputPara.Add("@Occupancy_Details", Convert.ToString(list1.Rows[i]["OccupancyDetail"]));
                    hInputPara.Add("@Renting_Potential", Convert.ToString(list1.Rows[i]["RenetingPotential"]));
                    hInputPara.Add("@Expected_Monthly_Rental", Convert.ToString(list1.Rows[i]["ExpectedMonthlyRental"]));
                    hInputPara.Add("@Occupied_Since", Convert.ToString(list1.Rows[i]["OccupiedSince"]));
                    hInputPara.Add("@Monthly_Rental", Convert.ToString(list1.Rows[i]["MonthlyRental"]));
                    hInputPara.Add("@Balanced_Leased_Period", Convert.ToString(list1.Rows[i]["BalancedLeasedPeriod"]));
                    hInputPara.Add("@Class_of_Locality", Convert.ToString(list1.Rows[i]["ClassOfLocality"]));
                    hInputPara.Add("@Type_of_Locality", Convert.ToString(list1.Rows[i]["TypeOfLocality"]));
                    hInputPara.Add("@DistFrom_City_Center", Convert.ToString(list1.Rows[i]["DistanceFromCityCenter"]));
                    hInputPara.Add("@Locality_Remarks", Convert.ToString(list1.Rows[i]["Remark"]));


                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot" || Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@mun_no", Convert.ToString(list1.Rows[i]["txtmunicipalnop"]));

                        hInputPara.Add("@ward_no", Convert.ToString(list1.Rows[i]["txtwardnop"]));
                        hInputPara.Add("@plot_no", Convert.ToString(list1.Rows[i]["PlotNumber"]));

                    }
                    else
                    {
                        hInputPara.Add("@mun_no", Convert.ToString(list1.Rows[i]["txtmunino"]));

                        hInputPara.Add("@ward_no", Convert.ToString(list1.Rows[i]["txtwardno"]));
                        hInputPara.Add("@plot_no", Convert.ToString(list1.Rows[i]["txtplotno"]));

                    }

                    //hInputPara.Add("@mun_no", Convert.ToString(list1.Rows[i]["txtmunino"]));
                    hInputPara.Add("@unitfloor", Convert.ToString(list1.Rows[i]["txtunitonfloor"]));
                    hInputPara.Add("@txtwingsno", Convert.ToString(list1.Rows[i]["txtwingsno"]));
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Bungalow")
                    {
                        hInputPara.Add("@townership", Convert.ToString(list1.Rows[i]["txtowner"]));
                        hInputPara.Add("@leasedyear", Convert.ToString(list1.Rows[i]["txtyear"]));
                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) == "Plot")
                    {
                        hInputPara.Add("@townership", Convert.ToString(list1.Rows[i]["txtownerp"]));
                        hInputPara.Add("@leasedyear", Convert.ToString(list1.Rows[i]["txtyearp"]));


                    }
                    if (Convert.ToString(list1.Rows[i]["Unittype"]) != "Bungalow" && Convert.ToString(list1.Rows[i]["Unittype"]) != "Plot")
                    {
                        hInputPara.Add("@townership", Convert.ToString(list1.Rows[i]["txtowner"]));
                        hInputPara.Add("@leasedyear", Convert.ToString(list1.Rows[i]["txtyear"]));
                    }
                    sqlDataAccess.ExecuteStoreProcedure("usp_UpdatePropertyDetailsNew", hInputPara);

                    hInputPara.Clear();
                    hInputPara1.Clear();

                }

                for (int i4 = 0; i4 < listSocialDEv.Rows.Count; i4++)
                {

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dtprojectID.Rows[0]["Project_ID"]));
                    hInputPara.Add("@socialDevelomentType", Convert.ToString(listSocialDEv.Rows[i4]["value"]));
                    hInputPara.Add("@Updated_By", "Branch Head");
                    var res4 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertSocialDevelopmentEdit", hInputPara);
                    hInputPara.Clear();
                }

                for (int i44 = 0; i44 < listCommonArea1.Rows.Count; i44++)
                {

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dtprojectID.Rows[0]["Project_ID"]));
                    hInputPara.Add("@socialDevelomentType", Convert.ToString(listCommonArea1.Rows[i44]["value"]));
                    hInputPara.Add("@Updated_By", "Branch Head");
                    var res4 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertCommonEdit", hInputPara);
                    hInputPara.Clear();
                }


                for (int i44 = 0; i44 < listCommonArea2.Rows.Count; i44++)
                {

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dtprojectID.Rows[0]["Project_ID"]));
                    hInputPara.Add("@socialDevelomentType", Convert.ToString(listCommonArea2.Rows[i44]["value"]));
                    hInputPara.Add("@Updated_By", "Branch Head");
                    var res4 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertfacilitiesEdit", hInputPara);
                    hInputPara.Clear();
                }


                for (int i44 = 0; i44 < listCommonArea3.Rows.Count; i44++)
                {

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dtprojectID.Rows[0]["Project_ID"]));
                    hInputPara.Add("@socialDevelomentType", Convert.ToString(listCommonArea3.Rows[i44]["value"]));
                    hInputPara.Add("@Updated_By", "Branch Head");
                    var res4 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertroofingEdit", hInputPara);
                    hInputPara.Clear();
                }


                for (int i4 = 0; i4 < list4.Rows.Count; i4++)
                {
                    //DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1ProjectIdFromProjectMasterNew");

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dtprojectID.Rows[0]["Project_ID"]));
                    hInputPara.Add("@LocalTransport_ID", Convert.ToString(list4.Rows[i4]["value"]));
                    hInputPara.Add("@Updated_By", "Branch Head");
                    var res4 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertLocaltrnasportEdit", hInputPara);
                    hInputPara.Clear();
                }
                for (int i4 = 0; i4 < listBasic.Rows.Count; i4++)
                {
                    // DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1ProjectIdFromProjectMasterNew");

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dtprojectID.Rows[0]["Project_ID"]));
                    hInputPara.Add("@BasicInfra_type", Convert.ToString(listBasic.Rows[i4]["value1"]));
                    // hInputPara.Add("@Updated_By", "Branch Head");
                    var res5 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertBasicInfraEdit", hInputPara);
                    hInputPara.Clear();
                }
                if (listUncheck.Rows.Count > 0)
                {

                    for (int i = 0; i < listUncheck.Rows.Count; i++)
                    {
                        hInputPara.Add("@ProjectId", Convert.ToString(dtprojectID.Rows[0]["Project_ID"]));
                        string Utype = listUncheck.Rows[i]["value3"].ToString();
                        hInputPara.Add("@SocialDevelopmentType", Utype);
                        string result = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteProjectUnitTypesEdit", hInputPara);
                        hInputPara.Clear();
                        hInputPara.Add("@ProjectId", Convert.ToInt32(dtprojectID.Rows[0]["Project_ID"]));
                        hInputPara.Add("@LocalTransport_ID", Utype);

                        sqlDataAccess.ExecuteStoreProcedure("usp_DeleteProjectLocalTransportEdit", hInputPara);

                        hInputPara.Clear();
                        hInputPara.Add("@ProjectId", Convert.ToInt32(dtprojectID.Rows[0]["Project_ID"]));
                        hInputPara.Add("@BasicInfra_type", Utype);

                        sqlDataAccess.ExecuteStoreProcedure("usp_DeleteProjectBasicInfraEdit", hInputPara);
                        hInputPara.Clear();
                    }
                    //for (int i = 0; i < listUncheck.Rows.Count; i++)
                    //{

                    //    string Utype = listUncheck.Rows[0]["value3"].ToString();
                    //    hInputPara.Add("@ProjectId", Convert.ToInt32(dtprojectid.Rows[0]["Project_ID"]));
                    //    hInputPara.Add("@LocalTransport_ID",Utype);

                    //    sqlDataAccess.ExecuteStoreProcedure("usp_DeleteProjectLocalTransportEdit",hInputPara);


                    //    hInputPara.Clear();
                    //    // hInputPara1.Clear();
                    //}
                }
                for (int p = 0; p < listfloor.Rows.Count; p++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dtprojectID.Rows[0]["Project_ID"]));
                    string floorType = listfloor.Rows[p]["PoVal"].ToString();
                    if (floorType == "Podium")
                    {
                        hInputPara.Add("@FloorType_ID", Convert.ToString(listfloor.Rows[p]["PoVal"]));
                        hInputPara.Add("@Number_Of_Floors", Convert.ToString(listfloor.Rows[p]["podium"]));
                        string result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingFloorDetailsEdit", hInputPara);
                    }

                    hInputPara.Clear();
                }
                for (int p = 0; p < listupper.Rows.Count; p++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dtprojectID.Rows[0]["Project_ID"]));
                    string floorType = listupper.Rows[p]["UppVal"].ToString();
                    if (floorType == "Upper Habitable Floors")
                    {
                        hInputPara.Add("@FloorType_ID", Convert.ToString(listupper.Rows[p]["UppVal"]));
                        hInputPara.Add("@Number_Of_Floors", Convert.ToString(listupper.Rows[p]["UpperHabitableFloors"]));
                        string result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingFloorDetailsEdit", hInputPara);
                    }
                    hInputPara.Clear();
                }
                for (int p = 0; p < listBasement.Rows.Count; p++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dtprojectID.Rows[0]["Project_ID"]));
                    string floorType = listBasement.Rows[p]["BaseVal"].ToString();
                    if (floorType == "Basement")
                    {
                        hInputPara.Add("@FloorType_ID", Convert.ToString(listBasement.Rows[p]["BaseVal"]));
                        hInputPara.Add("@Number_Of_Floors", Convert.ToString(listBasement.Rows[p]["BasementValue"]));
                        string result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingFloorDetailsEdit", hInputPara);
                    }
                    hInputPara.Clear();
                }
                for (int p = 0; p < listG.Rows.Count; p++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dtprojectID.Rows[0]["Project_ID"]));
                    hInputPara.Add("@FloorType_ID", Convert.ToString(listG.Rows[p]["grdVal"]));
                    hInputPara.Add("@Number_Of_Floors", Convert.ToString(listG.Rows[p]["GroundValue"]));
                    string result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingFloorDetailsEdit", hInputPara);
                    hInputPara.Clear();
                }
                for (int p = 0; p < listS.Rows.Count; p++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dtprojectID.Rows[0]["Project_ID"]));
                    hInputPara.Add("@FloorType_ID", Convert.ToString(listS.Rows[p]["stiltVal"]));
                    hInputPara.Add("@Number_Of_Floors", Convert.ToString(listS.Rows[p]["stiltvalue"]));
                    string result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingFloorDetailsEdit", hInputPara);
                    hInputPara.Clear();
                }
                for (int p = 0; p < listsecond.Rows.Count; p++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dtprojectID.Rows[0]["Project_ID"]));
                    hInputPara.Add("@FloorType_ID", Convert.ToString(listsecond.Rows[p]["secVal"]));
                    hInputPara.Add("@Number_Of_Floors", Convert.ToString(listsecond.Rows[p]["secondFloorVal"]));
                    string result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingFloorDetailsEdit", hInputPara);
                    hInputPara.Clear();
                }

                //code for delete while uncheck records

                if (listUncheck.Rows.Count > 0)
                {

                    for (int i = 0; i < listUncheck.Rows.Count; i++)
                    {
                        hInputPara.Add("@ProjectId", Convert.ToString(dtprojectID.Rows[0]["Project_ID"]));
                        string Utype = listUncheck.Rows[i]["value3"].ToString();
                        hInputPara.Add("@UTypeValue", Utype);
                        string result = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteProjectUnitTypesEdit", hInputPara);
                        hInputPara.Clear();
                        hInputPara.Add("@ProjectId", Convert.ToInt32(dtprojectID.Rows[0]["Project_ID"]));
                        hInputPara.Add("@LocalTransport_ID", Utype);

                        sqlDataAccess.ExecuteStoreProcedure("usp_DeleteProjectLocalTransportEdit", hInputPara);

                        hInputPara.Clear();
                        hInputPara.Add("@ProjectId", Convert.ToInt32(dtprojectID.Rows[0]["Project_ID"]));
                        hInputPara.Add("@BasicInfra_type", Utype);

                        sqlDataAccess.ExecuteStoreProcedure("usp_DeleteProjectBasicInfraEdit", hInputPara);
                        hInputPara.Clear();
                        hInputPara.Add("@ProjectId", Convert.ToInt32(dtprojectID.Rows[0]["Project_ID"]));
                        hInputPara.Add("@FloorType_ID", Utype);

                        sqlDataAccess.ExecuteStoreProcedure("usp_DeleteFloorDetailsEdit", hInputPara);
                        hInputPara.Clear();
                    }

                }
                return Json(new { success = true, Message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        [HttpPost]
        public ActionResult UpdateEmployee(ViewModel objproject)
        {
            return View();
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "")] ival_ProjectMaster project)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        Datamodel.Entry(project).State = EntityState.Modified;
        //        Datamodel.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(project);
        //}


        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ival_ProjectMaster project = Datamodel.ival_ProjectMaster.Find(id);
        //    if (project == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(project);
        //}


        //// POST: /Employee/Delete/5


        public ActionResult Delete(int id)
        {
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Project_ID", id);
            sqlDataAccess.ExecuteStoreProcedure("usp_DeleteProjectByID", hInputPara);
            //ival_ProjectMaster project = Datamodel.ival_ProjectMaster.Find(id);
            //Datamodel.ival_ProjectMaster.Remove(project);
            //Datamodel.SaveChanges();
            return RedirectToAction("Index", "Project");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        Datamodel.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        [HttpPost]
        public ActionResult SaveAllApproval(string str, string str1, string str2, string str3, string str4, string str5, string str6)
        {
            try
            {
                DataTable list = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));
                DataTable list2 = (DataTable)JsonConvert.DeserializeObject(str2, (typeof(DataTable)));
                DataTable list3 = (DataTable)JsonConvert.DeserializeObject(str3, (typeof(DataTable)));
                DataTable list4 = (DataTable)JsonConvert.DeserializeObject(str4, (typeof(DataTable)));
                DataTable list5 = (DataTable)JsonConvert.DeserializeObject(str5, (typeof(DataTable)));
                DataTable list6 = (DataTable)JsonConvert.DeserializeObject(str6, (typeof(DataTable)));



                for (int i = 0; i < list1.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Builder_Group_Master_ID", list1.Rows[i]["ddlgroupmaster"].ToString());
                    hInputPara.Add("@Builder_Company_Master_ID", list1.Rows[i]["ddlcompanyaster"].ToString());
                    hInputPara.Add("@P_TypeID", list1.Rows[i]["ddlprojecttypemaster"].ToString());
                    hInputPara.Add("@Project_Name", list1.Rows[i]["txtprojectname"].ToString());
                    hInputPara.Add("@Legal_Address1", list1.Rows[i]["txtlegaladdress1"].ToString());
                    hInputPara.Add("@Legal_Address2", list1.Rows[i]["txtlegaladdress2"].ToString());
                    hInputPara.Add("@Legal_Locality", list1.Rows[i]["txtLegalLocality"].ToString());
                    hInputPara.Add("@Legal_Street", list1.Rows[i]["txtstreet"].ToString());
                    hInputPara.Add("@Legal_City", list1.Rows[i]["txtCity"].ToString());
                    hInputPara.Add("@Legal_District", list1.Rows[i]["txtLegal_District"].ToString());
                    hInputPara.Add("@Legal_State", list1.Rows[i]["ddlstatemaster"].ToString());
                    hInputPara.Add("@Legal_Pincode", list1.Rows[i]["ddlpincodemaster"].ToString());
                    hInputPara.Add("@Postal_Address1", list1.Rows[i]["txtpostAddress1"].ToString());
                    hInputPara.Add("@Postal_Address2", list1.Rows[i]["txtpostAddress2"].ToString());
                    hInputPara.Add("@Postal_Street", list1.Rows[i]["txtpostStreet"].ToString());
                    hInputPara.Add("@Postal_Locality", list1.Rows[i]["txtpostLocality"].ToString());
                    hInputPara.Add("@Postal_City", list1.Rows[i]["txtpostCity"].ToString());
                    hInputPara.Add("@Postal_District", list1.Rows[i]["txtpostDistrict"].ToString());
                    hInputPara.Add("@Postal_State", list1.Rows[i]["ddlpoststatemaster"].ToString());
                    hInputPara.Add("@Postal_Pincode", list1.Rows[i]["ddlpostpincodemaster"].ToString());
                    hInputPara.Add("@Postal_NearbyLandmark", list1.Rows[i]["txtPostal_NearbyLandmark"].ToString());
                    hInputPara.Add("@RERA_Approval_Num", list1.Rows[i]["txtRERAApprovalno"].ToString());
                    hInputPara.Add("@Total_Land_Area", list1.Rows[i]["txtTotal_Land_Areasqmtrs"].ToString());
                    hInputPara.Add("@Total_Permissible_FSI", list1.Rows[i]["txtPermissiblesqmtrs"].ToString());
                    hInputPara.Add("@Approved_Builtup_Area", list1.Rows[i]["txtApproved_Builtup_Areasqmt"].ToString());
                    hInputPara.Add("@Approved_No_of_Buildings", list1.Rows[i]["txtApproved_No_of_Buildings"].ToString());
                    hInputPara.Add("@Approved_No_of_plotsorflats", list1.Rows[i]["txtApproved_No_of_plotsorflats"].ToString());
                    hInputPara.Add("@Approved_No_of_Wings", list1.Rows[i]["txtApproved_No_of_Wings"].ToString());
                    hInputPara.Add("@Approved_No_of_Floors", list1.Rows[i]["txtApproved_No_of_Floors"].ToString());
                    hInputPara.Add("@Approved_No_of_Units", list1.Rows[i]["txtApproved_No_of_Units"].ToString());
                    hInputPara.Add("@Specifications", list1.Rows[i]["txtSpecifications"].ToString());
                    hInputPara.Add("@Amenities", list1.Rows[i]["txtAmenities"].ToString());
                    hInputPara.Add("@Cord_Latitude", list1.Rows[i]["txtlatitude"].ToString());
                    hInputPara.Add("@Cord_Longitude", list1.Rows[i]["txtlongitude"].ToString());
                    hInputPara.Add("@AdjoiningRoad_Width1", list1.Rows[i]["txtRoad1"].ToString());
                    hInputPara.Add("@AdjoiningRoad_Width2", list1.Rows[i]["txtRoad2"].ToString());
                    hInputPara.Add("@AdjoiningRoad_Width3", list1.Rows[i]["txtRoad3"].ToString());
                    hInputPara.Add("@AdjoiningRoad_Width4", list1.Rows[i]["txtRoad4"].ToString());
                    hInputPara.Add("@Social_Infrastructure", list1.Rows[i]["ddllocalitymaster"].ToString());

                    hInputPara.Add("@Class_of_Locality", list1.Rows[i]["ddlclassoflocality"].ToString());
                    hInputPara.Add("@DistFrom_City_Center", list1.Rows[i]["txtdistfrmcity"].ToString());
                    hInputPara.Add("@Locality_Remarks", list1.Rows[i]["txtRemarks"].ToString());
                    hInputPara.Add("@Deviation_Description", list1.Rows[i]["txtDeviationDescription"].ToString());
                    sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectMaster", hInputPara);


                }



                for (int i1 = 0; i1 < list.Rows.Count; i1++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1Project");

                    hInputPara.Add("@Project_ID", Convert.ToString(dttop1project.Rows[0]["Project_ID"]));
                    hInputPara.Add("@Building_ID", 1);
                    hInputPara.Add("@Wing_ID", 1);
                    hInputPara.Add("@Approval_Type", list.Rows[i1]["ApprovalType"].ToString());
                    hInputPara.Add("@Approving_Authority", list.Rows[i1]["Approving_Authority"].ToString());
                    hInputPara.Add("@Date_of_Approval", list.Rows[i1]["Approving_Date"].ToString());
                    hInputPara.Add("@Approval_Num", list.Rows[i1]["Approving_Num"].ToString());
                    sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectApprovalDetails", hInputPara);
                }

                for (int i2 = 0; i2 < list2.Rows.Count; i2++)
                {
                    DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1Project");

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dttop1project.Rows[0]["Project_ID"]));
                    hInputPara.Add("@Building_ID", "");
                    hInputPara.Add("@Wing_ID", "");
                    hInputPara.Add("@Unit_ID", "");
                    hInputPara.Add("@Side_Margin_Description", list2.Rows[i2]["SidemarginsType"].ToString());
                    hInputPara.Add("@As_per_Approved", list2.Rows[i2]["Asperapprovedplan"].ToString());
                    hInputPara.Add("@As_per_Measurement", list2.Rows[i2]["Aspermeasurement"].ToString());
                    hInputPara.Add("@Percentage_Deviation", list2.Rows[i2]["perdevistion"].ToString());
                    hInputPara.Add("@Deviation_Description", list2.Rows[i2]["perdevistion"].ToString());

                    sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectSideMargins", hInputPara);
                }


                for (int i3 = 0; i3 < list3.Rows.Count; i3++)
                {
                    DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1Project");

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dttop1project.Rows[0]["Project_ID"]));
                    hInputPara.Add("@Building_ID", 1);
                    hInputPara.Add("@Wing_ID", 1);
                    hInputPara.Add("@Unit_ID", 1);
                    hInputPara.Add("@Boundry_Name", list3.Rows[i3]["BoundariesType"].ToString());
                    hInputPara.Add("@AsPer_Document", list3.Rows[i3]["asperdoc"].ToString());
                    hInputPara.Add("@AsPer_Site", list3.Rows[i3]["aspersite"].ToString());
                    sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectBoundaries", hInputPara);

                }

                for (int i4 = 0; i4 < list4.Rows.Count; i4++)
                {
                    DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1Project");

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dttop1project.Rows[0]["Project_ID"]));
                    hInputPara.Add("@UType_ID", Convert.ToString(list4.Rows[i4]["value"]));
                    sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectUnitTypes", hInputPara);
                }

                for (int i4 = 0; i4 < list5.Rows.Count; i4++)
                {
                    DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1Project");

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dttop1project.Rows[0]["Project_ID"]));
                    hInputPara.Add("@BasicInfra_type", Convert.ToString(list5.Rows[i4]["value1"]));
                    hInputPara.Add("@Created_By", "Branch Head");
                    sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectBasicInfraTypes", hInputPara);
                }

                for (int i4 = 0; i4 < list6.Rows.Count; i4++)
                {
                    DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1Project");

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", Convert.ToString(dttop1project.Rows[0]["Project_ID"]));
                    hInputPara.Add("@LocalTransport_ID", Convert.ToString(list6.Rows[i4]["value2"]));
                    hInputPara.Add("@Created_By", "Branch Head");
                    sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectLocaltransport", hInputPara);
                }


                return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult UpdateAllApproval(string str, string str1, string str2, string str3, string str4, string str5, string str6)
        {
            try
            {
                DataTable list = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                DataTable list1 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));
                DataTable list2 = (DataTable)JsonConvert.DeserializeObject(str2, (typeof(DataTable)));
                DataTable list3 = (DataTable)JsonConvert.DeserializeObject(str3, (typeof(DataTable)));
                DataTable list4 = (DataTable)JsonConvert.DeserializeObject(str4, (typeof(DataTable)));
                DataTable list5 = (DataTable)JsonConvert.DeserializeObject(str5, (typeof(DataTable)));
                DataTable list6 = (DataTable)JsonConvert.DeserializeObject(str6, (typeof(DataTable)));
                int ID = Convert.ToInt32(Session["parojectid"]);
                for (int i = 0; i < list1.Rows.Count; i++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Projet_ID", ID);
                    hInputPara.Add("@Builder_Group_Master_ID", list1.Rows[i]["ddlgroupmaster"].ToString());
                    hInputPara.Add("@Builder_Company_Master_ID", list1.Rows[i]["ddlcompanyaster"].ToString());
                    hInputPara.Add("@P_TypeID", list1.Rows[i]["ddlprojecttypemaster"].ToString());
                    hInputPara.Add("@Project_Name", list1.Rows[i]["txtprojectname"].ToString());
                    hInputPara.Add("@Legal_Address1", list1.Rows[i]["txtlegaladdress1"].ToString());
                    hInputPara.Add("@Legal_Address2", list1.Rows[i]["txtlegaladdress2"].ToString());
                    hInputPara.Add("@Legal_Locality", list1.Rows[i]["txtLegalLocality"].ToString());
                    hInputPara.Add("@Legal_Street", list1.Rows[i]["txtstreet"].ToString());
                    hInputPara.Add("@Legal_City", list1.Rows[i]["txtCity"].ToString());
                    hInputPara.Add("@Legal_District", list1.Rows[i]["txtLegal_District"].ToString());
                    hInputPara.Add("@Legal_State", list1.Rows[i]["ddlstatemaster"].ToString());
                    hInputPara.Add("@Legal_Pincode", list1.Rows[i]["ddlpincodemaster"].ToString());
                    hInputPara.Add("@Postal_Address1", list1.Rows[i]["txtpostAddress1"].ToString());
                    hInputPara.Add("@Postal_Address2", list1.Rows[i]["txtpostAddress2"].ToString());
                    hInputPara.Add("@Postal_Street", list1.Rows[i]["txtpostStreet"].ToString());
                    hInputPara.Add("@Postal_Locality", list1.Rows[i]["txtpostLocality"].ToString());
                    hInputPara.Add("@Postal_City", list1.Rows[i]["txtpostCity"].ToString());
                    hInputPara.Add("@Postal_District", list1.Rows[i]["txtpostDistrict"].ToString());
                    hInputPara.Add("@Postal_State", list1.Rows[i]["ddlpoststatemaster"].ToString());
                    hInputPara.Add("@Postal_Pincode", list1.Rows[i]["ddlpostpincodemaster"].ToString());
                    hInputPara.Add("@Postal_NearbyLandmark", list1.Rows[i]["txtPostal_NearbyLandmark"].ToString());
                    hInputPara.Add("@RERA_Approval_Num", list1.Rows[i]["txtRERAApprovalno"].ToString());
                    hInputPara.Add("@Total_Land_Area", list1.Rows[i]["txtTotal_Land_Areasqmtrs"].ToString());
                    hInputPara.Add("@Total_Permissible_FSI", list1.Rows[i]["txtPermissiblesqmtrs"].ToString());
                    hInputPara.Add("@Approved_Builtup_Area", list1.Rows[i]["txtApproved_Builtup_Areasqmt"].ToString());
                    hInputPara.Add("@Approved_No_of_Buildings", list1.Rows[i]["txtApproved_No_of_Buildings"].ToString());
                    hInputPara.Add("@Approved_No_of_plotsorflats", list1.Rows[i]["txtApproved_No_of_plotsorflats"].ToString());
                    hInputPara.Add("@Approved_No_of_Wings", list1.Rows[i]["txtApproved_No_of_Wings"].ToString());
                    hInputPara.Add("@Approved_No_of_Floors", list1.Rows[i]["txtApproved_No_of_Floors"].ToString());
                    hInputPara.Add("@Approved_No_of_Units", list1.Rows[i]["txtApproved_No_of_Units"].ToString());
                    hInputPara.Add("@Specifications", list1.Rows[i]["txtSpecifications"].ToString());
                    hInputPara.Add("@Amenities", list1.Rows[i]["txtAmenities"].ToString());
                    hInputPara.Add("@Cord_Latitude", list1.Rows[i]["txtlatitude"].ToString());
                    hInputPara.Add("@Cord_Longitude", list1.Rows[i]["txtlongitude"].ToString());
                    hInputPara.Add("@AdjoiningRoad_Width1", list1.Rows[i]["txtRoad1"].ToString());
                    hInputPara.Add("@AdjoiningRoad_Width2", list1.Rows[i]["txtRoad2"].ToString());
                    hInputPara.Add("@AdjoiningRoad_Width3", list1.Rows[i]["txtRoad3"].ToString());
                    hInputPara.Add("@AdjoiningRoad_Width4", list1.Rows[i]["txtRoad4"].ToString());
                    hInputPara.Add("@Social_Infrastructure", list1.Rows[i]["ddllocalitymaster"].ToString());

                    hInputPara.Add("@Class_of_Locality", list1.Rows[i]["ddlclassoflocality"].ToString());
                    hInputPara.Add("@DistFrom_City_Center", list1.Rows[i]["txtdistfrmcity"].ToString());
                    hInputPara.Add("@Locality_Remarks", list1.Rows[i]["txtRemarks"].ToString());
                    hInputPara.Add("@Deviation_Description", list1.Rows[i]["txtDeviationDescription"].ToString());
                    sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectMaster", hInputPara);


                }

                ViewModel vmappr = new ViewModel();
                vmappr.ival_ProjectApprovalDetails = GetApprovalDetails(ID);
                vmappr.ival_ProjectSideMargin = GetSideMarginsDetails(ID);
                vmappr.ival_ProjectBoundaries = GetBoundryDetails(ID);

                foreach (ival_ProjectApprovalDetails ival_ProjApprovalDetails1 in vmappr.ival_ProjectApprovalDetails)
                {

                    for (int i1 = 0; i1 < list.Rows.Count; i1++)
                    {
                        if (ival_ProjApprovalDetails1.Approval_Type == list.Rows[i1]["ApprovalType"].ToString())
                        {
                            hInputPara = new Hashtable();
                            sqlDataAccess = new SQLDataAccess();


                            hInputPara.Add("@PApproval_ID", ival_ProjApprovalDetails1.PApproval_ID);
                            hInputPara.Add("@Approving_Authority", list.Rows[i1]["Approving_Authority"].ToString());
                            hInputPara.Add("@Date_of_Approval", list.Rows[i1]["Approving_Date"].ToString());
                            hInputPara.Add("@Approval_Num", list.Rows[i1]["Approving_Num"].ToString());
                            sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectApprovalDetails", hInputPara);
                            //break;
                        }
                    }
                    //  break;
                }

                foreach (ival_ProjectSideMargin ival_Projsidemargins in vmappr.ival_ProjectSideMargin)
                {

                    for (int i2 = 0; i2 < list2.Rows.Count; i2++)
                    {
                        if (ival_Projsidemargins.Side_Margin_Description == list2.Rows[i2]["SidemarginsType"].ToString())
                        {
                            hInputPara = new Hashtable();
                            sqlDataAccess = new SQLDataAccess();


                            hInputPara.Add("@Side_Margin_ID", ival_Projsidemargins.Side_Margin_ID);
                            //hInputPara.Add("@Side_Margin_Description", list2.Rows[i2]["SidemarginsType"].ToString());
                            hInputPara.Add("@As_per_Approved", list2.Rows[i2]["Asperapprovedplan"].ToString());
                            hInputPara.Add("@As_per_Measurement", list2.Rows[i2]["Aspermeasurement"].ToString());
                            hInputPara.Add("@Percentage_Deviation", list2.Rows[i2]["perdevistion"].ToString());
                            //hInputPara.Add("@Deviation_Description", list2.Rows[i2]["perdevistion"].ToString());

                            sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectSideMargins", hInputPara);
                            //break;
                        }
                    }
                    //  break;
                }

                foreach (ival_ProjectBoundaries ival_ProjBoundaries in vmappr.ival_ProjectBoundaries)
                {

                    for (int i3 = 0; i3 < list3.Rows.Count; i3++)
                    {
                        if (ival_ProjBoundaries.Boundry_Name == list3.Rows[i3]["BoundariesType"].ToString())
                        {
                            hInputPara = new Hashtable();
                            sqlDataAccess = new SQLDataAccess();
                            hInputPara.Add("@Boundry_ID", ival_ProjBoundaries.Boundry_ID);
                            //  hInputPara.Add("@Boundry_Name", list3.Rows[i3]["BoundariesType"].ToString());
                            hInputPara.Add("@AsPer_Document", list3.Rows[i3]["asperdoc"].ToString());
                            hInputPara.Add("@AsPer_Site", list3.Rows[i3]["aspersite"].ToString());
                            sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectBoundaries", hInputPara);

                        }
                    }
                }

                //for (int i2 = 0; i2 < list2.Rows.Count; i2++)
                //{
                //    DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1Project");

                //    hInputPara = new Hashtable();
                //    sqlDataAccess = new SQLDataAccess();
                //    hInputPara.Add("@Project_ID", ID);
                //    hInputPara.Add("@Building_ID", "");
                //    hInputPara.Add("@Wing_ID", "");
                //    hInputPara.Add("@Unit_ID", "");
                //    hInputPara.Add("@Side_Margin_Description", list2.Rows[i2]["SidemarginsType"].ToString());
                //    hInputPara.Add("@As_per_Approved", list2.Rows[i2]["Asperapprovedplan"].ToString());
                //    hInputPara.Add("@As_per_Measurement", list2.Rows[i2]["Aspermeasurement"].ToString());
                //    hInputPara.Add("@Percentage_Deviation", list2.Rows[i2]["perdevistion"].ToString());
                //    hInputPara.Add("@Deviation_Description", list2.Rows[i2]["perdevistion"].ToString());

                //    sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectSideMargins", hInputPara);
                //}


                //for (int i3 = 0; i3 < list3.Rows.Count; i3++)
                //{
                //    DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1Project");

                //    hInputPara = new Hashtable();
                //    sqlDataAccess = new SQLDataAccess();
                //    hInputPara.Add("@Project_ID", ID);
                //    hInputPara.Add("@Building_ID", 1);
                //    hInputPara.Add("@Wing_ID", 1);
                //    hInputPara.Add("@Unit_ID", 1);
                //    hInputPara.Add("@Boundry_Name", list3.Rows[i3]["BoundariesType"].ToString());
                //    hInputPara.Add("@AsPer_Document", list3.Rows[i3]["asperdoc"].ToString());
                //    hInputPara.Add("@AsPer_Site", list3.Rows[i3]["aspersite"].ToString());
                //    sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectBoundaries", hInputPara);

                //}
                for (int i41 = 0; i41 < list4.Rows.Count; i41++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", ID);
                    sqlDataAccess.ExecuteStoreProcedure("usp_deleteProjectUnitTypes", hInputPara);
                }

                for (int i4 = 0; i4 < list4.Rows.Count; i4++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", ID);
                    hInputPara.Add("@UType_ID", Convert.ToString(list4.Rows[i4]["value"]));
                    sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectUnitTypes", hInputPara);
                }

                for (int i51 = 0; i51 < list4.Rows.Count; i51++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", ID);
                    sqlDataAccess.ExecuteStoreProcedure("usp_deleteProjectBasic", hInputPara);
                }


                for (int i5 = 0; i5 < list5.Rows.Count; i5++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", ID);
                    hInputPara.Add("@BasicInfra_type", Convert.ToString(list5.Rows[i5]["value1"]));
                    hInputPara.Add("@Created_By", "Branch Head");
                    sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectBasicInfraTypes", hInputPara);
                }
                for (int i61 = 0; i61 < list4.Rows.Count; i61++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", ID);
                    sqlDataAccess.ExecuteStoreProcedure("usp_deleteProjectlovaltransports", hInputPara);
                }


                for (int i6 = 0; i6 < list6.Rows.Count; i6++)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Project_ID", ID);
                    hInputPara.Add("@LocalTransport_ID", Convert.ToString(list6.Rows[i6]["value2"]));
                    hInputPara.Add("@Created_By", "Branch Head");
                    sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectLocaltransport", hInputPara);
                }


                return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public ActionResult AddProject(ViewModel objPrj)
        //{


        //    string whereCndn = string.Empty;
        //    List<ival_LookupCategoryValues> lHob = new List<ival_LookupCategoryValues>();
        //    //  lHob.Add(new ival_LookupCategoryValues() {Lookup_Value};

        //    foreach (var item in objPrj.ival_LookupCategoryValuesunittype)
        //    {
        //        if (item.IsSelected)
        //        {
        //            whereCndn += item.Lookup_Value + ","; //here you will get set of selected checkbox value like (8,9,)
        //        }
        //    }


        //        var appauth = objPrj.ival_ProjectApprovalDetails.Approving_Authority;
        //        var appnum = objPrj.ival_ProjectApprovalDetails.Date_of_Approval;
        //        var appno = objPrj.ival_ProjectApprovalDetails.Approval_Num;


        //    //foreach (var activity in Datamodel.ival_ProjectApprovalDetails)
        //    //{

        //    //    // pass activity.Hours to database here
        //    //}
        //    //USING CODE FIRST 
        //    //string strDDLValue = objPrj.ival_BuilderGroupMasters.ToList().Select(p=>new 
        //    //return View(vm);
        //    //vm.ival_ProjectMasters.Project_ID=vm.ival_ProjectMasters.Project_ID;

        //    // Datamodel.vm.Add(objPrj);
        //    // Datamodel.SaveChanges();
        //    //return RedirectToAction("Index");

        //    try
        //    {
        //        //   dynamic obj = JsonConvert.DeserializeObject(objEmp);
        //        hInputPara = new Hashtable();
        //        sqlDataAccess = new SQLDataAccess();

        //        hInputPara = new Hashtable();
        //        hInputPara.Add("@Builder_Group_Master_ID", objPrj.SelectedModelGroupmaster);
        //        hInputPara.Add("@Builder_Company_Master_ID", objPrj.SelectedModelCompanyMaster);
        //        hInputPara.Add("@P_TypeID", objPrj.SelectedModellookupprojecttype);
        //        hInputPara.Add("@Project_Name", Convert.ToString(objPrj.ival_ProjectMasters.Project_Name));
        //        hInputPara.Add("@Legal_Address1", Convert.ToString(objPrj.ival_ProjectMasters.Legal_Address1));
        //        hInputPara.Add("@Legal_Address2", Convert.ToString(objPrj.ival_ProjectMasters.Legal_Address2));
        //        hInputPara.Add("@Legal_Street", Convert.ToString(objPrj.ival_ProjectMasters.Legal_Street));
        //        hInputPara.Add("@Legal_City", Convert.ToString(objPrj.ival_ProjectMasters.Legal_City));
        //        hInputPara.Add("@Legal_District", Convert.ToString(objPrj.ival_ProjectMasters.Legal_District));
        //        hInputPara.Add("@Legal_State", objPrj.SelectedModelstates);
        //        hInputPara.Add("@Legal_Pincode", objPrj.SelectedModelpincode);
        //        hInputPara.Add("@Postal_Address1", Convert.ToString(objPrj.ival_ProjectMasters.Postal_Address1));
        //        hInputPara.Add("@Postal_Address2", Convert.ToString(objPrj.ival_ProjectMasters.Postal_Address2));
        //        hInputPara.Add("@Postal_Street", Convert.ToString(objPrj.ival_ProjectMasters.Postal_Street));
        //        hInputPara.Add("@Postal_Locality", objPrj.ival_ProjectMasters.Postal_Locality);
        //        hInputPara.Add("@Postal_City", Convert.ToString(objPrj.ival_ProjectMasters.Postal_City));
        //        hInputPara.Add("@Postal_District", Convert.ToString(objPrj.ival_ProjectMasters.Postal_District));
        //        hInputPara.Add("@Postal_State", objPrj.ival_States_postal);
        //        hInputPara.Add("@Postal_Pincode", objPrj.ival_LookupCategoryValuespincode_postal);
        //        hInputPara.Add("@Postal_NearbyLandmark", Convert.ToString(objPrj.ival_ProjectMasters.Postal_NearbyLandmark));
        //        hInputPara.Add("@RERA_Approval_Num", Convert.ToString(objPrj.ival_ProjectMasters.RERA_Approval_Num));
        //        hInputPara.Add("@Total_Land_Area", Convert.ToString(objPrj.ival_ProjectMasters.Total_Land_Area));
        //        hInputPara.Add("@Total_Permissible_FSI", Convert.ToString(objPrj.ival_ProjectMasters.Total_Permissible_FSI));
        //        hInputPara.Add("@Approved_Builtup_Area", Convert.ToString(objPrj.ival_ProjectMasters.Approved_Builtup_Area));
        //        hInputPara.Add("@Approved_No_of_Buildings", Convert.ToString(objPrj.ival_ProjectMasters.Approved_No_of_Buildings));
        //        hInputPara.Add("@Approved_No_of_plotsorflats", Convert.ToString(objPrj.ival_ProjectMasters.Approved_No_of_plotsorflats));
        //        hInputPara.Add("@Approved_No_of_Wings", Convert.ToString(objPrj.ival_ProjectMasters.Approved_No_of_Wings));
        //        hInputPara.Add("@Approved_No_of_Floors", Convert.ToString(objPrj.ival_ProjectMasters.Approved_No_of_Floors));
        //        hInputPara.Add("@Approved_No_of_Units", Convert.ToString(objPrj.ival_ProjectMasters.Approved_No_of_Units));
        //        hInputPara.Add("@Specifications", Convert.ToString(objPrj.ival_ProjectMasters.Specifications));
        //        hInputPara.Add("@Amenities", Convert.ToString(objPrj.ival_ProjectMasters.Amenities));
        //        hInputPara.Add("@Cord_Latitude", Convert.ToString(objPrj.ival_ProjectMasters.Cord_Latitude));
        //        hInputPara.Add("@Cord_Longitude", Convert.ToString(objPrj.ival_ProjectMasters.Cord_Longitude));
        //        hInputPara.Add("@AdjoiningRoad_Width1", Convert.ToString(objPrj.ival_ProjectMasters.AdjoiningRoad_Width1));
        //        hInputPara.Add("@AdjoiningRoad_Width2", Convert.ToString(objPrj.ival_ProjectMasters.AdjoiningRoad_Width2));
        //        hInputPara.Add("@AdjoiningRoad_Width3", Convert.ToString(objPrj.ival_ProjectMasters.AdjoiningRoad_Width3));
        //        hInputPara.Add("@AdjoiningRoad_Width4", Convert.ToString(objPrj.ival_ProjectMasters.AdjoiningRoad_Width4));
        //        hInputPara.Add("@Social_Infrastructure", Convert.ToString(objPrj.SelectedModelsocialInfra));
        //        hInputPara.Add("@Quality_Of_Infra", Convert.ToString(objPrj.SelectedModelbasicinfra));
        //        hInputPara.Add("@Class_of_Locality", Convert.ToString(objPrj.SelectedModelclassoflocality));
        //        hInputPara.Add("@DistFrom_City_Center", Convert.ToString(objPrj.ival_ProjectMasters.DistFrom_City_Center));
        //        hInputPara.Add("@Locality_Remarks", Convert.ToString(objPrj.ival_ProjectMasters.Locality_Remarks));
        //        sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectMaster", hInputPara);


        //        // sqlDataAccess.ExecuteStoreProcedure("usp_Save_User_login", inputUser);
        //        // }
        //        //else
        //        //{
        //        //    return Content("Fail");
        //        //}

        //        //return RedirectToAction("Index", "Ival_Employee");
        //    }
        //    catch (Exception e)
        //    {
        //        e.InnerException.ToString();
        //    }
        //    return RedirectToAction("Index", "Project");
        //}



        /// <summary>
        /// Priyanka Raut
        /// </summary>
        /// <returns></returns>
        public ActionResult AddBuilding()
        {
            viewModel = new ViewModel();
            if (Session["ProjectID"] != null)
            {
                Int32 projectId = Convert.ToInt32(Session["projectID"].ToString());
                viewModel.ProjectID = projectId;
                List<ival_ProjectMaster> ival_ProjectMastersList = GetProjects();
                var projectName = ival_ProjectMastersList.AsEnumerable().Where(p => p.Project_ID == projectId).Select(row => row.Project_Name).FirstOrDefault();
                ViewBag.ProjectName = projectName;
            }



            viewModel.ival_BuildingWingFloorDetailsList = GetFloorType();
            viewModel.ival_ProjectBoundariesList = GetBoundariesList();
            viewModel.ival_ProjectSideMarginList = GetSideMarginsList();
            return View(viewModel);
        }
        /// <summary>
        /// Priyanka Raut
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddBuilding(ViewModel viewModel)
        {
            int buildingID = 0;
            try
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara = new Hashtable();

                hInputPara.Add("@Project_ID", viewModel.ProjectID);
                hInputPara.Add("@Building_Name", viewModel.ival_ProjectBuildingMaster.Building_Name);
                hInputPara.Add("@No_of_Wings", viewModel.ival_ProjectBuildingMaster.No_of_Wings);
                hInputPara.Add("@Approved_No_Of_Units", viewModel.ival_ProjectBuildingMaster.Approved_No_of_Units);
                hInputPara.Add("@Approved_Building_Area", viewModel.ival_ProjectBuildingMaster.Approved_Building_Area);
                hInputPara.Add("@Remarks", viewModel.ival_ProjectBuildingMaster.Remarks);
                hInputPara.Add("@deviation_Percentage", viewModel.Deviation_Percentage);
                hInputPara.Add("@Deviation_Description", viewModel.ival_ProjectBuildingMaster.Deviation_Description);
                hInputPara.Add("@Cord_Latitude", viewModel.ival_ProjectBuildingMaster.Cord_Latitutde);
                hInputPara.Add("@Cord_Longitude", viewModel.ival_ProjectBuildingMaster.Cord_Longitude);
                hInputPara.Add("@Created_By", string.Empty);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectBuildingMaster", hInputPara);

                if (Convert.ToInt32(result) != 0)
                {
                    try
                    {
                        buildingID = Convert.ToInt32(result);
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara = new Hashtable();


                        StringBuilder sb_BulkinsertBuildingWingFloor = new StringBuilder();
                        sb_BulkinsertBuildingWingFloor.AppendLine("<BulkBuildingWingFloorDetails>");

                        foreach (ival_BuildingWingFloorDetails _BuildingWingFloorDetails in viewModel.ival_BuildingWingFloorDetailsList)
                        {
                            sb_BulkinsertBuildingWingFloor.AppendLine("<BulkBuildingWingFloorDetailsData>");

                            sb_BulkinsertBuildingWingFloor.AppendLine("<Building_ID>" + buildingID + "</Building_ID>");
                            sb_BulkinsertBuildingWingFloor.AppendLine("<Wing_ID>" + 0 + "</Wing_ID>");
                            sb_BulkinsertBuildingWingFloor.AppendLine("<FloorType_ID>" + _BuildingWingFloorDetails.FloorType_ID + "</FloorType_ID>");
                            sb_BulkinsertBuildingWingFloor.AppendLine("<Number_Of_Floors>" + _BuildingWingFloorDetails.Number_Of_Floors + "</Number_Of_Floors>");

                            sb_BulkinsertBuildingWingFloor.AppendLine("</BulkBuildingWingFloorDetailsData>");
                        }

                        sb_BulkinsertBuildingWingFloor.AppendLine("</BulkBuildingWingFloorDetails>");
                        string xmlData = sb_BulkinsertBuildingWingFloor.ToString().Replace("&", "&amp;").Replace(" <", "<").Replace("> ", ">");
                        hInputPara.Add("@InsertBuildingWingFloorXML", xmlData);


                        var resultBuildingWingFloor = sqlDataAccess.ExecuteStoreProcedure("usp_InsertBuildingWingFloorDetails", hInputPara);
                        if (Convert.ToInt32(resultBuildingWingFloor) == 1)
                        {

                            try
                            {
                                hInputPara = new Hashtable();
                                sqlDataAccess = new SQLDataAccess();
                                hInputPara = new Hashtable();

                                StringBuilder sb_BulkinsertProjectSideMarginDetails = new StringBuilder();
                                sb_BulkinsertProjectSideMarginDetails.AppendLine("<BulkProjectSideMarginDetails>");

                                foreach (ival_ProjectSideMargin ival_ProjectSideMargin in viewModel.ival_ProjectSideMarginList)
                                {
                                    sb_BulkinsertProjectSideMarginDetails.AppendLine("<BulkProjectSideMarginDetailsData>");

                                    sb_BulkinsertProjectSideMarginDetails.AppendLine("<Project_ID>" + 0 + "</Project_ID>");
                                    sb_BulkinsertProjectSideMarginDetails.AppendLine("<Building_ID>" + buildingID + "</Building_ID>");
                                    sb_BulkinsertProjectSideMarginDetails.AppendLine("<Wing_ID>" + 0 + "</Wing_ID>");
                                    sb_BulkinsertProjectSideMarginDetails.AppendLine("<Unit_ID>" + 0 + "</Unit_ID>");
                                    sb_BulkinsertProjectSideMarginDetails.AppendLine("<Side_Margin_Description>" + ival_ProjectSideMargin.Side_Margin_Description + "</Side_Margin_Description>");
                                    sb_BulkinsertProjectSideMarginDetails.AppendLine("<As_per_Approved>" + ival_ProjectSideMargin.As_per_Approved + "</As_per_Approved>");
                                    sb_BulkinsertProjectSideMarginDetails.AppendLine("<As_per_Measurement>" + ival_ProjectSideMargin.As_per_Measurement + "</As_per_Measurement>");
                                    sb_BulkinsertProjectSideMarginDetails.AppendLine("<Percentage_Deviation>" + ival_ProjectSideMargin.Percentage_Deviation + "</Percentage_Deviation>");
                                    sb_BulkinsertProjectSideMarginDetails.AppendLine("<Deviation_Description>" + ival_ProjectSideMargin.Deviation_Description + "</Deviation_Description>");
                                    sb_BulkinsertProjectSideMarginDetails.AppendLine("</BulkProjectSideMarginDetailsData>");
                                }

                                sb_BulkinsertProjectSideMarginDetails.AppendLine("</BulkProjectSideMarginDetails>");
                                string xmlDataSideMargin = sb_BulkinsertProjectSideMarginDetails.ToString().Replace("&", "&amp;").Replace(" <", "<").Replace("> ", ">");
                                hInputPara.Add("@InsertProjectSideMarginXML", xmlDataSideMargin);


                                var resultSideMargin = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectSideMarginsList", hInputPara);
                                if (Convert.ToInt32(resultSideMargin) == 1)
                                {
                                    try
                                    {
                                        hInputPara = new Hashtable();
                                        sqlDataAccess = new SQLDataAccess();
                                        hInputPara = new Hashtable();

                                        StringBuilder sb_BulkinsertProjectBoundariesDetails = new StringBuilder();
                                        sb_BulkinsertProjectBoundariesDetails.AppendLine("<BulkProjectBoundariesDetails>");

                                        foreach (ival_ProjectBoundaries ival_ProjectBoundaries in viewModel.ival_ProjectBoundariesList)
                                        {
                                            sb_BulkinsertProjectBoundariesDetails.AppendLine("<BulkProjectBoundariesDetailsData>");

                                            sb_BulkinsertProjectBoundariesDetails.AppendLine("<Project_ID>" + 0 + "</Project_ID>");
                                            sb_BulkinsertProjectBoundariesDetails.AppendLine("<Building_ID>" + buildingID + "</Building_ID>");
                                            sb_BulkinsertProjectBoundariesDetails.AppendLine("<Wing_ID>" + 0 + "</Wing_ID>");
                                            sb_BulkinsertProjectBoundariesDetails.AppendLine("<Unit_ID>" + 0 + "</Unit_ID>");
                                            sb_BulkinsertProjectBoundariesDetails.AppendLine("<Boundry_Name>" + ival_ProjectBoundaries.Boundry_Name + "</Boundry_Name>");
                                            sb_BulkinsertProjectBoundariesDetails.AppendLine("<AsPer_Document>" + ival_ProjectBoundaries.AsPer_Document + "</AsPer_Document>");
                                            sb_BulkinsertProjectBoundariesDetails.AppendLine("<AsPer_Site>" + ival_ProjectBoundaries.AsPer_Site + "</AsPer_Site>");

                                            sb_BulkinsertProjectBoundariesDetails.AppendLine("</BulkProjectBoundariesDetailsData>");
                                        }

                                        sb_BulkinsertProjectBoundariesDetails.AppendLine("</BulkProjectBoundariesDetails>");
                                        string xmlDataBoundaries = sb_BulkinsertProjectBoundariesDetails.ToString().Replace("&", "&amp;").Replace(" <", "<").Replace("> ", ">");
                                        hInputPara.Add("@InsertProjectBoundariesXML", xmlDataBoundaries);

                                        var resultBoundaries = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectBoundariesList", hInputPara);
                                        if (Convert.ToInt32(resultBoundaries) == 1)
                                        {
                                            return RedirectToAction("Index", "Project");
                                        }
                                        else
                                        {
                                            //ModelState.AddModelError("Message", "Please Enter Data");
                                            return Json(new { status = "error", message = "Error in Database Record not Save!" }, JsonRequestBehavior.AllowGet);
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                        //ModelState.AddModelError("Message", ex.Message);
                                        return Json(new { status = "error", message = ex.Message });
                                    }
                                }
                                else
                                {
                                    return Json(new { status = "error", message = "Error in Database Record not Save!" }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            catch (Exception ex)
                            {

                                //ModelState.AddModelError("Message", ex.Message);
                                return Json(new { status = "error", message = ex.Message }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new { status = "error", message = "Error in Database Record not Save!" }, JsonRequestBehavior.AllowGet);
                        }

                    }
                    catch (Exception ex)
                    {
                        //ModelState.AddModelError("Message", ex.Message);
                        return Json(new { status = "error", message = ex.Message }, JsonRequestBehavior.AllowGet);
                        //return RedirectToAction("Index", "Project");
                    }
                }

                else
                {
                    //ModelState.AddModelError("Message", "Please Enter Data");
                    return Json(new { status = "error", message = "Error in Database Record not Save!" }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                //e.InnerException.ToString();
                //ModelState.AddModelError("Message", ex.Message);
                return Json(new { status = "error", message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

            //return RedirectToAction("Index", "Project");
        }
        [HttpGet]
        public ActionResult EditBuilding(int ID)
        {
            if (string.IsNullOrEmpty(ID.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //return View();
            }
            viewModel = new ViewModel();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            ival_ProjectBuildingMaster ObjBuilding = new ival_ProjectBuildingMaster();
            try
            {
                hInputPara.Add("@Building_ID", ID);
                DataTable dtBuilding = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GeProjectBuildingMasterByID", hInputPara);

                if (dtBuilding.Rows.Count > 0 && dtBuilding != null)
                {

                    object valueBuilding_ID = Convert.ToInt32(dtBuilding.Rows[0]["Building_ID"]);
                    if (dtBuilding.Columns.Contains("Building_ID") && valueBuilding_ID != null)
                        ObjBuilding.Building_ID = Convert.ToInt32(valueBuilding_ID);
                    else
                        ObjBuilding.Building_ID = 0;

                    object valueProject_ID = Convert.ToInt32(dtBuilding.Rows[0]["Project_ID"]);
                    if (dtBuilding.Columns.Contains("Project_ID") && valueProject_ID != null)
                        ObjBuilding.Project_ID = Convert.ToInt32(valueProject_ID);
                    else
                        ObjBuilding.Project_ID = 0;

                    object valueBuilding_Name = dtBuilding.Rows[0]["Building_Name"];
                    if (dtBuilding.Columns.Contains("Building_Name") && valueBuilding_Name != null)
                        ObjBuilding.Building_Name = Convert.ToString(dtBuilding.Rows[0]["Building_Name"]);
                    else
                        ObjBuilding.Building_Name = string.Empty;

                    object valueNo_of_Wings = dtBuilding.Rows[0]["No_of_Wings"];
                    if (dtBuilding.Columns.Contains("No_of_Wings") && valueNo_of_Wings != null)
                        ObjBuilding.No_of_Wings = Convert.ToString(dtBuilding.Rows[0]["No_of_Wings"]);
                    else
                        ObjBuilding.No_of_Wings = string.Empty;

                    object valueApproved_Building_Area = dtBuilding.Rows[0]["Approved_Building_Area"];
                    if (dtBuilding.Columns.Contains("Approved_Building_Area") && valueApproved_Building_Area != null)
                        ObjBuilding.Approved_Building_Area = Convert.ToString(dtBuilding.Rows[0]["Approved_Building_Area"]);
                    else
                        ObjBuilding.Approved_Building_Area = string.Empty;

                    object valueApproved_No_of_Units = dtBuilding.Rows[0]["Approved_No_of_Units"];
                    if (dtBuilding.Columns.Contains("Approved_No_of_Units") && valueApproved_No_of_Units != null)
                        ObjBuilding.Approved_No_of_Units = Convert.ToString(dtBuilding.Rows[0]["Approved_No_of_Units"]);
                    else
                        ObjBuilding.Approved_No_of_Units = string.Empty;

                    object valueRemarks = dtBuilding.Rows[0]["Remarks"];
                    if (dtBuilding.Columns.Contains("Remarks") && valueRemarks != null)
                        ObjBuilding.Remarks = Convert.ToString(dtBuilding.Rows[0]["Remarks"]);
                    else
                        ObjBuilding.Remarks = string.Empty;

                    object valueCord_Latitutde = dtBuilding.Rows[0]["Cord_Latitutde"];
                    if (dtBuilding.Columns.Contains("Cord_Latitutde") && valueCord_Latitutde != null)
                        ObjBuilding.Cord_Latitutde = Convert.ToString(dtBuilding.Rows[0]["Cord_Latitutde"]);
                    else
                        ObjBuilding.Cord_Latitutde = string.Empty;

                    object valueCord_Longitude = dtBuilding.Rows[0]["Cord_Longitude"];
                    if (dtBuilding.Columns.Contains("Cord_Longitude") && valueCord_Latitutde != null)
                        ObjBuilding.Cord_Longitude = Convert.ToString(dtBuilding.Rows[0]["Cord_Longitude"]);
                    else
                        ObjBuilding.Cord_Longitude = string.Empty;

                    object valueDeviation_Percentage = dtBuilding.Rows[0]["Deviation_Percentage"];
                    if (dtBuilding.Columns.Contains("Deviation_Percentage") && valueCord_Latitutde != null)
                        viewModel.Deviation_Percentage = Convert.ToString(dtBuilding.Rows[0]["Deviation_Percentage"]);
                    else
                        viewModel.Deviation_Percentage = string.Empty;

                    object valueDeviation_Description = dtBuilding.Rows[0]["Deviation_Description"];
                    if (dtBuilding.Columns.Contains("Deviation_Description") && valueDeviation_Description != null)
                        ObjBuilding.Deviation_Description = Convert.ToString(dtBuilding.Rows[0]["Deviation_Description"]);
                    else
                        ObjBuilding.Deviation_Description = string.Empty;

                    if (ObjBuilding.Project_ID != 0)
                    {
                        viewModel.ProjectID = Convert.ToInt32(ObjBuilding.Project_ID);
                        List<ival_ProjectMaster> ival_ProjectMastersList = GetProjects();
                        var projectName = ival_ProjectMastersList.AsEnumerable().Where(p => p.Project_ID == ObjBuilding.Project_ID).Select(row => row.Project_Name).FirstOrDefault();
                        ViewBag.ProjectName = projectName;
                    }

                }
                else
                {
                    return Json("Error in Database !", JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
            viewModel.ival_ProjectBuildingMaster = ObjBuilding;
            viewModel.ival_ProjectBoundariesList = GetBuildingBoundariesListByBuildingID(ID);
            viewModel.ival_ProjectSideMarginList = GetSideMarginsListByBuildingID(ID);
            viewModel.ival_BuildingWingFloorDetailsList = GetBuildingWingFloorListByBuildingID(ID);
            viewModel.GetApprovalFlagList = GetApprovalFlagList();
            return View(viewModel);
        }

        public ActionResult EditBuilding(ViewModel viewModel)
        {
            // int buildingID = 0;
            try
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara = new Hashtable();

                hInputPara.Add("@Building_ID", viewModel.ival_ProjectBuildingMaster.Building_ID);
                hInputPara.Add("@Building_Name", viewModel.ival_ProjectBuildingMaster.Building_Name);
                hInputPara.Add("@No_Of_Wings", viewModel.ival_ProjectBuildingMaster.No_of_Wings);
                hInputPara.Add("@Approved_Building_Area", viewModel.ival_ProjectBuildingMaster.Approved_Building_Area);
                hInputPara.Add("@Approved_No_of_Units", viewModel.ival_ProjectBuildingMaster.Approved_No_of_Units);
                hInputPara.Add("@Remarks", viewModel.ival_ProjectBuildingMaster.Remarks);
                hInputPara.Add("@Cord_Longitude", viewModel.ival_ProjectBuildingMaster.Cord_Longitude);
                hInputPara.Add("@Cord_Latitutde", viewModel.ival_ProjectBuildingMaster.Cord_Latitutde);
                hInputPara.Add("@Deviation_Percentage", viewModel.Deviation_Percentage);
                hInputPara.Add("@Deviation_Description", viewModel.ival_ProjectBuildingMaster.Deviation_Description);
                hInputPara.Add("@Updated_By", "User");
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateWingMasterByBuildingID", hInputPara);

                if (Convert.ToInt32(result) == 1)
                {
                    try
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara = new Hashtable();

                        foreach (ival_BuildingWingFloorDetails ival_BuildingWingFloorDetails in viewModel.ival_BuildingWingFloorDetailsList)
                        {
                            hInputPara = new Hashtable();
                            sqlDataAccess = new SQLDataAccess();


                            hInputPara.Add("@BuildingFloor_ID", ival_BuildingWingFloorDetails.BuildingFloor_ID);
                            hInputPara.Add("@FloorType_ID", ival_BuildingWingFloorDetails.FloorType_ID);
                            hInputPara.Add("@Number_Of_Floors", ival_BuildingWingFloorDetails.Number_Of_Floors);

                            var res = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateBuildingWingFloorDetails", hInputPara);
                        }

                        foreach (ival_ProjectSideMargin ival_ProjectSideMargin1 in viewModel.ival_ProjectSideMarginList)
                        {
                            hInputPara = new Hashtable();
                            sqlDataAccess = new SQLDataAccess();


                            hInputPara.Add("@Side_Margin_ID", ival_ProjectSideMargin1.Side_Margin_ID);
                            hInputPara.Add("@As_per_Approved", ival_ProjectSideMargin1.As_per_Approved);
                            hInputPara.Add("@As_per_Measurement", ival_ProjectSideMargin1.As_per_Measurement);
                            hInputPara.Add("@Percentage_Deviation", ival_ProjectSideMargin1.Percentage_Deviation);
                            hInputPara.Add("@Deviation_Description", ival_ProjectSideMargin1.@Deviation_Description);

                            sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectSideMarginsByID", hInputPara);
                        }

                        foreach (ival_ProjectBoundaries ival_ProjectBoundaries1 in viewModel.ival_ProjectBoundariesList)
                        {
                            hInputPara = new Hashtable();
                            sqlDataAccess = new SQLDataAccess();


                            hInputPara.Add("@Boundry_ID", ival_ProjectBoundaries1.Boundry_ID);
                            hInputPara.Add("@AsPer_Document", ival_ProjectBoundaries1.AsPer_Document);
                            hInputPara.Add("@AsPer_Site", ival_ProjectBoundaries1.AsPer_Site);

                            sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectBoundariesByID", hInputPara);
                        }

                        return RedirectToAction("Index", "Project");
                    }

                    catch (Exception ex)
                    {
                        return Json(ex.Message, JsonRequestBehavior.AllowGet);
                        // throw;
                    }
                }

                else
                {
                    return Json("Error in Database !", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //e.InnerException.ToString();
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                //return View(viewModel);
            }
        }
        [HttpGet]
        public ActionResult DeleteBilding(int id)
        {
            try
            {
                sqlDataAccess = new SQLDataAccess();
                hInputPara = new Hashtable();
                hInputPara.Add("@Building_ID", id);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteProjectBuildingMasterID", hInputPara);
                result.ToString();
                if (Convert.ToInt32(result) == 1)
                {
                    return RedirectToAction("Index", "Project");
                }
                else
                {
                    return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }

        }

        // Priyanka Raut
        public ActionResult AddWing()
        {

            viewModel = new ViewModel();

            if (Session["ProjectID"] != null)
            {
                projectId = Convert.ToInt32(Session["projectID"].ToString());
                viewModel.ProjectID = projectId;
                List<ival_ProjectMaster> ival_ProjectMastersList = GetProjects();
                var projectName = ival_ProjectMastersList.AsEnumerable().Where(p => p.Project_ID == projectId).Select(row => row.Project_Name).FirstOrDefault();
                ViewBag.ProjectName = projectName;
            }
            if (Session["BuildingID"] != null)
            {
                buildingId = Convert.ToInt32(Session["BuildingID"].ToString());
                viewModel.BuildingID = buildingId;
                List<ival_ProjectBuildingMaster> ival_BuildingMastersList = GetBuildingListByProjectId(projectId);
                var buildingName = ival_BuildingMastersList.AsEnumerable().Where(p => p.Building_ID == buildingId).Select(row => row.Building_Name).FirstOrDefault();
                ViewBag.BuildingName = buildingName;
            }

            viewModel.ival_BuilderGroupMasters = GetBuilderMaster();
            viewModel.ival_BuilderCompanyMasters = GetCompanyBuilderMaster();
            viewModel.ival_LookupCategoryValuesProjecttype = GetProjectTypeMaster();
            viewModel.ival_LookupCategoryValuesunittype = GetChkboxunits();

            viewModel.ival_LookupCategoryValuesbasicInfra = GetBasicInfra();
            viewModel.ival_CommonAreaI = GetCommonAreas();
            viewModel.ival_CommonAreaI2 = GetCommonAreas2();
            viewModel.ival_CommonAreaI3 = GetCommonAreas3();
            viewModel.ival_LookupCategoryValuesSocialInfra = GetSocialInfra();
            viewModel.ival_LookupCategoryValuesconnectivity = GetConnectivity();
            viewModel.ival_ApprovalTypeList = GetApprovalTypeList();
            viewModel.ival_ProjectBoundariesList = GetBoundariesList();
            viewModel.ival_ProjectSideMarginList = GetSideMarginsList();
            viewModel.ival_BuildingWingFloorDetailsList = GetFloorType();
            viewModel.GetApprovalFlagList = GetApprovalFlagList();
            Session["Apprtype"] = viewModel.ival_ApprovalType;
            return View(viewModel);
        }
        // Priyanka Raut
        [HttpPost]
        public ActionResult AddWing(ViewModel viewModel)
        {
            int wingID = 0;
            try
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara = new Hashtable();

                hInputPara.Add("@Building_ID", viewModel.BuildingID);
                hInputPara.Add("@Wing_Name", viewModel.ival_ProjBldgWingMaster.Wing_Name);
                hInputPara.Add("@Approved_No_Of_Units", viewModel.ival_ProjBldgWingMaster.Approved_No_Of_Units);
                hInputPara.Add("@Approved_Area_Of_Wing", viewModel.Approved_Area_Of_Wing);
                hInputPara.Add("@Remarks", viewModel.ival_ProjBldgWingMaster.Remarks);
                hInputPara.Add("@Approval_Flag", viewModel.SelectedApprovalFlag);
                hInputPara.Add("@RERA_Registration_No", viewModel.ival_ProjBldgWingMaster.RERA_Registration_No);
                hInputPara.Add("@No_of_Buildings", viewModel.ival_ProjBldgWingMaster.No_of_Buildings);
                hInputPara.Add("@No_Of_Wings", viewModel.ival_ProjBldgWingMaster.No_Of_Wings);
                hInputPara.Add("@No_of_Floors", viewModel.ival_ProjBldgWingMaster.No_of_Floors);
                hInputPara.Add("@No_of_Units", viewModel.ival_ProjBldgWingMaster.No_of_Units);
                hInputPara.Add("@Expected_completion_date", viewModel.ival_ProjBldgWingMaster.Expected_completion_date);
                hInputPara.Add("@Deviation_Description", viewModel.Deviation_Description);
                hInputPara.Add("@Cord_Latitude", viewModel.Cord_Latitude);
                hInputPara.Add("@Cord_Longitude", viewModel.Cord_Longitude);
                hInputPara.Add("@Created_By", string.Empty);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingMaster", hInputPara);

                if (Convert.ToInt32(result) != 0)
                {
                    try
                    {
                        wingID = Convert.ToInt32(result);
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara = new Hashtable();


                        StringBuilder sb_BulkinsertBuildingWingFloor = new StringBuilder();
                        sb_BulkinsertBuildingWingFloor.AppendLine("<BulkBuildingWingFloorDetails>");

                        foreach (ival_BuildingWingFloorDetails _BuildingWingFloorDetails in viewModel.ival_BuildingWingFloorDetailsList)
                        {
                            sb_BulkinsertBuildingWingFloor.AppendLine("<BulkBuildingWingFloorDetailsData>");

                            sb_BulkinsertBuildingWingFloor.AppendLine("<Building_ID>" + 0 + "</Building_ID>");
                            sb_BulkinsertBuildingWingFloor.AppendLine("<Wing_ID>" + wingID + "</Wing_ID>");
                            sb_BulkinsertBuildingWingFloor.AppendLine("<FloorType_ID>" + _BuildingWingFloorDetails.FloorType_ID + "</FloorType_ID>");
                            sb_BulkinsertBuildingWingFloor.AppendLine("<Number_Of_Floors>" + _BuildingWingFloorDetails.Number_Of_Floors + "</Number_Of_Floors>");

                            sb_BulkinsertBuildingWingFloor.AppendLine("</BulkBuildingWingFloorDetailsData>");
                        }

                        sb_BulkinsertBuildingWingFloor.AppendLine("</BulkBuildingWingFloorDetails>");
                        string xmlData = sb_BulkinsertBuildingWingFloor.ToString().Replace("&", "&amp;").Replace(" <", "<").Replace("> ", ">");
                        hInputPara.Add("@InsertBuildingWingFloorXML", xmlData);


                        var resultBuildingWingFloor = sqlDataAccess.ExecuteStoreProcedure("usp_InsertBuildingWingFloorDetails", hInputPara);
                        if (Convert.ToInt32(resultBuildingWingFloor) == 1)
                        {
                            try
                            {
                                hInputPara = new Hashtable();
                                sqlDataAccess = new SQLDataAccess();
                                hInputPara = new Hashtable();

                                StringBuilder sb_BulkinsertProjectApprovalDetails = new StringBuilder();
                                sb_BulkinsertProjectApprovalDetails.AppendLine("<BulkProjectApprovalDetails>");

                                foreach (ival_ProjectApprovalDetails ival_ProjectApprovalDetails in viewModel.ival_ApprovalTypeList)
                                {
                                    sb_BulkinsertProjectApprovalDetails.AppendLine("<BulkProjectApprovalDetailsData>");

                                    sb_BulkinsertProjectApprovalDetails.AppendLine("<Project_ID>" + 0 + "</Project_ID>");
                                    sb_BulkinsertProjectApprovalDetails.AppendLine("<Building_ID>" + 0 + "</Building_ID>");
                                    sb_BulkinsertProjectApprovalDetails.AppendLine("<Wing_ID>" + wingID + "</Wing_ID>");
                                    sb_BulkinsertProjectApprovalDetails.AppendLine("<Approval_Type>" + ival_ProjectApprovalDetails.Approval_Type + "</Approval_Type>");
                                    sb_BulkinsertProjectApprovalDetails.AppendLine("<Approving_Authority>" + ival_ProjectApprovalDetails.Approving_Authority + "</Approving_Authority>");
                                    sb_BulkinsertProjectApprovalDetails.AppendLine("<Date_of_Approval>" + ival_ProjectApprovalDetails.Date_of_Approval + "</Date_of_Approval>");
                                    sb_BulkinsertProjectApprovalDetails.AppendLine("<Approval_Num>" + ival_ProjectApprovalDetails.Approval_Num + "</Approval_Num>");

                                    sb_BulkinsertProjectApprovalDetails.AppendLine("</BulkProjectApprovalDetailsData>");
                                }

                                sb_BulkinsertProjectApprovalDetails.AppendLine("</BulkProjectApprovalDetails>");
                                string xmlDataApproval = sb_BulkinsertProjectApprovalDetails.ToString().Replace("&", "&amp;").Replace(" <", "<").Replace("> ", ">");
                                hInputPara.Add("@InsertApprovalDetailsXML", xmlDataApproval);


                                var resultApprovalDetails = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectApprovalDetailsList", hInputPara);

                                if (Convert.ToInt32(resultApprovalDetails) == 1)
                                {
                                    try
                                    {
                                        hInputPara = new Hashtable();
                                        sqlDataAccess = new SQLDataAccess();
                                        hInputPara = new Hashtable();

                                        StringBuilder sb_BulkinsertProjectSideMarginDetails = new StringBuilder();
                                        sb_BulkinsertProjectSideMarginDetails.AppendLine("<BulkProjectSideMarginDetails>");

                                        foreach (ival_ProjectSideMargin ival_ProjectSideMargin in viewModel.ival_ProjectSideMarginList)
                                        {
                                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<BulkProjectSideMarginDetailsData>");

                                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<Project_ID>" + 0 + "</Project_ID>");
                                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<Building_ID>" + 0 + "</Building_ID>");
                                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<Wing_ID>" + wingID + "</Wing_ID>");
                                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<Unit_ID>" + 0 + "</Unit_ID>");
                                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<Side_Margin_Description>" + ival_ProjectSideMargin.Side_Margin_Description + "</Side_Margin_Description>");
                                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<As_per_Approved>" + ival_ProjectSideMargin.As_per_Approved + "</As_per_Approved>");
                                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<As_per_Measurement>" + ival_ProjectSideMargin.As_per_Measurement + "</As_per_Measurement>");
                                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<Percentage_Deviation>" + ival_ProjectSideMargin.Percentage_Deviation + "</Percentage_Deviation>");
                                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<Deviation_Description>" + ival_ProjectSideMargin.Deviation_Description + "</Deviation_Description>");
                                            sb_BulkinsertProjectSideMarginDetails.AppendLine("</BulkProjectSideMarginDetailsData>");
                                        }

                                        sb_BulkinsertProjectSideMarginDetails.AppendLine("</BulkProjectSideMarginDetails>");
                                        string xmlDataSideMargin = sb_BulkinsertProjectSideMarginDetails.ToString().Replace("&", "&amp;").Replace(" <", "<").Replace("> ", ">");
                                        hInputPara.Add("@InsertProjectSideMarginXML", xmlDataSideMargin);


                                        var resultSideMargin = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectSideMarginsList", hInputPara);
                                        if (Convert.ToInt32(resultSideMargin) == 1)
                                        {
                                            try
                                            {
                                                hInputPara = new Hashtable();
                                                sqlDataAccess = new SQLDataAccess();
                                                hInputPara = new Hashtable();

                                                StringBuilder sb_BulkinsertProjectBoundariesDetails = new StringBuilder();
                                                sb_BulkinsertProjectBoundariesDetails.AppendLine("<BulkProjectBoundariesDetails>");

                                                foreach (ival_ProjectBoundaries ival_ProjectBoundaries in viewModel.ival_ProjectBoundariesList)
                                                {
                                                    sb_BulkinsertProjectBoundariesDetails.AppendLine("<BulkProjectBoundariesDetailsData>");

                                                    sb_BulkinsertProjectBoundariesDetails.AppendLine("<Project_ID>" + 0 + "</Project_ID>");
                                                    sb_BulkinsertProjectBoundariesDetails.AppendLine("<Building_ID>" + 0 + "</Building_ID>");
                                                    sb_BulkinsertProjectBoundariesDetails.AppendLine("<Wing_ID>" + wingID + "</Wing_ID>");
                                                    sb_BulkinsertProjectBoundariesDetails.AppendLine("<Unit_ID>" + 0 + "</Unit_ID>");
                                                    sb_BulkinsertProjectBoundariesDetails.AppendLine("<Boundry_Name>" + ival_ProjectBoundaries.Boundry_Name + "</Boundry_Name>");
                                                    sb_BulkinsertProjectBoundariesDetails.AppendLine("<AsPer_Document>" + ival_ProjectBoundaries.AsPer_Document + "</AsPer_Document>");
                                                    sb_BulkinsertProjectBoundariesDetails.AppendLine("<AsPer_Site>" + ival_ProjectBoundaries.AsPer_Site + "</AsPer_Site>");

                                                    sb_BulkinsertProjectBoundariesDetails.AppendLine("</BulkProjectBoundariesDetailsData>");
                                                }

                                                sb_BulkinsertProjectBoundariesDetails.AppendLine("</BulkProjectBoundariesDetails>");
                                                string xmlDataBoundaries = sb_BulkinsertProjectBoundariesDetails.ToString().Replace("&", "&amp;").Replace(" <", "<").Replace("> ", ">");
                                                hInputPara.Add("@InsertProjectBoundariesXML", xmlDataBoundaries);

                                                var resultBoundaries = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectBoundariesList", hInputPara);
                                                if (Convert.ToInt32(resultBoundaries) == 1)
                                                {
                                                    return RedirectToAction("Index", "Project");
                                                }
                                                else
                                                {
                                                    //ModelState.AddModelError("Message", "Please Enter Data");
                                                    return View(viewModel);
                                                }
                                            }
                                            catch (Exception ex)
                                            {

                                                //ModelState.AddModelError("Message", ex.Message);
                                                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                        else
                                        {
                                            return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                        return Json(ex.Message, JsonRequestBehavior.AllowGet);
                                        //return View(viewModel);
                                    }
                                }
                                else
                                {
                                    return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                                }

                            }
                            catch (Exception ex)
                            {
                                //ModelState.AddModelError("Message", ex.Message);
                                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                                // return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                            //return View(viewModel);
                        }
                    }
                    catch (Exception ex)
                    {
                        return Json(ex.Message, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    //ModelState.AddModelError("Message", "Please Enter Data");
                    return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //e.InnerException.ToString();
                //ModelState.AddModelError("Message", ex.Message);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

            //return RedirectToAction("Index", "Project");
        }
        // Priyanka Raut
        [HttpGet]
        public ActionResult EditWing(int id)
        {
            if (string.IsNullOrEmpty(id.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //return View();
            }
            viewModel = new ViewModel();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            ival_ProjBldgWingMaster ObjWing = new ival_ProjBldgWingMaster();
            try
            {
                hInputPara.Add("@Wing_ID", id);
                DataTable dtWing = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBldgWingMasterByID", hInputPara);

                if (dtWing.Rows.Count > 0 && dtWing != null)
                {

                    object valueWing_ID = Convert.ToInt32(dtWing.Rows[0]["Wing_ID"]);
                    if (dtWing.Columns.Contains("Wing_ID") && valueWing_ID != null)
                        ObjWing.Wing_ID = Convert.ToInt32(valueWing_ID);
                    else
                        ObjWing.Wing_ID = 0;

                    object valueBuilding_ID = Convert.ToInt32(dtWing.Rows[0]["Building_ID"]);
                    if (dtWing.Columns.Contains("Building_ID") && valueBuilding_ID != null)
                        ObjWing.Building_ID = Convert.ToInt32(valueBuilding_ID);
                    else
                        ObjWing.Building_ID = 0;

                    object valueWing_Name = dtWing.Rows[0]["Wing_Name"];
                    if (dtWing.Columns.Contains("Wing_Name") && valueWing_ID != null)
                        ObjWing.Wing_Name = Convert.ToString(dtWing.Rows[0]["Wing_Name"]);
                    else
                        ObjWing.Wing_Name = string.Empty;

                    object valueApproved_No_Of_Units = dtWing.Rows[0]["Approved_No_Of_Units"];
                    if (dtWing.Columns.Contains("Approved_No_Of_Units") && valueApproved_No_Of_Units != null)
                        ObjWing.Approved_No_Of_Units = Convert.ToString(valueWing_ID);
                    else
                        ObjWing.Approved_No_Of_Units = string.Empty;

                    object valueApproved_Area_Of_Wing = dtWing.Rows[0]["Approved_Area_Of_Wing"];
                    if (dtWing.Columns.Contains("Approved_Area_Of_Wing") && valueWing_ID != null)
                        viewModel.Approved_Area_Of_Wing = Convert.ToString(dtWing.Rows[0]["Approved_Area_Of_Wing"]);
                    else
                        viewModel.Approved_Area_Of_Wing = string.Empty;

                    object valueRemarks = dtWing.Rows[0]["Remarks"];
                    if (dtWing.Columns.Contains("Remarks") && valueRemarks != null)
                        ObjWing.Remarks = Convert.ToString(dtWing.Rows[0]["Remarks"]);
                    else
                        ObjWing.Remarks = string.Empty;

                    object valueApproval_Flag = dtWing.Rows[0]["Approval_Flag"];
                    if (dtWing.Columns.Contains("Remarks") && valueApproval_Flag != null)
                        viewModel.SelectedApprovalFlag = Convert.ToString(dtWing.Rows[0]["Approval_Flag"]).TrimEnd();
                    else
                        viewModel.SelectedApprovalFlag = string.Empty;

                    object valueRERA_Registration_No = dtWing.Rows[0]["RERA_Registration_No"];
                    if (dtWing.Columns.Contains("RERA_Registration_No") && valueRERA_Registration_No != null)
                        ObjWing.RERA_Registration_No = Convert.ToString(dtWing.Rows[0]["RERA_Registration_No"]);
                    else
                        ObjWing.RERA_Registration_No = string.Empty;


                    object valueNo_of_Buildings = dtWing.Rows[0]["No_of_Buildings"];
                    if (dtWing.Columns.Contains("RERA_Registration_No") && valueNo_of_Buildings != null)
                        ObjWing.No_of_Buildings = Convert.ToString(dtWing.Rows[0]["No_of_Buildings"]);
                    else
                        ObjWing.No_of_Buildings = string.Empty;


                    object valueNo_Of_Wings = dtWing.Rows[0]["No_Of_Wings"];
                    if (dtWing.Columns.Contains("No_Of_Wings") && valueNo_Of_Wings != null)
                        ObjWing.No_Of_Wings = Convert.ToString(dtWing.Rows[0]["No_Of_Wings"]);
                    else
                        ObjWing.No_Of_Wings = string.Empty;


                    object valueNo_of_Floors = dtWing.Rows[0]["No_of_Floors"];
                    if (dtWing.Columns.Contains("No_of_Floors") && valueNo_of_Floors != null)
                        ObjWing.No_of_Floors = Convert.ToString(dtWing.Rows[0]["No_of_Floors"]);
                    else
                        ObjWing.No_of_Floors = string.Empty;


                    object valueNo_of_Units = dtWing.Rows[0]["No_of_Units"];
                    if (dtWing.Columns.Contains("No_of_Units") && valueNo_of_Units != null)
                        ObjWing.No_of_Units = Convert.ToString(dtWing.Rows[0]["No_of_Units"]);
                    else
                        ObjWing.No_of_Units = string.Empty;

                    object valueExpected_completion_date = dtWing.Rows[0]["Expected_completion_date"];
                    if (dtWing.Columns.Contains("Expected_completion_date") && valueExpected_completion_date != null)
                        ObjWing.Expected_completion_date = Convert.ToString(dtWing.Rows[0]["Expected_completion_date"]);
                    else
                        ObjWing.Expected_completion_date = string.Empty;


                    object valueDeviation_Description = dtWing.Rows[0]["Deviation_Description"];
                    if (dtWing.Columns.Contains("Deviation_Description") && valueDeviation_Description != null)
                        viewModel.Deviation_Description = Convert.ToString(dtWing.Rows[0]["Deviation_Description"]);
                    else
                        viewModel.Deviation_Description = string.Empty;

                    object valueCord_Latitude = dtWing.Rows[0]["Cord_Latitude"];
                    if (dtWing.Columns.Contains("Cord_Latitude") && valueCord_Latitude != null)
                        viewModel.Cord_Latitude = Convert.ToString(dtWing.Rows[0]["Cord_Latitude"]);
                    else
                        viewModel.Cord_Latitude = string.Empty;

                    object valueCord_Longitude = dtWing.Rows[0]["Cord_Longitude"];
                    if (dtWing.Columns.Contains("Cord_Longitude") && valueCord_Longitude != null)
                        viewModel.Cord_Longitude = Convert.ToString(dtWing.Rows[0]["Cord_Longitude"]);
                    else
                        viewModel.Cord_Longitude = string.Empty;

                    if (ObjWing.Building_ID != 0 && ObjWing.Building_ID != null)
                    {
                        if (Session["ProjectID"] != null)
                        {
                            projectId = Convert.ToInt32(Session["projectID"].ToString());
                            viewModel.ProjectID = projectId;
                            List<ival_ProjectMaster> ival_ProjectMastersList = GetProjects();
                            var projectName = ival_ProjectMastersList.AsEnumerable().Where(p => p.Project_ID == projectId).Select(row => row.Project_Name).FirstOrDefault();
                            ViewBag.ProjectName = projectName;
                        }

                        List<ival_ProjectBuildingMaster> ival_BuildingMastersList = GetBuildingListByProjectId(projectId);
                        var buildingName = ival_BuildingMastersList.AsEnumerable().Where(p => p.Building_ID == ObjWing.Building_ID).Select(row => row.Building_Name).FirstOrDefault();
                        ViewBag.BuildingName = buildingName;
                    }

                }
                else
                {
                    return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
            viewModel.ival_ProjBldgWingMaster = ObjWing;
            viewModel.ival_ApprovalTypeList = GetApprovalDetailsListByWingID(id);
            viewModel.ival_ProjectBoundariesList = GetBuildingBoundariesListByWingID(id);
            viewModel.ival_ProjectSideMarginList = GetSideMarginsListByWingID(id);
            viewModel.ival_BuildingWingFloorDetailsList = GetBuildingWingFloorListByWingID(id);
            viewModel.GetApprovalFlagList = GetApprovalFlagList();
            //return Json(new { redirecturl = "http://www.codeproject.com/" }, JsonRequestBehavior.AllowGet);
            return View(viewModel);
        }
        // Priyanka Raut
        public ActionResult UpdateWing(ViewModel viewModel)
        {
            int wingID = viewModel.ival_ProjBldgWingMaster.Wing_ID;
            try
            {
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara = new Hashtable();

                hInputPara.Add("@Wing_ID", wingID);
                hInputPara.Add("@Wing_Name", viewModel.ival_ProjBldgWingMaster.Wing_Name);
                hInputPara.Add("@Approved_No_Of_Units", viewModel.ival_ProjBldgWingMaster.Approved_No_Of_Units);
                hInputPara.Add("@Approved_Area_Of_Wing", viewModel.Approved_Area_Of_Wing);
                hInputPara.Add("@Remarks", viewModel.ival_ProjBldgWingMaster.Remarks);
                hInputPara.Add("@Approval_Flag", viewModel.SelectedApprovalFlag);
                hInputPara.Add("@RERA_Registration_No", viewModel.ival_ProjBldgWingMaster.RERA_Registration_No);
                hInputPara.Add("@No_of_Buildings", viewModel.ival_ProjBldgWingMaster.No_of_Buildings);
                hInputPara.Add("@No_Of_Wings", viewModel.ival_ProjBldgWingMaster.No_Of_Wings);
                hInputPara.Add("@No_of_Floors", viewModel.ival_ProjBldgWingMaster.No_of_Floors);
                hInputPara.Add("@No_of_Units", viewModel.ival_ProjBldgWingMaster.No_of_Units);
                hInputPara.Add("@Expected_completion_date", viewModel.ival_ProjBldgWingMaster.Expected_completion_date);
                hInputPara.Add("@Deviation_Description", viewModel.Deviation_Description);
                hInputPara.Add("@Cord_Latitude", viewModel.Cord_Latitude);
                hInputPara.Add("@Cord_Longitude", viewModel.Cord_Longitude);
                hInputPara.Add("@Updated_By", string.Empty);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateWingMasterByWingID", hInputPara);

                if (Convert.ToInt32(result) == 1)
                {
                    try
                    {

                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara = new Hashtable();

                        foreach (ival_BuildingWingFloorDetails ival_BuildingWingFloorDetails in viewModel.ival_BuildingWingFloorDetailsList)
                        {
                            hInputPara = new Hashtable();
                            sqlDataAccess = new SQLDataAccess();


                            hInputPara.Add("@BuildingFloor_ID", ival_BuildingWingFloorDetails.BuildingFloor_ID);
                            hInputPara.Add("@FloorType_ID", ival_BuildingWingFloorDetails.FloorType_ID);
                            hInputPara.Add("@Number_Of_Floors", ival_BuildingWingFloorDetails.Number_Of_Floors);

                            sqlDataAccess.ExecuteStoreProcedure("usp_UpdateBuildingWingFloorDetails", hInputPara);
                        }

                        try
                        {

                            foreach (ival_ProjectApprovalDetails ival_ProjectApprovalDetails1 in viewModel.ival_ApprovalTypeList)
                            {
                                hInputPara = new Hashtable();
                                sqlDataAccess = new SQLDataAccess();


                                hInputPara.Add("@PApproval_ID", ival_ProjectApprovalDetails1.PApproval_ID);
                                hInputPara.Add("@Approving_Authority", ival_ProjectApprovalDetails1.Approving_Authority);
                                hInputPara.Add("@Date_of_Approval", ival_ProjectApprovalDetails1.Date_of_Approval);
                                hInputPara.Add("@Approval_Num", ival_ProjectApprovalDetails1.@Approval_Num);

                                sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectApprovalDetailsByID", hInputPara);
                            }
                            try
                            {

                                foreach (ival_ProjectSideMargin ival_ProjectSideMargin1 in viewModel.ival_ProjectSideMarginList)
                                {
                                    hInputPara = new Hashtable();
                                    sqlDataAccess = new SQLDataAccess();


                                    hInputPara.Add("@Side_Margin_ID", ival_ProjectSideMargin1.Side_Margin_ID);
                                    hInputPara.Add("@As_per_Approved", ival_ProjectSideMargin1.As_per_Approved);
                                    hInputPara.Add("@As_per_Measurement", ival_ProjectSideMargin1.As_per_Measurement);
                                    hInputPara.Add("@Percentage_Deviation", ival_ProjectSideMargin1.Percentage_Deviation);
                                    hInputPara.Add("@Deviation_Description", ival_ProjectSideMargin1.@Deviation_Description);

                                    sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectSideMarginsByID", hInputPara);
                                }
                                try
                                {


                                    foreach (ival_ProjectBoundaries ival_ProjectBoundaries1 in viewModel.ival_ProjectBoundariesList)
                                    {
                                        hInputPara = new Hashtable();
                                        sqlDataAccess = new SQLDataAccess();


                                        hInputPara.Add("@Boundry_ID", ival_ProjectBoundaries1.Boundry_ID);
                                        hInputPara.Add("@AsPer_Document", ival_ProjectBoundaries1.AsPer_Document);
                                        hInputPara.Add("@AsPer_Site", ival_ProjectBoundaries1.AsPer_Site);

                                        sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectBoundariesByID", hInputPara);
                                    }


                                    return RedirectToAction("Index", "Project");

                                }
                                catch (Exception ex)
                                {

                                    //ModelState.AddModelError("Message", ex.Message);
                                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                                }

                            }
                            catch (Exception ex)
                            {

                                //ModelState.AddModelError("Message", ex.Message);
                                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                            }


                        }
                        catch (Exception ex)
                        {
                            //ModelState.AddModelError("Message", ex.Message);
                            return Json(ex.Message, JsonRequestBehavior.AllowGet);
                            //return RedirectToAction("Index", "Project");
                        }

                    }
                    catch (Exception ex)
                    {
                        return Json(ex.Message, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    // ModelState.AddModelError("Message", "Please Enter Data");
                    return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //e.InnerException.ToString();
                //ModelState.AddModelError("Message", ex.Message);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

        }
        // Priyanka Raut
        [HttpGet]
        public ActionResult DeleteWing(int id)
        {
            try
            {
                sqlDataAccess = new SQLDataAccess();
                hInputPara = new Hashtable();
                hInputPara.Add("@Wing_ID", id);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteProjBldgWingMasterID", hInputPara);
                result.ToString();
                if (Convert.ToInt32(result) == 1)
                {
                    return RedirectToAction("Index", "Project");
                }
                else
                {
                    return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddUnit()
        {
            viewModel = new ViewModel();
            if (Session["ProjectID"] != null)
            {
                projectId = Convert.ToInt32(Session["projectID"].ToString());
                viewModel.ProjectID = projectId;
                List<ival_ProjectMaster> ival_ProjectMastersList = GetProjects();
                var projectName = ival_ProjectMastersList.AsEnumerable().Where(p => p.Project_ID == projectId).Select(row => row.Project_Name).FirstOrDefault();
                ViewBag.ProjectName = projectName;
            }
            if (Session["BuildingID"] != null)
            {
                buildingId = Convert.ToInt32(Session["BuildingID"].ToString());
                viewModel.BuildingID = buildingId;
                List<ival_ProjectBuildingMaster> ival_BuildingMastersList = GetBuildingListByProjectId(projectId);
                var buildingName = ival_BuildingMastersList.AsEnumerable().Where(p => p.Building_ID == buildingId).Select(row => row.Building_Name).FirstOrDefault();
                ViewBag.BuildingName = buildingName;
            }
            if (Session["WingID"] != null)
            {
                wingId = Convert.ToInt32(Session["WingID"].ToString());
                viewModel.WingID = wingId;
                List<ival_ProjBldgWingMaster> ival_ProjBldgWingMastersList = GetWingListByBuildingID(buildingId);
                var wingName = ival_ProjBldgWingMastersList.AsEnumerable().Where(p => p.Wing_ID == wingId).Select(row => row.Wing_Name).FirstOrDefault();
                ViewBag.WingName = wingName;
            }

            viewModel.propertyTypeList = GetProjectTypeMasterByprojectId(projectId);
            viewModel.ival_LookupCategoryBoundaries = GetBoundaries();
            viewModel.ival_LookupCategorySideMargins = GetSideMargins();
            viewModel.viewFromUnitList = GetViewFromUnitList();
            return View(viewModel);
        }

        public List<ival_ProjectMaster> GetProjects()
        {
            List<ival_ProjectMaster> listProjects = new List<ival_ProjectMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtProjects = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjects");
            if (dtProjects.Rows.Count > 0)

            {
                for (int i = 0; i < dtProjects.Rows.Count; i++)
                {

                    listProjects.Add(new ival_ProjectMaster
                    {
                        Project_ID = Convert.ToInt32(dtProjects.Rows[i]["Project_ID"]),
                        Project_Name = Convert.ToString(dtProjects.Rows[i]["Project_Name"]),
                        Legal_City = Convert.ToString(dtProjects.Rows[i]["Legal_City"]),
                        Legal_State = Convert.ToString(dtProjects.Rows[i]["Legal_State"]),


                    });


                }
            }
            return listProjects;
        }

        public List<ival_BuilderGroupMaster> GetBuilderMaster()
        {
            List<ival_BuilderGroupMaster> ListBuilders = new List<ival_BuilderGroupMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuliderGroup");
            for (int i = 0; i < dtBuildermaster.Rows.Count; i++)
            {

                ListBuilders.Add(new ival_BuilderGroupMaster
                {
                    Builder_Group_Master_ID = Convert.ToInt32(dtBuildermaster.Rows[i]["Builder_Group_Master_ID"]),
                    BuilderGroupName = Convert.ToString(dtBuildermaster.Rows[i]["BuilderGroupName"])

                });


            }
            return ListBuilders;
        }

        [HttpPost]
        public List<ival_BuilderCompanyMaster> GetCompanyBuilderMaster()
        {
            List<ival_BuilderCompanyMaster> ListCompanyBuilders = new List<ival_BuilderCompanyMaster>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtCompBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBuliderCompanyName");
            for (int i = 0; i < dtCompBuildermaster.Rows.Count; i++)
            {

                ListCompanyBuilders.Add(new ival_BuilderCompanyMaster
                {
                    Builder_Company_Master_ID = Convert.ToInt32(dtCompBuildermaster.Rows[i]["Builder_Company_Master_ID"]),
                    Builder_Company_Name = Convert.ToString(dtCompBuildermaster.Rows[i]["Builder_Company_Name"])

                });


            }
            return ListCompanyBuilders;
        }

        [HttpPost]
        public List<ival_States> GetStates()
        {
            List<ival_States> ListStates = new List<ival_States>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtCompBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetStateByID");
            for (int i = 0; i < dtCompBuildermaster.Rows.Count; i++)
            {

                ListStates.Add(new ival_States
                {
                    State_ID = Convert.ToInt32(dtCompBuildermaster.Rows[i]["State_ID"]),
                    State_Name = Convert.ToString(dtCompBuildermaster.Rows[i]["State_Name"])

                });


            }
            return ListStates;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> Getpincode()
        {
            List<ival_LookupCategoryValues> Listpincode = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtCompBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPincode");
            for (int i = 0; i < dtCompBuildermaster.Rows.Count; i++)
            {

                Listpincode.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtCompBuildermaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtCompBuildermaster.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtCompBuildermaster.Rows[i]["Lookup_Value"])

                });


            }
            return Listpincode;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetProjectTypeMaster()
        {
            List<ival_LookupCategoryValues> Listprojtype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtprojecttypermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectType");
            for (int i = 0; i < dtprojecttypermaster.Rows.Count; i++)
            {

                Listprojtype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtprojecttypermaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtprojecttypermaster.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtprojecttypermaster.Rows[i]["Lookup_Value"])

                });


            }
            return Listprojtype;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetChkboxunits()
        {
            List<ival_LookupCategoryValues> Listunittype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtunittypermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitType");
            for (int i = 0; i < dtunittypermaster.Rows.Count; i++)
            {

                Listunittype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtunittypermaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtunittypermaster.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtunittypermaster.Rows[i]["Lookup_Value"])

                });


            }
            return Listunittype;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetSocialInfra()
        {
            List<ival_LookupCategoryValues> Listsocialinfra = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtsocialinfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSocialInfra");
            for (int i = 0; i < dtsocialinfra.Rows.Count; i++)
            {

                Listsocialinfra.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtsocialinfra.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtsocialinfra.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtsocialinfra.Rows[i]["Lookup_Value"])

                });


            }
            return Listsocialinfra;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetClassoflocality()
        {
            List<ival_LookupCategoryValues> ListClassoflocality = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtListClassoflocality = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalityType");
            for (int i = 0; i < dtListClassoflocality.Rows.Count; i++)
            {

                ListClassoflocality.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtListClassoflocality.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtListClassoflocality.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtListClassoflocality.Rows[i]["Lookup_Value"])

                });

            }
            return ListClassoflocality;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetBasicInfra()
        {
            List<ival_LookupCategoryValues> ListBasicInfra = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicInfraType");
            for (int i = 0; i < dtBasicInfra.Rows.Count; i++)
            {

                ListBasicInfra.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtBasicInfra.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtBasicInfra.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtBasicInfra.Rows[i]["Lookup_Value"])

                });

            }
            return ListBasicInfra;
        }



        [HttpPost]
        public List<ival_LookupCategoryValues> GetCommonAreas()
        {
            List<ival_LookupCategoryValues> ListBasicInfra = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCommonAreas");
            for (int i = 0; i < dtBasicInfra.Rows.Count; i++)
            {

                ListBasicInfra.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtBasicInfra.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtBasicInfra.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtBasicInfra.Rows[i]["Lookup_Value"])

                });

            }
            return ListBasicInfra;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetCommonAreas2()
        {
            List<ival_LookupCategoryValues> ListBasicInfra = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Getfacilities");
            for (int i = 0; i < dtBasicInfra.Rows.Count; i++)
            {

                ListBasicInfra.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtBasicInfra.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtBasicInfra.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtBasicInfra.Rows[i]["Lookup_Value"])

                });

            }
            return ListBasicInfra;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetCommonAreas3()
        {
            List<ival_LookupCategoryValues> ListBasicInfra = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBasicInfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRoofing");
            for (int i = 0; i < dtBasicInfra.Rows.Count; i++)
            {

                ListBasicInfra.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtBasicInfra.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtBasicInfra.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtBasicInfra.Rows[i]["Lookup_Value"])

                });

            }
            return ListBasicInfra;
        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetConnectivity()
        {
            List<ival_LookupCategoryValues> ListConnectivity = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtConnectivity = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalTransportType");
            for (int i = 0; i < dtConnectivity.Rows.Count; i++)
            {

                ListConnectivity.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtConnectivity.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtConnectivity.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtConnectivity.Rows[i]["Lookup_Value"])

                });

            }
            return ListConnectivity;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetApprovalType()
        {
            List<ival_LookupCategoryValues> ListApprovalType = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtApprovalType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovalType");
            for (int i = 0; i < dtApprovalType.Rows.Count; i++)
            {

                ListApprovalType.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtApprovalType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtApprovalType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtApprovalType.Rows[i]["Lookup_Value"])

                });

            }
            return ListApprovalType;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetApprovalType(int Project_ID)
        {
            List<ival_LookupCategoryValues> ListApprovalType = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtApprovalType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovalDetailsByProjectID");
            for (int i = 0; i < dtApprovalType.Rows.Count; i++)
            {

                ListApprovalType.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtApprovalType.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtApprovalType.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtApprovalType.Rows[i]["Lookup_Value"])

                });

            }
            return ListApprovalType;
        }



        [HttpPost]
        public List<ival_LookupCategoryValues> GetSideMargins()
        {
            List<ival_LookupCategoryValues> ListSideMargins = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtSideMargins = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSideMargin");
            for (int i = 0; i < dtSideMargins.Rows.Count; i++)
            {

                ListSideMargins.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtSideMargins.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtSideMargins.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtSideMargins.Rows[i]["Lookup_Value"])

                });

            }
            return ListSideMargins;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetBoundaries()
        {
            List<ival_LookupCategoryValues> ListBoundaries = new List<ival_LookupCategoryValues>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBoundaries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBoundryType");
            for (int i = 0; i < dtBoundaries.Rows.Count; i++)
            {

                ListBoundaries.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtBoundaries.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Value"])

                });

            }
            return ListBoundaries;
        }


        [HttpGet]
        public JsonResult getUnitEquipmentAsset()
        {

            ival_ProjectUnitDetails obj = new ival_ProjectUnitDetails();
            List<ival_ProjectUnitDetails> listProjectschecked = new List<ival_ProjectUnitDetails>();
            listProjectschecked = Getcheckedlistfromdb();

            return Json(listProjectschecked, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public List<ival_ProjectUnitDetails> Getcheckedlistfromdb()
        {
            int ID = Convert.ToInt32(Session["parojectid"]);
            List<ival_ProjectUnitDetails> listProjectschecked = new List<ival_ProjectUnitDetails>();
            hInputPara = new Hashtable();
            hInputPara.Add("@Project_ID", ID);
            sqlDataAccess = new SQLDataAccess();
            DataTable dtprojectscheckunits = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetcheckedUnitType", hInputPara);

            if (dtprojectscheckunits.Rows.Count > 0)

            {
                for (int i = 0; i < dtprojectscheckunits.Rows.Count; i++)
                {

                    listProjectschecked.Add(new ival_ProjectUnitDetails
                    {
                        Project_ID = Convert.ToInt32(dtprojectscheckunits.Rows[i]["Project_ID"]),
                        UType_ID = Convert.ToString(dtprojectscheckunits.Rows[i]["UType_ID"]),

                    });
                }
            }
            return listProjectschecked;
        }
        // Priyanka Raut
        [HttpPost]
        public List<ival_ProjectApprovalDetails> GetApprovalTypeList()
        {
            List<ival_ProjectApprovalDetails> ListApprovalType = new List<ival_ProjectApprovalDetails>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtApprovalType = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovalType");
            for (int i = 0; i < dtApprovalType.Rows.Count; i++)
            {

                ListApprovalType.Add(new ival_ProjectApprovalDetails
                {
                    Approval_Type = Convert.ToString(dtApprovalType.Rows[i]["Lookup_Value"])
                });

            }
            return ListApprovalType;
        }

        // Priyanka Raut
        [HttpPost]
        public List<ival_ProjectBoundaries> GetBoundariesList()
        {
            List<ival_ProjectBoundaries> ival_ProjectBoundariesList = new List<ival_ProjectBoundaries>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBoundaries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBoundryType");
            for (int i = 0; i < dtBoundaries.Rows.Count; i++)
            {

                ival_ProjectBoundariesList.Add(new ival_ProjectBoundaries
                {
                    Boundry_Name = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Value"])

                });

            }
            return ival_ProjectBoundariesList;
        }
        // Priyanka Raut
        public List<ival_ProjectSideMargin> GetSideMarginsList()
        {
            List<ival_ProjectSideMargin> ListSideMargins = new List<ival_ProjectSideMargin>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtSideMargins = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSideMargin");
            for (int i = 0; i < dtSideMargins.Rows.Count; i++)
            {
                ListSideMargins.Add(new ival_ProjectSideMargin
                {
                    Side_Margin_Description = Convert.ToString(dtSideMargins.Rows[i]["Lookup_Value"])
                });
            }
            return ListSideMargins;
        }

        // Priyanka Raut
        [HttpPost]
        public List<ival_BuildingWingFloorDetails> GetFloorType()
        {
            List<ival_LookupCategoryValues> ListFloors = new List<ival_LookupCategoryValues>();
            List<ival_BuildingWingFloorDetails> ival_BuildingWingFloorDetailsList = new List<ival_BuildingWingFloorDetails>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtBoundaries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorType");
            for (int i = 0; i < dtBoundaries.Rows.Count; i++)
            {
                ival_BuildingWingFloorDetailsList.Add(new ival_BuildingWingFloorDetails
                {
                    FloorType_ID = Convert.ToString(dtBoundaries.Rows[i]["Lookup_Value"])
                });
            }
            return ival_BuildingWingFloorDetailsList;
        }

        // Priyanka Raut
        [HttpPost]
        public IEnumerable<SelectListItem> GetApprovalFlagList()
        {
            var _ApprovalFlag = new List<SelectListItem>();
            string[] ApprovalFlagpArray = { "Yes", "No" };
            for (int i = 0; i < ApprovalFlagpArray.Length; i++)
            {

                _ApprovalFlag.Add(new SelectListItem()
                {
                    Text = Convert.ToString(ApprovalFlagpArray[i]),
                    Value = Convert.ToString(ApprovalFlagpArray[i])

                });
            }
            return _ApprovalFlag;
        }
        // Priyanka Raut
        [HttpPost]
        public JsonResult GetBuildingList(int projectID)
        {
            Session["ProjectID"] = projectID;
            List<ival_ProjectBuildingMaster> ival_ProjectBuildingMastersList = new List<ival_ProjectBuildingMaster>();


            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Project_ID", projectID);
            DataTable dtBuilding = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectBuildingMasterList", hInputPara);
            for (int i = 0; i < dtBuilding.Rows.Count; i++)
            {
                ival_ProjectBuildingMastersList.Add(new ival_ProjectBuildingMaster
                {
                    Building_ID = Convert.ToInt32(dtBuilding.Rows[i]["Building_ID"]),
                    Building_Name = Convert.ToString(dtBuilding.Rows[i]["Building_Name"]),
                    No_of_Wings = Convert.ToString(dtBuilding.Rows[i]["Building_Name"]),
                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(ival_ProjectBuildingMastersList);
            return Json(jsonProjectBuildingMaster);
        }
        // Priyanka Raut
        [HttpPost]
        public JsonResult GetWingList(int buildingID)
        {
            Session["BuildingID"] = buildingID;

            List<ival_ProjBldgWingMaster> ival_ProjBldgWingMastersList = new List<ival_ProjBldgWingMaster>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Building_ID", buildingID);
            DataTable dtBuilding = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBldgWingMasterList", hInputPara);
            for (int i = 0; i < dtBuilding.Rows.Count; i++)
            {
                ival_ProjBldgWingMastersList.Add(new ival_ProjBldgWingMaster
                {
                    Wing_ID = Convert.ToInt32(dtBuilding.Rows[i]["Wing_ID"]),
                    Wing_Name = Convert.ToString(dtBuilding.Rows[i]["Wing_Name"]),
                    No_Of_Wings = Convert.ToString(dtBuilding.Rows[i]["No_Of_Wings"])
                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(ival_ProjBldgWingMastersList);
            return Json(jsonProjectBuildingMaster);
        }
        // Priyanka Raut
        [HttpPost]
        public JsonResult GetUnitList(int wingID)
        {
            Session["WingID"] = wingID;
            List<ViewModel> viewModelsUnitList = new List<ViewModel>();


            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Wing_ID", wingID);
            DataTable dtBuilding = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBldgWingUnitMasterList", hInputPara);
            for (int i = 0; i < dtBuilding.Rows.Count; i++)
            {
                viewModelsUnitList.Add(new ViewModel
                {
                    Unit_ID = Convert.ToInt32(dtBuilding.Rows[i]["Unit_ID"]),
                    No_of_Units = Convert.ToString(dtBuilding.Rows[i]["UnitNo"]),
                    Wing_Name = Convert.ToString(dtBuilding.Rows[i]["Wing_Name"]),
                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(viewModelsUnitList);
            return Json(jsonProjectBuildingMaster);
        }
        // Priyanka Raut

        public List<ival_ProjectSideMargin> GetSideMarginsListByWingID(int WingID)
        {
            List<ival_ProjectSideMargin> ListSideMargins = new List<ival_ProjectSideMargin>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Wing_ID", WingID);
            DataTable dtSideMargins = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSideMarginByWingID", hInputPara);
            for (int i = 0; i < dtSideMargins.Rows.Count; i++)
            {
                ListSideMargins.Add(new ival_ProjectSideMargin
                {
                    Side_Margin_ID = Convert.ToInt32(dtSideMargins.Rows[i]["Side_Margin_ID"]),
                    Project_ID = Convert.ToInt32(dtSideMargins.Rows[i]["Project_ID"]),
                    Building_ID = Convert.ToInt32(dtSideMargins.Rows[i]["Building_ID"]),
                    Wing_ID = Convert.ToInt32(dtSideMargins.Rows[i]["Wing_ID"]),
                    Side_Margin_Description = Convert.ToString(dtSideMargins.Rows[i]["Side_Margin_Description"]),
                    As_per_Approved = Convert.ToString(dtSideMargins.Rows[i]["As_per_Approved"]),
                    As_per_Measurement = Convert.ToString(dtSideMargins.Rows[i]["As_per_Measurement"]),
                    Percentage_Deviation = Convert.ToString(dtSideMargins.Rows[i]["Percentage_Deviation"]),

                });
            }
            return ListSideMargins;
        }

        // Priyanka Raut
        public List<ival_ProjectApprovalDetails> GetApprovalDetailsListByWingID(int WingID)
        {
            List<ival_ProjectApprovalDetails> ProjectApprovalDetailsList = new List<ival_ProjectApprovalDetails>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Wing_ID", WingID);
            DataTable dtApproval = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovalDetailsByWingID", hInputPara);
            for (int i = 0; i < dtApproval.Rows.Count; i++)
            {
                ProjectApprovalDetailsList.Add(new ival_ProjectApprovalDetails
                {
                    PApproval_ID = Convert.ToInt32(dtApproval.Rows[i]["PApproval_ID"]),
                    Project_ID = Convert.ToInt32(dtApproval.Rows[i]["Project_ID"]),
                    Building_ID = Convert.ToInt32(dtApproval.Rows[i]["Building_ID"]),
                    Approval_Type = Convert.ToString(dtApproval.Rows[i]["Approval_Type"]),
                    Wing_ID = Convert.ToInt32(dtApproval.Rows[i]["Wing_ID"]),
                    Approving_Authority = Convert.ToString(dtApproval.Rows[i]["Approving_Authority"]),
                    Date_of_Approval = Convert.ToDateTime(dtApproval.Rows[i]["Date_of_Approval"]),
                    Approval_Num = Convert.ToString(dtApproval.Rows[i]["Approval_Num"])
                });
            }
            return ProjectApprovalDetailsList;
        }
        // Priyanka Raut
        public List<ival_BuildingWingFloorDetails> GetBuildingWingFloorListByWingID(int WingID)
        {
            List<ival_BuildingWingFloorDetails> ival_BuildingWingFloorDetailsList = new List<ival_BuildingWingFloorDetails>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Wing_ID", WingID);
            DataTable dtApproval = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetWingFloorDetailsByWingID", hInputPara);
            for (int i = 0; i < dtApproval.Rows.Count; i++)
            {
                ival_BuildingWingFloorDetailsList.Add(new ival_BuildingWingFloorDetails
                {

                    BuildingFloor_ID = Convert.ToInt32(dtApproval.Rows[i]["BuildingFloor_ID"]),
                    Building_ID = Convert.ToInt32(dtApproval.Rows[i]["Building_ID"]),
                    Wing_ID = Convert.ToInt32(dtApproval.Rows[i]["Wing_ID"]),
                    FloorType_ID = Convert.ToString(dtApproval.Rows[i]["FloorType_ID"]),
                    Number_Of_Floors = Convert.ToString(dtApproval.Rows[i]["Number_Of_Floors"]),

                });
            }
            return ival_BuildingWingFloorDetailsList;
        }
        // Priyanka Raut
        public List<ival_ProjectBoundaries> GetBuildingBoundariesListByWingID(int WingID)
        {
            List<ival_ProjectBoundaries> ival_ProjectBoundariesList = new List<ival_ProjectBoundaries>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Wing_ID", WingID);
            DataTable dtBoundries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBoundariesByWingID", hInputPara);
            for (int i = 0; i < dtBoundries.Rows.Count; i++)
            {
                ival_ProjectBoundariesList.Add(new ival_ProjectBoundaries
                {

                    Boundry_ID = Convert.ToInt32(dtBoundries.Rows[i]["Boundry_ID"]),
                    Project_ID = Convert.ToInt32(dtBoundries.Rows[i]["Project_ID"]),
                    Building_ID = Convert.ToInt32(dtBoundries.Rows[i]["Building_ID"]),
                    Wing_ID = Convert.ToInt32(dtBoundries.Rows[i]["Wing_ID"]),
                    Boundry_Name = Convert.ToString(dtBoundries.Rows[i]["Boundry_Name"]),
                    AsPer_Document = Convert.ToString(dtBoundries.Rows[i]["AsPer_Document"]),
                    AsPer_Site = Convert.ToString(dtBoundries.Rows[i]["AsPer_Site"])
                });
            }
            return ival_ProjectBoundariesList;
        }

        public List<ival_BuildingWingFloorDetails> GetBuildingWingFloorListByBuildingID(int BuildingID)
        {
            List<ival_BuildingWingFloorDetails> ival_BuildingWingFloorDetailsList = new List<ival_BuildingWingFloorDetails>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Building_ID", BuildingID);
            DataTable dtApproval = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetWingFloorDetailsByBuildingID", hInputPara);
            for (int i = 0; i < dtApproval.Rows.Count; i++)
            {
                ival_BuildingWingFloorDetailsList.Add(new ival_BuildingWingFloorDetails
                {

                    BuildingFloor_ID = Convert.ToInt32(dtApproval.Rows[i]["BuildingFloor_ID"]),
                    Building_ID = Convert.ToInt32(dtApproval.Rows[i]["Building_ID"]),
                    Wing_ID = Convert.ToInt32(dtApproval.Rows[i]["Wing_ID"]),
                    FloorType_ID = Convert.ToString(dtApproval.Rows[i]["FloorType_ID"]),
                    Number_Of_Floors = Convert.ToString(dtApproval.Rows[i]["Number_Of_Floors"]),

                });
            }
            return ival_BuildingWingFloorDetailsList;
        }

        public List<ival_ProjectBoundaries> GetBuildingBoundariesListByBuildingID(int BuildingID)
        {
            List<ival_ProjectBoundaries> ival_ProjectBoundariesList = new List<ival_ProjectBoundaries>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Building_ID", BuildingID);
            DataTable dtBoundries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBoundariesByBuildingID", hInputPara);
            for (int i = 0; i < dtBoundries.Rows.Count; i++)
            {
                ival_ProjectBoundariesList.Add(new ival_ProjectBoundaries
                {

                    Boundry_ID = Convert.ToInt32(dtBoundries.Rows[i]["Boundry_ID"]),
                    Project_ID = Convert.ToInt32(dtBoundries.Rows[i]["Project_ID"]),
                    Building_ID = Convert.ToInt32(dtBoundries.Rows[i]["Building_ID"]),
                    Wing_ID = Convert.ToInt32(dtBoundries.Rows[i]["Wing_ID"]),
                    Boundry_Name = Convert.ToString(dtBoundries.Rows[i]["Boundry_Name"]),
                    AsPer_Document = Convert.ToString(dtBoundries.Rows[i]["AsPer_Document"]),
                    AsPer_Site = Convert.ToString(dtBoundries.Rows[i]["AsPer_Site"])
                });
            }
            return ival_ProjectBoundariesList;
        }

        public List<ival_ProjectSideMargin> GetSideMarginsListByBuildingID(int BuildingID)
        {
            List<ival_ProjectSideMargin> ListSideMargins = new List<ival_ProjectSideMargin>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Building_ID", BuildingID);
            DataTable dtSideMargins = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSideMarginByBuildingID", hInputPara);
            for (int i = 0; i < dtSideMargins.Rows.Count; i++)
            {
                ListSideMargins.Add(new ival_ProjectSideMargin
                {
                    Side_Margin_ID = Convert.ToInt32(dtSideMargins.Rows[i]["Side_Margin_ID"]),
                    Project_ID = Convert.ToInt32(dtSideMargins.Rows[i]["Project_ID"]),
                    Building_ID = Convert.ToInt32(dtSideMargins.Rows[i]["Building_ID"]),
                    Wing_ID = Convert.ToInt32(dtSideMargins.Rows[i]["Wing_ID"]),
                    Side_Margin_Description = Convert.ToString(dtSideMargins.Rows[i]["Side_Margin_Description"]),
                    As_per_Approved = Convert.ToString(dtSideMargins.Rows[i]["As_per_Approved"]),
                    As_per_Measurement = Convert.ToString(dtSideMargins.Rows[i]["As_per_Measurement"]),
                    Percentage_Deviation = Convert.ToString(dtSideMargins.Rows[i]["Percentage_Deviation"]),

                });
            }
            return ListSideMargins;
        }

        //By Meenakshi vedpathak for approvalupdate
        public List<ival_ProjectApprovalDetails> GetApprovalDetails(int Project_ID)
        {
            List<ival_ProjectApprovalDetails> ProjectApprovalDetailsLists = new List<ival_ProjectApprovalDetails>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Project_ID", Project_ID);
            DataTable dtApproval = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetApprovalDetailsByProjectID", hInputPara);
            for (int i = 0; i < dtApproval.Rows.Count; i++)
            {
                ProjectApprovalDetailsLists.Add(new ival_ProjectApprovalDetails
                {
                    PApproval_ID = Convert.ToInt32(dtApproval.Rows[i]["PApproval_ID"]),
                    Project_ID = Convert.ToInt32(dtApproval.Rows[i]["Project_ID"]),
                    Building_ID = Convert.ToInt32(dtApproval.Rows[i]["Building_ID"]),
                    Approval_Type = Convert.ToString(dtApproval.Rows[i]["Approval_Type"]),
                    Wing_ID = Convert.ToInt32(dtApproval.Rows[i]["Wing_ID"]),
                    Approving_Authority = Convert.ToString(dtApproval.Rows[i]["Approving_Authority"]),
                    Date_of_Approval = Convert.ToDateTime(dtApproval.Rows[i]["Date_of_Approval"]),
                    Approval_Num = Convert.ToString(dtApproval.Rows[i]["Approval_Num"])
                });
            }
            return ProjectApprovalDetailsLists;
        }

        public List<ival_ProjectSideMargin> GetSideMarginsDetails(int Project_ID)
        {
            List<ival_ProjectSideMargin> ProjectsideLists = new List<ival_ProjectSideMargin>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Project_ID", Project_ID);
            DataTable dtsides = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSideMarginsByProjectID", hInputPara);
            for (int i = 0; i < dtsides.Rows.Count; i++)
            {
                ProjectsideLists.Add(new ival_ProjectSideMargin
                {
                    Side_Margin_ID = Convert.ToInt32(dtsides.Rows[i]["Side_Margin_ID"]),
                    As_per_Approved = Convert.ToString(dtsides.Rows[i]["As_per_Approved"]),
                    As_per_Measurement = Convert.ToString(dtsides.Rows[i]["As_per_Measurement"]),
                    Percentage_Deviation = Convert.ToString(dtsides.Rows[i]["Percentage_Deviation"]),
                    Deviation_Description = Convert.ToString(dtsides.Rows[i]["Deviation_Description"]),
                    Side_Margin_Description = Convert.ToString(dtsides.Rows[i]["Side_Margin_Description"]),

                });
            }
            return ProjectsideLists;
        }


        public List<ival_ProjectBoundaries> GetBoundryDetails(int Project_ID)
        {
            List<ival_ProjectBoundaries> ProjectboundryLists = new List<ival_ProjectBoundaries>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Project_ID", Project_ID);
            DataTable dtboundry = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBoundariesByProjectID", hInputPara);
            for (int i = 0; i < dtboundry.Rows.Count; i++)
            {
                ProjectboundryLists.Add(new ival_ProjectBoundaries
                {
                    Boundry_ID = Convert.ToInt32(dtboundry.Rows[i]["Boundry_ID"]),
                    Boundry_Name = Convert.ToString(dtboundry.Rows[i]["Boundry_Name"]),
                    AsPer_Document = Convert.ToString(dtboundry.Rows[i]["AsPer_Document"]),


                });
            }
            return ProjectboundryLists;
        }

        /// <summary>
        /// Priyanka Raut
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public JsonResult GetUnitTypeListByPropertyTypeID(int Lookup_ID)
        {


            List<ival_LookupCategoryValues> unitList = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@propertyId", Lookup_ID);
            DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitTypeByProperty_Id", hInputPara);
            for (int i = 0; i < dtrptmaster.Rows.Count; i++)
            {

                unitList.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtrptmaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtrptmaster.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtrptmaster.Rows[i]["Lookup_Value"])

                });


            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonvMUnitTypes = jsonSerialiser.Serialize(unitList);
            return Json(jsonvMUnitTypes);
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetReportType()
        {
            List<ival_LookupCategoryValues> Listrpttype = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetReportType");
            for (int i = 0; i < dtrptmaster.Rows.Count; i++)
            {

                Listrpttype.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtrptmaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtrptmaster.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtrptmaster.Rows[i]["Lookup_Value"])

                });


            }
            return Listrpttype;
        }

        public List<ival_ProjBldgWingMaster> GetWingListByBuildingID(Int32 buildingID)
        {

            List<ival_ProjBldgWingMaster> ival_ProjBldgWingMastersList = new List<ival_ProjBldgWingMaster>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Building_ID", buildingID);
            DataTable dtBuilding = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBldgWingMasterList", hInputPara);
            for (int i = 0; i < dtBuilding.Rows.Count; i++)
            {
                ival_ProjBldgWingMastersList.Add(new ival_ProjBldgWingMaster
                {
                    Wing_ID = Convert.ToInt32(dtBuilding.Rows[i]["Wing_ID"]),
                    Wing_Name = Convert.ToString(dtBuilding.Rows[i]["Wing_Name"]),
                    No_Of_Wings = Convert.ToString(dtBuilding.Rows[i]["No_Of_Wings"])
                });
            }


            return ival_ProjBldgWingMastersList;
        }
        public List<ival_ProjectBuildingMaster> GetBuildingListByProjectId(int projectID)
        {

            List<ival_ProjectBuildingMaster> ival_ProjectBuildingMastersList = new List<ival_ProjectBuildingMaster>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Project_ID", projectID);
            DataTable dtBuilding = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectBuildingMasterList", hInputPara);
            for (int i = 0; i < dtBuilding.Rows.Count; i++)
            {
                ival_ProjectBuildingMastersList.Add(new ival_ProjectBuildingMaster
                {
                    Building_ID = Convert.ToInt32(dtBuilding.Rows[i]["Building_ID"]),
                    Building_Name = Convert.ToString(dtBuilding.Rows[i]["Building_Name"]),
                    No_of_Wings = Convert.ToString(dtBuilding.Rows[i]["Building_Name"]),
                });
            }


            return ival_ProjectBuildingMastersList;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetProjectTypeMasterByprojectId(int projectID)
        {
            List<ival_LookupCategoryValues> propetyTypesList = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Project_ID", projectID);
            DataTable propertyTypeDT = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPropertyTypeByProjectId", hInputPara);

            if (propertyTypeDT.Rows.Count > 0 && propertyTypeDT != null)
            {
                for (int i = 0; i < propertyTypeDT.Rows.Count; i++)
                {

                    propetyTypesList.Add(new ival_LookupCategoryValues
                    {
                        Lookup_ID = Convert.ToInt32(propertyTypeDT.Rows[i]["Lookup_ID"]),
                        Lookup_Type = Convert.ToString(propertyTypeDT.Rows[i]["Lookup_Type"]),
                        Lookup_Value = Convert.ToString(propertyTypeDT.Rows[i]["Lookup_Value"])

                    });


                }

            }

            return propetyTypesList;
        }
        [HttpPost]
        public JsonResult GetUnitDetails(string UnitType)
        {
            try
            {
                List<ival_UnitParameterMaster> Listunits = new List<ival_UnitParameterMaster>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtUnitParameter = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitMaster", hInputPara);
                if (dtUnitParameter.Rows.Count > 0 && dtUnitParameter != null)
                {
                    for (int i = 0; i < dtUnitParameter.Rows.Count; i++)
                    {

                        Listunits.Add(new ival_UnitParameterMaster
                        {
                            UnitParamterID = Convert.ToInt32(dtUnitParameter.Rows[i]["UnitParamterID"]),
                            Name = Convert.ToString(dtUnitParameter.Rows[i]["Name"])

                        });

                    }

                }
                var jsonSerialiser = new JavaScriptSerializer();
                var jsonvUnitParameterMaster = jsonSerialiser.Serialize(Listunits);
                return Json(jsonvUnitParameterMaster);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }


        }
        [HttpPost]
        public List<ival_LookupCategoryValues> GetViewFromUnitList()
        {
            List<ival_LookupCategoryValues> viewFromUnitList = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("GetViewFromUnitList");
            for (int i = 0; i < dtrptmaster.Rows.Count; i++)
            {

                viewFromUnitList.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtrptmaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtrptmaster.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtrptmaster.Rows[i]["Lookup_Value"])

                });


            }
            return viewFromUnitList;
        }

        [HttpPost]

        public ActionResult SaveUnitDetailsMaster(string str, string str1, string str2, string str3)
        {
            Int32 unitId = 0;
            string viewFromUnitStr = string.Empty;
            if (Session["WingID"] != null)
            {
                wingId = Convert.ToInt32(Session["WingID"].ToString());
            }
            else
            {
                wingId = 0;
            }
            if (Session["ProjectID"] != null)
            {
                projectId = Convert.ToInt32(Session["ProjectID"].ToString());
            }
            else
            {
                projectId = 0;
            }
            if (Session["BuildingID"] != null)
            {
                buildingId = Convert.ToInt32(Session["BuildingID"].ToString());
            }
            else
            {
                buildingId = 0;
            }
            try
            {

                int unitNO1 = 0;
                DataTable UnitMaseterlist = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                DataTable UnitParameterlist1 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));
                DataTable Boundarieslist2 = (DataTable)JsonConvert.DeserializeObject(str3, (typeof(DataTable)));
                DataTable Sidemarginslist3 = (DataTable)JsonConvert.DeserializeObject(str2, (typeof(DataTable)));



                for (int i = 0; i < UnitParameterlist1.Rows.Count; i++)
                {
                    if (UnitParameterlist1.Columns.Contains("UnitParameter_Name"))
                    {
                        var item = UnitParameterlist1.Rows[i]["UnitParameter_Name"].ToString();
                        if (item == "Unit No" && item.ToString() != "")
                        {
                            unitNO1 = Convert.ToInt32(UnitParameterlist1.Rows[i]["UnitParameter_Value"].ToString());
                        }

                    }

                }

                if (UnitMaseterlist.Rows.Count > 0 && UnitMaseterlist != null && UnitMaseterlist.Columns.Contains("ViewFromUnit"))
                {
                    viewFromUnitStr = UnitMaseterlist.Rows[0]["ViewFromUnit"].ToString();
                }


                if (UnitMaseterlist.Rows.Count > 0 && UnitMaseterlist != null)
                {
                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    hInputPara.Add("@Wing_ID", wingId);
                    hInputPara.Add("@Project_ID", projectId);
                    hInputPara.Add("@Building_ID", buildingId);
                    hInputPara.Add("@UType", UnitMaseterlist.Rows[0]["UType"].ToString());
                    hInputPara.Add("@PTypeName", UnitMaseterlist.Rows[0]["PTypeName"].ToString());
                    hInputPara.Add("@UnitNo", Convert.ToInt32(unitNO1));
                    hInputPara.Add("@Deviation_Description", UnitMaseterlist.Rows[0]["Deviation_Description"].ToString());
                    hInputPara.Add("@Cord_Latitude", UnitMaseterlist.Rows[0]["Cord_Latitude"].ToString());
                    hInputPara.Add("@Cord_Longitude", UnitMaseterlist.Rows[0]["Cord_Longitude"].ToString());
                    hInputPara.Add("@Created_By", string.Empty);
                    var result = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectUnitMaster", hInputPara);

                    if (Convert.ToInt32(result.ToString()) != 0)
                    {
                        unitId = Convert.ToInt32(result.ToString());
                        Session["UnitId"] = unitId;

                        for (int i1 = 0; i1 < UnitParameterlist1.Rows.Count; i1++)
                        {
                            hInputPara = new Hashtable();
                            sqlDataAccess = new SQLDataAccess();
                            var UnitParameter_IDVal = UnitParameterlist1.Rows[i1]["UnitParameter_ID"];
                            var UnitParameter_Value = UnitParameterlist1.Rows[i1]["UnitParameter_Value"];
                            if (UnitParameter_IDVal.ToString() != "" && UnitParameter_Value.ToString() != "")
                            {
                                hInputPara.Add("@Unit_ID", unitId);
                                hInputPara.Add("@UnitParameter_ID", Convert.ToInt32(UnitParameterlist1.Rows[i1]["UnitParameter_ID"].ToString()));

                                hInputPara.Add("@UnitParameter_Value", UnitParameterlist1.Rows[i1]["UnitParameter_Value"].ToString());
                                var r1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertUnitParametersValue", hInputPara);
                            }

                        }
                        if (UnitMaseterlist.Rows[0]["UType"].ToString() == "Flat")
                        {
                            hInputPara = new Hashtable();
                            sqlDataAccess = new SQLDataAccess();
                            hInputPara.Add("@Unit_ID", unitId);
                            hInputPara.Add("@UnitParameter_ID", 8);
                            hInputPara.Add("@UnitParameter_Value", viewFromUnitStr.ToString());
                            var r11 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertUnitParametersValue", hInputPara);
                        }

                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara = new Hashtable();

                        StringBuilder sb_BulkinsertProjectSideMarginDetails = new StringBuilder();
                        sb_BulkinsertProjectSideMarginDetails.AppendLine("<BulkProjectSideMarginDetails>");

                        for (int i2 = 0; i2 < Sidemarginslist3.Rows.Count; i2++)
                        {
                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<BulkProjectSideMarginDetailsData>");

                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<Project_ID>" + projectId + "</Project_ID>");
                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<Building_ID>" + buildingId + "</Building_ID>");
                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<Wing_ID>" + wingId + "</Wing_ID>");
                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<Unit_ID>" + unitId + "</Unit_ID>");
                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<Side_Margin_Description>" + Sidemarginslist3.Rows[i2]["SidemarginsType"].ToString() + "</Side_Margin_Description>");
                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<As_per_Approved>" + Sidemarginslist3.Rows[i2]["Asperapprovedplan"].ToString() + "</As_per_Approved>");
                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<As_per_Measurement>" + Sidemarginslist3.Rows[i2]["Aspermeasurement"].ToString() + "</As_per_Measurement>");
                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<Percentage_Deviation>" + Sidemarginslist3.Rows[i2]["perdevistion"].ToString() + "</Percentage_Deviation>");
                            sb_BulkinsertProjectSideMarginDetails.AppendLine("<Deviation_Description>" + Sidemarginslist3.Rows[i2]["perdevistion"].ToString() + "</Deviation_Description>");
                            sb_BulkinsertProjectSideMarginDetails.AppendLine("</BulkProjectSideMarginDetailsData>");
                        }

                        sb_BulkinsertProjectSideMarginDetails.AppendLine("</BulkProjectSideMarginDetails>");
                        string xmlDataSideMargin = sb_BulkinsertProjectSideMarginDetails.ToString().Replace("&", "&amp;").Replace(" <", "<").Replace("> ", ">");
                        hInputPara.Add("@InsertProjectSideMarginXML", xmlDataSideMargin);


                        var resultSideMargin = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectSideMarginsList", hInputPara);



                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara = new Hashtable();

                        StringBuilder sb_BulkinsertProjectBoundariesDetails = new StringBuilder();
                        sb_BulkinsertProjectBoundariesDetails.AppendLine("<BulkProjectBoundariesDetails>");

                        for (int i3 = 0; i3 < Boundarieslist2.Rows.Count; i3++)
                        {
                            sb_BulkinsertProjectBoundariesDetails.AppendLine("<BulkProjectBoundariesDetailsData>");

                            sb_BulkinsertProjectBoundariesDetails.AppendLine("<Project_ID>" + projectId + "</Project_ID>");
                            sb_BulkinsertProjectBoundariesDetails.AppendLine("<Building_ID>" + buildingId + "</Building_ID>");
                            sb_BulkinsertProjectBoundariesDetails.AppendLine("<Wing_ID>" + wingId + "</Wing_ID>");
                            sb_BulkinsertProjectBoundariesDetails.AppendLine("<Unit_ID>" + unitId + "</Unit_ID>");
                            sb_BulkinsertProjectBoundariesDetails.AppendLine("<Boundry_Name>" + Boundarieslist2.Rows[i3]["BoundariesType"].ToString() + "</Boundry_Name>");
                            sb_BulkinsertProjectBoundariesDetails.AppendLine("<AsPer_Document>" + Boundarieslist2.Rows[i3]["asperdoc"].ToString() + "</AsPer_Document>");
                            sb_BulkinsertProjectBoundariesDetails.AppendLine("<AsPer_Site>" + Boundarieslist2.Rows[i3]["aspersite"].ToString() + "</AsPer_Site>");

                            sb_BulkinsertProjectBoundariesDetails.AppendLine("</BulkProjectBoundariesDetailsData>");
                        }

                        sb_BulkinsertProjectBoundariesDetails.AppendLine("</BulkProjectBoundariesDetails>");
                        string xmlDataBoundaries = sb_BulkinsertProjectBoundariesDetails.ToString().Replace("&", "&amp;").Replace(" <", "<").Replace("> ", ">");
                        hInputPara.Add("@InsertProjectBoundariesXML", xmlDataBoundaries);

                        var resultBoundaries = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectBoundariesList", hInputPara);
                    }
                }


                return Json(new { success = true, message = "Insert Successfully Unit Details !" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        // Priyanka Raut
        [HttpGet]
        public ActionResult DeleteUnit(int id)
        {
            try
            {
                sqlDataAccess = new SQLDataAccess();
                hInputPara = new Hashtable();
                hInputPara.Add("@Unit_ID", id);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteProjBldgWingUnitMasterID", hInputPara);
                result.ToString();
                if (Convert.ToInt32(result) == 1)
                {
                    return RedirectToAction("Index", "Project");
                }
                else
                {
                    return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult EditUnit(int id)
        {
            if (string.IsNullOrEmpty(id.ToString()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                //return View();
            }
            viewModel = new ViewModel();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            ival_ProjBldgWingUnitMaster objUnit = new ival_ProjBldgWingUnitMaster();
            try
            {
                hInputPara.Add("@Unit_ID", id);
                DataTable dtUnit = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjBldgWingUnitMasterByID", hInputPara);

                if (dtUnit.Rows.Count > 0 && dtUnit != null)
                {

                    object valueUnit_ID = Convert.ToInt32(dtUnit.Rows[0]["Unit_ID"]);
                    if (dtUnit.Columns.Contains("Unit_ID") && valueUnit_ID != null)
                    {
                        objUnit.Unit_ID = Convert.ToInt32(dtUnit.Rows[0]["Unit_ID"]);
                        Session["UnitId"] = objUnit.Unit_ID;
                    }
                    else
                        objUnit.Unit_ID = 0;

                    object valueProjectID = Convert.ToInt32(dtUnit.Rows[0]["Project_ID"]);
                    if (dtUnit.Columns.Contains("Project_ID") && valueProjectID != null && valueProjectID.ToString() != "")
                        viewModel.ProjectID = Convert.ToInt32(dtUnit.Rows[0]["Project_ID"]);
                    else
                        viewModel.ProjectID = 0;

                    object valueBuilding_ID = Convert.ToInt32(dtUnit.Rows[0]["Building_ID"]);
                    if (dtUnit.Columns.Contains("Building_ID") && valueBuilding_ID.ToString() != "")
                        viewModel.BuildingID = Convert.ToInt32(dtUnit.Rows[0]["Building_ID"]);
                    else
                        viewModel.BuildingID = 0;

                    object valueWing_ID = Convert.ToInt32(dtUnit.Rows[0]["Wing_ID"]);
                    if (dtUnit.Columns.Contains("Wing_ID") && valueWing_ID.ToString() != "")
                        viewModel.WingID = Convert.ToInt32(dtUnit.Rows[0]["Wing_ID"]);
                    else
                        viewModel.WingID = 0;


                    object valueUType = dtUnit.Rows[0]["UType"];
                    if (dtUnit.Columns.Contains("UType") && valueUType.ToString() != "")
                        objUnit.UType = Convert.ToString(dtUnit.Rows[0]["UType"]);
                    else
                        objUnit.UType = string.Empty;

                    object valuePTypeName = dtUnit.Rows[0]["PTypeName"];
                    if (dtUnit.Columns.Contains("PTypeName") && valuePTypeName.ToString() != "")
                        objUnit.PTypeName = Convert.ToString(dtUnit.Rows[0]["PTypeName"]);
                    else
                        objUnit.PTypeName = string.Empty;

                    object valueUnitNo = dtUnit.Rows[0]["UnitNo"];
                    if (dtUnit.Columns.Contains("UnitNo") && valueUnitNo.ToString() != "")
                        objUnit.UnitNo = Convert.ToInt32(dtUnit.Rows[0]["UnitNo"]);
                    else
                        objUnit.UnitNo = 0;

                    object Deviation_Description = dtUnit.Rows[0]["Deviation_Description"];
                    if (dtUnit.Columns.Contains("Deviation_Description") && valueUnitNo.ToString() != "")
                        viewModel.Deviation_Description = Convert.ToString(dtUnit.Rows[0]["Deviation_Description"]);
                    else
                        viewModel.Deviation_Description = string.Empty;

                    object Cord_Latitude = dtUnit.Rows[0]["Cord_Latitude"];
                    if (dtUnit.Columns.Contains("Cord_Latitude") && Cord_Latitude.ToString() != "")
                        viewModel.Cord_Latitude = Convert.ToString(dtUnit.Rows[0]["Cord_Latitude"]);
                    else
                        viewModel.Cord_Latitude = string.Empty;


                    object Cord_Longitude = dtUnit.Rows[0]["Cord_Longitude"];
                    if (dtUnit.Columns.Contains("Cord_Longitude") && Cord_Longitude.ToString() != "")
                        viewModel.Cord_Longitude = Convert.ToString(dtUnit.Rows[0]["Cord_Longitude"]);
                    else
                        viewModel.Cord_Longitude = string.Empty;

                    object total_Cost_of_Property = dtUnit.Rows[0]["Total_Cost_of_Property"];
                    if (dtUnit.Columns.Contains("Total_Cost_of_Property") && total_Cost_of_Property.ToString() != "")
                        viewModel.Total_Cost_of_Property = Convert.ToString(dtUnit.Rows[0]["Total_Cost_of_Property"]);
                    else
                        viewModel.Total_Cost_of_Property = string.Empty;

                    object stage_of_Construction = dtUnit.Rows[0]["Stage_of_Construction"];
                    if (dtUnit.Columns.Contains("Stage_of_Construction") && stage_of_Construction.ToString() != "")
                        viewModel.Stage_of_Construction = Convert.ToString(dtUnit.Rows[0]["Stage_of_Construction"]);
                    else
                        viewModel.Stage_of_Construction = string.Empty;

                    object stage_Description = dtUnit.Rows[0]["Stage_Description"];
                    if (dtUnit.Columns.Contains("Stage_Description") && stage_Description.ToString() != "")
                        viewModel.Stage_Description = Convert.ToString(dtUnit.Rows[0]["Stage_Description"]);
                    else
                        viewModel.Stage_Description = string.Empty;

                    object valuation_of_unit = dtUnit.Rows[0]["Valuation_of_unit"];
                    if (dtUnit.Columns.Contains("Valuation_of_unit") && valuation_of_unit.ToString() != "")
                        viewModel.Valuation_of_unit = Convert.ToString(dtUnit.Rows[0]["Valuation_of_unit"]);
                    else
                        viewModel.Valuation_of_unit = string.Empty;

                    object percent_Completed = dtUnit.Rows[0]["Percent_Completed"];
                    if (dtUnit.Columns.Contains("Percent_Completed") && percent_Completed.ToString() != "")
                        viewModel.Percent_Completed = Convert.ToString(dtUnit.Rows[0]["Percent_Completed"]);
                    else
                        viewModel.Percent_Completed = string.Empty;

                    object current_Value = dtUnit.Rows[0]["Current_Value"];
                    if (dtUnit.Columns.Contains("Current_Value") && current_Value.ToString() != "")
                        viewModel.Current_Value = Convert.ToString(dtUnit.Rows[0]["Current_Value"]);
                    else
                        viewModel.Current_Value = string.Empty;

                    object government_Rate = dtUnit.Rows[0]["Government_Rate"];
                    if (dtUnit.Columns.Contains("Cord_Longitude") && government_Rate.ToString() != "")
                    {
                        viewModel.Government_Rate = Convert.ToString(dtUnit.Rows[0]["Government_Rate"]);
                        viewModel.Government_RateSq = Convert.ToString(Convert.ToDouble(dtUnit.Rows[0]["Government_Value"]) * 10.764);
                    }
                    else
                    {
                        viewModel.Government_Rate = string.Empty;
                        viewModel.Government_RateSq = string.Empty;
                    }
                    object government_Value = dtUnit.Rows[0]["Government_Value"];
                    if (dtUnit.Columns.Contains("Cord_Longitude") && government_Value.ToString() != "")
                    {
                        viewModel.Government_Value = Convert.ToString(dtUnit.Rows[0]["Government_Value"]);
                        viewModel.Government_ValueSq = Convert.ToString(Convert.ToDouble(dtUnit.Rows[0]["Government_Value"]) * 10.764);
                    }
                    else
                    {
                        viewModel.Government_Value = string.Empty;
                        viewModel.Government_ValueSq = string.Empty;
                    }

                    object unitCostVal = dtUnit.Rows[0]["UnitCost"];
                    if (dtUnit.Columns.Contains("UnitCost") && unitCostVal.ToString() != "")
                        viewModel.UnitCost = Convert.ToString(dtUnit.Rows[0]["UnitCost"]);
                    else
                        viewModel.UnitCost = string.Empty;

                    List<ival_ProjectMaster> ival_ProjectMastersList = GetProjects();
                    var projectName = ival_ProjectMastersList.AsEnumerable().Where(p => p.Project_ID == viewModel.ProjectID).Select(row => row.Project_Name).FirstOrDefault();
                    ViewBag.ProjectName = projectName;


                    List<ival_ProjectBuildingMaster> ival_BuildingMastersList = GetBuildingListByProjectId(viewModel.ProjectID);
                    var buildingName = ival_BuildingMastersList.AsEnumerable().Where(p => p.Building_ID == viewModel.BuildingID).Select(row => row.Building_Name).FirstOrDefault();
                    ViewBag.BuildingName = buildingName;

                    List<ival_ProjBldgWingMaster> ival_ProjBldgWingMastersList = GetWingListByBuildingID(viewModel.BuildingID);
                    var wingName = ival_ProjBldgWingMastersList.AsEnumerable().Where(p => p.Wing_ID == viewModel.WingID).Select(row => row.Wing_Name).FirstOrDefault();
                    ViewBag.WingName = wingName;

                    var projectTypeList = GetProjectTypeMasterByprojectId(viewModel.ProjectID);
                    var propertyId = projectTypeList.AsEnumerable().Where(r => r.Lookup_Value == Convert.ToString(objUnit.PTypeName)).Select(p => p.Lookup_ID).FirstOrDefault();
                    viewModel.SelectedPropertyType = propertyId;

                    var vMUnitTypelist = GetUnitTypeListByPropertyTypeIDList(propertyId);
                    var unitId = vMUnitTypelist.AsEnumerable().Where(r => r.Lookup_Value == Convert.ToString(objUnit.UType)).Select(p => p.Lookup_ID).FirstOrDefault();
                    List<ival_LookupCategoryValues> unitIdList = vMUnitTypelist.Where(r => r.Lookup_Value == Convert.ToString(objUnit.UType)).ToList();
                    viewModel.selectedUnitID = unitId;
                    viewModel.unitTypeList = unitIdList;
                }

                viewModel.ival_ProjBldgWingUnitMaster1 = objUnit;

                viewModel.ival_ProjectBoundariesList = GetBuildingBoundariesListByUnitID(id);
                viewModel.ival_ProjectSideMarginList = GetSideMarginsListByUnitID(id);
                viewModel.propertyTypeList = GetProjectTypeMasterByprojectId(viewModel.ProjectID);

                viewModel.viewFromUnitList = GetViewFromUnitList();
                viewModel.vMUnitParameterDetailslist = GetUnitParameterDetailslist(id);
                viewModel.vMUnitParameterPriceDetailsList = GetUnitParameterPriceDetailsList(id);
                var UnitParameterDetailslist = GetUnitParameterDetailslist(id);
                var viewFromUnitDropdownValue = UnitParameterDetailslist.Where(row => row.Name == "View From Unit").Select(row => row.UnitParameter_Value).FirstOrDefault();
                if (viewFromUnitDropdownValue != null && viewFromUnitDropdownValue.ToString() != "0")
                {
                    var viewFromUnitList = GetViewFromUnitList();
                    var selectedViewFromUnitId = viewFromUnitList.AsEnumerable().Where(row => row.Lookup_Value == Convert.ToString(viewFromUnitDropdownValue)).Select(row => row.Lookup_ID).FirstOrDefault();
                    viewModel.SelectedViewFromUnit = Convert.ToInt32(selectedViewFromUnitId.ToString());
                }

            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult UpdateUnitDetailsMaster(string str, string str1, string str2, string str3)
        {
            Int32 unitId = 0;
            Int32 unitNoValue = 0;
            string viewFromUnitStr = string.Empty;
            DataTable UnitMaseterlist = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
            DataTable UnitParameterlist1 = (DataTable)JsonConvert.DeserializeObject(str1, (typeof(DataTable)));
            DataTable Boundarieslist2 = (DataTable)JsonConvert.DeserializeObject(str3, (typeof(DataTable)));
            DataTable Sidemarginslist3 = (DataTable)JsonConvert.DeserializeObject(str2, (typeof(DataTable)));

            if (Session["WingID"] != null)
            {
                wingId = Convert.ToInt32(Session["WingID"].ToString());
            }
            else
            {
                wingId = 0;
            }
            if (Session["ProjectID"] != null)
            {
                projectId = Convert.ToInt32(Session["ProjectID"].ToString());
            }
            else
            {
                projectId = 0;
            }
            if (Session["BuildingID"] != null)
            {
                buildingId = Convert.ToInt32(Session["BuildingID"].ToString());
            }
            else
            {
                buildingId = 0;
            }
            try
            {
                for (int i = 0; i < UnitParameterlist1.Rows.Count; i++)
                {
                    if (UnitParameterlist1.Columns.Contains("UnitParameter_Name"))
                    {
                        var item = UnitParameterlist1.Rows[i]["UnitParameter_Name"].ToString();
                        if (item == "Unit No" && item.ToString() != "")
                        {
                            unitNoValue = Convert.ToInt32(UnitParameterlist1.Rows[i]["UnitParameter_Value"].ToString());
                        }

                    }

                }

                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                hInputPara.Add("@Unit_ID", viewModel.ival_ProjBldgWingUnitMaster1.Unit_ID);
                hInputPara.Add("@UnitNo", unitNoValue);
                hInputPara.Add("@Deviation_Description", viewModel.Deviation_Description);
                hInputPara.Add("@Cord_Latitude", viewModel.Cord_Latitude);
                hInputPara.Add("@Cord_Longitude", viewModel.Cord_Longitude);
                hInputPara.Add("@Updated_By", string.Empty);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateUnitMasterByUnitID", hInputPara);

                if (Convert.ToInt32(result) == 1)
                {
                    try
                    {

                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara = new Hashtable();

                        for (int i1 = 0; i1 < UnitParameterlist1.Rows.Count; i1++)
                        {
                            hInputPara = new Hashtable();
                            sqlDataAccess = new SQLDataAccess();
                            var UnitParameter_IDVal = UnitParameterlist1.Rows[i1]["UnitParameter_ID"];
                            var UnitParameter_Value = UnitParameterlist1.Rows[i1]["UnitParameter_Value"];
                            if (UnitParameter_IDVal.ToString() != "" && UnitParameter_Value.ToString() != "")
                            {
                                //hInputPara.Add("@Unit_ID", unitId);
                                hInputPara.Add("@UnitParameter_ID", Convert.ToInt32(UnitParameterlist1.Rows[i1]["UnitParameter_ID"].ToString()));

                                hInputPara.Add("@UnitParameter_Value", UnitParameterlist1.Rows[i1]["UnitParameter_Value"].ToString());
                                var r1 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateUnitParametersValueByUnitID", hInputPara);
                            }

                        }
                        if (UnitMaseterlist.Rows[0]["UType"].ToString() == "Flat")
                        {
                            hInputPara = new Hashtable();
                            sqlDataAccess = new SQLDataAccess();
                            // hInputPara.Add("@Unit_ID", unitId);
                            hInputPara.Add("@UnitParameter_ID", 8);
                            hInputPara.Add("@UnitParameter_Value", viewFromUnitStr.ToString());
                            var r11 = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateUnitParametersValueByUnitID", hInputPara);
                        }

                        try
                        {
                            for (int i2 = 0; i2 < Sidemarginslist3.Rows.Count; i2++)
                            {

                                hInputPara = new Hashtable();
                                sqlDataAccess = new SQLDataAccess();
                                hInputPara.Add("@Side_Margin_ID", Sidemarginslist3.Rows[i2]["Side_Margin_ID"]);
                                hInputPara.Add("@As_per_Approved", Sidemarginslist3.Rows[i2]["As_per_Approved"]);
                                hInputPara.Add("@As_per_Measurement", Sidemarginslist3.Rows[i2]["As_per_Measurement"]);
                                hInputPara.Add("@Percentage_Deviation", Sidemarginslist3.Rows[i2]["Percentage_Deviation"]);
                                hInputPara.Add("@Deviation_Description", Sidemarginslist3.Rows[i2]["Deviation_Description"]);

                                sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectSideMarginsByID", hInputPara);
                            }

                            try
                            {

                                for (int i3 = 0; i3 < Boundarieslist2.Rows.Count; i3++)
                                {
                                    hInputPara = new Hashtable();
                                    sqlDataAccess = new SQLDataAccess();


                                    hInputPara.Add("@Boundry_ID", Boundarieslist2.Rows[i3]["Boundry_ID"]);
                                    hInputPara.Add("@AsPer_Document", Boundarieslist2.Rows[i3]["AsPer_Document"]);
                                    hInputPara.Add("@AsPer_Site", Boundarieslist2.Rows[i3]["AsPer_Site"]);

                                    sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectBoundariesByID", hInputPara);
                                }

                                return Json(new { success = true, message = "Update Successfully Unit Details !" }, JsonRequestBehavior.AllowGet);


                            }
                            catch (Exception ex)
                            {

                                //ModelState.AddModelError("Message", ex.Message);
                                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {

                            //ModelState.AddModelError("Message", ex.Message);
                            return Json(ex.Message, JsonRequestBehavior.AllowGet);
                        }




                    }
                    catch (Exception ex)
                    {
                        return Json(ex.Message, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    // ModelState.AddModelError("Message", "Please Enter Data");
                    return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //e.InnerException.ToString();
                //ModelState.AddModelError("Message", ex.Message);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult EditUnit(ViewModel viewModel)
        {

            //int wingID = viewModel.ival_ProjBldgWingMaster.Wing_ID;
            try
            {

                var unitNoValue = viewModel.vMUnitParameterDetailslist.Where(row => row.Name == "Unit No").Select(p => p.UnitParameter_Value).FirstOrDefault();

                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();

                hInputPara.Add("@Unit_ID", viewModel.ival_ProjBldgWingUnitMaster1.Unit_ID);
                hInputPara.Add("@UnitNo", unitNoValue);
                hInputPara.Add("@Deviation_Description", viewModel.Deviation_Description);
                hInputPara.Add("@Cord_Latitude", viewModel.Cord_Latitude);
                hInputPara.Add("@Cord_Longitude", viewModel.Cord_Longitude);
                hInputPara.Add("@Updated_By", string.Empty);
                var result = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateUnitMasterByUnitID", hInputPara);

                if (Convert.ToInt32(result) == 1)
                {
                    try
                    {

                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara = new Hashtable();

                        foreach (VMUnitParameterDetails vMUnit in viewModel.vMUnitParameterDetailslist)
                        {
                            hInputPara = new Hashtable();
                            sqlDataAccess = new SQLDataAccess();


                            hInputPara.Add("@UnitParaValue_ID", vMUnit.UnitParaValue_ID);
                            hInputPara.Add("@UnitParameter_Value", vMUnit.UnitParameter_Value);


                            sqlDataAccess.ExecuteStoreProcedure("usp_UpdateUnitParametersValueByUnitID", hInputPara);
                        }

                        try
                        {

                            foreach (ival_ProjectSideMargin ival_ProjectSideMargin1 in viewModel.ival_ProjectSideMarginList)
                            {
                                hInputPara = new Hashtable();
                                sqlDataAccess = new SQLDataAccess();


                                hInputPara.Add("@Side_Margin_ID", ival_ProjectSideMargin1.Side_Margin_ID);
                                hInputPara.Add("@As_per_Approved", ival_ProjectSideMargin1.As_per_Approved);
                                hInputPara.Add("@As_per_Measurement", ival_ProjectSideMargin1.As_per_Measurement);
                                hInputPara.Add("@Percentage_Deviation", ival_ProjectSideMargin1.Percentage_Deviation);
                                hInputPara.Add("@Deviation_Description", ival_ProjectSideMargin1.@Deviation_Description);

                                sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectSideMarginsByID", hInputPara);
                            }
                            try
                            {


                                foreach (ival_ProjectBoundaries ival_ProjectBoundaries1 in viewModel.ival_ProjectBoundariesList)
                                {
                                    hInputPara = new Hashtable();
                                    sqlDataAccess = new SQLDataAccess();


                                    hInputPara.Add("@Boundry_ID", ival_ProjectBoundaries1.Boundry_ID);
                                    hInputPara.Add("@AsPer_Document", ival_ProjectBoundaries1.AsPer_Document);
                                    hInputPara.Add("@AsPer_Site", ival_ProjectBoundaries1.AsPer_Site);

                                    sqlDataAccess.ExecuteStoreProcedure("usp_UpdateProjectBoundariesByID", hInputPara);
                                }


                                return RedirectToAction("Index", "Project");

                            }
                            catch (Exception ex)
                            {

                                //ModelState.AddModelError("Message", ex.Message);
                                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                            }

                        }
                        catch (Exception ex)
                        {

                            //ModelState.AddModelError("Message", ex.Message);
                            return Json(ex.Message, JsonRequestBehavior.AllowGet);
                        }




                    }
                    catch (Exception ex)
                    {
                        return Json(ex.Message, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    // ModelState.AddModelError("Message", "Please Enter Data");
                    return Json("Error in Database Record not Save!", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //e.InnerException.ToString();
                //ModelState.AddModelError("Message", ex.Message);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public List<VMUnitParameterDetails> GetUnitParameterDetailslist(int UnitId)
        {
            List<VMUnitParameterDetails> VMUnitParameterDetailsList = new List<VMUnitParameterDetails>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Unit_ID", UnitId);
            try
            {
                DataTable dtUnit = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitParameterByUnitID", hInputPara);
                if (dtUnit.Rows.Count > 0 && dtUnit != null)
                {
                    for (int i = 0; i < dtUnit.Rows.Count; i++)
                    {
                        VMUnitParameterDetailsList.Add(new VMUnitParameterDetails
                        {

                            UnitParaValue_ID = Convert.ToInt32(dtUnit.Rows[i]["UnitParaValue_ID"]),
                            Unit_ID = Convert.ToInt32(dtUnit.Rows[i]["Unit_ID"]),
                            UnitParameter_Value = Convert.ToString(dtUnit.Rows[i]["UnitParameter_Value"]),
                            Name = Convert.ToString(dtUnit.Rows[i]["Name"]),
                            UnitParameter_ID = Convert.ToInt32(dtUnit.Rows[i]["UnitParameter_ID"]),
                        });
                    }
                }
                return VMUnitParameterDetailsList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<VMUnitParameterPriceDetails> GetUnitParameterPriceDetailsList(int UnitId)
        {
            List<VMUnitParameterPriceDetails> vMUnitParameterPriceDetailsList = new List<VMUnitParameterPriceDetails>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Unit_ID", UnitId);
            try
            {
                DataTable dtUnit = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitPriceParameterByUnitID", hInputPara);
                if (dtUnit.Rows.Count > 0 && dtUnit != null)
                {
                    for (int i = 0; i < dtUnit.Rows.Count; i++)
                    {
                        vMUnitParameterPriceDetailsList.Add(new VMUnitParameterPriceDetails
                        {

                            UnitPriceParameterID = Convert.ToInt32(dtUnit.Rows[i]["UnitPriceParameterID"]),
                            UnitPriceParaValue_ID = Convert.ToInt32(dtUnit.Rows[i]["UnitPriceParaValue_ID"]),
                            UnitPriceParameter_Value = Convert.ToString(dtUnit.Rows[i]["UnitPriceParameter_Value"]),
                            UnitPriceParameter_ValueSq = Convert.ToString(Convert.ToDouble(dtUnit.Rows[i]["UnitPriceParameter_Value"]) * 10.764),

                            Name = Convert.ToString(dtUnit.Rows[i]["Name"])
                        }); ;
                    }
                }
                return vMUnitParameterPriceDetailsList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<ival_ProjectBoundaries> GetBuildingBoundariesListByUnitID(int UnitId)
        {
            List<ival_ProjectBoundaries> ival_ProjectBoundariesList = new List<ival_ProjectBoundaries>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Unit_ID", UnitId);
            try
            {
                DataTable dtBoundries = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBoundariesByUnitID", hInputPara);
                for (int i = 0; i < dtBoundries.Rows.Count; i++)
                {
                    ival_ProjectBoundariesList.Add(new ival_ProjectBoundaries
                    {

                        Boundry_ID = Convert.ToInt32(dtBoundries.Rows[i]["Boundry_ID"]),
                        Project_ID = Convert.ToInt32(dtBoundries.Rows[i]["Project_ID"]),
                        Building_ID = Convert.ToInt32(dtBoundries.Rows[i]["Building_ID"]),
                        Wing_ID = Convert.ToInt32(dtBoundries.Rows[i]["Wing_ID"]),
                        Unit_ID = Convert.ToInt32(dtBoundries.Rows[i]["Unit_ID"]),
                        Boundry_Name = Convert.ToString(dtBoundries.Rows[i]["Boundry_Name"]),
                        AsPer_Document = Convert.ToString(dtBoundries.Rows[i]["AsPer_Document"]),
                        AsPer_Site = Convert.ToString(dtBoundries.Rows[i]["AsPer_Site"])
                    });
                }

                return ival_ProjectBoundariesList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ival_ProjectSideMargin> GetSideMarginsListByUnitID(int UnitId)
        {
            List<ival_ProjectSideMargin> ListSideMargins = new List<ival_ProjectSideMargin>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Unit_ID", UnitId);
            DataTable dtSideMargins = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSideMarginByUnitID", hInputPara);
            for (int i = 0; i < dtSideMargins.Rows.Count; i++)
            {
                ListSideMargins.Add(new ival_ProjectSideMargin
                {
                    Side_Margin_ID = Convert.ToInt32(dtSideMargins.Rows[i]["Side_Margin_ID"]),
                    Project_ID = Convert.ToInt32(dtSideMargins.Rows[i]["Project_ID"]),
                    Building_ID = Convert.ToInt32(dtSideMargins.Rows[i]["Building_ID"]),
                    Wing_ID = Convert.ToInt32(dtSideMargins.Rows[i]["Wing_ID"]),
                    Unit_ID = Convert.ToInt32(dtSideMargins.Rows[i]["Unit_ID"]),
                    Side_Margin_Description = Convert.ToString(dtSideMargins.Rows[i]["Side_Margin_Description"]),
                    As_per_Approved = Convert.ToString(dtSideMargins.Rows[i]["As_per_Approved"]),
                    As_per_Measurement = Convert.ToString(dtSideMargins.Rows[i]["As_per_Measurement"]),
                    Percentage_Deviation = Convert.ToString(dtSideMargins.Rows[i]["Percentage_Deviation"]),

                });
            }
            return ListSideMargins;
        }

        public List<ival_UnitParameterMaster> GetUnitDetailsList(string UnitType)
        {
            try
            {
                List<ival_UnitParameterMaster> Listunits = new List<ival_UnitParameterMaster>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@UnitType", UnitType);
                DataTable dtBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitMaster", hInputPara);
                if (dtBuildermaster.Rows.Count > 0 && dtBuildermaster != null)
                {
                    for (int i = 0; i < dtBuildermaster.Rows.Count; i++)
                    {

                        Listunits.Add(new ival_UnitParameterMaster
                        {
                            UnitParamterID = Convert.ToInt32(dtBuildermaster.Rows[i]["UnitParamterID"]),
                            Name = Convert.ToString(dtBuildermaster.Rows[i]["Name"])

                        });

                    }

                }

                return Listunits;
            }
            catch (Exception ex)
            {
                return null;
            }


        }


        public List<ival_LookupCategoryValues> GetUnitTypeListByPropertyTypeIDList(int Lookup_ID)
        {


            List<ival_LookupCategoryValues> unitList = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@propertyId", Lookup_ID);
            DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitTypeByProperty_Id", hInputPara);
            for (int i = 0; i < dtrptmaster.Rows.Count; i++)
            {

                unitList.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtrptmaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtrptmaster.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtrptmaster.Rows[i]["Lookup_Value"])

                });


            }

            return unitList;
        }

        public JsonResult GetUnitPriceDetails(string UnitType, string PType)
        {
            try
            {
                List<ival_UniPriceParameterMaster> Listunits = new List<ival_UniPriceParameterMaster>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                hInputPara.Add("@UnitType", UnitType);
                hInputPara.Add("@PType", PType);
                DataTable dtUniPriceParameter = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUnitPriceMaster", hInputPara);
                if (dtUniPriceParameter.Rows.Count > 0 && dtUniPriceParameter != null)
                {
                    for (int i = 0; i < dtUniPriceParameter.Rows.Count; i++)
                    {

                        Listunits.Add(new ival_UniPriceParameterMaster
                        {
                            UnitPriceParameterID = Convert.ToInt32(dtUniPriceParameter.Rows[i]["UnitPriceParameterID"]),
                            Name = Convert.ToString(dtUniPriceParameter.Rows[i]["Name"])

                        });

                    }

                }
                var jsonSerialiser = new JavaScriptSerializer();
                var jsonvUnitParameterMaster = jsonSerialiser.Serialize(Listunits);
                return Json(jsonvUnitParameterMaster);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveUnitPrice(string unitDetailPriceStr, string unitPriceParameterstr)
        {
            string res = "";
            int unitId = 0;
            try
            {
                if (Session["UnitId"] != null)
                {
                    unitId = Convert.ToInt32(Session["UnitId"].ToString());
                }
                else
                {
                    unitId = 0;
                }

                DataTable unitPriceDetailsDT = (DataTable)JsonConvert.DeserializeObject(unitDetailPriceStr, (typeof(DataTable)));
                DataTable unitPriceParameterDT = (DataTable)JsonConvert.DeserializeObject(unitPriceParameterstr, (typeof(DataTable)));

                if (unitPriceDetailsDT.Rows.Count > 0 && unitPriceDetailsDT != null)
                {
                    var unitCost = unitPriceDetailsDT.Rows[0]["unitCost"].ToString();
                    var CostValueTotal = unitPriceDetailsDT.Rows[0]["CostValueTotal"].ToString();
                    var valuationOfUnit = unitPriceDetailsDT.Rows[0]["valuationOfUnit"].ToString();
                    var StageOfConstruction = unitPriceDetailsDT.Rows[0]["StageOfConstruction"].ToString();
                    var DescriptionofStageOfConstructionId = unitPriceDetailsDT.Rows[0]["DescriptionofStageOfConstructionId"].ToString();
                    var workCompleted = unitPriceDetailsDT.Rows[0]["workCompleted"].ToString();
                    var currentValueOfPropertyUnderConstructionUnit = unitPriceDetailsDT.Rows[0]["workCompleted"].ToString();
                    var governmentRateSqMt = unitPriceDetailsDT.Rows[0]["governmentRateSqMt"].ToString();
                    var governmentValueSqMt = unitPriceDetailsDT.Rows[0]["governmentValueSqMt"].ToString();

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    if (CostValueTotal.ToString() != "" && valuationOfUnit.ToString() != "" && StageOfConstruction.ToString() != "" && DescriptionofStageOfConstructionId.ToString() != "" && workCompleted.ToString() != "" && currentValueOfPropertyUnderConstructionUnit.ToString() != "" && governmentRateSqMt.ToString() != "" && governmentValueSqMt.ToString() != "" && unitCost.ToString() != "")
                    {
                        hInputPara.Add("@Unit_ID", unitId);
                        hInputPara.Add("@UnitCost", unitCost);
                        hInputPara.Add("@Total_Cost_of_Property", CostValueTotal);
                        hInputPara.Add("@Stage_of_Construction", StageOfConstruction);
                        hInputPara.Add("@Stage_Description", DescriptionofStageOfConstructionId);
                        hInputPara.Add("@Percent_Completed", workCompleted);
                        hInputPara.Add("@Current_Value", currentValueOfPropertyUnderConstructionUnit);
                        hInputPara.Add("@Government_Rate", governmentRateSqMt);
                        hInputPara.Add("@Valuation_of_unit", valuationOfUnit);
                        hInputPara.Add("@Government_Value", governmentValueSqMt);
                        hInputPara.Add("@Created_By", string.Empty);
                        var result = sqlDataAccess.ExecuteStoreProcedure("[usp_InsertProjectUnitPriceMaster]", hInputPara);
                        res = result.ToString();
                    }

                }



                if (unitPriceParameterDT.Rows.Count > 0 && unitPriceParameterDT != null && res == "1" || Convert.ToInt32(res) > 1)
                {
                    for (int i1 = 0; i1 < unitPriceParameterDT.Rows.Count; i1++)
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        var UnitParameter_IDVal = unitPriceParameterDT.Rows[i1]["UnitPriceParameter_ID"];
                        var UnitParameter_Value = unitPriceParameterDT.Rows[i1]["UnitPriceParameter_Value"];
                        if (UnitParameter_IDVal.ToString() != "" && UnitParameter_Value.ToString() != "")
                        {
                            hInputPara.Add("@Unit_ID", unitId);
                            hInputPara.Add("@UnitPriceParaValue_ID", Convert.ToInt32(unitPriceParameterDT.Rows[i1]["UnitPriceParameter_ID"].ToString()));

                            hInputPara.Add("@UnitPriceParameter_Value", unitPriceParameterDT.Rows[i1]["UnitPriceParameter_Value"].ToString());
                            var r1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertUnitPriceParametersValue", hInputPara);
                        }

                    }
                }
                return Json(new { success = true, message = "Insert Successfully Unit Price !" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateUnitPrice(string unitDetailPriceStr, string unitPriceParameterstr)
        {
            string res = "";
            int unitId = 0;
            try
            {
                if (Session["UnitId"] != null)
                {
                    unitId = Convert.ToInt32(Session["UnitId"].ToString());
                }
                else
                {
                    unitId = 0;
                }

                DataTable unitPriceDetailsDT = (DataTable)JsonConvert.DeserializeObject(unitDetailPriceStr, (typeof(DataTable)));
                DataTable unitPriceParameterDT = (DataTable)JsonConvert.DeserializeObject(unitPriceParameterstr, (typeof(DataTable)));

                if (unitPriceDetailsDT.Rows.Count > 0 && unitPriceDetailsDT != null)
                {
                    var unitCost = unitPriceDetailsDT.Rows[0]["unitCost"].ToString();
                    var CostValueTotal = unitPriceDetailsDT.Rows[0]["CostValueTotal"].ToString();
                    var valuationOfUnit = unitPriceDetailsDT.Rows[0]["valuationOfUnit"].ToString();
                    var StageOfConstruction = unitPriceDetailsDT.Rows[0]["StageOfConstruction"].ToString();
                    var DescriptionofStageOfConstructionId = unitPriceDetailsDT.Rows[0]["DescriptionofStageOfConstructionId"].ToString();
                    var workCompleted = unitPriceDetailsDT.Rows[0]["workCompleted"].ToString();
                    var currentValueOfPropertyUnderConstructionUnit = unitPriceDetailsDT.Rows[0]["workCompleted"].ToString();
                    var governmentRateSqMt = unitPriceDetailsDT.Rows[0]["governmentRateSqMt"].ToString();
                    var governmentValueSqMt = unitPriceDetailsDT.Rows[0]["governmentValueSqMt"].ToString();

                    hInputPara = new Hashtable();
                    sqlDataAccess = new SQLDataAccess();
                    if (CostValueTotal.ToString() != "" && valuationOfUnit.ToString() != "" && StageOfConstruction.ToString() != "" && DescriptionofStageOfConstructionId.ToString() != "" && workCompleted.ToString() != "" && currentValueOfPropertyUnderConstructionUnit.ToString() != "" && governmentRateSqMt.ToString() != "" && governmentValueSqMt.ToString() != "" && unitCost.ToString() != "")
                    {
                        hInputPara.Add("@Unit_ID", unitId);
                        hInputPara.Add("@UnitCost", unitCost);
                        hInputPara.Add("@Total_Cost_of_Property", CostValueTotal);
                        hInputPara.Add("@Stage_of_Construction", StageOfConstruction);
                        hInputPara.Add("@Stage_Description", DescriptionofStageOfConstructionId);
                        hInputPara.Add("@Percent_Completed", workCompleted);
                        hInputPara.Add("@Current_Value", currentValueOfPropertyUnderConstructionUnit);
                        hInputPara.Add("@Government_Rate", governmentRateSqMt);
                        hInputPara.Add("@Valuation_of_unit", valuationOfUnit);
                        hInputPara.Add("@Government_Value", governmentValueSqMt);
                        hInputPara.Add("@Created_By", string.Empty);
                        var result = sqlDataAccess.ExecuteStoreProcedure("[usp_InsertProjectUnitPriceMaster]", hInputPara);
                        res = result.ToString();
                    }

                }



                if (unitPriceParameterDT.Rows.Count > 0 && unitPriceParameterDT != null && res == "1" || Convert.ToInt32(res) > 1)
                {
                    for (int i1 = 0; i1 < unitPriceParameterDT.Rows.Count; i1++)
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        var UnitParameter_IDVal = unitPriceParameterDT.Rows[i1]["UnitPriceParameter_ID"];
                        var UnitParameter_Value = unitPriceParameterDT.Rows[i1]["UnitPriceParameter_Value"];
                        if (UnitParameter_IDVal.ToString() != "" && UnitParameter_Value.ToString() != "")
                        {
                            // hInputPara.Add("@Unit_ID", unitId);
                            hInputPara.Add("@UnitPriceParaValue_ID", Convert.ToInt32(unitPriceParameterDT.Rows[i1]["UnitPriceParaValue_ID"].ToString()));

                            hInputPara.Add("@UnitPriceParameter_Value", unitPriceParameterDT.Rows[i1]["UnitPriceParameter_Value"].ToString());
                            var r1 = sqlDataAccess.ExecuteStoreProcedure("[usp_UnitPriceParametersValueByUnitID]", hInputPara);
                        }

                    }
                }
                return Json(new { success = true, message = "Insert Successfully Unit Price !" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        //Rupali jadhav

        #region
        [HttpGet]
        public List<ival_LookupCategoryValues> GetProjectType()
        {
            List<ival_LookupCategoryValues> ListProjectType = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtCompBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLookupProjectType");
            for (int i = 0; i < dtCompBuildermaster.Rows.Count; i++)
            {

                ListProjectType.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtCompBuildermaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtCompBuildermaster.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtCompBuildermaster.Rows[i]["Lookup_Value"])

                });


            }
            return ListProjectType;
        }

        public JsonResult GetTypeOfIdbyPropertyId(string Lookup_Id)
        {
            List<ival_LookupCategoryValues> ival_Proplist = new List<ival_LookupCategoryValues>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@propertyId", Lookup_Id);
            DataTable dtprptype = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetType", hInputPara);
            for (int i = 0; i < dtprptype.Rows.Count; i++)
            {
                ival_Proplist.Add(new ival_LookupCategoryValues
                {
                    //Lookup_ID = Convert.ToInt32(dtprptype.Rows[i]["Lookup_ID"]),
                    Lookup_Value = Convert.ToString(dtprptype.Rows[i]["Lookup_Value"]),
                    Lookup_Type = Convert.ToString(dtprptype.Rows[i]["Lookup_Value"])
                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(ival_Proplist);
            return Json(jsonProjectBuildingMaster);
        }

        public JsonResult GetDistByStateId(int? State_Id)
        {
            List<ival_Districts> ival_Districts = new List<ival_Districts>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@State_ID", State_Id);
            DataTable dtdist = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDisticts", hInputPara);
            for (int i = 0; i < dtdist.Rows.Count; i++)
            {
                ival_Districts.Add(new ival_Districts
                {
                    District_ID = Convert.ToInt32(dtdist.Rows[i]["District_ID"]),
                    District_Name = Convert.ToString(dtdist.Rows[i]["District_Name"]),

                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectdistMaster = jsonSerialiser.Serialize(ival_Districts);
            return Json(jsonProjectdistMaster);
        }

        public JsonResult GetcitybydistId(int District_ID)
        {
            List<ival_Cities> ival_city = new List<ival_Cities>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@District_ID", District_ID);
            DataTable dtcity = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Getloc", hInputPara);
            for (int i = 0; i < dtcity.Rows.Count; i++)
            {
                ival_city.Add(new ival_Cities
                {
                    Loc_ID = Convert.ToInt32(dtcity.Rows[i]["Loc_ID"]),
                    Loc_Name = Convert.ToString(dtcity.Rows[i]["Loc_Name"]),

                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectcityMaster = jsonSerialiser.Serialize(ival_city);
            return Json(jsonProjectcityMaster);
        }



        public JsonResult GetsublocId(int District_ID)
        {
            List<ival_Cities> ival_city = new List<ival_Cities>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Subl_ID", District_ID);
            DataTable dtcity = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSublocality", hInputPara);
            for (int i = 0; i < dtcity.Rows.Count; i++)
            {
                ival_city.Add(new ival_Cities
                {
                    SubLoc_ID = Convert.ToInt32(dtcity.Rows[i]["SubLocality_Id"]),
                    SubLoc_Name = Convert.ToString(dtcity.Rows[i]["Sub_Locality"]),

                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectcityMaster = jsonSerialiser.Serialize(ival_city);
            return Json(jsonProjectcityMaster);
        }

        public JsonResult GetpinbyCityId(int City_ID)
        {
            List<ival_Cities> ival_pin = new List<ival_Cities>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@City_ID", City_ID);
            DataTable dtpin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPinCodeByLoc", hInputPara);
            for (int i = 0; i < dtpin.Rows.Count; i++)
            {
                ival_pin.Add(new ival_Cities
                {
                    Loc_ID = Convert.ToInt32(dtpin.Rows[i]["Loc_ID"]),

                    Pin_Code = Convert.ToString(dtpin.Rows[i]["Pin_Code"]),

                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectpinMaster = jsonSerialiser.Serialize(ival_pin);
            return Json(jsonProjectpinMaster);
        }

        public JsonResult GetCityId(int District_ID)
        {
            List<ival_Cities> ival_pin = new List<ival_Cities>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@District_ID", District_ID);
            DataTable dtpin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCityFind", hInputPara);
            for (int i = 0; i < dtpin.Rows.Count; i++)
            {
                ival_pin.Add(new ival_Cities
                {

                    City_ID = Convert.ToInt32(dtpin.Rows[i]["City_ID"]),
                    City_Name = Convert.ToString(dtpin.Rows[i]["City_Name"]),
                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectpinMaster = jsonSerialiser.Serialize(ival_pin);
            return Json(jsonProjectpinMaster);
        }


        public List<ival_LookupCategoryValues> GetTypeOfUnit()
        {
            List<ival_LookupCategoryValues> ListuspGetTypeOfUnit = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtCompBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTypeOfUnitByPropertyType");
            for (int i = 0; i < dtCompBuildermaster.Rows.Count; i++)
            {

                ListuspGetTypeOfUnit.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtCompBuildermaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtCompBuildermaster.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtCompBuildermaster.Rows[i]["Lookup_Value"])

                });


            }
            return ListuspGetTypeOfUnit;
        }

        public List<ival_LookupCategoryValues> GetRetailStatusOfProperty()
        {
            List<ival_LookupCategoryValues> ListRetailStatusProperty = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtCompBuildermaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRetailStatusOfProperty");
            for (int i = 0; i < dtCompBuildermaster.Rows.Count; i++)
            {

                ListRetailStatusProperty.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtCompBuildermaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtCompBuildermaster.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtCompBuildermaster.Rows[i]["Lookup_Value"])

                });


            }
            return ListRetailStatusProperty;
        }



        public ActionResult SavePropertyDetails()
        {
            ViewModel vm = new ViewModel();//Convert.ToInt32(Session["RequestID"]);
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Request_ID", Convert.ToInt32(Session["RequestID"].ToString()));
            DataTable dtpin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProperty", hInputPara);
            if (dtpin.Rows.Count > 0)
            {
                // Redirect("EditPropertydetailsViewNew/Project");
                return RedirectToAction("EditPropertydetailsViewNew", "Project", new { area = "Project" });
            }
            else
            {

                vm.ival_LookupCategoryValuesProjecttype = GetProjectTypeMaster();
                vm.ival_LookupCategoryValuesRetailStatusOfProperty = GetRetailStatusOfProperty();
                vm.ival_States = GetStates();
                vm.ival_LookupCategoryValuespincode = Getpincode();
                string Request_ID = Session["RequestID"].ToString();
                Session["Request_ID"] = Request_ID;
                // return RedirectToAction("SavePropertyDetails", "Project", new { area = "Project" });

            }
            return View(vm);
        }

        [HttpPost]
        public JsonResult GetRetailPropertyDetail(int PlotBunglowNumber)
        {
            List<ival_ProjectMaster_New> ival_Proplist = new List<ival_ProjectMaster_New>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@PlotBunglowNumber", PlotBunglowNumber);
            DataTable dtprptype = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRetailPropertyDetail", hInputPara);
            for (int i = 0; i < dtprptype.Rows.Count; i++)
            {
                ival_Proplist.Add(new ival_ProjectMaster_New
                {
                    Project_ID = Convert.ToInt32(dtprptype.Rows[i]["Project_ID"]),
                    PlotBunglowNumber = Convert.ToString(dtprptype.Rows[i]["PlotBunglowNumber"]),
                    Survey_Number = Convert.ToString(dtprptype.Rows[i]["Survey_Number"]),
                    Village_Name = Convert.ToString(dtprptype.Rows[i]["Village_Name"]),
                    City = Convert.ToString(dtprptype.Rows[i]["City"]),
                    State = Convert.ToString(dtprptype.Rows[i]["State"]),
                    PinCode = Convert.ToString(dtprptype.Rows[i]["PinCode"])


                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(ival_Proplist);
            return Json(jsonProjectBuildingMaster);
        }


        [HttpPost]
        public JsonResult GetRetailPropertyDetailsOfSingleUnit(string UnitNumber, string Village_Name)
        {
            List<ival_ProjectMaster_New> ival_Proplist = new List<ival_ProjectMaster_New>();

            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@UnitNumber", UnitNumber);
            //hInputPara.Add("@Project_Name", Project_Name);
            //hInputPara.Add("@Building_Name_RI", Building_Name_RI);
            //hInputPara.Add("@Wing_Name_RI", Wing_Name_RI);
            hInputPara.Add("@Village_Name", Village_Name);
            //hInputPara.Add("@City", City);
            //hInputPara.Add("@State", State);
            //hInputPara.Add("@PinCode", PinCode);
            DataTable dtprptype = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRetailPropertyDetailByUnitNumber", hInputPara);
            for (int i = 0; i < dtprptype.Rows.Count; i++)
            {
                ival_Proplist.Add(new ival_ProjectMaster_New
                {
                    UnitNumber = Convert.ToString(dtprptype.Rows[i]["UnitNumber"]),
                    Project_Name = Convert.ToString(dtprptype.Rows[i]["Project_Name"]),
                    Building_Name_RI = Convert.ToString(dtprptype.Rows[i]["Building_Name_RI"]),
                    Wing_Name_RI = Convert.ToString(dtprptype.Rows[i]["Wing_Name_RI"]),
                    // Survey_Number = Convert.ToString(dtprptype.Rows[i]["Survey_Number"]),
                    Village_Name = Convert.ToString(dtprptype.Rows[i]["Village_Name"]),
                    City = Convert.ToString(dtprptype.Rows[i]["City"]),
                    State = Convert.ToString(dtprptype.Rows[i]["State"]),
                    PinCode = Convert.ToString(dtprptype.Rows[i]["PinCode"])


                });
            }

            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(ival_Proplist);
            return Json(jsonProjectBuildingMaster);
        }



        [HttpPost]
        public ActionResult SaveAllRetailPropertyDetails(string str, string str4, string stringBasicInfra, string stringSocialDev, string stringtblCommonArea1, string stringtblCommonArea2, string stringtblCommonArea3, string stringfloor, string stringupper, string str3, string strground, string str5)
        {
            try
            {
                DataTable list = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                DataTable list4 = (DataTable)JsonConvert.DeserializeObject(str4, (typeof(DataTable)));
                DataTable listBasic = (DataTable)JsonConvert.DeserializeObject(stringBasicInfra, (typeof(DataTable)));
                DataTable listSocialDEv = (DataTable)JsonConvert.DeserializeObject(stringSocialDev, (typeof(DataTable)));
                DataTable listCommonArea1 = (DataTable)JsonConvert.DeserializeObject(stringtblCommonArea1, (typeof(DataTable)));
                DataTable listCommonArea2 = (DataTable)JsonConvert.DeserializeObject(stringtblCommonArea2, (typeof(DataTable)));
                DataTable listCommonArea3 = (DataTable)JsonConvert.DeserializeObject(stringtblCommonArea3, (typeof(DataTable)));
                DataTable listfloor = (DataTable)JsonConvert.DeserializeObject(stringfloor, (typeof(DataTable)));
                DataTable listupper = (DataTable)JsonConvert.DeserializeObject(stringupper, (typeof(DataTable)));
                DataTable listBasement = (DataTable)JsonConvert.DeserializeObject(str3, (typeof(DataTable)));
                DataTable listG = (DataTable)JsonConvert.DeserializeObject(strground, (typeof(DataTable)));
                DataTable listS = (DataTable)JsonConvert.DeserializeObject(str5, (typeof(DataTable)));
                // DataTable listsecond = (DataTable)JsonConvert.DeserializeObject(str6, (typeof(DataTable)));

                int Request_ID = Convert.ToInt32(Session["Request_ID"]);
                try
                {
                    for (int i = 0; i < list.Rows.Count; i++)
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        Session["UnitType"] = list.Rows[i]["ddlunittype"].ToString();
                        Session["PType"] = list.Rows[i]["ddlprojecttype"].ToString();

                        // DataTable dttop1RequestId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1RequestID");

                        //int Request_ID = 536;
                        hInputPara.Add("@Request_ID", Request_ID);
                        hInputPara.Add("@Property_Type", list.Rows[i]["ddlprojecttype"].ToString());
                        hInputPara.Add("@TypeOfSelect", list.Rows[i]["ddlselecttype"].ToString());
                        hInputPara.Add("@TypeOfUnit", list.Rows[i]["ddlunittype"].ToString());
                        hInputPara.Add("@StatusOfProperty", list.Rows[i]["ddlretailstatusproperty"].ToString());
                        hInputPara.Add("@Project_Name", list.Rows[i]["txtprojectname"].ToString());
                        hInputPara.Add("@Building_Name_RI", list.Rows[i]["txtbuildingnameri"].ToString());
                        hInputPara.Add("@Wing_Name_RI", list.Rows[i]["txtwingnameri"].ToString());
                        hInputPara.Add("@UnitNumber", list.Rows[i]["txtunitnumber"].ToString());
                        hInputPara.Add("@PlotBunglowNumber", list.Rows[i]["txtplotorbungalows"].ToString());
                        hInputPara.Add("@BunglowNumber", list.Rows[i]["txtbangalowno"].ToString());
                        //This for landmark Details

                        hInputPara.Add("@NearBy_Landmark", list.Rows[i]["txtNearBylandmark"].ToString() == "" ? list.Rows[i]["txtNearBylandmarkplot"].ToString() : list.Rows[i]["txtNearBylandmark"].ToString());
                        //hInputPara.Add("@NearBy_Landmark", list.Rows[i]["txtNearBylandmarkplot"].ToString());
                        hInputPara.Add("@Nearest_RailwayStation", list.Rows[i]["txtRailwaystation"].ToString() == "" ? list.Rows[i]["txtRailwaystationplot"].ToString() : list.Rows[i]["txtRailwaystation"].ToString());
                        //hInputPara.Add("@Nearest_RailwayStation", list.Rows[i]["txtRailwaystationplot"].ToString());
                        hInputPara.Add("@Nearest_BusStop", list.Rows[i]["txtBusStop"].ToString() == "" ? list.Rows[i]["txtBusStopplot"].ToString() : list.Rows[i]["txtBusStop"].ToString());
                        //hInputPara.Add("@Nearest_BusStop", list.Rows[i]["txtBusStopplot"].ToString());
                        hInputPara.Add("@Nearest_Hospital", list.Rows[i]["txtNearHospital"].ToString() == "" ? list.Rows[i]["txtNearHospitalplot"].ToString() : list.Rows[i]["txtNearHospital"].ToString());
                        // hInputPara.Add("@Nearest_Hospital", list.Rows[i]["txtNearHospitalplot"].ToString());
                        hInputPara.Add("@Nearest_Market", list.Rows[i]["txtNearMarket"].ToString() == "" ? list.Rows[i]["txtNearMarketplot"].ToString() : list.Rows[i]["txtNearMarket"].ToString());
                        //hInputPara.Add("@Nearest_Market", list.Rows[i]["txtNearMarketplot"].ToString());
                        hInputPara.Add("@Actual_No_Floors_G", list.Rows[i]["txtActualNoOfFlors"].ToString());
                        hInputPara.Add("@Floor_Of_Unit", list.Rows[i]["txtFloorofunit"].ToString());
                        hInputPara.Add("@Unit_on_Floor", list.Rows[i]["txtFloorofunitno"].ToString());
                        hInputPara.Add("@txtwingsno", list.Rows[i]["txtwingsno"].ToString());
                        hInputPara.Add("@Property_Description", list.Rows[i]["txtbriefdescription"].ToString());

                        hInputPara.Add("@Type_Of_Structure", list.Rows[i]["ddlprojecttypeRetail"].ToString());
                        if (list.Rows[i]["TypeOwnerShip"].ToString() == "Please Select")
                        {
                            hInputPara.Add("@Type_Ownership", list.Rows[i]["ddlOwnerBank10"].ToString());
                            hInputPara.Add("@Leased_Year", list.Rows[i]["txtbal_year"].ToString());

                        }
                        else
                        //if (list.Rows[i]["ddlOwnerBank10"].ToString() == "Please Select")
                        {
                            hInputPara.Add("@Type_Ownership", list.Rows[i]["TypeOwnerShip"].ToString());
                            hInputPara.Add("@Leased_Year", list.Rows[i]["leasedperiod"].ToString());
                        }
                        //hInputPara.Add("@Type_Ownership", list.Rows[i]["TypeOwnerShip"].ToString() == "Please select" ? list.Rows[i]["ddlOwnerBank10"].ToString() : list.Rows[i]["TypeOwnerShip"].ToString());
                        //hInputPara.Add("@Leased_Year", list.Rows[i]["leasedperiod"].ToString() == "" ? list.Rows[i]["txtbal_year"].ToString() : list.Rows[i]["leasedperiod"].ToString());


                        hInputPara.Add("@RERA_Registration_No", list.Rows[i]["txtRegistarionNumber"].ToString());
                        hInputPara.Add("@Approved_Usage_Of_Property", list.Rows[i]["ddlApprovedprojecttype"].ToString() == "Please select" ? list.Rows[i]["ddlApprovedprojecttypeplot"].ToString() : list.Rows[i]["ddlApprovedprojecttype"].ToString());
                        // hInputPara.Add("@Approved_Usage_Of_Property", list.Rows[i]["ddlApprovedprojecttypeplot"].ToString());
                        hInputPara.Add("@Actual_Usage_Of_Property", list.Rows[i]["ddlActualprojecttype"].ToString() == "Please select" ? list.Rows[i]["ddlActualprojecttypeplot"].ToString() : list.Rows[i]["ddlActualprojecttype"].ToString());
                        // hInputPara.Add("@Actual_Usage_Of_Property", list.Rows[i]["ddlActualprojecttypeplot"].ToString());
                        hInputPara.Add("@Current_Age_Of_Property", list.Rows[i]["txtCurrentAgeOfProp"].ToString() == "" ? list.Rows[i]["txtCurrentAgeOfPropplot"].ToString() : list.Rows[i]["txtCurrentAgeOfProp"].ToString());
                        // hInputPara.Add("@Current_Age_Of_Property", list.Rows[i]["txtCurrentAgeOfPropplot"].ToString());
                        hInputPara.Add("@Residual_Age_Of_Property", list.Rows[i]["txtResiduslAge"].ToString() == "" ? list.Rows[i]["txtResiduslAgeplot"].ToString() : list.Rows[i]["txtResiduslAge"].ToString());
                        // hInputPara.Add("@Residual_Age_Of_Property", list.Rows[i]["txtResiduslAgeplot"].ToString());
                        hInputPara.Add("@Common_Areas", list.Rows[i]["txtCommonAreas"].ToString() == "" ? list.Rows[i]["txtCommonAreasplot"].ToString() : list.Rows[i]["txtCommonAreas"].ToString());
                        //hInputPara.Add("@Common_Areas", list.Rows[i]["txtCommonAreasplot"].ToString());
                        hInputPara.Add("@Facilities", list.Rows[i]["txtfacilities"].ToString());
                        hInputPara.Add("@Anyother_Observation", list.Rows[i]["txtAnyOtherObservation"].ToString() == "" ? list.Rows[i]["txtAnyOtherObservationplot"].ToString() : list.Rows[i]["txtAnyOtherObservation"].ToString());
                        //hInputPara.Add("@Anyother_Observation", list.Rows[i]["txtAnyOtherObservationplot"].ToString());

                        hInputPara.Add("@Roofing_Terracing", list.Rows[i]["txtRoofingAndTerracing"].ToString());
                        hInputPara.Add("@Quality_Of_Fixture", list.Rows[i]["txtQualityOffixture"].ToString());
                        hInputPara.Add("@Quality_Of_Construction", list.Rows[i]["ddlQualityOfConstruction"].ToString());
                        hInputPara.Add("@Maintenance_Of_Property", list.Rows[i]["ddlMaintenanceOfProp"].ToString());
                        hInputPara.Add("@Marketability_Of_Property", list.Rows[i]["txtMarketability"].ToString() == "" ? list.Rows[i]["txtMarketabilityplot"].ToString() : list.Rows[i]["txtMarketability"].ToString());
                        //hInputPara.Add("@Marketability_Of_Property", list.Rows[i]["txtMarketabilityplot"].ToString());
                        if (Convert.ToString(list.Rows[i]["ddlOccupancyDetails"]) == "Please select")
                        {
                            hInputPara.Add("@Occupancy_Details", list.Rows[i]["ddlOccupancyDetails1"].ToString());
                            hInputPara.Add("@Expected_Monthly_Rental", list.Rows[i]["txtExpMonthlyRental1"].ToString());

                            hInputPara.Add("@Name_Of_Occupant", list.Rows[i]["txtNameOccipant1"].ToString());

                            hInputPara.Add("@Occupied_Since", list.Rows[i]["txtOccupiedsince1"].ToString());

                            hInputPara.Add("@Monthly_Rental", list.Rows[i]["txtMonthlyRental1"].ToString());

                            hInputPara.Add("@Balanced_Leased_Period", list.Rows[i]["txtBalLeasePeriod1"].ToString());
                        }
                        else
                        {
                            hInputPara.Add("@Occupancy_Details", list.Rows[i]["ddlOccupancyDetails"].ToString());
                            hInputPara.Add("@Expected_Monthly_Rental", list.Rows[i]["txtExpMonthlyRental"].ToString());

                            hInputPara.Add("@Name_Of_Occupant", list.Rows[i]["txtNameOccipant"].ToString());

                            hInputPara.Add("@Occupied_Since", list.Rows[i]["txtOccupiedsince"].ToString());

                            hInputPara.Add("@Monthly_Rental", list.Rows[i]["txtMonthlyRental"].ToString());

                            hInputPara.Add("@Balanced_Leased_Period", list.Rows[i]["txtBalLeasePeriod"].ToString());
                        }



                        hInputPara.Add("@Class_of_Locality", list.Rows[i]["ddlclassoflocality"].ToString());
                        hInputPara.Add("@Type_of_Locality", list.Rows[i]["ddlTypeOfLocality"].ToString());
                        hInputPara.Add("@Locality_Remarks", list.Rows[i]["txtremarks"].ToString());

                        hInputPara.Add("@DistFrom_City_Center", list.Rows[i]["txtDistancefromCenter"].ToString());
                        hInputPara.Add("@Approval_Flag", list.Rows[i]["ddlapproveflag"].ToString());
                        hInputPara.Add("@Approved_No_of_Floors", list.Rows[i]["txtEstimatedNoOfFlors"].ToString());
                        hInputPara.Add("@Distance_from_Airport", list.Rows[i]["txtdisAirport"].ToString());
                        hInputPara.Add("@txtdistanceland", list.Rows[i]["txtdistanceland"].ToString());
                        hInputPara.Add("@txtdistancerailway", list.Rows[i]["txtdistancerailway"].ToString());
                        hInputPara.Add("@txtdistancebus", list.Rows[i]["txtdistancebus"].ToString());
                        hInputPara.Add("@txtdistancehospital", list.Rows[i]["txtdistancehospital"].ToString());
                        hInputPara.Add("@txtdistancemarket", list.Rows[i]["txtdistancemarket"].ToString());
                        hInputPara.Add("@txtdistanceairport", list.Rows[i]["txtdistanceairport"].ToString());


                        string Podium = list.Rows[i]["txtpodium"].ToString();
                        Session["Podium"] = Podium;
                        string Upper = list.Rows[i]["txtupper"].ToString();
                        Session["Upper"] = Upper;
                        Session["Basement"] = list.Rows[i]["txtBasement"].ToString();
                        Session["Ground"] = list.Rows[i]["txtGround"].ToString();
                        Session["Stilt"] = list.Rows[i]["txtStilt"].ToString();
                        //  Session["secondfloor"] = list.Rows[i]["txtsecondfloor"].ToString();





                        hInputPara.Add("@Renting_Potential", list.Rows[i]["ddlRentingPotential"].ToString());
                        string unitnumberforcity = list.Rows[i]["txtunitnumber"].ToString();
                        if (unitnumberforcity.Length == 0)
                        {
                            hInputPara.Add("@City", list.Rows[i]["txtCityforSingle"].ToString());

                            hInputPara.Add("@District", list.Rows[i]["txtDistrictsingle"].ToString());
                            hInputPara.Add("@State", list.Rows[i]["ddlstatemastersnigle"].ToString());
                            hInputPara.Add("@PinCode", list.Rows[i]["ddlpincodemastersingle"].ToString());
                            hInputPara.Add("@Survey_Number", list.Rows[i]["txtsureynosingle"].ToString());
                            hInputPara.Add("@Village_Name", list.Rows[i]["txtvillagenamesingle"].ToString());
                            hInputPara.Add("@Street", list.Rows[i]["txtstreetsingle"].ToString());
                            hInputPara.Add("@Locality", list.Rows[i]["txtLocalitysingle"].ToString());
                            hInputPara.Add("@Sub_Locality", list.Rows[i]["txtSubLocalitysingle"].ToString());

                            hInputPara.Add("@Plot_No", list.Rows[i]["txtplotorbungalows1"].ToString());
                            hInputPara.Add("@Ward_No", list.Rows[i]["txtwardno1"].ToString());
                            hInputPara.Add("@Municipal_No", list.Rows[i]["txtmunicipalno1"].ToString());

                        }
                        else
                        {
                            hInputPara.Add("@City", list.Rows[i]["txtCity"].ToString());
                            hInputPara.Add("@District", list.Rows[i]["txtDistrict"].ToString());
                            hInputPara.Add("@State", list.Rows[i]["ddlstatemaster"].ToString());
                            hInputPara.Add("@PinCode", list.Rows[i]["ddlpincodemaster"].ToString());
                            hInputPara.Add("@Survey_Number", list.Rows[i]["txtsureyno"].ToString());
                            hInputPara.Add("@Village_Name", list.Rows[i]["txtvillagename"].ToString());
                            hInputPara.Add("@Street", list.Rows[i]["txtstreet"].ToString());
                            hInputPara.Add("@Locality", list.Rows[i]["txtLocality"].ToString());
                            hInputPara.Add("@Sub_Locality", list.Rows[i]["txtSubLocality"].ToString());


                            hInputPara.Add("@Plot_No", list.Rows[i]["txtplotno"].ToString());


                            hInputPara.Add("@Ward_No", list.Rows[i]["txtwardno"].ToString());
                            hInputPara.Add("@Municipal_No", list.Rows[i]["txtmunicipalno"].ToString());

                        }


                        // hInputPara.Add("@Expected_Monthly_Rental", list.Rows[i]["txtExpMonthlyRental"].ToString());

                        var res1 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectMasterForRetailProperty", hInputPara);
                        Log.Info("submit successfull");

                    }
                }
                catch (Exception e)
                {
                    //throw e;
                    string a = e.InnerException.ToString();
                    Log.Info(a);
                }
                //DataTable dttop1projectId = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1ProjectIdFromProjectMasterNew");
                //for (int j = 0; j < dttop1projectId.Rows.Count; j++)
                //{
                //    hInputPara = new Hashtable();
                //    sqlDataAccess = new SQLDataAccess();
                //    hInputPara.Add("@Project_ID", Convert.ToInt32(dttop1projectId.Rows[0]["Project_ID"]));
                //    hInputPara.Add("@UType", Convert.ToString(dttop1projectId.Rows[0]["TypeOfUnit"]));
                //    hInputPara.Add("@PTypeName", Convert.ToString(dttop1projectId.Rows[0]["Property_type"]));


                //    var res2 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectIdINProjBldgWingUnitMaster", hInputPara);

                //}
                DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1ProjectIdFromProjectMasterNew");
                Session["Project_ID"] = dttop1project.Rows[0]["Project_ID"].ToString();
                try
                {
                    for (int i4 = 0; i4 < listSocialDEv.Rows.Count; i4++)
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                        hInputPara.Add("@Social_Development_Type", Convert.ToString(listSocialDEv.Rows[i4]["value"]));
                        hInputPara.Add("@Created_By", "Branch Head");
                        var res3 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertSocialDev", hInputPara);
                    }
                }
                catch (Exception e)
                {
                    e.InnerException.ToString();
                }

                try
                {
                    for (int i4 = 0; i4 < listCommonArea1.Rows.Count; i4++)
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                        hInputPara.Add("@Social_Development_Type", Convert.ToString(listCommonArea1.Rows[i4]["value"]));
                        hInputPara.Add("@Created_By", "Branch Head");
                        var res3 = sqlDataAccess.ExecuteStoreProcedure("usp_Insertival_CommonAreas", hInputPara);
                    }
                }
                catch (Exception e)
                {
                    e.InnerException.ToString();
                }

                try
                {
                    for (int i4 = 0; i4 < listCommonArea2.Rows.Count; i4++)
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                        hInputPara.Add("@Social_Development_Type", Convert.ToString(listCommonArea2.Rows[i4]["value"]));
                        hInputPara.Add("@Created_By", "Branch Head");
                        var res3 = sqlDataAccess.ExecuteStoreProcedure("usp_Insertival_facilities", hInputPara);
                    }
                }
                catch (Exception e)
                {
                    e.InnerException.ToString();
                }

                try
                {
                    for (int i4 = 0; i4 < listCommonArea3.Rows.Count; i4++)
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                        hInputPara.Add("@Social_Development_Type", Convert.ToString(listCommonArea3.Rows[i4]["value"]));
                        hInputPara.Add("@Created_By", "Branch Head");
                        var res3 = sqlDataAccess.ExecuteStoreProcedure("usp_Insertival_Roofing", hInputPara);
                    }
                }
                catch (Exception e)
                {
                    e.InnerException.ToString();
                }



                try
                {
                    for (int i4 = 0; i4 < list4.Rows.Count; i4++)
                    {
                        //  DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1ProjectIdFromProjectMasterNew");

                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                        hInputPara.Add("@LocalTransport_ID", Convert.ToString(list4.Rows[i4]["value"]));
                        hInputPara.Add("@Created_By", "Branch Head");
                        var res4 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectLocaltransport", hInputPara);
                    }
                }
                catch (Exception e)
                {
                    e.InnerException.ToString();
                }
                try
                {
                    for (int i4 = 0; i4 < listBasic.Rows.Count; i4++)
                    {
                        //DataTable dttop1project = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1ProjectIdFromProjectMasterNew");

                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                        hInputPara.Add("@BasicInfra_type", Convert.ToString(listBasic.Rows[i4]["value1"]));
                        hInputPara.Add("@Created_By", "Branch Head");
                        var res5 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertProjectBasicInfraTypes", hInputPara);
                    }
                }
                catch (Exception e)
                {
                    e.InnerException.ToString();
                }
                //DataTable dttop1projectIdforfloor = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Gettop1ProjectIdFromProjectMasterNew");
                try
                {
                    for (int u = 0; u < listfloor.Rows.Count; u++)
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                        hInputPara.Add("@FloorType_ID", listfloor.Rows[0]["v"].ToString());
                        string FloorTypeId = listfloor.Rows[0]["v"].ToString();
                        if (FloorTypeId == "Podium")
                        {

                            hInputPara.Add("@Number_Of_Floors", Session["Podium"].ToString());
                        }

                        var res6 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingFloorDetails", hInputPara);
                    }
                }
                catch (Exception e)
                {
                    e.InnerException.ToString();
                }
                try
                {
                    for (int u = 0; u < listupper.Rows.Count; u++)
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                        hInputPara.Add("@FloorType_ID", listupper.Rows[0]["v"].ToString());
                        string FloorTypeId = listupper.Rows[0]["v"].ToString();
                        if (FloorTypeId == "Upper Habitable Floors")
                        {

                            hInputPara.Add("@Number_Of_Floors", Session["Upper"]);
                        }

                        var res7 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingFloorDetails", hInputPara);
                    }
                }
                catch (Exception e)
                {
                    e.InnerException.ToString();
                }
                try
                {
                    for (int u = 0; u < listBasement.Rows.Count; u++)
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                        hInputPara.Add("@FloorType_ID", listBasement.Rows[0]["v"].ToString());
                        string FloorTypeId = listBasement.Rows[0]["v"].ToString();
                        if (FloorTypeId == "Basement")
                        {
                            hInputPara.Add("@Number_Of_Floors", Session["Basement"].ToString());
                        }

                        var res8 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingFloorDetails", hInputPara);
                    }
                }
                catch (Exception e)
                {
                    e.InnerException.ToString();
                }
                try
                {
                    for (int u = 0; u < listG.Rows.Count; u++)
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                        hInputPara.Add("@FloorType_ID", listG.Rows[0]["v"].ToString());
                        string FloorTypeId = listG.Rows[0]["v"].ToString();
                        if (FloorTypeId == "Ground")
                        {

                            hInputPara.Add("@Number_Of_Floors", Session["Ground"].ToString());
                        }

                        var res9 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingFloorDetails", hInputPara);
                    }
                }
                catch (Exception e)
                {
                    e.InnerException.ToString();
                }
                try
                {
                    for (int u = 0; u < listS.Rows.Count; u++)
                    {
                        hInputPara = new Hashtable();
                        sqlDataAccess = new SQLDataAccess();
                        hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                        hInputPara.Add("@FloorType_ID", listS.Rows[0]["v"].ToString());
                        string FloorTypeId = listS.Rows[0]["v"].ToString();
                        if (FloorTypeId == "Stilt")
                        {

                            hInputPara.Add("@Number_Of_Floors", Session["Stilt"].ToString());
                        }

                        var res10 = sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingFloorDetails", hInputPara);
                    }
                }
                catch (Exception e)
                {
                    e.InnerException.ToString();
                }
                //for (int u = 0; u < listsecond.Rows.Count; u++)
                //{
                //    hInputPara = new Hashtable();
                //    sqlDataAccess = new SQLDataAccess();
                //    hInputPara.Add("@Project_ID", Session["Project_ID"].ToString());
                //    hInputPara.Add("@FloorType_ID", listsecond.Rows[0]["v"].ToString());
                //    string FloorTypeId = listsecond.Rows[0]["v"].ToString();
                //    if (FloorTypeId == "second floor")
                //    {

                //        hInputPara.Add("@Number_Of_Floors", Session["secondfloor"].ToString());
                //    }

                //  var res11=  sqlDataAccess.ExecuteStoreProcedure("usp_InsertWingFloorDetails", hInputPara);
                //}


                return Json(new { success = true, Message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //ILog log = LogManager.GetLogger("mylog");
                // log.Error("An error happened", ex);
                throw ex;


            }

        }


        public JsonResult GetRetailPropDetailsByProjectId(int Project_ID)
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_ProjectMaster_New> REQLists = new List<ival_ProjectMaster_New>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Project_ID", Project_ID);
            Session["Projectidretail"] = Project_ID;
            DataTable dtREQ = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRetailPropDetailsByProjectID", hInputPara);
            TempData["data"] = dtREQ;
            for (int i = 0; i < dtREQ.Rows.Count; i++)
            {
                REQLists.Add(new ival_ProjectMaster_New
                {
                    Property_Type = Convert.ToString(dtREQ.Rows[i]["Property_Type"]),
                    TypeOfUnit = Convert.ToString(dtREQ.Rows[i]["TypeOfUnit"]),
                    UnitNumber = Convert.ToString(dtREQ.Rows[i]["UnitNumber"]),

                    PlotBunglowNumber = Convert.ToString(dtREQ.Rows[i]["Plot_Number"]),
                    Project_Name = Convert.ToString(dtREQ.Rows[i]["Project_Name"]),
                    Building_Name_RI = Convert.ToString(dtREQ.Rows[i]["Building_Name_RI"]),
                    Wing_Name_RI = Convert.ToString(dtREQ.Rows[i]["Wing_Name_RI"]),


                    Survey_Number = Convert.ToString(dtREQ.Rows[i]["Survey_Number"]),
                    Village_Name = Convert.ToString(dtREQ.Rows[i]["Village_Name"]),
                    Street = Convert.ToString(dtREQ.Rows[i]["Street"]),
                    Locality = Convert.ToString(dtREQ.Rows[i]["Locality"]),
                    Sub_Locality = Convert.ToString(dtREQ.Rows[i]["Sub_Locality"]),
                    City = Convert.ToString(dtREQ.Rows[i]["City"]),
                    District = Convert.ToString(dtREQ.Rows[i]["District"]),
                    State = Convert.ToString(dtREQ.Rows[i]["State"]),
                    PinCode = Convert.ToString(dtREQ.Rows[i]["PinCode"]),


                });
            }
            ViewBag.unitdetails = REQLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(REQLists);
            return Json(jsonProjectBuildingMaster);
        }

        public ActionResult RetailLandMarkDetails()
        {
            ViewModel vm = new ViewModel();
            ViewBag.data = TempData["data"];
            DataTable dt = ViewBag.data;
            Session["dt"] = dt;
            if (dt == null)
            {
                vm.ival_LookupCategoryValuesProjecttype = GetProjectTypeMaster();
                vm.ival_LookupCategoryValuesRetailStatusOfProperty = GetRetailStatusOfProperty();
                vm.ival_LookupCategoryValuesTypeOfStrycture = GetTypeOfStructure();
                vm.ival_LookupCategoryValuesProjecttype = GetProjectTypeMaster();
                vm.ival_LookupCategoryValuesQualityOfConstruction = GetQualityOfConstruction();
                vm.ival_LookupCategoryValuesMaintenanceOfproperty = GetMaintenanceOfProperty();
                vm.ival_LookupCategoryValuesOccupancyDetails = GetOccupancyDetails();
                // vm.ival_LookupCategoryValuesFloorType = GetFloorsType();
                vm.ival_BuildingWingFloorDetailsList = GetFloorType();
                vm.ival_LookupCategoryValuesSocialDev = GetSocialDevelopment();

                vm.ival_LookupCategoryValuesbasicInfra = GetBasicInfra();
                vm.ival_CommonAreaI = GetCommonAreas();
                vm.ival_CommonAreaI2 = GetCommonAreas2();
                vm.ival_CommonAreaI3 = GetCommonAreas3();
                vm.ival_LookupCategoryValuesconnectivity = GetConnectivity();
                vm.ival_LookupCategoryValuesClassOfLocality = GetClassoflocality();
                vm.ival_LookupCategoryValuesTypeOfLocality = GetTypeOfLocality();
                vm.ival_States = GetStates();
                vm.ival_LookupCategoryValuespincode = Getpincode();
                vm.ival_LookupCategoryValuesrentingPotential = GetRentingPotential();
                ViewBag.ddlprojecttype = TempData["ddlprojecttype"];
                ViewBag.ddlselecttype = TempData["ddlselecttype"];
                ViewBag.ddlunittype = TempData["ddlunittype"];
                ViewBag.ddlretailstatusproperty = TempData["ddlretailstatusproperty"];
                ViewBag.PlotBunglowNumber = TempData["txtplotorbungalows"];
                ViewBag.Survey_Number = TempData["txtsureynosingle"];
                ViewBag.Village_Name = TempData["txtvillagenamesingle"];

                ViewBag.UnitNumber = TempData["txtunitnumber"];
                if (ViewBag.PlotBunglowNumber == null)
                {
                    ViewBag.Survey_Number = TempData["txtsureyno"];
                    ViewBag.Village_Name = TempData["txtvillagename"];
                }
            }
            else if (dt.Rows.Count > 0)

            {
                Response.Redirect("RetailLandMarkDetailsSearch");

            }

            return View(vm);
        }


        public ActionResult RetailLandMarkDetailsSearch()

        {
            ViewModel vm = new ViewModel();
            ViewBag.data = TempData["data"];
            DataTable dt = new DataTable();
            dt = Session["dt"] as DataTable;

            ViewBag.ddlprojecttype = dt.Rows[0]["Property_Type"].ToString();
            ViewBag.ddlselecttype = dt.Rows[0]["TypeOfSelect"].ToString();
            ViewBag.ddlunittype = dt.Rows[0]["TypeOfUnit"].ToString();
            ViewBag.ddlretailstatusproperty = dt.Rows[0]["StatusOfProperty"].ToString();
            ViewBag.UnitNumber = dt.Rows[0]["UnitNumber"].ToString();
            ViewBag.PlotBunglowNumber = dt.Rows[0]["Plot_Number"].ToString();
            ViewBag.Project_Name = dt.Rows[0]["Project_Name"].ToString();
            ViewBag.Survey_Number = dt.Rows[0]["Survey_Number"].ToString();
            ViewBag.Village_Name = dt.Rows[0]["Village_Name"].ToString();
            ViewBag.Street = dt.Rows[0]["Street"].ToString();
            ViewBag.SelectedModellookupclassOfLocality = dt.Rows[0]["Locality"].ToString();
            ViewBag.Sub_Locality = dt.Rows[0]["Sub_Locality"].ToString();
            ViewBag.selectedCityID = dt.Rows[0]["City"].ToString();
            ViewBag.selectedDistID = dt.Rows[0]["District"].ToString();
            ViewBag.SelectedModelstates = dt.Rows[0]["State"].ToString();
            ViewBag.selectedpinID = dt.Rows[0]["PinCode"].ToString();
            ViewBag.NearBy_Landmark = dt.Rows[0]["NearBy_Landmark"].ToString();
            ViewBag.Nearest_RailwayStation = Convert.ToString(dt.Rows[0]["Nearest_RailwayStation"]);
            ViewBag.Nearest_BusStop = Convert.ToString(dt.Rows[0]["Nearest_BusStop"]);
            ViewBag.Nearest_Hospital = Convert.ToString(dt.Rows[0]["Nearest_Hospital"]);
            ViewBag.Building_Name_RI = Convert.ToString(dt.Rows[0]["Building_Name_RI"]);
            ViewBag.Nearest_Market = Convert.ToString(dt.Rows[0]["Nearest_Market"]);
            ViewBag.RERA_Registration_No = Convert.ToString(dt.Rows[0]["RERA_Registration_No"]);
            ViewBag.Wing_Name_RI = Convert.ToString(dt.Rows[0]["Wing_Name_RI"]);
            ViewBag.Approved_No_of_Buildings = Convert.ToString(dt.Rows[0]["Approved_No_of_Buildings"]);
            ViewBag.Actual_No_Floors_G = Convert.ToString(dt.Rows[0]["Actual_No_Floors_G"]);
            ViewBag.Distance_from_Airport = Convert.ToString(dt.Rows[0]["Distance_from_Airport"]);

            ViewBag.txtdistanceland = Convert.ToString(dt.Rows[0]["txtdistanceland"]);
            ViewBag.txtdistancerailway = Convert.ToString(dt.Rows[0]["txtdistancerailway"]);
            ViewBag.txtdistancebus = Convert.ToString(dt.Rows[0]["txtdistancebus"]);
            ViewBag.txtdistancehospital = Convert.ToString(dt.Rows[0]["txtdistancehospital"]);
            ViewBag.txtdistancemarket = Convert.ToString(dt.Rows[0]["txtdistancemarket"]);
            ViewBag.txtdistanceairport = Convert.ToString(dt.Rows[0]["txtdistanceairport"]);
         

            ViewBag.Floor_Of_Unit = Convert.ToString(dt.Rows[0]["Floor_Of_Unit"]);
            ViewBag.Property_Description = Convert.ToString(dt.Rows[0]["Property_Description"]);
            ViewBag.SelectedModelRetailTypeOfStructure = Convert.ToString(dt.Rows[0]["Type_Of_Structure"]);
            ViewBag.SelectedModellookupprojecttype = Convert.ToString(dt.Rows[0]["Approved_Usage_Of_Property"]);

            ViewBag.Residual_Age_Of_Property = Convert.ToString(dt.Rows[0]["Residual_Age_Of_Property"]);
            ViewBag.Common_Areas = Convert.ToString(dt.Rows[0]["Common_Areas"]);
            ViewBag.Facilities = Convert.ToString(dt.Rows[0]["Facilities"]);
            ViewBag.Anyother_Observation = Convert.ToString(dt.Rows[0]["Anyother_Observation"]);

            ViewBag.Roofing_Terracing = Convert.ToString(dt.Rows[0]["Roofing_Terracing"]);
            ViewBag.Quality_Of_Fixture = Convert.ToString(dt.Rows[0]["Quality_Of_Fixture"]);
            ViewBag.SelectedModelRetailQualityOfConstruction = Convert.ToString(dt.Rows[0]["Quality_Of_Construction"]);
            ViewBag.SelectedModelRetailMaintenanceOfProp = Convert.ToString(dt.Rows[0]["Maintenance_Of_Property"]);

            ViewBag.Marketability_Of_Property = Convert.ToString(dt.Rows[0]["Marketability_Of_Property"]);
            ViewBag.SelectedModelRetailOccupancyDetails = Convert.ToString(dt.Rows[0]["Occupancy_Details"]);
            ViewBag.SelectedModelRetailRentingPotential = Convert.ToString(dt.Rows[0]["Renting_Potential"]);
            ViewBag.Expected_Monthly_Rental = Convert.ToString(dt.Rows[0]["Expected_Monthly_Rental"]);

            ViewBag.Name_Of_Occupant = Convert.ToString(dt.Rows[0]["Name_Of_Occupant"]);
            ViewBag.Occupied_Since = Convert.ToString(dt.Rows[0]["Occupied_Since"]);
            ViewBag.Monthly_Rental = Convert.ToString(dt.Rows[0]["Monthly_Rental"]);
            ViewBag.Balanced_Leased_Period = Convert.ToString(dt.Rows[0]["Balanced_Leased_Period"]);

            ViewBag.SelectedModellookupprojecttype = Convert.ToString(dt.Rows[0]["Actual_Usage_Of_Property"]);
            ViewBag.Current_Age_Of_Property = Convert.ToString(dt.Rows[0]["Current_Age_Of_Property"]);

            ViewBag.DistFrom_City_Center = Convert.ToString(dt.Rows[0]["DistFrom_City_Center"]);
            ViewBag.Locality_Remarks = Convert.ToString(dt.Rows[0]["Locality_Remarks"]);
            ViewBag.Approval_Flag = Convert.ToString(dt.Rows[0]["Approval_Flag"]);
            ViewBag.SelectedModellookupclassOfLocality = Convert.ToString(dt.Rows[0]["Class_of_Locality"]);
            ViewBag.SelectedModellookuptypeOfLocality = Convert.ToString(dt.Rows[0]["Type_of_Locality"]);

            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            //  int RequestID = Convert.ToInt32(Session["RequestID"]);
            //string Request_ID = Session["Request_ID"].ToString();
            int projectID = Convert.ToInt32(Session["Projectidretail"].ToString());

            vm.ival_BuildingWingFloorDetailsList = GetFloorType();
            vm.ival_LookupCategoryValuesSocialDev = GetSocialDevelopment();




            hInputPara.Add("@Project_ID", projectID);
            DataTable dtbasicinfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectBasicInfraAvailability", hInputPara);
            DataTable dtlocaltransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalTransport", hInputPara);
            DataTable dtprojectscheckunits = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetcheckedUnitType", hInputPara);



            vm.ival_ProjectUnitDetails = GetProjectUnitDetails(projectID);
            vm.ival_LookupCategoryValuesTransaportEdit = GetProjectLocalTransportDetails(projectID);
            vm.ival_LookupCategoryValuesBasicInfraEdit = GetProjectBasicInfraDetails(projectID);
            vm.ival_commonarea = GetCoomaDetails(projectId);
            vm.ival_commonarea2 = GetCoomaDetails2(projectId);
            vm.ival_commonarea3 = GetCoomaDetails3(projectId);
            if (dtprojectscheckunits.Rows.Count > 0 && dtprojectscheckunits != null)
            {
                List<ival_ProjectUnitDetails> Listcheckunits = new List<ival_ProjectUnitDetails>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                for (int i = 0; i < dtprojectscheckunits.Rows.Count; i++)
                {
                    Listcheckunits.Add(new ival_ProjectUnitDetails
                    {
                        Project_ID = Convert.ToInt32(dtprojectscheckunits.Rows[i]["Project_ID"]),
                        UType_ID = Convert.ToString(dtprojectscheckunits.Rows[i]["UType_ID"]),

                    });
                }
                //test
                ViewBag.ival_Prjchkunits = JsonConvert.SerializeObject(Listcheckunits);
            }


            if (dtbasicinfra.Rows.Count > 0 && dtbasicinfra != null)
            {
                List<ival_BasicInfraAvailability> Listcheckbsics = new List<ival_BasicInfraAvailability>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                for (int i = 0; i < dtbasicinfra.Rows.Count; i++)
                {
                    Listcheckbsics.Add(new ival_BasicInfraAvailability
                    {
                        Project_ID = Convert.ToInt32(dtbasicinfra.Rows[i]["Project_ID"]),
                        BasicInfra_type = Convert.ToString(dtbasicinfra.Rows[i]["BasicInfra_type"]),

                    });
                }
                ViewBag.ival_Prjchkbasic = JsonConvert.SerializeObject(Listcheckbsics);
            }


            if (dtlocaltransport.Rows.Count > 0 && dtlocaltransport != null)
            {
                List<ival_LocalTransportForProject> Listchecklocal = new List<ival_LocalTransportForProject>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                for (int i = 0; i < dtlocaltransport.Rows.Count; i++)
                {
                    Listchecklocal.Add(new ival_LocalTransportForProject
                    {
                        Project_ID = Convert.ToInt32(dtlocaltransport.Rows[i]["Project_ID"]),
                        LocalTransport_ID = Convert.ToString(dtlocaltransport.Rows[i]["LocalTransport_ID"]),

                    });
                }
                ViewBag.ival_Prjchklocal = JsonConvert.SerializeObject(Listchecklocal);
            }


            vm.ival_LookupCategoryValuesProjecttype = GetProjectTypeMaster();
            vm.ival_LookupCategoryValuesRetailStatusOfProperty = GetRetailStatusOfProperty();
            vm.ival_LookupCategoryValuesTypeOfStrycture = GetTypeOfStructure();
            vm.ival_LookupCategoryValuesProjecttype = GetProjectTypeMaster();
            vm.ival_LookupCategoryValuesQualityOfConstruction = GetQualityOfConstruction();
            vm.ival_LookupCategoryValuesMaintenanceOfproperty = GetMaintenanceOfProperty();
            vm.ival_LookupCategoryValuesOccupancyDetails = GetOccupancyDetails();
            vm.ival_LookupCategoryValuesFloorType = GetFloorsType();
            vm.ival_LookupCategoryValuesSocialDev = GetSocialDevelopment();

            vm.ival_LookupCategoryValuesbasicInfra = GetBasicInfra();
            vm.ival_CommonAreaI = GetCommonAreas();
            vm.ival_CommonAreaI2 = GetCommonAreas2();
            vm.ival_CommonAreaI3 = GetCommonAreas3();
            vm.ival_LookupCategoryValuesconnectivity = GetConnectivity();
            vm.ival_LookupCategoryValuesClassOfLocality = GetClassoflocality();
            vm.ival_LookupCategoryValuesTypeOfLocality = GetTypeOfLocality();
            vm.ival_States = GetStates();
            vm.ival_LookupCategoryValuespincode = Getpincode();
            vm.ival_LookupCategoryValuesrentingPotential = GetRentingPotential();

            vm.ival_StatesList = GetState();
            vm.PincodeList = GetPincode();

            vm.ival_DistrictsList = GetDistrict();
            vm.ival_CitiesList = GetCity();
            vm.ival_pincodelist = GetLoc();
            vm.ival_LocalityList = GetLoc();
            vm.ival_subLocalityList = GetSubLoc();

            IEnumerable<ival_States> ival_StatesList = GetStates();
            if (ival_StatesList.ToList().Count > 0 && ival_StatesList != null)
            {
                var BuilderState = ival_StatesList.Where(s => s.State_Name == dt.Rows[0]["State"].ToString()).Select(s => s.State_ID);

                vm.SelectedModelstates = Convert.ToInt32(BuilderState.FirstOrDefault());

            }


            IEnumerable<ival_Districts> ival_DistrictsList = GetDistrict();
            if (ival_DistrictsList.ToList().Count > 0 && ival_DistrictsList != null)
            {
                var BuilderDistrict = ival_DistrictsList.Where(s => s.District_Name == dt.Rows[0]["District"].ToString()).Select(s => s.District_ID);

                vm.selectedDistID = Convert.ToInt32(BuilderDistrict.FirstOrDefault());

            }
            IEnumerable<ival_Cities> ival_CitiesList = GetCity();
            if (ival_CitiesList.ToList().Count > 0 && ival_CitiesList != null)
            {
                var BuilderCity = ival_CitiesList.Where(s => s.City_Name == dt.Rows[0]["City"].ToString()).Select(s => s.City_ID);

                vm.selectedCityID = Convert.ToInt32(BuilderCity.FirstOrDefault());

            }
            IEnumerable<ival_Cities> ival_pincodelist = GetLoc();
            if (ival_pincodelist.ToList().Count > 0 && ival_pincodelist != null)
            {
                var builderpostalpincode = ival_pincodelist.Where(s => s.Pin_Code == dt.Rows[0]["Pincode"].ToString()).Select(s => s.Loc_ID);

                vm.SelectedModelpincode = Convert.ToInt32(builderpostalpincode.FirstOrDefault());

            }

            return View(vm);
        }
        [HttpPost]
        public List<ival_ProjectBuildingWingFloorDetails> GetFloorListbyreqID()
        {
            List<ival_ProjectBuildingWingFloorDetails> Listsocialinfra = new List<ival_ProjectBuildingWingFloorDetails>();
            hInputPara = new Hashtable();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            sqlDataAccess = new SQLDataAccess();
            DataTable dtsocialinfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorDetailsByReqID", hInputPara);
            if (dtsocialinfra.Rows.Count > 0)
            {
                for (int i = 0; i < dtsocialinfra.Rows.Count; i++)
                {

                    Listsocialinfra.Add(new ival_ProjectBuildingWingFloorDetails
                    {
                        BuildingFloor_ID = Convert.ToInt32(dtsocialinfra.Rows[i]["BuildingFloor_ID"]),
                        FloorType_ID = Convert.ToString(dtsocialinfra.Rows[i]["FloorType_ID"]),
                        Number_Of_Floors = Convert.ToString(dtsocialinfra.Rows[i]["Number_Of_Floors"]),

                    });
                }
            }
            else
            {

            }
            return Listsocialinfra;
        }
        [HttpPost]
        public List<ival_LocalTransportForProject> Gettransport()
        {
            List<ival_LocalTransportForProject> Listlocalinfra = new List<ival_LocalTransportForProject>();
            hInputPara = new Hashtable();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            sqlDataAccess = new SQLDataAccess();
            DataTable dtlocal = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocaltransportBYReqID", hInputPara);
            for (int i = 0; i < dtlocal.Rows.Count; i++)
            {

                Listlocalinfra.Add(new ival_LocalTransportForProject
                {
                    LocalTransport_ID = Convert.ToString(dtlocal.Rows[i]["LocalTransport_ID"]),
                    ProjectLocalTransport_ID = Convert.ToInt32(dtlocal.Rows[i]["ProjectLocalTransport_ID"]),
                });
            }
            return Listlocalinfra;
        }
        [HttpPost]
        public List<ival_BasicInfraAvailability> GetSocialBasicInfra()
        {
            List<ival_BasicInfraAvailability> Listbasicinfra = new List<ival_BasicInfraAvailability>();
            hInputPara = new Hashtable();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            sqlDataAccess = new SQLDataAccess();
            DataTable dtbasicinfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBasicInfraBYReqID", hInputPara);
            for (int i = 0; i < dtbasicinfra.Rows.Count; i++)
            {

                Listbasicinfra.Add(new ival_BasicInfraAvailability
                {
                    ProjectInfra_ID = Convert.ToInt32(dtbasicinfra.Rows[i]["ProjectInfra_ID"]),
                    BasicInfra_type = Convert.ToString(dtbasicinfra.Rows[i]["BasicInfra_type"]),
                });
            }
            return Listbasicinfra;
        }

        [HttpPost]
        public List<ival_SocialDevelopmentForProject> GetSocialInfraByid()
        {
            List<ival_SocialDevelopmentForProject> Listsocialinfra = new List<ival_SocialDevelopmentForProject>();
            hInputPara = new Hashtable();
            int RequestID = Convert.ToInt32(Session["RequestID"]);
            hInputPara.Add("@Request_ID", RequestID);
            sqlDataAccess = new SQLDataAccess();
            DataTable dtsocialinfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSocialDevBYReqID", hInputPara);
            for (int i = 0; i < dtsocialinfra.Rows.Count; i++)
            {

                Listsocialinfra.Add(new ival_SocialDevelopmentForProject
                {
                    Social_Dev_ID = Convert.ToInt32(dtsocialinfra.Rows[i]["Social_Dev_ID"]),
                    Social_Development_Type = Convert.ToString(dtsocialinfra.Rows[i]["Social_Development_Type"]),


                });
            }
            return Listsocialinfra;
        }

        [HttpPost]
        public List<ival_LookupCategoryValues> GetRentingPotential()
        {
            List<ival_LookupCategoryValues> ListFloors = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtFloors = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRentingPotenali");
            for (int i = 0; i < dtFloors.Rows.Count; i++)
            {
                ListFloors.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtFloors.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtFloors.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtFloors.Rows[i]["Lookup_Value"])
                });
            }
            return ListFloors;
        }


        public List<ival_LookupCategoryValues> GetTypeOfLocality()
        {
            List<ival_LookupCategoryValues> ListFloors = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtFloors = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTypeOfLocality");
            for (int i = 0; i < dtFloors.Rows.Count; i++)
            {
                ListFloors.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtFloors.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtFloors.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtFloors.Rows[i]["Lookup_Value"])
                });
            }
            return ListFloors;
        }


        public List<ival_LookupCategoryValues> GetSocialDevelopment()
        {
            List<ival_LookupCategoryValues> ListSocialDev = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtFloors = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSocialDevelopment");
            for (int i = 0; i < dtFloors.Rows.Count; i++)
            {
                ListSocialDev.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtFloors.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtFloors.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtFloors.Rows[i]["Lookup_Value"])
                });
            }
            return ListSocialDev;
        }
        //changes by punam
        public List<ival_ProjectUnitDetailsWithCategory> GetProjectUnitDetails(int ProjectId)
        {
            List<ival_ProjectUnitDetailsWithCategory> ListProjectUnitDetails = new List<ival_ProjectUnitDetailsWithCategory>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@ProjectId", ProjectId);
            DataTable dtFloors = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ProjectUnitDetailsByProjectId", hInputPara);
            for (int i = 0; i < dtFloors.Rows.Count; i++)
            {
                ival_ProjectUnitDetailsWithCategory obj = new ival_ProjectUnitDetailsWithCategory();
                if (dtFloors.Rows[i]["Social_Dev_ID"] != DBNull.Value)
                {
                    obj.ID = Convert.ToInt32(dtFloors.Rows[i]["Social_Dev_ID"]);
                }
                if (dtFloors.Rows[i]["Project_ID"] != DBNull.Value)
                {
                    obj.Project_ID = Convert.ToInt32(dtFloors.Rows[i]["Project_ID"]);
                }
                if (dtFloors.Rows[i]["Social_Development_Type"] != DBNull.Value)
                {
                    obj.UType_ID = dtFloors.Rows[i]["Social_Development_Type"].ToString();
                }
                obj.Lookup_ID = Convert.ToInt32(dtFloors.Rows[i]["Lookup_ID"]);
                //obj.Lookup_Type = dtFloors.Rows[i]["Lookup_Type"].ToString();
                obj.Lookup_Value = dtFloors.Rows[i]["Lookup_Value"].ToString();
                ListProjectUnitDetails.Add(obj);
            }
            return ListProjectUnitDetails;
        }

        //changes by punam
        public List<ival_ProjectLocalTransaportEdit> GetProjectLocalTransportDetails(int ProjectId)
        {
            List<ival_ProjectLocalTransaportEdit> ListProjectUnitDetails = new List<ival_ProjectLocalTransaportEdit>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@ProjectId", ProjectId);
            DataTable dtFloors = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ProjectTransportDetailsEdit", hInputPara);
            for (int i = 0; i < dtFloors.Rows.Count; i++)
            {
                ival_ProjectLocalTransaportEdit obj = new ival_ProjectLocalTransaportEdit();
                if (dtFloors.Rows[i]["ProjectLocalTransport_ID"] != DBNull.Value)
                {
                    obj.ProjectLocalTransport_ID = Convert.ToInt32(dtFloors.Rows[i]["ProjectLocalTransport_ID"]);
                }
                if (dtFloors.Rows[i]["Project_ID"] != DBNull.Value)
                {
                    obj.Project_ID = Convert.ToInt32(dtFloors.Rows[i]["Project_ID"]);
                }
                if (dtFloors.Rows[i]["LocalTransport_ID"] != DBNull.Value)
                {
                    obj.LocalTransport_ID = dtFloors.Rows[i]["LocalTransport_ID"].ToString();
                }
                obj.Lookup_ID = Convert.ToInt32(dtFloors.Rows[i]["Lookup_ID"]);
                // obj.Lookup_Type = dtFloors.Rows[i]["Lookup_Type"].ToString();
                obj.Lookup_Value = dtFloors.Rows[i]["Lookup_Value"].ToString();
                ListProjectUnitDetails.Add(obj);
            }
            return ListProjectUnitDetails;
        }

        //changes by punam

        public List<iva_ProjectwithBasicInfraEdit> GetProjectBasicInfraDetails(int ProjectId)
        {
            List<iva_ProjectwithBasicInfraEdit> ListProjectUnitDetails = new List<iva_ProjectwithBasicInfraEdit>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@ProjectId", ProjectId);
            DataTable dtFloors = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ProjectBasicInfraEdit", hInputPara);
            for (int i = 0; i < dtFloors.Rows.Count; i++)
            {
                iva_ProjectwithBasicInfraEdit obj = new iva_ProjectwithBasicInfraEdit();
                if (dtFloors.Rows[i]["ProjectInfra_ID"] != DBNull.Value)
                {
                    obj.ProjectInfra_ID = Convert.ToInt32(dtFloors.Rows[i]["ProjectInfra_ID"]);
                }
                if (dtFloors.Rows[i]["Project_ID"] != DBNull.Value)
                {
                    obj.Project_ID = Convert.ToInt32(dtFloors.Rows[i]["Project_ID"]);
                }
                if (dtFloors.Rows[i]["BasicInfra_type"] != DBNull.Value)
                {
                    obj.BasicInfra_type = dtFloors.Rows[i]["BasicInfra_type"].ToString();
                }
                obj.Lookup_ID = Convert.ToInt32(dtFloors.Rows[i]["Lookup_ID"]);
                // obj.Lookup_Type = dtFloors.Rows[i]["Lookup_Type"].ToString();
                obj.Lookup_Value = dtFloors.Rows[i]["Lookup_Value"].ToString();
                ListProjectUnitDetails.Add(obj);
            }
            return ListProjectUnitDetails;
        }


        public List<iva_ProjectwithBasicInfraEdit> GetCoomaDetails(int ProjectId)
        {
            List<iva_ProjectwithBasicInfraEdit> ListProjectUnitDetails = new List<iva_ProjectwithBasicInfraEdit>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@ProjectId", ProjectId);
            DataTable dtFloors = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ProjectCommon", hInputPara);
            for (int i = 0; i < dtFloors.Rows.Count; i++)
            {
                iva_ProjectwithBasicInfraEdit obj = new iva_ProjectwithBasicInfraEdit();
                if (dtFloors.Rows[i]["CommonAreasInfra_ID"] != DBNull.Value)
                {
                    obj.ProjectInfra_ID = Convert.ToInt32(dtFloors.Rows[i]["CommonAreasInfra_ID"]);
                }
                if (dtFloors.Rows[i]["Project_ID"] != DBNull.Value)
                {
                    obj.Project_ID = Convert.ToInt32(dtFloors.Rows[i]["Project_ID"]);
                }
                if (dtFloors.Rows[i]["CommonAreas_type"] != DBNull.Value)
                {
                    obj.BasicInfra_type = dtFloors.Rows[i]["CommonAreas_type"].ToString();
                }
                obj.Lookup_ID = Convert.ToInt32(dtFloors.Rows[i]["Lookup_ID"]);
                // obj.Lookup_Type = dtFloors.Rows[i]["Lookup_Type"].ToString();
                obj.Lookup_Value = dtFloors.Rows[i]["Lookup_Value"].ToString();
                ListProjectUnitDetails.Add(obj);
            }
            return ListProjectUnitDetails;
        }
        public List<iva_ProjectwithBasicInfraEdit> GetCoomaDetails2(int ProjectId)
        {
            List<iva_ProjectwithBasicInfraEdit> ListProjectUnitDetails = new List<iva_ProjectwithBasicInfraEdit>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@ProjectId", ProjectId);
            DataTable dtFloors = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ProjectCommon2", hInputPara);
            for (int i = 0; i < dtFloors.Rows.Count; i++)
            {
                iva_ProjectwithBasicInfraEdit obj = new iva_ProjectwithBasicInfraEdit();
                if (dtFloors.Rows[i]["CommonAreasInfra_ID"] != DBNull.Value)
                {
                    obj.ProjectInfra_ID = Convert.ToInt32(dtFloors.Rows[i]["CommonAreasInfra_ID"]);
                }
                if (dtFloors.Rows[i]["Project_ID"] != DBNull.Value)
                {
                    obj.Project_ID = Convert.ToInt32(dtFloors.Rows[i]["Project_ID"]);
                }
                if (dtFloors.Rows[i]["facilities_type"] != DBNull.Value)
                {
                    obj.BasicInfra_type = dtFloors.Rows[i]["facilities_type"].ToString();
                }
                obj.Lookup_ID = Convert.ToInt32(dtFloors.Rows[i]["Lookup_ID"]);
                // obj.Lookup_Type = dtFloors.Rows[i]["Lookup_Type"].ToString();
                obj.Lookup_Value = dtFloors.Rows[i]["Lookup_Value"].ToString();
                ListProjectUnitDetails.Add(obj);
            }
            return ListProjectUnitDetails;
        }
        public List<iva_ProjectwithBasicInfraEdit> GetCoomaDetails3(int ProjectId)
        {
            List<iva_ProjectwithBasicInfraEdit> ListProjectUnitDetails = new List<iva_ProjectwithBasicInfraEdit>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@ProjectId", ProjectId);
            DataTable dtFloors = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ProjectCommon3", hInputPara);
            for (int i = 0; i < dtFloors.Rows.Count; i++)
            {
                iva_ProjectwithBasicInfraEdit obj = new iva_ProjectwithBasicInfraEdit();
                if (dtFloors.Rows[i]["CommonAreasInfra_ID"] != DBNull.Value)
                {
                    obj.ProjectInfra_ID = Convert.ToInt32(dtFloors.Rows[i]["CommonAreasInfra_ID"]);
                }
                if (dtFloors.Rows[i]["Project_ID"] != DBNull.Value)
                {
                    obj.Project_ID = Convert.ToInt32(dtFloors.Rows[i]["Project_ID"]);
                }
                if (dtFloors.Rows[i]["Roofing_type"] != DBNull.Value)
                {
                    obj.BasicInfra_type = dtFloors.Rows[i]["Roofing_type"].ToString();
                }
                obj.Lookup_ID = Convert.ToInt32(dtFloors.Rows[i]["Lookup_ID"]);
                // obj.Lookup_Type = dtFloors.Rows[i]["Lookup_Type"].ToString();
                obj.Lookup_Value = dtFloors.Rows[i]["Lookup_Value"].ToString();
                ListProjectUnitDetails.Add(obj);
            }
            return ListProjectUnitDetails;
        }
        //changes by punam

        public List<ival_ProjectBuildingWingFloorDetails> GetProjectFloorWingDetails(int ProjectId)
        {
            List<ival_ProjectBuildingWingFloorDetails> ListProjectUnitDetails = new List<ival_ProjectBuildingWingFloorDetails>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@ProjectId", ProjectId);
            DataTable dtFloors = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_FloorWingDetailsByProjectIdEdit", hInputPara);
            for (int i = 0; i < dtFloors.Rows.Count; i++)
            {
                ival_ProjectBuildingWingFloorDetails obj = new ival_ProjectBuildingWingFloorDetails();
                if (dtFloors.Rows[i]["FloorType_ID"] != DBNull.Value)
                {
                    obj.FloorType_ID = dtFloors.Rows[i]["FloorType_ID"].ToString();
                }
                if (dtFloors.Rows[i]["Project_ID"] != DBNull.Value)
                {
                    obj.Project_ID = Convert.ToInt32(dtFloors.Rows[i]["Project_ID"]);
                }
                if (dtFloors.Rows[i]["BuildingFloor_ID"] != DBNull.Value)
                {
                    obj.BuildingFloor_ID = Convert.ToInt32(dtFloors.Rows[i]["BuildingFloor_ID"]);
                }
                if (dtFloors.Rows[i]["Number_Of_Floors"] != DBNull.Value)
                {
                    obj.Number_Of_Floors = dtFloors.Rows[i]["Number_Of_Floors"].ToString();
                }

                ListProjectUnitDetails.Add(obj);
            }
            return ListProjectUnitDetails;
        }

        public List<ival_LookupCategoryValues> GetFloorsType()
        {
            List<ival_LookupCategoryValues> ListFloors = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtFloors = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFloorType");
            for (int i = 0; i < dtFloors.Rows.Count; i++)
            {
                ListFloors.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtFloors.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtFloors.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtFloors.Rows[i]["Lookup_Value"])
                });
            }
            return ListFloors;
        }


        [HttpPost]
        public List<ival_LookupCategoryValues> GetTypeOfStructure()
        {
            List<ival_LookupCategoryValues> ListTypeOfStructure = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtTypeOfStructure = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTypeOfstructure");
            for (int i = 0; i < dtTypeOfStructure.Rows.Count; i++)
            {

                ListTypeOfStructure.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtTypeOfStructure.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtTypeOfStructure.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtTypeOfStructure.Rows[i]["Lookup_Value"])

                });

            }
            return ListTypeOfStructure;
        }

        public List<ival_LookupCategoryValues> GetQualityOfConstruction()
        {
            List<ival_LookupCategoryValues> ListQualityOfConstruction = new List<ival_LookupCategoryValues>();
            sqlDataAccess = new SQLDataAccess();
            hInputPara = new Hashtable();
            DataTable dtQualityOfConstruction = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetQualityOfConstruction");
            for (int i = 0; i < dtQualityOfConstruction.Rows.Count; i++)
            {
                ListQualityOfConstruction.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtQualityOfConstruction.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtQualityOfConstruction.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtQualityOfConstruction.Rows[i]["Lookup_Value"])

                });
            }
            return ListQualityOfConstruction;
        }


        public List<ival_LookupCategoryValues> GetMaintenanceOfProperty()
        {
            List<ival_LookupCategoryValues> ListOfMaintenanceOfProperty = new List<ival_LookupCategoryValues>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtMaintenanceOfprop = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetMaintenanceOfProperty");
            for (int i = 0; i < dtMaintenanceOfprop.Rows.Count; i++)
            {
                ListOfMaintenanceOfProperty.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtMaintenanceOfprop.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtMaintenanceOfprop.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtMaintenanceOfprop.Rows[i]["Lookup_Value"])

                });
            }
            return ListOfMaintenanceOfProperty;
        }

        public List<ival_LookupCategoryValues> GetOccupancyDetails()
        {
            List<ival_LookupCategoryValues> ListOfMaintenanceOfProperty = new List<ival_LookupCategoryValues>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtOccupancyDetails = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_getOccupancyDetails");
            for (int i = 0; i < dtOccupancyDetails.Rows.Count; i++)
            {
                ListOfMaintenanceOfProperty.Add(new ival_LookupCategoryValues
                {
                    Lookup_ID = Convert.ToInt32(dtOccupancyDetails.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtOccupancyDetails.Rows[i]["Lookup_Type"]),
                    Lookup_Value = Convert.ToString(dtOccupancyDetails.Rows[i]["Lookup_Value"])

                });
            }
            return ListOfMaintenanceOfProperty;
        }


        [HttpPost]
        public JsonResult GetUnitTypeByProperty_IdAndSelectType(string Lookup_ID, string Type_Id)
        {


            List<ival_LookupCategoryValues> unitList = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@propertyId", Lookup_ID);
            hInputPara.Add("@typeId", Type_Id);
            DataTable dtrptmaster = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUType", hInputPara);
            for (int i = 0; i < dtrptmaster.Rows.Count; i++)
            {

                unitList.Add(new ival_LookupCategoryValues
                {
                    //Lookup_ID = Convert.ToInt32(dtrptmaster.Rows[i]["Lookup_ID"]),
                    Lookup_Type = Convert.ToString(dtrptmaster.Rows[i]["Lookup_Value"]),
                    Lookup_Value = Convert.ToString(dtrptmaster.Rows[i]["Lookup_Value"])

                });


            }
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonvMUnitTypes = jsonSerialiser.Serialize(unitList);
            return Json(jsonvMUnitTypes);
        }

        //Added by meenakshi on 9sept20
        [HttpPost]
        public JsonResult GetSearchRetailByPlot(string PlotNumber, string Village_Name, string Survey_Number)
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_ProjectMaster_New> SearchLists = new List<ival_ProjectMaster_New>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@PlotNumber", PlotNumber);
            hInputPara.Add("@Village_Name", Village_Name);
            hInputPara.Add("@Survey_Number", Survey_Number);
            DataTable dtsearch = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_SearchRetailByPlotNumber", hInputPara);
            for (int i = 0; i < dtsearch.Rows.Count; i++)
            {
                SearchLists.Add(new ival_ProjectMaster_New
                {
                    Project_ID = Convert.ToInt32(dtsearch.Rows[i]["Project_ID"]),
                    PlotNumber = Convert.ToString(dtsearch.Rows[i]["Plot_Number"]),
                    Village_Name = Convert.ToString(dtsearch.Rows[i]["Village_Name"]),
                    Survey_Number = Convert.ToString(dtsearch.Rows[i]["Survey_Number"]),
                    City = Convert.ToString(dtsearch.Rows[i]["City"]),
                    District = Convert.ToString(dtsearch.Rows[i]["District"]),
                    State = Convert.ToString(dtsearch.Rows[i]["State"]),
                    Locality = Convert.ToString(dtsearch.Rows[i]["Locality"]),
                    Sub_Locality = Convert.ToString(dtsearch.Rows[i]["Sub_Locality"]),

                });
            }
            ViewBag.unitdetails = SearchLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(SearchLists);
            return Json(jsonProjectBuildingMaster);
        }

        [HttpPost]
        public JsonResult GetSearchRetailByBungalow(string BungalowNumber, string Village_Name, string Survey_Number)
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_ProjectMaster_New> SearchLists1 = new List<ival_ProjectMaster_New>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@BungalowNumber", BungalowNumber);
            hInputPara.Add("@Village_Name", Village_Name);
            hInputPara.Add("@Survey_Number", Survey_Number);
            DataTable dtsearch1 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_SearchRetailByBungalowNumber", hInputPara);
            for (int i = 0; i < dtsearch1.Rows.Count; i++)
            {
                SearchLists1.Add(new ival_ProjectMaster_New
                {
                    Project_ID = Convert.ToInt32(dtsearch1.Rows[i]["Project_ID"]),
                    BunglowNumber = Convert.ToString(dtsearch1.Rows[i]["Bunglow_Number"]),
                    Village_Name = Convert.ToString(dtsearch1.Rows[i]["Village_Name"]),
                    Survey_Number = Convert.ToString(dtsearch1.Rows[i]["Survey_Number"]),
                    City = Convert.ToString(dtsearch1.Rows[i]["City"]),
                    District = Convert.ToString(dtsearch1.Rows[i]["District"]),
                    State = Convert.ToString(dtsearch1.Rows[i]["State"]),
                    Locality = Convert.ToString(dtsearch1.Rows[i]["Locality"]),
                    Sub_Locality = Convert.ToString(dtsearch1.Rows[i]["Sub_Locality"]),

                });
            }
            ViewBag.unitdetails = SearchLists1;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster1 = jsonSerialiser.Serialize(SearchLists1);
            return Json(jsonProjectBuildingMaster1);
        }


        [HttpPost]
        public JsonResult GetSearchRetailByPlotBunglowNumber(string PlotBunglowNumber)
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_ProjectMaster_New> SearchLists = new List<ival_ProjectMaster_New>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@PlotBunglowNumber", PlotBunglowNumber);
            DataTable dtsearch = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_SearchRetailByPlotBunglowNumber", hInputPara);
            for (int i = 0; i < dtsearch.Rows.Count; i++)
            {
                SearchLists.Add(new ival_ProjectMaster_New
                {
                    Project_ID = Convert.ToInt32(dtsearch.Rows[i]["Project_ID"]),
                    PlotBunglowNumber = Convert.ToString(dtsearch.Rows[i]["Plot_Number"]),
                    Village_Name = Convert.ToString(dtsearch.Rows[i]["Village_Name"]),
                    Survey_Number = Convert.ToString(dtsearch.Rows[i]["Survey_Number"]),
                    City = Convert.ToString(dtsearch.Rows[i]["City"]),
                    District = Convert.ToString(dtsearch.Rows[i]["District"]),
                    State = Convert.ToString(dtsearch.Rows[i]["State"]),
                    Locality = Convert.ToString(dtsearch.Rows[i]["Locality"]),
                    Sub_Locality = Convert.ToString(dtsearch.Rows[i]["Sub_Locality"]),

                });
            }
            ViewBag.unitdetails = SearchLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(SearchLists);
            return Json(jsonProjectBuildingMaster);
        }


        public JsonResult GetSearchRetailByUnitNumber(string UnitNumber)
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_ProjectMaster_New> SearchLists = new List<ival_ProjectMaster_New>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@UnitNumber", UnitNumber);
            DataTable dtsearch = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_SearchRetailByUnitNumber", hInputPara);
            for (int i = 0; i < dtsearch.Rows.Count; i++)
            {
                SearchLists.Add(new ival_ProjectMaster_New
                {
                    Project_ID = Convert.ToInt32(dtsearch.Rows[i]["Project_ID"]),
                    UnitNumber = Convert.ToString(dtsearch.Rows[i]["UnitNumber"]),
                    Village_Name = Convert.ToString(dtsearch.Rows[i]["Village_Name"]),
                    Survey_Number = Convert.ToString(dtsearch.Rows[i]["Survey_Number"]),
                    City = Convert.ToString(dtsearch.Rows[i]["City"]),
                    District = Convert.ToString(dtsearch.Rows[i]["District"]),
                    State = Convert.ToString(dtsearch.Rows[i]["State"]),
                    Locality = Convert.ToString(dtsearch.Rows[i]["Locality"]),
                    Sub_Locality = Convert.ToString(dtsearch.Rows[i]["Sub_Locality"]),

                });
            }
            ViewBag.unitdetails = SearchLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(SearchLists);
            return Json(jsonProjectBuildingMaster);
        }

        public JsonResult GetSearchRetailByVillage_Name(string Village_Name)
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_ProjectMaster_New> SearchLists = new List<ival_ProjectMaster_New>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Village_Name", Village_Name);
            DataTable dtsearch = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_SearchRetailByVillage_Name", hInputPara);
            for (int i = 0; i < dtsearch.Rows.Count; i++)
            {
                SearchLists.Add(new ival_ProjectMaster_New
                {
                    Project_ID = Convert.ToInt32(dtsearch.Rows[i]["Project_ID"]),
                    PlotBunglowNumber = Convert.ToString(dtsearch.Rows[i]["PlotBunglowNumber"]),
                    Village_Name = Convert.ToString(dtsearch.Rows[i]["Village_Name"]),
                    Survey_Number = Convert.ToString(dtsearch.Rows[i]["Survey_Number"]),
                    City = Convert.ToString(dtsearch.Rows[i]["City"]),
                    District = Convert.ToString(dtsearch.Rows[i]["District"]),
                    State = Convert.ToString(dtsearch.Rows[i]["State"]),
                    Locality = Convert.ToString(dtsearch.Rows[i]["Locality"]),
                    Sub_Locality = Convert.ToString(dtsearch.Rows[i]["Sub_Locality"]),

                });
            }
            ViewBag.unitdetails = SearchLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(SearchLists);
            return Json(jsonProjectBuildingMaster);
        }


        public JsonResult GetSearchRetailBySurvey_Number(string Survey_Number)
        {
            ViewModelRequest vm = new ViewModelRequest();
            List<ival_ProjectMaster_New> SearchLists = new List<ival_ProjectMaster_New>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Survey_Number", Survey_Number);
            DataTable dtsearch = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_SearchRetailBySureyNo", hInputPara);
            for (int i = 0; i < dtsearch.Rows.Count; i++)
            {
                SearchLists.Add(new ival_ProjectMaster_New
                {
                    Project_ID = Convert.ToInt32(dtsearch.Rows[i]["Project_ID"]),
                    PlotBunglowNumber = Convert.ToString(dtsearch.Rows[i]["PlotBunglowNumber"]),
                    Village_Name = Convert.ToString(dtsearch.Rows[i]["Village_Name"]),
                    Survey_Number = Convert.ToString(dtsearch.Rows[i]["Survey_Number"]),
                    City = Convert.ToString(dtsearch.Rows[i]["City"]),
                    District = Convert.ToString(dtsearch.Rows[i]["District"]),
                    State = Convert.ToString(dtsearch.Rows[i]["State"]),
                    Locality = Convert.ToString(dtsearch.Rows[i]["Locality"]),
                    Sub_Locality = Convert.ToString(dtsearch.Rows[i]["Sub_Locality"]),

                });
            }
            ViewBag.unitdetails = SearchLists;
            var jsonSerialiser = new JavaScriptSerializer();
            var jsonProjectBuildingMaster = jsonSerialiser.Serialize(SearchLists);
            return Json(jsonProjectBuildingMaster);
        }

        #endregion



        [HttpPost]
        public ActionResult PropertyDetails(string str)
        {
            try
            {
                DataTable list = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                TempData["values"] = list;
                TempData["ddlprojecttype"] = list.Rows[0]["ddlprojecttype"].ToString();

                TempData["ddlselecttype"] = list.Rows[0]["ddlselecttype"].ToString();
                TempData["ddlunittype"] = list.Rows[0]["ddlunittype"].ToString();
                TempData["ddlretailstatusproperty"] = list.Rows[0]["ddlretailstatusproperty"].ToString();
                TempData["txtplotorbungalows"] = list.Rows[0]["txtplotorbungalows"].ToString();
                TempData["txtsureynosingle"] = list.Rows[0]["txtsureynosingle"].ToString();
                TempData["txtvillagenamesingle"] = list.Rows[0]["txtvillagenamesingle"].ToString();





            }
            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return Json(Url.Action("RetailLandMarkDetails", "Project"));
        }



        [HttpPost]
        public ActionResult RetailPropertyDetails(string str)
        {
            try
            {
                DataTable list = (DataTable)JsonConvert.DeserializeObject(str, (typeof(DataTable)));
                TempData["values"] = list;
                TempData["ddlprojecttype"] = list.Rows[0]["ddlprojecttype"].ToString();

                TempData["ddlselecttype"] = list.Rows[0]["ddlselecttype"].ToString();
                TempData["ddlunittype"] = list.Rows[0]["ddlunittype"].ToString();
                TempData["ddlretailstatusproperty"] = list.Rows[0]["ddlretailstatusproperty"].ToString();
                //TempData["txtplotorbungalows"] = list.Rows[0]["txtplotorbungalows"].ToString();
                //TempData["txtsureynosingle"] = list.Rows[0]["txtsureynosingle"].ToString();
                //TempData["txtvillagenamesingle"] = list.Rows[0]["txtvillagenamesingle"].ToString();

                TempData["txtunitnumber"] = list.Rows[0]["txtunitnumber"].ToString();
                TempData["txtsureyno"] = list.Rows[0]["txtsureyno"].ToString();
                TempData["txtvillagename"] = list.Rows[0]["txtvillagename"].ToString();




            }
            catch (Exception e)
            {
                e.InnerException.ToString();
            }
            return Json(Url.Action("RetailLandMarkDetails", "Project"));
        }

        public ActionResult SubsequentPropertydetails()
        {
            hInputPara = new Hashtable();
            hInputPara1 = new Hashtable();
            sqlDataAccess = new SQLDataAccess();

            int subRequestID = Convert.ToInt32(Session["SubRequest_ID"].ToString());
            Session["RequestID"] = subRequestID;
            hInputPara.Add("@Request_ID", subRequestID);
            DataTable dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRetailPropertyByReqID", hInputPara);
            DataTable dtprojectid = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetprjIDByRequestID", hInputPara);

            //changes by punam
            DataTable dtsocial = new DataTable();
            DataTable dtbasicinfra = new DataTable();
            DataTable dtlocaltransport = new DataTable();
            if (dtprojectid.Rows.Count > 0)
            {
                hInputPara1.Add("@Project_ID", dtprojectid.Rows[0]["Project_ID"].ToString());
                dtsocial = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectSocialDev", hInputPara1);
                dtbasicinfra = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProjectBasicInfraAvailability", hInputPara1);
                dtlocaltransport = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalTransport", hInputPara1);

            }

            if (dt.Rows.Count > 0 && dt.Columns.Count > 0)
            {
                if (DBNull.Value.Equals(dt.Rows[0]["RequestID"]))
                {
                    ViewBag.RequestID = Convert.ToInt32(!DBNull.Value.Equals(dt.Rows[0]["RequestID"]));
                }
                else
                {
                    ViewBag.RequestID = Convert.ToInt32(dt.Rows[0]["RequestID"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Builder_Group_ID"]))
                {
                    ViewBag.Builder_Group_ID = Convert.ToInt32(!DBNull.Value.Equals(dt.Rows[0]["Builder_Group_ID"]));

                }
                else
                {
                    ViewBag.Builder_Group_ID = Convert.ToInt32(dt.Rows[0]["Builder_Group_ID"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Builder_Company_ID"]))
                {
                    ViewBag.Builder_Company_ID = Convert.ToInt32(!DBNull.Value.Equals(dt.Rows[0]["Builder_Company_ID"]));

                }
                else
                {
                    ViewBag.Builder_Company_ID = Convert.ToInt32(dt.Rows[0]["Builder_Company_ID"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Is_Active"]))
                {
                    ViewBag.Is_Active = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Is_Active"]));

                }
                else
                {
                    ViewBag.Is_Active = Convert.ToString(dt.Rows[0]["Is_Active"]);


                }
                if (DBNull.Value.Equals(dt.Rows[0]["Is_Retail_Individual"]))
                {
                    ViewBag.Is_Retail_Individual = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Is_Retail_Individual"]));

                }
                else
                {
                    ViewBag.Is_Retail_Individual = Convert.ToString(dt.Rows[0]["Is_Retail_Individual"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Property_Type"]))
                {
                    ViewBag.Property_Type = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Property_Type"]));

                }
                else
                {
                    ViewBag.Property_Type = Convert.ToString(dt.Rows[0]["Property_Type"]);
                    Session["Property_Type"] = ViewBag.Property_Type;
                }
                if (DBNull.Value.Equals(dt.Rows[0]["TypeOfSelect"]))
                {
                    ViewBag.TypeOfSelect = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["TypeOfSelect"]));

                }
                else
                {
                    ViewBag.TypeOfSelect = Convert.ToString(dt.Rows[0]["TypeOfSelect"]);
                    Session["TypeOfSelect"] = ViewBag.TypeOfSelect;

                }
                if (DBNull.Value.Equals(dt.Rows[0]["TypeOfUnit"]))
                {
                    ViewBag.TypeOfUnit = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["TypeOfUnit"]));
                }
                else
                {
                    ViewBag.TypeOfUnit = Convert.ToString(dt.Rows[0]["TypeOfUnit"]);
                    Session["TypeOfUnit"] = ViewBag.TypeOfUnit;
                }

                if (DBNull.Value.Equals(dt.Rows[0]["StatusOfProperty"]))
                {
                    ViewBag.StatusOfProperty = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["StatusOfProperty"]));

                }
                else
                {
                    ViewBag.StatusOfProperty = Convert.ToString(dt.Rows[0]["StatusOfProperty"]);
                    Session["StatusOfProperty"] = ViewBag.StatusOfProperty;
                }

                if (DBNull.Value.Equals(dt.Rows[0]["Plot_Number"]))
                {
                    ViewBag.PlotBunglowNumber = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Plot_Number"]));

                }
                else
                {
                    ViewBag.PlotBunglowNumber = Convert.ToString(dt.Rows[0]["Plot_Number"]);

                }


                //if (DBNull.Value.Equals(dt.Rows[0]["Plot_No"]))
                //{
                //    ViewBag.Plot_Number = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Plot_No"]));

                //}
                //else
                //{
                //    ViewBag.Plot_Number = Convert.ToString(dt.Rows[0]["Plot_No"]);

                //}

                if (DBNull.Value.Equals(dt.Rows[0]["Ward_No"]))
                {
                    ViewBag.Ward_Number = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Ward_No"]));

                }
                else
                {
                    ViewBag.Ward_Number = Convert.ToString(dt.Rows[0]["Ward_No"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["Municipal_No"]))
                {
                    ViewBag.Municipal_No = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Municipal_No"]));

                }
                else
                {
                    ViewBag.Municipal_No = Convert.ToString(dt.Rows[0]["Municipal_No"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["Bunglow_Number"]))
                {
                    ViewBag.BunglowNumber = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Bunglow_Number"]));

                }
                else
                {
                    ViewBag.BunglowNumber = Convert.ToString(dt.Rows[0]["Bunglow_Number"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["UnitNumber"]))
                {
                    ViewBag.UnitNumber = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["UnitNumber"]));
                }
                else
                {
                    ViewBag.UnitNumber = Convert.ToString(dt.Rows[0]["UnitNumber"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Project_Name"]))
                {
                    ViewBag.Project_Name = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Project_Name"]));

                }
                else
                {
                    ViewBag.Project_Name = Convert.ToString(dt.Rows[0]["Project_Name"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Building_Name_RI"]))
                {
                    ViewBag.Building_Name_RI = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Building_Name_RI"]));

                }
                else
                {
                    ViewBag.Building_Name_RI = Convert.ToString(dt.Rows[0]["Building_Name_RI"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Wing_Name_RI"]))
                {
                    ViewBag.Wing_Name_RI = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Wing_Name_RI"]));
                }
                else
                {
                    ViewBag.Wing_Name_RI = Convert.ToString(dt.Rows[0]["Wing_Name_RI"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Survey_Number"]))
                {
                    ViewBag.Survey_Number = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Survey_Number"]));

                }
                else
                {
                    ViewBag.Survey_Number = Convert.ToString(dt.Rows[0]["Survey_Number"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Street"]))
                {
                    ViewBag.Street = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Street"]));
                }
                else
                {
                    ViewBag.Street = Convert.ToString(dt.Rows[0]["Street"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Locality"]))
                {

                    ViewBag.Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Locality"]));
                }
                else
                {

                    ViewBag.Locality = Convert.ToString(dt.Rows[0]["Locality"]);
                }
                //if (DBNull.Value.Equals(dt.Rows[0]["City"]))
                //{
                //    ViewBag.City = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["City"]));
                //}
                //else
                //{
                //    ViewBag.City = Convert.ToString(dt.Rows[0]["City"]);
                //}
                //if (DBNull.Value.Equals(dt.Rows[0]["District"]))
                //{
                //    ViewBag.District = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["District"]));
                //}
                //else
                //{
                //    ViewBag.District = Convert.ToString(dt.Rows[0]["District"]);
                //}
                //if (DBNull.Value.Equals(dt.Rows[0]["State"]))
                //{
                //    ViewBag.SelectedModelstates = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["State"]));
                //}
                //else
                //{
                //    ViewBag.SelectedModelstates = Convert.ToString(dt.Rows[0]["State"]);
                //}
                //if (DBNull.Value.Equals(dt.Rows[0]["PinCode"]))
                //{
                //    ViewBag.SelectedModelpincode = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["PinCode"]));
                //}
                //else
                //{
                //    ViewBag.SelectedModelpincode = Convert.ToString(dt.Rows[0]["PinCode"]);
                //}
                if (DBNull.Value.Equals(dt.Rows[0]["NearBy_Landmark"]))
                {
                    ViewBag.NearBy_Landmark = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["NearBy_Landmark"]));

                }
                else
                {
                    ViewBag.NearBy_Landmark = Convert.ToString(dt.Rows[0]["NearBy_Landmark"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Nearest_RailwayStation"]))
                {
                    ViewBag.Nearest_RailwayStation = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Nearest_RailwayStation"]));

                }
                else
                {
                    ViewBag.Nearest_RailwayStation = Convert.ToString(dt.Rows[0]["Nearest_RailwayStation"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Nearest_BusStop"]))
                {
                    ViewBag.Nearest_BusStop = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Nearest_BusStop"]));
                }
                else
                {
                    ViewBag.Nearest_BusStop = Convert.ToString(dt.Rows[0]["Nearest_BusStop"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Nearest_Market"]))
                {
                    ViewBag.Nearest_Market = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Nearest_Market"]));
                }
                else
                {
                    ViewBag.Nearest_Market = Convert.ToString(dt.Rows[0]["Nearest_Market"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["Nearest_Hospital"]))
                {
                    ViewBag.Nearest_Hospital = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Nearest_Hospital"]));
                }
                else
                {
                    ViewBag.Nearest_Hospital = Convert.ToString(dt.Rows[0]["Nearest_Hospital"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Address1"]))
                {
                    ViewBag.Legal_Address1 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Address1"]));
                }
                else
                {
                    ViewBag.Legal_Address1 = Convert.ToString(dt.Rows[0]["Legal_Address1"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Address2"]))
                {
                    ViewBag.Legal_Address2 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Address2"]));
                }
                else
                {
                    ViewBag.Legal_Address2 = Convert.ToString(dt.Rows[0]["Legal_Address2"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Locality"]))
                {
                    ViewBag.Legal_Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Locality"]));
                }
                else
                {
                    ViewBag.Legal_Locality = Convert.ToString(dt.Rows[0]["Legal_Locality"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Street"]))
                {
                    ViewBag.Legal_Street = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Street"]));
                }
                else
                {
                    ViewBag.Legal_Street = Convert.ToString(dt.Rows[0]["Legal_Street"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_City"]))
                {
                    ViewBag.Legal_City = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_City"]));
                }
                else
                {
                    ViewBag.Legal_City = Convert.ToString(dt.Rows[0]["Legal_City"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_District"]))
                {
                    ViewBag.Legal_District = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_District"]));
                }
                else
                {
                    ViewBag.Legal_District = Convert.ToString(dt.Rows[0]["Legal_District"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_State"]))
                {
                    ViewBag.Legal_State = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_State"]));
                }
                else
                {
                    ViewBag.Legal_State = Convert.ToString(dt.Rows[0]["Legal_State"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Legal_Pincode"]))
                {
                    ViewBag.Legal_Pincode = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Legal_Pincode"]));
                }
                else
                {
                    ViewBag.Legal_Pincode = Convert.ToString(dt.Rows[0]["Legal_Pincode"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["RERA_Approval_Num"]))
                {
                    ViewBag.RERA_Approval_Num = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["RERA_Approval_Num"]));
                }
                else
                {
                    ViewBag.RERA_Approval_Num = Convert.ToString(dt.Rows[0]["RERA_Approval_Num"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approval_Flag"]))
                {
                    ViewBag.Approval_Flag = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approval_Flag"]));

                }
                else
                {
                    ViewBag.Approval_Flag = Convert.ToString(dt.Rows[0]["Approval_Flag"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Total_Land_Area"]))
                {
                    ViewBag.Total_Land_Area = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Total_Land_Area"]));

                }
                else
                {
                    ViewBag.Total_Land_Area = Convert.ToString(dt.Rows[0]["Total_Land_Area"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Total_Permissible_FSI"]))
                {
                    ViewBag.Total_Permissible_FSI = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Total_Permissible_FSI"]));

                }
                else
                {
                    ViewBag.Total_Permissible_FSI = Convert.ToString(dt.Rows[0]["Total_Permissible_FSI"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_Builtup_Area"]))
                {
                    ViewBag.Approved_Builtup_Area = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_Builtup_Area"]));

                }
                else
                {
                    ViewBag.Approved_Builtup_Area = Convert.ToString(dt.Rows[0]["Approved_Builtup_Area"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["Postal_Address1"]))
                {

                    ViewBag.Postal_Address1 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Address1"]));
                }
                else
                {
                    ViewBag.Postal_Address1 = Convert.ToString(dt.Rows[0]["Postal_Address1"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Buildings"]))
                {
                    ViewBag.Approved_No_of_Buildings = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Buildings"]));
                }
                else
                {
                    ViewBag.Approved_No_of_Buildings = Convert.ToString(dt.Rows[0]["Approved_No_of_Buildings"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_plotsorflats"]))
                {
                    ViewBag.Approved_No_of_plotsorflats = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_plotsorflats"]));
                }
                else
                {
                    ViewBag.Approved_No_of_plotsorflats = Convert.ToString(dt.Rows[0]["Approved_No_of_plotsorflats"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Wings"]))
                {
                    ViewBag.Approved_No_of_Wings = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Wings"]));
                }
                else
                {
                    ViewBag.Approved_No_of_Wings = Convert.ToString(dt.Rows[0]["Approved_No_of_Wings"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Floors"]))
                {

                    ViewBag.Approved_No_of_Floors = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Floors"]));
                }
                else
                {

                    ViewBag.Approved_No_of_Floors = Convert.ToString(dt.Rows[0]["Approved_No_of_Floors"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["Distance_from_Airport"]))
                {

                    ViewBag.Distance_from_Airport = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Distance_from_Airport"]));
                }
                else
                {

                    ViewBag.Distance_from_Airport = Convert.ToString(dt.Rows[0]["Distance_from_Airport"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["txtdistanceland"]))
                {

                    ViewBag.txtdistanceland = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["txtdistanceland"]));
                }
                else
                {

                    ViewBag.txtdistanceland = Convert.ToString(dt.Rows[0]["txtdistanceland"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["txtdistancerailway"]))
                {

                    ViewBag.txtdistancerailway = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["txtdistancerailway"]));
                }
                else
                {

                    ViewBag.txtdistancerailway = Convert.ToString(dt.Rows[0]["txtdistancerailway"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["txtdistancebus"]))
                {

                    ViewBag.txtdistancebus = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["txtdistancebus"]));
                }
                else
                {

                    ViewBag.txtdistancebus = Convert.ToString(dt.Rows[0]["txtdistancebus"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["txtdistancehospital"]))
                {

                    ViewBag.txtdistancehospital = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["txtdistancehospital"]));
                }
                else
                {

                    ViewBag.txtdistancehospital = Convert.ToString(dt.Rows[0]["txtdistancehospital"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["txtdistancemarket"]))
                {

                    ViewBag.txtdistancemarket = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["txtdistancemarket"]));
                }
                else
                {

                    ViewBag.txtdistancemarket = Convert.ToString(dt.Rows[0]["txtdistancemarket"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["txtdistanceairport"]))
                {

                    ViewBag.txtdistanceairport = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["txtdistanceairport"]));
                }
                else
                {

                    ViewBag.txtdistanceairport = Convert.ToString(dt.Rows[0]["txtdistanceairport"]);
                }
               

                if (DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Units"]))
                {
                    ViewBag.Approved_No_of_Units = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_No_of_Units"]));
                }
                else
                {
                    ViewBag.Approved_No_of_Units = Convert.ToString(dt.Rows[0]["Approved_No_of_Units"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Actual_No_Floors_G"]))
                {
                    ViewBag.Actual_No_Floors_G = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Actual_No_Floors_G"]));
                }
                else
                {
                    ViewBag.Actual_No_Floors_G = Convert.ToString(dt.Rows[0]["Actual_No_Floors_G"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Floor_Of_Unit"]))
                {
                    ViewBag.Floor_Of_Unit = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Floor_Of_Unit"]));
                }
                else
                {
                    ViewBag.Floor_Of_Unit = Convert.ToString(dt.Rows[0]["Floor_Of_Unit"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Property_Description"]))
                {
                    ViewBag.Property_Description = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Property_Description"]));
                }
                else
                {
                    ViewBag.Property_Description = Convert.ToString(dt.Rows[0]["Property_Description"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Type_Of_Structure"]))
                {
                    ViewBag.SelectedModelRetailTypeOfStructure = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Type_Of_Structure"]));
                }
                else
                {
                    ViewBag.SelectedModelRetailTypeOfStructure = Convert.ToString(dt.Rows[0]["Type_Of_Structure"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Approved_Usage_Of_Property"]))
                {
                    ViewBag.SelectedApprovedusage = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Approved_Usage_Of_Property"]));
                }
                else
                {
                    ViewBag.SelectedApprovedusage = Convert.ToString(dt.Rows[0]["Approved_Usage_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Actual_Usage_Of_Property"]))
                {
                    ViewBag.SelectedActualusage = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Actual_Usage_Of_Property"]));
                }
                else
                {
                    ViewBag.SelectedActualusage = Convert.ToString(dt.Rows[0]["Actual_Usage_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Current_Age_Of_Property"]))
                {
                    ViewBag.Current_Age_Of_Property = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Current_Age_Of_Property"]));
                }
                else
                {
                    ViewBag.Current_Age_Of_Property = Convert.ToString(dt.Rows[0]["Current_Age_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Residual_Age_Of_Property"]))
                {
                    ViewBag.Residual_Age_Of_Property = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Residual_Age_Of_Property"]));
                }
                else
                {
                    ViewBag.Residual_Age_Of_Property = Convert.ToString(dt.Rows[0]["Residual_Age_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Common_Areas"]))
                {
                    ViewBag.Common_Areas = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Common_Areas"]));
                }
                else
                {
                    ViewBag.Common_Areas = Convert.ToString(dt.Rows[0]["Common_Areas"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Facilities"]))
                {
                    ViewBag.Facilities = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Facilities"]));
                }
                else
                {
                    ViewBag.Facilities = Convert.ToString(dt.Rows[0]["Facilities"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Anyother_Observation"]))
                {
                    ViewBag.Anyother_Observation = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Anyother_Observation"]));
                }
                else
                {
                    ViewBag.Anyother_Observation = Convert.ToString(dt.Rows[0]["Anyother_Observation"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Roofing_Terracing"]))
                {
                    ViewBag.Roofing_Terracing = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Roofing_Terracing"]));
                }
                else
                {
                    ViewBag.Roofing_Terracing = Convert.ToString(dt.Rows[0]["Roofing_Terracing"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Quality_Of_Fixture"]))
                {
                    ViewBag.Quality_Of_Fixture = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Quality_Of_Fixture"]));
                }
                else
                {
                    ViewBag.Quality_Of_Fixture = Convert.ToString(dt.Rows[0]["Quality_Of_Fixture"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Quality_Of_Construction"]))
                {
                    ViewBag.SelectedModelRetailQualityOfConstruction = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Quality_Of_Construction"]));
                }
                else
                {
                    ViewBag.SelectedModelRetailQualityOfConstruction = Convert.ToString(dt.Rows[0]["Quality_Of_Construction"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Maintenance_Of_Property"]))
                {

                    ViewBag.SelectedModelRetailMaintenanceOfProp = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Maintenance_Of_Property"]));
                }
                else
                {

                    ViewBag.SelectedModelRetailMaintenanceOfProp = Convert.ToString(dt.Rows[0]["Maintenance_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Marketability_Of_Property"]))
                {
                    ViewBag.Marketability_Of_Property = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Marketability_Of_Property"]));
                }
                else
                {
                    ViewBag.Marketability_Of_Property = Convert.ToString(dt.Rows[0]["Marketability_Of_Property"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Occupancy_Details"]))
                {
                    ViewBag.SelectedModelRetailOccupancyDetails = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Occupancy_Details"]));
                }
                else
                {

                    ViewBag.SelectedModelRetailOccupancyDetails = Convert.ToString(dt.Rows[0]["Occupancy_Details"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Renting_Potential"]))
                {
                    ViewBag.Renting_Potential = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Renting_Potential"]));
                }
                else
                {
                    ViewBag.Renting_Potential = Convert.ToString(dt.Rows[0]["Renting_Potential"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Expected_Monthly_Rental"]))
                {
                    ViewBag.Expected_Monthly_Rental = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Expected_Monthly_Rental"]));
                }
                else
                {
                    ViewBag.Expected_Monthly_Rental = Convert.ToString(dt.Rows[0]["Expected_Monthly_Rental"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Name_Of_Occupant"]))
                {
                    ViewBag.Name_Of_Occupant = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Name_Of_Occupant"]));
                }
                else
                {
                    ViewBag.Name_Of_Occupant = Convert.ToString(dt.Rows[0]["Name_Of_Occupant"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Occupied_Since"]))
                {
                    ViewBag.Occupied_Since = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Occupied_Since"]));
                }
                else
                {
                    ViewBag.Occupied_Since = Convert.ToString(dt.Rows[0]["Occupied_Since"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Monthly_Rental"]))
                {
                    ViewBag.Monthly_Rental = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Monthly_Rental"]));
                }
                else
                {
                    ViewBag.Monthly_Rental = Convert.ToString(dt.Rows[0]["Monthly_Rental"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Balanced_Leased_Period"]))
                {
                    ViewBag.Balanced_Leased_Period = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Balanced_Leased_Period"]));
                }
                else
                {
                    ViewBag.Balanced_Leased_Period = Convert.ToString(dt.Rows[0]["Balanced_Leased_Period"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Specifications"]))
                {
                    ViewBag.Specifications = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Specifications"]));
                }
                else
                {
                    ViewBag.Specifications = Convert.ToString(dt.Rows[0]["Specifications"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Amenities"]))
                {
                    ViewBag.Amenities = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Amenities"]));
                }
                else
                {
                    ViewBag.Amenities = Convert.ToString(dt.Rows[0]["Amenities"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Deviation_Description"]))
                {
                    ViewBag.Deviation_Description = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Deviation_Description"]));

                }
                else
                {
                    ViewBag.Deviation_Description = Convert.ToString(dt.Rows[0]["Deviation_Description"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Cord_Latitude"]))
                {
                    ViewBag.Cord_Latitude = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Cord_Latitude"]));
                }
                else
                {
                    ViewBag.Cord_Latitude = Convert.ToString(dt.Rows[0]["Cord_Latitude"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Cord_Longitude"]))
                {
                    ViewBag.Cord_Longitude = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Cord_Longitude"]));
                }
                else
                {
                    ViewBag.Cord_Longitude = Convert.ToString(dt.Rows[0]["Cord_Longitude"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width1"]))
                {
                    ViewBag.AdjoiningRoad_Width1 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width1"]));
                }
                else
                {
                    ViewBag.AdjoiningRoad_Width1 = Convert.ToString(dt.Rows[0]["AdjoiningRoad_Width1"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width2"]))
                {
                    ViewBag.AdjoiningRoad_Width2 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width2"]));
                }
                else
                {
                    ViewBag.AdjoiningRoad_Width2 = Convert.ToString(dt.Rows[0]["AdjoiningRoad_Width2"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width3"]))
                {
                    ViewBag.AdjoiningRoad_Width3 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width3"]));
                }
                else
                {
                    ViewBag.AdjoiningRoad_Width3 = Convert.ToString(dt.Rows[0]["AdjoiningRoad_Width3"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width4"]))
                {
                    ViewBag.AdjoiningRoad_Width4 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["AdjoiningRoad_Width4"]));
                }
                else
                {
                    ViewBag.AdjoiningRoad_Width4 = Convert.ToString(dt.Rows[0]["AdjoiningRoad_Width4"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Social_Development"]))
                {
                    ViewBag.Social_Development = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Social_Development"]));
                }
                else
                {
                    ViewBag.Social_Development = Convert.ToString(dt.Rows[0]["Social_Development"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Social_Infrastructure"]))
                {
                    ViewBag.Social_Infrastructure = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Social_Infrastructure"]));
                }
                else
                {
                    ViewBag.Social_Infrastructure = Convert.ToString(dt.Rows[0]["Social_Infrastructure"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Type_of_Locality"]))
                {
                    ViewBag.SelectedModellookuptypeOfLocality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Type_of_Locality"]));
                }
                else
                {
                    ViewBag.SelectedModellookuptypeOfLocality = Convert.ToString(dt.Rows[0]["Type_of_Locality"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["Class_of_Locality"]))
                {

                    ViewBag.SelectedModellookupclassOfLocality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Class_of_Locality"]));
                }
                else
                {

                    ViewBag.SelectedModellookupclassOfLocality = Convert.ToString(dt.Rows[0]["Class_of_Locality"]);
                }
                if (DBNull.Value.Equals(dt.Rows[0]["RERA_Registration_No"]))
                {
                    ViewBag.RERA_Registration_No = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["RERA_Registration_No"]));
                }
                else
                {
                    ViewBag.RERA_Registration_No = Convert.ToString(dt.Rows[0]["RERA_Registration_No"]);
                }

                if (DBNull.Value.Equals(dt.Rows[0]["DistFrom_City_Center"]))
                {
                    ViewBag.DistFrom_City_Center = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["DistFrom_City_Center"]));

                }
                else
                {
                    ViewBag.DistFrom_City_Center = Convert.ToString(dt.Rows[0]["DistFrom_City_Center"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["Locality_Remarks"]))
                {
                    ViewBag.Locality_Remarks = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Locality_Remarks"]));

                }
                else
                {
                    ViewBag.Locality_Remarks = Convert.ToString(dt.Rows[0]["Locality_Remarks"]);

                }

                if (DBNull.Value.Equals(dt.Rows[0]["Village_Name"]))
                {
                    ViewBag.Village_Name = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Village_Name"]));

                }
                else
                {
                    ViewBag.Village_Name = Convert.ToString(dt.Rows[0]["Village_Name"]);

                }
                if (DBNull.Value.Equals(dt.Rows[0]["plot_no"]))
                {
                    ViewBag.Plot_Number = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["plot_no"]));

                }
                else
                {
                    ViewBag.Plot_Number = Convert.ToString(dt.Rows[0]["plot_no"]);

                }




                //if (DBNull.Value.Equals(dt.Rows[0]["Sub_Locality"]))
                //{
                //    ViewBag.Sub_Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Sub_Locality"]));

                //}
                //else
                //{
                //    ViewBag.Sub_Locality = Convert.ToString(dt.Rows[0]["Sub_Locality"]);

                //}

                //if (DBNull.Value.Equals(dt.Rows[0]["Sub_Locality"]))
                //{
                //    ViewBag.Sub_Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Sub_Locality"]));

                //}
                //else
                //{
                //    ViewBag.Sub_Locality = Convert.ToString(dt.Rows[0]["Sub_Locality"]);

                //}

                //ViewBag.Postal_Address2 = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Address2"]));
                //ViewBag.Postal_Street = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Street"]));
                //ViewBag.Postal_Locality = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Locality"]));
                //ViewBag.Postal_City = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_City"]));
                //ViewBag.Postal_District = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_District"]));
                //ViewBag.Postal_State = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_State"]));
                //ViewBag.Postal_Pincode = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_Pincode"]));
                //ViewBag.Postal_NearbyLandmark = Convert.ToString(!DBNull.Value.Equals(dt.Rows[0]["Postal_NearbyLandmark"]));






            }
            else
            {

            }
            ViewModel vm = new ViewModel();
            // vm.ival_LocalTransportForProject = Gettransport();
            //vm.ival_SocialDevelopmentForProject = GetSocialInfra();
            int sprojectId = Convert.ToInt32(dtprojectid.Rows[0]["Project_ID"].ToString());
            //vm.ival_BasicInfraAvailability = GetSocialBasicInfra();
            vm.ival_ProjectBuildingWingFloorDetails = GetFloorListbyreqID();
            vm.ival_ProjectBuildingWingFloorDetails = GetProjectFloorWingDetails(sprojectId);
            foreach (var a in vm.ival_ProjectBuildingWingFloorDetails)
            {
                if (a.FloorType_ID == "Podium")
                {
                    ViewBag.pocheck = a.Project_ID;
                    ViewBag.podium = a.Number_Of_Floors;
                }
                if (a.FloorType_ID == "Upper Habitable Floors")
                {
                    ViewBag.upcheck = a.Project_ID;
                    ViewBag.upper = a.Number_Of_Floors;
                }
                if (a.FloorType_ID == "Basement")
                {
                    ViewBag.BaseCheck = a.Project_ID;
                    ViewBag.basement = a.Number_Of_Floors;
                }
                if (a.FloorType_ID == "Ground")
                {
                    ViewBag.GroCheck = a.Project_ID;
                    ViewBag.Ground = a.Number_Of_Floors;
                }
                if (a.FloorType_ID == "Stilt")
                {
                    ViewBag.stiltCheck = a.Project_ID;
                    ViewBag.Stilt = a.Number_Of_Floors;
                }
                if (a.FloorType_ID == "second floor")
                {
                    ViewBag.secCheck = a.Project_ID;
                    ViewBag.secondfloor = a.Number_Of_Floors;
                }

            }



            vm.ival_States = GetStates();
            vm.ival_LookupCategoryValuespincode = Getpincode();


            vm.ival_StatesList = GetState();
            vm.PincodeList = GetPincode();

            vm.ival_DistrictsList = GetDistrict();
            vm.ival_CitiesList = GetCity();
            vm.ival_pincodelist = GetLoc();
            vm.ival_LocalityList = GetLoc();
            vm.ival_subLocalityList = GetSubLoc();

            vm.ival_LookupCategoryValuesTypeOfStrycture = GetTypeOfStructure();
            vm.ival_LookupCategoryValuesQualityOfConstruction = GetQualityOfConstruction();
            vm.ival_LookupCategoryValuesMaintenanceOfproperty = GetMaintenanceOfProperty();
            vm.ival_LookupCategoryValuesOccupancyDetails = GetOccupancyDetails();
            vm.ival_LookupCategoryValuesClassOfLocality = GetClassoflocality();
            vm.ival_LookupCategoryValuesTypeOfLocality = GetTypeOfLocality();
            vm.ival_LookupCategoryValuesProjecttype = GetProjectTypeMaster();

            vm.ival_LookupCategoryValuesSocialDev = GetSocialDevelopment();
            if (dtprojectid.Rows.Count > 0)
            {
                int projectId = Convert.ToInt32(dtprojectid.Rows[0]["Project_ID"]);
                vm.ival_ProjectUnitDetails = GetProjectUnitDetails(projectId);
                vm.ival_LookupCategoryValuesTransaportEdit = GetProjectLocalTransportDetails(projectId);
                vm.ival_commonarea = GetCoomaDetails(projectId);
                vm.ival_commonarea2 = GetCoomaDetails2(projectId);
                vm.ival_commonarea3 = GetCoomaDetails3(projectId);
            }
            vm.ival_LookupCategoryValuesconnectivity = GetConnectivity();

            vm.ival_LookupCategoryValuesbasicInfra = GetBasicInfra();

            vm.ival_CommonAreaI = GetCommonAreas();
            vm.ival_CommonAreaI2 = GetCommonAreas2();
            vm.ival_CommonAreaI3 = GetCommonAreas3();
            vm.ival_LookupCategoryValuesFloorType = GetFloorsType();

            if (dtsocial.Rows.Count > 0 && dtsocial != null)
            {
                List<ival_SocialDevelopmentForProject> Listsocial = new List<ival_SocialDevelopmentForProject>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                for (int i = 0; i < dtsocial.Rows.Count; i++)
                {
                    Listsocial.Add(new ival_SocialDevelopmentForProject
                    {
                        Project_ID = Convert.ToInt32(dtsocial.Rows[i]["Project_ID"]),
                        Social_Dev_ID = Convert.ToInt32(dtsocial.Rows[i]["Social_Dev_ID"]),
                        Social_Development_Type = Convert.ToString(dtsocial.Rows[i]["Social_Development_Type"]),


                    });
                }
                //test
                ViewBag.Listsocial = JsonConvert.SerializeObject(Listsocial);
            }
            if (dtlocaltransport.Rows.Count > 0 && dtlocaltransport != null)
            {
                List<ival_LocalTransportForProject> Listlocal = new List<ival_LocalTransportForProject>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                for (int i = 0; i < dtlocaltransport.Rows.Count; i++)
                {
                    Listlocal.Add(new ival_LocalTransportForProject
                    {
                        Project_ID = Convert.ToInt32(dtlocaltransport.Rows[i]["Project_ID"]),
                        ProjectLocalTransport_ID = Convert.ToInt32(dtlocaltransport.Rows[i]["ProjectLocalTransport_ID"]),
                        LocalTransport_ID = Convert.ToString(dtlocaltransport.Rows[i]["LocalTransport_ID"]),


                    });
                }
                //test
                ViewBag.Listlocaltransport = JsonConvert.SerializeObject(Listlocal);
            }

            if (dtbasicinfra.Rows.Count > 0 && dtbasicinfra != null)
            {
                List<ival_BasicInfraAvailability> Listbinfra = new List<ival_BasicInfraAvailability>();
                hInputPara = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                for (int i = 0; i < dtbasicinfra.Rows.Count; i++)
                {
                    Listbinfra.Add(new ival_BasicInfraAvailability
                    {
                        Project_ID = Convert.ToInt32(dtbasicinfra.Rows[i]["Project_ID"]),
                        BasicInfra_type = Convert.ToString(dtbasicinfra.Rows[i]["BasicInfra_type"]),
                        ProjectInfra_ID = Convert.ToInt32(dtbasicinfra.Rows[i]["ProjectInfra_ID"]),


                    });
                }
                //test
                ViewBag.Listbasicinfra = JsonConvert.SerializeObject(Listbinfra);
            }

            IEnumerable<ival_States> ival_StatesList = GetStates();
            if (ival_StatesList.ToList().Count > 0 && ival_StatesList != null)
            {
                var BuilderState = ival_StatesList.Where(s => s.State_Name == dt.Rows[0]["State"].ToString()).Select(s => s.State_ID);

                vm.SelectedModelstates = Convert.ToInt32(BuilderState.FirstOrDefault());

            }


            IEnumerable<ival_Districts> ival_DistrictsList = GetDistrict();
            if (ival_DistrictsList.ToList().Count > 0 && ival_DistrictsList != null)
            {
                var BuilderDistrict = ival_DistrictsList.Where(s => s.District_Name == dt.Rows[0]["District"].ToString()).Select(s => s.District_ID);

                vm.selectedDistID = Convert.ToInt32(BuilderDistrict.FirstOrDefault());

            }
            IEnumerable<ival_Cities> ival_LocalityList = GetLoc();
            if (ival_LocalityList.ToList().Count > 0 && ival_LocalityList != null)
            {
                var BuilderDistrict = ival_LocalityList.Where(s => s.Loc_Name == dt.Rows[0]["Loc_Name"].ToString()).Select(s => s.Loc_ID);

                vm.selectedLocID = Convert.ToInt32(BuilderDistrict.FirstOrDefault());

            }
            IEnumerable<ival_Cities> ival_SubLocalityList = GetSubLoc();
            if (ival_SubLocalityList.ToList().Count > 0 && ival_SubLocalityList != null)
            {
                var BuilderDistrict = ival_SubLocalityList.Where(s => s.SubLoc_Name == dt.Rows[0]["SubLoc_Name"].ToString()).Select(s => s.SubLoc_ID);

                vm.selectedSLocID = Convert.ToInt32(BuilderDistrict.FirstOrDefault());

            }

            IEnumerable<ival_Cities> ival_CitiesList = GetCity();
            if (ival_CitiesList.ToList().Count > 0 && ival_CitiesList != null)
            {
                var BuilderCity = ival_CitiesList.Where(s => s.City_Name == dt.Rows[0]["City"].ToString()).Select(s => s.City_ID);

                vm.selectedCityID = Convert.ToInt32(BuilderCity.FirstOrDefault());

            }
            IEnumerable<ival_Cities> ival_pincodelist = GetLoc();
            if (ival_pincodelist.ToList().Count > 0 && ival_pincodelist != null)
            {
                var builderpostalpincode = ival_pincodelist.Where(s => s.Pin_Code == dt.Rows[0]["Pincode"].ToString()).Select(s => s.Loc_ID);

                vm.SelectedModelpincode = Convert.ToInt32(builderpostalpincode.FirstOrDefault());

            }

            return View(vm);
        }
        public IEnumerable<ival_States> GetState()
        {
            List<ival_States> listState = new List<ival_States>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetStateByID");
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                listState.Add(new ival_States
                {
                    State_ID = Convert.ToInt32(dtState.Rows[i]["State_ID"]),
                    State_Name = Convert.ToString(dtState.Rows[i]["State_Name"])

                });


            }
            return listState.AsEnumerable();
        }
        public IEnumerable<ival_Districts> GetDistrict()
        {
            List<ival_Districts> listDistrict = new List<ival_Districts>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDistrictByID");
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                listDistrict.Add(new ival_Districts
                {
                    District_ID = Convert.ToInt32(dtState.Rows[i]["District_ID"]),
                    District_Name = Convert.ToString(dtState.Rows[i]["District_Name"])

                });


            }
            return listDistrict.AsEnumerable();
        }
        public IEnumerable<ival_Districts> GetDistrictN(string State)
        {
            List<ival_Districts> listDistrict = new List<ival_Districts>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@State", State);
            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDistrictByIDN", hInputPara);
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                listDistrict.Add(new ival_Districts
                {
                    District_ID = Convert.ToInt32(dtState.Rows[i]["District_ID"]),
                    District_Name = Convert.ToString(dtState.Rows[i]["District_Name"])

                });


            }
            return listDistrict.AsEnumerable();
        }
        public IEnumerable<ival_Cities> Getpincodebyloc()
        {
            List<ival_Cities> listCity = new List<ival_Cities>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtpin = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLocalityByID");
            for (int i = 0; i < dtpin.Rows.Count; i++)
            {

                listCity.Add(new ival_Cities
                {
                    Loc_ID = Convert.ToInt32(dtpin.Rows[i]["Loc_ID"]),
                    Pin_Code = Convert.ToString(dtpin.Rows[i]["Pin_Code"])

                });


            }
            return listCity.AsEnumerable();
        }
        public IEnumerable<ival_Cities> GetCity()
        {
            List<ival_Cities> listCity = new List<ival_Cities>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCityByID");
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                listCity.Add(new ival_Cities
                {
                    City_ID = Convert.ToInt32(dtState.Rows[i]["City_ID"]),

                    City_Name = Convert.ToString(dtState.Rows[i]["City"])


                });


            }
            return listCity.AsEnumerable();
        }
        public IEnumerable<ival_Cities> GetCityN(string District)
        {
            List<ival_Cities> listCity = new List<ival_Cities>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@District", District);
            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCityByIdN", hInputPara);
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                listCity.Add(new ival_Cities
                {
                    City_ID = Convert.ToInt32(dtState.Rows[i]["City_ID"]),

                    City_Name = Convert.ToString(dtState.Rows[i]["City"])


                });


            }
            return listCity.AsEnumerable();
        }
        public IEnumerable<ival_Cities> GetLoc()
        {
            List<ival_Cities> ival_LocalityList = new List<ival_Cities>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Getlocality");
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                ival_LocalityList.Add(new ival_Cities
                {
                    Loc_Id = Convert.ToInt32(dtState.Rows[i]["Loc_ID"]),
                    Loc_ID = Convert.ToInt32(dtState.Rows[i]["Loc_ID"]),
                    Loc_Name = Convert.ToString(dtState.Rows[i]["Loc_Name"]),
                    Pin_Code = Convert.ToString(dtState.Rows[i]["pin_code"])
                });


            }
            return ival_LocalityList.AsEnumerable();
        }
        public IEnumerable<ival_Cities> GetLocN(string District)
        {
            List<ival_Cities> ival_LocalityList = new List<ival_Cities>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@District", District);

            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetlocalityN", hInputPara);
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                ival_LocalityList.Add(new ival_Cities
                {
                    Loc_Id = Convert.ToInt32(dtState.Rows[i]["Loc_ID"]),
                    Loc_ID = Convert.ToInt32(dtState.Rows[i]["Loc_ID"]),
                    Loc_Name = Convert.ToString(dtState.Rows[i]["Loc_Name"]),
                    Pin_Code = Convert.ToString(dtState.Rows[i]["pin_code"])
                });


            }
            return ival_LocalityList.AsEnumerable();
        }
        public IEnumerable<ival_Cities> GetLocW(string Locality)
        {
            List<ival_Cities> ival_LocalityList = new List<ival_Cities>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@Loc_Name", Locality);

            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetlocalityPIN", hInputPara);
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                ival_LocalityList.Add(new ival_Cities
                {
                    Loc_Id = Convert.ToInt32(dtState.Rows[i]["Loc_ID"]),
                    Loc_ID = Convert.ToInt32(dtState.Rows[i]["Loc_ID"]),
                    Loc_Name = Convert.ToString(dtState.Rows[i]["Loc_Name"]),
                    Pin_Code = Convert.ToString(dtState.Rows[i]["pin_code"])
                });


            }
            return ival_LocalityList.AsEnumerable();
        }
        public IEnumerable<ival_Cities> GetSubLoc()
        {
            List<ival_Cities> listCity = new List<ival_Cities>();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Getsublocalityid");
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                listCity.Add(new ival_Cities
                {
                    SubLoc_ID = Convert.ToInt32(dtState.Rows[i]["sublocality_id"]),
                    SubLoc_Name = Convert.ToString(dtState.Rows[i]["sub_Locality"]),


                });


            }
            return listCity.AsEnumerable();
        }
        public IEnumerable<ival_Cities> GetSubLocN(string locality)
        {
            List<ival_Cities> listCity = new List<ival_Cities>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            hInputPara.Add("@loc", locality);
            DataTable dtState = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetsublocalityidN", hInputPara);
            for (int i = 0; i < dtState.Rows.Count; i++)
            {

                listCity.Add(new ival_Cities
                {
                    SubLoc_ID = Convert.ToInt32(dtState.Rows[i]["sublocality_id"]),
                    SubLoc_Name = Convert.ToString(dtState.Rows[i]["sub_Locality"]),


                });


            }
            return listCity.AsEnumerable();
        }
        public IEnumerable<ival_LookupCategoryValues> GetPincode()
        {
            List<ival_LookupCategoryValues> listPincode = new List<ival_LookupCategoryValues>();
            hInputPara = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dtPincode = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPincode");
            for (int i = 0; i < dtPincode.Rows.Count; i++)
            {

                listPincode.Add(new ival_LookupCategoryValues
                {
                    Lookup_Value = Convert.ToString(dtPincode.Rows[i]["Lookup_Value"]),
                    Lookup_ID = Convert.ToInt32(dtPincode.Rows[i]["Lookup_ID"])

                });


            }
            return listPincode.AsEnumerable();
        }
    }
}