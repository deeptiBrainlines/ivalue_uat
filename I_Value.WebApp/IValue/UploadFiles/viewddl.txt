@model Ashish.ViewModel
@{
	Layout = null;
}

<!DOCTYPE html>
<!-- template from http://getbootstrap.com/getting-started -->

<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<style type="text/css">
			div {
				margin: 10px;
			}
			label {
				display: inline-block;
				width: 100px;
			}
			input[type="text"] {
				  box-sizing: border-box;
				width: 200px;
			}
			select {
				width: 200px;
			}
			input[type="submit"] {
				margin-left: 115px;
			}

			.field-validation-error {
				display:block;
				margin-left: 110px;
				color: #ff0000;
			}

		</style>
	</head>
	<body>
		@using (Html.BeginForm())
		{
			<div>
				@Html.LabelFor(m => m.SelectedCity)
				@Html.DropDownListFor(m => m.SelectedCity, Model.CityList, "Please select")
				@Html.ValidationMessageFor(m => m.SelectedCity)
			</div>
			<div>
				@Html.LabelFor(m => m.SelectedLocality)
				@Html.DropDownListFor(m => m.SelectedLocality, Model.LocalityList)
				@Html.ValidationMessageFor(m => m.SelectedLocality)
			</div>
			<div>
				@Html.LabelFor(m => m.SelectedSubLocality)
				@Html.DropDownListFor(m => m.SelectedSubLocality, Model.SubLocalityList)
				@Html.ValidationMessageFor(m => m.SelectedSubLocality)
			</div>	
			
			<input type="submit" value="save" />
		}


		<!-- JS includes -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
		<script src="//ajax.aspnetcdn.com/ajax/mvc/4.0/jquery.validate.unobtrusive.min.js"></script>
		
		<script type="text/javascript">
			var localityUrl = '@Url.Action("FetchLocalities")';
			var subLocalityUrl = '@Url.Action("FetchSubLocalities")';
			var localities = $('#SelectedLocality');
			var subLocalities = $('#SelectedSubLocality');
			$('#SelectedCity').change(function() {
				localities.empty();
				subLocalities.empty();
				$.getJSON(localityUrl, { ID: $(this).val() },function(data) {
					if (!data) {
						return;
					}
					localities.append($('<option></option>').val('').text('Please select'));
					$.each(data, function(index, item) {
						localities.append($('<option></option>').val(item.Value).text(item.Text));
					});
				});
			})
			$('#SelectedLocality').change(function() {
				subLocalities.empty();
				$.getJSON(subLocalityUrl, { ID: $(this).val() },function(data) {
					if (!data) {
						return;
					}
					subLocalities.append($('<option></option>').val('').text('Please select'));
					$.each(data, function(index, item) {
						subLocalities.append($('<option></option>').val(item.Value).text(item.Text));
					});
				});
			})				  

		</script>
	</body>
</html>
