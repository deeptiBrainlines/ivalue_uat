 
 ---------------------- SP Paras -----------------------

 DECLARE @iMarketID INT
 DECLARE @iPeriodID INT

 -----------------------------------------------------

DECLARE @iCounter                          INT = 1, 
        @iRowCount                         INT = 1, 
        @dBonusSlabRateGainedPips          DECIMAL(15, 2), 
        @fTotalGainedPips                  FLOAT = 0, 
        @iSignalProviderID                 INT = 0, 
        @sdtPeriodFrom                     SMALLDATETIME, 
        @sdtPeriodTill                     SMALLDATETIME, 
        @dPlanCommissionRate               DECIMAL(18, 2), 
        @dFixedAmount                      DECIMAL(18, 2), 
        @dSuccessSignalsCount              INT = 0, 
        @iPinnedSignalsTradersCount        INT = 0, 
        @iAverageGainedPips                INT = 0, 
        @dSuccessEarningsRate              DECIMAL(18, 3), 
        @dAmountForThisMarketForThisPeriod DECIMAL(18, 3), 
        @dSuccessEarningsAmount            DECIMAL(18, 3), 
        @iSuccessSignalsCount              INT = 0, 
        @iBonusSubRate                     FLOAT = 0, 
        @fTotalSuccessSignalsCount         FLOAT = 0, 
        @fTotalPinnedSignalsTradersCount   FLOAT = 0, 
        @fPinnedEarningsAmount             FLOAT = 0, 
        @fPinedRate                        FLOAT = 0,--Need to discuss 
        @fBonusEarningsAmount              FLOAT = 0, 
        @fTotalEarnings                    FLOAT = 0


 IF OBJECT_ID('tempdb..SignalPerformanceEarningsPeriod') IS NOT NULL
	BEGIN
		DROP TABLE #SignalPerformanceEarningsPeriod
    END

 IF OBJECT_ID('tempdb..#SignalProviderEarningsBreakupForPeriod') IS NOT NULL
	BEGIN
		DROP TABLE #SignalProviderEarningsBreakupForPeriod
    END

 IF OBJECT_ID('tempdb..#UserPlanSubscriptions') IS NOT NULL
	BEGIN
		DROP TABLE #UserPlanSubscriptions
	END

 IF OBJECT_ID('tempdb..#SignalDashboardPinned') IS NOT NULL
	BEGIN
		DROP TABLE #SignalDashboardPinned
	END

	CREATE TABLE #SignalPerformanceEarningsPeriod
	( 
		 SignalperformanceearningsPeriodID INT IDENTITY(1, 1), 
		 MarketID                          INT, 
		 StartFrom                         DATETIME, 
		 ValidTill                         DATETIME, 
		 TotalPlanDays                     INT 
	) 

	CREATE TABLE #signalproviderearningsbreakupforperiod 
	( 
		 SignalProviderEarningsBreakupForPeriodID INT IDENTITY(1, 1), 
		 MarketID                                 INT, 
		 PeriodID                                 INT, 
		 PeriodFrom                               SMALLDATETIME, 
		 PeriodTill                               SMALLDATETIME, 
		 SignalProviderID                         INT, 
		 PlanCommissionRate                       DECIMAL(18, 3), 
		 FixedAmount                              DECIMAL(18, 3), 
		 SuccessSignalsCount                      INT, 
		 PinnedSignalsTradersCount                INT, 
		 AverageGainedPips                        INT, 
		 SuccessEarningsRate                      DECIMAL(18, 3), 
		 PinnedRate                               DECIMAL(18, 3), 
		 BonusSlabRateGainedPips                  DECIMAL(18, 3), 
		 SuccessEarningsAmount                    DECIMAL(18, 3), 
		 PinnedEarningsAmount                     DECIMAL(18, 3), 
		 BonusEarningsAmount                      DECIMAL(18, 3), 
		 TotalEarnings                            DECIMAL(18, 3) 
	) 
	
	CREATE TABLE #UserPlansubScriptions 
	( 
		 UserPlansubScriptionsID   INT IDENTITY(1, 1), 
		 UserID                    INT, 
		 PlanID                    INT, 
		 PlanCommissionRate        DECIMAL(18, 3), 
		 FixedAmount               DECIMAL(18, 3), 
		 SuccessSignalsCount       INT, 
		 PinnedSignalsTradersCount INT, 
		 AverageGainedPips         INT, 
		 SuccessEarningsRate       DECIMAL(18, 3), 
		 PinnedRate                DECIMAL(18, 3), 
		 BonusSlabRateGainedPips   DECIMAL(18, 3), 
	) 

	CREATE TABLE #signaldashboardpinned 
	( 
		 TempSignalID     INT IDENTITY(1, 1), 
		 SignalID         INT IDENTITY(1, 1), 
		 SignalProviderID INT 
	) 

	ALTER TABLE SignalPerformanceEarningsPeriod 
		ADD PRIMARY KEY CLUSTERED (SignalperformanceearningsPeriodID) 

	ALTER TABLE #signalproviderearningsbreakupforperiod 
		ADD PRIMARY KEY CLUSTERED (SignalProviderEarningsBreakupForPeriodID) 

	ALTER TABLE #userplansubscriptions 
		ADD PRIMARY KEY CLUSTERED (UserPlansubScriptionsID) 

	INSERT SignalPerformanceEarningsPeriod (MarketID, StartFrom, StartFrom, TotalPlanDays)
	SELECT MarketID, StartFrom, ValidTill, (ValidTill - StartFrom)AS TotalPlanDays  
	FROM SignalPerformanceEarningsPeriod
	WHERE MarketID = @iMarketID AND SignalperformanceearningsPeriodID = @iPeriodID

	INSERT #SignalDashboardPinned (SignalID, SignalProviderID)
	SELECT S.SignalsID, S.SigProID 
	FROM Signals S
	INNER JOIN SignalDashboardPinned SDP ON S.SignalsID = SDP.SigID
 

	INSERT #UserPlanSubscriptions ( UserID, PlanID, PlanCommissionRate, FixedAmount, SuccessSignalsCount, PinnedSignalsTradersCount, AverageGainedPips, SuccessEarningsRate, 
		PinnedRate, bonusslabrategainedpips )
	SELECT SU.UserID, UP.UserPlansID, UP.Commission, UP.FixedAmount, SOP.TotalSuccessSignal, 007, SOP.AverageGainedPips, SOP.SuccessRate, 0, 0
	FROM [dbo].[SignalProviderOverallPerformanceForPeriod] SOP
	INNER JOIN [dbo].[secApplicationUsers] AU ON SOP.SigProID = AU.ApplicationUserID
	INNER JOIN [dbo].[secUsers] SU ON AU.ApplicationUserID = SU.UserID
	INNER JOIN [dbo].[secRoles] SR ON  SU.RoleID = SR.RoleID
	INNER JOIN [dbo].[UserPlanSubscriptions] UPS ON SU.UserID = UPS.UserID
	INNER JOIN [dbo].[UserPlans] UP ON UPS.PlanID = UP.UserPlansID
	WHERE SU.UserIsActive = 1 AND UPS.StatusID = 1 AND UP.StatusID = 1 AND SR.RoleName = 'Signal Provider' 

	SELECT @iRowCount = COUNT(UserPlanSubscriptionsID) 
	FROM #UserPlanSubscriptions

	SELECT @dAmountForThisMarketForThisPeriod = AmountForThisMarketForThisPeriod
	FROM [dbo].[TraderRevenueCalculationsForPeriod]
	WHERE  MarketID = @iMarketID AND PeriodID = @iPeriodID
	 
	SELECT @fTotalSuccessSignalsCount = SUM(SuccessSignalsCount) 
	FROM #SignalProviderEarningsBreakupForPeriod

	SELECT @fTotalPinnedSignalsTradersCount =  SUM(TraderID) 
	FROM [dbo].[SignalDashboardPinned]

	WHILE(@iCounter <= @iRowCount)
	BEGIN

	SELECT @iSignalProviderID = SignalProviderID 
	FROM #SignalDashboardPinned
	WHERE TempSignalID = @iCounter

	SELECT @sdtPeriodFrom = PeriodFrom,	@sdtPeriodTill = PeriodTill, @dPlanCommissionRate = PlanCommissionRate,	@dFixedAmount = FixedAmount, @dSuccessSignalsCount = SuccessSignalsCount,
		@iAverageGainedPips = AverageGainedPips, @dSuccessEarningsRate = SuccessEarningsRate
	FROM #SignalProviderEarningsBreakupForPeriod	
	WHERE SignalProviderID = @iSignalProviderID AND MarketID = @iMarketID AND PeriodID = @iPeriodID

	SELECT @iPinnedSignalsTradersCount = COUNT(*) 
	FROM #SignalDashboardPinned
	WHERE SignalProviderID = @iSignalProviderID

	--SELECT @iPinnedSignalsTradersCount = PinnedSignalsTradersCount
	--FROM #SignalDashboardPinned
	--WHERE SignalProviderID = @iSignalProviderID

	SELECT @iBonusSubRate = BonusRate 
	FROM SignalProviderBonusRates 
	WHERE MarketID = @iMarketID AND (@iAverageGainedPips BETWEEN FromPipsPercentRate AND ToPipsPercentRate)	

	SELECT @dSuccessEarningsAmount = (CAST(@dSuccessSignalsCount AS FLOAT)/CAST(@fTotalSuccessSignalsCount AS FLOAT)) * CAST(@dSuccessEarningsRate  AS FLOAT) * CAST(@dPlanCommissionRate  AS FLOAT)  * CAST(@dAmountForThisMarketForThisPeriod  AS FLOAT) 
	SELECT @fPinnedEarningsAmount = (CAST(@iPinnedSignalsTradersCount AS FLOAT)/CAST(@fTotalPinnedSignalsTradersCount AS FLOAT)) * CAST(@fPinedRate  AS FLOAT)  * CAST(@dAmountForThisMarketForThisPeriod  AS FLOAT) 
	SELECT @fBonusEarningsAmount = CAST(@iBonusSubRate AS FLOAT) * CAST(@dSuccessEarningsAmount AS FLOAT) 
	SELECT @fTotalEarnings = @dSuccessEarningsAmount + @fPinnedEarningsAmount + @fBonusEarningsAmount

	INSERT	#SignalProviderEarningsBreakupForPeriod( MarketID, PeriodID, PeriodFrom, PeriodTill, SignalProviderID, PlanCommissionRate, FixedAmount, SuccessSignalsCount, 
	PinnedSignalsTradersCount, AverageGainedPips, SuccessEarningsRate, PinnedRate, BonusSlabRateGainedPips, SuccessEarningsAmount, PinnedEarningsAmount,
		BonusEarningsAmount, TotalEarnings )
	SELECT	@iMarketID, @iPeriodID, @sdtPeriodFrom, @sdtPeriodTill, @iSignalProviderID, @dPlanCommissionRate, @dFixedAmount, @dSuccessSignalsCount, @iPinnedSignalsTradersCount,
		@iAverageGainedPips, @dSuccessEarningsRate, @fPinedRate, @iBonusSubRate, @dSuccessEarningsAmount, @fPinnedEarningsAmount, @fBonusEarningsAmount, @fTotalEarnings
	FROM #SignalDashboardPinned
		
	SET @iCounter = @iCounter + 1


	END


