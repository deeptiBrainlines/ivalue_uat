namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_WingUnitFloorDetails
    {
        [Key]
        public int UnitFloor_ID { get; set; }

        public int? Unit_ID { get; set; }

        [StringLength(50)]
        public string Approved_Carpet_Area { get; set; }

        [StringLength(50)]
        public string Approved_Builtup_Area { get; set; }

        [StringLength(50)]
        public string Measured_Carpet_Area { get; set; }

        [StringLength(50)]
        public string Measured_Builtup_Area { get; set; }

        [StringLength(50)]
        public string Adopted_Area { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}
