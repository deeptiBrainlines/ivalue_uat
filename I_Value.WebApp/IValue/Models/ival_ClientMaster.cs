namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_ClientMaster
    {
        [Key]
        public int Client_ID { get; set; }

        public bool? IsInstitute { get; set; }

        [StringLength(50)]
        public string Client_Name { get; set; }

        [StringLength(50)]
        public string Branch_Name { get; set; }

        [StringLength(50)]
        public string Client_First_Name { get; set; }

        [StringLength(50)]
        public string Client_Last_Name { get; set; }

        [StringLength(50)]
        public string Client_Contact_Num { get; set; }

        [StringLength(50)]
        public string Client_Contact_Email { get; set; }

        [StringLength(50)]
        public string Client_Designation { get; set; }

        [StringLength(100)]
        public string Client_Address1 { get; set; }

        [StringLength(100)]
        public string Client_Address2 { get; set; }

        [StringLength(50)]
        public string Client_City { get; set; }

        [StringLength(50)]
        public string Client_Locality { get; set; }

        [StringLength(50)]
        public string Client_District { get; set; }

        [StringLength(50)]
        public string Client_State { get; set; }

        //[Column(TypeName = "numeric")]
        public decimal? Client_Pincode { get; set; }

        [StringLength(50)]
        public string PAN_Num { get; set; }

        [StringLength(50)]
        public string GSTIN_Num { get; set; }

        [StringLength(50)]
        public string Basic_Fees { get; set; }

        [StringLength(50)]
        public string Additional_Fees { get; set; }

        [StringLength(50)]
        public string OutofGeo_Fees { get; set; }

        [StringLength(50)]
        public string Total_Fees { get; set; }

        [StringLength(50)]
        public string Valuation_Type { get; set; }

        [StringLength(50)]
        public string Report_Type { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}
