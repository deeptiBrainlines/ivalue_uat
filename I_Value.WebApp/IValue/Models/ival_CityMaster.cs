﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public class ival_CityMaster
    {
        [Key]
        public int District_ID { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "District is required.")]
        public string District_Name { get; set; }

        [StringLength(250)]
        [Required(ErrorMessage = "City is required.")]
        public string City { get; set; }

        public int City_Id { get; set; }


    }
}