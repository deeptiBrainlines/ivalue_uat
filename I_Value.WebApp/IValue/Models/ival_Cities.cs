namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_Cities
    {
        [Key]
        public int Loc_Id { get; set; }

        public int? State_ID { get; set; }

        [StringLength(250)]
        public string City_Name { get; set; }

        [StringLength(250)]
        public string Pin_Code { get; set; }

        [Key]
        public int City_ID { get; set; }

        [Key]
        public int Loc_ID { get; set; }

        [StringLength(250)]
        public string Loc_Name { get; set; }



        [Key]
        public int SubLoc_ID { get; set; }

        [StringLength(250)]
        public string SubLoc_Name { get; set; }
    }
}
