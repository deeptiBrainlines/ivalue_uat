﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public class ival_natureOfCompany
    {
        public int NatureId { get; set; }
        public string NatureName { get; set; }
    }
}