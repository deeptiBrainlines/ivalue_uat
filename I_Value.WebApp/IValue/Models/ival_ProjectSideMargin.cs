namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_ProjectSideMargin
    {
        [Key]
        public int Side_Margin_ID { get; set; }

        public int Project_ID { get; set; }

        public int? Building_ID { get; set; }

        public int? Wing_ID { get; set; }

        public int? Unit_ID { get; set; }

        public bool? Is_Active { get; set; }

        [StringLength(50)]
        public string Side_Margin_Description { get; set; }

        [StringLength(50)]
        public string As_per_Approved { get; set; }

        [StringLength(50)]
        public string As_per_Measurement { get; set; }

        [StringLength(50)]
        public string As_per_Approvals { get; set; }

        [StringLength(50)]
        public string As_per_Local_Bye_Laws { get; set; }

        [StringLength(50)]
        public string As_Actuals { get; set; }

        [StringLength(50)]
        public string Deviation_No { get; set; }

        [StringLength(50)]
        public string Percentage_Deviation { get; set; }

        [StringLength(50)]
        public string Deviation_Description { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }

        [StringLength(50)]
        public string Floor_Of_Unit { get; set; }
    }
}
