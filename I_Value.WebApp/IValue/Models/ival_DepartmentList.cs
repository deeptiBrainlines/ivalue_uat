﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public class ival_DepartmentList
    {
        public IEnumerable<ival_ClientAndBranchMasterNew> ival_ClientAndBranchMasterNew { get; set; }
        public int SrNo { get; set; }
        public int Client_ID { get; set; }
        public int Client_Id { get; set; }
        public int SelectedDeptID1 { get; set; }
        public string SelectedDeptID { get; set; }
        [StringLength(50)]
        public string Branch_Name { get; set; }

        public string Branch_District { get; set; }

        public string Branch_City { get; set; }
        public string Branch_Locality { get; set; }
        public int Branch_Pincode { get; set; }
        public string Branch_State { get; set; }
        [StringLength(50)]
        public string Address1 { get; set; }
        [StringLength(50)]
        public string Address2 { get; set; }
        public string Contact_Name { get; set; }
        [StringLength(50)]
        public string Contact_No { get; set; }
        [StringLength(50)]
        public string Client_Desgn { get; set; }
        [StringLength(50)]
        public string Client_Email { get; set; }
        public string Basic_Fees { get; set; }
        [StringLength(50)]
        public string Additional_Fees { get; set; }

        [StringLength(50)]
        public string OutofGeo_Fees { get; set; }

        [StringLength(50)]
        public string Total_Fees { get; set; }
        public int Dept_ID { get; set; }
        public int Dept_Id { get; set; }
        public int Row_ID { get; set; }
       // public int Dept_Name { get; set; }
        public int Branch_Id { get; set; }
        [StringLength(50)]
        public string Dept_Name { get; set; }
        
        public string Client_Name { get; set; }
        [StringLength(50)]
        public string Contact_Num { get; set; }
        [StringLength(50)]
        public string Client_Desgn1 { get; set; }
        [StringLength(50)]
        public string Client_Mail { get; set; }
        public int Lookup_ID { get; set; }
        [StringLength(50)]
        public string Lookup_Value { get; set; }
    }
}