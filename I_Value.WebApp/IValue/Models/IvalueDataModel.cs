namespace I_Value.WebApp.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using IValuePublishProject.Models;

    public partial class IvalueDataModel : DbContext
    {
        public IvalueDataModel()
            : base("name=IvalueDataModel1")
        {
        }

        public virtual DbSet<ival_ProjectBuildingWingFloorDetails> ival_ProjectBuildingWingFloorDetails { get; set; }
        public virtual DbSet<ival_BasicInfraAvailability> ival_BasicInfraAvailability { get; set; }
        public virtual DbSet<ival_BranchMaster> ival_BranchMaster { get; set; }
        public virtual DbSet<ival_BuilderCompanyMaster> ival_BuilderCompanyMaster { get; set; }
        public virtual DbSet<ival_BuilderGroupMaster> ival_BuilderGroupMaster { get; set; }
        public virtual DbSet<ival_BuildingWingFloorDetails> ival_BuildingWingFloorDetails { get; set; }
        public virtual DbSet<ival_CategoryMaster> ival_CategoryMaster { get; set; }
        public virtual DbSet<ival_Cities> ival_Cities { get; set; }
        public virtual DbSet<ival_ClientMaster> ival_ClientMaster { get; set; }
        public virtual DbSet<ival_Districts> ival_Districts { get; set; }
        public virtual DbSet<ival_Document_Uploaded_List> ival_Document_Uploaded_List { get; set; }
        public virtual DbSet<ival_DocumentList> ival_DocumentList { get; set; }
        public virtual DbSet<ival_EmployeeMaster> ival_EmployeeMaster { get; set; }
        public virtual DbSet<ival_LocalTransportForProject> ival_LocalTransportForProject { get; set; }
        public virtual DbSet<ival_LookupCategoryValues> ival_LookupCategoryValues { get; set; }
        public virtual DbSet<ival_ProjBldgWingMaster> ival_ProjBldgWingMaster { get; set; }
        public virtual DbSet<ival_ProjBldgWingUnitMaster> ival_ProjBldgWingUnitMaster { get; set; }
        public virtual DbSet<ival_ProjectApprovalDetails> ival_ProjectApprovalDetails { get; set; }
        public virtual DbSet<ival_ProjectBoundaries> ival_ProjectBoundaries { get; set; }
        public virtual DbSet<ival_ProjectBuildingMaster> ival_ProjectBuildingMaster { get; set; }
        public virtual DbSet<ival_ProjectMaster> ival_ProjectMaster { get; set; }
        public virtual DbSet<ival_ProjectSideMargin> ival_ProjectSideMargin { get; set; }
        public virtual DbSet<ival_ProjectUnitDetails> ival_ProjectUnitDetails { get; set; }
        public virtual DbSet<ival_RequestDetails> ival_RequestDetails { get; set; }
        public virtual DbSet<ival_RoleMaster> ival_RoleMaster { get; set; }
        public virtual DbSet<ival_States> ival_States { get; set; }
        public virtual DbSet<ival_UniPriceParameterMaster> ival_UniPriceParameterMaster { get; set; }
        public virtual DbSet<ival_UnitParameterMaster> ival_UnitParameterMaster { get; set; }
        public virtual DbSet<ival_UnitParametersValue> ival_UnitParametersValue { get; set; }
        public virtual DbSet<ival_UnitPriceParametersValue> ival_UnitPriceParametersValue { get; set; }
        public virtual DbSet<ival_UnitTypeParamterLink> ival_UnitTypeParamterLink { get; set; }
        public virtual DbSet<ival_UnitypePriceParameterLink> ival_UnitypePriceParameterLink { get; set; }
        public virtual DbSet<ival_Valuation_TypeMaster> ival_Valuation_TypeMaster { get; set; }
        public virtual DbSet<ival_WingUnitFloorDetails> ival_WingUnitFloorDetails { get; set; }
        public virtual DbSet<ZNIU_ival_Report_TypeMaster> ZNIU_ival_Report_TypeMaster { get; set; }
        public virtual DbSet<ival_RequestDetails_New> ival_RequestDetails_New { get; set; }
        public virtual DbSet<ival_SocialDevelopmentForProject> ival_SocialDevelopmentForProject { get; set; }
        public virtual DbSet<ival_ProjectMaster_New> ival_ProjectMaster_New { get; set; }

        public virtual DbSet<iva_Comparable_Properties> iva_Comparable_Properties { get; set; }
        public virtual DbSet<ival_BrokerDetails> ival_BrokerDetails { get; set; }
        public virtual DbSet<Image_Uploaded_List> Image_Uploaded_List { get; set; }

        
        public virtual DbSet<ival_ImageMaster> ival_ImageMaster { get; set; }
        public virtual DbSet<ival_ProjectFloorWiseBreakUp> ival_ProjectFloorWiseBreakUp { get; set; }
        public virtual DbSet<ival_BuilderCompanyMasterNew> ival_BuilderCompanyMasterNew { get; set; }

        
        
        
        public virtual DbSet<ival_ProjectConstructionWorkProgress> ival_ProjectConstructionWorkProgress { get; set; }
        public virtual DbSet<ival_ProjectMasterDetails> ival_ProjectMasterDetails { get; set; }
        public virtual DbSet<ival_ProjectRequestDetails> ival_ProjectRequestDetails { get; set; }
       
        
        public virtual DbSet<ival_ProjectValuationAreaDetails> ival_ProjectValuationAreaDetails { get; set; }
        public virtual DbSet<ival_ProjectValuationPricing> ival_ProjectValuationPricing { get; set; }
        public virtual DbSet<ival_ProjectValuationSideMargin> ival_ProjectValuationSideMargin { get; set; }
        public virtual DbSet<ival_DocumentListNew> ival_DocumentListNew { get; set; }
        public virtual DbSet<ival_ProjectUnitDescription> ival_ProjectUnitDescription { get; set; }
        public virtual DbSet<ival_ProjectValuationAmenitiesInfrastructureProgress> ival_ProjectValuationAmenitiesInfrastructureProgress { get; set; }

        public virtual DbSet<ival_ProjectValuationConstructionWorkProgress> ival_ProjectValuationConstructionWorkProgress { get; set; }
        public virtual DbSet<ival_ProjectDetails> ival_ProjectDetails { get; set; }
        public virtual DbSet<ival_Project_Image_UploadedList> ival_Project_Image_UploadedList { get; set; }

        public virtual DbSet<ival_PropertyType> ival_PropertyType { get; set; }
        public virtual DbSet<ival_SelectType> ival_SelectType { get; set; }
        public virtual DbSet<ival_UnitType> ival_UnitType { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ival_BasicInfraAvailability>()
                .Property(e => e.BasicInfra_type)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BasicInfraAvailability>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BasicInfraAvailability>()
                .Property(e => e.Updated_Date)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Branch_Code)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Branch_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Branch_Address1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Branch_Address2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Branch_City)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Branch_District)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Branch_State)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Branch_Pincode)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Lessor_First_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Lessor_Middle_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Lessor_Last_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Lessor_Address1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Lessor_Address2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Lessor_City)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Lessor_District)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Lessor_State)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Lessor_Pincode)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Lessor_Contact_Num)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Lessor_Email_ID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Monthly_Rent)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Maintaince_Charges)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Other_Charges)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.LandLord_Bank)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.LandLord_Branch)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.LandLord_Address1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.LandLord_Address2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.LandLord_City)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.LandLord_District)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.LandLord_State)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.LandLord_Pincode)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Account_Type)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Account_Number)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.IFSC_Num)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.PAN_Num)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.GSTIN_Num)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Is_Active)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BranchMaster>()
                .Property(e => e.Modified_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMaster>()
                .Property(e => e.Builder_Company_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMaster>()
                .Property(e => e.Builder_Legal_Address1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMaster>()
                .Property(e => e.Builder_Legal_Address2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMaster>()
                .Property(e => e.Builder_C_City)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMaster>()
                .Property(e => e.Builder_C_District)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMaster>()
                .Property(e => e.Builder_C_State)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMaster>()
                .Property(e => e.Builder_C_Pincode)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_BuilderCompanyMaster>()
                .Property(e => e.Builder_Postal_Address1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMaster>()
                .Property(e => e.Builder_Postal_Address2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMaster>()
                .Property(e => e.Builder_Postal_City)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMaster>()
                .Property(e => e.Builder_Postal_District)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMaster>()
                .Property(e => e.Builder_Postal_State)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMaster>()
                .Property(e => e.Builder_Postal_Pincode)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_BuilderCompanyMaster>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMaster>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.BuilderGroupName)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Builder_Address1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Builder_Address2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Builder_Locality)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Builder_State)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Builder_District)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Builder_City)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Builder_Pincode)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Nearby_Landmark)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Director_Name1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Director_Phone1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Director_Mobile1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Director_Email1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Director_Name2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Director_Phone2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Director_Mobile2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Director_Email2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Director_Name3)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Director_Phone3)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Director_Mobile3)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Director_Email3)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Builder_Grade)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderGroupMaster>()
                .Property(e => e.Builder_Street)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuildingWingFloorDetails>()
                .Property(e => e.FloorType_ID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuildingWingFloorDetails>()
                .Property(e => e.Number_Of_Floors)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuildingWingFloorDetails>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuildingWingFloorDetails>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_CategoryMaster>()
                .Property(e => e.Category_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_CategoryMaster>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_CategoryMaster>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_Cities>()
                .Property(e => e.City_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Client_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Branch_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Client_First_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Client_Last_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Client_Contact_Num)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Client_Contact_Email)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Client_Designation)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Client_Address1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Client_Address2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Client_City)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Client_District)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Client_State)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Client_Pincode)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.PAN_Num)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.GSTIN_Num)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Basic_Fees)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Additional_Fees)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.OutofGeo_Fees)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Total_Fees)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Valuation_Type)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Report_Type)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ClientMaster>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_Districts>()
                .Property(e => e.District_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.First_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Middle_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Last_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Contact_Number);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Company_Email_ID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Personal_Email_ID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Current_Address_1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Current_Address_2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Current_City)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Current_District)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Current_State)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Current_Pincode)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Perm_Address_1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Perm_Address_2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Perm_City)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Perm_District)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Perm_State)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Perm_Pincode)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Qualification)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Emergency_Contact_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Emergency_Contact_No)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Relationship_with_Employee)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Blood_Group)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Aadhar_No)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Pan_No)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Bank_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Bank_Address1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Bank_Address2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Bank_City)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Bank_State)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Bank_Pincode)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Bank_IFSC_Code)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_EmployeeMaster>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_LocalTransportForProject>()
                .Property(e => e.LocalTransport_ID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_LocalTransportForProject>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_LocalTransportForProject>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_LookupCategoryValues>()
                .Property(e => e.Lookup_Type)
                .IsUnicode(false);

            modelBuilder.Entity<ival_LookupCategoryValues>()
                .Property(e => e.Lookup_Value)
                .IsUnicode(false);

            modelBuilder.Entity<ival_LookupCategoryValues>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_LookupCategoryValues>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingMaster>()
                .Property(e => e.Wing_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingMaster>()
                .Property(e => e.Approved_No_Of_Units)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingMaster>()
                .Property(e => e.Approved_Area_Of_Wing)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingMaster>()
                .Property(e => e.Remarks)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingMaster>()
                .Property(e => e.Approval_Flag)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingMaster>()
                .Property(e => e.RERA_Registration_No)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingMaster>()
                .Property(e => e.No_of_Buildings)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingMaster>()
                .Property(e => e.No_Of_Wings)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingMaster>()
                .Property(e => e.No_of_Floors)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingMaster>()
                .Property(e => e.No_of_Units)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingMaster>()
                .Property(e => e.Expected_completion_date)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingMaster>()
                .Property(e => e.Deviation_Description)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingMaster>()
                .Property(e => e.Cord_Latitude)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingMaster>()
                .Property(e => e.Cord_Longitude)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingMaster>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingMaster>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingUnitMaster>()
                .Property(e => e.UType)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingUnitMaster>()
                .Property(e => e.PTypeName)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingUnitMaster>()
                .Property(e => e.RERA_Registration_No)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingUnitMaster>()
                .Property(e => e.No_of_Buildings)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingUnitMaster>()
                .Property(e => e.No_Of_Wings)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingUnitMaster>()
                .Property(e => e.No_of_Floors)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingUnitMaster>()
                .Property(e => e.No_of_Units)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingUnitMaster>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjBldgWingUnitMaster>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectApprovalDetails>()
                .Property(e => e.Approval_Type)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectApprovalDetails>()
                .Property(e => e.Approving_Authority)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectApprovalDetails>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectApprovalDetails>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBoundaries>()
                .Property(e => e.Boundry_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBoundaries>()
                .Property(e => e.AsPer_Document)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBoundaries>()
                .Property(e => e.AsPer_Site)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBoundaries>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBoundaries>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBuildingMaster>()
                .Property(e => e.Building_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBuildingMaster>()
                .Property(e => e.No_of_Wings)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBuildingMaster>()
                .Property(e => e.Approved_Building_Area)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBuildingMaster>()
                .Property(e => e.Approved_No_of_Units)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBuildingMaster>()
                .Property(e => e.Remarks)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBuildingMaster>()
                .Property(e => e.Cord_Latitutde)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBuildingMaster>()
                .Property(e => e.Cord_Longitude)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBuildingMaster>()
                .Property(e => e.Deviation_Description)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBuildingMaster>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBuildingMaster>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.P_TypeID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Project_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Legal_Address1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Legal_Address2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Legal_Locality)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Legal_Street)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Legal_City)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Legal_District)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Legal_State)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Legal_Pincode)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Postal_Address1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Postal_Address2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Postal_Street)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Postal_Locality)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Postal_City)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Postal_District)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Postal_State)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Postal_Pincode)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Postal_NearbyLandmark)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.RERA_Approval_Num)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Total_Land_Area)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Total_Permissible_FSI)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Approved_Builtup_Area)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Approved_No_of_Buildings)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Approved_No_of_plotsorflats)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Approved_No_of_Wings)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Approved_No_of_Floors)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Approved_No_of_Units)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Specifications)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Amenities)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Deviation_Description)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Cord_Latitude)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Cord_Longitude)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.AdjoiningRoad_Width1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.AdjoiningRoad_Width2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.AdjoiningRoad_Width3)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.AdjoiningRoad_Width4)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Social_Infrastructure)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Class_of_Locality)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.DistFrom_City_Center)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Locality_Remarks)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectSideMargin>()
                .Property(e => e.Side_Margin_Description)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectSideMargin>()
                .Property(e => e.As_per_Approved)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectSideMargin>()
                .Property(e => e.As_per_Measurement)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectSideMargin>()
                .Property(e => e.Percentage_Deviation)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectSideMargin>()
                .Property(e => e.Deviation_Description)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectSideMargin>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectSideMargin>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectUnitDetails>()
                .Property(e => e.UType_ID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectUnitDetails>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectUnitDetails>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Request_Code)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Customer_ID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Customer_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Customer_Contact_No)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
               .Property(e => e.Address1)
               .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                           .Property(e => e.Address2)
                           .IsUnicode(false);



            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.District)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.PinCode)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.AssignedToEmpID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.PropertyType)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.ValuationType)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.ReportType)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Unit_ID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Type_of_Structure)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Current_Age_of_Structure)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Expected_Balance_Life)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Quality_of_Construction)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Mainte_of_Property)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Occupancy_Details)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Tenant_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Lease_Period)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Lease_Rental)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Approved_Usage)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Actual_Usage)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Total_Cost_of_Property)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Stage_of_Construction)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Stage_Description)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Percent_Completed)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Current_Value)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Government_Rate)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Government_Value)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RoleMaster>()
                .Property(e => e.Role_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RoleMaster>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RoleMaster>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_States>()
                .Property(e => e.State_Code)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ival_States>()
                .Property(e => e.State_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_UniPriceParameterMaster>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_UnitParameterMaster>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_UnitParametersValue>()
                .Property(e => e.UnitParameter_Value)
                .IsUnicode(false);

            modelBuilder.Entity<ival_UnitPriceParametersValue>()
                .Property(e => e.UnitPriceParameter_Value)
                .IsUnicode(false);

            modelBuilder.Entity<ival_UnitTypeParamterLink>()
                .Property(e => e.PType)
                .IsUnicode(false);

            modelBuilder.Entity<ival_UnitTypeParamterLink>()
                .Property(e => e.UnitType)
                .IsUnicode(false);

            modelBuilder.Entity<ival_UnitypePriceParameterLink>()
                .Property(e => e.PType)
                .IsUnicode(false);

            modelBuilder.Entity<ival_UnitypePriceParameterLink>()
                .Property(e => e.UnitType)
                .IsUnicode(false);

            modelBuilder.Entity<ival_Valuation_TypeMaster>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_WingUnitFloorDetails>()
                .Property(e => e.Approved_Carpet_Area)
                .IsUnicode(false);

            modelBuilder.Entity<ival_WingUnitFloorDetails>()
                .Property(e => e.Approved_Builtup_Area)
                .IsUnicode(false);

            modelBuilder.Entity<ival_WingUnitFloorDetails>()
                .Property(e => e.Measured_Carpet_Area)
                .IsUnicode(false);

            modelBuilder.Entity<ival_WingUnitFloorDetails>()
                .Property(e => e.Measured_Builtup_Area)
                .IsUnicode(false);

            modelBuilder.Entity<ival_WingUnitFloorDetails>()
                .Property(e => e.Adopted_Area)
                .IsUnicode(false);

            modelBuilder.Entity<ival_WingUnitFloorDetails>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_WingUnitFloorDetails>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ZNIU_ival_Report_TypeMaster>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
              .Property(e => e.Property_Type)
              .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.TypeOfUnit)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.StatusOfProperty)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.PlotBunglowNumber)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.UnitNumber)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Project_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Building_Name_RI)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Wing_Name_RI)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Survey_Number)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Village_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Street)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Locality)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Sub_Locality)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.District)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.PinCode)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.NearBy_Landmark)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Nearest_RailwayStation)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Nearest_BusStop)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Nearest_Hospital)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Nearest_Market)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Legal_Address1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Legal_Address2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Legal_Locality)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Legal_Street)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Legal_City)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Legal_District)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Legal_State)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Legal_Pincode)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Postal_Address1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Postal_Address2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Postal_Street)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Postal_Locality)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Postal_City)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Postal_District)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Postal_State)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Postal_Pincode)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Postal_NearbyLandmark)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.RERA_Approval_Num)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Approval_Flag)
                .IsFixedLength();

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Total_Land_Area)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Total_Permissible_FSI)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Approved_Builtup_Area)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Approved_No_of_Buildings)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Approved_No_of_plotsorflats)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Approved_No_of_Wings)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Approved_No_of_Floors)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Approved_No_of_Units)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Actual_No_Floors_G)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Floor_Of_Unit)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Property_Description)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Type_Of_Structure)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Approved_Usage_Of_Property)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Actual_Usage_Of_Property)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Current_Age_Of_Property)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Residual_Age_Of_Property)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Common_Areas)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Facilities)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Anyother_Observation)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Roofing_Terracing)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Quality_Of_Fixture)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Quality_Of_Construction)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Maintenance_Of_Property)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Marketability_Of_Property)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Renting_Potential)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Expected_Monthly_Rental)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Name_Of_Occupant)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Occupied_Since)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Monthly_Rental)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Balanced_Leased_Period)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Specifications)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Amenities)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Deviation_Description)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Cord_Latitude)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Cord_Longitude)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.AdjoiningRoad_Width1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.AdjoiningRoad_Width2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.AdjoiningRoad_Width3)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.AdjoiningRoad_Width4)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Social_Development)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Social_Infrastructure)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Class_of_Locality)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.DistFrom_City_Center)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Locality_Remarks)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMaster_New>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
               .Property(e => e.Request_Flag)
               .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.Request_Type)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.Client_ID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.Department)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.Loan_Type)
                .IsFixedLength();

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.Requestor_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.Customer_Application_ID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.Customer_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.Customer_Contact_No_1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.Customer_Contact_No_2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.Email_ID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.Valuation_Request_ID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.Document_Holder_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.Type_Of_Ownership)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.AssignedToEmpID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.PropertyType)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.ValuationType)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.ReportType)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.Method_of_Valuation)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_RequestDetails_New>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);
            modelBuilder.Entity<ival_SocialDevelopmentForProject>()
               .Property(e => e.Social_Development_Type)
               .IsUnicode(false);

            modelBuilder.Entity<ival_SocialDevelopmentForProject>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_SocialDevelopmentForProject>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_Document_Uploaded_List>()
               .Property(e => e.Uploaded_Document)
               .IsUnicode(false);

            modelBuilder.Entity<ival_DocumentList>()
                .Property(e => e.Document_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBuildingWingFloorDetails>()
              .Property(e => e.FloorType_ID)
              .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBuildingWingFloorDetails>()
                .Property(e => e.Number_Of_Floors)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBuildingWingFloorDetails>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectBuildingWingFloorDetails>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);
            modelBuilder.Entity<iva_Comparable_Properties>()
              .Property(e => e.Project_Name)
              .IsUnicode(false);

            modelBuilder.Entity<iva_Comparable_Properties>()
                .Property(e => e.Dist_Subject_Property)
                .IsUnicode(false);

            modelBuilder.Entity<iva_Comparable_Properties>()
                .Property(e => e.Comparable_Unit_Details)
                .IsUnicode(false);

            modelBuilder.Entity<iva_Comparable_Properties>()
                .Property(e => e.Quality_And_Amenities)
                .IsUnicode(false);

            modelBuilder.Entity<iva_Comparable_Properties>()
                .Property(e => e.Ongoing_Rate)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BrokerDetails>()
                .Property(e => e.Broker_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BrokerDetails>()
                .Property(e => e.Firm_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BrokerDetails>()
                .Property(e => e.Contact_Num)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BrokerDetails>()
                .Property(e => e.Rate_Quoted_For_Land)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BrokerDetails>()
                .Property(e => e.Rate_Quoted_For_Flat)
                .IsUnicode(false);

            modelBuilder.Entity<Image_Uploaded_List>()
               .Property(e => e.Image_Path)
               .IsUnicode(false);

            modelBuilder.Entity<ival_Document_Uploaded_List>()
                .Property(e => e.Approval_Authority)
                .IsUnicode(false);

            modelBuilder.Entity<ival_Document_Uploaded_List>()
                .Property(e => e.Approval_Number)
                .IsUnicode(false);

            modelBuilder.Entity<ival_Document_Uploaded_List>()
                .Property(e => e.Uploaded_Document)
                .IsUnicode(false);
            modelBuilder.Entity<ival_ImageMaster>()
              .Property(e => e.Image_Type)
              .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectFloorWiseBreakUp>()
               .Property(e => e.Floor_Type)
               .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectFloorWiseBreakUp>()
                .Property(e => e.Approved_Carpet_Area)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectFloorWiseBreakUp>()
                .Property(e => e.Approved_Built_up_Area)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectFloorWiseBreakUp>()
                .Property(e => e.Measured_Carpet_Area)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectFloorWiseBreakUp>()
                .Property(e => e.Measured_Built_up_Area)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectFloorWiseBreakUp>()
                .Property(e => e.Adopted_Area_for_Valuation)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectFloorWiseBreakUp>()
                .Property(e => e.Adopted_Cost_Construction)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
               .Property(e => e.Builder_Company_Name)
               .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Builder_Office_Address1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Builder_Office_Address2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Builder_C_Street)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Builder_C_Locality)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Builder_C_City)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Builder_C_District)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Builder_C_State)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Builder_C_Pincode)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Builder_Nearby_LandMark)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Nature_Of_Company)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Director_Name1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Director_Phone1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Director_Mobile1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Director_EmailId1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Director_Name2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Director_Phone2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Director_Mobile2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Director_EmailId2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Director_Name3)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Director_Phone3)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Director_Mobile3)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Director_EmailId3)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Builder_Grade)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_BuilderCompanyMasterNew>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectConstructionWorkProgress>()
               .Property(e => e.Name_Of_Building)
               .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectConstructionWorkProgress>()
                .Property(e => e.Name_Of_Wing)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectConstructionWorkProgress>()
                .Property(e => e.Description_Of_Stage_Of_Construction)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectConstructionWorkProgress>()
                .Property(e => e.Percentage_Work_Completed)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectConstructionWorkProgress>()
                .Property(e => e.Percentage_Disbursement_Recommended)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
               .Property(e => e.Property_Type)
               .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.TypeOfSelect)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.TypeOfUnit)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.StatusOfProperty)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Project_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Plot_Number)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Bungalow_Number)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Unit_Number)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Survey_Number)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Street)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Locality)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Sub_Locality)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.District)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.PinCode)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.NearBy_Landmark)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Nearest_RailwayStation)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Nearest_BusStop)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Nearest_Hopital)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Nearest_Market)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Approved_No_Of_Buildings)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Approved_No_Wings)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Approved_No_Of_Floors)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Type_Of_Structure)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Approved_Usage_Of_Property)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Actual_Usage_Of_Property)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Current_Age_Of_Property)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Residual_Age_Of_Property)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Common_Areas)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Facilities)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Anyother_observation)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Roofing_Terracing)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Quality_Of_Fixture)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Quality_Construction)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Maintenance_Of_Property)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Marketability_Of_Property)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Cord_Latitude)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Cord_Longitude)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.AdjoiningRoad_Width1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.AdjoiningRoad_Width2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.AdjoiningRoad_Width3)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.AdjoiningRoad_Width4)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Type_Of_Locality)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Class_Of_Locality)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.DistFrom_City_Center)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Remarks)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Name_Of_PropertyOwner1)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Name_Of_PropertyOwner2)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Nature_Of_ProjectTransaction)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Total_No_Residential_Units)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Total_No_Shops)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Lifts)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.NoOfLifts)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Total_No_Commercial_Units)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Total_No_Of_Units_Propsed)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Plot_Demarcated_At_Site)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Type_Of_Demarcation)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Are_Boundaries_Matching)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.FourBoundaries_Remarks)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Road_Finish)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.SeismicZone)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.FloodZone)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.CycloneZone)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.CRZ)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.PropertyZone_Remarks)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Project_Registered_Under_RERA)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Registration_Num)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Num_Of_Buildings)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Num_Of_Wings)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Num_Of_Floors)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Num_Of_Units)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Expected_Completion_Date)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Type_Of_Flooring)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Wall_Finish)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Plumbing_Fitting)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Electrical_Fitting)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Quality_Of_Fixtures)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.External_Finishing_Of_Property)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Proposed_Amenities)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Engineer_Visiting_Site)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Person_Met_at_Site)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Person_Contact_Number)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Rate_Range_In_Locality)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Parking_Charges)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Club_Membership_Charges)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.One_Time_Maintenance_Charges)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Any_Other_Charges)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Government_Rate_Of_Unit)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectMasterDetails>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);


            modelBuilder.Entity<ival_ProjectRequestDetails>()
                .Property(e => e.Request_Type)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectRequestDetails>()
                .Property(e => e.Report_Request_Type)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectRequestDetails>()
                .Property(e => e.Department)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectRequestDetails>()
                .Property(e => e.Requestor_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectRequestDetails>()
                .Property(e => e.Valuation_Request_ID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectRequestDetails>()
                .Property(e => e.Customer_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectRequestDetails>()
                .Property(e => e.Customer_Application_ID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectRequestDetails>()
                .Property(e => e.Name_Of_Document_Holder)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectRequestDetails>()
                .Property(e => e.Loan_Type)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectRequestDetails>()
                .Property(e => e.Method_Of_Valuation)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectRequestDetails>()
                .Property(e => e.PropertyType)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectRequestDetails>()
                .Property(e => e.AssignedToEmpID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectRequestDetails>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectRequestDetails>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectSideMargin>()
                .Property(e => e.Side_Margin_Description)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectSideMargin>()
                .Property(e => e.As_per_Approved)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectSideMargin>()
                .Property(e => e.As_per_Measurement)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectSideMargin>()
                .Property(e => e.As_per_Approvals)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectSideMargin>()
                .Property(e => e.As_per_Local_Bye_Laws)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectSideMargin>()
                .Property(e => e.As_Actuals)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectSideMargin>()
                .Property(e => e.Deviation_No)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectSideMargin>()
                .Property(e => e.Percentage_Deviation)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectSideMargin>()
                .Property(e => e.Deviation_Description)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectSideMargin>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectSideMargin>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectUnitDetails>()
                .Property(e => e.UType_ID)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectUnitDetails>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectUnitDetails>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationAreaDetails>()
                .Property(e => e.Description_Of_Unit)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationAreaDetails>()
                .Property(e => e.Measured_Carpet_Area)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectValuationAreaDetails>()
                .Property(e => e.Area_As_Per_Plan)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectValuationPricing>()
                .Property(e => e.Name_Of_Wing)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationPricing>()
                .Property(e => e.Carpet_Base_Rate)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectValuationPricing>()
                .Property(e => e.FloorWise_Rate)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectValuationPricing>()
                .Property(e => e.PLC_Charges)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectValuationPricing>()
                .Property(e => e.FloorWise_Applicable_Floor_No)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectValuationPricing>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationPricing>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationSideMargin>()
                .Property(e => e.Name_Of_Building)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationSideMargin>()
                .Property(e => e.Name_Of_Wing)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationSideMargin>()
                .Property(e => e.Approved_Front_Side_Margin)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationSideMargin>()
                .Property(e => e.Approved_Left_Side_Margin)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationSideMargin>()
                .Property(e => e.Approved_Right_Side_Margin)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationSideMargin>()
                .Property(e => e.Approved_Rear_Side_Margin)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationSideMargin>()
                .Property(e => e.Actual_Front_Side_Margin)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationSideMargin>()
                .Property(e => e.Actual_Left_Side_Margin)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationSideMargin>()
                .Property(e => e.Actual_Right_Side_Margin)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationSideMargin>()
                .Property(e => e.Actual_Rear_Side_Margin)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationSideMargin>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationSideMargin>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_SocialDevelopmentForProject>()
                .Property(e => e.Social_Development_Type)
                .IsUnicode(false);

            modelBuilder.Entity<ival_SocialDevelopmentForProject>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_SocialDevelopmentForProject>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_Valuation_TypeMaster>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_DocumentListNew>()
                .Property(e => e.Document_Name)
                .IsUnicode(false);

            modelBuilder.Entity<ival_DocumentListNew>()
                .Property(e => e.Project_Type)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectUnitDescription>()
                .Property(e => e.Description_of_Units)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectUnitDescription>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationAmenitiesInfrastructureProgress>()
                .Property(e => e.Amenity_Description)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationAmenitiesInfrastructureProgress>()
                .Property(e => e.Description_Of_Stage_Of_Construction)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationAmenitiesInfrastructureProgress>()
                .Property(e => e.Percentage_Work_Completed)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectValuationAmenitiesInfrastructureProgress>()
                .Property(e => e.Percentage_Disbursement_Recommended)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectValuationAmenitiesInfrastructureProgress>()
                .Property(e => e.Maintenance_Of_Property)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationAmenitiesInfrastructureProgress>()
                .Property(e => e.Quality_Of_Construction)
                .IsUnicode(false);



            modelBuilder.Entity<ival_ProjectValuationConstructionWorkProgress>()
                .Property(e => e.Name_Of_Building)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationConstructionWorkProgress>()
                .Property(e => e.Name_Of_Wing)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationConstructionWorkProgress>()
                .Property(e => e.Description_Of_Stage_Of_Construction)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectValuationConstructionWorkProgress>()
                .Property(e => e.Percentage_Work_Completed)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectValuationConstructionWorkProgress>()
                .Property(e => e.Percentage_Disbursement_Recommended)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ival_ProjectDetails>()
               .Property(e => e.Description_Of_Unit)
               .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectDetails>()
                .Property(e => e.Carpet_Area)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectDetails>()
                .Property(e => e.Saleable_Area)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectDetails>()
                .Property(e => e.Total_No_Of_Units)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectDetails>()
                .Property(e => e.Gross_Saleable_Area_Of_Units)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectDetails>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_ProjectDetails>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);
            modelBuilder.Entity<ival_Project_Image_UploadedList>()
               .Property(e => e.Image_Path)
               .IsUnicode(false);

            modelBuilder.Entity<ival_Project_Image_UploadedList>()
                .Property(e => e.Created_By)
                .IsUnicode(false);

            modelBuilder.Entity<ival_Project_Image_UploadedList>()
                .Property(e => e.Updated_By)
                .IsUnicode(false);
        }
    }
}
