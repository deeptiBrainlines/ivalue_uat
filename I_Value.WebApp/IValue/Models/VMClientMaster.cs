﻿using IValuePublishProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace I_Value.WebApp.Models
{
    public class VMClientMaster
    {
        public ival_ClientMaster ival_ClientMaster { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ValuationTypeList { get; set; }

        public IEnumerable<ival_LookupCategoryValues> ReportTypeList { get; set; }

        public IEnumerable<ival_Locality> pincodeList { get; set; }
        public IEnumerable<ival_Cities> ival_CitiesList { get; set; }
        public IEnumerable<ival_States> ival_StateList { get; set; }
        public IEnumerable<ival_Districts> ival_DistrictsList { get; set; }
        public IEnumerable<ival_Locality> ival_LocalityList { get; set; }

        public int SelectedStateID { get; set; }
        public int SelectedDistID { get; set; }

        public int SelectedCityID { get; set; }
        public int SelectedLocID { get; set; }
        public int SelectedBranchPincode { get; set; }

        public int SelectedValuationTypeID { get; set; }

        public int SelectedReportTypeID { get; set; }
        [Key]
        public int Client_ID { get; set; }

        public bool? IsInstitute { get; set; }

        [StringLength(50)]
        public string Client_Name { get; set; }

        [StringLength(50)]
        public string Branch_Name { get; set; }

        [StringLength(50)]
        public string Client_First_Name { get; set; }

        [StringLength(50)]
        public string Client_Last_Name { get; set; }

        [StringLength(50)]
        public string Client_Contact_Num { get; set; }

        [StringLength(50)]
        public string Client_Contact_Email { get; set; }

        [StringLength(50)]
        public string Client_Designation { get; set; }

        [StringLength(100)]
        public string Client_Address1 { get; set; }

        [StringLength(100)]
        public string Client_Address2 { get; set; }

        [StringLength(50)]
        public string Client_City { get; set; }

        [StringLength(50)]
        public string Client_Locality { get; set; }

        [StringLength(50)]
        public string Client_District { get; set; }

        [StringLength(50)]
        public string Client_State { get; set; }

       // [Column(TypeName = "numeric")]
        public int Client_Pincode { get; set; }

        [StringLength(50)]
        public string PAN_Num { get; set; }

        [StringLength(50)]
        public string GSTIN_Num { get; set; }

        [StringLength(50)]
        public string Basic_Fees { get; set; }

        [StringLength(50)]
        public string Additional_Fees { get; set; }

        [StringLength(50)]
        public string OutofGeo_Fees { get; set; }

        [StringLength(50)]
        public string Total_Fees { get; set; }

        [StringLength(50)]
        public string Valuation_Type { get; set; }

        [StringLength(50)]
        public string Report_Type { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}