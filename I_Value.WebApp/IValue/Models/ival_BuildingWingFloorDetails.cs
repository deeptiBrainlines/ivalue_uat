namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_BuildingWingFloorDetails
    {
        [Key]
        public int BuildingFloor_ID { get; set; }

        public int? Building_ID { get; set; }

        public int? Wing_ID { get; set; }

        public int? Unit_ID { get; set; }

        [StringLength(50)]
        public string FloorType_ID { get; set; }

        [StringLength(10)]
        public string Number_Of_Floors { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}
