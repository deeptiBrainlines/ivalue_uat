namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ZNIU_ival_Report_TypeMaster
    {
        [Key]
        public int Report_ID { get; set; }

        public int? Valuation_Type_ID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }
        public int Lookup_ID { get; set; }

        [StringLength(50)]
        public string Lookup_Type { get; set; }

        [StringLength(50)]
        public string Lookup_Value { get; set; }

    }
}
