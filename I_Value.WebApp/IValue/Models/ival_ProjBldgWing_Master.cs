namespace I_Value.WebApp
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    //using System.Data.Entity.Spatial;

    public partial class ival_ProjBldgWing_Master
    {
        [Key]
        public int Wing_ID { get; set; }

        public int? Building_ID { get; set; }

        [StringLength(50)]
        public string Wing_Name { get; set; }

        [StringLength(50)]
        public string Approved_NoofUnits { get; set; }

        [StringLength(50)]
        public string Remarks { get; set; }

        [StringLength(10)]
        public string Approval_Flag { get; set; }
    }
}
