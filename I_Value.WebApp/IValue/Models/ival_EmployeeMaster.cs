namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_EmployeeMaster
    {
        [Key]
        public int Employee_ID { get; set; }

        public int? Branch_ID { get; set; }
        public int selectedCityID { get; set; }
        public int? Loc_Id { get; set; }
        public int? Role_ID { get; set; }
        public int? District_ID { get; set; }

        [StringLength(50)]
        public string District_Name { get; set; }
        public int? City_ID { get; set; }

        [StringLength(50)]
        public string City_Name { get; set; }

        [StringLength(50)]
        public string First_Name { get; set; }

        [StringLength(50)]
        public string Middle_Name { get; set; }

        [StringLength(50)]
        public string Last_Name { get; set; }

        
        public string Contact_Number { get; set; }

        [StringLength(50)]
        public string Company_Email_ID { get; set; }

        [StringLength(50)]
        public string Personal_Email_ID { get; set; }

        [StringLength(100)]
        public string Current_Address_1 { get; set; }

        [StringLength(100)]
        public string Current_Address_2 { get; set; }

        [StringLength(50)]
        public string Current_City { get; set; }

        [StringLength(50)]
        public string Current_District { get; set; }

        [StringLength(50)]
        public string Current_State { get; set; }

        //[Column(TypeName = "numeric")]
        public decimal? Current_Pincode { get; set; }

        [StringLength(100)]
        public string Perm_Address_1 { get; set; }

        [StringLength(100)]
        public string Perm_Address_2 { get; set; }

        [StringLength(50)]
        public string Perm_City { get; set; }

        [StringLength(50)]
        public string Perm_District { get; set; }

        [StringLength(50)]
        public string Perm_State { get; set; }

        
        public decimal? Perm_Pincode { get; set; }

        [StringLength(50)]
        public string Qualification { get; set; }

        [StringLength(50)]
        public string Emergency_Contact_Name { get; set; }

        [StringLength(50)]
        public string Emergency_Contact_No { get; set; }

        [StringLength(50)]
        public string Relationship_with_Employee { get; set; }

        [StringLength(50)]
        public string Blood_Group { get; set; }

        [StringLength(50)]
        public string Aadhar_No { get; set; }

        [StringLength(50)]
        public string Pan_No { get; set; }

        [StringLength(50)]
        public string Bank_Name { get; set; }

        [StringLength(100)]
        public string Bank_Address1 { get; set; }

        [StringLength(100)]
        public string Bank_Address2 { get; set; }

        [StringLength(50)]
        public string Bank_City { get; set; }

        [StringLength(50)]
        public string Bank_Locality { get; set; }

        [StringLength(50)] 
        public string Bank_State { get; set; }

        
        public String Bank_Pincode { get; set; }

        [StringLength(50)]
        public string Bank_IFSC_Code { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
        [StringLength(50)]
        public string Perm_City1 { get; set; }
        [StringLength(50)]
        public string Current_City1 { get; set; }

        [StringLength(50)]
        public string Bank_District { get; set; }
    }
}
