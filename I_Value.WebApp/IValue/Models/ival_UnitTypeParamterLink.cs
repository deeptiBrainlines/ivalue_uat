namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_UnitTypeParamterLink
    {
        [Key]
        public int UnitTypeParamLink_ID { get; set; }

        public int? UnitParameter_ID { get; set; }

        [StringLength(50)]
        public string PType { get; set; }

        [StringLength(50)]
        public string UnitType { get; set; }
    }
}
