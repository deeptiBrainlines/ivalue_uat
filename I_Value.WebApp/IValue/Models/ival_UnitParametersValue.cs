namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_UnitParametersValue
    {
        [Key]
        public int UnitParaValue_ID { get; set; }

        public int Unit_ID { get; set; }

        public int UnitParameter_ID { get; set; }

        [StringLength(50)]
        public string UnitParameter_Value { get; set; }


        //Added for jquery binding on 9/5/2020 by meenakshi
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string DeviationDescription { get; set; }

        [StringLength(50)]
        public string Latitude { get; set; }

        [StringLength(50)]
        public string Longitude { get; set; }
        
    }
}
