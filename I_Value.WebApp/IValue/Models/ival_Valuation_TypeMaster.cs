namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_Valuation_TypeMaster
    {
        [Key]
        public int Valuation_Type_ID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }
    }
}
