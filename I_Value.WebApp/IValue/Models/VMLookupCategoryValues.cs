﻿using IValuePublishProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace I_Value.WebApp.Models
{
    public class VMLookupCategoryValues
    {
        public ival_LookupCategoryValues ival_LookupCategoryValues { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValueList { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryList { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_ValuationTypeList { get; set; }

        public string SelectedLookUp_ID { get; set; }
        public int Lookup_IDedit { get; set; }
        public string CategoryType { get; set; }
        public string LookupValue { get; set; }
        public string Lookup_ID { get; set; }
        public string Lookup_Type { get; set; }
        public string Lookup_Value { get; set; }
        public string SelectedNewCategory { get; set; }
        public int SelectedValuationType { get; set; }


    }
}