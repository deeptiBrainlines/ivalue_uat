﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public class ProjectSiteEnginner
    {
        public int Site_Engineer_ID { get; set; }
        public string Site_Engineer_Name { get; set; }
    }
}