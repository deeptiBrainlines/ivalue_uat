namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_DocumentList
    {
        [Key]
        public int Document_ID { get; set; }

        [StringLength(50)]
        public string Document_Name { get; set; }
    }
}
