namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_ProjBldgWingMaster
    {
        [Key]
        public int Wing_ID { get; set; }

        public int? Building_ID { get; set; }

        [StringLength(50)]
        public string Wing_Name { get; set; }

        [StringLength(50)]
        public string Approved_No_Of_Units { get; set; }

        [StringLength(50)]
        public string Approved_Area_Of_Wing { get; set; }

        [StringLength(50)]
        public string Remarks { get; set; }

        [StringLength(10)]
        public string Approval_Flag { get; set; }

        [StringLength(50)]
        public string RERA_Registration_No { get; set; }

        [StringLength(50)]
        public string No_of_Buildings { get; set; }

        [StringLength(50)]
        public string No_Of_Wings { get; set; }

        [StringLength(50)]
        public string No_of_Floors { get; set; }

        [StringLength(50)]
        public string No_of_Units { get; set; }

        [StringLength(50)]
        public string Expected_completion_date { get; set; }

        [StringLength(50)]
        public string Deviation_Description { get; set; }

        [StringLength(50)]
        public string Cord_Latitude { get; set; }

        [StringLength(50)]
        public string Cord_Longitude { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_date { get; set; }
    }
}
