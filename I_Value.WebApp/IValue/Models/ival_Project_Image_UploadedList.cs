namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_Project_Image_UploadedList
    {
        [Key]
        public int ImageUpload_ID { get; set; }

        public int? Image_Type_ID { get; set; }

        public int? Request_ID { get; set; }
        public int? Seq_No { get; set; }

        public int? Project_ID { get; set; }

        [StringLength(50)]
        public string Image_Path { get; set; }
        public string FilePath { get; set; }
        
        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}
