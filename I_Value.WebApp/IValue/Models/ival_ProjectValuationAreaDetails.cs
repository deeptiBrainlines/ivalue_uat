namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_ProjectValuationAreaDetails
    {
        [Key]
        public int ProjectArea_ID { get; set; }

        public int? Project_ID { get; set; }
        public int? Request_ID { get; set; }
        public int? Building_ID { get; set; }
        public int? Wing_ID { get; set; }

        [StringLength(50)]
        public string Description_Of_Unit { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Measured_Carpet_Area { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Area_As_Per_Plan { get; set; }

        public int? No_Of_Units { get; set; }

        [StringLength(50)]
        public string Details_Of_Unit_Deviation { get; set; }
    }
}
