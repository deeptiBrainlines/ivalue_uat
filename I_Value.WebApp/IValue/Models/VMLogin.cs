﻿using IValuePublishProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace I_Value.WebApp.Models
{
    public class VMLogin
    {
        public ival_Login ival_Login { get; set; }
        public IEnumerable<ival_Login> ival_LoginDetails { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}