namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_ProjectMaster
    {
        [Key]
        public int Project_ID { get; set; }

        public int? Builder_Group_ID { get; set; }

        public int? Builder_Company_ID { get; set; }

        public bool? Is_Active { get; set; }

        [StringLength(50)]
        public string P_TypeID { get; set; }

        [StringLength(50)]
        public string Project_Name { get; set; }

        [StringLength(50)]
        public string Legal_Address1 { get; set; }

        [StringLength(50)]
        public string Legal_Address2 { get; set; }

        [StringLength(50)]
        public string Legal_Locality { get; set; }

        [StringLength(50)]
        public string Legal_Street { get; set; }

        [StringLength(50)]
        public string Legal_City { get; set; }

        [StringLength(50)]
        public string Legal_District { get; set; }

        [StringLength(50)]
        public string Legal_State { get; set; }

        [StringLength(50)]
        public string Legal_Pincode { get; set; }

        [StringLength(50)]
        public string Postal_Address1 { get; set; }

        [StringLength(50)]
        public string Postal_Address2 { get; set; }

        [StringLength(50)]
        public string Postal_Street { get; set; }

        [StringLength(50)]
        public string Postal_Locality { get; set; }

        [StringLength(50)]
        public string Postal_City { get; set; }

        [StringLength(50)]
        public string Postal_District { get; set; }

        [StringLength(50)]
        public string Postal_State { get; set; }

        [StringLength(50)]
        public string Postal_Pincode { get; set; }

        [StringLength(50)]
        public string Postal_NearbyLandmark { get; set; }

        [StringLength(50)]
        public string RERA_Approval_Num { get; set; }

        [StringLength(50)]
        public string Total_Land_Area { get; set; }

        [StringLength(50)]
        public string Total_Permissible_FSI { get; set; }

        [StringLength(50)]
        public string Approved_Builtup_Area { get; set; }

        [StringLength(50)]
        public string Approved_No_of_Buildings { get; set; }

        [StringLength(50)]
        public string Approved_No_of_plotsorflats { get; set; }

        [StringLength(50)]
        public string Approved_No_of_Wings { get; set; }

        [StringLength(50)]
        public string Approved_No_of_Floors { get; set; }

        [StringLength(50)]
        public string Approved_No_of_Units { get; set; }

        [StringLength(50)]
        public string Specifications { get; set; }

        [StringLength(50)]
        public string Amenities { get; set; }

        [StringLength(50)]
        public string Deviation_Description { get; set; }

        [StringLength(50)]
        public string Cord_Latitude { get; set; }

        [StringLength(50)]
        public string Cord_Longitude { get; set; }

        [StringLength(50)]
        public string AdjoiningRoad_Width1 { get; set; }

        [StringLength(50)]
        public string AdjoiningRoad_Width2 { get; set; }

        [StringLength(50)]
        public string AdjoiningRoad_Width3 { get; set; }

        [StringLength(50)]
        public string AdjoiningRoad_Width4 { get; set; }

        [StringLength(50)]
        public string Social_Infrastructure { get; set; }

        [StringLength(50)]
        public string Class_of_Locality { get; set; }

        [StringLength(50)]
        public string DistFrom_City_Center { get; set; }

        [StringLength(50)]
        public string Locality_Remarks { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}
