﻿using IValuePublishProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace I_Value.WebApp.Models
{
    public class ViewModel
    {
        public ival_ProjectMaster ival_ProjectMasters { get; set; }
        // public ival_ProjectMasterRetailProperty ival_ProjectMasterRetailProperties { get; set; }
        public ival_ProjectMaster_New ival_ProjectMasterRetailProperties { get; set; }
        public ival_ProjectBuildingMaster ival_ProjectBuildingMaster { get; set; }
        public ival_ProjBldgWingMaster ival_ProjBldgWingMaster { get; set; }
        public ival_ProjectBuildingWingFloorDetails ival_ProjectBuildingFloor { get; set; }

        public ival_ProjBldgWingUnitMaster ival_ProjBldgWingUnitMaster1 { get; set; }
        //public List<ival_LookupCategoryValues> ival_LookupCategoryValues { get; set; }
        //public List<ival_States> ival_States { get; set; }
        //public List<ival_BuilderCompanyMaster> ival_BuilderCompanyMasters { get; set; }
        //public List<ival_BuilderGroupMaster> ival_BuilderGroupMasters { get; set; }

        public int selectedLocID { get; set; }
        public int selectedSLocID { get; set; }
        public int SelectedModelGroupmaster { get; set; }
        public int SelectedModelCompanyMaster { get; set; }
        public int SelectedModelstates { get; set; }
        public int SelectedModellookupunittype { get; set; }
        public int SelectedModelpincode { get; set; }
        public int SelectedModellookupprojecttype { get; set; }
        public int SelectedModellookupclassOfLocality { get; set; }
        public int SelectedModellookupTypeOfLocality { get; set; }
        public int SelectedModelRetailStatusOfProperty { get; set; }
        public int SelectedModelRetailTypeOfStructure { get; set; }
        public int SelectedModelRetailQualityOfConstruction { get; set; }
        public int SelectedModelRetailMaintenanceOfProp { get; set; }
        public int SelectedModelRetailOccupancyDetails { get; set; }
        public int SelectedModelRetailRentingPotential { get; set; }
        public int SelectedModelstates_postal { get; set; }
        public int SelectedModelpincode_postal { get; set; }
        public int SelectedModelsocialInfra { get; set; }
        public int SelectedModelclassoflocality { get; set; }
        public int SelectedModelbasicinfra { get; set; }
        public int SelectedModelconnectivity { get; set; }
        public Boolean IsReraDetails { get; set; }
        public int SelectedApprovedusage { get; set; }
        public int SelectedActualusage { get; set; }
        public int SelectedModellookuptypeOfLocality { get; set; }
        //public IEnumerable<ival_ProjectMaster> ival_ProjectMasters { get; set; }
        public List<ival_LookupCategoryValues> ival_LookupCategoryValuesunittype { get; set; }
        public List<ival_LookupCategoryValues> ival_LookupCategoryValuesTypeOfUnit { get; set; }
        public List<ival_LookupCategoryValues> ival_LookupCategoryValuesTypeOfStrycture { get; set; }
        public List<ival_LookupCategoryValues> ival_LookupCategoryValuesFloorType { get; set; }
        public List<ival_LookupCategoryValues> ival_LookupCategoryValuesSocialDev { get; set; }
        public List<ival_LookupCategoryValues> ival_LookupCategoryValuesBasicInfra { get; set; }




        public IEnumerable<ival_States> ival_States { get; set; }
        public IEnumerable<ival_BuilderCompanyMaster> ival_BuilderCompanyMasters { get; set; }
        public IEnumerable<ival_BuilderGroupMaster> ival_BuilderGroupMasters { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuespincode { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesProjecttype { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesQualityOfConstruction { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesMaintenanceOfproperty { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesOccupancyDetails { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesrentingPotential { get; set; }
        //added by punam
        public List<ival_ProjectUnitDetailsWithCategory> ival_ProjectUnitDetails { get; set; }

        public IEnumerable<ival_States> ival_States_postal { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuespincode_postal { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesSocialInfra { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesClassOfLocality { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesTypeOfLocality { get; set; }

        public List<ival_LookupCategoryValues> ival_LookupCategoryValuesbasicInfra { get; set; }

        public List<ival_LookupCategoryValues> ival_CommonAreaI { get; set; }
        public List<ival_LookupCategoryValues> ival_CommonAreaI2 { get; set; }
        public List<ival_LookupCategoryValues> ival_CommonAreaI3 { get; set; }
        public List<ival_LookupCategoryValues> ival_LookupCategoryValuesconnectivity { get; set; }

        //changes by punam
        public List<ival_ProjectLocalTransaportEdit> ival_LookupCategoryValuesTransaportEdit { get; set; }
        public List<iva_ProjectwithBasicInfraEdit> ival_LookupCategoryValuesBasicInfraEdit { get; set; }
        public List<iva_ProjectwithBasicInfraEdit> ival_commonarea { get; set; }
        public List<iva_ProjectwithBasicInfraEdit> ival_commonarea2 { get; set; }
        public List<iva_ProjectwithBasicInfraEdit> ival_commonarea3 { get; set; }
        public List<ival_ProjectFloorTypesWithCategory> ival_LookupCategoryFloorTypeEdit { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesRetailStatusOfProperty { get; set; }

        public IEnumerable<ival_ProjectApprovalDetails> ival_ProjectApprovalDetails { get; set; }

        public List<ival_ProjectBoundaries> ival_ProjectBoundaries { get; set; }
        public IEnumerable<ival_ProjectSideMargin> ival_ProjectSideMargin { get; set; }


        public List<ival_LookupCategoryValues> ival_ApprovalType { get; set; }
        public List<ival_ProjectApprovalDetails> ival_ApprovalTypeList { get; set; }

        public List<ival_ProjectApprovalDetails> ival_ProjectApprovallists { get; set; }
        public List<ival_ProjectUnitDetails> ival_ProjectUnitDetails_chk { get; set; }
        public bool Checked { get; set; }
        public bool selectedUnitType { get; set; }

        public int SelectedModeltextboxapproval { get; set; }

        public List<ival_LookupCategoryValues> ival_LookupCategorySideMargins { get; set; }

        public List<ival_ProjectSideMargin> ival_ProjectSideMarginList { get; set; }
        public List<ival_LookupCategoryValues> ival_LookupCategoryBoundaries { get; set; }
        public List<ival_ProjectBoundaries> ival_ProjectBoundariesList { get; set; }
        public List<ival_BuildingWingFloorDetails> ival_BuildingWingFloorDetailsList { get; set; }
        // public List<ival_ProjectBuildingWingFloorDetails> ival_BuildingWingFloorDetailsListforpr { get; set; }



        List<string> GetAllSelctedCheckBoxes { get; set; }

        public string Approved_Area_Of_Wing { get; set; }

        public string SelectedApprovalFlag { get; set; }

        public IEnumerable<SelectListItem> GetApprovalFlagList { get; set; }
        public string Deviation_Description { get; set; }

        public string Cord_Latitude { get; set; }

        public string Cord_Longitude { get; set; }
        public string Message { get; set; }
        public IEnumerable<ival_ProjectMaster> ival_ProjectMastersList { get; set; }

        public int selectedProjectID { get; set; }

        public int selectedBuilderGroupID { get; set; }

        public int selectedBuilderCompanyID { get; set; }
        public int selectedBuildingID { get; set; }
        public int selectedWingID { get; set; }
        public int selectedUnitID { get; set; }
        public int Unit_ID { get; set; }
        public String Wing_Name { get; set; }
        public String No_of_Units { get; set; }
        public int selectedStateID { get; set; }
        public int selectedDistID { get; set; }
        public int selectedCityID { get; set; }
        public int selectedpinID { get; set; }

        public string Deviation_Percentage { get; set; }

        public String ProjectName { get; set; }
        public String BuildingName { get; set; }
        public String WingName { get; set; }
        public Int32 ProjectID { get; set; }
        public Int32 BuildingID { get; set; }
        public Int32 WingID { get; set; }
        public Int32 SelectedViewFromUnit { get; set; }
        public Int32 SelectedPropertyType { get; set; }
        public List<VMUnitParameterDetails> vMUnitTypesList { get; set; }
        public List<ival_LookupCategoryValues> propertyTypeList { get; set; }

        public List<ival_LookupCategoryValues> unitTypeList { get; set; }
        public List<ival_UnitParameterMaster> ival_UnitParameterMastersList { get; set; }
        public List<ival_LookupCategoryValues> viewFromUnitList { get; set; }

        public List<VMUnitParameterDetails> vMUnitParameterDetailslist { get; set; }
        public List<VMUnitParameterPriceDetails> vMUnitParameterPriceDetailsList { get; set; }
        public string UnitCost { get; set; }
        public string Government_Value { get; set; }
        public string Government_Rate { get; set; }
        public string Current_Value { get; set; }
        public string Percent_Completed { get; set; }
        public string Valuation_of_unit { get; set; }
        public string Stage_Description { get; set; }
        public string Stage_of_Construction { get; set; }
        public string Total_Cost_of_Property { get; set; }
        public string Government_ValueSq { get; internal set; }
        public string Government_RateSq { get; internal set; }

        public List<ival_BasicInfraAvailability> ival_BasicInfraAvailability { get; set; }
        public List<ival_ProjectUnitDetails> ival_ProjectUnitDetail { get; set; }
        //public IEnumerable<ival_ProjectUnitDetails> ival_ProjectUnitDetail { get; set; }
        public List<ival_LocalTransportForProject> ival_LocalTransportForProject { get; set; }
        public List<ival_SocialDevelopmentForProject> ival_SocialDevelopmentForProject { get; set; }
        public List<ival_ProjectBuildingWingFloorDetails> ival_ProjectBuildingWingFloorDetails { get; set; }

        //For cascading ddl on edit by meenakshi
        public IEnumerable<ival_States> ival_StatesList { get; set; }

        public IEnumerable<ival_Districts> ival_DistrictsList { get; set; }
        public IEnumerable<ival_LookupCategoryValues> PincodeList { get; set; }
        public IEnumerable<ival_Cities> ival_CitiesList { get; set; }
        public IEnumerable<ival_Cities> ival_pincodelist { get; set; }
        public IEnumerable<ival_Cities> ival_LocalityList { get; set; }
        public IEnumerable<ival_Cities> ival_subLocalityList { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuestypeofownership { get; set; }
        public int? Selectedtypeofownership { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesNatureofproject { get; set; }
        public int? SelectedNatureofproject { get; set; }
        public List<ival_LookupCategoryValues> ival_LookupCategoryLiftTypes { get; set; }
        public int? SelectedModelLiftno { get; set; }
        public List<ival_LookupCategoryValues> ival_LookupCategoryextfinish { get; set; }
        public int? Selectedextfinish { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesdemarcation { get; set; }
        public int? Selectedtypeofdemarcations { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesRoadFinish { get; set; }
        public int? SelectedRoadFinish { get; set; }
        public List<ival_ProjectApprovalDetails> ival_ProjectApprovalDetailsnew { get; set; }
        public List<ival_ProjectUnitDescription> ival_LookupCategoryValuesProjectUnitDescription { get; set; }
        public List<ival_DocumentListNew> ival_DocumentListNew { get; set; }
        public List<ival_DocumentListNew> ival_DocumentListNewBuilder { get; set; }
        public List<ival_Document_Uploaded_List> ival_Document_Uploaded_ListS { get; set; }
        public List<ival_ProjectDetails> ival_ProjectDetails { get; set; }
        public IEnumerable<VMProjectIndex> VMProjectIndexs { get; set; }
        [DataType(DataType.Date)]
        [Required]
        public DateTime? DOR { get; set; }
    }

}