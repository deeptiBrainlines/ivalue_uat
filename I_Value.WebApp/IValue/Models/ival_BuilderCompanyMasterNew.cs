namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_BuilderCompanyMasterNew
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Builder_Company_Master_ID { get; set; }

        public int? Builder_Group_Master_ID { get; set; }

        [StringLength(100)]
        public string Builder_Company_Name { get; set; }

        [StringLength(50)]
        public string Builder_Office_Address1 { get; set; }

        [StringLength(50)]
        public string Builder_Office_Address2 { get; set; }

        [StringLength(50)]
        public string Builder_C_Street { get; set; }

        [StringLength(50)]
        public string Builder_C_Locality { get; set; }

        [StringLength(50)]
        public string Builder_C_City { get; set; }

        [StringLength(50)]
        public string Builder_C_District { get; set; }

        [StringLength(50)]
        public string Builder_C_State { get; set; }

        [StringLength(50)]
        public string Builder_C_Pincode { get; set; }

        [StringLength(50)]
        public string Builder_Nearby_LandMark { get; set; }

        [StringLength(50)]
        public string Nature_Of_Company { get; set; }

        [StringLength(50)]
        public string Director_Name1 { get; set; }

        [StringLength(50)]
        public string Director_Phone1 { get; set; }

        [StringLength(50)]
        public string Director_Mobile1 { get; set; }

        [StringLength(50)]
        public string Director_EmailId1 { get; set; }

        [StringLength(50)]
        public string Director_Name2 { get; set; }

        [StringLength(50)]
        public string Director_Phone2 { get; set; }

        [StringLength(50)]
        public string Director_Mobile2 { get; set; }

        [StringLength(50)]
        public string Director_EmailId2 { get; set; }

        [StringLength(50)]
        public string Director_Name3 { get; set; }

        [StringLength(50)]
        public string Director_Phone3 { get; set; }

        [StringLength(50)]
        public string Director_Mobile3 { get; set; }

        [StringLength(50)]
        public string Director_EmailId3 { get; set; }

        [StringLength(50)]
        public string Builder_Grade { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}
