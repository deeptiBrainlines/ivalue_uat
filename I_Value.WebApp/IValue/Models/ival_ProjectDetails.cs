namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_ProjectDetails
    {
        [Key]
        public int ProjectDetail_ID { get; set; }

        public int? Project_ID { get; set; }

        public int? Building_ID { get; set; }

        public int? Wing_ID { get; set; }

        [StringLength(100)]
        public string Description_Of_Unit { get; set; }

        [StringLength(100)]
        public string Carpet_Area { get; set; }

        [StringLength(100)]
        public string Saleable_Area { get; set; }

        [StringLength(100)]
        public string Total_No_Of_Units { get; set; }

        [StringLength(100)]
        public string Gross_Saleable_Area_Of_Units { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}
