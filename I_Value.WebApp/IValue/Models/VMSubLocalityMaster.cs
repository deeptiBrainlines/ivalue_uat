﻿using IValuePublishProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace I_Value.WebApp.Models
{
    public class VMSubLocalityMaster : ival_SubLocalityMaster
    {
        public IEnumerable<ival_SubLocalityMaster1> ival_SubLocalityMaster1 { get; set; }
        public IEnumerable<ival_SubLocalityMaster> ival_SubLocalityMaster { get; set; }
        public IEnumerable<ival_Locality> ival_LocalityList { get; set; }
        public IEnumerable<ival_Locality> ival_LocPincodeList { get; set; }
           public IEnumerable<ival_Districts> ival_DistrictsList { get; set; }

        public IEnumerable<ival_Districts> ival_CityList { get; set; }

        public int Loc_Id { get; set; }

            [StringLength(100)]
            public string Loc_Name { get; set; }

            public int? Pin_Code { get; set; }

            [StringLength(250)]
            public string Sub_Locality { get; set; }

        public int District_ID { get; set; }

        [StringLength(250)]
        public string District_Name { get; set; }


    }
}