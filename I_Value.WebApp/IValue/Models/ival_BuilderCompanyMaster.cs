namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_BuilderCompanyMaster
    {
        [Key]
        public int Builder_Company_Master_ID { get; set; }

        public int? Builder_Group_Master_ID { get; set; }

        [StringLength(50)]
        public string Builder_Company_Name { get; set; }

        [StringLength(100)]
        public string Builder_Legal_Address1 { get; set; }

        [StringLength(100)]
        public string Builder_Legal_Address2 { get; set; }

        [StringLength(50)]
        public string Builder_C_City { get; set; }

        [StringLength(50)]
        public string Builder_C_District { get; set; }

        [StringLength(50)]
        public string Builder_C_State { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Builder_C_Pincode { get; set; }

        [StringLength(100)]
        public string Builder_Postal_Address1 { get; set; }

        [StringLength(100)]
        public string Builder_Postal_Address2 { get; set; }

        [StringLength(50)]
        public string Builder_Postal_City { get; set; }

        [StringLength(50)]
        public string Builder_Postal_District { get; set; }

        [StringLength(50)]
        public string Builder_Postal_State { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Builder_Postal_Pincode { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}
