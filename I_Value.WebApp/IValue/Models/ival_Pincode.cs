﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace I_Value.WebApp.Models
{
    public partial class ival_Pincode
    {
       
        public Int32 Pincode_ID { get; set; }

        public Int32 Pincode_Number { get; set; }
    }
}