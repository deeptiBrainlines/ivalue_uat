namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_DocumentListNew
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Document_ID { get; set; }

        [StringLength(50)]
        public string Document_Name { get; set; }

        [StringLength(50)]
        public string Project_Type { get; set; }

        [StringLength(50)]
        public string SrNo { get; set; }
    }
}
