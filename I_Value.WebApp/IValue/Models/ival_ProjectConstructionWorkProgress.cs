namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_ProjectConstructionWorkProgress
    {
        [Key]
        public int ProjectProgress_ID { get; set; }

        public int? Project_ID { get; set; }

        [StringLength(50)]
        public string Name_Of_Building { get; set; }

        [StringLength(50)]
        public string Name_Of_Wing { get; set; }

        [StringLength(50)]
        public string Description_Of_Stage_Of_Construction { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Percentage_Work_Completed { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Percentage_Disbursement_Recommended { get; set; }
    }
}
