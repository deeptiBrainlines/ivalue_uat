namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_States
    {
        [Key]
        public int State_ID { get; set; }

        [StringLength(10)]
        public string State_Code { get; set; }

        [StringLength(250)]
        public string State_Name { get; set; }
    }
}
