namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_ProjectMasterDetails
    {
        [Key]
        public int Project_ID { get; set; }

        public int? Builder_Group_ID { get; set; }

        public int? Builder_Company_ID { get; set; }

        [StringLength(50)]
        public string Property_Type { get; set; }

        [StringLength(50)]
        public string TypeOfSelect { get; set; }

        [StringLength(50)]
        public string TypeOfUnit { get; set; }

        [StringLength(50)]
        public string StatusOfProperty { get; set; }

        [StringLength(100)]
        public string Project_Name { get; set; }

        [StringLength(50)]
        public string Plot_Number { get; set; }

        [StringLength(50)]
        public string Bungalow_Number { get; set; }

        [StringLength(50)]
        public string Unit_Number { get; set; }

        [StringLength(50)]
        public string Survey_Number { get; set; }

        [StringLength(50)]
        public string Street { get; set; }

        [StringLength(50)]
        public string Locality { get; set; }

        [StringLength(50)]
        public string Sub_Locality { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string District { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(50)]
        public string PinCode { get; set; }

        [StringLength(50)]
        public string NearBy_Landmark { get; set; }

        [StringLength(50)]
        public string Nearest_RailwayStation { get; set; }

        [StringLength(50)]
        public string Nearest_BusStop { get; set; }

        [StringLength(50)]
        public string Nearest_Hopital { get; set; }

        [StringLength(50)]
        public string Nearest_Market { get; set; }

        [StringLength(50)]
        public string Approved_No_Of_Buildings { get; set; }

        [StringLength(50)]
        public string Approved_No_Wings { get; set; }

        [StringLength(50)]
        public string Approved_No_Of_Floors { get; set; }

        [StringLength(50)]
        public string Type_Of_Structure { get; set; }

        [StringLength(50)]
        public string Approved_Usage_Of_Property { get; set; }

        [StringLength(50)]
        public string Actual_Usage_Of_Property { get; set; }

        [StringLength(50)]
        public string Current_Age_Of_Property { get; set; }

        [StringLength(50)]
        public string Residual_Age_Of_Property { get; set; }

        [StringLength(50)]
        public string Common_Areas { get; set; }

        [StringLength(50)]
        public string Facilities { get; set; }

        [StringLength(50)]
        public string Anyother_observation { get; set; }

        [StringLength(50)]
        public string Roofing_Terracing { get; set; }

        [StringLength(50)]
        public string Quality_Of_Fixture { get; set; }

        [StringLength(50)]
        public string Quality_Construction { get; set; }

        [StringLength(50)]
        public string Maintenance_Of_Property { get; set; }

        [StringLength(50)]
        public string Marketability_Of_Property { get; set; }

        [StringLength(50)]
        public string Cord_Latitude { get; set; }

        [StringLength(50)]
        public string Cord_Longitude { get; set; }

        [StringLength(50)]
        public string AdjoiningRoad_Width1 { get; set; }

        [StringLength(50)]
        public string AdjoiningRoad_Width2 { get; set; }

        [StringLength(50)]
        public string AdjoiningRoad_Width3 { get; set; }

        [StringLength(50)]
        public string AdjoiningRoad_Width4 { get; set; }

        [StringLength(50)]
        public string Type_Of_Locality { get; set; }

        [StringLength(50)]
        public string Class_Of_Locality { get; set; }

        [StringLength(50)]
        public string DistFrom_City_Center { get; set; }

        [StringLength(50)]
        public string Remarks { get; set; }

        [StringLength(50)]
        public string Name_Of_PropertyOwner1 { get; set; }

        [StringLength(50)]
        public string Name_Of_PropertyOwner2 { get; set; }

        [StringLength(50)]
        public string Nature_Of_ProjectTransaction { get; set; }

        [StringLength(50)]
        public string Total_No_Residential_Units { get; set; }

        [StringLength(50)]
        public string Total_No_Shops { get; set; }

        [StringLength(50)]
        public string Lifts { get; set; }

        [StringLength(50)]
        public string NoOfLifts { get; set; }

        [StringLength(50)]
        public string Total_No_Commercial_Units { get; set; }

        [StringLength(50)]
        public string Total_No_Of_Units_Propsed { get; set; }

        [StringLength(50)]
        public string Plot_Demarcated_At_Site { get; set; }

        [StringLength(50)]
        public string Type_Of_Demarcation { get; set; }

        [StringLength(5)]
        public string Are_Boundaries_Matching { get; set; }

        [StringLength(50)]
        public string FourBoundaries_Remarks { get; set; }

        [StringLength(50)]
        public string Road_Finish { get; set; }

        [StringLength(50)]
        public string SeismicZone { get; set; }

        [StringLength(50)]
        public string FloodZone { get; set; }

        [StringLength(50)]
        public string CycloneZone { get; set; }

        [StringLength(50)]
        public string CRZ { get; set; }

        [StringLength(50)]
        public string PropertyZone_Remarks { get; set; }

        [StringLength(50)]
        public string Project_Registered_Under_RERA { get; set; }

        [StringLength(50)]
        public string Registration_Num { get; set; }

        [StringLength(50)]
        public string Num_Of_Buildings { get; set; }

        [StringLength(50)]
        public string Num_Of_Wings { get; set; }

        [StringLength(50)]
        public string Num_Of_Floors { get; set; }

        [StringLength(50)]
        public string Num_Of_Units { get; set; }

        [StringLength(50)]
        public string Expected_Completion_Date { get; set; }

        [StringLength(50)]
        public string Type_Of_Flooring { get; set; }

        [StringLength(50)]
        public string Wall_Finish { get; set; }

        [StringLength(50)]
        public string Plumbing_Fitting { get; set; }

        [StringLength(50)]
        public string Electrical_Fitting { get; set; }

        [StringLength(50)]
        public string Quality_Of_Fixtures { get; set; }

        [StringLength(50)]
        public string External_Finishing_Of_Property { get; set; }

        [StringLength(50)]
        public string Proposed_Amenities { get; set; }

        public DateTime? Date_Of_Inspection { get; set; }

        [StringLength(50)]
        public string Engineer_Visiting_Site { get; set; }

        [StringLength(50)]
        public string Person_Met_at_Site { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Person_Contact_Number { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Rate_Range_In_Locality { get; set; }

        public bool? GST_Applicability { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Parking_Charges { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Club_Membership_Charges { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? One_Time_Maintenance_Charges { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Any_Other_Charges { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Government_Rate_Of_Unit { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}
