namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class iva_Comparable_Properties
    {
        [Key]
        public int Property_ID { get; set; }
        public int Property_IDs { get; set; }
        public int? Project_ID { get; set; }

        public int? Unit_ID { get; set; }

        [StringLength(50)]
        public string Project_Name { get; set; }

        [StringLength(50)]
        public string Dist_Subject_Property { get; set; }

        [StringLength(50)]
        public string Comparable_Unit_Details { get; set; }

        [StringLength(50)]
        public string Quality_And_Amenities { get; set; }

        [StringLength(50)]
        public string Ongoing_Rate { get; set; }
    }
}
