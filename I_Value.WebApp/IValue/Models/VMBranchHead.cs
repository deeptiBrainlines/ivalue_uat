﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public class VMBranchHead
    {
        public int Employee_ID { get; set; }
        public string First_Name { get; set; }
        public int Role_ID { get; set; }
        public string Role_Name { get; set; }

    }
}