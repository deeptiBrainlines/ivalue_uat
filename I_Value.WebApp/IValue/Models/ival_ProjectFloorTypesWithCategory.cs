﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public partial class ival_ProjectFloorTypesWithCategory
    {
        public int Lookup_ID { get; set; }

        [StringLength(50)]
        public string Lookup_Type { get; set; }

        [StringLength(50)]
        public string Lookup_Value { get; set; }

        public int BuildingFloor_ID { get; set; }

        public int? Project_ID { get; set; }
        [StringLength(50)]
        public string FloorType_ID { get; set; }

        [StringLength(10)]
        public string Number_Of_Floors { get; set; }


    }
}