﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public class UploadedImageAndImageMaster
    {
        public int ImageUpload_ID { get; set; }

        public int? Image_Type_ID { get; set; }
        [StringLength(50)]
        public string Image_Type { get; set; }

        [StringLength(50)]
        public string FilePath { get; set; }

        public int? Seq_No { get; set; }

        public int? Request_ID { get; set; }

        public int? Project_ID { get; set; }

        [StringLength(50)]
        public string Image_Path { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
        public List<ival_Project_Image_UploadedList> uploadImageList { get; set; }
        public IEnumerable<ival_Project_Image_UploadedList> UploadImageListNew { get; set; }
        //new fields added for UnitDetailsImage
        public int Image_ID { get; set; }
    }
}