namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_ProjectValuationPricing
    {
        [Key]
        public int ProjectPricing_ID { get; set; }

        public int? Project_ID { get; set; }

        public int? Building_ID { get; set; }

        public int? Wing_ID { get; set; }

        public int? Request_ID { get; set; }

        [StringLength(50)]
        public string Name_Of_Wing { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Carpet_Base_Rate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? FloorWise_Rate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PLC_Charges { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? FloorWise_Applicable_Floor_No { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}
