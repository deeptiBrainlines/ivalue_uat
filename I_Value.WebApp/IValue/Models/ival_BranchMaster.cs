namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_BranchMaster
    {
        [Key]
        public int Branch_ID { get; set; }

        [StringLength(100)]
        public string Branch_Code { get; set; }

        [StringLength(100)]
        public string Branch_Name { get; set; }

        [StringLength(100)]
        public string SelectedBranchStateID { get; set; }
        [StringLength(50)]
        public string Branch_Address1 { get; set; }
        

        [StringLength(50)]
        public string Branch_Address2 { get; set; }

        [StringLength(50)]
        public string Branch_Loc { get; set; }
        [StringLength(50)]
        public string Branch_City { get; set; }

        [StringLength(50)]
        public string Branch_District { get; set; }

        [StringLength(50)]
        public string Branch_State { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Branch_Pincode { get; set; }

        public bool? Is_Leased { get; set; }

        [StringLength(50)]
        public string Lessor_First_Name { get; set; }

        [StringLength(50)]
        public string Lessor_Middle_Name { get; set; }

        [StringLength(50)]
        public string Lessor_Last_Name { get; set; }

        [StringLength(50)]
        public string Lessor_Address1 { get; set; }

        [StringLength(50)]
        public string Lessor_Address2 { get; set; }

        [StringLength(50)]
        public string Lessor_City { get; set; }

        [StringLength(50)]
        public string Lessor_Loc { get; set; }

        [StringLength(50)]
        public string Lessor_District { get; set; }

        [StringLength(50)]
        public string Lessor_State { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Lessor_Pincode { get; set; }

        [StringLength(50)]
        public string Lessor_Contact_Num { get; set; }

        [StringLength(50)]
        public string Lessor_Email_ID { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? Lease_Start_Date { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? Lease_End_Date { get; set; }

        [StringLength(50)]
        public string Monthly_Rent { get; set; }

        [StringLength(50)]
        public string Maintaince_Charges { get; set; }

        [StringLength(50)]
        public string Other_Charges { get; set; }

        [StringLength(50)]
        public string LandLord_Bank { get; set; }

        [StringLength(50)]
        public string LandLord_Branch { get; set; }

        [StringLength(50)]
        public string LandLord_Address1 { get; set; }

        [StringLength(50)]
        public string LandLord_Address2 { get; set; }

        [StringLength(50)]
        public string LandLord_City { get; set; }

        [StringLength(50)]
        public string LandLord_Loc { get; set; }

        [StringLength(50)]
        public string LandLord_District { get; set; }

        [StringLength(50)]
        public string LandLord_State { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? LandLord_Pincode { get; set; }

        [StringLength(50)]
        public string Account_Type { get; set; }

        [StringLength(50)]
        public string Account_Number { get; set; }

        [StringLength(100)]
        public string IFSC_Num { get; set; }

        [StringLength(50)]
        public string PAN_Num { get; set; }

        [StringLength(50)]
        public string GSTIN_Num { get; set; }

        [StringLength(10)]
        public string Is_Active { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Modified_By { get; set; }

        public DateTime? Modified_Date { get; set; }
    }
}
