﻿using IValuePublishProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace I_Value.WebApp.Models
{
    public class VMReport_TypeMaster
    {

        public IEnumerable<ZNIU_ival_Report_TypeMaster> ival_ValuationReportList { get; set; }
    }
}