namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_ProjectMaster_New
    {
        [Key]
        public int Project_ID { get; set; }

        public int? RequestID { get; set; }

        public int? Builder_Group_ID { get; set; }

        public int? Builder_Company_ID { get; set; }

        public bool? Is_Active { get; set; }

        public bool? Is_Retail_Individual { get; set; }

        [StringLength(50)]
        public string Property_Type { get; set; }

        [StringLength(50)]
        public string TypeOfSelect { get; set; }

        
        [StringLength(50)]
        public string TypeOfUnit { get; set; }

        [StringLength(50)]
        public string StatusOfProperty { get; set; }

        [StringLength(100)]
        public string PlotBunglowNumber { get; set; }

        [StringLength(100)]
        public string BunglowNumber { get; set; }

        [StringLength(100)]
        public string UnitNumber { get; set; }

        [StringLength(50)]
        public string Project_Name { get; set; }

        [StringLength(50)]
        public string Building_Name_RI { get; set; }

        [StringLength(50)]
        public string Wing_Name_RI { get; set; }

        [StringLength(50)]
        public string Survey_Number { get; set; }

        [StringLength(50)]
        public string Plot_Number { get; set; }
        [StringLength(50)]
        public string Ward_Number { get; set; }


        [StringLength(50)]
        public string Municipal_Number { get; set; }



        [StringLength(50)]
        public string Village_Name { get; set; }

        [StringLength(50)]
        public string Street { get; set; }

        [StringLength(50)]
        public string Locality { get; set; }

        [StringLength(50)]
        public string Sub_Locality { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string District { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(50)]
        public string PinCode { get; set; }

        [StringLength(50)]
        public string NearBy_Landmark { get; set; }

        [StringLength(50)]
        public string Nearest_RailwayStation { get; set; }

        [StringLength(50)]
        public string Nearest_BusStop { get; set; }

        [StringLength(50)]
        public string Nearest_Hospital { get; set; }

        [StringLength(50)]
        public string Nearest_Market { get; set; }

        [StringLength(50)]
        public string Legal_Address1 { get; set; }

        [StringLength(50)]
        public string Legal_Address2 { get; set; }

        [StringLength(50)]
        public string Legal_Locality { get; set; }

        [StringLength(50)]
        public string Legal_Street { get; set; }

        [StringLength(50)]
        public string Legal_City { get; set; }

        [StringLength(50)]
        public string Legal_District { get; set; }

        [StringLength(50)]
        public string Legal_State { get; set; }

        [StringLength(50)]
        public string Legal_Pincode { get; set; }

        [StringLength(50)]
        public string Postal_Address1 { get; set; }

        [StringLength(50)]
        public string Postal_Address2 { get; set; }

        [StringLength(50)]
        public string Postal_Street { get; set; }

        [StringLength(50)]
        public string Postal_Locality { get; set; }

        [StringLength(50)]
        public string Postal_City { get; set; }

        [StringLength(50)]
        public string Postal_District { get; set; }

        [StringLength(50)]
        public string Postal_State { get; set; }

        [StringLength(50)]
        public string Postal_Pincode { get; set; }

        [StringLength(50)]
        public string Postal_NearbyLandmark { get; set; }

        [StringLength(50)]
        public string RERA_Approval_Num { get; set; }

        [StringLength(10)]
        public string Approval_Flag { get; set; }

        [StringLength(50)]
        public string Total_Land_Area { get; set; }

        [StringLength(50)]
        public string Total_Permissible_FSI { get; set; }

        [StringLength(50)]
        public string Approved_Builtup_Area { get; set; }

        [StringLength(50)]
        public string Approved_No_of_Buildings { get; set; }

        [StringLength(50)]
        public string Approved_No_of_plotsorflats { get; set; }

        [StringLength(50)]
        public string Approved_No_of_Wings { get; set; }

        [StringLength(50)]
        public string Approved_No_of_Floors { get; set; }

        [StringLength(50)]
        public string Approved_No_of_Units { get; set; }

        [StringLength(50)]
        public string Actual_No_Floors_G { get; set; }

        [StringLength(50)]
        public string Floor_Of_Unit { get; set; }


        [StringLength(50)]
        public string Floor_Of_UnitNo { get; set; }

        [StringLength(100)]
        public string Property_Description { get; set; }

        [StringLength(50)]
        public string Type_Of_Structure { get; set; }

        [StringLength(50)]
        public string Approved_Usage_Of_Property { get; set; }

        [StringLength(50)]
        public string Actual_Usage_Of_Property { get; set; }

        [StringLength(50)]
        public string Current_Age_Of_Property { get; set; }

        [StringLength(50)]
        public string Residual_Age_Of_Property { get; set; }

        [StringLength(50)]
        public string Common_Areas { get; set; }

        [StringLength(50)]
        public string Facilities { get; set; }

        [StringLength(50)]
        public string Anyother_Observation { get; set; }

        [StringLength(50)]
        public string Roofing_Terracing { get; set; }

        [StringLength(50)]
        public string RERA_Registration_No { get; set; }

        
        [StringLength(50)]
        public string Quality_Of_Fixture { get; set; }

        [StringLength(50)]
        public string Quality_Of_Construction { get; set; }

        [StringLength(50)]
        public string Maintenance_Of_Property { get; set; }

        [StringLength(50)]
        public string Marketability_Of_Property { get; set; }

        [StringLength(50)]
        public string Renting_Potential { get; set; }

        [StringLength(50)]
        public string Expected_Monthly_Rental { get; set; }

        [StringLength(50)]
        public string Name_Of_Occupant { get; set; }

        [StringLength(50)]
        public string Occupancy_Details { get; set; }

        
        [StringLength(50)]
        public string Occupied_Since { get; set; }

        [StringLength(50)]
        public string Monthly_Rental { get; set; }

        [StringLength(50)]
        public string Balanced_Leased_Period { get; set; }

        [StringLength(50)]
        public string Specifications { get; set; }

        [StringLength(50)]
        public string Amenities { get; set; }

        [StringLength(50)]
        public string Deviation_Description { get; set; }

        [StringLength(50)]
        public string Cord_Latitude { get; set; }

        [StringLength(50)]
        public string Cord_Longitude { get; set; }

        [StringLength(50)]
        public string AdjoiningRoad_Width1 { get; set; }

        [StringLength(50)]
        public string AdjoiningRoad_Width2 { get; set; }

        [StringLength(50)]
        public string AdjoiningRoad_Width3 { get; set; }

        [StringLength(50)]
        public string AdjoiningRoad_Width4 { get; set; }

        [StringLength(50)]
        public string Social_Development { get; set; }

        [StringLength(50)]
        public string Social_Infrastructure { get; set; }

        [StringLength(50)]
        public string Class_of_Locality { get; set; }

        [StringLength(50)]
        public string Type_of_Locality { get; set; }

        [StringLength(50)]
        public string DistFrom_City_Center { get; set; }

        [StringLength(50)]
        public string Locality_Remarks { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        [StringLength(50)]
        public string PlotNumber { get; set; }

        [StringLength(50)]
        public string Distance_from_Airport { get; set; }

        [StringLength(50)]
        public string txtdistanceland { get; set; }

        [StringLength(50)]
        public string txtdistancerailway { get; set; }

        [StringLength(50)]
        public string txtdistancebus { get; set; }

        [StringLength(50)]
        public string txtdistancehospital { get; set; }

        [StringLength(50)]
        public string txtdistancemarket { get; set; }

        [StringLength(50)]
        public string txtdistanceairport { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}
