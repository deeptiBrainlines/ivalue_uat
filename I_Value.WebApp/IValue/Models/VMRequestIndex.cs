﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace I_Value.WebApp.Models
{
    public partial class VMRequestIndex
    {
        [Key]
        public int Request_ID { get; set; }

        public int Project_ID { get; set; }

        [StringLength(250)]
        public string Request_Flag { get; set; }


        [StringLength(250)]
        public string Client_Name { get; set; }


        [StringLength(250)]
        public string Property_Type { get; set; }

        [StringLength(250)]
        public string Requesttype { get; set; }

        [StringLength(250)]
        public string Reporttype { get; set; }

        [StringLength(250)]
        public string Customer_Name { get; set; }

        [StringLength(250)]
        public string Project_Name { get; set; }

        [StringLength(250)]
        public string Legal_City { get; set; }

        [StringLength(250)]
        public string Legal_State { get; set; }

        [StringLength(250)]
        public string Status { get; set; }

        [StringLength(250)]
        public string Typeofselect { get; set; }

        [StringLength(250)]
        public string Typeofunit { get; set; }

        [StringLength(250)]
        public string Request_Date { get; set; }

        [StringLength(250)]
        public int D { get; set; }
        public int M { get; set; }
        public int Y { get; set; }

        //public List<VMRequestIndex> VMRequestIndexs { get; set; }
        //public Pager pagers { get; set; }

    }
    public class PersonViewModel
    {
        public List<VMRequestIndex> ListPerson { get; set; }
        public Pager pager { get; set; }
    }
    public class Pager
    {
        public Pager(int totalItems, int? page, int pageSize = 10)
        {
            // Total Paging need to show
            int _totalPages = (int)Math.Ceiling((decimal)totalItems / (decimal)pageSize);
            //Current Page
            int _currentPage = page != null ? (int)page : 1;
            //Paging to be starts with
            int _startPage = _currentPage - 5;
            //Paging to be end with
            int _endPage = _currentPage + 4;
            if (_startPage <= 0)
            {
                _endPage -= (_startPage - 1);
                _startPage = 1;
            }
            if (_endPage > _totalPages)
            {
                _endPage = _totalPages;
                if (_endPage > 10)
                {
                    _startPage = _endPage - 9;
                }
            }
            //Setting up the properties
            TotalItems = totalItems;
            CurrentPage = _currentPage;
            PageSize = pageSize;
            TotalPages = _totalPages;
            StartPage = _startPage;
            EndPage = _endPage;
        }
        public int TotalItems { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int TotalPages { get; set; }
        public int StartPage { get; set; }
        public int EndPage { get; set; }
    }
}