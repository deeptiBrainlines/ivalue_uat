﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public partial class ival_ProjectUnitDetailsWithCategory
    {
        public int ID { get; set; }

        public int Project_ID { get; set; }

        [StringLength(50)]
        public string UType_ID { get; set; }

        public int Lookup_ID { get; set; }

        [StringLength(50)]
        public string Lookup_Type { get; set; }

        [StringLength(50)]
        public string Lookup_Value { get; set; }

    }
}