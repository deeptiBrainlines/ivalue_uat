namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_ProjectFloorWiseBreakUp
    {
        [Key]
        public int PFloor_ID { get; set; }

        public int? Project_ID { get; set; }

        public int? Wing_ID { get; set; }

        public int? Unit_ID { get; set; }

        [StringLength(50)]
        public string Floor_Type { get; set; }

        [StringLength(50)]
        public string Approved_Carpet_Area { get; set; }

        [StringLength(50)]
        public string Approved_Built_up_Area { get; set; }

        [StringLength(50)]
        public string Measured_Carpet_Area { get; set; }

        [StringLength(50)]
        public string Measured_Built_up_Area { get; set; }

        [StringLength(50)]
        public string Adopted_Area_for_Valuation { get; set; }

        [StringLength(50)]
        public string Adopted_Cost_Construction { get; set; }

        [StringLength(50)]
        public string Total { get; set; }
    }
}
