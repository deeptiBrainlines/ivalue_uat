namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_ProjBldgWingUnitMaster
    {
        [Key]
        public int Unit_ID { get; set; }

        public int? Wing_ID { get; set; }

        [StringLength(50)]
        public string UType { get; set; }

        [StringLength(50)]
        public string PTypeName { get; set; }

        public int? UnitNo { get; set; }

        [StringLength(50)]
        public string RERA_Registration_No { get; set; }

        [StringLength(50)]
        public string No_of_Buildings { get; set; }

        [StringLength(50)]
        public string No_Of_Wings { get; set; }

        [StringLength(50)]
        public string No_of_Floors { get; set; }

        [StringLength(50)]
        public string No_of_Units { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Expected_completion_date { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}
