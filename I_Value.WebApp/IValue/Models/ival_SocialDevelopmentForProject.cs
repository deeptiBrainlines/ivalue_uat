namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_SocialDevelopmentForProject
    {
        [Key]
        public int Social_Dev_ID { get; set; }

        public int? Project_ID { get; set; }

        [StringLength(50)]
        public string Social_Development_Type { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        public DateTime? Updated_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }
    }
}
