namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_empList
    {
        [Key]
        public int Emp_ID { get; set; }

        [StringLength(50)]
        public string Emp_Name { get; set; }
    }
}
