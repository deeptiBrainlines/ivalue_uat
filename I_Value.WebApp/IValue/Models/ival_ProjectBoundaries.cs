namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_ProjectBoundaries
    {
        [Key]
        public int Boundry_ID { get; set; }

        public int Project_ID { get; set; }

        public int? Building_ID { get; set; }

        public int? Wing_ID { get; set; }

        public int? Unit_ID { get; set; }

        public bool? Is_Active { get; set; }

        [StringLength(50)]
        public string Boundry_Name { get; set; }

        [StringLength(50)]
        public string AsPer_Document { get; set; }

        [StringLength(50)]
        public string AsPer_Site { get; set; }

        [StringLength(50)]
        public string Plot_Dimentions { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}
