﻿using IValuePublishProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace I_Value.WebApp.Models
{
    public class VMPropertyUnitTypeMaster : ival_PropertyUnitTypeMaster
    {
        // public IEnumerable<ival_SubLocalityMaster1> ival_SubLocalityMaster1 { get; set; }
        public IEnumerable<ival_PropertyUnitTypeMaster> ival_PropertyUnitTypeMaster { get; set; }
        public IEnumerable<ival_PropertyType> ival_PropertyTypeList { get; set; }
        public IEnumerable<ival_SelectType> ival_SelectTypeList { get; set; }
        public IEnumerable<ival_UnitType> ival_UnitTypeList { get; set; }




        public int property_id { get; set; }


        [StringLength(100)]
        public string property_Type { get; set; }

        [StringLength(100)]
        public string select_Type { get; set; }



        [StringLength(100)]
        public string unit_Type { get; set; }

        public int Lookup_ID { get; set; }

        [StringLength(100)]
        public string Lookup_Type { get; set; }

        [StringLength(100)]
        public string Lookup_Value { get; set; }




    }
}