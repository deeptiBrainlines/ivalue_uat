﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public class ival_SubLocalityMaster
    {
          
            public int Loc_Id { get; set; }

        public int City_Id { get; set; }

        [StringLength(100)]
            public string Loc_Name { get; set; }

        [StringLength(100)]
        public string City_Name { get; set; }

        public int Pin_Code { get; set; }

            [StringLength(250)]
            public string Sub_Locality { get; set; }

            public int SubLocality_Id { get; set; }

        [StringLength(250)]
        public string District_Name { get; set; }

        

    }
}