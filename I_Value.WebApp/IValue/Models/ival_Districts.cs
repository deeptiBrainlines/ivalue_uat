namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_Districts
    {
        [Key]
        public int District_ID { get; set; }

        public int City_ID { get; set; }

        public int State_ID { get; set; }

        [StringLength(250)]
        public string District_Name { get; set; }

        [StringLength(250)]
        public string City_Name { get; set; }
    }
}
