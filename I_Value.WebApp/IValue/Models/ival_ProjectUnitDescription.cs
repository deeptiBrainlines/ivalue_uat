namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_ProjectUnitDescription
    {
        [Key]
        public int Desc_Unit_ID { get; set; }

        [StringLength(50)]
        public string Description_of_Units { get; set; }

        [StringLength(50)]
        public string Type { get; set; }
    }
}
