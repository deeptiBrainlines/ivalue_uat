﻿using IValuePublishProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace I_Value.WebApp.Models
{
    public class VMBuilderGroupMaster
    {
        
        public ival_BuilderGroupMaster ival_BuilderGroupMaster { get; set; }
        public ival_BuilderCompanyMaster ival_BuilderCompanyMaster { get; set; }
        public ival_ProjectMasterDetails Ival_ProjectMasterDetails { get; set; }
        public ival_BuilderCompanyMasterNew ival_BuilderCompanyMasterNew { get; set; }
        public ival_natureOfCompany ival_NatureOfCompany { get; set; }
        public IEnumerable<ival_States> ival_StatesList { get; set; }
        //changes by punam
        public IEnumerable<ival_Cities> ival_CitiesList { get; set; }
        public IEnumerable<ival_Districts> ival_DistrictsList { get; set; }
        public IEnumerable<ival_LookupCategoryValues> PincodeList { get; set; }
        public IEnumerable<ival_Locality> ival_LocalityList { get; set; }
        //public IEnumerable<ival_Cities> ival_pincodelist { get; set; }
        public IEnumerable<ival_natureOfCompany> ival_natureOfcompanyList { get; set; }
        public IEnumerable<ival_BuilderCompanyMasterNew> ival_BuilderCompanyMasterNewList { get; set; }
        public IEnumerable<ival_ProjectMasterDetails> ival_projectMasterDetailsList { get; set; }
        public IEnumerable<ival_BuilderGroupMaster> BuilderGroupList { get; set; }
       
        public int Builder_Group_Master_IDl { get; set; }
        public int Builder_Company_Master_IDl { get; set; }
        public int Builder_Project_Master_ID { get; set; }
        public string BuilderGroupNamel { get; set; }
        public string Builder_Cityl { get; set; }
        public string Builder_Company_Name { get; set; }
        public string Builder_Statel { get; set; }
        public string Builder_Grade { get; set; }
        public int SelectedBuilderStateId { get; set; }
        public int  SelectedBuilderPincodeID { get; set; }
        public int SelectedBuilderGroupMasterID { get; set; }
        public string SelectedBuilderNatureOfCom { get; set; }
        public int SelectedBuilderCompanyStateID { get; set; }
        public int SelectedBuilderCompanyPincodeID { get; set; }
        public int SelectedBuilderPostalStateID { get; set; }
        public int SelectedBuilderPostalPincodeID { get; set; }
        //added by punam
        public int selectedDistID { get; set; }
        public int selectedCityID { get; set; }
        public int selectedLocID { get; set; }
        public int Builder_Pincode { get; set; }
        public string ReportType { get; set; }
        ///for Project Request Details
        public IEnumerable<ival_ClientMaster> ival_ClientMasters { get; set; }
        public IEnumerable<ival_BranchMaster> ival_BranchMasters { get; set; }
        public int SelectBankID { get; set; }

        public string SelectBranchName { get; set; }

        public List<ival_LookupCategoryValues> ival_LookupCategoryDepartment { get; set; }
        public int SelectDepartmentID { get; set; }

        public List<ival_LookupCategoryValues> ival_LookupCategoryValuesreporttype { get; set; }
        public int Selectedreporttype { get; set; }
        public List<ival_LookupCategoryValues> ival_LookupCategoryValuesmethodvaluation { get; set; }
        public int Selectedmethodvaluation { get; set; }
        public List<ival_LookupCategoryValues> ival_LookupCategoryPropertyType { get; set; }
        public int SelectPropertyType { get; set; }
        public string RequestType { get; internal set; }
        public string ReportRequestType { get; set; }
        public int SelectedModelRiskDemo { get; set; }
        public int SelectedMProjectUnitDescription { get; set; }
        public int MaintanceOfProperty { get; set; }
        public int QualityOfConstruction { get; set; }
        public int BuildingId { get; set; }
        public int wingId { get; set; }
        public List<ival_DocumentListNew> ival_DocumentLists { get; set; }
        public List<iva_UploadDocumentListNew> ival_Document_Uploaded_ListS_New { get; set; }
        public List<iva_UploadDocumentListNew> ival_DocumentListsNew { get; set; }
        public List<ival_Document_Uploaded_List> ival_Document_Uploaded_ListS { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesRiskofdemolition { get; set; }
        public IEnumerable<ival_ProjectUnitDescription> ival_ProjectUnitDescriptions { get; set; }
        public List<ival_ProjectUnitDescription> ival_ProjectUnitDescriptionsList { get; set; }
        public List<ival_LookupCategoryValues> ival_lookupCategoryAmenities { get; set; }
        public List<ival_ProjectValuationAmenitiesInfrastructureProgress> ival_ProjectValuationAmenities { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_lookupProjectMaintainceProperty { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_lookupProjectQualityConstruction { get; set; }
        public IEnumerable<ival_ProjectValuationAmenitiesInfrastructureProgress> ival_ProjectValuationAmenitiesNew { get; set; }
        public IEnumerable<ival_ProjectBuildingMaster> ival_ProjectBuildingMasters { get; set; }
        public List<ival_LookupCategoryValues> ival_lookupProjectSideMargin { get; set; }
        public List<ProjectSiteEnginner> projSiteEnginnerList { get; set; }
        public List<ival_ImageMaster> ival_ProjectImageName { get; set; }
        public List<UploadedImageAndImageMaster> UploadedImagesList { get; set; }
        public int siteEnginnerId { get; set; }

        [Key]
        public int Builder_Group_Master_ID { get; set; }

        [StringLength(50)]
        [Required]
        public string BuilderGroupName { get; set; }

        [StringLength(100)]
        public string Builder_Address1 { get; set; }

        [StringLength(100)]
        public string Builder_Address2 { get; set; }

        [StringLength(100)]
        public string Builder_Locality { get; set; }

        [StringLength(50)]
        public string Builder_State { get; set; }

        [StringLength(50)]
        public string Builder_District { get; set; }

        [StringLength(50)]
        public string Builder_City { get; set; }

        [StringLength(100)]
        public string Nearby_Landmark { get; set; }

        [StringLength(50)]
        public string Director_Name1 { get; set; }

        [StringLength(50)]
        public string Director_Phone1 { get; set; }

        [StringLength(50)]
        public string Director_Mobile1 { get; set; }

        [StringLength(50)]
        public string Director_Email1 { get; set; }

        [StringLength(50)]
        public string Director_Name2 { get; set; }

        [StringLength(50)]
        public string Director_Phone2 { get; set; }

        [StringLength(50)]
        public string Director_Mobile2 { get; set; }

        [StringLength(50)]
        public string Director_Email2 { get; set; }

        [StringLength(50)]
        public string Director_Name3 { get; set; }

        [StringLength(50)]
        public string Director_Phone3 { get; set; }

        [StringLength(50)]
        public string Director_Mobile3 { get; set; }

        [StringLength(50)]
        public string Director_Email3 { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Create_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }

       

        [StringLength(50)]
        public string Builder_Street { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Builder_Company_Master_ID { get; set; }


        [StringLength(50)]
        public string Builder_Office_Address1 { get; set; }

        [StringLength(50)]
        public string Builder_Office_Address2 { get; set; }

        [StringLength(50)]
        public string Builder_C_Street { get; set; }

        [StringLength(50)]
        public string Builder_C_Locality { get; set; }

        [StringLength(50)]
        public string Builder_C_City { get; set; }

        [StringLength(50)]
        public string Builder_C_District { get; set; }

        [StringLength(50)]
        public string Builder_C_State { get; set; }

        [StringLength(50)]
        public string Builder_C_Pincode { get; set; }

        [StringLength(50)]
        public string Builder_Nearby_LandMark { get; set; }

        [StringLength(50)]
        public string Nature_Of_Company { get; set; }


        [StringLength(50)]
        public string Director_EmailId1 { get; set; }

       

        [StringLength(50)]
        public string Director_EmailId2 { get; set; }

       
        [StringLength(50)]
        public string Director_EmailId3 { get; set; }

       
        public DateTime? Created_Date { get; set; }

       
    }
}