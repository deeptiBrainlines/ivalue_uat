namespace I_Value.WebApp
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    //using System.Data.Entity.Spatial;

    public partial class ival_Project_Boundaries
    {
        [Key]
        //////// [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Project_ID { get; set; }


        public int? Building_ID { get; set; }

        public int? Wing_ID { get; set; }

        public int? Unit_ID { get; set; }

        public int? Boundry_ID { get; set; }

        [StringLength(50)]
        public string AsPer_Document { get; set; }
        [StringLength(50)]
        public string Boundry_Name { get; set; }

        [StringLength(50)]
        public string AsPer_Site { get; set; }
    }
}
