﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public class VMUnitParameterPriceDetails
    {
        public int UnitPriceParameterID { get; set; }
        public string Name { get; set; }

        public int UnitPriceParaValue_ID { get; set; }

        public string UnitPriceParameter_Value { get; set; }
        public string UnitPriceParameter_ValueSq { get; set; }
    }
}