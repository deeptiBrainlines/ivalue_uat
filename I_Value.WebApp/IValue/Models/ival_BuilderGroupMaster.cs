namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_BuilderGroupMaster
    {
        [Key]
        public int Builder_Group_Master_ID { get; set; }

        [StringLength(50)]
        [Required]
        public string BuilderGroupName { get; set; }

        [StringLength(100)]
        public string Builder_Address1 { get; set; }

        [StringLength(100)]
        public string Builder_Address2 { get; set; }

        [StringLength(100)]
        public string Builder_Locality { get; set; }

        [StringLength(50)]
        public string Builder_State { get; set; }

        [StringLength(50)]
        public string Builder_District { get; set; }

        [StringLength(50)]
        public string Builder_City { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Builder_Pincode { get; set; }

        [StringLength(100)]
        public string Nearby_Landmark { get; set; }

        [StringLength(50)]
        public string Director_Name1 { get; set; }

        [StringLength(50)]
        public string Director_Phone1 { get; set; }

        [StringLength(50)]
        public string Director_Mobile1 { get; set; }

        [StringLength(50)]
        public string Director_Email1 { get; set; }

        [StringLength(50)]
        public string Director_Name2 { get; set; }

        [StringLength(50)]
        public string Director_Phone2 { get; set; }

        [StringLength(50)]
        public string Director_Mobile2 { get; set; }

        [StringLength(50)]
        public string Director_Email2 { get; set; }

        [StringLength(50)]
        public string Director_Name3 { get; set; }

        [StringLength(50)]
        public string Director_Phone3 { get; set; }

        [StringLength(50)]
        public string Director_Mobile3 { get; set; }

        [StringLength(50)]
        public string Director_Email3 { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Create_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }

        [StringLength(50)]
        public string Builder_Grade { get; set; }

        [StringLength(50)]
        public string Builder_Street { get; set; }
    }
}
