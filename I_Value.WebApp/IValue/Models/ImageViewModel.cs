﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public class ImageViewModel
    {
        
            public byte[] Image { get; set; }
            public MemoryStream Imagestream { get; set; }
        
    }
}