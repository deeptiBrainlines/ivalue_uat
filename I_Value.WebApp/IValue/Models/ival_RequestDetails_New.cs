namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_RequestDetails_New
    {
        [Key]
        public int Request_ID { get; set; }

        [StringLength(50)]
        public string Request_Flag { get; set; }

        [StringLength(50)]
        public string Request_Type { get; set; }

        [StringLength(50)]
        public string Client_ID { get; set; }

        [StringLength(50)]
        public string Department { get; set; }

        [StringLength(50)]
        public string Loan_Type { get; set; }

        public bool? IsEndCustomer { get; set; }

        [StringLength(50)]
        public string Requestor_Name { get; set; }

        [StringLength(50)]
        public string Customer_Application_ID { get; set; }

        [StringLength(50)]
        public string Customer_Name { get; set; }

        [StringLength(50)]
        public string Customer_Name_1 { get; set; }

        [StringLength(50)]
        public string Customer_Contact_No_1 { get; set; }

        [StringLength(50)]
        public string Customer_Contact_No_2 { get; set; }

        [StringLength(50)]
        public string Request_Date { get; set; }

        [StringLength(50)]
        public string Email_ID { get; set; }

        [StringLength(50)]
        public string Valuation_Request_ID { get; set; }

        [StringLength(50)]
        public string Document_Holder_Name { get; set; }

        [StringLength(50)]
        public string Document_Holder_Name_1 { get; set; }

      

        [StringLength(50)]
        public string Type_Of_Ownership { get; set; }

        [StringLength(50)]
        public string AssignedToEmpID { get; set; }

        [StringLength(50)]
        public string PropertyType { get; set; }

        [StringLength(50)]
        public string ValuationType { get; set; }

        [StringLength(50)]
        public string ReportType { get; set; }

        [StringLength(50)]
        public string Method_of_Valuation { get; set; }

        public int? Project_ID { get; set; }

        public int? Building_ID { get; set; }

        public int? Wing_ID { get; set; }

        public int? Unit_ID { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}
