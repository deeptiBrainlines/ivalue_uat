﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public class VMUnitParameterDetails
    {
        public Int32 Unit_ID { get; set; }
        
        public Int32 UnitParaValue_ID { get; set; }

        public Int32 UnitParameter_ID { get; set; }

        public string UnitParameter_Value { get; set; }

        public string Name { get; set; }
    }
}