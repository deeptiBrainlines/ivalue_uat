﻿using IValuePublishProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace I_Value.WebApp.Models
{
    public class ViewModelRequest
    {
      
        public int SelectedModelClientmaster { get; set; }
        public int SelectedModelBranchMaster { get; set; }
        public int SelectedModelClientstate { get; set; }
        public int SelectedModelclientpincode { get; set; }
        public int SelectedModelAssignto { get; set; }
        public int SelectedModelProjectType { get; set; }
        public int SelectedModelValuationtype { get; set; }
        public int SelectedModelretailstates { get; set; }
        public int SelectedModelretailpincode { get; set; }
        public int SelectedModelretailvaluationtype { get; set; }
        public int SelectedModelretailreporttype { get; set; }

        public int? SelectedModelGroupMaster { get; set; }
        public int? SelectedModelCompanyGpMaster { get; set; }
        public int? SelectedModelProject { get; set; }
        public int? SelectedModelBuilding { get; set; }
        public int? SelectedModelWing { get; set; }
        public int? SelectedModelPropertytype { get; set; }
        public int? SelectedModelUnittype { get; set; }

        public int? SelectedModelviewunits { get; set; }
        public int? SelectedModelLiftno { get; set; }

        public int? Selectedtypeofstructure { get; set; }
        public int? SelectedModelQualityConstruction { get; set; }
        public int? SelectedModelMaintenanceoProperty { get; set; }
        public string SelectedRelationWithEmpID { get; set; }
        public string SelectedRelationWithEmpID3 { get; set; }
        public string SelectedRelationWithEmpID4 { get; set; }
        public string SelectedRelationWithEmpID1 { get; set; }
        public string SelectedRelationWithEmpID2 { get; set; }
        public int? SelectedModelOccupancyDetails { get; set; }
        //changes by punam
        public IEnumerable<ival_LookupCategoryValues> relationWithEmpList { get; set; }
        public IEnumerable<ival_LookupCategoryValues> relationWithEmpList1 { get; set; }
        public IEnumerable<ival_LookupCategoryValues> relationWithEmpList2 { get; set; }
        public int? Type_of_Flooring { get; set; }
        public int? Wall_Finish { get; set; }
        public int? Plumbing_Fitting { get; set; }
        public int? Electrical_Fittings { get; set; }

        public int? quality { get; set; }

        public int? Rough_Plaster { get; set; }

        public List<iva_UploadDocumentListNew> ival_Document_Uploaded_ListS_New { get; set; }
        public List<iva_UploadDocumentListNew> ival_DocumentListsNew { get; set; }


        public int? Selectedtypeofownership{ get; set; }
        public int? Selectedvaluationtype { get; set; }
        public int? Selectedreporttype { get; set; }
        public int? Selectedemp { get; set; }
        public int? Selectedmethodvaluation { get; set; }

        public int? SelectedModelRequesttype { get; set; }
        public int? SelectedModelReportrequesttype { get; set; }


        public int? SelectedModelRiskDemo { get; set; }
        public int? Selectedtypeofdemarcations { get; set; }
        public int? Selectedtypeofdemarcations1 { get; set; }
        public int? Selectedtypeofdemarcations2 { get; set; }
        public int? SelectedRoadFinish { get; set; }
        public int? SelectedpropZone { get; set; }

        public IEnumerable<ival_ClientMaster> ival_ClientMasters { get; set; }
        public IEnumerable<ival_BranchMaster> ival_BranchMasters { get; set; }
        public IEnumerable<ival_States> ival_States { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuespincode { get; set; }
        public IEnumerable<ival_EmployeeMaster> ival_LookupCategoryValuesAssign { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryProjectType { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValtype { get; set; }

        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuestypeofstructure { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesQualityConstruction { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesMaintenanceoProperty { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesOccupancyDetails { get; set; }

        public IEnumerable<ival_States> ival_RetailStates { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesretailpincode { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesretailreporttype { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesretailvaluetype { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesviewfromunit { get; set; }
        public ival_RequestDetails _Ival_RequestDetails { get; set; }
        public ival_ClientMaster Ival_ClientMasterretails { get; set; }

        //for client request 24-4-20
        public List <ival_BuilderCompanyMaster> ival_BuilderCompanyMasters { get; set; }
        public List<ival_BuilderGroupMaster> ival_BuilderGroupMasters { get; set; }

        public SelectList BuilderMasterslist { get; set; }
        public SelectList CompanyList { get; set; }
      //  public SelectList SubLocalityList { get; set; }

        //fordynamic unitdetails
        public List<ival_UnitParameterMaster> ival_UnitParameterMasters { get; set; }
        public List<ival_UnitParametersValue> ival_UnitParametersValues { get; set; }
        public List<ival_LookupCategoryValues> ival_LookupCategoryBoundaries { get; set; }
        public List<ival_LookupCategoryValues> ival_LookupCategoryFloorTypes{ get; set; }
        public List<ival_ProjectBuildingWingFloorDetails> ival_ProjectBuildingWingFloorDetails { get; set; }

        public List<ival_LookupCategoryValues> ival_Lookupwall { get; set; }

        public List<ival_LookupCategoryValues> ival_Lookupflooring { get; set; }
        public List<ival_LookupCategoryValues> ival_Lookupplumbing { get; set; }
        public List<ival_LookupCategoryValues> ival_Lookupelec { get; set; }
        public List<ival_LookupCategoryValues> ival_Lookupother { get; set; }
        public List<ival_LookupCategoryValues> ival_Lookupexter { get; set; }
        public List<ival_LookupCategoryValues> ival_LookupCategoryLiftTypes { get; set; }
        public List<ival_ProjectBoundaries> ival_ProjectBoundaries { get; set; }
        public List<ival_ProjectSideMargin> ival_ProjectSideMargins { get; set; }
        public List<ival_BrokerDetails> ival_BrokerDetails { get; set; }
        public List<iva_Comparable_Properties> iva_Comparable_Properties { get; set; }
        public List<ival_ProjectFloorWiseBreakUp> ival_ProjectFloorWiseBreakUps { get; set; }
        public List<ival_ProjectFloorWiseBreakUp> ival_ProjectFloorWiseBreakUpstotal { get; set; }
        public List<ival_LookupCategoryValues> ival_LookupCategorySideMargins { get; set; }
        public List<ival_DocumentList> ival_DocumentLists { get; set; }
        public List<ival_DocHolderName> ival_DocHolderNames { get; set; }
        public List<ival_Document_Uploaded_List> ival_Document_Uploaded_ListS { get; set; }
        //changes by punam
        //public List<iva_UploadDocumentListNew> ival_Document_Uploaded_ListS_New { get; set; }
        //public List<iva_UploadDocumentListNew> ival_DocumentListsNew { get; set; }

        public List<ival_ProjectSideMargin> ival_ProjectSideMargin { get; set; }
        public List<ival_LookupCategoryValues> ival_ApprovalType { get; set; }

        public List<ival_ProjectApprovalDetails> ival_ProjectApprovalDetails { get; set; }

        public List<ival_UnitPriceParametersValue> ival_UnitPriceParametersValues { get; set; }

        
        public IEnumerable<ival_UnitParameterMaster> ival_UnitParameterMaster { get; set; }


        public List<ival_ProjectMaster> ival_ProjectMasters { get; set; }
        public VMRequestIndex VMRequestIndexs { get; set; }
        public IEnumerable<VMRequestIndex> VMRequestIndex { get; set; }

        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuestypeofownership { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesvaluationtype { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesreporttype { get; set; }
        public IEnumerable<ival_empList> ival_LookupEmp { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesmethodvaluation { get; set; }

        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesRequestType { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesRequestReportType { get; set; }

        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesRiskofdemolition { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesdemarcation{ get; set; }

        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesdemarcation1 { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_LookupCategoryValuesRoadFinish { get; set; }
        public List<ival_LookupCategoryValues> ival_LookupCategoryValuesPropZone { get; set; }
        public List<ival_ImageMaster> ival_ImageMasters { get; set; }
        //changes by punam
        public List<ProjectSiteEnginner> projSiteEnginnerList { get; set; }
        public int siteEnginnerId { get; set; }


        [DataType(DataType.Date)]
        [Required]
        public DateTime? DOR { get; set; }

        //Priyanka 13-06-2020
        public bool IsBankNBFC { get;  set; }

        public List<ival_LookupCategoryValues> ival_LookupCategoryDepartment { get;  set; }
        public int SelectDepartmentID { get; set; }
        public List<ival_LookupCategoryValues> ival_LookupCategoryLoanType { get; set; }
        public int SelectLoanTypeID { get; set; }
        public string RequestType { get; internal set; }
        public string ReportRequestType { get;  set; }
        public int SelectBankID { get;  set; }

        public string SelectBranchName { get; set; }
        public List<ival_ClientMaster> ClientList { get; internal set; }

        public List<ival_ImageMaster> ival_ProjectImageName { get; set; }
        public List<UploadedImageAndImageMaster> UploadedImagesList { get; set; }
        public List<Image_Uploaded_List> Image_Uploaded_List { get; set; }
      
    }

}