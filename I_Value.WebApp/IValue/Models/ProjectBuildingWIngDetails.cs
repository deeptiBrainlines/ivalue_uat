﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public class ProjectBuildingWIngDetails
    {
        public int WingID { get; set; }
        public string WingName { get; set; }
        public int BuildingId { get; set; }
        public string BuildingName { get; set; }
        public int ProjectID { get; set; }
        public int ProjectValuationLogId { get; set; }
        public List<ival_ProjBldgWingMaster> bldWingList { get; set; }
        public IEnumerable<ival_ProjBldgWingMaster> bldWingListNew { get; set; }
    }
}