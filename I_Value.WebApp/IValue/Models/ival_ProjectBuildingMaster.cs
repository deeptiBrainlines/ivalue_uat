namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_ProjectBuildingMaster
    {
        [Key]
        public int Building_ID { get; set; }

        public int? Project_ID { get; set; }

        [StringLength(100)]
        public string Building_Name { get; set; }

        [StringLength(50)]
        public string No_of_Wings { get; set; }

        [StringLength(50)]
        public string Approved_Building_Area { get; set; }

        [StringLength(50)]
        public string Approved_No_of_Units { get; set; }

        [StringLength(50)]
        public string Remarks { get; set; }

        [StringLength(10)]
        public string Cord_Latitutde { get; set; }

        [StringLength(10)]
        public string Cord_Longitude { get; set; }

        [StringLength(50)]
        public string Deviation_Description { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}
