﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public class ival_PropertyType
    {
        [Key]
        public int property_id { get; set; }
        [StringLength(100)]
        public string property_Type { get; set; }

        [StringLength(100)]
        public string select_Type { get; set; }

        [StringLength(100)]
        public string unit_Type { get; set; }

        public int Lookup_ID { get; set; }

        [StringLength(100)]
        public string Lookup_Type { get; set; }

        [StringLength(100)]
        public string Lookup_Value { get; set; }
    }
}