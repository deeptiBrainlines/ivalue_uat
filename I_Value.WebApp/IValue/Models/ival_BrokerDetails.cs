namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_BrokerDetails
    {
        [Key]
        public int Broker_ID { get; set; }

        public int Broker_IDs { get; set; }
        public int? Project_ID { get; set; }

        public int? Unit_ID { get; set; }

        [StringLength(50)]
        public string Broker_Name { get; set; }

        [StringLength(100)]
        public string Firm_Name { get; set; }

        [StringLength(50)]
        public string Contact_Num { get; set; }

        [StringLength(50)]
        public string Rate_Quoted_For_Land { get; set; }

        [StringLength(50)]
        public string Rate_Quoted_For_Flat { get; set; }
    }
}
