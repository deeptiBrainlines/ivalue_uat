namespace IValuePublishProject.Models.test
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_Image_Uploaded_List
    {
        [Key]
        public int Image_ID { get; set; }

        public int? Image_Type_ID { get; set; }

        public int? Request_ID { get; set; }

        public int? Project_ID { get; set; }

        public int? Unit_ID { get; set; }

        [StringLength(50)]
        public string Image_Path { get; set; }

        [Column(TypeName = "image")]
        public byte[] Uploaded_Image { get; set; }

        [Column(TypeName = "image")]
        public byte[] Uploaded_IGR_Image { get; set; }
    }
}
