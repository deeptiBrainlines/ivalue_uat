﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public class ival_ProjectMasterRetailProperty
    {

        [Key]
        public int Project_ID { get; set; }

        [StringLength(50)]
        public string Property_Type { get; set; }

        [StringLength(50)]
        public string TypeOfUnit { get; set; }

        [StringLength(50)]
        public string StatusOfProperty { get; set; }

        [StringLength(100)]
        public string PlotBunglowNumber { get; set; }

        [StringLength(100)]
        public string UnitNumber { get; set; }

        [StringLength(50)]
        public string Project_Name { get; set; }

        [StringLength(50)]
        public string Building_Name_RI { get; set; }

        [StringLength(50)]
        public string Wing_Name_RI { get; set; }

        [StringLength(50)]
        public string Survey_Number { get; set; }

        [StringLength(50)]
        public string Plot_Number { get; set; }


        [StringLength(50)]
        public string Ward_Number { get; set; }

        [StringLength(50)]
        public string Municipal_Number { get; set; }

        [StringLength(50)]
        public string Village_Name { get; set; }

        [StringLength(50)]
        public string Street { get; set; }

        [StringLength(50)]
        public string Locality { get; set; }

        [StringLength(50)]
        public string Sub_Locality { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(50)]
        public string District { get; set; }

        [StringLength(50)]
        public string PinCode { get; set; }

        [StringLength(50)]
        public string NearBy_LandMark { get; set; }

        [StringLength(50)]
        public string Nearest_RailwayStation { get; set; }


        [StringLength(50)]
        public string Nearest_BusStop { get; set; }


        [StringLength(50)]
        public string Nearest_Hospital { get; set; }

        [StringLength(50)]
        public string Nearest_Market { get; set; }

        [StringLength(50)]
        public string RERA_Approval_Num { get; set; }

        [StringLength(50)]
        public string Approval_Flag { get; set; }

        [StringLength(50)]
       public string Actual_No_Floors_G { get; set; }

        [StringLength(50)]
        public string Floor_Of_Unit { get; set; }
        [StringLength(50)]
        public string Approved_Usage_Of_Property { get; set; }

        [StringLength(50)]
        public string Actual_Usage_Of_Property { get; set; }


        [StringLength(50)]
        public string Floor_Of_UnitNo { get; set; }



        [StringLength(50)]
        public string Current_Age_Of_Property { get; set; }

        [StringLength(50)]
        public string Residual_Age_Of_Property { get; set; }

        [StringLength(50)]
        public string Common_Areas { get; set; }

        [StringLength(50)]
        public string Facilities { get; set; }

        [StringLength(50)]
        public string Anyother_Observation { get; set; }

        [StringLength(50)]
        public string Roofing_Terracing { get; set; }

        [StringLength(50)]
        public string Quality_Of_Fixture { get; set; }

        [StringLength(50)]
        public string Quality_Of_Construction { get; set; }

        [StringLength(50)]
        public string Maintenance_Of_Property { get; set; }

        [StringLength(50)]
        public string Marketability_Of_Property { get; set; }

        [StringLength(50)]
        public string Renting_Potential { get; set; }

        [StringLength(50)]
        public string Occupied_Since { get; set; }

        [StringLength(50)]
        public string Expected_Monthly_Rental { get; set; }
        [StringLength(50)]
        public string Name_Of_Occupant { get; set; }
        [StringLength(50)]
        public string Monthly_Rental { get; set; }
        [StringLength(50)]
        public string Balanced_Leased_Period { get; set; }
        //[StringLength(50)]
        //public string Occupied_Since { get; set; }





















        public int? Builder_Group_ID { get; set; }

        public int? Builder_Company_ID { get; set; }

        public bool? Is_Active { get; set; }

        [StringLength(50)]
        public bool? Is_Retail_Individual { get; set; }



        [StringLength(50)]
        public string Legal_Address1 { get; set; }





        [StringLength(50)]
        public string Legal_Pincode { get; set; }

        [StringLength(50)]
        public string Postal_Address1 { get; set; }

        [StringLength(50)]
        public string Postal_Address2 { get; set; }


        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }


    }
}