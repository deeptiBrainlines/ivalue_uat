﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.Linq;
//using System.Web;

//namespace IValuePublishProject.Models
//{
//    public class ival_DocHolderName
//    {

//        public IEnumerable<ival_ClientAndBranchMasterNew> ival_ClientAndBranchMasterNew { get; set; }
//        public int SrNo { get; set; }
//        public int Client_ID { get; set; }
//        public int Client_Id { get; set; }
//    }
//}
namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_DocHolderName
    {
        [Key]
        public int SrNo { get; set; }
        public int Request_ID { get; set; }
        public int DocHolderName_id { get; set; }

        [StringLength(100)]
        public string DocHolderName { get; set; }

        
    }
}

