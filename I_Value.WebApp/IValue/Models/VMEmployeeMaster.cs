﻿using IValuePublishProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace I_Value.WebApp.Models
{
    public class VMEmployeeMaster
    {
        public ival_EmployeeMaster ival_EmployeeMaster { get; set; }
        public IEnumerable<ival_BranchMaster> ival_BranchMastersList { get; set; }

        public IEnumerable<ival_RoleMaster> ival_RoleMastersList { get; set; }
        public IEnumerable<ival_States> ival_StatesList { get; set; }
        
        public IEnumerable<ival_Districts> ival_DistrictsList { get; set; }
        public IEnumerable<ival_LookupCategoryValues> relationWithEmpList { get; set; }
        public IEnumerable<ival_LookupCategoryValues> currentstatus { get; set; }
        public IEnumerable<ival_LookupCategoryValues> banklist { get; set; }
        public IEnumerable<ival_Cities> ival_CitiesList { get; set; }
        public IEnumerable<ival_Locality> ival_LocalityList { get; set; }
        public IEnumerable<ival_Locality> pincodeList { get; set; }

       /* public IEnumerable<ival_LookupCategoryValues> pincodeList { get; set; }*/

        public IEnumerable<SelectListItem> bloodGrpList { get; set; }

        //public IEnumerable<SelectListItem> relationWithEmpList { get; set; }

        public int selectedDistID { get; set; }
        public int selectedCityID { get; set; }
        public int SelectedRoleId { get; set; }
        public int SelectedBranchId { get; set; }
        public string Selectedstatus { get; set; }
        public int SelectedCurrentStateId { get; set; }
        public int SelectedPermenantStateId { get; set; }
        public int SelectedBankStateId { get; set; }
        public int SelectedCurrentDistId { get; set; }
        public int SelectedPermenantDistId { get; set; }
        public int SelectedBankDistId { get; set; }
        public int SelectedCurrentCityID { get; set; }
        public int SelectedPermenantCityID { get; set; }
        public int SelectedBankCityID { get; set; }
        public int SelectedCurrentPincodeID { get; set; }
        public int SelectedPermanentPincodeID { get; set; }
        public int SelectedBankPincodeID { get; set; }

        public string SelectedRelationWithEmpID { get; set; }
        public string SelectedBloodGrpID { get; set; }
        public Boolean IsSameAddress { get; set; }
        public int Loc_Id { get; set; }

        public int BankLoc_Id { get; set; }
        public int City_ID { get; set; }
        
        public int PermLoc_Id { get; set; }
        public string Loc_Name { get; set; }
        public int? Pin_Code { get; set; }
        public int District_ID { get; set; }
        public string District_Name { get; set; }
        public int Employee_ID { get; set; }
        [Required(ErrorMessage = "Please Enter Company Email")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string Company_Email_ID { get; set; }
        [StringLength(50)]
        public string First_Name { get; set; }
        [StringLength(50)]
        public string current_status { get; set; }
        
        [StringLength(50)]
        public string Middle_Name { get; set; }
        [StringLength(50)]
        public string Last_Name { get; set; }
        [StringLength(50)]
        [Required(ErrorMessage = "Please Enter Phone Number")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string Contact_Number { get; set; }
        [StringLength(10)]
        [Required(ErrorMessage = "Please Enter Personal Email")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string Personal_Email_ID { get; set; }
        [StringLength(100)]
        public string Current_Address_1 { get; set; }
        [StringLength(100)]
        public string Current_Address_2 { get; set; }
        
        public int Current_City { get; set; }
        public int Current_State { get; set; }
        public int Perm_State { get; set; }
        public int Current_District { get; set; }
        [StringLength(50)]
        public string Aadhar_No { get; set; }
        [StringLength(50)]
        public string Pan_No { get; set; }
        [StringLength(50)]
        public string Bank_Name { get; set; }
        [StringLength(100)]
        public string Bank_Address1 { get; set; }
        [StringLength(100)]
        public string Bank_Address2 { get; set; }
        
        public int Bank_City { get; set; }

        public int Bank_Locality { get; set; }
        public int Bank_State { get; set; }
        public int Current_Pincode { get; set; }
        public int Perm_Pincode { get; set; }

        public int Bank_Pincode { get; set; }
        [StringLength(50)]
        public string Bank_IFSC_Code { get; set; }
        [StringLength(100)]
        public string Perm_Address_1 { get; set; }
        [StringLength(100)]
        public string Perm_Address_2 { get; set; }
        
        public int Perm_City { get; set; }
        
        public int Perm_District { get; set; }
       // public string Perm_State { get; set; }

        //public decimal? Perm_Pincode { get; set; }
        [StringLength(50)]
        public string Emergency_Contact_Name { get; set; }
        [StringLength(50)]
        public string Emergency_Contact_No { get; set; }
        [StringLength(50)]
        public string Branch_Name { get; set; }
        [StringLength(50)]
        public string Role_Name { get; set; }

        public int Perm_City1 { get; set; }
        public int Current_City1 { get; set; }
        
        public int Bank_District { get; set; }

    }
}