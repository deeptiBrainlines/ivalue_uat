namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_RequestDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Request_ID { get; set; }

        public bool? IsRetail { get; set; }

        public int? Client_ID { get; set; }

        [Column("Date Of Request")]
        public DateTime? Date_Of_Request { get; set; }

        [StringLength(50)]
        public string Request_Code { get; set; }

        public bool? IsEndCustomer { get; set; }

        [StringLength(50)]
        public string Customer_ID { get; set; }

        [StringLength(50)]
        public string Customer_Name { get; set; }

        [StringLength(50)]
        public string Customer_Contact_No { get; set; }

        [StringLength(50)]
        public string Address1 { get; set; }

        [StringLength(50)]
        public string Address2 { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string District { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PinCode { get; set; }

        [StringLength(50)]
        public string AssignedToEmpID { get; set; }

        [StringLength(50)]
        public string PropertyType { get; set; }

        [StringLength(50)]
        public string ValuationType { get; set; }

        [StringLength(50)]
        public string ReportType { get; set; }

        [StringLength(50)]
        public string Unit_ID { get; set; }

        [StringLength(50)]
        public string Type_of_Structure { get; set; }

        [StringLength(50)]
        public string Current_Age_of_Structure { get; set; }

        [StringLength(50)]
        public string Expected_Balance_Life { get; set; }

        [StringLength(50)]
        public string Quality_of_Construction { get; set; }

        [StringLength(50)]
        public string Mainte_of_Property { get; set; }

        [StringLength(50)]
        public string Occupancy_Details { get; set; }

        [StringLength(50)]
        public string Tenant_Name { get; set; }

        [StringLength(50)]
        public string Lease_Period { get; set; }

        [StringLength(50)]
        public string Lease_Rental { get; set; }

        [StringLength(50)]
        public string Approved_Usage { get; set; }

        [StringLength(50)]
        public string Actual_Usage { get; set; }

        [StringLength(50)]
        public string Total_Cost_of_Property { get; set; }

        [StringLength(50)]
        public string Stage_of_Construction { get; set; }

        [StringLength(50)]
        public string Stage_Description { get; set; }

        [StringLength(50)]
        public string Percent_Completed { get; set; }

        [StringLength(50)]
        public string Current_Value { get; set; }

        [StringLength(50)]
        public string Government_Rate { get; set; }

        [StringLength(50)]
        public string Government_Value { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}
