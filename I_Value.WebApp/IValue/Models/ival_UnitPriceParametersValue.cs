namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_UnitPriceParametersValue
    {
        [Key]
        public int UnitPriceParaValue_ID { get; set; }

        public int Unit_ID { get; set; }

        public int UnitPriceParameter_ID { get; set; }

        [StringLength(50)]
        public string UnitPriceParameter_Value { get; set; }

        //Added for binding price details update
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string UnitCost { get; set; }
          [StringLength(50)]
        public string Total_Cost_of_Property { get; set; }


        [StringLength(50)]
        public string Stage_of_Construction { get; set; }
        
        [StringLength(50)]
        public string Stage_Description { get; set; }


        [StringLength(50)]
        public string Percent_Completed { get; set; }

        [StringLength(50)]
        public string Valuation_of_unit { get; set; }


        [StringLength(50)]
        public string Current_Value { get; set; }

        [StringLength(50)]
        public string Government_Rate { get; set; }

        [StringLength(50)]
        public string Government_Value { get; set; }

        [StringLength(50)]
        public string UnitPriceSQMt_Value { get; set; }
        
    }
}
