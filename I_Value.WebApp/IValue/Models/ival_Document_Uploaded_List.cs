namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_Document_Uploaded_List
    {
        [Key]
        public int Docupload_ID { get; set; }

        public int? Document_ID { get; set; }

        public int? Request_ID { get; set; }

        public int? Unit_ID { get; set; }

        [StringLength(50)]
        public string Uploaded_Document { get; set; }
        [StringLength(50)]
        public string Document_Name { get; set; }

        public DateTime? Date_Of_Approval { get; set; }

        [StringLength(50)]
        public string Approval_Authority { get; set; }

        [StringLength(50)]
        public string Approval_Number { get; set; }

        [StringLength(50)]
        public string Date_Of_ApprovalNew { get; set; }
    }
}
