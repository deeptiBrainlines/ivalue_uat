﻿using IValuePublishProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace I_Value.WebApp.Models
{
    public class VMClientAndBranchMasterNew
    {
        public IEnumerable<ival_ClientAndBranchMasterNew> ival_ClientAndBranchMasterNew { get; set; }
        public List<ival_DepartmentList> ival_DepartmentList1 { get; set; }
        /*public IEnumerable<ival_DepartmentList> ival_DepartmentList1 { get; set; }*/
        public IEnumerable<ival_Cities> ival_CitiesList { get; set; }
        public IEnumerable<ival_States> ival_StatesList { get; set; }
        public IEnumerable<ival_Districts> ival_DistrictsList { get; set; }
        public IEnumerable<ival_Locality> ival_LocalityList { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_DepartmentList { get; set; }
        public IEnumerable<ival_LookupCategoryValues> ival_Des { get; set; }
        public IEnumerable<ival_ClientMaster> ival_ClientList { get; set; }

        public int SelectedStateID { get; set; }
        public int SelectedDistID { get; set; }

        public int SelectedCityID { get; set; }
        public int SelectedLocID { get; set; }
        public int SelectedBranchPincode { get; set; }

        public int SelectedClientID { get; set; }
        public int SelectedDepartmentID { get; set; }
        public int SelectedDeptID { get; set; }
        public int SelectedDeptID1 { get; set; }
        public int Row_ID { get; set; }
        public int Dept_ID { get; set; }
        public int Branch_Id { get; set; }
        public int Client_ID { get; set; }
        [StringLength(50)]
        public string Branch_Name { get; set; }
        [StringLength(50)]
        public string Address1 { get; set; }
        [StringLength(50)]

        public string Address2 { get; set; }
        [StringLength(50)]
        public string Contact_Name { get; set; }
        [StringLength(50)]
        public string Contact_No { get; set; }
        [StringLength(50)]
        public string Client_Desgn { get; set; }
        [StringLength(50)]
        public string Client_Email { get; set; }
        [StringLength(50)]
        public string Basic_Fees { get; set; }
        [StringLength(50)]
        public string Additional_Fees { get; set; }

        [StringLength(50)]
        public string OutofGeo_Fees { get; set; }

        [StringLength(50)]
        public string Total_Fees { get; set; }
    }
}