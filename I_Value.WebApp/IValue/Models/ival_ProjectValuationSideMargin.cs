namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_ProjectValuationSideMargin
    {
        [Key]
        public int ProjectSideMargin_ID { get; set; }

        public int? Project_ID { get; set; }

        public int? Building_ID { get; set; }

        public int? Wing_ID { get; set; }

        public int? Request_ID { get; set; }

        [StringLength(100)]
        public string Name_Of_Building { get; set; }

        [StringLength(50)]
        public string Name_Of_Wing { get; set; }

        [StringLength(50)]
        public string Approved_Front_Side_Margin { get; set; }

        [StringLength(50)]
        public string Approved_Left_Side_Margin { get; set; }

        [StringLength(50)]
        public string Approved_Right_Side_Margin { get; set; }

        [StringLength(50)]
        public string Approved_Rear_Side_Margin { get; set; }

        [StringLength(50)]
        public string Actual_Front_Side_Margin { get; set; }

        [StringLength(50)]
        public string Actual_Left_Side_Margin { get; set; }

        [StringLength(50)]
        public string Actual_Right_Side_Margin { get; set; }

        [StringLength(50)]
        public string Actual_Rear_Side_Margin { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}
