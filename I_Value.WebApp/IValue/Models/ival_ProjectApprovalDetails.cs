namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_ProjectApprovalDetails
    {
        [Key]
        public int PApproval_ID { get; set; }

        public int? Project_ID { get; set; }

        public int? Building_ID { get; set; }

        public int? Wing_ID { get; set; }

        public int? Unit_ID { get; set; }

        public bool? Is_Active { get; set; }

        [StringLength(50)]
        public string Approval_Type { get; set; }

        [StringLength(50)]
        public string Approving_Authority { get; set; }

        public DateTime? Date_of_Approval { get; set; }

        [StringLength(100)]
        public string Date_of_ApprovalNew { get; set; }

        [StringLength(100)]
        public string Approval_Num { get; set; }

        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
    }
}
