namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_ProjectValuationAmenitiesInfrastructureProgress
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProjectAmenities_ID { get; set; }

        public int? Project_ID { get; set; }

        [StringLength(50)]
        public string Amenity_Description { get; set; }

        [StringLength(50)]
        public string Description_Of_Stage_Of_Construction { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Percentage_Work_Completed { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Percentage_Disbursement_Recommended { get; set; }

        [StringLength(50)]
        public string Maintenance_Of_Property { get; set; }

        [StringLength(50)]
        public string Quality_Of_Construction { get; set; }
    }
}
