namespace IValuePublishProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ival_LookupCategoryValues
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Lookup_ID { get; set; }

        [StringLength(50)]
        public string Lookup_Type { get; set; }

        [StringLength(50)]
        public string Lookup_Value { get; set; }

        [StringLength(50)]
        public string Lookup_Description { get; set; }
        [StringLength(50)]
        public string Created_By { get; set; }

        public DateTime? Created_Date { get; set; }

        [StringLength(50)]
        public string Updated_By { get; set; }

        public DateTime? Updated_Date { get; set; }
        public int Dept_ID { get; set; }
        [StringLength(50)]
        public string Dept_Name { get; set; }
        [StringLength(50)]
        public string Client_Name { get; set; }
        [StringLength(50)]
        public string Contact_Num { get; set; }
        [StringLength(50)]
        public string Client_Desgn1 { get; set; }
        [StringLength(50)]
        public string Client_Mail { get; set; }
    }
}
