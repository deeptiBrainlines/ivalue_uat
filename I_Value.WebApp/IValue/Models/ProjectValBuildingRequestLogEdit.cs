﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public class ProjectValBuildingRequestLogEdit
    {
        public int BuildingId { get; set; }
        public string BuildingName { get; set; }
        public int ProjectValuationRequestID { get; set; }
    }
}