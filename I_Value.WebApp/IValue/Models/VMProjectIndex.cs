﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public partial class VMProjectIndex
    {
        [Key]
        public int Project_ID { get; set; }

        

        [StringLength(250)]
        public string BuilderGroupName { get; set; }

        [StringLength(250)]
        public string Builder_Company_Name { get; set; }

        [StringLength(250)]
        public string Project_Name { get; set; }

        [StringLength(250)]
        public string City { get; set; }

        [StringLength(250)]
        public string State { get; set; }

        public List<VMProjectIndex> VMProjectIndexs { get; set; }

    }
}