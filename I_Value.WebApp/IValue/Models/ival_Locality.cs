﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public class ival_Locality
    {
        [Key]
        public int Loc_Id { get; set; }

        public int Lacality_Id { get; set; }

        [StringLength(100)]
        public string Loc_Name { get; set; }

        public int Pin_Code { get; set; }

        public int District_ID { get; set; }

        [StringLength(250)]
        public string District_Name { get; set; }
    }

}