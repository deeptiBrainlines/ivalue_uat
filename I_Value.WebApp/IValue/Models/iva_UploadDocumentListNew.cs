﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IValuePublishProject.Models
{
    public partial class iva_UploadDocumentListNew
    {
        [Key]
        public int Docupload_ID { get; set; }

        public int? Document_ID { get; set; }
        public int? SrNo { get; set; }

        public int? Request_ID { get; set; }

        public int? Unit_ID { get; set; }

        [StringLength(50)]
        public string Uploaded_Document { get; set; }
        [StringLength(50)]
        public string Document_Name { get; set; }

        public DateTime? Date_Of_Approval { get; set; }
        public string Date_Of_Approval_New { get; set; }

        [StringLength(50)]
        public string Approval_Authority { get; set; }

        [StringLength(50)]
        public string Approval_Number { get; set; }
        [StringLength(50)]
        public string FileName { get; set; }

    }
}